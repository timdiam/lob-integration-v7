import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree
import openerp.addons.decimal_precision as dp
import openerp.exceptions

from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp import tools
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging
import xml.etree.cElementTree as ET


class crm_lead(osv.osv):
    _inherit='crm.lead'

    _columns={
                "push_pull":fields.selection([('push',"Push"),('pull','Pull')],string='Push/Pull'),
                "visitado":fields.boolean('Visitado ?'),
                "propuesta_entregado":fields.boolean('Propuesta Entregada ?'),
                "nuevo_repeat":fields.selection([('nuevo',"Nuevo"),('repeat','Repeat')],string='Nuevo/Repeat'),
                "quarter":fields.selection([('quarter_1',"1 Cuarto"),('quarter_2','2 Cuarto'),('quarter_3','3 Cuarto')],string='Cuarto'),
                "year":fields.integer("Ano"),
                "alcanze_proyecto":fields.selection([('estrategia',"Estrategia"),('gobierno_corp','Gobierno Corporativo'),('innovacion','Innovacion'),('Valoracion','Valoracion')],string='Alcance de Proyecto'),
    }


class ec_opportunity_report_wizard(osv.osv_memory):
    _name="ec.opportunity.report.wizard"

    def generar_reporte_opp(self,cr,uid,ids,context=None):
        records=self.browse(cr,uid,ids)[0]
        result = {}
        year_wizard=records.year
        context={'year':int(records.year)}
        year_wizard=int(records.year)
        ids=[]
        ids= self.pool.get("crm.lead").search(cr,uid,[("year","=",year_wizard)])

        result = { 'type': 'ir.actions.report.xml',
                   'context': context,
                   'report_name': 'report_kpi_wizard',
                   'datas': {'ids': ids}


        }

        return result

    _columns={
                "year":fields.integer("Ano"),

        }


class res_partner(osv.osv):
    _inherit="res.partner"

    def get_promedio(self, cr, uid, ids, field_name, field_value, arg, context=None):
            records = self.browse(cr, uid, ids)
            result = {}
            promedio=0
            for r in records:
                promedio=((float(r.cap_referirnos)+float(r.potencial_negocio)+float(r.sostificacion)+float(r.valor_agregado)+float(r.necesidad_lbs)+float(r.potencial_lbs)+float(r.posibilidad_lbs)+float(r.consultores)+float(r.monto)+float(r.propuesta)))/10
            result[r.id]=promedio
            return result

    _columns={
                "size_company":fields.selection([('grande',"Grande"),('media','Medina'),('pequena','Pequena')],string='Tamano de la Empresa'),
                "cap_referirnos":fields.selection([('1',"alta"),('2','media'),('3','baja')],string="Capacidad de referirnos"),
                "potencial_negocio":fields.selection([('1',"alta"),('2','media'),('3','baja')],string="Potencialidad de negocios futuros propios"),
                "sostificacion":fields.selection([('1',"alta"),('2','media'),('3','baja')],string="Sofisticacion de la empresa"),
                "valor_agregado":fields.selection([('1',"alta"),('2','media'),('3','baja')],string="Valor agregado para el cliente"),
                "necesidad_lbs":fields.selection([('1',"alta"),('2','media'),('3','baja')],string="Necesidad de LBS"),
                "potencial_lbs":fields.selection([('1',"alta"),('2','media'),('3','baja')],string="Potencial Diversificacion para LBS"),
                "posibilidad_lbs":fields.selection([('1',"alta"),('2','media'),('3','baja')],string="Posibilidad de aprendizaje de LBS"),
                "consultores":fields.selection([('1',"alta"),('2','media'),('3','baja')],string="Atractividad del proyecto para consultores"),
                "monto":fields.selection([('1',"alta"),('2','media'),('3','baja')],string="Monto proyecto individual"),
                "propuesta":fields.selection([('1',"alta"),('2','media'),('3','baja')],string="Probabilidad de exito inmediato de la propuesta"),
                "promedio":fields.function(get_promedio, string='Promedio', method=True, type='float'),
                }
