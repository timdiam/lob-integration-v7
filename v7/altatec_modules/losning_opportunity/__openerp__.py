{
    'name': 'Opportunity',
    'version': '1.0',
    'description': """
        Opportunity Addons
    """,
    'author': 'Harry Alvarez',
    'website': 'www.google.com',
    "depends" : ['crm'],
    "data" : [
                'losning_opportunity_view.xml',

                ],
    "installable": True,
    "auto_install": False
}