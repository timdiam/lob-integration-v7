# -*- coding: utf-8 -*-

{
    "name": 'remove_create_partner_voucher',
    "version": "0.1",
    "description": """
    
    Remove Create Partner test

""",
    "depends": [
        'base',
        'web',
        'web_m2x_options',
        'account_voucher',
        'account',
    ],
    "data": [
        'account_voucher.xml',
    ],
    "author": "AltaERP",
    "installable": True,
}
