# -*- coding: utf-8 -*-

from openerp.osv import fields, osv, orm
from openerp import netsvc


class confirm_payroll_wizard(osv.osv_memory):
    _name = 'confirm.payroll.wizard'

    #Obtiene los ids de la nominas seleccionadas y obtiene los campos de interes
    #Gets ids of payslip selected and fields which are shown on resume
    def get_payslip_line(self, cr, uid, context=None):

        result = []

        if not context or 'active_ids' not in context:
            raise osv.except_osv("Programming Error", "confirm_payroll_wizard get_payslip_lines() Context not found or not correct")

        for p in self.pool.get('hr.payslip').browse(cr, uid, context['active_ids'], context=context):
            payslip_dict = {
                            'reference' : p.number,
                            'employee'  : p.employee_id.name,
                            'date_from' : p.date_from,
                            'date_to'   : p.date_to,
                            'state'     : self.convert_state_to_name(p.state),
            }
            result.append((0,0,payslip_dict))

        return result

    #Confirma las nominas seleccionadas cuyo estado sea Borrador, caso contrario muestra mensaje de error
    #Confirms payslips selected which status is Draft, otherwise shows an error message
    def confirm_payslip(self, cr, uid, id, context=None):
        no_draft_payslip = []

        if not context or 'active_ids' not in context:
            raise osv.except_osv("Programming Error", "confirm_payroll_wizard get_payslip() Context not found or not correct")

        for p in self.pool.get('hr.payslip').browse(cr, uid, context['active_ids'], context=context):
            if p.state == 'draft':
                wf_service = netsvc.LocalService("workflow")
                wf_service.trg_validate(uid, 'hr.payslip', p.id, 'hr_verify_sheet', cr)
            else:
                #Guarda las nominas que no estan en estado Draft/Borrador
                no_draft_payslip.append(p.number)

        if no_draft_payslip:
            raise osv.except_osv("Error!", '-'.join(no_draft_payslip) + " no esta/estan en estado Borrador. Seleccione solo nominas en estado Borrador")


    #Traduce el estado a espanol
    #Translates state to spanish
    def convert_state_to_name(self, state):

        if( state == 'draft' ):
            return 'Borrador'
        elif( state == 'paid' ):
            return 'Pagado'
        elif( state == 'verify' ):
            return 'En espera'
        elif( state == 'done' ):
            return 'Realizado/a'
        elif( state == 'cancel' ):
            return 'Cancelado'
        else:
            return ''

    _columns = {
                "payslip_lines" : fields.one2many('confirm.payroll.wizard.lines','wizard_id',"Nomina"),
            }

    _defaults = {
               "payslip_lines" : get_payslip_line,
    }


#Contiene la informacion que se muestra en el resumen de las nominas seleccionadas
#Get fields of interest which are shown on resume according to payslip selected
class confirm_payroll_wizard_lines(osv.osv_memory):
    _name = 'confirm.payroll.wizard.lines'

    _columns = {
            'wizard_id' : fields.many2one("confirm.payroll.wizard", "Wizard ID"),
            'reference' : fields.char('Referencia'),
            'employee' : fields.char('Empleado'),
            'date_from': fields.date('Fecha desde'),
            'date_to'  : fields.date('Fecha hasta'),
            'state'    : fields.char('Estado'),
    }