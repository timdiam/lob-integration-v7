{
    'name': 'Confirm Several Payroll',
    'version': '1.0',
    'description': """
        Confirm Several Payroll
    """,
    'author': 'Denisse Ochoa',
    'website': 'www.altatec.com.ec',
    "depends" : ['hr_payroll',
                 ],
    "data" : [ 'views/confirm_payroll_batch_view.xml',
            ],
    "installable": True,
    "auto_install": False
}

