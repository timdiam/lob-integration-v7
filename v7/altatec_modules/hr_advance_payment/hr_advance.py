# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 TRESCloud (<http://www.trescloud.com>)
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from osv import osv, fields
import time
from openerp.tools.translate import _
from openerp import netsvc
import openerp.exceptions
import openerp.addons.decimal_precision as dp

class hr_salary_reminder(osv.Model):

    _name = 'hr.salary.reminder'
    _inherits = {'hr.salary.rule': 'rule_id'}

    _columns = {
                'employee_id' : fields.many2one('hr.employee', 'Employee', required=False),
                'rule_id' : fields.many2one('hr.salary.rule', 'Salary Rule'),
                'processed' : fields.boolean('Processed ?'),
                'rem_date' : fields.date('Date'),
                'invoice_id' : fields.many2one('hr.advance', 'Invoice'),
                }

class account_invoice(osv.Model):
    _inherit = 'account.invoice'
    
    _columns = {
        'advances' : fields.boolean('Advances'),
        #agregamos el tipo de documento para anticipos a empleados
        'type': fields.selection([
            ('out_invoice','Customer Invoice'),
            ('in_invoice','Supplier Invoice'),
            ('out_refund','Customer Refund'),
            ('in_refund','Supplier Refund'),
            ('hr_advance','Employee Advance'),
            ],'Type', readonly=True, select=True, change_default=True, track_visibility='always'),
    }
    
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        args.insert(0, ['advances', '=', False])
        return super(account_invoice, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)

account_invoice()

class hr_advance(osv.Model):
    
    _name = 'hr.advance'
    
    _inherits = {'account.invoice' : 'invoice_id'}
    _inherit = ['mail.thread']

    _order = 'id desc'
    
    def _get_payment_term(self, cr, uid, context=None):
        if not context:
            context={}
        if context.get('default_advances')==True:
            ir_data_obj=self.pool.get('ir.model.data')
            ir_datas=ir_data_obj.search(cr,uid,[('name','=','account_payment_term_immediate')])
            payment_term=ir_data_obj.browse(cr,uid,ir_datas)[0].res_id  
        return payment_term

    def _change_state(self, cr, uid, ids, field, arg, context=None):
        res={}
        if context is None:
            context = {}
        toremove = []
        obj_move_line = self.pool.get('account.move.line')

        for payslip in self.browse(cr,uid,ids):

            done = {}
            if not payslip.state in ['draft','cancel']:
                account_voucher_obj=self.pool.get('account.voucher')
                account_move_obj=self.pool.get('account.move')
                if payslip.move_id:
                    move=payslip.move_id
                    line_ids = map(lambda x: x.id, move.line_id)
                    for line in obj_move_line.browse(cr, uid, line_ids, context=context):
                       # err_msg = _('Move name (id): %s (%s)') % (line.move_id.name, str(line.move_id.id))
                        if line.move_id.state == 'posted' and line.journal_id.entry_posted and line.reconcile_id:
                            self.write(cr,uid,payslip.id,{'state':'paid'})
                            res[payslip.id]=True


                    #This hack fixes the issue where someone pays an anticipo, but then cancels the payment.
                    #before, the system would never again change the state from paid....
                    none_reconciled = True

                    for line in obj_move_line.browse(cr, uid, line_ids, context=context):
                        if line.move_id.state == 'posted' and line.journal_id.entry_posted and line.reconcile_id:
                            none_reconciled = False

                    if none_reconciled:
                        self.write(cr,uid,payslip.id,{'state':'open'})
                        res[payslip.id]=True
        return res


    # def _get_invoice_from_line_advance(self, cr, uid, ids, context=None):
    #     move = {}
    #     for line in self.pool.get('account.move.line').browse(cr, uid, ids, context=context):
    #         if line.reconcile_partial_id:
    #             for line2 in line.reconcile_partial_id.line_partial_ids:
    #                 move[line2.move_id.id] = True
    #         if line.reconcile_id:
    #             for line2 in line.reconcile_id.line_id:
    #                 move[line2.move_id.id] = True
    #
    #     advance_ids = []
    #
    #     if move:
    #         advance_ids = self.pool.get('hr.advance').search(cr,uid,[('move_id','in',move.keys())], context=context)
    #     return advance_ids
    #
    # def _get_invoice_from_reconcile_advance(self, cr, uid, ids, context=None):
    #     move = {}
    #     for r in self.pool.get('account.move.reconcile').browse(cr, uid, ids, context=context):
    #         for line in r.line_partial_ids:
    #             move[line.move_id.id] = True
    #         for line in r.line_id:
    #             move[line.move_id.id] = True
    #
    #     advance_ids = []
    #     if move:
    #         advance_ids = self.pool.get('hr.advance').search(cr,uid,[('move_id','in',move.keys())], context=context)
    #
    #     return advance_ids
    #
    # def _amount_residual_advance(self, cr, uid, ids, name, args, context=None):
    #     """Function of the field residua. It computes the residual amount (balance) for each invoice"""
    #     if context is None:
    #         context = {}
    #     ctx = context.copy()
    #     result = {}
    #     currency_obj = self.pool.get('res.currency')
    #     for invoice in self.browse(cr, uid, ids, context=context):
    #         result[invoice.id] = 0.0
    #         credit_sum = 0
    #         debit_sum = 0
    #         if invoice.move_id:
    #             for payment in invoice.payment_ids:
    #                 credit_sum += payment.credit
    #                 debit_sum += payment.debit
    #
    #         result[invoice.id] = round(invoice.amount_total - (debit_sum - credit_sum),2)
    #
    #     return result

    ########################################################################################
    # COPIED DIRECTLY FROM ACCOUNT_INVOICE.  THIS CODING ISN'T WORKING.  SO WE CHOOSE TO MAKE SIMPLE FUNCTION INSTEAD
    ########################################################################################3
    # def _amount_residual_advance(self, cr, uid, ids, name, args, context=None):
    #     """Function of the field residua. It computes the residual amount (balance) for each invoice"""
    #     if context is None:
    #         context = {}
    #     ctx = context.copy()
    #     result = {}
    #     currency_obj = self.pool.get('res.currency')
    #     for invoice in self.browse(cr, uid, ids, context=context):
    #         nb_inv_in_partial_rec = max_invoice_id = 0
    #         result[invoice.id] = 0.0
    #         if invoice.move_id:
    #             for aml in invoice.move_id.line_id:
    #                 if aml.account_id.type in ('receivable','payable'):
    #                     if aml.currency_id and aml.currency_id.id == invoice.currency_id.id:
    #                         result[invoice.id] += aml.amount_residual_currency
    #                     else:
    #                         ctx['date'] = aml.date
    #                         result[invoice.id] += currency_obj.compute(cr, uid, aml.company_id.currency_id.id, invoice.currency_id.id, aml.amount_residual, context=ctx)
    #
    #                     if aml.reconcile_partial_id.line_partial_ids:
    #                         #we check if the invoice is partially reconciled and if there are other invoices
    #                         #involved in this partial reconciliation (and we sum these invoices)
    #                         for line in aml.reconcile_partial_id.line_partial_ids:
    #                             advances=self.search(cr,uid,[('move_id','=',line.move_id.id)], limit=1,context=context)
    #                             if advances:
    #                                 nb_inv_in_partial_rec += 1
    #                                 #store the max invoice id as for this invoice we will make a balance instead of a simple division
    #                                 max_invoice_id = max(max_invoice_id, self.browse(cr,uid,advances[0],context=context).id)
    #         if nb_inv_in_partial_rec:
    #             #if there are several invoices in a partial reconciliation, we split the residual by the number
    #             #of invoice to have a sum of residual amounts that matches the partner balance
    #             new_value = currency_obj.round(cr, uid, invoice.currency_id, result[invoice.id] / nb_inv_in_partial_rec)
    #             if invoice.id == max_invoice_id:
    #                 #if it's the last the invoice of the bunch of invoices partially reconciled together, we make a
    #                 #balance to avoid rounding errors
    #                 result[invoice.id] = result[invoice.id] - ((nb_inv_in_partial_rec - 1) * new_value)
    #             else:
    #                 result[invoice.id] = new_value
    #
    #         #prevent the residual amount on the invoice to be less than 0
    #         result[invoice.id] = max(result[invoice.id], 0.0)
    #     return result


    def _get_advance_from_reconcile(self, cr, uid, ids, context=None):
        move = {}
        for r in self.pool.get('account.move.reconcile').browse(cr, uid, ids, context=context):
            for line in r.line_partial_ids:
                move[line.move_id.id] = True
            for line in r.line_id:
                move[line.move_id.id] = True

        payslip_ids = []
        if move:
            payslip_ids = self.pool.get('hr.advance').search(cr, uid, [('move_id','in',move.keys())], context=context)
        return payslip_ids

    def _get_advance_from_line(self, cr, uid, ids, context=None):
        move = {}
        for line in self.pool.get('account.move.line').browse(cr, uid, ids, context=context):
            if line.reconcile_partial_id:
                for line2 in line.reconcile_partial_id.line_partial_ids:
                    move[line2.move_id.id] = True
            if line.reconcile_id:
                for line2 in line.reconcile_id.line_id:
                    move[line2.move_id.id] = True
        payslip_ids = []
        if move:
            payslip_ids = self.pool.get('hr.advance').search(cr, uid, [('move_id','in',move.keys())], context=context)
        return payslip_ids

    _columns = {
        # 'residual': fields.function(_amount_residual_advance, digits_compute=dp.get_precision('Account'), string='Balance',
        #     store={
        #         'hr.advance': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line','move_id'], 50),
        #         'account.move.line': (_get_invoice_from_line_advance, None, 50),
        #         'account.move.reconcile': (_get_invoice_from_reconcile_advance, None, 50),
        #     }),
        'invoice_id' : fields.many2one('account.invoice', 'Invoice'),
        'reminder_ids' : fields.one2many('hr.salary.reminder', 'invoice_id', 'Reminder'),
        'if_pay':fields.function(_change_state,string='If Pay',type='boolean',method=True,
                                store={
                                        'account.move.line': (_get_advance_from_line, None, 50),
                                        'account.move.reconcile': (_get_advance_from_reconcile, None, 50),
                                     },),
       # 'document_invoice_type_id': fields.many2one('account.invoice.document.type', 'Document type', required=True, help='Indicates the type of accounting document authorized to issue when making a purchase or sale.',),
    }

    _defaults = {
        'advances' : True,
        'payment_term':_get_payment_term,
    }
    
    def action_invoice_sent(self, cr, uid, ids, context=None):
        invoice_obj = self.pool.get('account.invoice')
        invoice_ids = [adv.invoice_id.id for adv in self.browse(cr, uid, ids, context=context)]
        return invoice_obj.action_invoice_sent(cr, uid, invoice_ids, context=context)
        
    def invoice_print(self, cr, uid, ids, context=None):
        invoice_obj = self.pool.get('account.invoice')
        invoice_ids = [adv.invoice_id.id for adv in self.browse(cr, uid, ids, context=context)]
        return invoice_obj.invoice_print(cr, uid, invoice_ids, context=context)
    
    def onchange_employee_id(self, cr, uid, ids, type, partner_id,\
            date_invoice=False, payment_term=False, partner_bank_id=False, company_id=False,journal_id=False):
        '''
        Para evitar inconvenientes con las facturas el metodo toma otro nombre
        '''
        invoice_obj = self.pool.get('account.invoice')
        invoice_ids = [adv.invoice_id.id for adv in self.browse(cr, uid, ids)]
        #res= invoice_obj.onchange_partner_id(cr, uid, invoice_ids, type, partner_id,\
        #                                     date_invoice=date_invoice, payment_term=payment_term, partner_bank_id=partner_bank_id, company_id=company_id)
        res= invoice_obj.onchange_partner_id(payment_term, partner_bank_id, type, False,\
                                             partner_id, False, False, False, False, False)
        '''DESARROLLO ANDREA GARCIA'''
        if journal_id:
            journal_obj=self.pool.get('account.journal')
            journal=journal_obj.browse(cr,uid,journal_id)
            if journal.default_debit_account_id:
                res["value"].update({"account_id":journal.default_debit_account_id.id}) 
        context={}
        context['default_advances']=True
        payment_term= self._get_payment_term(cr, uid,context=context)
        res["value"].update({"payment_term":payment_term})
        '''END DESARROLLO'''
        return res
    
    def onchange_journal_id(self, cr, uid, ids, journal_id=False, context=None):
        invoice_obj = self.pool.get('account.invoice')
        invoice_ids = [adv.invoice_id.id for adv in self.browse(cr, uid, ids, context=context)]
        return invoice_obj.onchange_journal_id(cr, uid, invoice_ids, journal_id=journal_id, context=context)
    
    def onchange_company_id(self, cr, uid, ids, company_id, part_id, type, invoice_line, currency_id):
        invoice_obj = self.pool.get('account.invoice')
        invoice_ids = [adv.invoice_id.id for adv in self.browse(cr, uid, ids)]
        res=invoice_obj.onchange_company_id(cr, uid, invoice_ids, company_id, part_id, 'out_invoice', invoice_line, currency_id)
        obj_journal = self.pool.get('account.journal')
        journal_ids = obj_journal.search(cr, uid, [('company_id','=',company_id), ('type', '=', 'general'),('code','=','ANEMP')])  
        res['domain']['journal_id'] = [('id', 'in', journal_ids)]
        if journal_ids:
            res['value']['journal_id']=journal_ids[0] or False
        return res
    
    def action_cancel_draft(self, cr, uid, ids, *args):
        self.write(cr, uid, ids, {'state':'draft'})
        wf_service = netsvc.LocalService("workflow")
        for inv_id in ids:
            wf_service.trg_delete(uid, 'hr.advance', inv_id, cr)
            wf_service.trg_create(uid, 'hr.advance', inv_id, cr)
        return True
    
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        if context is None:
            context = {}
        if context.get('employee_id', False):
            employee_obj = self.pool.get('hr.employee')
            employee_ids = employee_obj.search(cr, uid, [('id', 'in', context.get('employee_id', False))], context=context)
            emp_recs = employee_obj.browse(cr, uid, employee_ids, context=context)
            partner_ids = [emp.address_home_id.id for emp in emp_recs]
            args.extend([['partner_id', 'in', partner_ids]])
        return super(hr_advance, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)
    
    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
        if context is None:
            context = {}
        res = super(hr_advance, self).fields_view_get(cr, uid, view_id, view_type, context, toolbar=toolbar, submenu=submenu)
        if not context.get('default_advances'):
            return res
        contract_obj = self.pool.get('hr.contract')
        contract_ids = contract_obj.search(cr, uid, [('date_start','<=',time.strftime('%Y-%m-%d %H:%M:%S'))])
        cont_recs = contract_obj.browse(cr, uid, contract_ids, context=context)
        partner_ids = []
        for contract in cont_recs:
            if contract.employee_id.address_home_id:
                partner_ids.append(contract.employee_id.address_home_id and contract.employee_id.address_home_id.id)
        #new_domain = "[('customer', '=', False), ('id', 'in', " + (str(partner_ids) or False) + ")]"
        new_domain = "[('id', 'in', " + (str(partner_ids) or False) + ")]"
        arch = res['arch'].replace("[('customer', '=', False),('supplier', '=', False)]", new_domain)
        
        res.update({'arch' : arch})
        return res
    
    def invoice_validate(self, cr, uid, ids, context=None):
        res = self.write(cr, uid, ids, {'state':'open'}, context=context)
        reminder_obj = self.pool.get('hr.salary.reminder')
        salary_rule=self.pool.get('hr.salary.rule.category').search(cr,uid,[('code','=','EGRESOS')])
        reminders=[]
        if salary_rule:
            categ=salary_rule[0]
        rem_categ = self.pool.get('ir.model.data').get_object(cr, uid, 'hr_payroll', 'DED', context)
        employee_obj = self.pool.get('hr.employee')
        invoice_line_obj = self.pool.get('account.invoice.line')
        for invoice in self.browse(cr, uid, ids, context=context):
            if not invoice.advances:
                continue
            employee_ids = employee_obj.search(cr, uid, [('address_home_id', 'in', [invoice.partner_id.id])], context=context)
            number = 0
            debit_account_id = False
            credit_account_id = False
            '''Desarrollo Andrea Garcia'''
            if invoice.journal_id.default_credit_account_id:
                debit_account_id=invoice.journal_id.default_credit_account_id.id
            '''End Desarrollo'''
            for move_line in invoice.move_id.line_id:
                number += 1
                if invoice.account_id and move_line.debit!=0.00:
                    credit_account_id=invoice.account_id.id
                    reminder_vals = {
                        'employee_id' : employee_ids and employee_ids[0] or False,
                        'name' : ''+invoice.invoice_line[0].name+'/'+invoice.number,
                        'category_id' : categ,
                        'code' : invoice.number + '-' + str(number),
                        'amount_fix' : move_line.debit,
                        'rem_date' : invoice.move_id and move_line.date_maturity or False,
                        'account_debit' : debit_account_id,
                        'account_credit' : credit_account_id,
                        'invoice_id' : invoice.id
                    }
                    reminder_id = reminder_obj.create(cr, uid, reminder_vals, context=context)
                    reminders.append((4,reminder_id))
            self.write(cr, uid, invoice.id, {'reminder_ids':reminders},context=context)
        
        return res
    
    def button_reset_taxes(self, cr, uid, ids, context=None):
        '''
        Se redefine la funion pues no se debe afectar al objeto fatura
        y ademas el objeto hr_advance no requiere manejo de impeustos
        '''
        #--------------------------------------------------- if context is None:
            #------------------------------------------------------ context = {}
        #-------------------------------------------------- ctx = context.copy()
        #------------------------ ait_obj = self.pool.get('account.invoice.tax')
        #-------------------------------------------------------- for id in ids:
            # cr.execute("DELETE FROM account_invoice_tax WHERE invoice_id=%s AND manual is False", (id,))
            #-------- partner = self.browse(cr, uid, id, context=ctx).partner_id
            #-------------------------------------------------- if partner.lang:
                #---------------------------- ctx.update({'lang': partner.lang})
            #--- for taxe in ait_obj.compute(cr, uid, id, context=ctx).values():
                #--------------------------------- ait_obj.create(cr, uid, taxe)
        # # Update the stored value (fields.function), so we write to trigger recompute
        # self.pool.get('account.invoice').write(cr, uid, ids, {'invoice_line':[]}, context=ctx)
        return True
    
    def action_cancel(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        invoice_obj = self.pool.get('account.invoice')
        invoice_ids = [adv.invoice_id.id for adv in self.browse(cr, uid, ids, context=context)]
        res = invoice_obj.action_cancel(cr, uid, invoice_ids)
        reminder_obj = self.pool.get('hr.salary.reminder')
        for invoice in self.browse(cr, uid, ids, context=context):
            if not invoice.advances:
                continue
            rem_list=reminder_obj.search(cr,uid,[('invoice_id','=',invoice.id)])
            for rem_line in reminder_obj.browse(cr,uid,rem_list):
                if rem_line.processed:
                    raise osv.except_osv(_('Warning !'), _('Invoice cannot be cancelled. One or All Reminders are already paid.'))
                else:
                    reminder_obj.unlink(cr, uid, rem_line.id, context)
        
        return res
    
    def action_date_assign(self, cr, uid, ids, *args):
        invoice_obj = self.pool.get('account.invoice')
        invoice_ids = [adv.invoice_id.id for adv in self.browse(cr, uid, ids)]
        return invoice_obj.action_date_assign(cr, uid, invoice_ids, args)
    
    def action_move_create(self, cr, uid, ids, context=None):
        #el tipo hr_advance debe comportarse como un in_invoice para el comportamiento deseado
        invoice_obj = self.pool.get('account.invoice')
        invoice_ids = [adv.invoice_id.id for adv in self.browse(cr, uid, ids, context=context)]
        for inv_id in invoice_ids:
            #TODO HOW WAS THIS WRONG ALL THIS TIME? MAJOR BUG.  Can't fix
            #quickly becuase most likely influences accounts on the reminder, etc...
            invoice_obj.write(cr, uid, [inv_id], {'type': 'out_invoice'})
            
        res = invoice_obj.action_move_create(cr, uid, invoice_ids, context=context)

        for inv_id in invoice_ids:
            invoice_obj.write(cr, uid, [inv_id], {'type': 'hr_advance'})
        return res
    
    def action_number(self, cr, uid, ids, context=None):
        invoice_obj = self.pool.get('account.invoice')
        invoice_ids = [adv.invoice_id.id for adv in self.browse(cr, uid, ids, context=context)]
        return invoice_obj.action_number(cr, uid, invoice_ids, context=context)
    
    def confirm_paid(self, cr, uid, ids, context=None):
        invoice_obj = self.pool.get('account.invoice')
        invoice_ids = [adv.invoice_id.id for adv in self.browse(cr, uid, ids, context=context)]
        return invoice_obj.confirm_paid(cr, uid, invoice_ids, context=context)
    
    def move_line_id_payment_get(self, cr, uid, ids, *args):
        invoice_obj = self.pool.get('account.invoice')
        invoice_ids = [adv.invoice_id.id for adv in self.browse(cr, uid, ids)]
        return invoice_obj.move_line_id_payment_get(cr, uid, invoice_ids, args)

    def test_paid(self, cr, uid, ids, *args):
        invoice_obj = self.pool.get('account.invoice')
        invoice_ids = [adv.invoice_id.id for adv in self.browse(cr, uid, ids)]
        return invoice_obj.test_paid(cr, uid, invoice_ids, args)
    
    def button_proforma_voucher(self, cr, uid, ids, context=None):
        context = context or {}
        res_id=0
        inv=self.browse(cr,uid,ids)[0]
        wf_service = netsvc.LocalService("workflow")
        mod_obj = self.pool.get('ir.model.data')
        res = mod_obj.get_object_reference(cr, uid, 'account_voucher', 'view_vendor_payment_form')
        res_ids = res and res[1] or False,
        res_id=res_ids[0]
        return {
            'name': _('Voucher Payment'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res_id],
            'res_model': 'account.voucher',
            'context': {
                    'invoice_type':'in_invoice',
                    'default_amount':inv.amount_total,
                    'default_partner_id':inv.partner_id.id,
                    'invoice_type':'in_invoice',
                    'type':'payment',
                    'default_type':'payment',
                    'invoice_id':inv.invoice_id.id,
                },
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': False,
        }

    def name_get(self, cr, uid, ids, context=None):
        if not ids:
            return []
        types = {
                'out_invoice': _('Advance'),
                'in_invoice': _('Supplier Invoice'),
                'out_refund': _('Refund'),
                'in_refund': _('Supplier Refund'),
                'hr_advance': _('Employee Advance'),
                }
        return [(r['id'], '%s %s' % (r['number'] or types[r['type']], r['name'] or '')) for r in self.read(cr, uid, ids, ['type', 'number', 'name'], context, load='_classic_write')]

    def name_search(self, cr, user, name, args=None, operator='ilike', context=None, limit=100):
        if not args:
            args = []
        if context is None:
            context = {}
        ids = []
        if name:
            ids = self.search(cr, user, [('number','=',name)] + args, limit=limit, context=context)
        if not ids:
            ids = self.search(cr, user, [('name',operator,name)] + args, limit=limit, context=context)
        return self.name_get(cr, user, ids, context)
    
    def copy(self, cr, uid, id, default=None, context=None):
        default = default or {}
        default.update({
            'state':'draft',
            'number':False,
            'move_id':False,
            'move_name':False,
            'internal_number': False,
            'period_id': False,
            'sent': False,
            'advances' : True
        })
        if 'date_invoice' not in default:
            default.update({
                'date_invoice':False
            })
        if 'date_due' not in default:
            default.update({
                'date_due':False
            })
        return super(hr_advance, self).copy(cr, uid, id, default, context)
    
    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        invoices = self.read(cr, uid, ids, ['state','internal_number'], context=context)
        unlink_ids = []
        
        for t in invoices:
            if t['state'] not in ('draft', 'cancel'):
                raise openerp.exceptions.Warning(_('You cannot delete an advance invoice which is not draft or cancelled. You should refund it instead.'))
            elif t['internal_number']:
                raise openerp.exceptions.Warning(_('You cannot delete an advance invoice after it has been validated (and received a number).  You can set it back to "Draft" state and modify its content, then re-confirm it.'))
            else:
                unlink_ids.append(t['id'])
        osv.osv.unlink(self, cr, uid, unlink_ids, context=context)
        return True
hr_advance()

class account_invoice_line(osv.Model):
    _inherit = 'account.invoice.line'
    
    def _get_product(self, cr, uid, context=None):
        if context is None:
            context = {}
        if context.get('advances', False):
            product_id = self.pool.get('ir.model.data').get_object(cr, uid, 'hr_advance_payment', 'product_product_employee', context)
            return product_id.id
        else:
            return False
    
    def product_id_change(self, cr, uid, ids, product, uom_id, qty=0, name='', type='out_invoice', partner_id=False, fposition_id=False, price_unit=False, currency_id=False, context=None, company_id=None):
        '''
        Hacemos que se comporte como out_invoice a pesar que el tipo es hr_advance
        '''
        if type == 'hr_advance':
            type = 'out_invoice'
        return super(account_invoice_line, self).product_id_change(cr, uid, ids, product, uom_id, qty, name, type, partner_id, fposition_id, price_unit, currency_id, context, company_id)
        
    _defaults = {
        'product_id' : _get_product,
        #'quantity':1
    }
account_invoice_line()