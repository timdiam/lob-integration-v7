# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

{
        "name" : "Human Resource Advance Payment",
        "version" : "1.2",
        "author" : "TRESCLOUD Cia Ltda",
        "website" : "http://www.trescloud.com",
        "category" : "Human Resources",
        "description": """
        Human Resources for Ecuadorian localization
        
        Authors:
        
        
        Andrea Garcia, Andres Calle
        """,
        "depends" : ['base',
                     #'hr_extra_input_output',
                     'ecua_invoice_type',
                     'report_aeroo',
                     'report_aeroo_ooo',
                     'sale_crm',
                     ],
        "update_xml" : ['data/sequence.xml',
                        'data/account_journals.xml',
                        'report/report.xml',
                        'hr_advance_view.xml',
                        'product_category_data.xml',
                        'product_data.xml',
                        'hr_advance_workflow.xml',
                        'security/ir.model.access.csv',
                        'wizard/hr_advances_employees_view.xml',
                        ],
        "auto_install": False,
        "installable": True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: