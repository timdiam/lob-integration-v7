# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors: Andrea García                                                                         
#Copyright (C) 2013 TRESCLOUD Cia. Ltda.                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

import time
from datetime import datetime
from dateutil import relativedelta
from openerp import netsvc
from openerp.osv import fields, osv
from openerp.tools.translate import _

class hr_advances_employees(osv.osv_memory):
    _name ='hr.advances.employees'
    
    def _get_payment_term(self, cr, uid, context=None):
        if not context:
            context={}
        payment_term=False
        ir_data_obj=self.pool.get('ir.model.data')
        ir_datas=ir_data_obj.search(cr,uid,[('name','=','account_payment_term_immediate')])
        payment_term=ir_data_obj.browse(cr,uid,ir_datas)[0].res_id  
        return payment_term
    
    def _get_journal(self, cr, uid, context=None):
        ir_model=self.pool.get('ir.model.data')
        res=ir_model.search(cr, uid, [('name', '=', 'employees_daily_advance')])
        res_id=False
        if res:
            res_id=ir_model.browse(cr,uid,res)[0].res_id
        return res_id
    
    def _get_account(self,cr,uid,context=None):
        journal_id=self._get_journal(cr, uid, context)
        account=False
        if journal_id:
            journal_obj=self.pool.get('account.journal')
            journal=journal_obj.browse(cr,uid,journal_id)
            if journal.default_debit_account_id:
                account=journal.default_debit_account_id.id

        return account
    
    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
        if context is None: context = {}
        fvg = super(hr_advances_employees, self).fields_view_get(cr, uid, view_id, view_type, context, toolbar, submenu)
        product_id = context and context.get('active_id', False) or False
        contract_obj = self.pool.get('hr.contract')
        contract_ids = contract_obj.search(cr, uid, [('date_start','<=',time.strftime('%Y-%m-%d %H:%M:%S'))])
        cont_recs = contract_obj.browse(cr, uid, contract_ids, context=context)
        partner_ids = []
        for contract in cont_recs:
            if contract.employee_id.address_home_id:
                partner_ids.append(contract.employee_id.id)
        new_domain = "[('id', 'in', " + (str(partner_ids) or False) + ")]"
        if fvg['fields'].get('employee_ids',False):
            arch = fvg['arch'].replace("[('address_home_id','!=',False)]", new_domain)
            fvg.update({'arch' : arch})
        return fvg
    
    _columns = {
        'type_calc':fields.selection([('amount','Amount'),('percentage','Percentage')],'Type of Calculation', help="You can choose the method that you want to calculate the advances"),
        'percentage':fields.float('Percentage', help="Percentage of nominal wages to deliver in advance. The nominal wage is set in the contract of each employee."),
        'amount':fields.float('Amount'),
        'date': fields.date('Invoice Date',select=False, help="Keep empty to use the current date"),
        'account_id': fields.many2one('account.account', 'Account', required=True, help="The partner account used for this invoice."),
        'journal_id': fields.many2one('account.journal', 'Journal', required=True),
        'document_invoice_type_id': fields.many2one('account.invoice.document.type', 'Document type', required=True, help='Indicates the type of accounting document authorized to issue when making a purchase or sale.',),
        'company_id': fields.many2one('res.company', 'Company', required=True, change_default=True, readonly=True),

        'payment_term': fields.many2one('account.payment.term', 'Payment Terms', 
            help="If you use payment terms, the due date will be computed automatically at the generation "\
                "of accounting entries. If you keep the payment term and the due date empty, it means direct payment. "\
                "The payment term may compute several due dates, for example 50% now, 50% in one month."),
        'employee_ids': fields.many2many('hr.employee', 'hr_advances_employees_group_rel', 'advance_id', 'employee_id', 'Employees'),
    }
    _defaults = {
        'payment_term':_get_payment_term,
        'journal_id':_get_journal,
        'company_id': lambda self,cr,uid,c: self.pool.get('res.company')._company_default_get(cr, uid, 'account.invoice', context=c),
        'account_id':_get_account,
        'type_calc':'amount',
    }
    
    def generate_batch(self, cr, uid, ids, context=None):
        res={}
        advance_obj=self.pool.get('hr.advance')
        advance_batch=self.browse(cr,uid,ids)[0]
        account_line_obj=self.pool.get('account.invoice.line')
        contract=self.pool.get('hr.contract')
        obj_employee=self.pool.get('hr.employee')
        advance_ids=[]
        wf_service = netsvc.LocalService("workflow")
        if advance_batch:
            if advance_batch.type_calc =='amount':
                res = {
                        'date_invoice': advance_batch.date,
                        'journal_id': advance_batch.journal_id.id,
                        'account_id': advance_batch.account_id.id,
                        'document_invoice_type_id': advance_batch.document_invoice_type_id.id,
                        'payment_term': advance_batch.payment_term.id,
                        'type': 'hr_advance',
                        'state':'open',
                    }
                context['advances']=True
                product_id=account_line_obj._get_product(cr,uid,context)
                product_obj=self.pool.get('product.product')
                product=product_obj.browse(cr,uid,product_id)
                for employee in advance_batch.employee_ids:
                    res['partner_id']=employee.address_home_id.id
                    adv_id=advance_obj.create(cr,uid,res)
                    advance=advance_obj.browse(cr,uid,adv_id)
                    if not product.property_account_income:
                        raise osv.except_osv("Error", "La cuenta de ingresos no esta configurada en el producto: " + str(product.name))
                    res_line={
                              'product_id':product_id,
                              'price_unit':advance_batch.amount,
                              'price_subtotal':advance_batch.amount,
                              'name':product.name,
                              'account_id':product.property_account_income.id,
                              'partner_id':employee.address_home_id.id,
                              'invoice_id':advance.invoice_id.id,
                              }
                    advance_id=account_line_obj.create(cr,uid,res_line) 
                    wf_service.trg_validate(uid, 'hr.advance', int(adv_id), 'invoice_open', cr)  
            elif advance_batch.type_calc =='percentage':
                res = {
                        'date_invoice': advance_batch.date,
                        'journal_id': advance_batch.journal_id.id,
                        'account_id': advance_batch.account_id.id,
                        'document_invoice_type_id': advance_batch.document_invoice_type_id.id,
                        'payment_term': advance_batch.payment_term.id,
                        'type': 'hr_advance',
                        'state':'open',
                    }
                context['advances']=True
                product_id=account_line_obj._get_product(cr,uid,context)
                product_obj=self.pool.get('product.product')
                product=product_obj.browse(cr,uid,product_id)
                advance_date=advance_batch.date
                for employee in advance_batch.employee_ids:
                    res['partner_id']=employee.address_home_id.id
                    adv_id=advance_obj.create(cr,uid,res)
                    advance=advance_obj.browse(cr,uid,adv_id)
                    contract_ids=obj_employee._get_correct_contract(cr,uid,employee.id,advance_batch.date)

                    if contract_ids == False:
                        raise osv.except_osv("Error", "El empleado %s no cuenta con un contrato vigente." % str(employee.name))

                    contract_id=contract.browse(cr,uid,contract_ids)
                    amount_per=contract_id.wage*advance_batch.percentage/100
                    if not product.property_account_income:
                        raise osv.except_osv("Error", "La cuenta de ingresos no esta configurada en el producto: " + str(product.name))
                    res_line={
                              'product_id':product_id,
                              'price_unit':amount_per,
                              'price_subtotal':amount_per,
                              'name':product.name,
                              'account_id':product.property_account_income.id,
                              'partner_id':employee.address_home_id.id,
                              'invoice_id':advance.invoice_id.id,
                              }
                    advance_id=account_line_obj.create(cr,uid,res_line)
                    wf_service.trg_validate(uid, 'hr.advance', adv_id, 'invoice_open', cr)                    
        ir_model_data = self.pool.get('ir.model.data')
        tree_res = ir_model_data.get_object_reference(cr, uid, 'hr_advance_payment', 'hr_advance_tree')
        tree_id = tree_res and tree_res[1] or False
        
        return {
           'name': _('Advances'),
           'view_type': 'form',
           'view_mode': 'tree,form',
           'res_model': 'hr.advance',
           'res_id': adv_id,
           'view_id': False,
           'views': [(tree_id, 'tree')],
           'type': 'ir.actions.act_window',
       }

hr_advances_employees()
