from datetime import datetime
import math

MESES = ('Enero ', 'Febrero ', 'Marzo ', 'Abril ', 'Mayo ', 'Junio ', 'Julio ', 'Agosto ', 'Septiembre ', 'Octubre ', 'Noviembre ', 'Diciembre '  )



def date_to_words_es(j):

    date = datetime.strptime(j,"%Y-%m-%d")
    mes_c=MESES[int((date.month)-1)]
    date_to_word=str(date.day)+" de "+ mes_c + " del " + str(date.year)
    return date_to_word
