from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta

from openerp.osv import fields, osv, orm
from operator import itemgetter
from amount_to_words import amount_to_words_es
from date_to_words import date_to_words_es

class sale_order(osv.osv):
    _inherit = "sale.order"

    def get_amount_in_word(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)[0]
        result = {}
        amount_in_words = amount_to_words_es(records.amount_total)
        result[records.id] = amount_in_words
        return result

    _columns={
        'client_contact':fields.many2one('res.partner',string="Contacto"),
        'total_en_letras':fields.function(get_amount_in_word, string='Total en Letras', method=True, type='char'),
        }

class hr_employee(osv.osv):
    _inherit = "hr.employee"

    _columns={

        "proyecto":fields.char("Proyecto"),
        "notificacion_de_desahucio":fields.date("Notificacion de desahucio"),
        "inicio_desahucio":fields.date("inicio tramite desahucio"),

        }

class hr_contract(osv.osv):
    _inherit = "hr.contract"


    def get_wage_in_word(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)[0]
        result = {}
        wage_in_words = amount_to_words_es(records.wage)
        result[records.id] = wage_in_words
        return result

    def get_date_in_word(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)[0]
        result = {}
        date_in_words = date_to_words_es(records.date_start)
        result[records.id] = date_in_words
        return result

    def get_alert(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}
        today = datetime.today()
        for r in records:
            if not r.trial_date_end:
                result[r.id] = False
            else:
                fecha_end_pruebas = datetime.strptime(r.trial_date_end,"%Y-%m-%d")
                margin = timedelta(days=7)
                one_week_prior = fecha_end_pruebas - margin
                one_week_after = fecha_end_pruebas

                if (today >= one_week_prior) and (today <= one_week_after):
                    result[r.id] = True
                else:
                    result[r.id] = False
        return result

    def on_change_dates(self, cr, uid, ids, date, context=None):
        fecha_inicial=datetime.strptime(date,"%Y-%m-%d")
        start_prueba=fecha_inicial
        fin_prueba=fecha_inicial + relativedelta( days= +90)
        fin_contrato=fecha_inicial + relativedelta( months= +12)
        inicio_desahucio= fin_contrato + relativedelta ( days= -45)
        notifi_desahucio=fin_contrato + relativedelta ( days= -30)

        return {'value' : {'trial_date_end':fin_prueba.strftime('%Y-%m-%d') , 'trial_date_start':start_prueba.strftime('%Y-%m-%d'),'date_start':fecha_inicial.strftime('%Y-%m-%d'),'date_end':fin_contrato.strftime('%Y-%m-%d') , 'notificacion_de_desahucio': notifi_desahucio.strftime('%Y-%m-%d'), 'inicio_desahucio': inicio_desahucio.strftime('%Y-%m-%d'), },}

    _columns={
        "notificacion_de_desahucio":fields.date(string="Notificacion de desahucio"),
        "inicio_desahucio":fields.date(string="Inicio tramite desahucio"),
        "state":fields.selection([('registrado', 'Registrado'), ('firmado', 'Firmado'), ('legalizado', "Legalizado")], string="Estado"),
        "wage_in_word":fields.function(get_wage_in_word, string='Salario en Letras', method=True, type='char'),
        "date_in_word":fields.function(get_date_in_word, string="Fecha en Letras", type='char'),
        "alert":fields.function(get_alert, string="alert", method=True, type='boolean'),
        }


class hr_payslip(osv.osv):

    _inherit= 'hr.payslip'


    def get_inputs(self, cr, uid, contract_ids, date_from, date_to, context=None):
        res = super(hr_payslip, self).get_inputs(cr, uid, contract_ids, date_from, date_to, context=context)
        prueba=map(itemgetter('code'), res).index('DIAS_TRABAJADOS')
        res[prueba].update({
            'amount': 15.0,
            'name': 'Numero de d\xc3\xadas calendario trabajados (1-15) [d\xc3\xadas]',
            })
        return  res

    _columns={

    }



