{
    'name': 'FacturaDeUna Support',
    'author': 'Trescloud S.A.',
    'category': 'Support',
    "version": "1.0",
    "website": "http://www.trescloud.com/",
    "description": """Provide support links for Factura De Una""",
    "depends": ["mail", "web"],
    "data": [],
    "demo_xml": [],
    "init_xml": [],
    "update_xml": ['menu.xml'],
    "installable": True,
    'js': ['static/src/js/facturadeuna_support.js'],
    'css': ['static/src/css/facturadeuna_support.css'],
    'qweb': ['static/src/qweb/facturadeuna_support.xml'],
}

