{
    'name': 'Cross Company',
    'version': '1.0',
    'description': """
        A module that stores the OpenERP login information of multiple OpenERP systems.
    """,
    'author': 'Dan Haggerty',
    'website': 'www.altatececuador.com',
    "depends" : [],
    "data" : [ 'cross_company.xml' ],
    "installable": True,
    "auto_install": False
}