# -*- coding: utf-8 -*-
#################################################################################
#
# Este archivo tiene el modelo Cross-Company. El objeto cross_company contiene
# datos de los otros systemas OpenERP de un grupo de compañías (nombre de compañía,
# URL, usuario, contraseña, nombre de base de datos).
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    11/12/2014
#
#################################################################################
from openerp.osv import fields, osv, orm
import xmlrpclib
import socket

#####################################################################################
# Definición de la clase cross_company
#####################################################################################
class cross_company(orm.Model):

    _name = 'cross.company'

    def write(self, cr, uid, ids, vals, context={}):
        if 'name' in vals:
            if len(self.search(cr, uid, [('name', '=', vals['name'])], context=context)):
                raise osv.except_osv("Error", "Nombre debe ser unico")

        if 'is_this_server' in vals and vals['is_this_server']:
            if len(self.search(cr, uid, [('is_this_server', '=', True)], context=context)):
                raise osv.except_osv("Error", "Ya existe un servidor marcado como 'Es este servidor'")

        return super(cross_company, self).write(cr, uid, ids, vals, context=context)

    def create(self, cr, uid, vals, context={}):
        if 'name' in vals:
            if len(self.search(cr, uid, [('name', '=', vals['name'])], context=context)):
                raise osv.except_osv("Error", "Nombre debe ser unico")

        if 'is_this_server' in vals and vals['is_this_server']:
            if len(self.search(cr, uid, [('is_this_server', '=', True)], context=context)):
                raise osv.except_osv("Error", "Ya existe un servidor marcado como 'Es este servidor'")

        return super(cross_company, self).create(cr, uid, vals, context=context)

    #####################################################################################
    # Esta función devuelve el estado de la connecion a un sistema
    #####################################################################################
    def _get_connection_state( self, cr, uid, ids, field_name, arg, context ):

        res = {}

        for company in self.browse( cr, uid, ids, context=context ):


            # Cambiar la URL si necesitamos
            correct_url = ""

            if( company.url[:7] != "http://" ):
                correct_url += "http://"

            if( company.url[:4] == "www." ):
                correct_url += company.url[4:]

            elif( company.url[:11] == "http://www." ):
                correct_url += "http://" + company.url[11:]

            else:
                correct_url += company.url

            try:
                socket.setdefaulttimeout(5)

                sock_common = xmlrpclib.ServerProxy( correct_url + ':' + str(company.port) + '/xmlrpc/common' )
                userid      = sock_common.login( company.dbname, company.username, company.password )
                sock        = xmlrpclib.ServerProxy( correct_url + ':' + str(company.port) + '/xmlrpc/object' )

                res[ company.id ] = 'connected'

            except Exception:
                res[ company.id ] = 'not_connected'

            # Guardar el estado de la conneción para mostrar en la vista tree
            write_dict = { 'last_state' : res[ company.id ] }

            # Guardar la URL si necesitamos
            if( company.url != correct_url ):
                write_dict[ 'url' ] = correct_url

            self.write( cr, uid, [ company.id ], write_dict )

        return res


    state_list = [ ( 'not_connected', 'No Conectado' ), ( 'connected', 'Conectado' ) ]

    _columns = { 'name'       : fields.char( "Nombre", required=True ),
                 'dbname'     : fields.char( "Nombre de base de datos", required=True ),
                 'sysname'    : fields.char( "Nombre de sistema" ),
                 'username'   : fields.char( "Usuario", required=True ),
                 'password'   : fields.char( "Contrasena", required=True ),
                 'url'        : fields.char( "URL", required=True ),
                 'port'       : fields.integer( "Puerto", required=True ),
                 'is_this_server' : fields.boolean("Es este servidor?"),
                 'last_state' : fields.selection( [('not_connected','No Conectado'),('connected','Conectado')], string="Estado de conexion" ),
                 'state'      : fields.function( fnct      = _get_connection_state,
                                                 type      = "selection",
                                                 selection = state_list,
                                                 method    = True,
                                                 string    = "Estado de conexion"
                                               )
               }

    _defaults = { 'state'      : 'not_connected',
                  'last_state' : 'not_connected',
                  'port'       : 8080,
                  'is_this_server': False,
                }