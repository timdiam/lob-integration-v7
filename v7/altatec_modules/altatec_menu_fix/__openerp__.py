{ 'name'         : 'AltaTec Menu Fix',
  'version'      : '1.0',
  'description'  : """
                   This module is used to fix issues where you've installed a menu, but cannot get it to appear at all.
                   Just install this module and then uninstall it.
                   """,
  'author'       : 'Dan Haggerty',
  'website'      : 'www.altatec.ec',
  "depends"      : [ 'base',
                   ],
  "data"         : [
                   ],
  "installable"  : True,
  "auto_install" : False
}
