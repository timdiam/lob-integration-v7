# -*- coding: utf-8 -*-

######################################################################
#
#  Note: Program metadata is available in /__init__.py
#
######################################################################

from openerp.osv import fields, osv

class ir_ui_menu(osv.osv):

    _inherit = 'ir.ui.menu'

    def init(self, cr):
        self._parent_store_compute(cr)