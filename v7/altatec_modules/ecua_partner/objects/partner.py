
from operator import itemgetter
import time

from openerp.osv import fields, osv

class account_fiscal_position(osv.osv):
    _inherit = "account.fiscal.position"

    _order = "name"