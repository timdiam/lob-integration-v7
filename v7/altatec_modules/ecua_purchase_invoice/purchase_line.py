# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 TRESCloud (<http://www.trescloud.com>)
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from osv import fields, osv
import ast
import time
from openerp.tools.translate import _

class purchase_order(osv.osv):
     _inherit = "purchase.order"
     
     def action_invoice_create(self, cr, uid, ids, context=None):
        """Agregamos datos para facturacion basada en
           - Basado en borrador de factura generado -
        """
        if context is None:
            context = {}

        inv_id = super(purchase_order, self).action_invoice_create(cr, uid, ids,context)
        
        '''write extra information about invoice'''
        if inv_id:
            inv_obj = self.pool.get('account.invoice')
            inv = inv_obj.browse(cr, uid, inv_id, context)
            invoice_header_for_ecuador = inv_obj._prepare_invoice_header(cr, uid, inv.partner_id.id, inv.type, inv_date=inv.date_invoice, context=context)
            inv_obj.write(cr,uid,inv_id,invoice_header_for_ecuador)

        return inv_id
    
purchase_order()

class purchase_line_invoice(osv.osv_memory):

    _inherit = 'purchase.order.line_invoice'

    def makeInvoices(self, cr, uid, ids, context=None):
        '''Cargamos datos para facturacion basada en
           - Basado en líneas de pedidos de compra -
        '''
        
        var={}
        ctx={}
        invoice_header_for_ecuador={} 
        res = super(purchase_line_invoice, self).makeInvoices(cr, uid, ids,context)
        invoice_obj=self.pool.get('account.invoice')
        var= ast.literal_eval(res['domain'])
        ctx=ast.literal_eval(res['context'])
        
        '''write extra information about invoice'''
        record_id =  context.get('active_id',[])
        if record_id:
            order_line_obj = self.pool.get('purchase.order.line')
            order_line = order_line_obj.browse(cr, uid, record_id, context)
            partner_id = order_line.partner_id.id
            inv_obj = self.pool.get('account.invoice')
            invoice_header_for_ecuador = inv_obj._prepare_invoice_header(cr, uid, partner_id, ctx['type'], context=context)
            var2= var[0][2] or False
            if var2!=False:
                invoice_obj.write(cr,uid,var2,invoice_header_for_ecuador)
            return res
    
purchase_line_invoice()