from openerp.osv import osv
from openerp import pooler


class account_withhold(osv.osv):
    _inherit = 'account.withhold'

    def init(self, cr):
        pool = pooler.get_pool(cr.dbname)
        uid = 1

        withhold_id_list = pool.get('account.withhold').search(cr, uid, [('state', '=', 'canceled')]) #obtiene todas las retenciones canceladas existentes

        for withhold in pool.get('account.withhold').browse(cr, uid, withhold_id_list):
            #actualizamos el transaction_type en cada una de las retenciones canceladas
            self.write(cr, uid, [withhold.id], {'transaction_type': 'canceled'})
