
{
    "name" : "Altatec Canceled Retentions Migration ",
    "version" : "1.0",
    "author"  : "Denisse Ochoa",
    "website" : "http://www.altatec.ec/",
    "category" : "Regulaciones Ecuatorianas",
    "depends"  : [
                 'base',
                 'ecua_tax_withhold',
                 'ecua_autorizaciones_sri'
                 ],
    "description" : """
    Este módulo realizará la migración de las retenciones anuladas en la sección "Retenciones en Compra" que se
    encuentran en estado "Cancelado" pero no se encuentran en la sección "Retenciones Canceladas (Anuladas)"
    """,

    "data": [

                   ],
    "installable": True,
    "auto_install": False,
    "application": False,
}
