# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors: Patricio Rangles                                                                         
#Copyright (C) 2013 TRESCLOUD Cia. Ltda.                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import fields,osv
from tools.translate import _

class account_voucher(osv.osv):

    _inherit = "account.voucher"
    _name = "account.voucher"

    _columns = {
            'refund_id': fields.many2one('sri.refund', 'Refund',
                                         help="Select the related Refund if it's necessary"),
                }
    
    def proforma_voucher(self, cr, uid, ids, context=None):
        """
        Al aprobar el account voucher asigna el respectivo id del reembolso
        If the voucher is validate this function asign the respective refund id
        in case this exist
        """
        super(account_voucher, self).proforma_voucher(cr, uid, ids, context)
        vo_mo_li_obj = self.pool.get('account.move.line')
        
        for voucher in self.browse(cr, uid, ids, context):
            if voucher.refund_id:
                move_line_list = []
                for move in voucher.move_ids: 
                    move_line_list.append(move.id)
                
                vo_mo_li_obj.write(cr, uid, move_line_list, {'refund_id': voucher.refund_id.id})
                
        return True

    
account_voucher()
