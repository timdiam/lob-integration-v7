# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors: Patricio Rangles                                                                         
#Copyright (C) 2013 TRESCLOUD Cia. Ltda.                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import fields,osv
from tools.translate import _
import time
import openerp.exceptions
from datetime import datetime
import pytz

class account_invoice(osv.osv):
    """
    This part determine if show the field for refund like intermediate 
    or refund like client follow this rules:
    
    If "sustento tributario" is "08" and "invoice_type" is "in_invoice", is refund intermediate 
    If "tipo de documento" is "41" and "invoice_type" is "out_invoice", is refund intermediate 
    If "tipo de documento" is "41" and "invoice_type" is "in_invoice", is refund client 
    
    """
    
    _inherit = "account.invoice"
    _name = "account.invoice"

    _columns = {
            'show_refund': fields.boolean('Show Refund', help="Select the refund that invoice is related"),
            'refund_id': fields.many2one('sri.refund', 'Refund'),
            'show_refund_client': fields.boolean('Show Refund Client'),
            'ats_data_line_ids': fields.one2many('sri.refund.client.ats.line', 'refund_invoice_id', 'ATS Data Line'),
            # Total to show
#            'total_base_vat_0_refund': fields.float('0% VAT base'),
#            'total_base_vat_no0_refund': fields.float('NO 0% VAT base'),
#            'total_vat_amount_no0_refund': fields.float('NO 0% VAT value'),
#            'total_no_vat_amount_refund': fields.float('NO 0% VAT base'),
#            'total_ice_amount_refund': fields.float('ICE Value'),
                }

    _defaults = {
            'show_refund' : False,
            'show_refund_client': False,
                 }
    def onchange_document_invoice_type_id(self, cr, uid, ids, type=False, date_invoice=False, partner_id=False, sri_tax_support_id=False, document_invoice_type_id=False, printer_id=False, authorizations_id=False, company_id=False, context=None):

        res = super(account_invoice, self).onchange_document_invoice_type_id(cr, uid, ids, type, date_invoice, partner_id, sri_tax_support_id, document_invoice_type_id, printer_id, authorizations_id, company_id, context)

        show_refund = False
        show_refund_client = False
        
        if document_invoice_type_id:
            
            document_invoice_type = self.pool.get('account.invoice.document.type').browse(cr, uid, document_invoice_type_id)
            
            if document_invoice_type.code == "41" and type == 'out_invoice':
                show_refund = True
            elif document_invoice_type.code == "41" and type == 'in_invoice':
                show_refund_client = True
            else:
                # bug que oculta el caso registrado cuando el tipo de documento es 08
                return res

            if not 'value' in res:
                res['value'] = {}
    
            res['value']['show_refund_client'] = show_refund_client
            res['value']['show_refund'] = show_refund
            
        return res

    
    def onchange_tax_support_id(self, cr, uid, ids, type=False, date_invoice=False, partner_id=False, tax_support_id=False, document_invoice_type_id=False, printer_id=False, authorizations_id=False, company_id=False, context=None):

        res = super(account_invoice, self).onchange_tax_support_id(cr, uid, ids, type, date_invoice, partner_id, tax_support_id, document_invoice_type_id, printer_id, authorizations_id, company_id, context)

        show_refund = False
        show_refund_client = False
                
        if tax_support_id:
            tax_support = self.pool.get('sri.tax.support').browse(cr, uid, tax_support_id);
            if tax_support.code == "08" and type == 'in_invoice':
                show_refund = True
        
        res['value']['show_refund_client'] = show_refund_client
        res['value']['show_refund'] = show_refund
        
        return res

    def invoice_validate(self, cr, uid, ids, context=None):
        """
        If the invoice is validate this function assign the respective refund id
        in case this exist
        """
        super(account_invoice, self).invoice_validate(cr, uid, ids, context)
        ac_mo_li_obj = self.pool.get('account.move.line')
        
        for invoice in self.browse(cr, uid, ids, context):
            
            #Check if the invoice of sale or purchase have the tax_support and the refund
            if (invoice.sri_tax_support_id.code == "08" and invoice.refund_id and invoice.type == "in_invoice") or (invoice.document_invoice_type_id.code == "41" and invoice.refund_id and invoice.type == "out_invoice"):
                #Search the account.move.line related with this invoice   
                move_line_list = ac_mo_li_obj.search(cr, uid, [('move_id','=',invoice.move_id.id)])
                ac_mo_li_obj.write(cr, uid, move_line_list, {'refund_id': invoice.refund_id.id})
                
        return True
    def unlink(self, cr, uid, ids, context=None, check=True):
        if context is None:
            context = {}
        result = False
        for invoice in self.browse(cr, uid, ids, context=context):
            if invoice.refund_id.state in ('generated', 'cancel'):
                raise openerp.exceptions.Warning(_('You cannot delete an invoice which refund is not in draft.'))    
        result = super(account_invoice, self).unlink(cr, uid, ids, context=context)
        return result
    
    def copy(self, cr, uid, id, default=None, context=None):
        default = default or {}
        default.update({
                        'refund_id':False,
                        })
        return super(account_invoice, self).copy(cr, uid, id, default, context)

    #TODO FIX WHY IS THIS FUNCTION NOT HAVE CONTEXT PARAMETER?!
    def action_date_assign(self, cr, uid, ids, *args):
        """
        Check the date of invoice versus the date in each ATS line, this can be less or
        equal than the invoice date. Also, check if have lines to data refund to process
        
        In the other hand, check if is refund client and if the invoice have withhold taxes, showing a
        warning if exist taxes withhold
        """
        
        for inv in self.browse(cr, uid, ids):
            
            if inv.document_invoice_type_id.code == "41" and inv.type == "in_invoice":
                # Verify if have at least one line of data refund and the date
                date_invoice = inv.date_invoice
                if not date_invoice:
                    #if empty field -> it's current date
                    #TODO FIX WHY IS THIS FUNCTION NOT HAVE CONTEXT PARAMETER!!!!!!
                    now_date = datetime.now(pytz.timezone("America/Guayaquil")).strftime("%Y-%m-%d")
                    #now_date = fields.date.context_today(self, cr, uid, context=context)
                    date_invoice = now_date

                if not inv.ats_data_line_ids:
                    raise osv.except_osv(_('Error'), 
                                         _(" The SRI Refund for Client need at least one refund data for ATS"))
                for line_ats in inv.ats_data_line_ids:
                    if line_ats.creation_date > date_invoice:
                        raise osv.except_osv(_('Warning!'), 
                                             _("La fecha en los datos de reembolso para el ATS del proveedor %s, Factura: %s, es mayor que la fecha de la factura de reembolso." %(line_ats.partner_id.name, line_ats.number)))
        
                ##Verify if this invoice have withholds.
                #for inv_line in inv.invoice_line:
                #    for tax_line in inv_line.invoice_line_tax_id:
                #        # always check the total_to_withhold because take the rest of cases except renta 0%
                #        if inv.total_to_withhold <> 0.0 or (tax_line.amount == 0.0 and tax_line.type_ec == 'renta'):
                #            raise osv.except_osv(_('Warning!'), 
                #                                 _("The invoice can't have withhold tax, please check Withhold VAT and remove"))

                #Verify if this invoice have a value in withholds.
                    if inv.total_to_withhold <> 0.0:
                        raise osv.except_osv(_('Warning!'), 
                                             _("The invoice can't have withhold tax, please check Withhold VAT and remove"))

        super(account_invoice, self).action_date_assign(cr, uid, ids, args)
                
        return True

    def action_view_refund(self, cr, uid, ids, context=None):
        '''
        This function returns an action that display the refund.
        '''
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        invoice = self.browse(cr, uid, ids[0])
        action = ""
        if invoice.type == 'in_invoice':
            action = "action_invoice_tree1"
        else:
            action = "action_invoice_tree2" 
        result = mod_obj.get_object_reference(cr, uid, 'account', action)
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]
        #compute the number of invoices to display
        inv_ids = []
    
        for so in self.browse(cr, uid, ids, context=context):
            if so.refund_id: 
                 if invoice.type == 'in_invoice':
                     inv_ids += [invoice.id for invoice in so.refund_id.created_invoice_refund_ids]
                 else:
                     inv_ids += [invoice.id for invoice in so.refund_id.invoice_refund_ids]             
        #choose the view_mode accordingly
        if len(inv_ids)>1:
            result['domain'] = "[('id','in',["+','.join(map(str, inv_ids))+"])]"
        else:
            res = mod_obj.get_object_reference(cr, uid, 'account', 'invoice_form')
            result['views'] = [(res and res[1] or False, 'form')]
            result['res_id'] = inv_ids and inv_ids[0] or False    
        return result

account_invoice()

