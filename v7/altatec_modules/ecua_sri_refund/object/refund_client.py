# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors: Patricio Rangles                                                                         
#Copyright (C) 2013 TRESCLOUD Cia. Ltda.                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import fields, osv
from tools.translate import _
import time
import netsvc
import re
import openerp.exceptions
from datetime import datetime
import pytz

class sri_refund_client_ats_line(osv.osv):

    _name = "sri.refund.client.ats.line"
    _inherit = ['mail.thread']

    def onchange_generated_date(self, cr, uid, ids, creation_date, refund_invoice_id, context=None):
        """
        Check the date vs. invoice date, this date must be less or iqual than invoice date
        """
        if not context:
            context = {}

        warning = {}
        
        date_invoice = False
        
        if refund_invoice_id:
            # if exist, use this to check the date
            invoice = self.pool.get("account.invoice").browse(cr, uid, refund_invoice_id)
            date_invoice = invoice.date_invoice
        elif 'date_invoice' in context:
            #the invoice is not save, try with context
            date_invoice = context['date_invoice']
        else:
            #No se encontro un dato de fecha, no se compara:
            return {}
            
        if not date_invoice:
            #campo vacio: indica fecha actual
            #now_date = datetime.now(pytz.timezone("America/Guayaquil")).strftime("%Y-%m-%d")
            now_date = fields.date.context_today(self, cr,uid,context=context)
            date_invoice = now_date
        
        if creation_date > date_invoice:
            warning = {
                    'title': _('Warning!!!'),
                    'message': _("The set date is not valid, this have to be less or iqual than the invoice date")
                       }

        return {'warning': warning }

    def onchange_total(self,cr,uid,ids,base_vat_0, base_vat_no0,vat_amount_no0,no_vat_amount,ice_amount,calc_vat=False):
        """
        Calculate the total and the vat value depending the field using
        calc_vat = True -> show the value of vat using the base
        """
        res={
             'value':{}
             }

        if calc_vat:
            vat_amount_no0 = base_vat_no0 * 12.0 / 100.0
            res['value']['vat_amount_no0'] = vat_amount_no0

        total = base_vat_0 + base_vat_no0 + vat_amount_no0 + no_vat_amount + ice_amount
        res['value']['total'] = total
        return res
    
    def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
        """
        When the partner changed need recalculate the transaction type.
        """
        
        code = ''
        res={'value':{}}
        res['value']['document_invoice_type_id'] = False
        res['value']['authorizations_id'] = False
        res['value']['number'] = False
        
        if not partner_id:
            return res
        
        partner_brow = self.pool.get('res.partner').browse(cr, uid, partner_id)
      
        if partner_brow and partner_brow.active:
            code = partner_brow.code
        else:
            raise orm.except_orm(_('Configuration Error!'),_('The select partner is inactive')) 

        if code == 'R': # Ruc
            res['value']['transaction_type'] = '01'
        elif code == 'C': # Cedula
            res['value']['transaction_type'] = '02'
        elif code == 'P': # Pasaporte / Identificación tributaria del exterior
            res['value']['transaction_type'] = '03' 
        else:
            res['value']['transaction_type'] = _("Partner has assigned a incorrect identification")     

        return res

    def onchange_document_invoice_type_id(self, cr, uid, ids, document_invoice_type_id, partner_id, date, company_id=None, context=None):
        """
        When the document change, show the first authorization available
        """
        
        auth_obj = self.pool.get('sri.authorizations')
        res = auth_obj.authorization_onchange_helper(cr,uid,document_invoice_type_id, partner_id, date, None, None, company_id)
                    
        return res

    def onchange_authorizations_id(self, cr, uid, ids, authorizations_id, context=None):
        """
        When the authorization change, show the number for the document
        """
        
        number=""
        res={'value':{}}
        res['value']['number'] = number
        if not authorizations_id:
            return res 

        auth_brow = self.pool.get('sri.authorizations').browse(cr, uid, authorizations_id)
        
        if auth_brow.shop:
            number = auth_brow.shop + "-"
        if auth_brow.printer_point:
            number = number + auth_brow.printer_point + "-" 
        
        res['value']['number'] = number    

        return res
    
    def onchange_number(self, cr, uid, ids, number, context=None):
        invoice_obj = self.pool.get('account.invoice')
        res1={'value':{}}
        res = invoice_obj.onchange_internal_number(cr, uid, ids, number, context)
        if res['value']:
            res1['value']['number']=res['value']['internal_number']
        return res1     
        
    def _assign_code_transaction_type(self, cr, uid, ids, field, arg, context=None):
        
        ''' 
        Función que asigna un código tipo de transacción al partner de acuerdo a su identificación 
        (cedula, ruc, pasaporte, etc) para compras en cada linea de datos para el ATS
        '''        
        
        res = {}
        
        for ats_line in self.browse(cr, uid, ids, context):
            code = ''
            if ats_line and ats_line.partner_id and ats_line.partner_id.active:
                code = ats_line.partner_id.code
            else:
                raise orm.except_orm(_('Configuration Error!'),_('Partner inactive in the document number ') + str(ats_line.number or "N/D")) 

            if code == 'R': # Ruc
                res[ats_line.id] = '01'
            elif code == 'C': # Cedula
                res[ats_line.id] = '02'
            elif code == 'P': # Pasaporte / Identificación tributaria del exterior
                res[ats_line.id] = '03' 
            else:
                res[ats_line.id] = _("Partner has assigned a incorrect identification. Document: ") + str(ats_line.number or "N/D")    

        return res

    _columns = {
            'partner_id': fields.many2one('res.partner', 'Partner', help="Select the partner asociated to the invoices"),
            'document_invoice_type_id': fields.many2one('account.invoice.document.type', 'Document Type', help="Select the type of document for this line"),
            'authorizations_id': fields.many2one('sri.authorizations', 'Authorization SRI', help="Select the authorization for this document "),
            'number': fields.char('Number of document', size=17),
            'creation_date': fields.date('Generated Date', help="Set the date of document generation"),
            'base_vat_0': fields.float('0% VAT base'),
            'base_vat_no0': fields.float('NO 0% VAT base'),
            'vat_amount_no0': fields.float('NO 0% VAT value'),
            'no_vat_amount': fields.float('NO VAT amount'),
            'ice_amount': fields.float('ICE Value'),
            'refund_invoice_id': fields.many2one('account.invoice', 'Refund Invoice'),
            'transaction_type': fields.function(_assign_code_transaction_type, string='Transaction type', type='char', store=False, method=True, help='Indicate the transaction type that performer the partner.', readonly=True),
            'total':fields.float('Total'), 
                }

    _defaults = {
            'refund_invoice_id': lambda self, cr, uid, context : 'active_id' in context and context['active_id'] or False,
                 }

    def create(self, cr, uid, values, context=None):
        number=values.get('number') or False
        document_invoice_type=values.get('document_invoice_type_id') or False
        document_object=self.pool.get('account.invoice.document.type')
        if document_invoice_type:
            document_invoice_type_id=document_object.browse(cr,uid,document_invoice_type)
        if number:
            if document_invoice_type_id.number_format_validation:
                cadena='(\d{3})+\-(\d{3})+\-(\d{9})'
                validate_document_type = document_invoice_type_id.number_format_validation
                if validate_document_type==True:
                    ref = number
                    if not re.match(cadena, ref):
                        raise osv.except_osv(_('Invalid action!'), _('The number of the refund Invoice is incorrect, it must be like 00X-00X-000XXXXXX, X is a number'))
        return super(sri_refund_client_ats_line, self).create(cr, uid, values, context=context)  
      
    def write(self, cr, uid, ids, values, context=None):
        if context is None:
            context = {}
        if not ids:
            return True
        number=values.get('number') or False
        document_invoice_type=values.get('document_invoice_type_id') or False
        document_object=self.pool.get('account.invoice.document.type')
        refund_client=self.browse(cr,uid,ids)[0]
        if not document_invoice_type:
            document_invoice_type_id=refund_client.document_invoice_type_id
        else:
            document_invoice_type_id=document_object.browse(cr,uid,document_invoice_type)
        if not number:
            number=refund_client.number
        if number and document_invoice_type_id:
            if document_invoice_type_id.number_format_validation:
                cadena='(\d{3})+\-(\d{3})+\-(\d{9})'
                validate_document_type = document_invoice_type_id.number_format_validation
                if validate_document_type==True:
                    ref = number
                    if not re.match(cadena, ref):
                        raise osv.except_osv(_('Invalid action!'), _('The number of the refund Invoice is incorrect, it must be like 00X-00X-000XXXXXX, X is a number'))
        return super(sri_refund_client_ats_line, self).write(cr, uid, ids, values, context=context)

sri_refund_client_ats_line()
