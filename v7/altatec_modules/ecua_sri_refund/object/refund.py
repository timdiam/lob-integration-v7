# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors: Patricio Rangles                                                                         
#Copyright (C) 2013 TRESCLOUD Cia. Ltda.                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import fields,osv
from tools.translate import _
import re
import decimal_precision as dp
import time
import netsvc
import openerp.exceptions



class sri_refund_partner_cost_division(osv.osv):
    
    _name = "sri.refund.partner.cost.division"
    
    _columns = {
            'refund_id': fields.many2one('sri.refund', 'Refund'),
            'partner_id': fields.many2one('res.partner', 'Partner' , help="Select the partner to use to generate the refund. If many partners is required set a percentage."),
            'percentage': fields.float('Percentage', digit=(5,2), help="Set the percentage to use in this partner. This is valid when the cost is divided for many of them."),
                }

sri_refund_partner_cost_division()

class sri_refund(osv.osv):
    
    _name = "sri.refund"

    _columns = {
            'name': fields.char('Name', size=64, help="Use to identify the refund, can be auto numeric"),
           # 'number': fields.char('Number', size=64, help="Use to identify the refund, can be auto numeric"),
            'date': fields.date('Date', help="Indicate the date of refund, this date is used in the invoices"),
            'partner_id': fields.many2one('res.partner', 'Partner', help="Select the partner to generate the refund invoices"),
            'partner_ids': fields.one2many('sri.refund.partner.cost.division', 'refund_id', 'Partners and Percentages'),
            'cost_division': fields.boolean('Cost Division?', help="Select if required generate refund invoice dividing cost between many partners"),
            'lines_by_invoice': fields.integer('Lines Number', help="Default number of lines in each invoice generate"),
            'account_id': fields.many2one('account.account', 'Refund Account', domain=[('type','!=','view')], help="It is used to replace the account in every line product in the refund invoice(s)"),
            'invoice_refund_ids': fields.one2many('account.invoice', 'refund_id', 'Invoices', domain=[('type','=','in_invoice'),('state','in',('open','paid'))]),
            'voucher_refund_ids': fields.one2many('account.voucher', 'refund_id', 'Purchase Receipt', domain=[('state','in',('posted',))]), #domain=[('type','=','payment'),('state','in',('posted',))]
            'state': fields.selection([('draft','Draft'),
                                       ('generated','Generated'),
                                       ('cancel','Cancelled'),],'State', select=True, readonly=True, track_visibility='onchange'),
            'created_invoice_refund_ids': fields.one2many('account.invoice', 'refund_id', 'Created Invoices', domain=[('type','=','out_invoice'),('state','in',('draft','open','paid'))]),
            'keep_out_invoice': fields.boolean('Keep Out Invoice', help="Help the system to check if the out invoice don't be delete when the refund is cancelled, when already exist out invoice asociated to the refund before automatically create"),
            'number':fields.char('Number',size=64,help="Number of sequence"),    
                }

    _defaults = {
            'state': 'draft',
            'cost_division': False,
            'lines_by_invoice': 10,
            'account_id': lambda self,cr,uid,c: self.pool.get('res.company').browse(cr, uid, 1).account_id and self.pool.get('res.company').browse(cr, uid, 1).account_id.id or False,
            'number': lambda obj, cr, uid, context: '/',
                 }

    def create_refund(self, cr, uid, ids, context=None):
        """
        Create the invoice(s) using the information of refund
        """
        
        
        def create_invoice_line(line, account_id, type, tax_mapping_brow):
            """
            Function that Create a invoice line using especific values
            return a dictionary with the fields and values
            
            Also, modify the description, account and taxes (TODO taxes)  
            """
            
            def mapping_tax(invoice_line_tax, tax_mapping_brow):
                """
                Accept a list of tax_id of purchase and mapping to the related tax_id in sale
                depending the configuration in Refund section in Account
                """
                list_mapping_tax = []
                
                for tax_id in invoice_line_tax:
                    if tax_mapping_brow.iva_0_purchase_refund_id and tax_id.id == tax_mapping_brow.iva_0_purchase_refund_id.id:
                        if tax_mapping_brow.iva_0_sale_refund_id:
                            list_mapping_tax.append(tax_mapping_brow.iva_0_sale_refund_id.id)
                    elif tax_mapping_brow.iva_12_purchase_refund_id and tax_id.id == tax_mapping_brow.iva_12_purchase_refund_id.id: 
                        if tax_mapping_brow.iva_12_sale_refund_id:
                            list_mapping_tax.append(tax_mapping_brow.iva_12_sale_refund_id.id)
                        
                return [(6, 0, list_mapping_tax)]

            #            
            #INICIO
            #
            if type == 'account.invoice':

                # Control of taxes and maped purchase-sale
                taxes = False
                if tax_mapping_brow:
                    taxes = mapping_tax(line.invoice_line_tax_id, tax_mapping_brow)

                #The description is in SPANISH
                ruc = ""

                neto = invoice.total_with_vat - invoice.total_iva
                #iva_0 = invoice.amount_untaxed - invoice.total_with_vat 
                #iva_12 = invoice.total_iva

                if neto < 0.0:
                    neto = 0.0 
                #if iva_0 < 0.0:
                #    iva_0 = 0.0 
                #if iva_12 < 0.0:
                #    iva_12 = 0.0 

                if line.invoice_id.partner_id.vat:
                    ruc = line.invoice_id.partner_id.vat[2:]
                    
                description = "REEMBOLSO 100% DE GASTO RUC: {0}; FACTURA No. {1}; NETO: {2}, IVA 0%: {3}, IVA 12%: {4}"
                description = description.format(ruc, 
                                                 line.invoice_id.internal_number, 
                                                 str(neto), 
                                                 str(invoice.base_cero_iva), 
                                                 str(invoice.base_doce_iva))

                return self.description_invoice_line(cr, uid, ids, account_id, description, line, taxes)
            if type == 'account.voucher':
                
                description = "GASTOS GENERALES"
                
                return {                    
                    'name': description,#line.name,
                    'origin': False, 
                    'account_id': account_id or line.account_id.id or False,
                    'price_unit': line.amount,
                    'quantity': 1,
                    'discount': 0,
                    'uos_id': False,
                    'product_id': False,
                    'invoice_line_tax_id': False, # posible reemplazo de los impuestos
                    #'comment': line.comment,
                    'account_analytic_id': line.account_analytic_id.id or False,
                    }


        def create_new_invoice(company_id, currency_id, journal_id, 
                               percentage, partner_id, invoice_lines, 
                               actual_invoice, number_lines, refund_id,origin_number=False):
            """
            Function that Create a new invoice with the respective lines
            and the percentage
            #TODO: Hacerlo DRY utilizando la funcion _prepare_invoice_header
            """

            partner_obj = self.pool.get('res.partner')
            inv_obj = self.pool.get('account.invoice')
            doc_inv_type_obj = self.pool.get('account.invoice.document.type')
            
            user = self.pool.get('res.users').browse(cr, uid, uid)
            
            type_document=user.company_id.type_document_id.id
            
            partner = partner_obj.browse(cr, uid, partner_id, context) 
            account = partner.property_account_receivable.id
            res = partner_obj.address_get(cr, uid, [partner_id], ['contact', 'invoice'])
            if type_document:
                document_invoice_type_list = [type_document,]
            else:
                document_invoice_type_list = doc_inv_type_obj.search(cr, uid, [('code','=','41')])
            contact_addr_id = res['contact']
            invoice_addr_id = res['invoice']

            # Using this help:
            # https://doc.openerp.com/v6.0/developer/2_5_Objects_Fields_Methods/methods.html/#osv.osv.osv.write
            lines = []
            
            for i in range((actual_invoice -1) *  number_lines, actual_invoice *  number_lines):
                
                if i < len(invoice_lines):
                    
                    line = invoice_lines[i].copy()
                    fact = percentage / 100.00
                    
                    if percentage != 100 and 'quantity' in line:
                        line['quantity'] = fact * line['quantity']
                        # Add the replace of the percentage of "REEMBOLSO 100%" to "REEMBOLSO ##%"
                        # depending the value of each client
                        description = line['name']

                        if description:
                            new = "REEMBOLSO " + "{:4.0f}".format(percentage).replace(" ","") + "%"
                            description = description.replace("REEMBOLSO 100%", new, 1)
                            line['name'] = description
                        
                    lines.append((0, 0, line))
                    
                else:
                    break
                
            new_id = inv_obj.create(cr, uid, {
                            'name': '',
                            'origin': origin_number,
                            'type': 'out_invoice',
                            'document_invoice_type_id': document_invoice_type_list[0], # id of document type
                            'show_refund': True, # Required to 
                            'refund_id': refund_id,
                            'reference': '',
                            'account_id': account,
                            'partner_id': partner.id,
                            'journal_id': journal_id,
                            'address_invoice_id': invoice_addr_id,
                            'address_contact_id': contact_addr_id,
                            'currency_id': currency_id,
                            'comment': "",
                            'payment_term': False,
                            'cost_division':True,
                            #'flag':False,
                            #'total_con_impuestos': 0.0,
                            'fiscal_position': partner.property_account_position.id,
                            'date_invoice': context.get('date_invoice',False),
                            'company_id': company_id,
                            'user_id': False,#order.user_id and order.user_id.id or
                            'invoice_address': partner.street or False,
                            'invoice_phone': partner.phone or False,
                            'invoice_line': lines 
                                            })
            date_invoice_o=context.get('date_invoice',False)
            # Values to be set after the invoice is created, this is for SRI authorizations
            printer_id = inv_obj._default_printer_point(cr, uid, uid)
            authorization_id = False
            internal_number = False

            #TODO: Hacerlo DRY utilizando la funcion _prepare_invoice_header
            if printer_id:
                res = inv_obj.onchange_data(cr, uid, date_invoice_o,new_id, partner_id, document_invoice_type_list[0], company_id=company_id, printer_id=printer_id)
                if 'value' in res and 'authorization_id' in res['value']:
                    authorization_id = res['value']['authorization_id']
                if 'value' in res and 'internal_number' in res['value']:
                    internal_number = res['value']['internal_number']
            
            inv_obj.write(cr, uid, new_id, {'printer_id':printer_id,
                                            'authorization_id': authorization_id,
                                            'internal_number': internal_number})        
            
            return new_id

        """
        Principal function, create the invoice checking the number of lines, cost division, account, etc.
        """
        if not context:
            context = {}
                
        invoice_lines = []
        new_invoice_list = []
        invoice_number = False

        journal_obj = self.pool.get('account.journal')
        inv_obj = self.pool.get('account.invoice')
        obj_invoice_line = self.pool.get('account.invoice.line')
        company_obj = self.pool.get('res.company')
        refund_obj = self.pool.get('sri.refund')
        period_obj = self.pool.get('account.period')
        #Config don't work with many2one and one2many
        #tax_mapping_obj = self.pool.get('account.config.settings')

        refund = self.browse(cr, uid, ids[0], context)

        # Allow "create" refund if out invoice is already asociated
        # Mark "created" and "keep_out_invoice" True to prevent delete the out invoice
        if refund.created_invoice_refund_ids:
            self.pool.get('sri.refund').write(cr, uid, refund.id, {'state':'generated','keep_out_invoice':True}, context)
            return True
                
        line_number = refund.lines_by_invoice
        list_invoice = refund.invoice_refund_ids
        list_voucher = refund.voucher_refund_ids
        account_id = refund.account_id.id
        cost_division = refund.cost_division
        date_invoice = refund.date
        origin_number=refund.number
        context['date_invoice'] = date_invoice
        
        # TODO: De donde saco la compañia?
        company_id = 1
        
        company = company_obj.browse(cr, uid, company_id, context)
        currency_id = company.currency_id.id
        journal_ids = journal_obj.search(cr, uid, [('type', '=', 'sale'), ('company_id', '=', company_id)], limit=1)
        #Config don't work with many2one and one2many
        #tax_mapping_list = tax_mapping_obj.search(cr, uid, [('company_id','=',company_id)])
        tax_mapping_brow = company
        
        #Config don't work with many2one and one2many
        #if tax_mapping_list:
        #    tax_mapping_brow = self.pool.get('account.config.settings').browse(cr, uid, tax_mapping_list[0])
        
        if not journal_ids:
            raise osv.except_osv(_('Error !'),
                                 _('There is no sales journal defined for this company: "%s" (id:%d)') % (order.company_id.name, order.company_id.id))
        
        period_ids = period_obj.find(cr, uid, refund.date, context)
        for period in period_obj.browse(cr,uid,period_ids):
            if period.state=='done':
                raise osv.except_osv(_('Error!'), _('You can not add/modify entries in a closed period %s.' % (period.name)))
        for invoice in list_invoice:
            for line_inv in invoice.invoice_line:
                invoice_lines.append(create_invoice_line(line_inv, account_id, 'account.invoice', tax_mapping_brow))  

        for voucher in list_voucher:
            for line_vou in voucher.line_dr_ids:
                invoice_lines.append(create_invoice_line(line_vou, account_id, 'account.voucher', tax_mapping_brow))  

        invoice_number = len(invoice_lines) // line_number  
        
        if (len(invoice_lines) % line_number) > 0:
            invoice_number = invoice_number + 1 
        
        for i in range(1, invoice_number + 1):
            # Must control if the user use cost division
            if cost_division:
                
                for partner in refund.partner_ids:
                    new_invoice_list.append(create_new_invoice(company_id, currency_id, journal_ids[0], 
                                                               partner.percentage, partner.partner_id.id, invoice_lines, 
                                                               i, line_number, refund.id,origin_number))
                    
            else:
                new_invoice_list.append(create_new_invoice(company_id, currency_id, journal_ids[0], 
                                                           100 ,refund.partner_id.id, invoice_lines, 
                                                           i, line_number, refund.id,origin_number))

        #refund_obj.write(cr, uid, ids[0], {'created_invoice_refund_ids': [(0, 0, new_invoice_list)]}, context)
        
        self.pool.get('sri.refund').write(cr, uid, refund.id, {'state':'generated'}, context)
        
        return True

    def cancel_refund(self, cr, uid, ids, context=None):
        """
        Cancel the refund deleting the invoices generated only if the invoices are in draft state and don't 
        have a sequence number OR allow Cancel if "keep_out_invoice" is set to true to preserve the out invoice 
        """
        if context is None:
            context = {}
        
        for refund in self.browse(cr, uid, ids, context):
            
            # White the state canceled and set the keep_out_invoice as False
            if refund.keep_out_invoice:
                self.pool.get('sri.refund').write(cr, uid, refund.id, {'state':'cancel','keep_out_invoice':False}, context)
            else:
                # Delete the out invoice only if are in "draft" state
                unlink_invoice_ids = []
                
                for invoice in refund.created_invoice_refund_ids:
                    if invoice.state == 'draft':
                        unlink_invoice_ids.append(invoice.id)
                    else:
                        raise openerp.exceptions.Warning(_('You cannot delete an Invoice generated by Refund which is not draft state'))
    
                osv.osv.unlink(self.pool.get('account.invoice'), cr, uid, unlink_invoice_ids, context=context)
                self.pool.get('sri.refund').write(cr, uid, refund.id, {'state':'cancel'}, context)
       
        return True

    def set_to_draft_refund(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state':'draft'}, context)
        return True
    
    def unlink(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        
        unlink_ids = []

        for refund in self.browse(cr, uid, ids, context):
            if refund.state == 'draft':
                unlink_ids.append(refund.id)
            else:
                raise openerp.exceptions.Warning(_('You cannot delete an Refund which is not draft state.'))
        if refund.invoice_refund_ids or refund.voucher_refund_ids or refund.created_invoice_refund_ids:
            raise openerp.exceptions.Warning(_('You cannot delete a leave with related documents !'))
        osv.osv.unlink(self, cr, uid, unlink_ids, context=context)
        
        return True    
    
    def copy(self, cr, uid, id, default=None, context=None):
        default = default or {}
        default.update({
                        'number':False,
                        'invoice_refund_ids':False,
                        'voucher_refund_ids':False,
                        'created_invoice_refund_ids':False,
                        })
        return super(sri_refund, self).copy(cr, uid, id, default, context)
    # Buttons to operate the workflow to prevent troubles whit double workflow
    def button_create(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        for id in ids:
            wf_service.trg_validate(uid, 'ecua.sri.refund', id, 'create_refund', cr)
        return True
    
    def button_cancel(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        for id in ids:
            wf_service.trg_validate(uid, 'ecua.sri.refund', id, 'cancel_refund', cr)
        return True
    
    def button_set_draft(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        for id in ids:
            wf_service.trg_delete(uid, 'ecua.sri.refund', id, cr)
            wf_service.trg_create(uid, 'ecua.sri.refund', id, cr)
        return True
    def create(self, cr, uid, vals, context=None):
        if vals.get('number','/')=='/': # en el caso de null o void te setea,y compara con "/"
            vals['number'] = self.pool.get('ir.sequence').get(cr, uid, 'sri.refund') or '/' # te asigna el valor de memoria del campo name
        
        return super(sri_refund, self).create(cr, uid, vals, context=context)
    
    def unlink(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid, ids, context=context):
            if rec.state<>'draft':
                raise osv.except_osv(_('Warning!'),_('You cannot delete a leave which is not in draft state !'))
        return super(sri_refund, self).unlink(cr, uid, ids, context)
    
    def name_get(self, cr, uid, ids, context=None):
        
        res = []

        if not context:
            context = {}
        if isinstance(ids, (long, int)):
            ids = [ids]
        for record in self.browse(cr, uid, ids, context=context):
            if record.number:
                nombre1= str(unicode(record.number))
                nombre='['+nombre1+'] '
                if record.name:
                    nombre = nombre+record.name
            else:
                    nombre = record.name
            res.append((record.id,nombre))
       
        return res
    
    def name_search(self, cr, user, name='', args=None, operator='ilike', context=None, limit=100):
        if not args:
            args = []
        if name:
            ids = self.search(cr, user, [('number','=',name)]+ args, limit=limit, context=context)
            if not ids:
                ids = self.search(cr, user, [('name','=',name)]+ args, limit=limit, context=context)
            if not ids:
                ids = set()
                ids.update(self.search(cr, user, args + [('number',operator,name)], limit=limit, context=context))
                if not limit or len(ids) < limit:
                    # we may underrun the limit because of dupes in the results, that's fine
                    ids.update(self.search(cr, user, args + [('name',operator,name)], limit=(limit and (limit-len(ids)) or False) , context=context))
                ids = list(ids)
            if not ids:
                ptrn = re.compile('(\[(.*?)\])')
                res = ptrn.search(name)
                if res:
                    ids = self.search(cr, user, [('number','=', res.group(2))] + args, limit=limit, context=context)
        else:
            ids = self.search(cr, user, args, limit=limit, context=context)
        result = self.name_get(cr, user, ids, context=context)
        return result
    


    def description_invoice_line(self, cr, uid, ids,account_id, description, line,taxes):
        print "hello"
        return {'name': description, #line.name,
                'origin': line.origin,
                'account_id': account_id or line.account_id.id or False,
                'price_unit': line.price_unit,
                'quantity': line.quantity,
                'discount': line.discount,
                'uos_id': line.uos_id.id or False,
                'product_id': line.product_id.id or False,
                'invoice_line_tax_id': taxes,
                #'comment': line.note,
                'account_analytic_id': line.account_analytic_id.id or False,
                        }

sri_refund()
