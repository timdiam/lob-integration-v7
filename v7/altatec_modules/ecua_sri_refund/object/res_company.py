# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors: Patricio Rangles                                                                         
#Copyright (C) 2013 TRESCLOUD Cia. Ltda.                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from osv import fields,osv
from tools.translate import _

class res_company(osv.osv):
    
    _name = 'res.company'
    _inherit = 'res.company'
    
    _columns = {
        'iva_0_purchase_refund_id': fields.many2one('account.tax', 'IVA 0% Purchase refund',
                                                 help="Select the IVA 0% tax used by invoice purchase SRI Refund"),
        'iva_12_purchase_refund_id': fields.many2one('account.tax', 'IVA 12% Purchase refund',
                                                 help="Select the IVA 12% tax used by invoice purchase SRI Refund "),
        'iva_0_sale_refund_id': fields.many2one('account.tax', 'IVA 0% Sale refund',
                                                 help="Select the IVA 0% tax used by invoice sale SRI Refund "),
        'iva_12_sale_refund_id': fields.many2one('account.tax', 'IVA 12% Sale refund',
                                                 help="Select the IVA 12% tax used by invoice sale SRI Refund"),
        'account_id': fields.many2one('account.account', 'Refund Account', domain=[('type','!=','view')], help="It is used to replace the account in every line product in the refund invoice(s)"),
        'type_document_id':fields.many2one('account.invoice.document.type', 'Type Document Refund', required=False),
                }    

res_company()