# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors: Patricio Rangles                                                                         
#Copyright (C) 2013 TRESCLOUD Cia. Ltda.                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import fields,osv
from tools.translate import _
import time

class sri_refund_voucher_invoice(osv.osv_memory):
    
    _name = "sri.refund.voucher.invoice.wizard"
    
    _columns = {
            'refund_append_id': fields.many2one('sri.refund.append.wizard', 'Refund'),
            'document_id': fields.integer('Document id', help="id of document registered"),
            'type': fields.char('Document Type', size=128, help="Type of object of document registered"),
            'number': fields.char('Number', size=128, help="Number of document registered"),
            'partner_id': fields.many2one('res.partner', 'Partner', help="Partner related to the document"),
                }

sri_refund_voucher_invoice()

class sri_refund_append_wizard(osv.osv):
    
    _name = "sri.refund.append.wizard"

    def default_get(self, cr, uid, fields, context=None):

        values = {}
        
        if context is None:
            return values

        type = False
        
        #type of document (invoice/voucher)
        if 'active_model' in context:
            type = context['active_model']
        else:
            return values    

        docs = []
        domain = []
        docs_ids_list = context['active_ids']
        # Before browse the docs needs to be filter:
        # invoice: only purchase invoice that have the document type for refund and state in draft or paid
        # voucher: only Purchase Receipts
        if type == 'account.invoice':
            code_ids = self.pool.get('sri.tax.support').search(cr, uid, [('code','=','08')], context=context)
            domain = [('sri_tax_support_id','=',code_ids[0]),
                      ('state','in',('open','paid')),
                      ('id', 'in', tuple(context['active_ids']))]
            
        elif type == 'account.voucher':
            domain = [('journal_id.type','in',['purchase','purchase_refund']), 
                      ('type','=','purchase'),
                      ('id', 'in', tuple(context['active_ids']))]
            
        docs_ids_list = self.pool.get(type).search(cr, uid, domain, context=context)
        
        for doc in self.pool.get(type).browse(cr, uid, docs_ids_list, context):
            
            val = {
                'document_id': doc.id,
                'type': type,
                'number': doc.number,
                'partner_id': doc.partner_id.id,
                   }
            
            docs.append(val)
        
        values['document_refund_ids'] = docs
        
        return values        
    
    _columns = {
            'refund_id': fields.many2one('sri.refund', 'Refund' , help="Select the refund use for this Invoice(s) or this Purchase Receipt(s)"),
            'document_refund_ids': fields.one2many('sri.refund.voucher.invoice.wizard', 'refund_append_id', 'Invoice/Purchase Receipt'),
                }

    def append_docs(self, cr, uid, ids, context=None):
        """
        Append the documents select in the respective refund, for this the function verify the type of
        object and add in the respective field
        """
        for ref in self.browse(cr, uid, ids):
            
            invoice_ids = []
            voucher_ids = []
            
            refund_obj = self.pool.get('sri.refund')
            refund = refund_obj.browse(cr, uid, ref.refund_id.id, context) 
            
            for inv in refund.invoice_refund_ids:
                invoice_ids.append(inv.id)

            for vou in refund.voucher_refund_ids:
                voucher_ids.append(vou.id)
            
            for doc in ref.document_refund_ids:
                if doc.type == 'account.invoice':
                    try: 
                        invoice_ids.index(doc.document_id)
                    except Exception:
                        invoice_ids.append(doc.document_id)
                    
                elif doc.type == 'account.voucher':
                    try:
                        voucher_ids.index(doc.document_id)
                    except Exception:
                        voucher_ids.append(doc.document_id)
            # Using this help:
            # https://doc.openerp.com/v6.0/developer/2_5_Objects_Fields_Methods/methods.html/#osv.osv.osv.write
            refund_obj.write(cr, uid, ref.refund_id.id, {'invoice_refund_ids': [(6, 0, invoice_ids)],
                                                         'voucher_refund_ids': [(6, 0, voucher_ids)]}, context)
                
        return {'type': 'ir.actions.act_window_close'}


sri_refund_append_wizard()
