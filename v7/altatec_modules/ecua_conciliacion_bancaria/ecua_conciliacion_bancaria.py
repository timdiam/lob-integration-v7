# -*- coding: utf-8 -*-
#################################################################################
#
# Este archivo tiene el modelo de Conciliacion Bancaria
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    11/12/2014
#
#################################################################################
from openerp.osv import fields, osv, orm
import base64
import csv
import re
from StringIO import StringIO
from ecua_conciliacion_lib import *
from datetime import timedelta, datetime


#####################################################################################
# Definición de la clase conciliacion_bancaria
#####################################################################################
class ecua_conciliacion_bancaria(orm.Model):

    _name = 'ecua.conciliacion.bancaria'

    #####################################################################################
    # Toggles the state of the reconciliation
    #####################################################################################
    def change_state( self, cr, uid, ids, context = None ):

        conciliacion = self.browse( cr, uid, ids[ 0 ], context=context )

        if( conciliacion.state == 'new' ):
            new_state = 'complete'
        else:
            new_state = 'new'

        self.write( cr, uid, [ ids[ 0 ] ], { 'state' : new_state } )


    #####################################################################################
    # Recalculate the reconciliation using the file
    #####################################################################################
    def recalcular( self, cr, uid, ids, context = None ):

        # First delete all reconciled and unreconciled lines
        conciliacion = self.browse( cr, uid, ids[ 0 ], context=context )

        rec_obj        = self.pool.get( 'ecua.reconciled.line'        )
        rec_adv_obj    = self.pool.get('ecua.reconcilied.advance')
        unrec_bank_obj = self.pool.get( 'ecua.unreconciled.bank.line' )
        unrec_move_obj = self.pool.get( 'ecua.unreconciled.move.line' )
        unrec_manual_move_obj = self.pool.get( 'ecua.unreconciled.manual.move.line' )
        bck_manual_move_ids = rec_obj.search(cr, uid, [('origin_reconcilie', '=', 'unrec_manual_move_line')], context=context)

        rec_adv_bank_obj   = self.pool.get('ecua.reconcilied.advance.bank.line')
        rec_adv_move_obj   = self.pool.get('ecua.reconcilied.advance.move.line')
        rec_adv_manual_move_obj   = self.pool.get('ecua.reconcilied.advance.manual.move.line')

        #restaurar las líneas agregadas manualmente
        for rec_line in rec_obj.browse(cr, uid, bck_manual_move_ids, context):
            unrec_manual_move_obj.create( cr,
                                   uid,
                                   { 'conciliacion_id'  : conciliacion.id,
                                     'name'             : 'Unreconciled Manual Move Line',
                                     'man_move_date'    : rec_line.move_date,
                                     'man_move_ref'     : rec_line.move_ref,
                                     'man_move_description' : rec_line.move_description,
                                     'man_move_type'        : rec_line.move_type,
                                     'man_move_amount'      : rec_line.move_amount,
                                   }
                                 )
        #restaurar las manuales de la conciliacion avanzada
        rec_adv_man_move_ids = rec_adv_manual_move_obj.search(cr, uid, [], context=context)

        for rec_man_move_line in rec_adv_manual_move_obj.browse(cr, uid, rec_adv_man_move_ids, context=context):
            unrec_manual_move_obj.create( cr,
                                   uid,
                                   { 'conciliacion_id'  : conciliacion.id,
                                     'name'             : 'Unreconciled Manual Move Line',
                                     'man_move_date'    : rec_man_move_line.man_move_date,
                                     'man_move_ref'     : rec_man_move_line.man_move_ref,
                                     'man_move_description' : rec_man_move_line.man_move_description,
                                     'man_move_type'        : rec_man_move_line.man_move_type,
                                     'man_move_amount'      : rec_man_move_line.man_move_amount,
                                   }
                                 )
            #eliminamos los objetos manuales
            rec_adv_manual_move_obj.unlink(cr, uid, rec_man_move_line.id, context=context)

        for rec_line in conciliacion.lines_reconciled:
            rec_obj.unlink( cr, uid, rec_line.id )

        for unrec_bank_line in conciliacion.lines_unreconciled_bank:
            unrec_bank_obj.unlink( cr, uid, unrec_bank_line.id )

        for unrec_move_line in conciliacion.lines_unreconciled_move:
            unrec_move_obj.unlink( cr,uid, unrec_move_line.id )

        #Eliminar los objetos de la conciliacion avanzada
        rec_adv_bank_ids = rec_adv_bank_obj.search(cr, uid, [], context=context)
        rec_adv_bank_obj.unlink(cr, uid, rec_adv_bank_ids, context=context)

        rec_adv_move_ids = rec_adv_move_obj.search(cr, uid, [], context=context)
        rec_adv_bank_obj.unlink(cr, uid, rec_adv_move_ids, context=context)

        #Eliminar las conciliaciones avanzadas
        for rec_adv_line in conciliacion.lines_reconciled_advance:
            rec_adv_obj.unlink( cr, uid, rec_adv_line.id, context=context )

        #Realizar nuevamente la conciliacion de forma automatica
        line_obj         = self.pool.get( 'account.move.line' )
        line_ids         = line_obj.search( cr, uid, [('account_id','=', conciliacion.account.id),('period_id', '=', conciliacion.period.id)], context=context )
        lines            = line_obj.browse( cr, uid, line_ids, context=context )
        reconciled_lines = []

        data = get_data_from_csv( conciliacion.input_file )

        #----------------------------------------------------------------------------------
        # Iterate over the rows in the bank data and reconcile them
        #----------------------------------------------------------------------------------
        for csvrow in data:

            # Iterate over
            for line in lines:

                # Skip this account.move.line if it's been reconciled
                if line.id in reconciled_lines:
                    continue

                # Check this account.move.line for a match on the amount
                if( csvrow[ 'type' ] == 'debit' ):
                    if( abs( line.debit - csvrow[ 'amount' ] ) > 0.001 ):
                        continue

                elif( abs( line.credit - csvrow[ 'amount' ] ) > 0.001 ):
                        continue

                # We have a match on amount, now lets check for match on ref
                if( compare_refs( csvrow[ 'ref' ], line.ref ) == False ):
                    continue

                csvrow[ 'line_id' ] = line.id
                reconciled_lines.append( line.id )
                break

        #----------------------------------------------------------------------------------
        # Create ecua.reconciled.line and ecua.unreconciled.bank.line objects
        #----------------------------------------------------------------------------------
        for row in data:

            # Create ecua.reconciled.line for this row
            if 'line_id' in row:

                line = line_obj.browse( cr, uid, row[ 'line_id' ], context = context )

                if( line.debit > 0 ):
                    line_type = 'debit'
                    line_amount = line.debit
                else:
                    line_type = 'credit'
                    line_amount = line.credit

                if( line_type != row[ 'type' ] ):
                    raise osv.except_osv( 'Error!', "Logic error when creating conciliacion bancaria!" )

                rec_obj.create( cr,
                                uid,
                                { 'conciliacion_id'  : conciliacion.id,
                                  'name'             : 'Reconciled Line',
                                  'bank_date'        : row[ 'date'        ],
                                  'bank_description' : row[ 'description' ],
                                  'bank_ref'         : row[ 'ref'         ],
                                  'bank_type'        : row[ 'type'        ],
                                  'bank_amount'      : row[ 'amount'      ],
                                  'move_date'        : line.date,
                                  'move_description' : line.name,
                                  'move_ref'         : line.ref,
                                  'move_type'        : line_type,
                                  'move_amount'      : line_amount,
                                }
                              )

            # Create ecua.unreconciled.bank.line for this row
            else:

                unrec_bank_obj.create( cr,
                                       uid,
                                       { 'conciliacion_id'  : conciliacion.id,
                                         'name'             : 'Unreconciled Bank Line',
                                         'bank_date'        : row[ 'date'        ],
                                         'bank_ref'         : row[ 'ref'         ],
                                         'bank_description' : row[ 'description' ],
                                         'bank_type'        : row[ 'type'        ],
                                         'bank_amount'      : row[ 'amount'      ],
                                       }
                                     )

        #----------------------------------------------------------------------------------
        # Create ecua.unreconciled.move.line objects
        #----------------------------------------------------------------------------------
        for line in lines:

            if line.id in reconciled_lines:
                continue

            if( line.debit > 0 ):
                line_type = 'debit'
                line_amount = line.debit
            else:
                line_type = 'credit'
                line_amount = line.credit

            unrec_move_obj.create( cr,
                                   uid,
                                   { 'conciliacion_id'  : conciliacion.id,
                                     'name'             : 'Unreconciled Move Line',
                                     'move_date'        : line.date,
                                     'move_ref'         : line.ref,
                                     'move_description' : line.name,
                                     'move_type'        : line_type,
                                     'move_amount'      : line_amount,
                                   }
                                 )

        return { 'type'      : 'ir.actions.act_window',
                 'view_type' : 'form',
                 'view_mode' : 'form,tree',
                 'res_model' : 'ecua.conciliacion.bancaria',
                 'res_id'    : conciliacion.id
               }

    #importar movimientos manuales de la conciliación del período anterior
    def import_manual_move_lines(self, cr, uid, ids, context=None):
        conciliacion = self.browse(cr, uid, ids[0], context=context)

        #convierte en un objeto datetime
        conciliacion_date = datetime.strptime(conciliacion.period.date_start, '%Y-%m-%d')
        #obtiene el último día del mes anterior
        date_start_previous_period =  conciliacion_date - timedelta(days=1)
        #obtener el id del período anterior
        prev_period_id = self.pool.get('account.period').find(cr, uid, date_start_previous_period, context)

        if prev_period_id:
            #obtener la conciliacion del periodo anterior
            conciliacion_obj = self.pool.get('ecua.conciliacion.bancaria')

            prev_conciliacion_id = conciliacion_obj.search(cr, uid, [('period.id', '=', prev_period_id)], context=context)

            if prev_conciliacion_id:
                manual_move_line_obj = self.pool.get('ecua.unreconciled.manual.move.line')
                man_move_lines = conciliacion_obj.browse(cr, uid, prev_conciliacion_id[0], context=context).lines_manual_unreconciled_move

                for man_move in man_move_lines:
                    manual_move_line_obj.create( cr,
                                           uid,
                                           { 'conciliacion_id'  : conciliacion.id,
                                             'name'             : 'Unreconciled Manual Move Line',
                                             'man_move_date'    : man_move.man_move_date,
                                             'man_move_ref'     : man_move.man_move_ref,
                                             'man_move_description' : man_move.man_move_description,
                                             'man_move_type'        : man_move.man_move_type,
                                             'man_move_amount'      : man_move.man_move_amount,
                                           }

                                         )

            else:
                raise osv.except_osv( 'Error!', "No hay conciliacion del periodo anterior" )
        else:
            raise osv.except_osv( 'Error!', "No hay periodo anterior a este, no hay nada que importar." )

        return

    #realiza la conciliación avanzada
    def reconcilie_advance(self, cr, uid, ids, context=None):

        conciliacion_obj = self.pool.get('ecua.conciliacion.bancaria')
        conciliacion     = conciliacion_obj.browse(cr, uid, ids[0], context=context)

        #Wizards
        wizard_obj       = self.pool.get('ecua.conciliacion.advance.wizard')
        wizard_bank_line_obj     = self.pool.get('ecua.conciliacion.advance.bank.wizard.line')
        wizard_move_line_obj     = self.pool.get('ecua.conciliacion.advance.move.wizard.line')
        wizard_man_move_line_obj = self.pool.get('ecua.conciliacion.advance.manual.move.wizard.line')

        #Objetos de los no conciliados
        unrec_bank_lines_obj    = self.pool.get('ecua.unreconciled.bank.line')
        unrec_move_lines_obj    = self.pool.get('ecua.unreconciled.move.line')
        unrec_man_move_lines_obj = self.pool.get('ecua.unreconciled.manual.move.line')

        #Obtener los ids de los items seleccionados
        unrec_bank_lines_sel_ids = unrec_bank_lines_obj.search(cr, uid, [('select', '=', True)], context=context)
        unrec_move_lines_sel_ids = unrec_move_lines_obj.search(cr, uid, [('select', '=', True)], context=context)
        unrec_man_move_lines_sel_ids = unrec_man_move_lines_obj.search(cr, uid, [('select', '=', True)], context=context)

        #Suma de totales
        sum_total_bank = 0.0
        sum_total_move = 0.0
        #Crear el wizard
        wizard_id = wizard_obj.create( cr,
                                       uid,
                                       { 'conciliacion_id'   : conciliacion.id,
                                       },
                                       context = context
                                     )

        #Popular los wizard line de cada objeto
        for bank_line in unrec_bank_lines_obj.browse(cr, uid, unrec_bank_lines_sel_ids, context=context):
            wizard_bank_line_obj.create(cr,
                                        uid,
                                        {
                                            'wizard_id'          : wizard_id,
                                            'unrec_bank_line_id' : bank_line.id,
                                            'bank_date'          : bank_line.bank_date,
                                            'bank_description'   : bank_line.bank_description,
                                            'bank_ref'           : bank_line.bank_ref,
                                            'bank_type'          : bank_line.bank_type,
                                            'bank_amount'        : bank_line.bank_amount,
                                        }
                                    )
            sum_total_bank = sum_total_bank + bank_line.bank_amount

        for move_line in unrec_move_lines_obj.browse(cr, uid, unrec_move_lines_sel_ids, context=context):
            wizard_move_line_obj.create(cr,
                                        uid,
                                        {
                                            'wizard_id'          : wizard_id,
                                            'unrec_move_line_id' : move_line.id,
                                            'move_date'          : move_line.move_date,
                                            'move_description'   : move_line.move_description,
                                            'move_ref'           : move_line.move_ref,
                                            'move_type'          : move_line.move_type,
                                            'move_amount'        : move_line.move_amount,
                                        }
                                    )
            sum_total_move = sum_total_move + move_line.move_amount

        for man_move_line in unrec_man_move_lines_obj.browse(cr, uid, unrec_man_move_lines_sel_ids, context=context):
            wizard_man_move_line_obj.create(cr,
                                        uid,
                                        {
                                            'wizard_id'                     : wizard_id,
                                            'unrec_manual_move_line_id'     : man_move_line.id,
                                            'man_move_date'                 : man_move_line.man_move_date,
                                            'man_move_description'          : man_move_line.man_move_description,
                                            'man_move_ref'                  : man_move_line.man_move_ref,
                                            'man_move_type'                 : man_move_line.man_move_type,
                                            'man_move_amount'               : man_move_line.man_move_amount,
                                        }
                                    )

            sum_total_move = sum_total_move + man_move_line.man_move_amount

        wizard_obj.write(cr, uid, wizard_id, {'sum_total_bank': sum_total_bank, 'sum_total_move': sum_total_move}, context=context)

        # Return the wizard's form view popup
        return { 'name'      :  "Conciliacion Avanzada",
                 'view_mode' : 'form',
                 'view_type' : 'form',
                 'res_model' : 'ecua.conciliacion.advance.wizard',
                 'res_id'    : wizard_id,
                 'type'      : 'ir.actions.act_window',
                 'target'    : 'new',
                 'context'   : context,
               }



    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = { 'name'                    : fields.char( 'Nombre' ),
                 'state'                   : fields.selection( [('new','Nuevo'),('complete','Realizado')], string="Estado" ),
                 'period'                  : fields.many2one( 'account.period', string='Periodo' ),
                 'account'                 : fields.many2one( 'account.account', string='Cuenta' ),
                 'input_file'              : fields.binary( 'Archivo Bancario' ),
                 'date_reconciled'         : fields.date( 'Fecha de conciliacion' ),
                 'lines_reconciled'        : fields.one2many( 'ecua.reconciled.line', 'conciliacion_id', string='Líneas Conciliadas' ),
                 'lines_unreconciled_bank' : fields.one2many( 'ecua.unreconciled.bank.line', 'conciliacion_id', string='Líneas de Banco No Conciliadas' ),
                 'lines_unreconciled_move' : fields.one2many( 'ecua.unreconciled.move.line', 'conciliacion_id', string='Líneas de Movimientos No Conciliadas' ),
                 'lines_manual_unreconciled_move' : fields.one2many('ecua.unreconciled.manual.move.line', 'conciliacion_id', string='Líneas de Movimientos Manuales No Conciliadas'),
                 'lines_reconciled_advance'       : fields.one2many('ecua.reconcilied.advance', 'conciliacion_id', string='Líneas Conciliadas Avanzadas'),
               }


    #####################################################################################
    # Defaults definitions
    #####################################################################################
    _defaults = { 'name'            : 'Conciliacion Bancaria',
                  'state'           : 'new',
                  'date_reconciled' : fields.date.context_today,
                }


#####################################################################################
# Definición de la clase conciliacion_bancaria
#####################################################################################
class ecua_reconciled_line( orm.Model ):

    _name = 'ecua.reconciled.line'

    #####################################################################################
    # Delete this ecua.reconciled.line and create an ecua.reconciled.bank.line and a
    # ecua.unreconciled.move.line
    #####################################################################################
    def unreconcile( self, cr, uid, ids, context = None ):

        conciliacion_obj = self.pool.get( 'ecua.conciliacion.bancaria'  )
        unrec_bank_obj   = self.pool.get( 'ecua.unreconciled.bank.line' )
        unrec_move_obj   = self.pool.get( 'ecua.unreconciled.move.line' )
        unrec_manual_move_obj   = self.pool.get( 'ecua.unreconciled.manual.move.line' )

        rec_line = self.browse( cr, uid, ids[ 0 ], context=context )

        if( rec_line.conciliacion_id.state != 'new' ):
            raise osv.except_osv( 'Error!', "No puede eliminar una linea conciliado en estado 'Realizado'" )

        parent_id = rec_line.conciliacion_id.id

        unrec_bank_obj.create( cr,
                               uid,
                               { 'conciliacion_id'  : parent_id,
                                 'name'             : 'Unreconciled Bank Line',
                                 'bank_date'        : rec_line.bank_date,
                                 'bank_ref'         : rec_line.bank_ref,
                                 'bank_description' : rec_line.bank_description,
                                 'bank_type'        : rec_line.bank_type,
                                 'bank_amount'      : rec_line.bank_amount,
                               }
                             )
        #momentaneo para solucionar un bug. Regresar cuando se haya probado toda la conciliacion

        if rec_line.origin_reconcilie == 'unrec_move_line':
            unrec_move_obj.create( cr,
                                   uid,
                                   { 'conciliacion_id'  : parent_id,
                                     'name'             : 'Unreconciled Move Line',
                                     'move_date'        : rec_line.move_date,
                                     'move_ref'         : rec_line.move_ref,
                                     'move_description' : rec_line.move_description,
                                     'move_type'        : rec_line.move_type,
                                     'move_amount'      : rec_line.move_amount,
                                   }
                                 )

        if rec_line.origin_reconcilie == 'unrec_manual_move_line':
            unrec_manual_move_obj.create( cr,
                                   uid,
                                   { 'conciliacion_id'  : parent_id,
                                     'name'             : 'Unreconciled Manual Move Line',
                                     'man_move_date'        : rec_line.move_date,
                                     'man_move_ref'         : rec_line.move_ref,
                                     'man_move_description' : rec_line.move_description,
                                     'man_move_type'        : rec_line.move_type,
                                     'man_move_amount'      : rec_line.move_amount,
                                   }
                                 )

        self.unlink( cr, uid, ids )

        return { 'type'      : 'ir.actions.act_window',
                 'view_type' : 'form',
                 'view_mode' : 'form,tree',
                 'res_model' : 'ecua.conciliacion.bancaria',
                 'res_id'    : parent_id
               }


    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = { 'conciliacion_id'  : fields.many2one( 'ecua.conciliacion.bancaria', string='Conciliacion Bancaria' ),
                 'name'             : fields.char( 'Nombre' ),
                 'bank_date'        : fields.char( 'Fecha Banco' ),
                 'bank_description' : fields.char( 'Descripcion Banco' ),
                 'bank_ref'         : fields.char( 'Referencia Banco' ),
                 'bank_type'        : fields.selection( [('debit', 'Debito'), ('credit', 'Credito')], string="Tipo Banco" ),
                 'bank_amount'      : fields.float( 'Valor Banco' ),
                 'move_date'        : fields.char( 'Fecha' ),
                 'move_description' : fields.char( 'Descripcion' ),
                 'move_ref'         : fields.char( 'Referencia' ),
                 'move_type'        : fields.selection( [('debit', 'Debito'), ('credit', 'Credito')], string="Tipo" ),
                 'move_amount'      : fields.float( 'Valor' ),
                 'origin_reconcilie': fields.selection([('unrec_move_line', 'Unreconcilied Move Lines'),
                                                        ('unrec_manual_move_line', 'Unreconcilied Manual Move Lines')],
                                                       string='Origen de Reconciliación'), #para al reversar saber qué objeto hay que restaurar


               }


#####################################################################################
# Definición de la clase conciliacion_bancaria
#####################################################################################
class ecua_unreconciled_bank_line( orm.Model ):

    _name = 'ecua.unreconciled.bank.line'


    #####################################################################################
    # Create a wizard that allows the user to select an account move line to reconcile
    # this bank line with.
    #####################################################################################
    def reconcile_bank_line( self, cr, uid, ids, context = None ):

        unreconciled_bank_line  = self.pool.get( 'ecua.unreconciled.bank.line' ).browse( cr, uid, ids[ 0 ], context=context )

        if( unreconciled_bank_line.conciliacion_id.state != 'new' ):
            raise osv.except_osv( 'Error!', "No puede conciliar una linea en estado 'Realizado'" )

        unreconciled_move_lines = unreconciled_bank_line.conciliacion_id.lines_unreconciled_move
        unreconcilied_manual_move_lines = unreconciled_bank_line.conciliacion_id.lines_manual_unreconciled_move #líneas no conciliadas agregadas manualmente
        wizard_obj              = self.pool.get( 'ecua.conciliacion.manual.bank.wizard' )
        wizard_line_obj         = self.pool.get( 'ecua.conciliacion.manual.bank.wizard.line' )

        wizard_id = wizard_obj.create( cr,
                                       uid,
                                       { 'conciliacion_id'   : unreconciled_bank_line.conciliacion_id.id,
                                         'unrec_bank_line_id': unreconciled_bank_line.id,
                                         'bank_date'         : unreconciled_bank_line.bank_date,
                                         'bank_description'  : unreconciled_bank_line.bank_description,
                                         'bank_ref'          : unreconciled_bank_line.bank_ref,
                                         'bank_type'         : unreconciled_bank_line.bank_type,
                                         'bank_amount'       : unreconciled_bank_line.bank_amount,
                                       },
                                       context = context
                                     )

        for move_line in unreconciled_move_lines:

            if( abs( move_line.move_amount - unreconciled_bank_line.bank_amount ) > 0.01 ):
                continue

            if( move_line.move_type != unreconciled_bank_line.bank_type ):
                continue

            wizard_line_obj.create( cr,
                                    uid,
                                    { 'wizard_id'          : wizard_id,
                                      'unrec_move_line_id' : move_line.id,
                                      'move_date'          : move_line.move_date,
                                      'move_description'   : move_line.move_description,
                                      'move_ref'           : move_line.move_ref,
                                      'move_type'          : move_line.move_type,
                                      'move_amount'        : move_line.move_amount,
                                      'origin_move_line'   : 'unrec_move_line',
                                    },
                                    context = context,
                                  )

        for move_line in unreconcilied_manual_move_lines:

            if( abs( move_line.man_move_amount - unreconciled_bank_line.bank_amount ) > 0.01 ):
                continue

           # if( move_line.move_type != unreconciled_bank_line.bank_type ):
            #    continue

            wizard_line_obj.create( cr,
                                    uid,
                                    { 'wizard_id'          : wizard_id,
                                      'unrec_move_line_id' : move_line.id,
                                      'move_date'          : move_line.man_move_date,
                                      'move_description'   : move_line.man_move_description,
                                      'move_ref'           : move_line.man_move_ref,
                                      'move_type'          : move_line.man_move_type,
                                      'move_amount'        : move_line.man_move_amount,
                                      'origin_move_line'   : 'unrec_manual_move_line',
                                    },
                                    context = context,
                                  )

        # Return the wizard's form view popup
        return { 'name'      :  "Nueva Conciliacion",
                 'view_mode' : 'form',
                 'view_type' : 'form',
                 'res_model' : 'ecua.conciliacion.manual.bank.wizard',
                 'res_id'    : wizard_id,
                 'type'      : 'ir.actions.act_window',
                 'target'    : 'new',
                 'context'   : context,
               }

    #marca la línea seleccionada
    def select_unrec(self, cr, uid, ids, context=None):
        line_obj  = self.pool.get('ecua.unreconciled.bank.line')
        this_line = line_obj.browse( cr, uid, ids[0], context=context )
        line_obj.write(cr, uid, [this_line.id], {'select': True})
        return

    #retira la marca de la línea seleccionada
    def unselect_unrec(self, cr, uid, ids, context=None):
        line_obj  = self.pool.get('ecua.unreconciled.bank.line')
        this_line = line_obj.browse( cr, uid, ids[0], context=context )
        line_obj.write(cr, uid, [this_line.id], {'select': False})
        return

    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = { 'conciliacion_id'  : fields.many2one( 'ecua.conciliacion.bancaria', string='Conciliacion Bancaria' ),
                 'name'             : fields.char( 'Nombre' ),
                 'bank_date'        : fields.char( 'Fecha' ),
                 'bank_description' : fields.char( 'Descripcion Banco' ),
                 'bank_ref'         : fields.char( 'Referencia' ),
                 'bank_type'        : fields.selection( [('debit', 'Debito'), ('credit', 'Credito')], string="Tipo" ),
                 'bank_amount'      : fields.float( 'Valor' ),
                 'select'           : fields.boolean('Seleccionar', default=False),

               }


#####################################################################################
# Definición de la clase conciliacion_bancaria
#####################################################################################
class ecua_unreconciled_move_line( orm.Model ):

    _name = 'ecua.unreconciled.move.line'

    #marca la línea seleccionada
    def select_unrec(self, cr, uid, ids, context=None):
        line_obj  = self.pool.get('ecua.unreconciled.move.line')
        this_line = line_obj.browse( cr, uid, ids[0], context=context )
        line_obj.write(cr, uid, [this_line.id], {'select': True})
        return

    #retira la marca de la línea seleccionada
    def unselect_unrec(self, cr, uid, ids, context=None):
        line_obj  = self.pool.get('ecua.unreconciled.move.line')
        this_line = line_obj.browse( cr, uid, ids[0], context=context )
        line_obj.write(cr, uid, [this_line.id], {'select': False})
        return

    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = { 'conciliacion_id'  : fields.many2one( 'ecua.conciliacion.bancaria', string='Conciliacion Bancaria' ),
                 'name'             : fields.char( 'Nombre' ),
                 'move_date'        : fields.char( 'Fecha' ),
                 'move_ref'         : fields.char( 'Referencia' ),
                 'move_description' : fields.char( 'Descripcion' ),
                 'move_type'        : fields.selection( [('debit', 'Debito'), ('credit', 'Credito')], string="Tipo" ),
                 'move_amount'      : fields.float( 'Valor' ),
                 'select'           : fields.boolean('Seleccionar', default=False),
               }


#####################################################################################
# Definición de la clase conciliacion_bancaria
#####################################################################################
class ecua_unreconciled_manual_move_line(orm.Model):
    _name = 'ecua.unreconciled.manual.move.line'

    #marca la línea seleccionada
    def select_unrec(self, cr, uid, ids, context=None):
        line_obj  = self.pool.get('ecua.unreconciled.manual.move.line')
        this_line = line_obj.browse( cr, uid, ids[0], context=context )
        line_obj.write(cr, uid, [this_line.id], {'select': True})
        return

    #retira la marca de la línea seleccionada
    def unselect_unrec(self, cr, uid, ids, context=None):
        line_obj  = self.pool.get('ecua.unreconciled.manual.move.line')
        this_line = line_obj.browse( cr, uid, ids[0], context=context )
        line_obj.write(cr, uid, [this_line.id], {'select': False})
        return

    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = {
        'conciliacion_id'  : fields.many2one( 'ecua.conciliacion.bancaria', string='Conciliacion Bancaria'),
        'name'             : fields.char('Nombre'),
        'man_move_date'    : fields.date('Fecha'),
        'man_move_ref'     : fields.char( 'Referencia' ),
        'man_move_description' : fields.char('Descripción'),
        'man_move_type'    : fields.selection( [('debit', 'Debito'), ('credit', 'Credito')], string="Tipo" ),
        'man_move_amount'  : fields.float('Valor'),
        'select'           : fields.boolean('Seleccionar', default=False),

    }

#####################################################################################
# Definición de la clase conciliacion_bancaria
#####################################################################################
class ecua_reconcilied_advance(orm.Model):
    _name = 'ecua.reconcilied.advance'

    #Reversar la conciliacion avanzada
    def unreconcile(self, cr, uid, ids, context=None):

        conciliacion_obj = self.pool.get('ecua.conciliacion.bancaria')
        unrec_bank_obj   = self.pool.get( 'ecua.unreconciled.bank.line' )
        unrec_move_obj   = self.pool.get( 'ecua.unreconciled.move.line' )
        unrec_manual_move_obj   = self.pool.get('ecua.unreconciled.manual.move.line')

        rec_adv_bank_obj   = self.pool.get('ecua.reconcilied.advance.bank.line')
        rec_adv_move_obj   = self.pool.get('ecua.reconcilied.advance.move.line')
        rec_adv_manual_move_obj   = self.pool.get('ecua.reconcilied.advance.manual.move.line')

        rec_line = self.browse(cr, uid, ids[0], context=context)

        if (rec_line.conciliacion_id.state != 'new'):
            raise osv.except_osv('Error!', "No puede eliminar una linea conciliado en estado 'Realizado'")

        parent_id = rec_line.conciliacion_id.id

        for bank_line in rec_line.bank_lines:
            unrec_bank_obj.create(cr,
                                  uid,
                                  {
                                      'conciliacion_id'  : parent_id,
                                      'name'             : 'Unreconciled Bank Line',
                                      'bank_date'        : bank_line.bank_date,
                                      'bank_ref'         : bank_line.bank_ref,
                                      'bank_description' : bank_line.bank_description,
                                      'bank_type'        : bank_line.bank_type,
                                      'bank_amount'      : bank_line.bank_amount,
                                  },
                                  context = context,
                                )

            rec_adv_bank_obj.unlink(cr, uid, bank_line.id, context=context)

        for move_line in rec_line.move_lines:
            unrec_move_obj.create(cr,
                                  uid,
                                  {
                                     'conciliacion_id'  : parent_id,
                                     'name'             : 'Unreconciled Move Line',
                                     'move_date'        : move_line.move_date,
                                     'move_ref'         : move_line.move_ref,
                                     'move_description' : move_line.move_description,
                                     'move_type'        : move_line.move_type,
                                     'move_amount'      : move_line.move_amount,
                                  },
                                  context = context,
                                )
            rec_adv_move_obj.unlink(cr, uid, move_line.id, context=context)

        for man_move_line in rec_line.man_move_lines:
            unrec_manual_move_obj.create(cr,
                                         uid,
                                         { 'conciliacion_id'       : parent_id,
                                            'name'                 : 'Unreconciled Manual Move Line',
                                            'man_move_date'        : man_move_line.man_move_date,
                                            'man_move_ref'         : man_move_line.man_move_ref,
                                            'man_move_description' : man_move_line.man_move_description,
                                            'man_move_type'        : man_move_line.man_move_type,
                                            'man_move_amount'      : man_move_line.man_move_amount,
                                        },
                                         context=context,
                                )
            rec_adv_manual_move_obj.unlink(cr, uid, man_move_line.id, context=context)

        self.unlink(cr, uid, ids)

        return { 'type'      : 'ir.actions.act_window',
                 'view_type' : 'form',
                 'view_mode' : 'form,tree',
                 'res_model' : 'ecua.conciliacion.bancaria',
                 'res_id'    : parent_id
               }


    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = { 'conciliacion_id'  : fields.many2one( 'ecua.conciliacion.bancaria', string='Conciliacion Bancaria' ),
                 'name'             : fields.char('Nombre'),
                 'period'           : fields.char('Período'),
                 'total'            : fields.float('Total Reconciliado'),
                 'bank_lines'       : fields.one2many('ecua.reconcilied.advance.bank.line', 'reconciled_advance', string='Líneas de Banco Conciliadas'),
                 'move_lines'       : fields.one2many('ecua.reconcilied.advance.move.line', 'reconciled_advance', string='Líneas de Movimientos Conciliadas'),
                 'man_move_lines'   : fields.one2many('ecua.reconcilied.advance.manual.move.line', 'reconciled_advance', string='Líneas de Movimientos Manuales Conciliadas'),

                }

#####################################################################################
# Definición de la clase conciliacion_bancaria
#####################################################################################
class ecua_reconcilied_advance_bank_line(orm.Model):
    _name = 'ecua.reconcilied.advance.bank.line'

    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = { 'conciliacion_id'   : fields.many2one('ecua.conciliacion.bancaria', string='Conciliacion Bancaria'),
                 'reconciled_advance' : fields.many2one('ecua.reconcilied.advance', string='Conciliacion Avanzada'),
                 'name'              : fields.char('Nombre'),
                 'bank_date'         : fields.char('Fecha'),
                 'bank_description'  : fields.char('Descripcion Banco'),
                 'bank_ref'          : fields.char('Referencia'),
                 'bank_type'         : fields.selection([('debit', 'Debito'), ('credit', 'Credito')], string="Tipo"),
                 'bank_amount'       : fields.float('Valor'),
                }

#####################################################################################
# Definición de la clase conciliacion_bancaria
#####################################################################################
class ecua_reconcilied_advance_move_line(orm.Model):
    _name = 'ecua.reconcilied.advance.move.line'

    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = { 'conciliacion_id'   : fields.many2one('ecua.conciliacion.bancaria', string='Conciliacion Bancaria'),
                 'reconciled_advance': fields.many2one('ecua.reconcilied.advance', string='Conciliacion Avanzada'),
                 'name'              : fields.char('Nombre'),
                 'move_date'         : fields.char('Fecha'),
                 'move_ref'          : fields.char('Referencia'),
                 'move_description'  : fields.char('Descripcion'),
                 'move_type'         : fields.selection([('debit', 'Debito'), ('credit', 'Credito')], string="Tipo"),
                 'move_amount'       : fields.float( 'Valor' ),
                }

class ecua_reconcilied_advance_manual_move_line(orm.Model):
    _name = 'ecua.reconcilied.advance.manual.move.line'

    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = {
        'conciliacion_id'   : fields.many2one('ecua.conciliacion.bancaria', string='Conciliacion Bancaria'),
        'reconciled_advance': fields.many2one('ecua.reconcilied.advance', string='Conciliacion Avanzada'),
        'name'              : fields.char('Nombre'),
        'man_move_date'     : fields.date('Fecha'),
        'man_move_ref'      : fields.char('Referencia'),
        'man_move_description': fields.char('Descripción'),
        'man_move_type'     : fields.selection([('debit', 'Debito'), ('credit', 'Credito')], string="Tipo"),
        'man_move_amount'   : fields.float('Valor'),
        }
