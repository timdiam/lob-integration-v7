{
    'name': 'Ecua Conciliacion Bancaria',
    'version': '1.0',
    'description': """
        Ecua Conciliacion Bancaria
    """,
    'author': 'Dan Haggerty',
    'website': 'www.altatececuador.com',
    "depends" : [ 'account', 'cross_company' ],
    "data" : [ 'ecua_conciliacion_bancaria.xml' ],
    "installable": True,
    "auto_install": False
}