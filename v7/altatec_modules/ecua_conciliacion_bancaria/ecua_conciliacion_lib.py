# -*- coding: utf-8 -*-
#################################################################################
#
# This file contains the CSV reading and handling functions that are used
# by both the wizard and the ecua.conciliacion.bancaria itself.
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    January 16th, 2015
#
#################################################################################
from   openerp.osv import fields, osv, orm
from   StringIO    import StringIO
import base64
import csv
import re

### THIS IS GOOD CODE ###

#################################################################################
# Read a CSV file's encoded contents and return a list of dictionaries containing
# the formatted data
#################################################################################
def get_data_from_csv( filestring ):

    CSV_NUM_COLUMNS = 5

    bank_data = []
    error_string = ""

    file        = StringIO( base64.decodestring( filestring ) )
    #file_reader = csv.reader( file, delimiter=',', quotechar='"')
    file_reader = csv.reader( file, delimiter=';', quotechar='"')
    for i, row in enumerate( file_reader ):

        bank_move = {}

        # Validate number of columns
        if( len( row ) != CSV_NUM_COLUMNS ):
            raise osv.except_osv( 'Error!', "Fila " + str( i+1 ) + ": " + "Esta fila debe tener " + str( CSV_NUM_COLUMNS ) + " columnas\n" )

        # Strip spaces from date, type, and amount, but not reference
        input_date        = row[ 0 ].replace( " ", "" )
        input_ref         = row[ 1 ].strip()
        input_type        = row[ 2 ].replace( " ", "" )
        input_amount      = row[ 3 ].replace( " ", "" ).replace( ",", "" )
        input_description = row[ 4 ].strip()

        # Validate the date in the first column
        if( re.match( r'(\d+/\d+/\d+)', input_date ) == None ):
            error_string += "Fila " + str( i+1 ) + ": " + "Columna 1 debe ser del formato MM/DD/YYYY\n"

        # Validate the type in the third column
        if( input_type != "+" and input_type != "-" ):
            error_string += "Fila " + str( i+1 ) + ": " + "Columna 3 debe ser '+' o '-'\n"

        # Validate the amount in the 4th column
        try:    float( input_amount )
        except: error_string += "Fila " + str( i+1 ) + ": " + "Columna 4 debe ser un monto\n"

        # Store the data in a dictionary
        bank_move[ 'date'        ] = input_date[ -4: ] + '-' + input_date[ :2 ] + '-' + input_date[ 3:5 ]
        bank_move[ 'ref'         ] = input_ref
        bank_move[ 'type'        ] = 'debit' if input_type == '+' else 'credit'
        bank_move[ 'amount'      ] = float( input_amount )
        bank_move[ 'description' ] = input_description
        bank_data.append( bank_move )

    if( error_string != "" ):
        raise osv.except_osv( 'Error!', 'Se encontraron los siguientes errores con el archivo bancario:\n' + error_string )

    return bank_data


#####################################################################################
# Esta funcion accepta los campos del wizard de conciliacion, y hace la conciliacion.
#####################################################################################
def compare_refs( csv_ref, line_ref ):

    # account.move.line.ref needs to be at least as long as csv_ref
    if( len( line_ref ) < len( csv_ref ) ):
        return False

    # If the end of the account.move.line.ref is the same as the csv_ref, we have a match
    if( line_ref[ -1 * len( csv_ref ): ] == csv_ref ):
        return True

    return False