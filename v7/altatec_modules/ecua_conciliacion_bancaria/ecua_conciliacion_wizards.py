# -*- coding: utf-8 -*-
#################################################################################
#
# This file contains the different wizards associated with the
# ecua_conciliacion_bancaria module.
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    11/12/2014
#
#################################################################################
from openerp.osv import fields, osv, orm
from ecua_conciliacion_lib import *

#####################################################################################
# Definición de la clase conciliacion_bancaria
#####################################################################################
class ecua_conciliacion_bancaria_wizard(osv.osv_memory):

    _name = 'ecua.conciliacion.bancaria.wizard'


    #####################################################################################
    # Esta funcion accepta los campos del wizard de conciliacion, y hace la conciliacion.
    #####################################################################################
    def crear_conciliacion( self, cr, uid, ids, context = None ):

        wizard = self.browse( cr, uid, ids[ 0 ], context=context )

        #----------------------------------------------------------------------------------
        # Obtain and validate the bank CSV file
        #----------------------------------------------------------------------------------
        if( wizard.file == False ):
            raise osv.except_osv( 'Error!', "El archivo bancario esta vacia!" )

        # Extract data from the csv file into a list of dictionaries
        data = get_data_from_csv( wizard.file )

        line_obj         = self.pool.get( 'account.move.line' )
        line_ids         = line_obj.search( cr, uid, [('account_id', '=', wizard.account.id), ('period_id', '=', wizard.period.id), ('state', '=', 'valid')], context=context )
        lines            = line_obj.browse( cr, uid, line_ids, context=context )
        reconciled_lines = []

        #----------------------------------------------------------------------------------
        # Iterate over the rows in the bank data and reconcile them
        #----------------------------------------------------------------------------------
        for csvrow in data:

            # Iterate over
            for line in lines:

                # Skip this account.move.line if it's been reconciled
                if line.id in reconciled_lines:
                    continue

                # Check this account.move.line for a match on the amount
                if( csvrow[ 'type' ] == 'debit' ):
                    if( abs( line.debit - csvrow[ 'amount' ] ) > 0.001 ):
                        continue

                elif( abs( line.credit - csvrow[ 'amount' ] ) > 0.001 ):
                        continue

                # We have a match on amount, now lets check for match on ref
                if( compare_refs( csvrow[ 'ref' ], line.ref ) == False ):
                    continue


                csvrow[ 'line_id' ] = line.id
                reconciled_lines.append( line.id )
                break

        #----------------------------------------------------------------------------------
        # Now that it's been reconciled create an ecua.conciliacion.bancaria object
        #----------------------------------------------------------------------------------
        conciliacion_obj = self.pool.get( 'ecua.conciliacion.bancaria' )

        new_conciliacion_id = conciliacion_obj.create( cr,
                                                       uid,
                                                       { 'period'     : wizard.period.id,
                                                         'account'    : wizard.account.id,
                                                         'input_file' : wizard.file,
                                                       },
                                                     )

        #----------------------------------------------------------------------------------
        # Create ecua.reconciled.line and ecua.unreconciled.bank.line objects
        #----------------------------------------------------------------------------------
        rec_obj        = self.pool.get( 'ecua.reconciled.line'        )
        unrec_bank_obj = self.pool.get( 'ecua.unreconciled.bank.line' )
        unrec_move_obj = self.pool.get( 'ecua.unreconciled.move.line' )

        for row in data:

            # Create ecua.reconciled.line for this row
            if 'line_id' in row:

                line = line_obj.browse( cr, uid, row[ 'line_id' ], context = context )

                if( line.debit > 0 ):
                    line_type = 'debit'
                    line_amount = line.debit
                else:
                    line_type = 'credit'
                    line_amount = line.credit

                if( line_type != row[ 'type' ] ):
                    raise osv.except_osv( 'Error!', "Logic error when creating conciliacion bancaria!" )

                rec_obj.create( cr,
                                uid,
                                { 'conciliacion_id'  : new_conciliacion_id,
                                  'name'             : 'Reconciled Line',
                                  'bank_date'        : row[ 'date'        ],
                                  'bank_description' : row[ 'description' ],
                                  'bank_ref'         : row[ 'ref'         ],
                                  'bank_type'        : row[ 'type'        ],
                                  'bank_amount'      : row[ 'amount'      ],
                                  'move_date'        : line.date,
                                  'move_description' : line.name,
                                  'move_ref'         : line.ref,
                                  'move_type'        : line_type,
                                  'move_amount'      : line_amount,
                                  'origin_reconcilie': 'unrec_move_line', #TODO:si a futuro se va crear la conciliacion automática no solo con los movimientos contables sino con los manuales esto debe cambiar
                                }
                              )

            # Create ecua.unreconciled.bank.line for this row
            else:

                unrec_bank_obj.create( cr,
                                       uid,
                                       { 'conciliacion_id'  : new_conciliacion_id,
                                         'name'             : 'Unreconciled Bank Line',
                                         'bank_date'        : row[ 'date'        ],
                                         'bank_ref'         : row[ 'ref'         ],
                                         'bank_description' : row[ 'description' ],
                                         'bank_type'        : row[ 'type'        ],
                                         'bank_amount'      : row[ 'amount'      ],
                                       }
                                     )

        #----------------------------------------------------------------------------------
        # Create ecua.unreconciled.move.line objects
        #----------------------------------------------------------------------------------
        for line in lines:

            if line.id in reconciled_lines:
                continue

            if( line.debit > 0 ):
                line_type = 'debit'
                line_amount = line.debit
            else:
                line_type = 'credit'
                line_amount = line.credit

            unrec_move_obj.create( cr,
                                   uid,
                                   { 'conciliacion_id'  : new_conciliacion_id,
                                     'name'             : 'Unreconciled Move Line',
                                     'move_date'        : line.date,
                                     'move_ref'         : line.ref,
                                     'move_description' : line.name,
                                     'move_type'        : line_type,
                                     'move_amount'      : line_amount,
                                   }
                                 )

        return { 'type'      : 'ir.actions.act_window',
                 'view_type' : 'form',
                 'view_mode' : 'form,tree',
                 'res_model' : 'ecua.conciliacion.bancaria',
                 'res_id'    : new_conciliacion_id
               }


    #####################################################################################
    # El boton cancelar.
    #####################################################################################
    def cancelar_conciliacion( self, cr, uid, ids, context = None ):
        return


    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = { 'period'    : fields.many2one( 'account.period', string='Periodo' ),
                 'account'   : fields.many2one( 'account.account', string='Cuenta' ),
                 'file'      : fields.binary( 'Archivo Bancario', required=True ),
               }


    #####################################################################################
    # Default definitions
    #####################################################################################
    _defaults = { 'period'    : 25,
                  'account'   : 371,
                  'file'      : 'MTEvMDUvMjAxNCw0MCwtLDM3Mi4wMCxQQUdPIERFIENIRVFVRSBERSBWRU5UQU5JTExBCjExLzA1LzIwMTQsNDEsLSwzNzAuMjMsUEFHTyBERSBDSEVRVUUgREUgVkVOVEFOSUxMQQoxMS8wNS8yMDE0LDM4LC0sIjQsMDQyLjUwIixQQUdPIERFIENIRVFVRSBUUkFOU0ZFUklETwoxMS8wNy8yMDE0LDQyLC0sNDA1LjAwLFBBR08gREUgQ0hFUVVFIERFIENBTUFSQQoxMS8wNy8yMDE0LDQzLC0sMjAuNTgsUEFHTyBERSBDSEVRVUUgREUgVkVOVEFOSUxMQQoxMS8xMS8yMDE0LDQ0LC0sMTQyLjA4LFBBR08gREUgQ0hFUVVFIERFIENBTUFSQQoxMS8xMS8yMDE0LDQ1LC0sMTQyLjA4LFBBR08gREUgQ0hFUVVFIERFIENBTUFSQQoxMS8xMi8yMDE0LDMsLSwzMC4wMCxFTUlTSU9OIERFIENIRVFVRVJBCjExLzEzLzIwMTQsMjk0LC0sNDc1LjIwLE4vRCBQQUdPIFJFQ0FVREFDSU9ORVMgSUVTUwoxMS8xNC8yMDE0LDQ2LC0sMTA4LjAwLFBBR08gREUgQ0hFUVVFIERFIFZFTlRBTklMTEEKMTEvMTQvMjAxNCwxLCssIjYsODIwLjAwIixERVBPU0lUTyBDT01QTEVUTwoxMS8xNC8yMDE0LDEwMCwtLCIxLDEwMC4wMCIsTi9EIERJUi1PUkRFTiBERSBQQUdPCjExLzE0LzIwMTQsMjc0LC0sNC4wMCxOL0QgRElSLUNPTUlTSU9OIC0gT1JERU4gREUgUEEKMTEvMTQvMjAxNCwxMDAsLSw3NTAuMDAsTi9EIERJUi1PUkRFTiBERSBQQUdPCjExLzE0LzIwMTQsMjc0LC0sMS4wMCxOL0QgRElSLUNPTUlTSU9OIC0gT1JERU4gREUgUEEKMTEvMTQvMjAxNCwxMDAsLSw2NTAuMDAsTi9EIERJUi1PUkRFTiBERSBQQUdPCjExLzE0LzIwMTQsMjc0LC0sMS4wMCxOL0QgRElSLUNPTUlTSU9OIC0gT1JERU4gREUgUEEKMTEvMTgvMjAxNCw0OCwtLDQ0Ni44OCxDRVJUSUZJQ0FDSU9OIERFIENIRVFVRVMKMTEvMTkvMjAxNCwxLCssNjA5LjYwLERFUE9TSVRPIENPTVBMRVRPCjExLzE5LzIwMTQsMSwrLDIyMC4wMCxERVBPU0lUTyBDT01QTEVUTwoxMS8xOS8yMDE0LDEsKywyMjAuMDAsREVQT1NJVE8gQ09NUExFVE8KMTEvMTkvMjAxNCwxLCssMjIwLjAwLERFUE9TSVRPIENPTVBMRVRPCjExLzIwLzIwMTQsNDksLSwiMSwzMjAuMDAiLFBBR08gREUgQ0hFUVVFIERFIENBTUFSQQoxMS8yNS8yMDE0LDQ3LC0sMzg1LjAwLFBBR08gREUgQ0hFUVVFIERFIENBTUFSQQoxMS8yNS8yMDE0LDAsLSw5OC4zNSxQQUdPIElOVEVSQkFOQ0FSSU8gQ1RBQ1RFCjExLzI1LzIwMTQsMjgyLC0sMC4zMCxOL0QgQ09NSVNJT04gQ09CUk9TIElOVEVSQkFOQ0EKMTEvMjUvMjAxNCwwLC0sMTQ5Ljk2LFBBR08gSU5URVJCQU5DQVJJTyBDVEFDVEUKMTEvMjUvMjAxNCwyODIsLSwwLjMwLE4vRCBDT01JU0lPTiBDT0JST1MgSU5URVJCQU5DQQoxMS8yNS8yMDE0LDUyLC0sIjMsMDAwLjAwIixQQUdPIERFIENIRVFVRSBUUkFOU0ZFUklETwoxMS8yNy8yMDE0LDQ4LCssNDQ2Ljg4LExFVkFOVEFNSUVOVE8gQU5VTEFDSU9OIENFUlRJRgoxMS8yOC8yMDE0LDEwMCwtLDY2Ni4zNSxOL0QgRElSLU9SREVOIERFIFBBR08KMTEvMjgvMjAxNCwyNzQsLSwzLjAwLE4vRCBESVItQ09NSVNJT04gLSBPUkRFTiBERSBQQQoxMS8yOC8yMDE0LDEwMCwtLDc1MC4wMCxOL0QgRElSLU9SREVOIERFIFBBR08KMTEvMjgvMjAxNCwyNzQsLSwxLjAwLE4vRCBESVItQ09NSVNJT04gLSBPUkRFTiBERSBQQQoxMS8yOC8yMDE0LDEwMCwtLDIwMC4wMCxOL0QgRElSLU9SREVOIERFIFBBR08KMTEvMjgvMjAxNCwyNzQsLSwxLjAwLE4vRCBESVItQ09NSVNJT04gLSBPUkRFTiBERSBQQQoxMS8yOC8yMDE0LDEwMCwtLDY1MC4wMCxOL0QgRElSLU9SREVOIERFIFBBR08KMTEvMjgvMjAxNCwyNzQsLSwxLjAwLE4vRCBESVItQ09NSVNJT04gLSBPUkRFTiBERSBQQQo=',
                }


#####################################################################################
# A class that represents a manual reconciliation of a bank line. The wizard
# presents a list of possible account.move.line to reconcile the bank line with.
#####################################################################################
class ecua_conciliacion_manual_bank_wizard( osv.osv_memory ):

    _name = 'ecua.conciliacion.manual.bank.wizard'


    #####################################################################################
    # Cancel manual reconciliation
    #####################################################################################
    def cancelar(self, cr, uid, ids, context = None ):
        return


    #####################################################################################
    # Create a new ecua_reconciled_line with the bank line and the selected move line
    #####################################################################################
    def conciliar(self, cr, uid, ids, context = None ):

        # First get the ecua.conciliacion.bancaria object
        this_wizard  = self.pool.get( 'ecua.conciliacion.manual.bank.wizard' ).browse( cr, uid, ids[ 0 ], context=context )
        conciliacion = self.pool.get( 'ecua.conciliacion.bancaria' ).browse( cr, uid, this_wizard.conciliacion_id, context=context )

        # First create a new ecua.reconciled.line with the data in this wizard
        move_date              = move_description = move_ref = move_type = u''
        move_amount            = 0.0
        unrec_move_line_id     = 0
        origin_move_line       = u''
        line_has_been_selected = False

        # Find the selected 'available_move_line'
        if( len( this_wizard.available_move_lines ) < 1 ):
            raise osv.except_osv( 'Error!', 'No hay lineas de asiento con el mismo monto para conciliar' )

        for line in this_wizard.available_move_lines:
            if( line.selected == True ):
                move_date              = line.move_date
                move_description       = line.move_description
                move_ref               = line.move_ref
                move_type              = line.move_type
                move_amount            = line.move_amount
                unrec_move_line_id     = line.unrec_move_line_id
                line_has_been_selected = True
                origin_move_line       = line.origin_move_line
                break

        if( line_has_been_selected == False):
            raise osv.except_osv( 'Error!', 'Debe elegir una linea de asiento para conciliar' )

        # Now create the reconciled line object
        reconciled_line_obj = self.pool.get( 'ecua.reconciled.line' )



        reconciled_line_obj.create( cr,
                                    uid,
                                    { 'conciliacion_id'  : this_wizard.conciliacion_id,
                                      'name'             : 'Reconciled Line',
                                      'bank_date'        : this_wizard.bank_date,
                                      'bank_description' : this_wizard.bank_description,
                                      'bank_ref'         : this_wizard.bank_ref,
                                      'bank_type'        : this_wizard.bank_type,
                                      'bank_amount'      : this_wizard.bank_amount,
                                      'move_date'        : move_date,
                                      'move_description' : move_description,
                                      'move_ref'         : move_ref,
                                      'move_type'        : move_type,
                                      'move_amount'      : move_amount,
                                      'origin_reconcilie': origin_move_line,
                                    },
                                    context = context,
                                  )

        # Now delete the ecua.unreconciled.bank.line that corresponds to this wizard
        self.pool.get( 'ecua.unreconciled.bank.line' ).unlink( cr, uid, this_wizard.unrec_bank_line_id )

        # Also delete the ecua.unreconciled.move.line that corresponds to the selected move line
        if origin_move_line == 'unrec_move_line':
            self.pool.get( 'ecua.unreconciled.move.line' ).unlink( cr, uid, unrec_move_line_id )

        # Also delete the ecua.unreconciled.mamual.move.line that corresponds to the selected move line
        if origin_move_line == 'unrec_manual_move_line':
            self.pool.get( 'ecua.unreconciled.manual.move.line' ).unlink( cr, uid, unrec_move_line_id )

        return { 'type'      : 'ir.actions.act_window',
                 'view_type' : 'form',
                 'view_mode' : 'form,tree',
                 'res_model' : 'ecua.conciliacion.bancaria',
                 'res_id'    : this_wizard.conciliacion_id,
               }


    #####################################################################################
    # Column and default definitions
    #####################################################################################
    _columns = { 'conciliacion_id'      : fields.integer( 'Conciliacion ID' ),
                 'unrec_bank_line_id'   : fields.integer( 'Unreconciled Bank Line ID' ),
                 'selected'             : fields.boolean( 'Selected' ),
                 'bank_date'            : fields.char( 'Fecha' ),
                 'bank_description'     : fields.char( 'Descripcion' ),
                 'bank_ref'             : fields.char( 'Referencia' ),
                 'bank_type'            : fields.selection( [('debit', 'Debito'), ('credit', 'Credito')], string="Tipo" ),
                 'bank_amount'          : fields.float( 'Monto' ),
                 'available_move_lines' : fields.one2many( 'ecua.conciliacion.manual.bank.wizard.line', 'wizard_id', string='Unreconciled Move Lines' ),

               }

    _defaults = { 'selected' : True,
                }


#####################################################################################
# This class represents the available account move lines for the current manual
# reconciliation.
#####################################################################################
class ecua_conciliacion_manual_bank_wizard_line( osv.osv_memory ):

    _name = 'ecua.conciliacion.manual.bank.wizard.line'


    #####################################################################################
    # Mark the line as selected
    #####################################################################################
    def select( self, cr, uid, ids, context = None ):

        line_obj  = self.pool.get( 'ecua.conciliacion.manual.bank.wizard.line' )
        this_line = line_obj.browse( cr, uid, ids[ 0 ], context=context )
        all_lines = this_line.wizard_id.available_move_lines

        # First set the 'selected' field for all the other lines in this wizard to False
        for line in all_lines:
            line_obj.write( cr, uid, [ line.id ], { 'selected' : False } )

        # Now set this line's 'selected' field to True
        line_obj.write( cr, uid, [ this_line.id ], { 'selected' : True } )


        return { 'name'      :  "Nueva Conciliacion",
                 'view_mode' : 'form',
                 'view_type' : 'form',
                 'res_model' : 'ecua.conciliacion.manual.bank.wizard',
                 'res_id'    : this_line.wizard_id.id,
                 'type'      : 'ir.actions.act_window',
                 'target'    : 'new',
                 'context'   : context,
               }


    #####################################################################################
    # Column and default definitions
    #####################################################################################
    _columns = { 'wizard_id'          : fields.many2one( 'ecua.conciliacion.manual.bank.wizard', string='Wizard ID', ondelete='cascade' ),
                 'unrec_move_line_id' : fields.integer( 'Unreconciled Move Line ID' ),
                 'selected'           : fields.boolean( 'Selected' ),
                 'move_date'          : fields.char( 'Fecha' ),
                 'move_description'   : fields.char( 'Descripcion' ),
                 'move_ref'           : fields.char( 'Referencia' ),
                 'move_type'          : fields.selection( [('debit', 'Debito'), ('credit', 'Credito')], string="Tipo" ),
                 'move_amount'        : fields.float( 'Monto' ),
                 'origin_move_line'     : fields.selection([('unrec_move_line', 'Unreconcilied Move Lines'),
                                                            ('unrec_manual_move_line', 'Unreconcilied Manual Move Lines')],
                                                           string='Origen de Reconciliación'),
               }

    _defaults = { 'selected' : False,
                }

#####################################################################################
# Wizard para la conciliacion avanzada
#####################################################################################
class ecua_conciliacion_advance_wizard(osv.osv_memory):
    _name = 'ecua.conciliacion.advance.wizard'

    #####################################################################################
    # Cancel manual reconciliation
    #####################################################################################
    def cancelar(self, cr, uid, ids, context = None ):
        return

    def conciliar(self, cr, uid, ids, context=None):
        this_wizard  = self.pool.get('ecua.conciliacion.advance.wizard').browse(cr, uid, ids[0], context=context)
        conciliacion = self.pool.get( 'ecua.conciliacion.bancaria' ).browse( cr, uid, this_wizard.conciliacion_id, context=context )

        period = conciliacion.period.name

        rec_adv_obj  = self.pool.get('ecua.reconcilied.advance')

        rec_bank_line_obj     = self.pool.get('ecua.reconcilied.advance.bank.line')
        rec_move_line_obj     = self.pool.get('ecua.reconcilied.advance.move.line')
        rec_man_move_line_obj = self.pool.get('ecua.reconcilied.advance.manual.move.line')

        unrec_bank_obj   = self.pool.get( 'ecua.unreconciled.bank.line' )
        unrec_move_obj   = self.pool.get( 'ecua.unreconciled.move.line' )
        unrec_manual_move_obj   = self.pool.get('ecua.unreconciled.manual.move.line')

        if this_wizard.sum_total_bank != this_wizard.sum_total_move:
            raise osv.except_osv( 'Error!', 'No se puede conciliar. La suma de las selecciones de Banco no es igual a la suma de los Movimientos.')

        #crea la conciliacion avanzada
        rec_adv_id = rec_adv_obj.create(cr,
                                        uid,
                                        {
                                            'conciliacion_id' : conciliacion.id,
                                            'name'            : 'Resumen de líneas conciliadas',
                                            'period'          : period,
                                            'total'           : this_wizard.sum_total_bank,
                                        },
                                        context = context,
                                )

        #crea los objetos que serviran de backup para el unreconcile
        for bank_line in this_wizard.bank_lines:
            rec_bank_line_obj.create(cr,
                                      uid,
                                      {
                                        'conciliacion_id'    : conciliacion.id,
                                        'reconciled_advance' : rec_adv_id,
                                        'bank_date'          : bank_line.bank_date,
                                        'bank_description'   : bank_line.bank_description,
                                        'bank_ref'           : bank_line.bank_ref,
                                        'bank_type'          : bank_line.bank_type,
                                        'bank_amount'        : bank_line.bank_amount,
                                      }
                                    )
            unrec_bank_obj.unlink(cr, uid, bank_line.unrec_bank_line_id, context=context)

        for move_line in this_wizard.move_lines:
            rec_move_line_obj.create(cr,
                                      uid,
                                      {
                                        'conciliacion_id'    : conciliacion.id,
                                        'reconciled_advance' : rec_adv_id,
                                        'move_date'          : move_line.move_date,
                                        'move_description'   : move_line.move_description,
                                        'move_ref'           : move_line.move_ref,
                                        'move_type'          : move_line.move_type,
                                        'move_amount'        : move_line.move_amount,
                                      }
                                    )
            unrec_move_obj.unlink(cr, uid, move_line.unrec_move_line_id, context=context)

        for man_move_line in this_wizard.manual_move_lines:
            rec_man_move_line_obj.create(cr,
                                          uid,
                                          {
                                            'conciliacion_id'      : conciliacion.id,
                                            'reconciled_advance'   : rec_adv_id,
                                            'man_move_date'        : man_move_line.man_move_date,
                                            'man_move_description' : man_move_line.man_move_description,
                                            'man_move_ref'         : man_move_line.man_move_ref,
                                            'man_move_type'        : man_move_line.man_move_type,
                                            'man_move_amount'      : man_move_line.man_move_amount,
                                          }
                                    )
            unrec_manual_move_obj.unlink(cr, uid, man_move_line.unrec_manual_move_line_id, context=context)

        return

    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = {'conciliacion_id'      : fields.integer('Conciliacion ID'),
                'sum_total_bank'       : fields.float('Suma Total de Banco'),
                'sum_total_move'       : fields.float('Suma Total de Movimientos'),
                'bank_lines'           : fields.one2many('ecua.conciliacion.advance.bank.wizard.line', 'wizard_id', string='Unreconciled Bank Lines'),
                'move_lines'           : fields.one2many('ecua.conciliacion.advance.move.wizard.line', 'wizard_id', string='Unreconciled Move Lines'),
                'manual_move_lines'    : fields.one2many('ecua.conciliacion.advance.manual.move.wizard.line', 'wizard_id', string='Unreconciled Manual Move Lines'),

               }

#####################################################################################
# Wizard para la líneas de conciliacion avanzada para los unrec bank lines
#####################################################################################
class ecua_conciliacion_advance_bank_wizard_line(osv.osv_memory):
    _name = 'ecua.conciliacion.advance.bank.wizard.line'

    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = {'wizard_id'          : fields.many2one('ecua.conciliacion.advance.wizard', string='Wizard ID', ondelete='cascade'),
                'unrec_bank_line_id' : fields.integer( 'Unreconciled Bank Line ID' ),
                'bank_date'          : fields.char( 'Fecha' ),
                'bank_description'   : fields.char( 'Descripcion' ),
                'bank_ref'           : fields.char( 'Referencia' ),
                'bank_type'          : fields.selection( [('debit', 'Debito'), ('credit', 'Credito')], string="Tipo" ),
                'bank_amount'        : fields.float( 'Monto' ),

               }

#####################################################################################
# Wizard para la líneas de conciliacion avanzada para los unrec move lines
#####################################################################################
class ecua_conciliacion_advance_move_wizard_line(osv.osv_memory):
    _name = 'ecua.conciliacion.advance.move.wizard.line'

    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = {'wizard_id'          : fields.many2one('ecua.conciliacion.advance.wizard', string='Wizard ID', ondelete='cascade'),
                'unrec_move_line_id' : fields.integer( 'Unreconciled Move Line ID' ),
                'move_date'          : fields.char( 'Fecha' ),
                'move_description'   : fields.char( 'Descripcion' ),
                'move_ref'           : fields.char( 'Referencia' ),
                'move_type'          : fields.selection( [('debit', 'Debito'), ('credit', 'Credito')], string="Tipo" ),
                'move_amount'        : fields.float( 'Monto' ),

               }

#####################################################################################
# Wizard para la líneas de conciliacion avanzada para los unrec manual move lines
#####################################################################################
class ecua_conciliacion_advance_manual_move_wizard_line(osv.osv_memory):
    _name = 'ecua.conciliacion.advance.manual.move.wizard.line'

    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = {'wizard_id'                 : fields.many2one('ecua.conciliacion.advance.wizard', string='Wizard ID', ondelete='cascade'),
                'unrec_manual_move_line_id' : fields.integer( 'Unreconciled Manual Move Line ID' ),
                'man_move_date'          : fields.char( 'Fecha' ),
                'man_move_description'   : fields.char( 'Descripcion' ),
                'man_move_ref'           : fields.char( 'Referencia' ),
                'man_move_type'          : fields.selection( [('debit', 'Debito'), ('credit', 'Credito')], string="Tipo" ),
                'man_move_amount'        : fields.float( 'Monto' ),

               }