# -*- coding: utf-8 -*-
##############################################################################
#
#    Authors: Patricio Rangles, Trescloud Cia. Ltda.
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv,fields
from openerp.tools.translate import _
from amount_to_words import amount_to_words_es

#TODO ECUA_CHECK
# 1. clean up UI
#     a. readonly fields
#     b. domain on journals.. (egreso)
#     c. onchange journal populates chequera
#     d. maybe move check number to front page...
# 2. Chequera:
#     a. A journal can be of type Ingreso / Egreso.
#     b. If Egreso, journal has a mandatory chequera Object.
#     c. Chequera object has name, sequence with good default values.
# 3. Can't return to draft state.  Only Anular.
# 4. Voucher number = name on account move.
# 5. Voucher ref? maybe is check number?
# 6. Make sure there is a check for duplicate written checks (same number)

##############################################################################
# Inherited account_journal class definition
##############################################################################
class account_journal(osv.osv):

    _inherit = "account.journal"

    _columns = { 'report_check_id' : fields.many2one('ir.actions.report.xml','Name report', change_default=1, domain="[('model','=','account.voucher')]",
                                                     help='Report to use when printing checks.'),
               }

##############################################################################
# Inherited account_voucher class definition
##############################################################################
class account_voucher(osv.osv):
   
    _inherit = 'account.voucher'

    ##############################################################################
    # Get the next value from a sequence_id
    ##############################################################################
    def get_next_from_sequence(self, cr, uid, sequence_id, test='id', context=None):
        """
        Function to find next sequence number
        """
        seq_pool = self.pool.get('ir.sequence')
        assert test in ('code', 'id')
        company_id = self.pool.get('res.users').read(cr, uid, uid, ['company_id'], context=context)['company_id'][0] or None
        cr.execute('''SELECT id, number_next, prefix, suffix, padding
                      FROM ir_sequence
                      WHERE %s=%%s
                      AND active=true
                      AND (company_id = %%s or company_id is NULL)
                      ORDER BY company_id, id
                      FOR UPDATE NOWAIT''' % test,
                      (sequence_id, company_id))
        res = cr.dictfetchone()
        if res:

            interpolation_dic = seq_pool._interpolation_dict()

            if res['number_next']:
                return (seq_pool._interpolate(res['prefix'], interpolation_dic) + '%%0%sd' % res['padding'] % res['number_next']+ seq_pool._interpolate(res['suffix'], interpolation_dic),
                        res['number_next'])
            else:
                return (seq_pool._interpolate(res['prefix'], interpolation_dic) + seq_pool._interpolate(res['suffix'], interpolation_dic), 0)

        return False

    ##############################################################################
    # button_print() override. Validates voucher before printing
    ##############################################################################
    def button_print(self, cr, uid, ids, context=None):
        self.button_proforma_voucher(cr, uid, ids, context=None)
        return self.print_check(cr, uid, ids, context)

    ##############################################################################
    # Returns a report action for a report that represents a check
    ##############################################################################
    def print_check(self, cr, uid, ids, context=None):

        # re-writing the print_check original function of account_check_writing
        if not ids:
            return  {}

        ac_vouch = self.browse(cr, uid, ids[0], context=context)
        
        if not ac_vouch.journal_id.report_check_id:
            raise osv.except_osv(_('Warning!'), _("The select Journal don't have a report associated!!")) 
        
        return { 'type'        : 'ir.actions.report.xml',
                 'report_name' : ac_vouch.journal_id.report_check_id.report_name,
                 'datas'       : { 'model'       : 'account.voucher',
                                   'id'          : ids and ids[0] or False,
                                   'ids'         : ids and ids or [],
                                   'report_type' : 'pdf'
                                 },
                 'nodestroy'   : True
               }

    ##############################################################################
    # Verify check number
    ##############################################################################
    def _verify_check_number(self, cr, uid, ids, context=None):

        if not context:
            context = {}

        for obj in self.browse(cr, uid, ids, context=context):

            if obj.need_check_info and obj.check_number and self.search(cr, uid, [('id','!=',obj.id),
                                                                                  ('check_number','=',obj.check_number),
                                                                                  ('journal_id','=',obj.journal_id.id),
                                                                                  ('state','!=','draft')], context=context):
                return False
                  
        return True


    ##############################################################################
    # onchange_partner_id() override. Updates 'payee_name' field as well
    ##############################################################################
    def onchange_partner_id(self, cr, uid, ids, partner_id, journal_id, amount, currency_id, ttype, date, context=None):
        """
        Inherit the on_change from account.voucher, add payee_name
        """

        if not context:
            context = {}
            
        default = super(account_voucher, self).onchange_partner_id(cr, uid, ids, partner_id, journal_id, amount, currency_id, ttype, date, context=context)

        if 'value' in default and partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            default['value'].update({'payee_name': partner.name})

        return default

    ##############################################################################
    # onchange_journal_override(). Updates 'need_check_info', 'check_number',
    # and 'number' fields.
    ##############################################################################
    def onchange_journal(self, cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount, ttype, company_id, context=None):
        """
        Inherit the on_change from account.voucher, add allow_check_writing and sequence check
        """

        if not context:
            context = {}

        default = super(account_voucher, self).onchange_journal(cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount, ttype, company_id, context=context)

        if not default:
            default = {}

        if 'value' in default:

            journal_obj = self.pool.get('account.journal')
            journal = None

            allow = False

            if journal_id:
                journal = journal_obj.browse(cr, uid, journal_id, context=context)
                allow = journal.allow_check_writing

            if ttype == 'payment':
                default['value'].update({'need_check_info': allow})

            # is allowed print checks, show the next number to use
            if allow:
                res = self.get_next_from_sequence(cr, uid, journal.sequence_id.id, context=context)
                # return a tuple with the number in char and integer number
                default['value'].update({
                                         'check_number': res[0],
                                         'number': res[0],
                                         })
            else:
                default['value'].update({
                                         'check_number': '',
                                         'number': '',})

        return default

    ##############################################################################
    # onchange_amount() override. Updates 'amount_in_word' field
    ##############################################################################
    def onchange_amount(self, cr, uid, ids, amount, rate, partner_id, journal_id, currency_id, ttype, date, payment_rate_currency_id, company_id, context=None):
        """ Inherited - add amount_in_word in returned value dictionary """
        if not context:
            context = {}
            
        default = super(account_voucher, self).onchange_amount(cr, uid, ids, amount, rate, partner_id, journal_id, currency_id, ttype, date, payment_rate_currency_id, company_id, context=context)
        
        if 'value' in default:
            amount = 'amount' in default['value'] and default['value']['amount'] or amount

            amount_in_word = amount_to_words_es(amount)
            default['value'].update({'amount_in_word': amount_in_word})

        return default

    ##############################################################################
    # proforma_voucher() override. Writes the check_number to the
    # account move's and all account move line's 'ref' field.
    ##############################################################################
    def proforma_voucher(self, cr, uid, ids, context=None):

        for check in self.browse(cr, uid, ids, context=context):
            if check.need_check_info and check.amount_in_word == "con 00/100":
                raise osv.except_osv("Error", "El campo 'Cantidad en Letras' debe estar establecido")

        if super(account_voucher, self).proforma_voucher(cr, uid, ids, context=context):
        
            for voucher in self.browse(cr, uid, ids, context=context):
                if voucher.need_check_info:

                    # write this number in the name, ref of account.move
                    self.pool.get('account.move').write(cr, uid, [voucher.move_id.id,], {'ref': voucher.check_number}, context=context)

                    # write this number in the name, ref of account.move
                    line_obj = self.pool.get('account.move.line')
                    line_list = line_obj.search(cr, uid, [('move_id','=',voucher.move_id.id)], context=context)
                    line_obj.write(cr, uid, line_list, {'ref': voucher.check_number}, context=context)
                    
        return True

    ##############################################################################
    # Get default need_check_info based on context
    ##############################################################################
    def _get_default_need_check_info( self, cr, uid, context=None ):
        if( context and 'write_check' in context and context['write_check'] == True ):
            return True
        return False

    ##############################################################################
    # Column, defaults, and constraints definition
    ##############################################################################
    _columns = { 'payee_name'      : fields.char('Payee Name', size=256, readonly=True, states={'draft':[('readonly',False)]},
                                                 help="This field have the Name that will be printed on the check"),
                 'city_check'      : fields.char('City', size=64, readonly=True, states={'draft':[('readonly',False)]},
                                                 help="This field have the city that will be printed on the check"),
                 'need_check_info' : fields.boolean('Show if need check info'), # new, distinct field of allow_chek
                 'check_number'    : fields.char("Check Number", size=64, readonly=True, states={'draft':[('readonly',False)]},
                                                 help="This is the number of check for internal control"),
                 'create_uid'       : fields.many2one('res.users', 'Usuario de Creacion', readonly=True),
               }

    _defaults = { 'city_check'     : lambda self,cr,uid,context: self.pool.get('res.company').browse(cr, uid, self.pool.get('res.company')._company_default_get(cr, uid, 'account.voucher',context=context), context=context).partner_id.city or "",
                  'need_check_info': _get_default_need_check_info,
                }

    _constraints = [ (_verify_check_number, 'This Number of Check already exist, please change this number', ['check_number']), ]
