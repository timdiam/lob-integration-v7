# -*- coding: utf-8 -*-
##############################################################################
#
#    Author:Patricio Rangles, Trescloud Cia. ltda.
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Ecua Check',
    'version': '1.0',
    'author': 'Trescloud cia. ltda.',
    'category': 'Generic Modules/Accounting',
    'description': """
Module for the Check Writing, Check Printing, Print Report depending the select Journal
=======================================================================================

Author: Patricio Rangles
    """,
    'website': 'http://www.trescloud.com',
    'depends' : [
        'base',
        'account_check_writing',
        'ecua_payment'
                 ],
    'data': [
        #'security/ir.model.access.csv',
        'security/access_data.xml',
        'data/report_data.xml',
        'view/ecua_check_view.xml',
        'view/account_voucher.xml',
            ],
    'demo': [],
    'test': [],
    'installable': True,
    'active': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
