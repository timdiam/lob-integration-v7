######################################################################################################
# Improvements made to various aspects of the Sales modules
#
# Authors: Harry Alvarez, Dan Haggerty
# Date:    9/4/2015
######################################################################################################
import locale
from openerp.osv import fields, osv, orm
import logging
from openerp.tools.translate import _
_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')

######################################################################################################
# Inherited sale order class definition
######################################################################################################
class sale_order(osv.osv):

    _inherit = "sale.order"



class sale_advance_payment_inv(osv.osv_memory):
    _inherit = "sale.advance.payment.inv"

    _columns = {
                'advance_payment_method':  fields.selection(
                                                          [('all', 'Invoice the whole sales order'), ('percentage','Percentage'), ('fixed','Fixed price (deposit)'),
                                                            ('lines', 'Some order lines'),  ('all_separate_invoices',"Facturas Separadas")]),
                'lines_per_invoice':       fields.integer("Lineas / Factura"),
                'number_invoices'  :       fields.integer("Numero de Facturas", readonly="1"),
    }


    def get_number_invoices(self, cr, uid, context={}):
        #Yield successive n-sized chunks from l.
        def chunks(l, n):
            for i in xrange(0, len(l), n):
                yield l[i:i+n]
        sale_id = context.get('active_id', False)
        if sale_id:
            sale_order_lines = self.pool.get('sale.order.line').search(cr,uid,[('order_id','=',sale_id)])
            sale_line_chunks = chunks(sale_order_lines, 10)

            return len(list(sale_line_chunks))

        return 1

    _defaults = {
                'advance_payment_method':  'all_separate_invoices',
                'lines_per_invoice'     :   10,
                'number_invoices'       :   get_number_invoices,
    }

    def onchange_lines_per_invoice(self, cr, uid, ids, lines_per_invoice, context={}):
        #Yield successive n-sized chunks from l.
        def chunks(l, n):
            for i in xrange(0, len(l), n):
                yield l[i:i+n]
        if lines_per_invoice:
            sale_id = context.get('active_id', False)
            if sale_id:
                sale_order_lines = self.pool.get('sale.order.line').search(cr,uid,[('order_id','=',sale_id)])
                sale_line_chunks = chunks(sale_order_lines, lines_per_invoice)

                return {'value':{'number_invoices' : len(list(sale_line_chunks))}}

        return {'value':{'number_invoices':0}}

    def create_invoices(self, cr, uid, ids, context=None):
        """ create invoices for the active sales orders """
        sale_obj = self.pool.get('sale.order')
        act_window = self.pool.get('ir.actions.act_window')
        wizard = self.browse(cr, uid, ids[0], context)
        sale_ids = context.get('active_ids', [])

        if wizard.advance_payment_method == "all_separate_invoices":
            assert len(sale_ids) == 1
            lines_per_invoice = wizard.lines_per_invoice
            sale_order_lines = self.pool.get('sale.order.line').search(cr,uid,[('order_id','=',sale_ids)])

            #Yield successive n-sized chunks from l.
            def chunks(l, n):
                for i in xrange(0, len(l), n):
                    yield l[i:i+n]

            sale_line_chunks = chunks(sale_order_lines, lines_per_invoice)
            line_wiz_obj = self.pool.get('sale.order.line.make.invoice')
            result_ids = []
            for chunk in sale_line_chunks:
                context.update({'active_ids':chunk})
                if context.get('open_invoices', False):
                    result_ids.append(line_wiz_obj.make_invoices(cr,uid,[-1],context)['res_id'])
                else:
                    line_wiz_obj.make_invoices(cr,uid,[-1],context)

            if context.get('open_invoices', False):
                return self.pool.get('sale.order').action_view_invoice(cr, uid, sale_ids, context)
            else:
                return True

        else:
            return super(sale_advance_payment_inv, self).create_invoices(cr,uid,ids,context=context)
