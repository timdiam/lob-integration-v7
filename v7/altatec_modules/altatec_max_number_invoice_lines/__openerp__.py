{
    'name': 'Altatec Max Number of Invoice Lines',
    'version': '1.0',
    'description': """
        This modules adds some features so that we can define the maximum number of lines on an invoice, and thus make multiple invoices from a sale if needed.
    """,
    'author': 'Tim Diamond',
    'website': 'www.altatececuador.com',
    "depends" : [
                'sale',
                'account',
                'purchase',
                ],
    "data" : [
                'views/sale_order_wizard.xml',
             ],
    "installable": True,
    "auto_install": False
}

