# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 Serpent Consulting Services (<http://www.serpentcs.com>)
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from osv import osv, fields
import time
from openerp.tools.translate import _
from trc_mod_python import rounding
from openerp.tools.safe_eval import safe_eval as eval

from datetime import datetime
import pytz

class hr_salary_rule(osv.Model):
    
    _inherit = 'hr.salary.rule'
    
    _columns = {
        'reminder' : fields.boolean('Reminder?')
    }
    
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        args.insert(0, ['reminder', '=', False])
        return super(hr_salary_rule, self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)

    #TODO should add some checks on the type of result (should be float)
    def compute_rule(self, cr, uid, rule_id, localdict, context=None):
        """
        :param rule_id: id of rule to compute
        :param localdict: dictionary containing the environement in which to compute the rule
        :return: returns a tuple build as the base/amount computed, the quantity and the rate
        :rtype: (float, float, float)
        """
        globals_temp = globals()

        if not localdict:
            localdict = {}
        
        for key in globals_temp:
            if key == 'rounding':
                localdict[key] = globals_temp[key]
            
        rule = self.browse(cr, uid, rule_id, context=context)
        if rule.amount_select == 'fix':
            try:
                return rule.amount_fix, eval(rule.quantity, localdict), 100.0
            except:
                raise osv.except_osv(_('Error!'), _('Wrong quantity defined for salary rule %s (%s).')% (rule.name, rule.code))
        elif rule.amount_select == 'percentage':
            try:
                return eval(rule.amount_percentage_base, localdict), eval(rule.quantity, localdict), rule.amount_percentage
            except:
                raise osv.except_osv(_('Error!'), _('Wrong percentage base or quantity defined for salary rule %s (%s).')% (rule.name, rule.code))
        else:
            try:
                eval(rule.amount_python_compute, localdict, mode='exec', nocopy=True)
                return localdict['result'], 'result_qty' in localdict and localdict['result_qty'] or 1.0, 'result_rate' in localdict and localdict['result_rate'] or 100.0
            except:
                raise osv.except_osv(_('Error!'), _('Wrong python code defined for salary rule %s (%s).')% (rule.name, rule.code))

hr_salary_rule()

class hr_salary_reminder(osv.Model):

    _name = 'hr.salary.reminder'
    _inherits = {'hr.salary.rule': 'rule_id'}
    
    _columns = {
                'employee_id' : fields.many2one('hr.employee', 'Employee', required=False),
                'rule_id' : fields.many2one('hr.salary.rule', 'Salary Rule'),
                'processed' : fields.boolean('Processed ?'),
                'rem_date' : fields.date('Date'),
                'invoice_id' : fields.many2one('hr.advance', 'Invoice'),
                }

    def _get_good_date(self,cr,uid,context=None):
        #now_date = datetime.now(pytz.timezone("America/Guayaquil")).strftime("%Y-%m-%d")
        now_date = fields.date.context_today(self, cr,uid,context=context)
        return now_date

    _defaults = {
        'rem_date' : _get_good_date,
        'reminder' : True,
        #'code':_create,
        }

    def write(self, cr, uid, ids, vals, context={}):
        if not context:
            context = {}
        #Can't after creation assign a debit account id!!!
        if 'account_debit' in vals and vals['account_debit']:
            raise osv.except_osv(_('Configuration Error!'),_('Un documento de recordatorio no deberia tener una cuenta de Debito.  Por favor, revise el Diario de Presto / Anticipos '))

        return super(hr_salary_reminder, self).write(cr,uid,ids,vals,context=context)

    def create(self, cr, uid, vals, context=None):
        #if vals.get('name','/')=='/':
        vals['code'] = self.pool.get('ir.sequence').get(cr, uid, 'hr.salary.reminder')

        #WE SHOULD NEVER ALLOW CREATION OF A RECORDATORIO WITH AN ACCOUNT DEBIT... BECAUSE
        #THIS GIVES US ERRORS WHEN WE TRY TO VALIDATE THE PAYROLL
        if 'account_debit' in vals and vals['account_debit']:
            raise osv.except_osv(_('Configuration Error!'),_('Un documento de recordatorio no deberia tener una cuenta de Debito.  Por favor, revise el Diario de Presto / Anticipos '))
        return super(hr_salary_reminder, self).create(cr, uid, vals, context=context)
    
    def copy(self, cr, uid, id, default=None, context=None): 
        if not context:
            context={}
        if not default: default={}
        default.update({'processed': False})
        return super(hr_salary_reminder, self).copy(cr, uid, id, default, context)
    
    def onchange_register(self,cr,uid,ids,register_id,employee_id,context=None):
        v = {}
        if register_id:
            debit_account_id=0
            credit_account_id=0
            contract_type=self.pool.get('hr.contribution.register')
            ctr_type_obj=contract_type.browse(cr,uid,register_id)
            contract_obj = self.pool.get('hr.contract')
            contract_ids = contract_obj.search(cr, uid, [('date_start','<=',time.strftime('%Y-%m-%d %H:%M:%S')),('employee_id','=',employee_id)])
            cont_recs = contract_obj.browse(cr, uid, contract_ids)
            partner_ids = []
            for contract in cont_recs:
                if contract.employee_id.address_home_id:
                    """debe enviar el id de contrato"""
                    partner_ids.append(contract.id)
            if partner_ids:
                contract=contract_obj.browse(cr, uid, partner_ids)[0]
                obj_model_data=self.pool.get("ir.model.data")
                "FABRICACIÓN DIRECTA"
                model_dir=obj_model_data.search(cr,uid,[('name','=','hr_contract_type_mdi')])
                if model_dir:
                    fdir=obj_model_data.browse(cr,uid,model_dir)[0].res_id
                "FABRICACIÓN INDIRECTA"
                model_ind=obj_model_data.search(cr,uid,[('name','=','hr_contract_type_min')])
                if model_ind:
                    find=obj_model_data.browse(cr,uid,model_ind)[0].res_id
                "ADMINISTRATIVO"
                model_adm=obj_model_data.search(cr,uid,[('name','=','hr_contract_type_wrkr')])
                if model_adm:
                    admi=obj_model_data.browse(cr,uid,model_adm)[0].res_id
                "VENTAS"
                model_ven=obj_model_data.search(cr,uid,[('name','=','hr_contract_type_sub')])
                if model_ven:
                    ven=obj_model_data.browse(cr,uid,model_ven)[0].res_id
    
                if contract.type_id.id==fdir:
                    debit_account_id = ctr_type_obj.debit_acc_manu_di.id
                    credit_account_id = ctr_type_obj.credit_acc_manu_di.id
                elif contract.type_id.id==find:
                    debit_account_id = ctr_type_obj.debit_acc_manu_in.id
                    credit_account_id = ctr_type_obj.credit_acc_manu_in.id
                elif contract.type_id.id==admi:
                    debit_account_id = ctr_type_obj.debit_acc_administrative.id
                    credit_account_id = ctr_type_obj.credit_acc_administrative.id
                elif contract.type_id.id==ven:
                    debit_account_id = ctr_type_obj.debit_acc_sales.id
                    credit_account_id = ctr_type_obj.credit_acc_sales.id
                v['account_debit'] = debit_account_id
                v['account_credit']=credit_account_id
            v['category_id']=ctr_type_obj.category_id.id or False
            v['name']=ctr_type_obj.name or False
            v['account_tax_id']=ctr_type_obj.account_tax_id.id or False
        return {'value': v}
    
hr_salary_reminder()


class hr_payslip(osv.Model):
    '''
    Pay Slip
    '''
    _inherit = 'hr.payslip'
    
    def get_payslip_lines(self, cr, uid, contract_ids, payslip_id, context):
        def _sum_salary_rule_category(localdict, category, amount):
            if category.parent_id:
                localdict = _sum_salary_rule_category(localdict, category.parent_id, amount)
            localdict['categories'].dict[category.code] = category.code in localdict['categories'].dict and localdict['categories'].dict[category.code] + amount or amount
            return localdict

        class BrowsableObject(object):
            def __init__(self, pool, cr, uid, employee_id, dict):
                self.pool = pool
                self.cr = cr
                self.uid = uid
                self.employee_id = employee_id
                self.dict = dict

            def __getattr__(self, attr):
                return attr in self.dict and self.dict.__getitem__(attr) or 0.0

        class InputLine(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""
            def sum(self, code, from_date, to_date=None):
                if to_date is None:
                    #to_date = datetime.now().strftime('%Y-%m-%d')
                    now_date = fields.date.context_today(self, cr, uid, context=context)
                    to_date = now_date
                result = 0.0
                self.cr.execute("SELECT sum(amount) as sum\
                            FROM hr_payslip as hp, hr_payslip_input as pi \
                            WHERE hp.employee_id = %s AND hp.state = 'done' \
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pi.payslip_id AND pi.code = %s",
                           (self.employee_id, from_date, to_date, code))
                res = self.cr.fetchone()[0]
                return res or 0.0

        class WorkedDays(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""
            def _sum(self, code, from_date, to_date=None):
                if to_date is None:
                    #to_date = datetime.now().strftime('%Y-%m-%d')
                    now_date = fields.date.context_today(self, cr, uid, context=context)
                    to_date = now_date
                result = 0.0
                self.cr.execute("SELECT sum(number_of_days) as number_of_days, sum(number_of_hours) as number_of_hours\
                            FROM hr_payslip as hp, hr_payslip_worked_days as pi \
                            WHERE hp.employee_id = %s AND hp.state = 'done'\
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pi.payslip_id AND pi.code = %s",
                           (self.employee_id, from_date, to_date, code))
                return self.cr.fetchone()

            def sum(self, code, from_date, to_date=None):
                res = self._sum(code, from_date, to_date)
                return res and res[0] or 0.0

            def sum_hours(self, code, from_date, to_date=None):
                res = self._sum(code, from_date, to_date)
                return res and res[1] or 0.0

        class Payslips(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""

            def sum(self, code, from_date, to_date=None):
                if to_date is None:
                    #to_date = datetime.now().strftime('%Y-%m-%d')
                    now_date = fields.date.context_today(self, cr, uid, context=context)
                    to_date = now_date
                self.cr.execute("SELECT sum(case when hp.credit_note = False then (pl.total) else (-pl.total) end)\
                            FROM hr_payslip as hp, hr_payslip_line as pl \
                            WHERE hp.employee_id = %s AND hp.state = 'done' \
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pl.slip_id AND pl.code = %s",
                            (self.employee_id, from_date, to_date, code))
                res = self.cr.fetchone()
                return res and res[0] or 0.0

        #we keep a dict with the result because a value can be overwritten by another rule with the same code
        result_dict = {}
        rules = {}
        categories_dict = {}
        blacklist = []
        payslip_obj = self.pool.get('hr.payslip')
        inputs_obj = self.pool.get('hr.payslip.worked_days')
        obj_rule = self.pool.get('hr.salary.rule')
        reminder_obj = self.pool.get('hr.salary.reminder')
        obj_decimal=self.pool.get('decimal.precision')
        payslip = payslip_obj.browse(cr, uid, payslip_id, context=context)
        worked_days = {}
        decimals=obj_decimal.search(cr,uid,[('name','=','Payroll')])
        #to_date = datetime.now().strftime('%Y-%m-%d')
        if decimals:
            digit=obj_decimal.browse(cr,uid,decimals)[0].digits
        for worked_days_line in payslip.worked_days_line_ids:
            worked_days[worked_days_line.code] = worked_days_line
        inputs = {}
        for input_line in payslip.input_line_ids:
            inputs[input_line.code] = input_line

        categories_obj = BrowsableObject(self.pool, cr, uid, payslip.employee_id.id, categories_dict)
        input_obj = InputLine(self.pool, cr, uid, payslip.employee_id.id, inputs)
        worked_days_obj = WorkedDays(self.pool, cr, uid, payslip.employee_id.id, worked_days)
        payslip_obj = Payslips(self.pool, cr, uid, payslip.employee_id.id, payslip)
        rules_obj = BrowsableObject(self.pool, cr, uid, payslip.employee_id.id, rules)

        localdict = {'categories': categories_obj, 'rules': rules_obj, 'payslip': payslip_obj, 'worked_days': worked_days_obj, 'inputs': input_obj}
        #get the ids of the structures on the contracts and their parent id as well
        structure_ids = self.pool.get('hr.contract').get_all_structures(cr, uid, contract_ids, context=context)
        #get the rules of the structure and thier children
        rule_ids = self.pool.get('hr.payroll.structure').get_all_rules(cr, uid, structure_ids, context=context)
        #run the rules by sequence
        sorted_rule_ids = [id for id, sequence in sorted(rule_ids, key=lambda x:x[1])]

        for contract in self.pool.get('hr.contract').browse(cr, uid, contract_ids, context=context):
            employee = contract.employee_id
            localdict.update({'employee': employee, 'contract': contract})
            #find reminder Un processed(unpaid) IDS
            '''AQUI BUSCAR POR LA FECHA'''
            reminder_ids = reminder_obj.search(cr, uid, [('employee_id', '=', employee.id), ('processed', '=', False)], context=context)
            if reminder_ids:
                for reminder in reminder_obj.browse(cr, uid, reminder_ids, context=context):
                    if reminder.rem_date <= payslip.date_to and reminder.rem_date>=payslip.date_from:
                    #sorted_rule_ids.append(reminder.rule_id.id)
                        sorted_rule_ids.insert(0,reminder.rule_id.id)
            #Find reminder processed(paid) IDS
#            reminder_ids = reminder_obj.search(cr, uid, [('rule_id', 'in', sorted_rule_ids), ('processed', '=', True)], context=context)
#            if reminder_ids:
#                for reminder in reminder_obj.browse(cr, uid, reminder_ids, context=context):
#                    sorted_rule_ids.remove(reminder.rule_id.id)
            for rule in obj_rule.browse(cr, uid, sorted_rule_ids, context=context):
                key = rule.code + '-' + str(contract.id)
                localdict['result'] = None
                localdict['result_qty'] = 1.0
                #check if the rule can be applied
                if obj_rule.satisfy_condition(cr, uid, rule.id, localdict, context=context) and rule.id not in blacklist:
                    #compute the amount of the rule
                    amount, qty, rate = obj_rule.compute_rule(cr, uid, rule.id, localdict, context=context)
                    #check if there is already a rule computed with that code
                    '''aqui cambio''' 
                    amount=round(amount,digit)
                    previous_amount = rule.code in localdict and localdict[rule.code] or 0.0
                    #set/overwrite the amount computed for this rule in the localdict
                    tot_rule = amount * qty * rate / 100.0
                    localdict[rule.code] = tot_rule
                    rules[rule.code] = rule
                    #sum the amount for its salary category
                    localdict = _sum_salary_rule_category(localdict, rule.category_id, tot_rule - previous_amount)
                    #create/overwrite the rule in the temporary results
                    result_dict[key] = {
                        'salary_rule_id': rule.id,
                        'contract_id': contract.id,
                        'name': rule.name,
                        'code': rule.code,
                        'category_id': rule.category_id.id,
                        'sequence': rule.sequence,
                        'appears_on_payslip': rule.appears_on_payslip,
                        'condition_select': rule.condition_select,
                        'condition_python': rule.condition_python,
                        'condition_range': rule.condition_range,
                        'condition_range_min': rule.condition_range_min,
                        'condition_range_max': rule.condition_range_max,
                        'amount_select': rule.amount_select,
                        'amount_fix': rule.amount_fix,
                        'amount_python_compute': rule.amount_python_compute,
                        'amount_percentage': rule.amount_percentage,
                        'amount_percentage_base': rule.amount_percentage_base,
                        'register_id': rule.register_id.id,
                        'amount': amount,
                        'employee_id': contract.employee_id.id,
                        'quantity': qty,
                        'rate': rate,
                    }
                else:
                    #blacklist this rule and its children
                    blacklist += [id for id, seq in self.pool.get('hr.salary.rule')._recursive_search_of_rules(cr, uid, [rule], context=context)]

        result = [value for code, value in result_dict.items()]
        return result
    
    def process_sheet(self, cr, uid, ids, context=None):
        move_pool = self.pool.get('account.move')
        period_pool = self.pool.get('account.period')
        reminder_obj = self.pool.get('hr.salary.reminder')

        #now_date = datetime.now(pytz.timezone("America/Guayaquil")).strftime("%Y-%m-%d")
        now_date = fields.date.context_today(self, cr,uid,context=context)
        timenow = now_date

        for slip in self.browse(cr, uid, ids, context=context):           
            #make reminder as processed
            salery_rule_ids = []
            for line in slip.line_ids:
                salery_rule_ids.append(line.salary_rule_id.id)
            reminder_ids = reminder_obj.search(cr, uid, [('rule_id', 'in', salery_rule_ids)], context=context)
            if reminder_ids:
                reminder_obj.write(cr, uid, reminder_ids, {'processed' : True}, context=context)
            
            line_ids = []
            debit_sum = 0.0
            credit_sum = 0.0
            if not slip.period_id:
                ctx = dict(context or {}, account_period_prefer_normal=True)
                search_periods = period_pool.find(cr, uid, slip.date_to, context=ctx)
                period_id = search_periods[0]
            else:
                period_id = slip.period_id.id

            default_partner_id = slip.employee_id.address_home_id.id
            name = _('Payslip of %s') % (slip.employee_id.name)
            move = {
                'narration': name,
                'date': slip.date_to,
                'ref': slip.number,
                'journal_id': slip.journal_id.id,
                'period_id': period_id,
            }
            for line in slip.details_by_salary_rule_category:
                #valores iniciales
                analytics_id = None #la distribucion de centros de costos a utilizar
                debit_account_id=False

                amt = slip.credit_note and -line.total or line.total
#                partner_id = line.salary_rule_id.register_id.partner_id and line.salary_rule_id.register_id.partner_id.id or default_partner_id
                debit_partner_id = line.salary_rule_id.register_id.debitor_partner_id and line.salary_rule_id.register_id.debitor_partner_id.id or default_partner_id
                credit_partner_id = line.salary_rule_id.register_id.creditor_partner_id and line.salary_rule_id.register_id.creditor_partner_id.id or default_partner_id
               #AQUI ANDREA MODIFICO ACCOUNT_DEBIT
                if line.salary_rule_id.condition_acc:
                    obj_model_data=self.pool.get("ir.model.data")
                    "FABRICACIÓN DIRECTA"
                    model_dir=obj_model_data.search(cr,uid,[('name','=','hr_contract_type_mdi')])
                    if model_dir:
                        fdir=obj_model_data.browse(cr,uid,model_dir)[0].res_id
                    "FABRICACIÓN INDIRECTA"
                    model_ind=obj_model_data.search(cr,uid,[('name','=','hr_contract_type_min')])
                    if model_ind:
                        find=obj_model_data.browse(cr,uid,model_ind)[0].res_id
                    "ADMINISTRATIVO"
                    model_adm=obj_model_data.search(cr,uid,[('name','=','hr_contract_type_wrkr')])
                    if model_adm:
                        admi=obj_model_data.browse(cr,uid,model_adm)[0].res_id
                    "VENTAS"
                    model_ven=obj_model_data.search(cr,uid,[('name','=','hr_contract_type_sub')])
                    if model_ven:
                        ven=obj_model_data.browse(cr,uid,model_ven)[0].res_id
                    if slip.contract_id.type_id.id==fdir:
                        debit_account_id = line.salary_rule_id.debit_acc_manu_di.id
                    elif slip.contract_id.type_id.id==find:
                        debit_account_id = line.salary_rule_id.debit_acc_manu_in.id
                    elif slip.contract_id.type_id.id==admi:
                        debit_account_id = line.salary_rule_id.debit_acc_administrative.id
                    elif slip.contract_id.type_id.id==ven:
                        debit_account_id = line.salary_rule_id.debit_acc_sales.id
                    else:
                        raise osv.except_osv(_('Configuration Error!'),_('The Contract do not have the correct type of Contract, you must to choose between Direct Manufacturing,Indirect Manufacturing, Administrative or Sales'))
                else:        
                    debit_account_id = line.salary_rule_id.account_debit.id
                
                credit_account_id = line.salary_rule_id.account_credit.id   

                if debit_account_id:
                    #si la cuenta es de ingresos o egresos enviamos al centro de costos
                    analytics_id = None
                    if line.salary_rule_id.account_debit.user_type.code in ['expense','income']:
                        analytics_id = slip.analytics_id.id or None
                    
                    debit_line = (0, 0, {
                    'name': line.name,
                    'date': timenow,
#                    'partner_id': (line.salary_rule_id.register_id.partner_id or line.salary_rule_id.account_debit.type in ('receivable', 'payable')) and partner_id or False,
                    'partner_id' : debit_partner_id or False,
                    'account_id': debit_account_id,
                    'journal_id': slip.journal_id.id,
                    'period_id': period_id,
                    'debit': amt > 0.0 and amt or 0.0,
                    'credit': amt < 0.0 and -amt or 0.0,
                    'analytics_id': analytics_id,
                    'analytic_account_id': line.salary_rule_id.analytic_account_id and line.salary_rule_id.analytic_account_id.id or False,
                    'tax_code_id': line.salary_rule_id.account_tax_id and line.salary_rule_id.account_tax_id.id or False,
                    'tax_amount': line.salary_rule_id.account_tax_id and amt or 0.0,
                })
                    line_ids.append(debit_line)
                    debit_sum += debit_line[2]['debit'] - debit_line[2]['credit']

                if credit_account_id:
                    #si la cuenta es de ingresos o egresos enviamos al centro de costos
                    analytics_id = None
                    if line.salary_rule_id.account_credit.user_type.code in ['expense','income']:
                        analytics_id = slip.analytics_id.id or None
                        
                    credit_line = (0, 0, {
                    'name': line.name,
                    'date': timenow,
#                    'partner_id': (line.salary_rule_id.register_id.partner_id or line.salary_rule_id.account_credit.type in ('receivable', 'payable')) and partner_id or False,
                    'partner_id' : credit_partner_id or False,
                    'account_id': credit_account_id,
                    'journal_id': slip.journal_id.id,
                    'period_id': period_id,
                    'debit': amt < 0.0 and -amt or 0.0,
                    'credit': amt > 0.0 and amt or 0.0,
                    'analytics_id': analytics_id,
                    'analytic_account_id': line.salary_rule_id.analytic_account_id and line.salary_rule_id.analytic_account_id.id or False,
                    'tax_code_id': line.salary_rule_id.account_tax_id and line.salary_rule_id.account_tax_id.id or False,
                    'tax_amount': line.salary_rule_id.account_tax_id and amt or 0.0,
                })
                    line_ids.append(credit_line)
                    credit_sum += credit_line[2]['credit'] - credit_line[2]['debit']

            ##################################################################
            # Altatec adds simple check at end of payroll computation
            #################################################################3
            if abs(debit_sum - credit_sum) >= .001:
                raise osv.except_osv(_('Configuration Error!'),_('Existe un descuadre en el asiento contable de nómina, por favor revise las cuentas contables de los recordatorios y de las reglas salariales.  Monto del descuadre: '+str(debit_sum-credit_sum)))

            ##################################################################
            # REMOVED AS THIS SHOULD NEVER HAPPEN.  IF IT DOES, SYSTEM SHOULD THROW ERROR
            #################################################################3
            # if str(debit_sum) > str(credit_sum):
            #     acc_id = slip.journal_id.default_credit_account_id.id
            #     if not acc_id:
            #         raise osv.except_osv(_('Configuration Error!'),_('Existe un descuadre en el asiento contable de nómina, por favor revise las cuentas contables de los recordatorios y de las reglas salariales.'))
            #         #raise osv.except_osv(_('Configuration Error!'),_('The Expense Journal "%s" has not properly configured the Credit Account!')%(slip.journal_id.name))
            #     adjust_credit = (0, 0, {
            #         'name': _('Adjustment Entry'),
            #         'date': timenow,
            #         'partner_id': False,
            #         'account_id': acc_id,
            #         'journal_id': slip.journal_id.id,
            #         'period_id': period_id,
            #         'debit': 0.0,
            #         'credit': debit_sum - credit_sum,
            #     })
            #     line_ids.append(adjust_credit)


            ##################################################################
            # REMOVED AS THIS SHOULD NEVER HAPPEN.  IF IT DOES, SYSTEM SHOULD THROW ERROR
            #################################################################3
            # #asi estaba antes elif debit_sum < credit_sum:
            # elif str(debit_sum) < str(credit_sum):
            #     acc_id = slip.journal_id.default_debit_account_id.id
            #     if not acc_id:
            #         raise osv.except_osv(_('Configuration Error!'),_('Existe un descuadre en el asiento contable de nómina, por favor revise las cuentas contables de los recordatorios y de las reglas salariales.'))
            #         #raise osv.except_osv(_('Configuration Error!'),_('The Expense Journal "%s" has not properly configured the Debit Account!')%(slip.journal_id.name))
            #     adjust_debit = (0, 0, {
            #         'name': _('Adjustment Entry'),
            #         'date': timenow,
            #         'partner_id': False,
            #         'account_id': acc_id,
            #         'journal_id': slip.journal_id.id,
            #         'period_id': period_id,
            #         'debit': credit_sum - debit_sum,
            #         'credit': 0.0,
            #     })
            #     line_ids.append(adjust_debit)

            move.update({'line_id': line_ids})
            move_id = move_pool.create(cr, uid, move, context=context)
            self.write(cr, uid, [slip.id], {'move_id': move_id, 'period_id' : period_id}, context=context)
            if slip.journal_id.entry_posted:
                move_pool.post(cr, uid, [move_id], context=context)
        return self.write(cr, uid, ids, {'paid': True, 'state': 'done'}, context=context)
    
    def cancel_sheet(self, cr, uid, ids, context=None):
        res = super(hr_payslip, self).cancel_sheet(cr, uid, ids, context=context)
        reminder_obj = self.pool.get('hr.salary.reminder')
        for payslip in self.browse(cr, uid, ids, context=context):
            salery_rule_ids = []
            for line in payslip.line_ids:
                salery_rule_ids.append(line.salary_rule_id.id)
            reminder_ids = reminder_obj.search(cr, uid, [('rule_id', 'in', salery_rule_ids)], context=context)
            if reminder_ids:
                reminder_obj.write(cr, uid, reminder_ids, {'processed' : False}, context=context)
        return res
    
    '''CODGIO A MEJORAR'''
    def _check_rule_per_employee(self, cr, uid, ids, context=None): 
        if not context:
            context={}
            arg=[]
        for slip in self.browse(cr, uid, ids, context):
            slip_ids=self.search(cr, uid, [('date_from','<=',slip.date_from),('date_to','>=',slip.date_from),('employee_id','=',slip.employee_id.id),('id','!=',slip.id)])
            if slip_ids:
                raise osv.except_osv(_(u'Información!'),_("El empleado %s ya tiene registrado %s dentro de las fecha indicadas:(inicio %s fin %s), corrija por favor") % (self.pool.get('hr.employee').name_get(cr, uid, [slip.employee_id.id,])[0][1], slip.employee_id.name, slip.date_from, slip.date_to))
                return False
        return True
    
    _constraints = [(_check_rule_per_employee, _('Error: El empleado ya tiene registrado nomina en las fechas indicadas'), ['line_ids']), ]

hr_payslip()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
