import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree
import openerp.addons.decimal_precision as dp
import openerp.exceptions

from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp import tools
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging
import xml.etree.cElementTree as ET

_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL, 'en_US.utf8')


class document_note_type(osv.osv):

    _name="document.note.type"

    _columns={
                "name":fields.char("Nombre"),
                "codigo":fields.char("Codigo"),
                "documento":fields.one2many("document.note","type", string="Documento"),
    }

class document_note(osv.osv):

    def create(self, cr, uid, vals, context={}):
        return 0

    _name="document.note"

    _columns={
                "name":fields.char(string="Nombre"),
                "notas":fields.text("Notas"),
                "type":fields.many2one("document.note.type", string="Tipo"),
                "fecha_creacion":fields.date("Fecha Creacion"),
                "fecha_actual":fields.date("Fecha Actual", readonly="1"),
                "res_id":fields.integer("Id Objeto", readonly="1"),
                "objeto":fields.many2one("ir.model", string="Objeto"),
                "usuario":fields.many2one("res.users", string="Usuario", readonly="1"),
    }


class add_information_wizard(osv.osv_memory):

    def create(self, cr, uid, vals, context={}):
        return 0

    def create_document_note(self, cr, uid, ids, context=None):
        records=self.browse(cr,uid,ids)[0]
        documnet_note_id= self.pool.get('document.note').create(cr,uid,{
                                                                        "name":records.name,
                                                                        "notas":records.notas,
                                                                        "type":records.type.id,
                                                                        "res_id":records.res_id,
                                                                        "objeto":records.objeto.id,
                                                                        "usuario":records.usuario.id,
                                                                        })

        return records

    def get_res_id(self, cr, uid, context):
        if 'active_id' in context:
            return context['active_id']
        else:
            return None

    def get_ir_model(self, cr, uid, context):
        if 'active_model' in context:
            objecto=self.pool.get('ir.model').search(cr,uid,[("model","=",context['active_model'])])[0]
            return objecto
        else:
            return None

    def _get_user(self, cr, uid, context):
        return self.pool.get('res.users').browse(cr, uid, uid, context).id

    _name="add.information.wizard"

    _columns={

                "name":fields.char("Nombre"),
                "notas":fields.text("Notas"),
                "type":fields.many2one("document.note.type", string="Tipo"),
                "fecha_creacion":fields.date("Fecha Creacion"),
                "fecha_actual":fields.date("Fecha Actual", readonly="1"),
                "res_id":fields.integer("Id Objeto", readonly="1"),
                "objeto":fields.many2one("ir.model", string="Objeto"),
                "usuario":fields.many2one("res.users", string="Usuario", readonly="1"),
    }

    _defaults={

                "res_id":get_res_id,
                'usuario':_get_user,
                "fecha_actual":time.strftime('%Y-%m-%d'),
                "objeto":get_ir_model,
    }