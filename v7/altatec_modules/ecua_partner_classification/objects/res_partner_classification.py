# -*- coding: utf-8 -*-
#################################################################################
#
# This module adds a new model to be added as fields to res.partner that are
# relevant to Grupo Sovi
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    9/4/2015
#
#################################################################################
from openerp.osv import fields, osv, orm

#################################################################################
# res.partner.classification class definition
#################################################################################
class res_partner_classification(orm.Model):

    _name = 'res.partner.classification'

    _columns = { "name"         : fields.char( "Nombre", required=True ),
                 "partner_type" : fields.selection( string="Cliente/Proveedor", selection=[( "client", "Cliente" ),( "supplier", "Proveedor" )], required=True),
                 "category"     : fields.selection( string="Categoria", selection=[ ( "type"          , "Tipo"          ),
                                                                                    ( "qualification" , "Calificacion"  ),
                                                                                    ( "classification", "Clasificacion" ),
                                                                                    ( "state"         , "Estado"        ),
                                                                                    ( "segment"       , "Segmentacion"  ),
                                                                                  ], required=True ),
                 "notas":fields.char("Rango Promedio Ingresos")
               }