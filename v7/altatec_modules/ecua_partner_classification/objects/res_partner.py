# -*- coding: utf-8 -*-
#################################################################################
#
# This module adds new selection fields to res.partner that are relevant to Grupo Sovi
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    10/2/2015
#
#################################################################################
from openerp.osv import fields, osv, orm


#################################################################################
# Inherited res_partner class definition
#################################################################################
class res_partner(orm.Model):

    _inherit = 'res.partner'

    _columns = { 'tipo_cliente'            : fields.many2one( "res.partner.classification", "Tipo",          domain=[("partner_type","=","client"),("category","=","type")]),
                 'calificacion_cliente'    : fields.many2one( "res.partner.classification", "Calificacion",  domain=[("partner_type","=","client"),("category","=","qualification")] ),
                 'clasificacion_cliente'   : fields.many2one( "res.partner.classification", "Clasificacion", domain=[("partner_type","=","client"),("category","=","classification")] ),
                 'estado_cliente'          : fields.many2one( "res.partner.classification", "Estado",        domain=[("partner_type","=","client"),("category","=","state")] ),
                 'segmentacion_cliente'    : fields.many2one( "res.partner.classification", "Segmentacion",  domain=[("partner_type","=","client"),("category","=","segment")] ),

                 'tipo_proveedor'          : fields.many2one( "res.partner.classification", "Tipo",          domain=[("partner_type","=","supplier"),("category","=","type")]),
                 'calificacion_proveedor'  : fields.many2one( "res.partner.classification", "Calificacion",  domain=[("partner_type","=","supplier"),("category","=","qualification")] ),
                 'clasificacion_proveedor' : fields.many2one( "res.partner.classification", "Clasificacion", domain=[("partner_type","=","supplier"),("category","=","classification")] ),
                 'estado_proveedor'        : fields.many2one( "res.partner.classification", "Estado",        domain=[("partner_type","=","supplier"),("category","=","state")] ),
                 'segmentacion_proveedor'  : fields.many2one( "res.partner.classification", "Segmentacion",  domain=[("partner_type","=","supplier"),("category","=","segment")] ),
               }