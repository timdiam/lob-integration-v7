{ 'name'         : 'Ecua Partner Classification',
  'version'      : '1.0',
  'description'  : """
                   This module adds new selection fields to res.partner that are relevant to Grupo Sovi.
                   """,
  'author'       : 'Dan Haggerty',
  'website'      : 'www.altatececuador.com',
  "depends"      : [ 'base',
                     'web_m2x_options',
                   ],
  "data"         : [ 'views/res_partner.xml',
                     'views/res_partner_classification.xml',
                     'security/ir.model.access.csv',
                   ],
  "installable"  : True,
  "auto_install" : False,
}