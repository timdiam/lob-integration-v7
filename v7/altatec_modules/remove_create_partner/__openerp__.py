# -*- coding: utf-8 -*-

{
    "name": 'Remove Create Partner',
    "version": "0.1",
    "description": """
    
    Remove Create Partner from sales, purchase, invoices, and voucher menus.

""",
    "depends": [
        'base',
        'web',
        'web_m2x_options',
        'account_voucher',
        'account',
        'sale',
        'purchase',
        'stock',
    ],
    "data": [
        'remove_create_partner.xml',
    ],
    "author": "AltaERP",
    "installable": True,
}
