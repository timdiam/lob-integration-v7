# -*- coding: utf-8 -*-
##############################################################################
#
# This module contains the model for a "purchase.container" object. It represents
# a shipping container to be imported.
#
# Author:   Dan Haggerty
# Date:     Feb. 17th, 2015
#
##############################################################################
from openerp.osv import fields, osv

##############################################################################
# Class definition for purchase_container
##############################################################################
class purchase_container(osv.osv):

    _name = 'purchase.container'

    _columns = { 'name' : fields.char( "Nombre" ),
               }


##############################################################################
# Class definition for purchase_container_category
##############################################################################
class purchase_container_category(osv.osv):

    _name = 'purchase.container.category'

    _columns = { 'name'      : fields.char( "Nombre" ),
                 'code_name' : fields.char( "Nombre Codigo" ),
               }
