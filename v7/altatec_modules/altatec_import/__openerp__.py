{ 'name'         : 'AltaTec Import',
  'version'      : '1.0',
  'description'  : """
                   AltaTec Import Module
                   """,
  'author'       : 'Dan Haggerty',
  'website'      : 'www.altatec.ec',
  "depends"      : [ 'base',
                   ],
  "data"         : [ 'views/purchase_container.xml',
                     'security/ir.model.access.csv',
                   ],
  "installable"  : True,
  "auto_install" : False
}
