{
    'name': 'Recursos_campos',
    'version': '1.0',
    'description': """
        Recursos Addons
    """,
    'author': 'Harry Alvarez',
    'website': 'www.google.com',
    "depends" : ['hr','hr_contract','ecua_hr','hr_extra_input_output','web_m2x_options'],
    "data" : [
                'views/rrhh_fields_view.xml',
                'views/hr_contract_view.xml',
                'views/shift_register_wizard.xml',
                'security/ir.model.access.csv',
                'data/contract.xml',

                ],
    "installable": True,
    "auto_install": False
}
