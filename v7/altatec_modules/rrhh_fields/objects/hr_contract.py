
from openerp.osv import fields, osv, orm

import logging

_ref_vat = {
    'at': 'ATU12345675',
    'be': 'BE0477472701',
    'bg': 'BG1234567892',
    'ch': 'CHE-123.456.788 TVA or CH TVA 123456', #Swiss by Yannick Vaucher @ Camptocamp
    'cy': 'CY12345678F',
    'cz': 'CZ12345679',
    'de': 'DE123456788',
    'dk': 'DK12345674',
    'ee': 'EE123456780',
    'el': 'EL12345670',
    'es': 'ESA12345674',
    'fi': 'FI12345671',
    'fr': 'FR32123456789',
    'gb': 'GB123456782',
    'gr': 'GR12345670',
    'hu': 'HU12345676',
    'hr': 'HR01234567896', # Croatia, contributed by Milan Tribuson
    'ie': 'IE1234567FA',
    'it': 'IT12345670017',
    'lt': 'LT123456715',
    'lu': 'LU12345613',
    'lv': 'LV41234567891',
    'mt': 'MT12345634',
    'mx': 'MXABC123456T1B',
    'nl': 'NL123456782B90',
    'no': 'NO123456785',
    'pl': 'PL1234567883',
    'pt': 'PT123456789',
    'ro': 'RO1234567897',
    'se': 'SE123456789701',
    'si': 'SI12345679',
    'sk': 'SK0012345675',
    }

class codigo_sectorial(osv.osv):
    _name = "codigo.sectorial"


    _columns={
        "name":fields.char("Nombre"),
        "codigo":fields.char("Codigo"),
        "sueldo":fields.float("Sueldo"),
        }


class hr_contract(osv.osv):
    _inherit = "hr.contract"

    def create(self, cr, uid, values, context=None):
        res=super(hr_contract, self).create(cr, uid, values, context)
        sueldo=values["wage"]
        cod_sect = ''
        cargo_name = ''
        date = fields.date.context_today(self, cr, uid, context=context)

        if 'job_id' in values:
            cargo = self.pool.get('hr.job').browse(cr, uid, values['job_id'], context=context)

            if cargo:
                cargo_name = cargo.name

        if 'codigo_sectorial' in values:
            cod_sect_obj = self.pool.get('codigo.sectorial').browse(cr, uid, values['codigo_sectorial'], context=context)
            cod_sect=""
            if cod_sect_obj:
                cc = cod_sect_obj.codigo if cod_sect_obj.codigo else ""
                cc2 = cod_sect_obj.name if cod_sect_obj.name else ""
                cod_sect = cc + '-' + cc2

        #for c in cargo:
        #    name=c.name
        self.pool.get('ascenso.line').create(cr, uid, {
            'fecha': date,
            #'cargo_actual':name,
            'sueldo':sueldo,
            'sueldo_anterior':0,
            'motivo':"Sueldo Inicial",
            "empleado":values['employee_id']
        })

        self.pool.get('entrada.salida.historial.line').create(cr, uid, {
            'fecha': date,
            'status' : 'Activo',
            'cargo' : cargo_name,
            'cod_sectorial' : cod_sect,
            'motivo' : 'Contratacion',
            'empleado' : values['employee_id']

        })

        return res



    def write(self, cr, uid, ids, values, context = None):
        if 'date' in values:
            date = values['date']
        else:
            date = fields.date.context_today(self, cr, uid, context=context)

        for r in self.browse(cr,uid,ids, context=context):
            sueldo1=r.wage

            res = super(hr_contract, self).write(cr, uid, [r.id], values, context = context)

            #If a employee is promoted with a new job and salary
            if ('job_id' in values) and ('wage' in values):
                new_job = self.pool.get('hr.job').browse(cr, uid, values['job_id'], context=context)
                if new_job:
                    new_job_name = new_job.name

                sueldo2=values["wage"]
                old_job = r.job_id.name

                if abs(sueldo1 - sueldo2) > .001:
                    self.pool.get('ascenso.line').create(cr,uid,{
                        'fecha': date,
                        'cargo_anterior' : old_job,
                        'cargo_actual' : new_job_name,
                        'sueldo':float(sueldo2),
                        'sueldo_anterior':float(sueldo1),
                        'motivo':"Ascenso de puesto",
                        "empleado":r.employee_id.id
                    })
            #If a employee has new salary
            elif 'wage' in values:
                sueldo2=values["wage"]
                if abs(sueldo1 - sueldo2) > .001:
                    self.pool.get('ascenso.line').create(cr,uid,{
                        'fecha': date,
                        'sueldo':float(sueldo2),
                        'sueldo_anterior':float(sueldo1),
                        'motivo':"Aumento de Sueldo",
                        "empleado":r.employee_id.id
                    })

            #If employee's contract is inactivated
            if not values.get('active', True):
                cod_sect = ''
                if r.codigo_sectorial:
                    cc = r.codigo_sectorial.codigo if r.codigo_sectorial.codigo else ""
                    cc2 = r.codigo_sectorial.name if r.codigo_sectorial.name else ""
                    cod_sect = cc + '-' + cc2

                self.pool.get('entrada.salida.historial.line').create(cr,uid,{
                    'fecha': date,
                    'status' : 'Inactivo',
                    'cargo' : r.job_id.name,
                    'cod_sectorial' : cod_sect,
                    'motivo' : 'Cambia a estado INACTIVO',
                    'empleado' : r.employee_id.id,

                })

        return res

    _columns={
        "codigo_sectorial":fields.many2one("codigo.sectorial", string="Codigo Sectorial"),
        "contrato_sri":fields.many2one("hr.contract.sri", string="Nuevo tipo de contrato"),
        'active': fields.boolean('Activo'),
    }


    _defaults= {
        'active':True,
    }
class hr_contract_sri(osv.osv):
    _name="hr.contract.sri"

    _columns={
        "name":fields.char("Clasificacion de contrato"),
        "concepto":fields.text("Concepto"),
        "codigo_de_trabajo":fields.char("Codigo de trabajo")
}
