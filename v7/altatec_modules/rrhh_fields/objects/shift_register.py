from openerp.osv import fields, osv, orm
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import datetime


class shift_register_wizard(osv.osv):
    _name = 'shift.register.wizard'

    def shift_register(self, cr, uid, ids, context=None):
        wizard = self.browse( cr, uid, ids )[ 0 ]

        if wizard.shift_type == 'salary':
            salary = wizard.new_salary
            #Get active and correct contract
            contract_id = self.pool.get('hr.employee')._get_correct_contract(cr, uid, context['active_id'], wizard.date)
            if contract_id:
                #Update new salary
                self.pool.get('hr.contract').write(cr, uid, [contract_id], { 'wage' : salary, 'date': wizard.date}, context)
            else:
                raise orm.except_orm( 'Error', "El empleado no cuenta con un contrato asignado a la fecha." )


        if wizard.shift_type == 'promotion':
            contract_id = self.pool.get('hr.employee')._get_correct_contract(cr, uid, context['active_id'], wizard.date)
            if contract_id:
                #Update promotion
                self.pool.get('hr.contract').write(cr, uid, [contract_id], { 'wage' : wizard.new_salary,
                                                                             'date': wizard.date,
                                                                             'job_id': wizard.new_job.id }, context)
            else:
                raise orm.except_orm( 'Error', "El empleado no cuenta con un contrato asignado a la fecha." )

        if wizard.shift_type == 'deactivate':
            #Deactivate all employee's contract
            self.pool.get('hr.employee').deactivate_employee_contracts(cr, uid, context['active_id'], context)
            #Deactivate employee
            self.pool.get('hr.employee').write(cr, uid, context['active_id'], {'active': False})

        return 0

    def onchange_promotion(self, cr, uid, ids, shift_type, context):
        if shift_type == 'promotion':
            #Today because I need a date to get actual contract
            today = datetime.datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)
            contract_id = self.pool.get('hr.employee')._get_correct_contract(cr, uid, context['active_id'], today)
            contract_data = self.pool.get('hr.contract').browse(cr, uid, contract_id, context)
            if contract_id:
                return {'value': {'old_job': contract_data.job_id.name, 'old_salary' : contract_data.wage}}

        return {}

    _columns={
        'shift_type': fields.selection([('salary', 'Nuevo Sueldo'),
                                        ('promotion', 'Nuevo Ascenso'),
                                        ('deactivate', 'Inactivar')], 'Tipo de cambio'),
        'new_salary': fields.float('Nuevo Sueldo'),
        'old_salary': fields.char('Sueldo Actual'),
        'old_job'   : fields.char('Cargo Actual'),
        'new_job'   : fields.many2one('hr.job', 'Nuevo Cargo'),
        'date'      : fields.date('Fecha de Cambio'),
    }

    _defaults = {
        'date' : datetime.datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT),
    }
