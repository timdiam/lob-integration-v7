import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree
import openerp.addons.decimal_precision as dp
import openerp.exceptions

from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp import tools
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging
import xml.etree.cElementTree as ET

_ref_vat = {
    'at': 'ATU12345675',
    'be': 'BE0477472701',
    'bg': 'BG1234567892',
    'ch': 'CHE-123.456.788 TVA or CH TVA 123456', #Swiss by Yannick Vaucher @ Camptocamp
    'cy': 'CY12345678F',
    'cz': 'CZ12345679',
    'de': 'DE123456788',
    'dk': 'DK12345674',
    'ee': 'EE123456780',
    'el': 'EL12345670',
    'es': 'ESA12345674',
    'fi': 'FI12345671',
    'fr': 'FR32123456789',
    'gb': 'GB123456782',
    'gr': 'GR12345670',
    'hu': 'HU12345676',
    'hr': 'HR01234567896', # Croatia, contributed by Milan Tribuson
    'ie': 'IE1234567FA',
    'it': 'IT12345670017',
    'lt': 'LT123456715',
    'lu': 'LU12345613',
    'lv': 'LV41234567891',
    'mt': 'MT12345634',
    'mx': 'MXABC123456T1B',
    'nl': 'NL123456782B90',
    'no': 'NO123456785',
    'pl': 'PL1234567883',
    'pt': 'PT123456789',
    'ro': 'RO1234567897',
    'se': 'SE123456789701',
    'si': 'SI12345679',
    'sk': 'SK0012345675',
    }

class hr_payslip(osv.osv):
    _inherit = "hr.payslip"

    def onchange_employee_id(self, cr, uid, ids, date_from, date_to, employee_id=False, contract_id=False, context=None):
        res= super(hr_payslip,self).onchange_employee_id(cr, uid, ids, date_from, date_to, employee_id, contract_id, context)
        contratos = self.pool.get('hr.contract').search(cr,uid,[('employee_id','=',employee_id), ('active','=',True)])

        if(employee_id):
            emp = self.pool.get('hr.employee').browse(cr,uid,employee_id,context=context)

            if(len(contratos) == 0):
                raise osv.except_osv( _( 'ERROR' ), _( 'El empleado: '+emp.name+' no tiene ningun contrato activo' ) )
            elif(len(contratos) > 1):
                raise osv.except_osv( _( 'ERROR' ), _( 'El empleado: '+emp.name+' tienen varios contratos activos' ) )
            else:
                res['value'].update({'contract_id':contratos[0]})
        return res


class hijos_info_line(osv.osv):
    _name = "hijos.info.line"


    def get_edad_hijos(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}
        #TODO FIX BAD SPACING AND DECIDE IF THIS SHOULD USE TIMEZONES
        for r in records:
            edad1 = datetime.now()

            if(r.fecha_nacimiento):
                edad2 = datetime.strptime(r.fecha_nacimiento, "%Y-%m-%d")
                edad = int((datetime.now() - datetime.strptime(r.fecha_nacimiento, "%Y-%m-%d")).days / 365)
                result[r.id] = edad
            else:
                result[r.id] = 0

        return result

    _columns = {
        "apellidos_nombres": fields.char(string="Apellidos/nombres"),
        "cedula": fields.char(string="Cedula"),
        "fecha_nacimiento": fields.date("Fecha de Nacimiento"),
        "edad_hijos": fields.function(get_edad_hijos, string="Edad", type='integer'),
        "discapacidad": fields.boolean("Tiene discapacidad?"),
        "dis_grado":fields.char("Grado de discapacidad"),
        "dis_tipo":fields.char("Tipo de discapacidad"),
        "empleado":fields.many2one("hr.employee", "Empleado"),
        }

class entrada_salida_historial_line(osv.osv):
    _name= "entrada.salida.historial.line"

    _columns ={

        "fecha":fields.date("Fecha"),
        "status":fields.char("Status"),
        "cargo":fields.char("Cargo"),
        "cod_sectorial":fields.char("Codigo Sectorial"),
        "motivo":fields.char("Motivo"),
        "empleado":fields.many2one("hr.employee", "Empleado"),

        }

class observaciones_rrhh(osv.osv):
    _name= "observaciones.rrhh"

    _columns ={
        "fecha":fields.date("Fecha"),
        "notas":fields.char("Notas"),
        "empleado":fields.many2one("hr.employee", "Empleado"),
        }

class ascenso_line(osv.osv):
    _name= "ascenso.line"

    _columns ={

        "fecha":fields.date("Fecha"),
        "cargo_anterior":fields.char("Cargo anterior"),
        "cargo_actual":fields.char("Cargo actual"),
        "sueldo":fields.float("Sueldo Actual"),
        "sueldo_anterior":fields.float("Sueldo Anterior"),
        "motivo":fields.char("Motivo"),
        "empleado":fields.many2one("hr.employee", "Empleado"),

        }


class bank_ec(osv.osv):
    _name = "bank.ec"

    _columns={
        "name":fields.char("Nombre"),

        }

class hr_employee(osv.osv):
    _inherit = "hr.employee"


    def onchange_address_home_id(self, cr, uid, address, context=None):
        partner=context
        proveedor=self.pool.get('res.partner').browse(cr,uid,partner)
        cedula=""
        if address:
            if proveedor.vat:
                for w in proveedor.vat:
                    if (w.isalpha())==False:
                        cedula+=w
        return {'value':{'identification_id':cedula}}



    def get_edad_empleado(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)[0]
        result = {}
        edad1 = datetime.now()
        if(records.birthday):
            edad = int((datetime.now() - datetime.strptime(records.birthday, "%Y-%m-%d")).days / 365)
            result[records.id] = edad
        else:
            result[records.id] = 0

        return result

    #Inactivate all employee's contract.
    def deactivate_employee_contracts(self, cr, uid, employee_id, context=None):
        obj_contract = self.pool.get('hr.contract')
        contract_ids = obj_contract.search(cr, uid, [('employee_id', '=', employee_id),], order='date_start')
        if contract_ids:
            obj_contract.write(cr, uid, contract_ids, {'active': False})
            return True

        return False


    _columns = {

        'marital': fields.selection([('single', 'Soltero(a)'), ('married', 'Casado(a)'), ('widower', 'Viudo(a)'), ('divorced', 'Divorciado(a)'),('cohabitation','Union Libre')], 'Marital Status'),
        'vat_subjected': fields.boolean('VAT Legal Statement', help="Check this box if the partner is subjected to the VAT. It will be used for the VAT legal statement."),
        "fecha_de_ingreso": fields.date(string="Fecha de ingreso"),
        "nivel_educativo": fields.char("Nivel Educativo"),
        "nacionalidad": fields.many2one("res.country", string="Nacionalidad"),
        "tele_personal_movil": fields.char(string="Telefono personal movil"),
        "tele_personal_fijo": fields.char("Telefono personal fijo"),
        "tipo_sangre": fields.selection([('o_nega', 'O-'), ('o_posi', 'O+'), ('a_nega', "A-"),
                                         ('a_posi', "A+"), ('b_nega', "B-"), ('b_posi', "B+"),
                                         ('ab_nega', "AB-"), ('ab_posi', "AB+")], string="Tipo de Sangre"),

        "nombre_conyuge": fields.char("Nombre Conyuge"),
        "cedula_conyuge":fields.char("Cedula de Conyuge"),
        "numero_hijos":fields.boolean("Tiene hijos?"),
        "hijos_info_line": fields.one2many('hijos.info.line', 'empleado', string='Informacion de Hijos'),
        "edad": fields.function(get_edad_empleado, string="Edad", type='integer' ,required=True),
        "nombre1":fields.char("Nombre:"),
        "nombre2":fields.char("Nombre:"),
        "parentesco1":fields.char("Parentesco:"),
        "parentesco2":fields.char("Parentesco:"),
        "telef1":fields.char("Telefono:"),
        "telef2":fields.char("Telefono:"),
        "entrada_salida_historial_lines":fields.one2many('entrada.salida.historial.line', 'empleado', string="Datos"),
        "ascenso_line":fields.one2many('ascenso.line', 'empleado', string="Datos"),
        "observaciones_rrhh":fields.one2many('observaciones.rrhh', 'empleado', string="Datos"),
        "discapacidad":fields.boolean("Tiene Dispacacidad"),
        "dis_grado":fields.char("Grado de discapacidad"),
        "dis_tipo":fields.char("Tipo de discapacidad"),

        "licencia":fields.boolean("Licencia"),
        "cate_licencia":fields.char("Categoria de Licencia"),
        "num_licencia":fields.char("Numero de Licencia"),

        "servicio_militar":fields.boolean("Servicio Militar"),
        "num_servicio":fields.char("Numero de Servicio Militar"),
        "blanco":fields.char("blanco"),

        "pasaporte":fields.boolean("Pasaporte"),
        "num_pasaporte":fields.char("Numero de Pasaporte"),
        "codigo_empleado":fields.char(string="Codigo del Empleado"),
        "numero_cuenta":fields.char("Numero de Cuenta"),
        "tipo_cuenta":fields.selection([('ahorro', 'Ahorro'), ('corriente', 'Corriente'),], string="Tipo de Cuenta"),
        "institucion_bancaria":fields.char("Institucion Bancaria"),
        "banco":fields.many2one('bank.ec', string="Banco"),
        "otro_id":fields.char("Otro ID"),

        }

    _defaults={
        "numero_hijos":False,

        }
