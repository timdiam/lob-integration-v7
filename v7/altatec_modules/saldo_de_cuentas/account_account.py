# -*- coding: utf-8 -*-
##############################################################################
#  test push
#
#    Copyright (C) 2013 Agile Business Group sagl
#    (<http://www.agilebg.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, orm, osv
from openerp.tools.translate import _
import xmlrpclib
import json
import logging
from datetime import date
import time
from datetime import datetime

_logger = logging.getLogger(__name__)


class account_saldo_wizard(osv.osv_memory):

    _name='account.saldo.wizard'

    _columns = {
            'account_id':fields.many2one('account.account',string="Cuenta",required=True),
            'date_from':fields.date("Desde",required=True),
            'date_to':fields.date("Hasta",required=True),
            'level':fields.related('account_id','level',type='integer',string="level"),
            'result':fields.text('Saldos'),
        }
    _defaults ={
        'date_from':date(date.today().year, 1, 1).strftime('%Y-%m-%d'),
        'date_to':fields.date.context_today,
    }


    def generate_saldo_report(self, cr, uid, ids, context=None):

        records=self.browse(cr,uid,ids)[0]

        cross_company_obj = self.pool.get( 'cross.company' )
        company_ids       = cross_company_obj.search( cr, uid, [], context=context          )
        companies         = cross_company_obj.browse( cr, uid, company_ids, context=context )

        dbnames = []
        names     = []
        urls      = []
        usernames = []
        pwds      = []

        for company in companies:
            dbnames.append( company.dbname )
            names.append( company.name )
            urls.append( company.url )
            usernames.append( company.username )
            pwds.append( company.password )

        report = {}

        for i in range(len(dbnames)):

            db   = dbnames[i]
            url  = urls[i]
            name = names[i]
            user = usernames[i]
            pwd  = pwds[i]

            try:
                sock_common = xmlrpclib.ServerProxy( url + '/xmlrpc/common')
                user_id = sock_common.login(db,user,pwd)
                sock = xmlrpclib.ServerProxy(url + '/xmlrpc/object')
            except Exception:
                raise osv.except_osv(_('Error!'),
                                     _('No se puede conectar al sistema ' + name))

            try:
                cuentas = sock.execute( db, user_id, pwd, 'account.account', 'search', [('id','child_of',[records.account_id.id])])
            except Exception:
                raise osv.except_osv(_('Error!'),
                                     _('No se puede conectar al sistema ' + name))

            if records.account_id.id in cuentas:
                cuentas.remove(records.account_id.id)

            probando=len(cuentas)

            if len(cuentas)<=0:
                raise osv.except_osv(_('Accion Invalida!'),
                                     _('No existen subcuentas'))

            i=0

            for c_id in cuentas:

                fields = [ 'name' , 'balance', 'credit', 'debit' ]

                try:
                    if (records.date_from and records.date_to):
                        context.update({'date_from':records.date_from,'date_to':records.date_to})
                    context.update({'state':u'posted'})
                    data = sock.execute( db, user_id, pwd, 'account.account', 'read', c_id, fields, context )
                except Exception:
                    raise osv.except_osv(_('Error!'),
                                     _('No se puede conectar al sistema ' + name))

                name_cuenta= data[ 'name' ],

                if i==0:
                    report[name]={
                                    name_cuenta[0]:{
                                                    'credit' : data[  'credit' ],
                                                    'debit'   : data[   'debit' ],
                                                    'balance' : data[ 'balance' ],
                                                }

                                }

                else:
                    report[name][name_cuenta[0]]={
                                                'credit' : data[  'credit' ],
                                                'debit'   : data[   'debit' ],
                                                'balance' : data[ 'balance' ],
                                                }


                i=i+1

        x=report

        self.write( cr, uid, records.id, { 'result' : json.dumps( report ) } )
        result = { 'type'        : 'ir.actions.report.xml',
                   'context'     : context,
                   'report_name' : 'account_saldo_wizard',
                   'datas'       : { 'ids' : ids }
                 }
        
        return result
