from report import report_sxw
import json
import logging
_logger = logging.getLogger(__name__)
from datetime import datetime
import pytz


class Parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_saldos':self.get_saldos,
            'get_current_date':self.get_current_date,
            'get_current_time':self.get_current_time,
            'get_date_from':self.get_date_from,
            'get_date_to':self.get_date_to,
            'cr':cr,
            'uid':uid,
            'date_from':context['date_from'],
            'date_to':context['date_to'],
        })

    def get_date_from(self):
        return self.localcontext['date_from']

    def get_date_to(self):
        return self.localcontext['date_to']


    def get_current_date(self):
        return datetime.now(pytz.timezone("America/Guayaquil")).strftime("%Y-%m-%d")

    def get_current_time(self):
        return datetime.now(pytz.timezone("America/Guayaquil")).strftime( "%H:%M" )

    def get_saldos(self,objs):
        for obj in objs:
            data_list = []
            total_credit = 0
            total_debit = 0
            total_saldo = 0
            total_cuenta = 0
            x = json.loads(obj.result)

            for empresas,banco  in x.items():
                for cuenta_b,datos in banco.items():
                    if( abs( datos[ 'balance' ] ) >= 0.01 ):
                        data_list.append({'company':empresas,'cuenta':cuenta_b,'credit':datos['credit'],'debit':datos['debit'],'saldo':datos['balance']})
                        total_credit += datos['credit']
                        total_debit += datos['debit']
                        total_saldo += datos['balance']

            data_list.append({'company':'Total','cuenta':' de Cuentas','credit':total_credit,'debit':total_debit,'saldo':total_saldo})


        return data_list