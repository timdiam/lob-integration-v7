{ 'name'         : 'AltaTec Units',
  'version'      : '1.0',
  'description'  : """
                   This module adds measurement units to the products module
                   """,
  'author'       : 'Dan Haggerty, Henry Lomas, Juan Romero',
  'website'      : 'www.altatec.ec',
  "depends"      : [ 'product',
                   ],
  "data"         : [ 'views/product_product.xml',
                     'views/manage_units_wizard.xml',
                   ],
  "installable"  : True,
  "auto_install" : False
}
