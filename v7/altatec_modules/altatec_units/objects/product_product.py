# -*- coding: utf-8 -*-
########################################################################
#
########################################################################
from osv import fields,osv

########################################################################
#
########################################################################
class product_product(osv.osv):

    _inherit = 'product.product'

    ########################################################################
    #
    ########################################################################
    def manage_units(self, cr, uid, ids, context ):

        product = self.browse( cr, uid, ids[0], context )

        wizard_obj = self.pool.get('manage.units.wizard')

        simple_lines = []
        for line in product.product_unit_line_simple:
            simple_lines.append( ( 0, 0, { 'base_uom'         : line.base_uom.id,
                                           'is_purchase_unit' : line.is_purchase_unit,
                                           'is_stock_unit'    : line.is_stock_unit,
                                           'is_sale_unit'     : line.is_sale_unit,
                                         }
                                 )
                               )

        complex_lines = []
        for line in product.product_unit_line_complex:
            complex_lines.append( ( 0, 0, { 'name'             : line.name,
                                            'quantity'         : line.quantity,
                                            'base_unit'        : line.base_unit,
                                            'is_purchase_unit' : line.is_purchase_unit,
                                            'is_stock_unit'    : line.is_stock_unit,
                                            'is_sale_unit'     : line.is_sale_unit,
                                          }
                                  )
                                )

        # Create the wizard object
        wizard_id = wizard_obj.create( cr, uid,
                                       { 'unit_definition_mode'      : product.unit_definition_mode,
                                         'unit_category'             : product.unit_category.id,
                                         'product_unit_line_simple'  : simple_lines,
                                         'product_unit_line_complex' : complex_lines,
                                       }, context=context
                                     )

        # Return this wizard
        return { 'type'      : 'ir.actions.act_window',
                 'name'      : 'Configurar Unidades',
                 'view_type' : 'form',
                 'view_mode' : 'form,tree',
                 'res_model' : 'manage.units.wizard',
                 'res_id'    : wizard_id,
                 'target'    : 'new',
               }

    ########################################################################
    #
    ########################################################################
    def _get_default_unit_line_simple(self, cr, uid, context=None ):
        return [ ( 0, 0, { 'base_uom'         : 1, #TODO Find a better way to look up the "Units" product.uom
                           'is_purchase_unit' : True,
                           'is_stock_unit'    : True,
                           'is_sale_unit'     : True,
                           'product'          : False,
                         }
                 )
               ]

    ########################################################################
    #
    ########################################################################
    def _get_default_unit_line_complex(self, cr, uid, context=None ):
        return [ ( 0, 0, { 'name'             : "Unidades",
                           'quantity'         : 1,
                           'base_unit'        : "",
                           'is_purchase_unit' : True,
                           'is_stock_unit'    : True,
                           'is_sale_unit'     : True,
                         }
                 )
               ]

    ########################################################################
    # Column and default definitions
    ########################################################################
    _columns = { 'unit_definition_mode'      : fields.selection( [("simple","Sencillo"),("complex","Complejo")], "Modo de definicion", required=True ),
                 'unit_category'             : fields.many2one( "product.uom.categ", "Categoria de unidad" ),
                 'product_unit_line_simple'  : fields.one2many("product.unit.line.simple",'product','Lineas de unidad'),
                 'product_unit_line_complex' : fields.one2many("product.unit.line.complex",'product','Lineas de unidad'),
               }

    _defaults = { 'unit_definition_mode' : "simple",
                  'unit_category' : 1, #TODO Find a better way to look up the "Units" product.uom
                  'product_unit_line_simple' : _get_default_unit_line_simple,
                  'product_unit_line_complex' : _get_default_unit_line_complex,
                }

########################################################################
#
########################################################################
class product_unit_line_simple(osv.osv):

    _name = 'product.unit.line.simple'

    _columns = { 'base_uom'         : fields.many2one( "product.uom", "Unidad" ),
                 'is_purchase_unit' : fields.boolean("Unidad de compras"),
                 'is_stock_unit'    : fields.boolean("Unidad de stock"),
                 'is_sale_unit'     : fields.boolean("Unidad de ventas"),
                 'product'          : fields.many2one('product.product','Producto')
               }

########################################################################
#
########################################################################
class product_unit_line_complex(osv.osv):

    _name = 'product.unit.line.complex'

    _columns = { 'name'             : fields.char("Nombre"),
                 'quantity'         : fields.integer("Cantidad"),
                 'base_unit'        : fields.char("Unidad base"),
                 'is_purchase_unit' : fields.boolean("Unidad de compras"),
                 'is_stock_unit'    : fields.boolean("Unidad de stock"),
                 'is_sale_unit'     : fields.boolean("Unidad de ventas"),
                 'product'          : fields.many2one('product.product','Producto')
               }

