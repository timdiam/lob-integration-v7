# -*- coding: utf-8 -*-
########################################################################
#
########################################################################
from osv import fields,osv

########################################################################
#
########################################################################
class manage_units_wizard(osv.osv_memory):

    _name = 'manage.units.wizard'

    ########################################################################
    #
    ########################################################################
    def save(self, cr, uid, ids, context=None):

        if not context:
            context = {}

        if 'active_ids' not in context or len(context['active_ids']) != 1:
            raise osv.except_osv( "Error!", "Active ID for product not found. Contact AltaTec." )

        for wizard in self.browse( cr, uid, ids, context=context ):

            product = self.pool.get('product.product').browse( cr, uid, context['active_ids'][0], context=context)

            simple_line_obj = self.pool.get('product.unit.line.simple')
            complex_line_obj = self.pool.get('product.unit.line.complex')

            # Delete the existing unit lines from the product
            old_simple_line_ids = simple_line_obj.search(cr, uid, [("product","=",product.id)], context=context)
            simple_line_obj.unlink(cr, uid, old_simple_line_ids, context=context)

            old_complex_line_ids = complex_line_obj.search(cr, uid, [("product","=",product.id)], context=context)
            complex_line_obj.unlink(cr, uid, old_complex_line_ids, context=context)

            # Write the definition mode and unit category (not really necessary for complex mode) from the wizard to the product
            product.write( { 'unit_definition_mode':wizard.unit_definition_mode, 'unit_category':wizard.unit_category.id } )

            # Write the unit lines from the wizard to the product
            simple_lines = []
            for line in wizard.product_unit_line_simple:
                simple_lines.append( ( 0, 0, { 'base_uom'         : line.base_uom.id,
                                               'is_purchase_unit' : line.is_purchase_unit,
                                               'is_stock_unit'    : line.is_stock_unit,
                                               'is_sale_unit'     : line.is_sale_unit,
                                             }
                                     )
                                   )
            product.write({'product_unit_line_simple' : simple_lines})

            complex_lines = []
            for line in wizard.product_unit_line_complex:
                complex_lines.append( ( 0, 0, { 'name'             : line.name,
                                                'quantity'         : line.quantity,
                                                'base_unit'        : line.base_unit,
                                                'is_purchase_unit' : line.is_purchase_unit,
                                                'is_stock_unit'    : line.is_stock_unit,
                                                'is_sale_unit'     : line.is_sale_unit,
                                              }
                                      )
                                    )
            product.write({'product_unit_line_complex' : complex_lines})


    ########################################################################
    #
    ########################################################################
    _columns = { 'unit_definition_mode'      : fields.selection( [("simple","Sencillo"),("complex","Complejo")], "Modo de definicion", required=True ),
                 'unit_category'             : fields.many2one( "product.uom.categ", "Categoria de unidad" ),
                 'product_unit_line_simple'  : fields.one2many("product.unit.wizard.line.simple",'wizard','Lineas de unidad'),
                 'product_unit_line_complex' : fields.one2many("product.unit.wizard.line.complex",'wizard','Lineas de unidad'),
               }

    _defaults = { 'unit_definition_mode' : "simple",
                  'unit_category' : 1,
                }

########################################################################
#
########################################################################
class product_unit_wizard_line_simple(osv.osv_memory):

    _name = 'product.unit.wizard.line.simple'

    _columns = { 'base_uom'         : fields.many2one( "product.uom", "Unidad", required=True),
                 'is_purchase_unit' : fields.boolean("Unidad de compras"),
                 'is_stock_unit'    : fields.boolean("Unidad de stock"),
                 'is_sale_unit'     : fields.boolean("Unidad de ventas"),
                 'wizard'           : fields.many2one('manage.units.wizard','Wizard'),
               }

########################################################################
#
########################################################################
class product_unit_wizard_line_complex(osv.osv_memory):

    _name = 'product.unit.wizard.line.complex'

    _columns = { 'name'             : fields.char("Nombre", required=True),
                 'quantity'         : fields.integer("Cantidad"),
                 'base_unit'        : fields.char("Unidad base"),
                 'is_purchase_unit' : fields.boolean("Unidad de compras"),
                 'is_stock_unit'    : fields.boolean("Unidad de stock"),
                 'is_sale_unit'     : fields.boolean("Unidad de ventas"),
                 'wizard'           : fields.many2one('manage.units.wizard','Wizard'),
               }










