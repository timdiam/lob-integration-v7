from osv import fields, osv
#import logging
#_logger = logging.getLogger(__name__)

class purchase_requisition(osv.osv):
    _inherit = 'purchase.requisition'

    def generate_report(self, cr, uid, ids, context=None):
        context = context or {}
        return { 'type'        : 'ir.actions.report.xml',
                 'context'     : context,
                 'report_name' : 'purchase.comparison.analysis',
                 'datas'       : { 'ids' : ids }
               }
