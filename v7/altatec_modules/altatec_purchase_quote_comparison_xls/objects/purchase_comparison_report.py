# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    
#    Copyright (c) 2013 Noviat nv/sa (www.noviat.be). All rights reserved.
# 
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import xlwt
from report_xls.report_xls import report_xls
from report_xls.utils import rowcol_to_cell
from altatec_purchase_quote_comparison_xls.report.parser import purchase_analyis_parser
from tools.translate import _
import logging
import base64
from PIL import Image

_logger = logging.getLogger(__name__)

class purchase_comparison_report(report_xls):
    
    def generate_xls_report(self, _p, _xs, data, objects, wb):
        company_ruc = objects.company_id.vat
        company_name = objects.company_id.name
        company_logo = objects.company_id.logo #Obtener el logo
        #logo = base64.b64decode(company_logo[0])#decodificar el logo
        #filename = 'company_logo.jpg'
        #crear imagen del logo
        #with open(filename, 'wb') as f:
        #    f.write(logo)

        #convertir el logo de .jpg a .bmp (bitmap)
        #Image.open('company_logo.jpg').convert("RGB").save('company_logo.bmp')
        #im = Image.open('company_logo.bmp')
        #resize la image
        #size = 150, 150
        #im.thumbnail(size, Image.ANTIALIAS)
        #im.save('company_logo.bmp')


        report_name = objects[0]._description or objects[0]._name
        ws = wb.add_sheet("Comparacion Presupuestos")

        #STYLE DEFINITIONS
        # Here we define some useful styles for the sheet.  At any point in time, we can grab one of these to use
        cell_format = _xs['borders_all']
        cell_format_2 = "borders: top thin, bottom thin, left thin, right thin;"
        cell_format_3 = "borders: top thin;"
        cell_style = xlwt.easyxf(cell_format_2)
        cell_style_2 = xlwt.easyxf(cell_format)
        cell_style_3 = xlwt.easyxf(cell_format_3)
        cell_style_center = xlwt.easyxf(cell_format_2 + _xs['center'])
        #cell_style_date = xlwt.easyxf(cell_format + _xs['left'], num_format_str = report_xls.date_format)
        cell_style_date = xlwt.easyxf(cell_format_2 + _xs['center'], num_format_str = report_xls.date_format)
        cell_style_decimal = xlwt.easyxf(cell_format_2 + _xs['right'], num_format_str = report_xls.decimal_format)
        cell_style_bold = xlwt.easyxf(cell_format_2 + _xs['bold'])
        cell_style_bold_2 = xlwt.easyxf(cell_format + _xs['bold'])
        cell_style_wrap_center_bold = xlwt.easyxf('font: height 180, name Arial, colour_index black, bold on, italic on; align: wrap on, vert centre, horiz centre;' + cell_format_2)
        cell_style_center_bold = xlwt.easyxf(cell_format_2 + _xs['center'] + 'font: bold on')
        cell_style_center_bold_2 = xlwt.easyxf(cell_format + _xs['center'] + 'font: bold on')
        cell_style_decimal_centered = xlwt.easyxf(cell_format + _xs['center'], num_format_str = report_xls.decimal_format)
        cell_style_decimal_right = xlwt.easyxf(cell_format_2 + _xs['right'], num_format_str = report_xls.decimal_format)

        #Starting at row 0
        row_pos = 0


        #Utility function.  The write row function needs a unique name for each column each time a row is written
        #We define here a function that will generate a unique id each time we call it.  It is a simple incrementer.
        self.auto_gen_id = 0
        def auto_name():
            self.auto_gen_id = self.auto_gen_id + 1
            return self.auto_gen_id

        #Encabezado
        #ws.write_merge(0,3,0,1)
        #agregar logo a reporte
        #ws.insert_bitmap('company_logo.bmp',0,0)
        c_specs = [ (auto_name(), 3, 0, 'text', '', None,cell_style_2),
                    (auto_name(), 2, 0, 'text', company_name[0], None,cell_style_wrap_center_bold ),
                    ]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold)

        c_specs = [ (auto_name(), 3, 0, 'text', '', None,cell_style_2),
                    (auto_name(), 2, 0, 'text', company_ruc[0], None, cell_style_wrap_center_bold ),
                    ]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold)

        c_specs = [ (auto_name(), 3, 0, 'text', '', None,cell_style_2),
                    (auto_name(), 2, 0, 'text', 'SOLICITUD DE COMPRA COMPARATIVO', None, cell_style_wrap_center_bold ),
                    ]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold)

        c_specs = [ (auto_name(), 1, 0, 'text', '',None,cell_style_2),]

        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold)

        #Loop through each solicitud (should only have one)
        #TODO decide what to do if we are sent more than one solicitud
        for solicitud in objects:
            quote_dict = {}

            #Datos de solicitud
            c_specs = [ (auto_name(), 2, 0, 'text', "Solicitud #", None, cell_style_bold),
                        (auto_name(), 1, 0, 'text', solicitud.name, None, cell_style_center),
                        (auto_name(), 1, 0, 'text', "Fecha Pedido", None, cell_style_bold),
                        (auto_name(), 1, 0, 'text', solicitud.date_start, None, cell_style_date),
                    ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold)

            if solicitud.date_end:
                date_end = solicitud.date_end
            else:
                date_end = ""

            c_specs = [ (auto_name(), 2, 0, 'text', "Dept. Solicitante", None, cell_style_bold),
                        (auto_name(), 1, 0, 'text', solicitud.department.name, None, cell_style_center),
                        (auto_name(), 1, 0, 'text', "Fecha Limite", None, cell_style_bold),
                        (auto_name(), 1, 0, 'text', date_end, None, cell_style_date),
                    ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold)

            c_specs = [ (auto_name(), 2, 0, 'text', "Destino", None, cell_style_bold),
                        (auto_name(), 1, 0, 'text', solicitud.warehouse_id.name, None, cell_style_center),
                        (auto_name(), 1, 0, 'text', "Orden Produccion", None, cell_style_bold),
                        (auto_name(), 1, 0, 'text', solicitud.origin, None, cell_style_date),
                    ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold)

            row_data = self.xls_row_template([], [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold)

            row_data = self.xls_row_template([], [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold)

            c_specs = [ (auto_name(), 1, 0, 'text', "Cant.", None, cell_style_wrap_center_bold),
                        (auto_name(), 1, 0, 'text', "Und. Med.", None, cell_style_wrap_center_bold),
                        (auto_name(), 3, 0, 'text', "Descripcion", None, cell_style_wrap_center_bold),
                        ]
            for quote in solicitud.purchase_ids:

                quote_dict[quote.partner_id.id] = 0

                c_specs += [
                    (auto_name(), 2, 0, 'text', quote.partner_id.name, None, cell_style_wrap_center_bold),
                ]


            #We always use two lines to define a row write.  The format of c_specs is the following:
            #FORMAT:  List of tuples (each tuple is one column / merged set up columns making up the row
            #FORMAT TUPLES:
            #     (unique_id, cells_to_occupy, force_column_width, type, value, formula, style)
            # unique_id: unique_id required for our wrapper class.  Can use utility function auto_name()
            # cells_to_occupy: if greater than one, written cell will occupy the n neighboring cells (merged)
            # force_column_width: here we can specify the column width.  It is normally however, better to leave this until the end.
            # type: can be text, number, date, more???
            # value: value for the cell
            # formula:  Can assign the cell a formula, rather than a value
            # style:  A style object (normally we define these at the top, ready to be used whenever
            # The tuple can be length 4, 5, or 6, depending if we are defining a style/formula

            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_wrap_center_bold)


            #This is how we can force the height of a row, if the default does not suit us
            #For whatever reason, we have to turn the height_mismatch flag on, and then set the height.
            #ws is our sheet object.
            ws.row(9).height_mismatch = True
            ws.row(9).height = 256*4

            c_specs = [ (auto_name(), 5, 0, 'text', ""),
                        ]

            for quote in solicitud.purchase_ids:
                c_specs += [
                                (auto_name(), 1, 0, 'text', "P. Unit", None, cell_style_center_bold),
                                (auto_name(), 1, 0, 'text', "P. Tot", None, cell_style_center_bold),
                            ]

            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold)

            for sol_line in solicitud.line_ids:
                c_specs = []

                c_specs += [
                    (auto_name(), 1, 0, 'number', sol_line.product_qty, None, cell_style_center),
                    (auto_name(),1,0,'text',sol_line.product_uom_id.name, None, cell_style_center),
                    (auto_name(),3,0,'text',sol_line.product_id.name, None, cell_style),
                ]
                flag = False
                for quote in solicitud.purchase_ids:
                    product_found_flag = False
                    for p_line in quote.order_line:
                        if p_line.product_id.id == sol_line.product_id.id:
                            quote_dict[quote.partner_id.id] = quote_dict[quote.partner_id.id] + p_line.price_subtotal
                            product_found_flag = True
                            flag = True
                            c_specs += [
                                        (auto_name(),1,0,'number',p_line.price_unit, None, cell_style_decimal),
                                        (auto_name(),1,0,'number',p_line.price_subtotal, None, cell_style_decimal),
                                ]
                            break
                    if(not product_found_flag):
                        c_specs += [
                            (auto_name(),1,0,'text',""),
                            (auto_name(),1,0,'text',''),
                            ]



                if flag:
                    row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
                    row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style)


            c_specs = [
                        (auto_name(), 5, 0, 'text', "",None,cell_style),
                    ]

            for quote in solicitud.purchase_ids:
                c_specs += [
                        (auto_name(), 1, 0, 'text', "Sub tot", None, cell_style_bold),
                        (auto_name(), 1, 0, 'number', quote_dict[quote.partner_id.id], None, cell_style_decimal_right),
                    ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_center_bold)

            c_specs = [
                        (auto_name(), 5, 0, 'text', "",None,cell_style_2),
                    ]

            for quote in solicitud.purchase_ids:
                c_specs += [
                        (auto_name(), 1, 0, 'text', "IVA 12%", None, cell_style_bold),
                        (auto_name(), 1, 0, 'number', quote_dict[quote.partner_id.id] * .12, None, cell_style_decimal_right),
                    ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_center_bold)

            c_specs = [
                        (auto_name(), 5, 0, 'text', "",None, cell_style_2),
                    ]

            for quote in solicitud.purchase_ids:
                c_specs += [
                        (auto_name(), 1, 0, 'text', "TOTAL", None, cell_style_bold),
                        (auto_name(), 1, 0, 'number', quote_dict[quote.partner_id.id] * 1.12, None, cell_style_decimal_right),
                    ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_center_bold)


            c_specs = [(auto_name(),2, 0, 'text', "Observaciones:", None, cell_style_bold_2),
                       (auto_name(),3, 0, 'text', solicitud.description, None, cell_style_bold_2),
                      ]


            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style, set_column_size=True)

            c_specs = [(auto_name(), 1, 0, 'text', ""),]
            row_data = self.xls_row_template([], [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style, set_column_size=True)

            c_specs = [(auto_name(), 2, 0, 'text', "-----------------", None, cell_style_center_bold_2),
                       (auto_name(), 2, 0, 'text', "-----------------", None, cell_style_center_bold_2),
                       (auto_name(), 1, 0, 'text', "-----------------", None, cell_style_center_bold_2),
                       ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style, set_column_size=True)

            c_specs = [(auto_name(), 2, 0, 'text', "Solicitado por:", None, cell_style_center_bold_2),
                       (auto_name(), 2, 0, 'text', "Validado por:", None, cell_style_center_bold_2),
                       (auto_name(), 1, 0, 'text', "Aprobado por:", None, cell_style_center_bold_2),
                       ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style, set_column_size=True)

            c_specs = [(auto_name(), 2, 0, 'text', solicitud.create_uid.signature, None, cell_style_center_bold_2),
                       (auto_name(), 2, 0, 'text', "", None, cell_style_center_bold_2),
                       (auto_name(), 1, 0, 'text', "", None, cell_style_center_bold_2),
                       ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style, set_column_size=True)



            #Write an empty row to set column widths at the end of the document (acts globally on document).
            c_specs = [
                        (auto_name(), 1, 10, 'text', ""),
                        (auto_name(), 1, 13, 'text', ''),
                        (auto_name(), 1, 18, 'text', ''),
                        (auto_name(), 1, 18, 'text', ''),
                        (auto_name(), 1, 25, 'text', ''),
                ]

            for quote in solicitud.purchase_ids:
                c_specs += [ (auto_name(), 1, 20,'text',""),
                             (auto_name(), 1, 20, 'text', '')]

            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2, set_column_size=True)



purchase_comparison_report('report.purchase.comparison.analysis',
    'purchase.requisition',
    parser=purchase_analyis_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
