# -*- coding: utf-8 -*-
#################################################################################
#
# This file simply adds a credit_card object as a many2one field in account_invoice
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    Jan. 12th, 2014
#
#################################################################################
from openerp.osv import fields, osv, orm


#################################################################################
# Class definition of inherited account_invoice
#################################################################################
class account_invoice( orm.Model ):

    _inherit = 'account.invoice'

    _columns = { 'credit_card' : fields.many2one( 'ecua.credit.card', 'Tarjeta de Credito' ),
               }