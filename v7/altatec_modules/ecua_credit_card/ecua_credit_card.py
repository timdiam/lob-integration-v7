# -*- coding: utf-8 -*-
#################################################################################
#
# A module that contains a credit card model can be attached to other
# objects (e.g. invoices).
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    Jan. 9th, 2014
#
#################################################################################
from openerp.osv import fields, osv, orm


#####################################################################################
# Class definition for credit_card
#####################################################################################
class ecua_credit_card( orm.Model ):

    _name = "ecua.credit.card"

    #####################################################################################
    # Column definition
    #####################################################################################
    _columns = { 'name'    : fields.char( string="Name", required=True, ),
                 'partner' : fields.many2one( "res.partner", string="Partner" ),
                 'diario'  : fields.many2one( "account.journal", string="Diario" ),
               }



