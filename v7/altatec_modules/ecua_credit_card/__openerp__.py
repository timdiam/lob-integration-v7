{
    'name': 'Ecua Credit Card',
    'version': '1.0',
    'description': """
        A module that contains a credit card model can be attached to other objects (e.g. invoices).
    """,
    'author': 'Dan Haggerty',
    'website': 'www.altatececuador.com',
    "depends" : [ 'account' ],
    "data" : [ 'ecua_credit_card.xml', 'account_invoice.xml', 'security/ir.model.access.csv', 'account_journal.xml' ],
    "installable": True,
    "auto_install": False
}