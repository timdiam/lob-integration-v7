# -*- coding: utf-8 -*-
#################################################################################
#
# This file simply adds an "is_credit_card" to account.journal
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    7/4/2015
#
#################################################################################
from openerp.osv import fields, osv, orm

#################################################################################
# Class definition of inherited account_journal
#################################################################################
class account_journal( orm.Model ):

    _inherit = 'account.journal'

    _columns = { 'is_credit_card' : fields.boolean( "Es tarjeta de credito?" ),
               }

    _defaults = { 'is_credit_card' : False,
                }