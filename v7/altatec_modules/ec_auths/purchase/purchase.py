import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree
import openerp.addons.decimal_precision as dp
import openerp.exceptions

from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging
import xml.etree.cElementTree as ET


_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL, 'en_US.utf8')



class ec_auths_order(osv.osv):
    _name = 'ec.auths.order'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _track = {
        'state': {
            'ec_auths_order.mt_draft': lambda self, cr, uid, obj, ctx=None: obj['state'] == 'draft',
            'ec_auths_order.mt_abierto': lambda self, cr, uid, obj, ctx=None: obj['state'] == 'abierto',


            },
        }

    state_order_selection=[('draft', 'Borrador'), ('abierto', 'Abierto'), ('terminado', "Terminado"),
                           ('cancelado', "Cancelado"), ('autorizado', "Autorizado")]


    def create(self, cr, uid, values, context=None):

        line_pool = self.pool.get('ec.auths.order.line')
        line_ids = values['lines']
        for line in line_ids:
            line[2]['obra'] = values['worksite']

        create_context= dict(context, mail_create_nolog=False)



        return super(ec_auths_order, self).create(cr, uid, values, context=create_context)

    def write(self, cr, uid, ids, values, context=None):
        res = super(ec_auths_order, self).write(cr, uid, ids, values, context=context)

        for order in self.browse(cr, uid, ids, context=context):
            for line in order.lines:
                if 'worksite' in values:
                    self.pool.get('ec.auths.order.line').write(cr, uid, [line.id], {'obra': values['worksite'], },
                                                               context=context)

        return res

    def unconfirm_work_order(self, cr, uid, ids, context):
        obj = self.pool.get('ec.auths.order').browse(cr, uid, ids)
        wf_service = netsvc.LocalService("workflow")
        for order in obj:
            for line in order.lines:
                if (line.state == 'aprobada'):
                    raise osv.except_osv(
                        _('Invalid Action!'),
                        _('Una linea ya esta aprobada!'))

                elif (line.state == 'realizada'):
                    raise osv.except_osv(
                        _('Invalid Action!'),
                        _('una linea ya esta realizada'))

                elif (line.state == 'esperando_aprobacion'):
                    wf_service.trg_validate(uid, 'ec.auths.order.line', line.id, 'rechazar', cr)
                    wf_service.trg_validate(uid, 'ec.auths.order.line', line.id, 'volver_draft', cr)

            order.write({'state': 'draft'})

    def confirm_work_order(self, cr, uid, ids, context):
        obj = self.pool.get('ec.auths.order').browse(cr, uid, ids)
        wf_service = netsvc.LocalService("workflow")
        for order in obj:
            for line in order.lines:
                if line.qty == 0:
                    raise osv.except_osv(
                        _('Invalid Action!'),
                        _('No hay Cantidad de producto!'))

                wf_service.trg_validate(uid, 'ec.auths.order.line', line.id, 'enviar_solicitud', cr)

            order.write({'state': 'abierto'})

    def _get_user(self, cr, uid, context):
        x = 1
        return self.pool.get('res.users').browse(cr, uid, uid, context).id

    _columns = {
        'name': fields.char("Nombre", required=True, states={'abierto': [('readonly', True)]},),
        'date': fields.date("Fecha", states={'abierto': [('readonly', True)]}),
        'responsable': fields.many2one("res.partner", string="Persona Responsable",
                                       states={'abierto': [('readonly', True)]}),
        'per_responsable': fields.many2one("res.users", string="Persona Responsable",
                                           states={'abierto': [('readonly', True)]}),
        'lines': fields.one2many('ec.auths.order.line', 'order2', string='Linea de Solicitud', track_visibility='always'),
        'worksite': fields.many2one('ec.auths.worksite', string="Obra", required=True,
                                    states={'abierto': [('readonly', True)]}),
        'state': fields.selection(state_order_selection,string="Estado",
                                  required=True, track_visibility='onchange'),
        }

    _defaults = {'state': 'draft',
                 'per_responsable': _get_user,

                 }


class ec_auths_order_line(osv.osv):
    _name = 'ec.auths.order.line'

    def create(self, cr, uid, values, context=None):
        if ('unidad' in values) and ('material_type' in values):
            product = self.pool.get('product.product').browse(cr, uid, values['material_type'], context)
            if values['unidad'] != product.uom_id.id:
                raise osv.except_osv(_('Invalid Action!'), _(
                    'No se puede crear una linea de solicidud con una unidad diferente de lo que esta definido en el producto.  Producto: ' + str(
                        product.name)))

        return super(ec_auths_order_line, self).create(cr, uid, values, context)

    def write(self, cr, uid, ids, values, context=None):
        res = super(ec_auths_order_line, self).write(cr, uid, ids, values, context=context)

        if 'unidad' in values:
            for line in self.browse(cr, uid, ids, context):
                if 'material_type' in values:
                    product = self.pool.get('product.product').browse(cr, uid, values['material_type'], context)
                else:
                    product = line.material_type

                if product.uom_id.id != values['unidad']:
                    raise osv.except_osv(_('Invalid Action!'), _(
                        'No se puede crear una linea de solicidud con una unidad diferente de lo que esta definido en el producto.  Producto: ' + str(
                            product.name)))

        return res

    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        for rec in self.browse(cr, uid, ids, context=context):
            if rec.state not in ['draft']:
                raise osv.except_osv(_('Invalid Action!'),
                                     _('No se puede borrar una linea de solicitud en estado diferente que borrador'))

        return super(ec_auths_order_line, self).unlink(cr, uid, ids, context=context)

    def open_recent_purchases(self, cr, uid, ids, context):
        this = self.browse(cr, uid, ids, context=context)[0]

        order_list = []

        for f in this.fulfill:
            order = f.big_order.name
            if order not in order_list:
                order_list.append(order)

        return {
            'type': 'ir.actions.act_window',
            'name': 'Abrir Facturas',
            'view_mode': 'tree,form',
            'view_type': 'form',
            'res_model': 'account.invoice',
            'nodestroy': 'true',
            'domain': '[("origin","in",' + str(order_list) + ')]',

            }

    def open_fulfills(self, cr, uid, ids, context):

        this = self.browse(cr, uid, ids, context=context)[0]
        return {
            'type': 'ir.actions.act_window',
            'name': 'Open Fulfills',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'ec.auths.order.line',
            'nodestroy': 'false',
            'res_id': ids[0], }


    def get_qty_filled(self, cr, uid, ids, field_name, field_value, arg, context=None):

        records = self.browse(cr, uid, ids)
        result = {}

        for r in records:
            qty = 0
            for line in r.fulfill:
                if (line.state not in ['draft', 'cancel']) and (line.state):
                    qty += line.qty
            if (qty >= r.qty):

                result[r.id] = {'qty_filled': qty, 'all_filled': True}

            else:
                result[r.id] = {'qty_filled': qty, 'all_filled': False}

        return result

    def change_unidad(self, cr, uid, ids, material_type, context):
        material_obj = self.pool.get('product.product').browse(cr, uid, material_type, context)
        return {'value': {'unidad': material_obj.uom_id.id, 'desc': material_obj.default_code}, }

    def get_qty_inv(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
            result[r.id] = 1

        return result

    def get_qty_rec(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
            r_sum = 0
            for f in r.fulfill:
                order_name = f.big_order.name
                # TODO put in function the location_id below (or config menu)!!!!!
                stock_move_obj = self.pool.get('stock.move')
                if (f.order_ref):
                    stock_move_in_ids = stock_move_obj.search(cr, uid, [('product_id', '=', f.order_ref.product_id.id),
                                                                        ('origin', '=', order_name),
                                                                        ('type', '=', 'in'), ('location_id', '=', 8),
                                                                        ('state', '=', 'done')])
                    stock_move_out_ids = stock_move_obj.search(cr, uid, [('product_id', '=', f.order_ref.product_id.id),
                                                                         ('origin', '=', order_name),
                                                                         ('type', '=', 'out'),
                                                                         ('location_dest_id', '=', 8),
                                                                         ('state', '=', 'done')])

                    stock_move_in_objs = stock_move_obj.browse(cr, uid, stock_move_in_ids, context)
                    stock_move_out_objs = stock_move_obj.browse(cr, uid, stock_move_out_ids, context)

                    f_in_sum = 0
                    f_out_sum = 0

                    for s in stock_move_in_objs:
                        f_in_sum += s.product_qty

                    for s in stock_move_out_objs:
                        f_out_sum += s.product_qty

                    r_sum += f_in_sum - f_out_sum

            result[r.id] = r_sum

        return result

    def get_qty_paid(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
            result[r.id] = 1

        return result

    def get_cant_fact(self, cr, uid, ids, field_name, field_value, arg, context=None):
        result = {}
        valor = 0
        records = self.browse(cr, uid, ids)

        for r in records:
            valor = 0
            for re in r.fulfill:
                if (re.order_ref):
                    valor = valor + re.order_ref.qty_invoiced

            result[r.id] = valor

        return result

    _columns = {'name': fields.related('material_type', 'name', type='char', string="Nombre"),
                'authorized': fields.boolean('Autorizado'),
                'material_type': fields.many2one('product.product', string="Material", required=True, readonly=True,
                                                 states={'draft': [('readonly', False)]}),
                'desc': fields.char('Descripcion'),
                'qty': fields.float('Cant.', readonly=True, states={'draft': [('readonly', False)]}),
                'obra': fields.many2one('ec.auths.worksite', string='Obra'),
                'cant_facturado': fields.function(get_cant_fact, string='Cantidad Facturado', type='integer'),

                'qty_recibido': fields.function(get_qty_rec, string='Cant. Recibido', type='integer'),
                'qty_inv2': fields.function(get_qty_inv, string='Cant. Facturado', type="integer"),
                'qty_paid2': fields.function(get_qty_paid, string='Cant. Pagado', type="integer"),

                'qty_filled': fields.function(get_qty_filled, string="Comprado", method=True, type="float", multi='b'),
                'all_filled': fields.function(get_qty_filled, string='Todo Comprado', method=True, type='boolean',
                                              multi='b'),
                'qty_remaining': fields.float('Cant. Quedado'),
                'state': fields.selection([('rechazada', "Rechazada"), ('draft', 'Borrador'),
                                           ('esperando_aprobacion', 'Esperando Aprobacion'),
                                           ('aprobada', "Aprobada"), ('realizada', "Realizada")], string="Estado",
                                          readonly=True, required="True", track_visibility='always'),
                'fulfill': fields.one2many('ec.auths.fulfill.line', 'order_line', string="Fulfillments"),
                'order2': fields.many2one('ec.auths.order', string="Orden de Solicitud", required=True, readonly=True),
                'unidad': fields.many2one('product.uom', string='Unidad', readonly=True,
                                          states={'draft': [('readonly', False)]}),
                'budget_line': fields.many2one('ec.auths.budget.line', 'Rubro Presupuesto'),
                'comentario': fields.char("Comentarios"),
                'nombre_solicitud':fields.related('order2','name', type='char', string="Orden de Solicitud"),

                }
    _defaults = {'state': 'draft',
                 }
    _order = "id desc"


# TODO could probably do this with just a list of purchase order lines
#and not with this other object
class ec_auths_fulfill_line(osv.osv):
    _name = 'ec.auths.fulfill.line'


    def on_change_order_ref(self, cr, uid, ids, order, context=None):
        line = self.pool.get('purchase.order.line').browse(cr, uid, order, context)

        return {'value': {'qty': line.product_qty - line.ec_auths_used_qtys, 'price': line.price_unit}, }


    def get_line_price(self, cr, uid, ids, field_name, field_value, arg, context=None):

        records = self.browse(cr, uid, ids)
        result = {}

        for r in records:
            result[r.id] = r.order_ref.price_unit

        return result

    def get_qty_from_line(self, cr, uid, ids, field_name, field_value, arg, context=None):

        records = self.browse(cr, uid, ids)
        result = {}

        for r in records:
            qty = 0
            if (r.order_ref):
                qty = self.pool.get('purchase.order.line').browse(cr, uid, r.order_ref.id, context).product_qty

            result[r.id] = qty

        return result


    _columns = {
        'qty': fields.float("Cantidad", required=True),
        'price': fields.function(get_line_price, method="True", type='float', string="Precio"),
        'order_ref': fields.many2one("purchase.order.line", string="Pedido Linea", select=True, required=True),
        'partner_id': fields.related("big_order", "partner_id", type='many2one', relation='res.partner',
                                     string="Proveedor"),
        'order_line': fields.many2one('ec.auths.order.line', string="Linea de Solicitud", required=True),
        'material_type': fields.related('order_line', 'material_type', type='many2one', relation='product.product',
                                        string="Tipo de Material"),
        'product_qty_hint': fields.function(get_qty_from_line, string='Cant. En Orden', method=True, type='float'),
        'big_order': fields.related('order_ref', 'order_id', type='many2one', relation='purchase.order', string="Pedido"),
        'state': fields.related('order_ref', 'order_id', 'state', type='char', string="Estado"),
        }


    def get_def_orden(self, cr, uid, context):
        if 'order_line' in context:
            return context['order_line']

    def get_def_mat(self, cr, uid, context):
        if 'material_type' in context:
            return context['material_type']


    _defaults = {

        'material_type': get_def_mat,
        'order_line': get_def_orden,
        }


class product_product(osv.osv):
    _inherit = 'product.product'

    _columns = {
        'inv_val': fields.float('Val. Inv.', digits_compute=dp.get_precision("Inventory Valuation")),
        'precio_referencial': fields.float("Precio Referencial"),

        }


class ec_auths_worksite(osv.osv):
    _name = 'ec.auths.worksite'

    _columns = {
        'name': fields.char("Nombre", required=True),
        'bloques': fields.one2many("ec.auths.bloque", 'worksite', string="Bloques"),
        'address': fields.char("Direccion"),
        'bodega': fields.many2one("stock.warehouse", string="Bodega"),
        'budget': fields.many2one('ec.auths.budget', string="Presupuesta"),
        'analytic_account': fields.many2one('account.analytic.account', string="Cuenta Analytica")
    }


class ec_auths_budget(osv.osv):
    _name = 'ec.auths.budget'
    _columns = {
        'name': fields.char("Nombre"),
        'date_created': fields.date("Fecha de Vencimiento"),
        'worksite': fields.many2one('ec.auths.worksite', string="Obra", required=True),
        'analytic_account': fields.many2one('account.analytic.account', string='Cuenta Analytica'),
        'lines': fields.one2many('ec.auths.budget.line', 'budget', string="Lineas"),

        }


    def update_budget(self, cr, uid, ids, context={}):
        list_house_type = {}
        budg_obj = self.pool.get('ec.auths.budget')
        budg = budg_obj.browse(cr, uid, ids[0], context)
        worksite_id = budg.worksite.id
        house_obj = self.pool.get('ec.auths.house')
        house = house_obj.search(cr, uid, [('worksite.id', '=', worksite_id)], )  #OBETENEMOS LA LISTA DE CASAS
        my_houses = house_obj.browse(cr, uid, house, context)
        _logger.debug(house)
        for my_house in my_houses:
            if my_house.house_type.id not in list_house_type:
                list_house_type[my_house.house_type.id] = 1
            else:
                list_house_type[my_house.house_type.id] = list_house_type[my_house.house_type.id] + 1
        materiales = {}
        _logger.debug(list_house_type)

        tipos_casas = self.pool.get('ec.auths.job.type').search(cr, uid,
                                                                [('house_type.id', 'in', list_house_type.keys()), ])
        _logger.debug(tipos_casas)
        aux = self.pool.get('ec.auths.job.type').browse(cr, uid, tipos_casas, context)

        for tipos in aux:
            for li in tipos.lines:
                if li.material_id.id not in materiales:
                    materiales[li.material_id.id] = li.qty
                else:
                    materiales[li.material_id.id] = materiales[li.material_id.id] + li.qty
        lineas_obj = self.pool.get('ec.auths.budget.line')
        for key, value in materiales.items():
            _logger.debug(key)
            _logger.debug(value)
            lineas_obj.create(cr, uid, {'budgeted': value, 'material_type': key, 'budget': ids[0]})

        _logger.debug('bbb')
        _logger.debug(materiales)

        return True


class ec_auths_budget_line(osv.osv):
    _name = 'ec.auths.budget.line'

    def ec_auths_total(self, cr, uid, ids, field_name, field_value, arg, context=None):
        result = {}
        records = self.browse(cr, uid, ids)

        for r in records:
            if not r.tiene_movimientos:
                line_ids = self.pool.get('ec.auths.budget.line').search(cr, uid, [('parent_left', '>', r.parent_left),
                                                                                  ('parent_left', '<', r.parent_right),
                                                                                  ('tiene_movimientos', '=', True)])
                objs = self.pool.get('ec.auths.budget.line').browse(cr, uid, line_ids)
                total = 0
                for obj in objs:
                    total += obj.ref_price * obj.budgeted
                result[r.id] = total
            else:
                result[r.id] = 0
                result[r.id] = r.ref_price * r.budgeted
        return result

    def ec_auths_buy(self, cr, uid, ids, field_name, field_value, arg, context=None):
        result = {}
        records = self.browse(cr, uid, ids)

        for r in records:
            result[r.id] = 0
            id_compra = self.pool.get('purchase.order.line').search(cr, uid, [('presupuesto_linea.id', '=', r.id), (
                'parent_state', 'in', ['confirmed', 'approved', 'done'])])
            compras = self.pool.get('purchase.order.line').browse(cr, uid, id_compra)
            x = 1
            for comp in compras:
                result[r.id] = result.get(r.id, 0) + comp.product_qty
        return result

    def ec_auths_buy_price(self, cr, uid, ids, field_name, field_value, arg, context=None):
        result = {}
        records = self.browse(cr, uid, ids)

        for r in records:

            if not r.tiene_movimientos:
                line_ids = self.pool.get('ec.auths.budget.line').search(cr, uid, [('parent_left', '>', r.parent_left),
                                                                                  ('parent_left', '<', r.parent_right),
                                                                                  ('tiene_movimientos', '=', True)])
                objs = self.pool.get('ec.auths.budget.line').browse(cr, uid, line_ids)
                total = 0
                for obj in objs:
                    total += obj.buy_price
                result[r.id] = total
            else:
                result[r.id] = 0
                id_compra = self.pool.get('purchase.order.line').search(cr, uid, [('presupuesto_linea.id', '=', r.id), (
                    'parent_state', 'in', ['confirmed', 'done', 'approved'])])
                compras = self.pool.get('purchase.order.line').browse(cr, uid, id_compra)
                for comp in compras:
                    result[r.id] = result.get(r.id, 0) + comp.price_subtotal

        return result

    def ec_auths_invoice(self, cr, uid, ids, field_name, field_value, arg, context=None):
        result = {}
        records = self.browse(cr, uid, ids)
        for r in records:
            result[r.id] = 0
            id_compra = self.pool.get('purchase.order.line').search(cr, uid,
                                                                    [('product_id.id', '=', r.material_type.id),
                                                                     ('invoiced', '=', True)])
            compras = self.pool.get('purchase.order.line').browse(cr, uid, id_compra)
            for comp in compras:
                result[r.id] = result.get(r.id, 0) + comp.product_qty
        #_logger.debug(result)
        return result

    def get_total_fact(self, cr, uid, ids, field_name, field_value, arg, context=None):
        result = {}
        records = self.browse(cr, uid, ids)
        return 0


    _parent_name = "parent_id"
    _parent_store = True
    _parent_order = 'name'
    _order = 'parent_left'

    _columns = {
        'parent_id': fields.many2one('ec.auths.budget.line', string='Padre', ondelete='cascade'),
        'parent_left': fields.integer("Parent Left", select=1),
        'parent_right': fields.integer('Parent Right', select=1),
        'child_ids': fields.one2many('ec.auths.budget.line', 'parent_id', 'Contains'),
        'name': fields.char("Nombre"),
        'budget': fields.many2one('ec.auths.budget', string="Presupuesto", required=True),
        'material_type': fields.many2one('product.product', string="Material"),
        'ref_price': fields.float("Precio Unitario Ref."),
        'budgeted': fields.float("Cant. Planificada"),
        'total': fields.function(ec_auths_total, string="Tot. Planificado", method=True, type='float'),
        'buy': fields.function(ec_auths_buy, string='Cant. Comprado', method=True, type='float'),
        'buy_price': fields.function(ec_auths_buy_price, string='Tot. Comprado', method=True, type='float'),
        'invoiced': fields.function(ec_auths_invoice, string='Facturado', method=True, type='float'),
        'total_facturado': fields.function(get_total_fact, string='Tot. Facturado', method=True, type='float'),
        "tiene_movimientos": fields.boolean('Tiene Movimientos?'),  #'invoiced':fields.float('Facturado'),
    }


class ec_auths_bloque(osv.osv):
    _name = 'ec.auths.bloque'

    _columns = {
        'name': fields.char("Nombre", required=True),
        'number': fields.char("Numero", required=True),
        'platforms': fields.one2many('ec.auths.platform', 'bloque', string="Plataformas"),
        'worksite': fields.many2one('ec.auths.worksite', string="Obra", required=True),

        }


class ec_auths_platform(osv.osv):
    _name = 'ec.auths.platform'

    _columns = {
        'name': fields.char("Nombre", required=True),
        'number': fields.char("Numero", required=True),
        'houses': fields.one2many("ec.auths.house", 'platform', string="Casas"),
        'bloque': fields.many2one("ec.auths.bloque", string="Bloque", required=True),
        'worksite': fields.related('bloque', 'worksite', type="many2one", relation='ec.auths.worksite', string="Obra"),
        }


class ec_auths_house(osv.osv):
    _name = 'ec.auths.house'

    _columns = {
        'name': fields.char("Nombre", required=True),
        'number': fields.char("Numero", required=True),
        'house_type': fields.many2one("ec.auths.house.type", string="Tipo", required=True),
        'job_list': fields.one2many('ec.auths.job.need', 'house', string="Trabajos"),
        'platform': fields.many2one('ec.auths.platform', string="Plataforma", required=True),
        'bloque': fields.related('platform', 'bloque', type='many2one', relation='ec.auths.bloque', string="Bloque"),
        'worksite': fields.related('platform', 'worksite', type='many2one', relation='ec.auths.worksite', string="Obra"),
        }


class ec_auths_house_type(osv.osv):
    _name = 'ec.auths.house.type'
    _columns = {
        'name': fields.char("Nombre", required=True),
        'job_type_lines': fields.one2many("ec.auths.job.type.line", 'house_type', string='Trabajos'),
        }


class ec_auths_job_type_line(osv.osv):
    _name = 'ec.auths.job.type.line'

    _rec_name = 'job_type'


    def get_house_type(self, cr, uid, context):
        if 'house_type' in context:
            return context['house_type']

    _columns = {
        'house_type': fields.many2one("ec.auths.house.type", string="Tipo de Casa", required=True),
        'job_type': fields.many2one('ec.auths.job.type', string="Tipo de Trabajo", required=True),
        'sequence': fields.integer("Secuencia"),
        'percent': fields.float("Porcentaje de Trabajo"),
        }

    _defaults = {
        'house_type': get_house_type
    }


class ec_auths_job_need(osv.osv):
    _name = 'ec.auths.job.need'
    _columns = {
        'job_type': fields.many2one('ec.auths.job.type', string="Tipo de Trabajo", required=True),
        'percent': fields.float("Porcentaje"),
        'state': fields.selection([('unauth', "No Autorizado"), ('auth', 'Autorizado'), ('working', "Trabajando"),
                                   ("finished", "Terminado"), ("problem", "En Problemas")]),
        'house': fields.many2one('ec.auths.house', string="Casa", required=True),
        'platform': fields.related('house', 'platform', type='many2one', relation='ec.auths.platform', string="Platform"),
        'bloque': fields.related('house', 'bloque', type='many2one', relation='ec.auths.bloque', string="Bloque"),
        'worksite': fields.related('house', 'worksite', type='many2one', relation='ec.auths.worksite', string="Obra"),
        }


class ec_auths_job_type(osv.osv):
    _name = 'ec.auths.job.type'
    _columns = {
        'name': fields.char('Nombre', required=True),
        'lines': fields.one2many('ec.auths.job.type.requirments', 'job_type', string="Lineas"),
        #TODO this should probably be a related field below
        'worksite': fields.many2one('ec.auths.worksite', string='Obra'),
        'house_type': fields.many2one('ec.auths.house.type', string="Tipo de Casa", required=True),
        }


class ec_auths_job_type_requirments(osv.osv):
    _name = "ec.auths.job.type.requirments"

    _columns = {
        'name': fields.char("Nombre"),
        'material_id': fields.many2one("product.product", string="Material", required=True),
        'code': fields.related('material_id', 'desc', type='char', string="Codigo"),
        'qty': fields.float("Cantidad", required=True),
        'unit': fields.many2one('product.uom', string="Unit", required=True),
        'job_type': fields.many2one('ec.auths.job.type', string="Tipo de Trabajo", required=True),
        }

    def on_change_material_id(self, cr, uid, ids, material, context=None):
        unit = self.pool.get('product.product').browse(cr, uid, material, context).uom_id
        return {'value': {'unit': unit.id}, }


    def create(self, cr, uid, vals, context={}):
        unit = self.pool.get('product.product').browse(cr, uid, vals['material_id']).uom_id
        vals['unit'] = unit.id
        return super(ec_auths_job_type_requirments, self).create(cr, uid, vals, context)


class purchase_order(osv.osv):
    _inherit = 'purchase.order'


    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        ful_obj = self.pool.get('ec.auths.fulfill.line')

        for rec in self.browse(cr, uid, ids, context=context):
            for line in rec.order_line:
                ful_ids = ful_obj.search(cr, uid, [('order_ref', '=', line.id)])
                if ful_ids:
                    for fulfill in ful_obj.browse(cr, uid, ful_ids):
                        ful_obj.unlink(cr, uid, fulfill.id)

        result = super(purchase_order, self).unlink(cr, uid, ids, context=context)

        return result


    _columns = {
        'obra': fields.many2one('ec.auths.worksite', string="Obra"),
        'minuto_type': fields.selection([('arquitectos', "Arquitectos"), ('normal', 'Normal')], string="Tipo"),
        'nombre_solicitud':fields.text("# Solicitud"),
        "numero_ref":fields.char("Numero Referencia"),

        }


class account_voucher(osv.osv):
    _inherit = 'account.voucher'


    _columns = {
        'obra': fields.many2one('ec.auths.worksite', string="Obra"),


        }


class account_invoice_line(osv.osv):
    _inherit = 'account.invoice.line'

    _columns = {
        'po_line': fields.many2one('purchase.order.line', string='Linea de Pedido de Compra'),

        }


class purchase_order_line(osv.osv):
    _inherit = 'purchase.order.line'

    def get_used_qty(self, cr, uid, ids, field_name, field_value, arg, context=None):

        records = self.browse(cr, uid, ids)
        result = {}

        for r in records:
            wa_obj = self.pool.get('ec.auths.fulfill.line').search(cr, uid, [('order_ref', '=', r.id)])
            used = 0
            for l in self.pool.get('ec.auths.fulfill.line').browse(cr, uid, wa_obj):
                used += l.qty

            result[r.id] = used

        return result

    def get_all_used(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}

        for r in records:
            if r.ec_auths_used_qtys >= r.product_qty:
                result[r.id] = True
            else:
                result[r.id] = False

        return result


    def update_assigned(self, cr, uid, ids, context=None):
        result = {}
        for l in self.pool.get('ec.auths.fulfill.line').browse(cr, uid, ids, context=context):
            result[l.order_ref.id] = True
        return result.keys()


    def get_cant_facturado(self, cr, uid, ids, field_name, field_value, arg, context=None):

        records = self.browse(cr, uid, ids)
        result = {}

        for r in records:
            qty = 0
            for line in r.invoice_line_list:
                if (line.invoice_id.state in ['open', 'paid']):
                    qty += line.quantity
            if (qty):

                result[r.id] = qty

            else:
                result[r.id] = qty

        return result


    _columns = {
        'parent_order_name': fields.related('order_id', 'name', type='text', string='Pedido'),
        'presupuesto_linea': fields.many2one('ec.auths.budget.line', string="Presupuesto"),
        'invoice_line_list': fields.one2many('account.invoice.line', 'po_line', string='Lineas De Factura'),
        'qty_invoiced': fields.function(get_cant_facturado, method=True, type='float', string='Cant. Facturado'),
        'parent_state': fields.related('order_id', 'state', type='char', string='p_state'),
        'comentario':fields.char("Comentario"),
        'ec_auths_used_qtys': fields.function(get_used_qty, method=True, type='float', string="Used"),

        'all_used': fields.function(get_all_used, string='Asignado', type='boolean',
                                    store={
                                        'ec.auths.fulfill.line': (update_assigned, None, 50),
                                        }),
        }

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        if context is None:
            context = {}
        if context and context.get('search_default_categ_id', False):
            args.append((('presupuesto_linea', 'child_of', context['search_default_categ_id'])))
        return super(purchase_order_line, self).search(cr, uid, args, offset=offset, limit=limit, order=order,
                                                       context=context, count=count)


class ec_auths_purchase_wizard_line(osv.osv_memory):
    _name = 'ec.auths.purchase.wizard.line'

    _columns = {'need_line': fields.many2one('ec.auths.order.line'),
                'name': fields.char("Nombre"),
                'unidad': fields.many2one('product.uom', string='Unidad', readonly=True),
                'qty': fields.float("Cant."),
                'price':fields.float("Precio",digits=(12,4)),
                'material_id': fields.many2one("product.product", string="Tipo de Material"),
                'wizard': fields.many2one('ec.auths.purchase.wizard', readonly=True),
                'rubro_presupuesto': fields.many2one('ec.auths.budget.line', "Rubro Presupuesto"),
                "nombre_solicitud":fields.char("# Solicitud"),
                "comentario":fields.related("need_line","comentario", type='char', string="Comentarios")

                }


class ec_auths_purchase_wizard(osv.osv_memory):
    _name = 'ec.auths.purchase.wizard'


    def get_warehouse(self, cr, uid, context):
        if 'active_ids' in context:

            need_objs = self.pool.get('ec.auths.order.line').browse(cr, uid, context['active_ids'], context)
            warehouses = []
            for need in need_objs:
                w = need.order2.worksite.bodega.id
                if w not in warehouses:
                    warehouses.append(w)

            if len(warehouses) == 0:
                raise osv.except_osv(
                    _('Invalid Action!'),
                    _('No hay bodegas definidas en obra!'))
            if len(warehouses) > 1:

                raise osv.except_osv(
                    _('Invalid Action!'),
                    _('Usted ha elijido varios obras en este seleccion!'))
            else:
                return warehouses[0]


    def get_obra(self, cr, uid, context):

        if 'active_ids' in context:
            need_objs = self.pool.get('ec.auths.order.line').browse(cr, uid, context['active_ids'], context)
            obras = []
            for need in need_objs:
                o = need.order2.worksite.id
                if o not in obras:
                    obras.append(o)

            if len(obras) == 0:
                raise osv.except_osv(
                    _('Invalid Action!'),
                    _('No hay obra definida'))
            if len(obras) > 1:

                raise osv.except_osv(
                    _('Invalid Action!'),
                    _('Usted ha elijido varios obras en este seleccion!'))
            else:
                return obras[0]

    def get_lines(self, cr, uid, context):

        need_objs = self.pool.get('ec.auths.order.line').browse(cr, uid, context['active_ids'], context)
        lines = []
        for need in need_objs:
            lines.append([0, 0, {'name': need.name, 'unidad': need.unidad.id, 'rubro_presupuesto': need.budget_line.id,
                                 'nombre_solicitud':need.nombre_solicitud,
                                 'qty': need.qty - need.qty_filled, 'material_id': need.material_type.id,
                                 'need_line': need.id, 'price': need.material_type.standard_price}])

        return lines

    def view_init(self, cr, uid, fields, context=None):

        obj = self.pool.get('ec.auths.order.line').browse(cr, uid, context['active_ids'])

        super(ec_auths_purchase_wizard, self).view_init(cr, uid, fields, context=context)

        for o in obj:
            if o.all_filled == True:
                raise osv.except_osv(
                    _('Invalid Action!'),
                    _('One or more of the work order lines have been completely filled!'))

            if o.state != 'aprobada':
                raise osv.except_osv(
                    _('Invalid Action!'),
                    _('Una (o mas) de las lineas no esta autorizada!'))


    _columns = {
        'provider_id': fields.many2one('res.partner', string="Proveedor", required=True),
        'warehouse': fields.many2one('stock.warehouse', string="Bodega", required=True, readonly=True),
        'obra': fields.many2one('ec.auths.worksite', string="Obra", required=True, readonly=True),
        'name': fields.char("Nombre"),
        'numero_ref':fields.char("Numero de Referencia"),
        'lines': fields.one2many('ec.auths.purchase.wizard.line', 'wizard', string="Lineas"),


        }

    _defaults = {
        'lines': get_lines,
        'warehouse': get_warehouse,
        'obra': get_obra,
        }

    def generate_purchases(self, cr, uid, ids, context):

        po_line_obj = self.pool.get('purchase.order.line')
        po_obj = self.pool.get('purchase.order')
        f_line_obj = self.pool.get('ec.auths.fulfill.line')

        wiz = self.pool.get('ec.auths.purchase.wizard').browse(cr, uid, ids, context)[0]

        ware_obj = self.pool.get('stock.warehouse').browse(cr, uid, wiz.warehouse.id, context)
        obra_obj = self.pool.get('ec.auths.worksite').browse(cr, uid, wiz.obra.id, context)

        solicitudes=None
        solicitudes_list=[]
        for li in wiz.lines:
            if solicitudes==None:
                solicitudes_list.append(li.nombre_solicitud)
                solicitudes=li.nombre_solicitud
            if li.nombre_solicitud not in solicitudes_list:
                solicitudes_list.append(li.nombre_solicitud)
                solicitudes=solicitudes+'\n'+li.nombre_solicitud

        if (obra_obj.bodega.id != ware_obj.id):
            raise osv.except_osv(
                _('Invalid Action!'),
                _('La bodega y obra no son compatibles'))

        po_id = po_obj.create(cr, uid, {'partner_id': wiz.provider_id.id,
                                        'warehouse_id': wiz.warehouse.id,
                                        'location_id': wiz.warehouse.lot_input_id.id,
                                        'pricelist_id': wiz.provider_id.property_product_pricelist_purchase.id,
                                        'nombre_solicitud':solicitudes,
                                        'numero_ref':wiz.numero_ref,
                                        'obra': wiz.obra.id})

        for l in wiz.lines:
            tax_list = []

            for t in l.material_id.supplier_taxes_id:
                tax_list.append(t.id)

            po_line_id = po_line_obj.create(cr, uid, {
                'account_analytic_id': l.rubro_presupuesto.budget.worksite.id if l.rubro_presupuesto else None,
                'presupuesto_linea': l.rubro_presupuesto.id,
                'date_planned': datetime.now(),
                'product_id': l.material_id.id,
                'name': l.material_id.name,
                'product_qty': l.qty,
                'price_unit': l.price,
                'product_uom': l.material_id.uom_id.id,
                'comentario':l.comentario,
                'order_id': po_id,
                'taxes_id': [(6, 0, tax_list)]})

            f_line_obj.create(cr, uid,
                              {'qty': l.qty, 'price': 0, 'order_ref': po_line_id, 'order_line': l.need_line.id, })

        return {
            'type': 'ir.actions.act_window',
            'name': 'Form heading',
            'view_mode': 'form,tree',
            'view_type': 'form',
            'res_model': 'purchase.order',
            'res_id': po_id  #  'domain':[('report','=',ids[0])]
        }

class ec_auths_line_invoice_search(osv.osv_memory):
    _name = 'ec.auths.line.invoice.search'

    _columns={
            "name":fields.char("Nombre"),
            "proveedor":fields.char("Proveedor"),
            "total":fields.char("Total"),
            "invoices":fields.many2one("account.invoice", string="Facturas"),
            "join_wizard_id":fields.many2one('ec.auths.invoice.join.wizard'),
            "join_check":fields.boolean("Enlazar"),

        }

class ec_auths_invoice_join_line(osv.osv_memory):
    _name = 'ec.auths.invoice.join.line'

    _columns={

        "name":fields.char("Nombre"),
        "qty":fields.float("Cant."),
        "join_wizard_id":fields.many2one('ec.auths.invoice.join.wizard'),
        "purchase_order_line":fields.many2one('purchase.order.line',"Linea de Compra"),

        }

class ec_auths_invoice_join_wizard(osv.osv_memory):
    _name = 'ec.auths.invoice.join.wizard'

    def get_lines(self, cr, uid, context):

        po_objs = self.pool.get('purchase.order.line').browse(cr, uid, context['active_ids'], context)
        lines = []
        for po in po_objs:
            lines.append([0, 0,
                          {'name': po.name, 'qty': po.product_qty, 'purchase_order_line':po.id}])

        return lines

    def generar_invoices_join (self, cr, uid, ids, context):
        records = self.browse(cr, uid, ids)[0]
        invoices_id=self.pool.get('account.invoice').search(cr,uid,[("internal_number","=",records.numero_interno)])
        invoices=self.pool.get('account.invoice').browse(cr,uid,invoices_id)
        for fact in invoices:
            invoice_search_ids = self.pool.get("ec.auths.line.invoice.search").create(cr, uid, {
                "name":fact.internal_number,
                "proveedor":fact.partner_id.name,
                "total":fact.amount_total,
                "invoices":fact.id,
                "join_wizard_id":ids[0],
                })

        return { 'type'      : 'ir.actions.act_window',
                     'name'      : 'Enlazar Facturas',
                     'view_type' : 'form',
                     'view_mode' : 'form',
                     'target': 'new',
                     'res_model' : 'ec.auths.invoice.join.wizard',
                     'res_id'    : records.id,
                   }

    def enlazar_facturas(self, cr, uid, ids, context):
        records = self.browse(cr, uid, ids)[0]
        cont=0
        if len(records.lines_invoices_search)==0:
            raise osv.except_osv(_('Warning!'), _('Debe seleccionar una Factura para Enlazar'))
        for line_join in records.lines_invoices_search:
            if line_join.join_check==True:
                cont+=1
                invoice_for_join=line_join
        if cont!=1:
            raise osv.except_osv(_('Warning!'), _('Debe selecionar solo 1 Factura para Enlazar'))

        if (invoice_for_join.invoices.invoice_line[0].po_line):
            raise osv.except_osv(_('Warning!'), _('La Factura ya esta Enlazada'))

        self.pool.get('account.invoice.line').write(cr, uid, [invoice_for_join.invoices.invoice_line[0].id],
                                                  {'po_line': records.join_line_ids[0].purchase_order_line.id, })

        return True

    def view_init(self, cr, uid, fields, context=None):
        obj = self.pool.get('purchase.order.line').browse(cr, uid, context['active_ids'])
        super(ec_auths_invoice_join_wizard, self).view_init(cr, uid, fields, context=context)
        if (len(obj))!=1:
            raise osv.except_osv(_('Warning!'), _('Solo se puede selecionar 1 Linea de Compras'))





    _columns={
        'join_line_ids': fields.one2many('ec.auths.invoice.join.line', 'join_wizard_id', 'Lineas'),
        'numero_interno':fields.char("Numero Interno"),
        "invoices":fields.many2one("account.invoice","Facturas"),
        "lines_invoices_search":fields.one2many("ec.auths.line.invoice.search","join_wizard_id", "Lineas Facturas"),
        }

    _defaults = {
        'join_line_ids': get_lines,
        }


class ec_auths_invoice_wizard_line(osv.osv_memory):
    _name = 'ec.auths.invoice.wizard.line'

    _columns = {'po_line': fields.many2one('purchase.order.line'),
                'name': fields.char("Nombre"),
                'qty_suggested': fields.float("Cant."),
                'price': fields.float("Precio"),
                'qty_chosen': fields.float('Cant. Facturar Ahora'),
                'material_id': fields.many2one("product.product", string="Material"),
                'wizard': fields.many2one('ec.auths.invoice.wizard', required=True, readonly=True),
                'budget_line': fields.many2one('ec.auths.budget.line', string="Rubro Presupuesto"),
                }


class ec_auths_invoice_wizard(osv.osv_memory):
    _name = 'ec.auths.invoice.wizard'

    def get_prov_def(self, cr, uid, context):
        po_objs = self.pool.get('purchase.order.line').browse(cr, uid, context['active_ids'], context)
        provs = []
        for pol in po_objs:
            if pol.partner_id.id not in provs:
                provs.append(pol.partner_id.id)
        if len(provs) != 1:
            raise osv.except_osv(_('Warning!'), _('Solo se puede selecionar compras de solo un proveedor'))
        else:
            return provs[0]


    def get_lines(self, cr, uid, context):

        po_objs = self.pool.get('purchase.order.line').browse(cr, uid, context['active_ids'], context)
        lines = []
        for po in po_objs:
            lines.append([0, 0,
                          {'budget_line': po.presupuesto_linea.id, 'name': po.name, 'qty_suggested': po.product_qty,
                           'material_id': po.product_id.id, 'po_line': po.id, 'price': po.price_unit}])

        return lines

    def view_init(self, cr, uid, fields, context=None):

        obj = self.pool.get('purchase.order.line').browse(cr, uid, context['active_ids'])

        super(ec_auths_invoice_wizard, self).view_init(cr, uid, fields, context=context)

        for o in obj:
            #if o.all_filled == True:
            #   raise osv.except_osv(
            #       _('Invalid Action!'),
            #       _('One or more of the work order lines have been completely filled!'))

            #if o.authorized == False:
            #   raise osv.except_osv(
            #       _('Invalid Action!'),
            #       _('Una (o mas) de las lineas no esta autorizada!'))
            x = 3

    _columns = {
        'lines': fields.one2many('ec.auths.invoice.wizard.line', 'wizard', string="Lineas"),
        'provider_id': fields.many2one('res.partner', string='Proveedor', readonly=True),
        }

    _defaults = {
        'lines': get_lines,
        'provider_id': get_prov_def,
        }

    def generate_facturas(self, cr, uid, ids, context):

        invoice_line_obj = self.pool.get('account.invoice.line')
        invoice_obj = self.pool.get('account.invoice')

        wiz = self.pool.get('ec.auths.invoice.wizard').browse(cr, uid, ids, context)[0]

        invoice_header = invoice_obj._prepare_invoice_header(cr, uid, wiz.provider_id.id, 'in_invoice',
                                                             inv_date=time.strftime('%Y-%m-%d'), context=context)

        invoice_header.update({'account_id': wiz.provider_id.property_account_payable.id,
                               'partner_id': wiz.provider_id.id,
                               'type': 'in_invoice', })

        context.update({'type':'in_invoice'})
        invoice_id = invoice_obj.create(cr, uid, invoice_header,context=context)

        #TODO WHEN MAKING INVOICE LINES, DO SO WITH NEW SHADOW TAX MODULE 
        for l in wiz.lines:

            tax_list = []
            for t in l.material_id.supplier_taxes_id:
                tax_list.append(t.id)

            product_id = l.material_id
            invoice_line_id = invoice_line_obj.create(cr, uid, {
                'account_analytic_id': l.budget_line.budget.worksite.analytic_account.id if l.budget_line else None,
                'product_id': product_id.id,
                'account_id': product_id.categ_id.property_account_expense_categ.id,
                'name': product_id.name,
                'quantity': l.qty_chosen,
                'price_unit': l.price,
                'invoice_id': invoice_id,
                'invoice_line_tax_id': [(6, 0, tax_list)],
                'po_line': l.po_line.id})
            invoice_obj.button_reset_taxes(cr, uid, [invoice_id], context)

        return {
            'type': 'ir.actions.act_window',
            'name': 'Form heading',
            'view_mode': 'form,tree',
            'view_type': 'form',
            'res_model': 'account.invoice',
            'res_id': invoice_id  # 'domain':[('report','=',ids[0])]
        }


class ec_auths_kardex(osv.osv):
    _name = 'ec.auths.kardex'

    _columns = {
        'name': fields.char("Nombre", required=True),
        'product_id': fields.many2one('product.product', string="Material"),
        'lines': fields.one2many('ec.auths.kardex.line', 'kardex', string='Lineas'),
        }


class ec_auths_kardex_line(osv.osv):
    _name = 'ec.auths.kardex.line'


    def get_day(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
            #   str_date = datetime.strptime(r.date, '%Y-%m-%d').strftime("%d/%m/%Y") if invoice.fechaEmision else ""
            #   x = datetime(r.date.year,r.date.month,r.date.day)
            str_date = datetime.strptime(r.date[0:18], '%Y-%m-%d %H:%M:%S').strftime("%Y-%m-%d %H:%M")
            result[r.id] = str_date

        return result

    _columns = {
        'filler': fields.char(""),
        'name': fields.char("Nombre", required=True),
        'kardex': fields.many2one('ec.auths.kardex', string="Kardex"),
        'date': fields.datetime('Fecha'),
        'date_day': fields.function(get_day, type='char', store=True, method=True, string="Dia"),
        'product_id': fields.many2one('product.product', string="Material"),
        'type': fields.selection(
            [('initial', "Inicial"), ('input', 'Entra'), ('output', "Salida"), ('internal', "Internal")]),
        'c_qty': fields.float("+Cant."),
        'c_cost': fields.float("Costo", digits_compute=dp.get_precision('Product Price')),
        'c_total': fields.float('Tot.', digits_compute=dp.get_precision('Product Price')),
        's_qty': fields.float('-Cant.'),
        's_cost': fields.float('Costo', digits_compute=dp.get_precision('Product Price')),
        's_total': fields.float('Tot.', digits_compute=dp.get_precision('Product Price')),
        'new_qty': fields.float('Cant.'),
        'new_total': fields.float('Valor', digits_compute=dp.get_precision('Product Price')),
        'new_avg': fields.float("Promedio", digits_compute=dp.get_precision('Product Price')),
        'move_id': fields.many2one('stock.move', string="Movimiento"),
        }


class stock_move(osv.osv):
    _inherit = 'stock.move'

    _columns = {
        'ec_auths_price': fields.float('Costo Ref'),
        }

    def on_change_product_id(self, cr, uid, ids, product_id, context=None):
        prod = self.pool.get('product.product').browse(cr, uid, product_id, context)
        name = prod.name
        unit = prod.uom_id.id

        return {'value': {'product_uom': unit, 'name': name}}

    def _default_location_destination(self, cr, uid, context=None):

        if ('ec_auths_def_dest' in context):
            if not context['ec_auths_def_dest']:
                raise osv.except_osv(_('Warning!'), _('Por favor, elija una obra y trabajo destino'))

            return context['ec_auths_def_dest']
        else:
            return super(stock_move, self)._default_location_destination(cr, uid, context)


    def _default_location_source(self, cr, uid, context=None):
        if ('ec_auths_def_source' in context):
            return context['ec_auths_def_source']
        else:
            return super(stock_move, self)._default_location_source(cr, uid, context)

    _defaults = {
        'location_id': _default_location_source,
        'location_dest_id': _default_location_destination,
        }


class stock_partial_picking(osv.osv_memory):
    _inherit = 'stock.partial.picking'


    def _partial_move_for(self, cr, uid, move):
        partial_move = super(stock_partial_picking, self)._partial_move_for(cr, uid, move)

        if move.picking_id.type == 'out' and move.product_id.cost_method == 'average' and move.location_dest_id.usage == 'supplier':
            partial_move.update(update_cost=True, **self._product_cost_for_average_update(cr, uid, move))

        elif move.picking_id.type == 'out' and move.product_id.cost_method == 'average' and move.location_dest_id.usage == 'customer':
            partial_move.update(update_cost=True, **self._product_cost_for_average_update(cr, uid, move))

        elif move.picking_id.type == 'in' and move.product_id.cost_method == 'average' and move.location_id.usage == 'customer':
            #use_this_price = self.pool.get('stock.move').browse(cr,uid,move.move_history_ids[0]).price_unit
            partial_move.update(update_cost=True, cost=move.price_unit)

        return partial_move


    def do_partial(self, cr, uid, ids, context=None):

        assert len(ids) == 1, 'Partial picking processing may only be done one at a time.'
        stock_picking = self.pool.get('stock.picking')
        stock_move = self.pool.get('stock.move')
        uom_obj = self.pool.get('product.uom')
        partial = self.browse(cr, uid, ids[0], context=context)
        partial_data = {
            'delivery_date': partial.date
        }
        picking_type = partial.picking_id.type

        for wizard_line in partial.move_ids:

            line_uom = wizard_line.product_uom
            move_id = wizard_line.move_id.id

            #Quantiny must be Positive
            if wizard_line.quantity < 0:
                raise osv.except_osv(_('Warning!'), _('Please provide proper Quantity.'))

            #Compute the quantity for respective wizard_line in the line uom (this jsut do the rounding if necessary)
            qty_in_line_uom = uom_obj._compute_qty(cr, uid, line_uom.id, wizard_line.quantity, line_uom.id)

            if line_uom.factor and line_uom.factor <> 0:
                if float_compare(qty_in_line_uom, wizard_line.quantity, precision_rounding=line_uom.rounding) != 0:
                    raise osv.except_osv(_('Warning!'), _(
                        'The unit of measure rounding does not allow you to ship "%s %s", only rounding of "%s %s" is accepted by the Unit of Measure.') % (
                                             wizard_line.quantity, line_uom.name, line_uom.rounding, line_uom.name))
            if move_id:

                #Check rounding Quantity.ex.
                #picking: 1kg, uom kg rounding = 0.01 (rounding to 10g),
                #partial delivery: 253g
                #=> result= refused, as the qty left on picking would be 0.747kg and only 0.75 is accepted by the uom.
                initial_uom = wizard_line.move_id.product_uom
                #Compute the quantity for respective wizard_line in the initial uom
                qty_in_initial_uom = uom_obj._compute_qty(cr, uid, line_uom.id, wizard_line.quantity, initial_uom.id)
                without_rounding_qty = (wizard_line.quantity / line_uom.factor) * initial_uom.factor
                if float_compare(qty_in_initial_uom, without_rounding_qty,
                                 precision_rounding=initial_uom.rounding) != 0:
                    raise osv.except_osv(_('Warning!'), _(
                        'The rounding of the initial uom does not allow you to ship "%s %s", as it would let a quantity of "%s %s" to ship and only rounding of "%s %s" is accepted by the uom.') % (
                                             wizard_line.quantity, line_uom.name,
                                             wizard_line.move_id.product_qty - without_rounding_qty, initial_uom.name,
                                             initial_uom.rounding, initial_uom.name))
            else:
                seq_obj_name = 'stock.picking.' + picking_type
                move_id = stock_move.create(cr, uid, {'name': self.pool.get('ir.sequence').get(cr, uid, seq_obj_name),
                                                      'product_id': wizard_line.product_id.id,
                                                      'product_qty': wizard_line.quantity,
                                                      'product_uom': wizard_line.product_uom.id,
                                                      'prodlot_id': wizard_line.prodlot_id.id,
                                                      'location_id': wizard_line.location_id.id,
                                                      'location_dest_id': wizard_line.location_dest_id.id,
                                                      'picking_id': partial.picking_id.id
                                                      }, context=context)
                stock_move.action_confirm(cr, uid, [move_id], context)

            partial_data['move%s' % (move_id)] = {
                'product_id': wizard_line.product_id.id,
                'product_qty': wizard_line.quantity,
                'product_uom': wizard_line.product_uom.id,
                'prodlot_id': wizard_line.prodlot_id.id,
                }
            if (wizard_line.product_id.cost_method == 'average'):
                partial_data['move%s' % (wizard_line.move_id.id)].update(product_price=wizard_line.cost,
                                                                         product_currency=wizard_line.currency.id)
        stock_picking.do_partial(cr, uid, [partial.picking_id.id], partial_data, context=context)
        return {'type': 'ir.actions.act_window_close'}


class stock_picking_out(osv.osv):
    _inherit = 'stock.picking.out'

    def create(self, cr, uid, vals, context=None):
        #Your code goes here

        if ('check_ec_auths' in context):
            location_source = vals['ec_auths_source']
            destination_source = vals['ec_auths_destination']
            move_lines = vals['move_lines']
            for m in move_lines:
                fields = m[2]
                if fields['location_id'] != location_source:
                    raise osv.except_osv(
                        _('Invalid Action!'),
                        _(
                            'Todos las lineas deberian tener la misma ubicacion fuente.  Borre esta entrega, y intente de nuevo!'))

                if fields['location_dest_id'] != destination_source:
                    raise osv.except_osv(
                        _('Invalid Action!'),
                        _(
                            'Todos las lineas deberian tener la misma ubicacion destina.  Borre esta entrega, y intente de nuevo!'))

            res = super(stock_picking_out, self).create(cr, uid, vals, context=context)
        else:
            res = super(stock_picking_out, self).create(cr, uid, vals, context=context)
        #Your code goes here
        return res

    def write(self, cr, uid, ids, vals, context=None):
        if ('check_ec_auths' in context):
            res = super(stock_picking_out, self).write(cr, uid, ids, vals, context=context)
            new_picking = self.pool.get('stock.picking').browse(cr, uid, ids[0], context)
            location_dest = new_picking.ec_auths_destination.id
            location_source = new_picking.ec_auths_source.id
            for m in new_picking.move_lines:
                if m.location_dest_id.id != location_dest:
                    raise osv.except_osv(
                        _('Invalid Action!'),
                        _('Todas las lineas deberian tener la misma ubicacion destina'))

                if m.location_id.id != location_source:
                    raise osv.except_osv(
                        _('Invalid Action!'),
                        _('Todas las lineas deberian tener la misma ubicacion fuente'))



        else:
            res = super(stock_picking_out, self).write(cr, uid, ids, vals, context=context)
        #Your code goes here
        return res

    def on_change_project_id(self, cr, uid, ids, project_id, move_lines, context=None):
        proj = self.pool.get('ec.auths.worksite').browse(cr, uid, project_id, context)

        #for m in picking.move_lines:
        #   m.write({'location_id':proj.bodega.lot_stock_id.id})

        return {'value': {'ec_auths_source': proj.bodega.lot_stock_id.id}}

    _columns = {
        'ec_auths_source': fields.many2one('stock.location', string="Bodega Fuente",
                                           domain=[('usage', '=', 'internal')]),
        'ec_auths_destination': fields.many2one('stock.location', string="Trabajo Destino",
                                                domain=[('usage', '=', 'customer')]),
        'ec_auths_project': fields.many2one('ec.auths.worksite', string='Obra'),
        'bodeguero': fields.many2one("res.users", string="Bodeguero Responsable"),
        'contratista': fields.many2one("res.users", string="Contratista Responsable"),
        }


class stock_picking_in(osv.osv):
    _inherit = 'stock.picking.in'
    _columns = {

        'ec_auths_warehouse2': fields.related('purchase_id', 'warehouse_id', type='many2one',
                                              relation='stock.warehouse', string="Bodega"),
        }


class stock_picking(osv.osv):
    _inherit = 'stock.picking'
    _columns = {

        'ec_auths_warehouse2': fields.related('purchase_id', 'warehouse_id', type='many2one',
                                              relation='stock.warehouse', string="Bodega"),
        'ec_auths_source': fields.many2one('stock.location', string="Bodega Fuente",
                                           domain=[('usage', '=', 'internal')]),
        'ec_auths_destination': fields.many2one('stock.location', string="Trabajo Destino",
                                                domain=[('usage', '=', 'customer')]),
        'ec_auths_project': fields.many2one('ec.auths.worksite', string='Obra'),
        'bodeguero': fields.many2one("res.users", string="Bodeguero Responsable"),
        'contratista': fields.many2one("res.users", string="Contratista Responsable"),
        }

    def do_partial(self, cr, uid, ids, partial_datas, context=None):
        """ Makes partial picking and moves done.
        @param partial_datas : Dictionary containing details of partial picking
                          like partner_id, partner_id, delivery_date,
                          delivery moves with product_id, product_qty, uom
        @return: Dictionary of values
        """
        if context is None:
            context = {}
        else:
            context = dict(context)
        res = {}
        move_obj = self.pool.get('stock.move')
        product_obj = self.pool.get('product.product')
        currency_obj = self.pool.get('res.currency')
        uom_obj = self.pool.get('product.uom')
        sequence_obj = self.pool.get('ir.sequence')
        wf_service = netsvc.LocalService("workflow")
        for pick in self.browse(cr, uid, ids, context=context):
            new_picking = None
            complete, too_many, too_few = [], [], []
            move_product_qty, prodlot_ids, product_avail, partial_qty, product_uoms = {}, {}, {}, {}, {}
            for move in pick.move_lines:
                if move.state in ('done', 'cancel'):
                    continue
                partial_data = partial_datas.get('move%s' % (move.id), {})
                product_qty = partial_data.get('product_qty', 0.0)
                move_product_qty[move.id] = product_qty
                product_uom = partial_data.get('product_uom', False)
                product_price = partial_data.get('product_price', 0.0)
                product_currency = partial_data.get('product_currency', False)
                prodlot_id = partial_data.get('prodlot_id')
                prodlot_ids[move.id] = prodlot_id
                product_uoms[move.id] = product_uom
                partial_qty[move.id] = uom_obj._compute_qty(cr, uid, product_uoms[move.id], product_qty,
                                                            move.product_uom.id)
                if move.product_qty == partial_qty[move.id]:
                    complete.append(move)
                elif move.product_qty > partial_qty[move.id]:
                    too_few.append(move)
                else:
                    too_many.append(move)

                # Average price computation
                if (move.product_id.cost_method == 'average'):
                    product = product_obj.browse(cr, uid, move.product_id.id)
                    move_currency_id = move.company_id.currency_id.id
                    context['currency_id'] = move_currency_id
                    qty = uom_obj._compute_qty(cr, uid, product_uom, product_qty, product.uom_id.id)

                    if product.id not in product_avail:
                        # keep track of stock on hand including processed lines not yet marked as done
                        product_avail[product.id] = product.qty_available

                    if qty > 0:
                        new_price = currency_obj.compute(cr, uid, product_currency,
                                                         move_currency_id, product_price, round=False)
                        new_price = uom_obj._compute_price(cr, uid, product_uom, new_price,
                                                           product.uom_id.id)
                        if product_avail[product.id] < 0:
                            product_avail[product.id] = 0
                        else:
                            # Get the standard price
                            amount_unit = product.price_get('standard_price', context=context)[product.id]
                            amount_unit_no_round = product.standard_price

                            old_total_val = 0
                            new_total_val = 0
                            new_avg_price = 0

                            if (move.type == 'in'):
                                old_total_val = product.inv_val
                                new_total_val = old_total_val + (new_price * qty)
                                new_avg_price = new_total_val / (qty + product_avail[product.id])

                                self.pool.get('ec.auths.kardex.line').create(cr, uid, {'name': move.picking_id.name,
                                                                                       'date': datetime.now(),
                                                                                       'product_id': product.id,
                                                                                       'type': 'input',
                                                                                       'c_qty': qty,
                                                                                       'c_cost': new_price,
                                                                                       'c_total': new_price * qty,
                                                                                       'new_qty': product_avail[
                                                                                                      product.id] + qty,
                                                                                       'new_total': new_total_val,
                                                                                       'new_avg': new_avg_price,
                                                                                       'move_id': move.id})

                                product_avail[product.id] += qty

                                product_obj.write(cr, uid, [product.id],
                                                  {'standard_price': new_avg_price, 'inv_val': new_total_val})

                            elif (move.type == 'out'):

                                old_total_val = product.inv_val
                                new_total_val = old_total_val - (new_price * qty)
                                if ((product_avail[product.id] - qty) == 0):
                                    new_avg_price = 0
                                else:
                                    new_avg_price = new_total_val / (product_avail[product.id] - qty)

                                self.pool.get('ec.auths.kardex.line').create(cr, uid, {'name': move.picking_id.name,
                                                                                       'date': datetime.now(),
                                                                                       'product_id': product.id,
                                                                                       'type': 'output',
                                                                                       's_qty': qty,
                                                                                       's_cost': new_price,
                                                                                       's_total': new_price * qty,
                                                                                       'new_qty': product_avail[
                                                                                                      product.id] - qty,
                                                                                       'new_total': new_total_val,
                                                                                       'new_avg': new_avg_price,
                                                                                       'move_id': move.id})

                                product_avail[product.id] -= qty

                                product_obj.write(cr, uid, [product.id],
                                                  {'standard_price': new_avg_price, 'inv_val': new_total_val})

                            elif (move.type == 'internal'):
                                old_total_val = product.inv_val

                                self.pool.get('ec.auths.kardex.line').create(cr, uid, {'name': move.picking_id.name,
                                                                                       'date': datetime.now(),
                                                                                       'product_id': product.id,
                                                                                       'type': 'internal',
                                                                                       's_qty': qty,
                                                                                       's_cost': product.standard_price,
                                                                                       's_total': product.standard_price * qty,
                                                                                       'c_qty': qty,
                                                                                       'c_cost': product.standard_price,
                                                                                       'c_total': product.standard_price * qty,
                                                                                       'new_qty': product.qty_available,
                                                                                       'new_total': old_total_val,
                                                                                       'new_avg': product.standard_price,
                                                                                       'move_id': move.id})

                            move_obj.write(cr, uid, [move.id],
                                           {'price_unit': product_price,
                                            'price_currency_id': product_currency})

            for move in too_few:
                product_qty = move_product_qty[move.id]
                if not new_picking:
                    new_picking_name = pick.name
                    self.write(cr, uid, [pick.id],
                               {'name': sequence_obj.get(cr, uid,
                                                         'stock.picking.%s' % (pick.type)),
                                })
                    new_picking = self.copy(cr, uid, pick.id,
                                            {
                                                'name': new_picking_name,
                                                'move_lines': [],
                                                'state': 'draft',
                                                })
                if product_qty != 0:
                    defaults = {
                        'product_qty': product_qty,
                        'product_uos_qty': product_qty,  #TODO: put correct uos_qty
                        'picking_id': new_picking,
                        'state': 'assigned',
                        'move_dest_id': False,
                        'price_unit': move.price_unit,
                        'product_uom': product_uoms[move.id]
                    }
                    prodlot_id = prodlot_ids[move.id]
                    if prodlot_id:
                        defaults.update(prodlot_id=prodlot_id)
                    move_obj.copy(cr, uid, move.id, defaults)
                move_obj.write(cr, uid, [move.id],
                               {
                                   'product_qty': move.product_qty - partial_qty[move.id],
                                   'product_uos_qty': move.product_qty - partial_qty[move.id],
                                   #TODO: put correct uos_qty
                                   'prodlot_id': False,
                                   'tracking_id': False,
                                   })

            if new_picking:
                move_obj.write(cr, uid, [c.id for c in complete], {'picking_id': new_picking})
            for move in complete:
                defaults = {'product_uom': product_uoms[move.id], 'product_qty': move_product_qty[move.id]}
                if prodlot_ids.get(move.id):
                    defaults.update({'prodlot_id': prodlot_ids[move.id]})
                move_obj.write(cr, uid, [move.id], defaults)
            for move in too_many:
                product_qty = move_product_qty[move.id]
                defaults = {
                    'product_qty': product_qty,
                    'product_uos_qty': product_qty,  #TODO: put correct uos_qty
                    'product_uom': product_uoms[move.id]
                }
                prodlot_id = prodlot_ids.get(move.id)
                if prodlot_ids.get(move.id):
                    defaults.update(prodlot_id=prodlot_id)
                if new_picking:
                    defaults.update(picking_id=new_picking)
                move_obj.write(cr, uid, [move.id], defaults)

            # At first we confirm the new picking (if necessary)
            if new_picking:
                wf_service.trg_validate(uid, 'stock.picking', new_picking, 'button_confirm', cr)
                # Then we finish the good picking
                self.write(cr, uid, [pick.id], {'backorder_id': new_picking})
                self.action_move(cr, uid, [new_picking], context=context)
                wf_service.trg_validate(uid, 'stock.picking', new_picking, 'button_done', cr)
                wf_service.trg_write(uid, 'stock.picking', pick.id, cr)
                delivered_pack_id = pick.id
                back_order_name = self.browse(cr, uid, delivered_pack_id, context=context).name
                self.message_post(cr, uid, new_picking,
                                  body=_("Back order <em>%s</em> has been <b>created</b>.") % (back_order_name),
                                  context=context)
            else:
                self.action_move(cr, uid, [pick.id], context=context)
                wf_service.trg_validate(uid, 'stock.picking', pick.id, 'button_done', cr)
                delivered_pack_id = pick.id

            delivered_pack = self.browse(cr, uid, delivered_pack_id, context=context)
            res[pick.id] = {'delivered_picking': delivered_pack.id or False}

        return res


class stock_location(osv.osv):
    _inherit = 'stock.location'

    _columns = {
        'ec_auths_source_choice': fields.boolean('Fuente?'),
        'ec_auths_dest_choice': fields.boolean('Destino?'),
        }


class ec_print_kardex_wizard(osv.osv_memory):
    _name = 'ec.print.kardex.wizard'

    _columns = {
        'fecha_init': fields.date(string='Fecha Inicio'),
        'fecha_term': fields.date(string='fecha de entrega'),
        'product_id': fields.many2one('product.product', string="Producto"),
        }


    def generate_kardex_report(self, cr, uid, ids, context=None):
        records = self.browse(cr, uid, ids)[0]
        kardex_obj = self.pool.get('ec.auths.kardex.line')
        kardex = kardex_obj.search(cr, uid, [('product_id.id', '=', records.product_id.id)], )

        result = {'type': 'ir.actions.report.xml',
                  'context': context,
                  'report_name': 'reporte_kardex',
                  'datas':
                      {'ids': kardex}
                  }

        return result


class ec_print_kardex_product_wizard(osv.osv_memory):
    _name = 'ec.print.kardex.product.wizard'

    _columns = {
        'product_id': fields.many2one('product.product', string="Producto"),
        }


    def generate_kardex_product_report(self, cr, uid, ids, context=None):
        records = self.browse(cr, uid, ids)[0]
        return {
            'type': 'ir.actions.act_window',
            'name': 'Abrir Kardex',
            'view_mode': 'tree',
            'view_type': 'form',
            'res_model': 'ec.auths.kardex.line',
            'nodestroy': 'true',
            'domain': '[("product_id","=",' + str(records.product_id.id) + ')]',
            }


class ec_product_report_wizard(osv.osv_memory):
    _name = 'ec.product.report.wizard'


    def generate_product_report(self, cr, uid, ids, context=None):
        records = self.browse(cr, uid, ids)[0]
        prod_obj = self.pool.get('product.product')
        context.update({'location': int(records.ubi.id)})
        context.update({'location_name': records.ubi.name})
        #context.update({'compute_child':False})
        prod = prod_obj.search(cr, uid, [])  #hacemos una lista de todos los ids

        ids_with_cant = []
        #move_obj = self.pool.get('stock.move')
        for p in prod_obj.browse(cr, uid, prod, context):
            if p.qty_available > 0:
                ids_with_cant.append(p.id)

        result = {'type': 'ir.actions.report.xml',
                  'context': context,
                  'report_name': 'cant_por_ubi_prod',
                  'datas':
                      {'ids': ids_with_cant}
                  }

        return result

    _columns = {
        'ubi': fields.many2one('stock.location', string='Ubicacion')
    }
