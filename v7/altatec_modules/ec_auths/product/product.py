import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree
import openerp.addons.decimal_precision as dp
import openerp.exceptions

from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging
import xml.etree.cElementTree as ET


_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')

class ec_auths_material_familia(osv.osv):
        _name= 'ec.auths.material.familia'
        
        _columns={
                  'name':fields.char(string="Nombre"),
                  'desc':fields.char(string="Descripcion"),
                  
                  }

class ec_auths_material_tipo(osv.osv):
        _name= 'ec.auths.material.tipo'
        
        _columns={
                  'name':fields.char(string="Nombre"),
                  'desc':fields.char(string="Descripcion"),
                  
                  }
        
class ec_auths_material_descrip(osv.osv):
        _name= 'ec.auths.material.descrip'
        
        _columns={
                  'name':fields.char(string="Nombre"),
                  'desc':fields.char(string="Descripcion"),
                  
                  }
        
class ec_auths_material_medida(osv.osv):
        _name= 'ec.auths.material.medida'
        
        _columns={
                  'name':fields.char(string="Nombre"),
                  'desc':fields.char(string="Descripcion"),
                  
                  }
        
class ec_auths_material_modelo(osv.osv):
        _name= 'ec.auths.material.modelo'
        
        _columns={
                  'name':fields.char(string="Nombre"),
                  'desc':fields.char(string="Descripcion"),
                  
                  }        
        

class product_product(osv.osv):
        _inherit = 'product.product'
        
        def get_codigo_2(self,cr,uid,ids,field_name,field_value,arg,context=None):
                records = self.browse(cr,uid,ids)
                result ={}
                codigo=""
                
                for r in records:
                        if(r.ec_familia.name!=None):
                            codigo+=str(r.ec_familia.name)
                        if r.ec_descrip != False:
                            codigo+=str(r.ec_descrip)
               
                result[r.id]=codigo
                
                return result
        
        def get_codigo_3(self,cr,uid,ids,field_name,field_value,arg,context=None):
                records = self.browse(cr,uid,ids)
                result ={}
                codigo=""
                
                for r in records:
                        if( r.ec_familia.name != None):
                            codigo+=str(r.ec_familia.name)
                        if ( r.ec_descrip != False):
                            codigo+=str(r.ec_descrip)
                        if( r.ec_tipo!= False):
                            codigo+=str(r.ec_tipo)
                
                result[r.id]=codigo
                
                return result
        
        def get_codigo_4(self,cr,uid,ids,field_name,field_value,arg,context=None):
                records = self.browse(cr,uid,ids)
                result ={}
                codigo=""
                
                for r in records:
                        if( r.ec_familia.name != None):
                            codigo+=str(r.ec_familia.name)
                        if ( r.ec_descrip != False):
                            codigo+=str(r.ec_descrip)
                        if( r.ec_tipo!= False):
                            codigo+=str(r.ec_tipo)
                        if( r.ec_modelo != False):
                            codigo += str(r.ec_modelo)
                
                result[r.id]=codigo
                
                return result

       
        def get_ids(self,cr,uid,ids,context={}):
                return ids
        
        def on_change_code(self,cr, uid, ids, ec_familia, ec_descrip, context):
            
            prod_ids=self.pool.get('product.product').search(cr,uid,[('ec_familia','=',ec_familia),('ec_descrip','=',ec_descrip)])
            prod_objs = self.pool.get('product.product').browse(cr,uid,prod_ids,context)
            
            tipo_list=[]
            
            for obj in prod_objs:
                tipo_list.append(obj.ec_tipo)
                
            tipo_list.sort()
            
            if len(tipo_list) > 0:
                new_code = str(int(tipo_list[-1]) + 1).zfill(3)
                return {'value':{'ec_tipo': new_code}}
            else:
                return {'value':{'ec_tipo':'001'}}
            
        _columns = {      
                        'ec_familia':fields.many2one('ec.auths.material.familia',"Familia"),
                        'ec_tipo':fields.char("Tipo"),
                        'ec_descrip':fields.char("Descripcion"),
                        #'ec_medida':fields.many2one('ec.auths.material.medida',"Codigo de Modelo"),
                        'ec_modelo':fields.char("Marca"),
                        'default_code_2': fields.function(get_codigo_2, type="char", string="Nivel 1",
                                        store={'product.product':(get_ids,['ec_familia','ec_descrip',],10),}   ),     
                        'default_code_3': fields.function(get_codigo_3, type="char", string="Nivel 2",
                                        store={'product.product':(get_ids,['ec_familia','ec_descrip','ec_tipo',],10),} ),
                        'default_code': fields.function(get_codigo_4, type="char", size=64, select=True, readonly=True, string="Referencia Interna",
                                        store = {'product.product':(get_ids,['ec_familia','ec_descrip','ec_tipo','ec_modelo'],10),} ),
                                                    
                }
        
    
        
        