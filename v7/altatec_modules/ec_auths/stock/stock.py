import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree
import openerp.addons.decimal_precision as dp
import openerp.exceptions

from openerp import netsvc
from openerp import pooler
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging

import xml.etree.cElementTree as ET


_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')

