from report import report_sxw
from report.report_sxw import rml_parse
import pooler
import logging
_logger = logging.getLogger(__name__)



class Parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_locations':self.get_locations,
            'get_qty':self.get_qty,
            'get_big_data':self.get_big_data,
            'cr':cr,
            'uid':uid,
            'g_context':context,
        })
        
    def get_locations(self, with_name):
        
        names=[]
        
        if with_name:
            class Object(object):
                pass
            fake_loc = Object()
            fake_loc.name = "Producto"
            names = names+[fake_loc]
        else:
            class Object(object):
                pass
            fake_loc = Object()
            fake_loc.id = -99
            names = names+[fake_loc]

        fake_loc2 = Object()
        fake_loc2.id=-96
        fake_loc2.name = "Codigo de Producto"
        names=names+[fake_loc2]

        fake_loc3 = Object()
        fake_loc3.id=-97
        fake_loc3.name = "Modelo"
        names=names+[fake_loc3]



        location_ids = self.localcontext['report_locations']
        names= names + self.pool.get('stock.location').browse(self.localcontext['cr'], self.localcontext['uid'],location_ids,context=self.localcontext['g_context'])

        fake_loc4 = Object()
        fake_loc4.id=-98
        fake_loc4.name = "Total"
        names=names+[fake_loc4]

        return names

    def get_big_data(self,objects):
        id_list = []
        for o in objects:
            id_list.append(o.id)
            
        location_ids = self.localcontext['report_locations']
        product_pool = self.pool.get('product.product')
        
        big_data = [{}]
        total={}
        cont=0
        for location in location_ids:

            self.localcontext['g_context'].update({'states': ('done',), 'what': ('in', 'out'), 'location': location})
            loc_stock = product_pool.get_product_available(self.localcontext['cr'], self.localcontext['uid'], id_list, context=self.localcontext['g_context'])
            big_data[0].update({location:loc_stock})
            for t_id, t_value in loc_stock.items():
                if cont==0:
                    total[t_id]=t_value
                else:
                    s_tu=total[t_id]+t_value
                    total[t_id]=s_tu
            cont=cont+1

        
        names = {}
        codigo = {}
        modelo = {}

        for prod in product_pool.browse(self.localcontext['cr'], self.localcontext['uid'], id_list, context=self.localcontext['g_context']):
            names[prod.id] = prod.name
            codigo[prod.id] = prod.default_code
            modelo[prod.id] = prod.modelo
            
        big_data[0].update({-99:names,-98:total,-97:modelo,-96:codigo})

        return big_data

    def get_qty(self,location,product):

        if(location.name=="Producto"):
            return {product.id:product.name}
        
        self.localcontext['g_context'].update({'states': ('done',), 'what': ('in', 'out'), 'location': location.id})
        product_pool = self.pool.get('product.product')
        qty = product_pool.get_product_available(self.localcontext['cr'], self.localcontext['uid'], [product.id], context=self.localcontext['g_context'])

        return qty