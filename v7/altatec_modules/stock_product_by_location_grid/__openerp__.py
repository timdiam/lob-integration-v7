{
    'name': 'Stock product by location grid',
    'version': '1.0',
    'description': """
        This modules creates a report showing the list of products, each line showing the stock at a specific (configurable) location
    """,
    'author': 'Tim Diamond',
    'website': 'www.altatececuador.com',
    "depends" : [ 'account' ],
    "data" : [ 'stock_product_by_location_grid.xml', 'data/report.xml'],
    "installable": True,
    "auto_install": False
}