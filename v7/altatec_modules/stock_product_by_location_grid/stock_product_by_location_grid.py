# -*- coding: utf-8 -*-
#################################################################################
#
# A module that contains a credit card model can be attached to other
# objects (e.g. invoices).
#
# Author:  Tim Diamond
# Company: AltaTec Ecuador
# Date:    Jan. 17th, 2014
#
#################################################################################
from openerp.osv import fields, osv, orm

class wizard_grid_location_line(osv.osv_memory):

    _name='wizard.grid.location.line'
    
    _columns = {
            'location_id':fields.many2one('stock.location',string='Ubicacion'),
            'wizard':fields.many2one('generate.product.grid.report', string='Wizard'),
           }


class generate_product_grid_report(osv.osv_memory):

    _name='generate.product.grid.report'
    
    def generate_report(self, cr, uid, ids, context=None):
        records=self.browse(cr,uid,ids)[0]    
        prod_obj=self.pool.get('product.product')
        ids_for_report=[]
        
        prods = prod_obj.search(cr,uid,[]) #hacemos una lista de todos los ids
        
        if(records.all_products):
            ids_for_report = prods
        else:
            for p in prod_obj.browse(cr,uid,prods,context):
                if p.qty_available > 0:
                    ids_for_report.append(p.id)
                    
        report_locations = []
        if(records.location_ids):
            for l in records.location_ids:
                report_locations.append(l.location_id.id)
            
        context.update({'report_locations':report_locations})

        result = {'type' :'ir.actions.report.xml',
                  'context':context,
                  'report_name':'product_location_grid',
                  'datas':
                            {
                             'ids':ids_for_report}
                            }

        return result
    
    def get_default_locations(self, cr, uid, context):
        l_ids = self.pool.get('stock.location').search(cr,uid,[('show_in_report','=',True)],context=context)
        lines = []
        for l_id in l_ids:
            lines.append([0,0,{'location_id':l_id}])
        return lines
        

    _columns = {
            'location_ids':fields.one2many('wizard.grid.location.line', 'wizard', string='Ubicacion'),
            'all_products':fields.boolean(string="Incluir productos con stock = zero?"),
            'include_children':fields.boolean(string="Incluir ubicaciones hijos?"),
        }
    
    _defaults = {
                 'location_ids': get_default_locations,
                 'all_products': False,
                 'include_children':True,
                 }

class stock_location(osv.osv):
    _inherit = 'stock.location'

    _columns = {
                'show_in_report':fields.boolean('Sale en reporte?'),
                }
    
    _defaults = {'show_in_report':False}

