{ 'name'         : 'AltaTec Account Move View Improvements',
  'version'      : '1.0',
  'description'  : """
                   This module changes a few view related things for account moves. It removes the filters that
                   appear at the top of the account.move.line tree view, changes that same view so that it's not
                   inline-editable, and it changes the account.move form view to remove the sheet.
                   """,
  'author'       : 'Dan Haggerty',
  'website'      : 'www.altatec.ec',
  "depends"      : [ 'account',
                     'web_hide_duplicate',
                   ],
  "data"         : [ 'account_move.xml',
                   ],
  "css" :['static/account_move.css'],
  "installable"  : True,
  "auto_install" : False
}
