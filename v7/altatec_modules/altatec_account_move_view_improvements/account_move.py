# -*- coding: utf-8 -*-
#################################################################################
#
# This file adds a "type" seletion field to account moves, which has the options
# "manual" and "normal". Normal is the default and is used to represent account
# moves created by other documents (invoices, stock moves etc). Manual represents
# account moves that were created manually in the "Asientos manuales" menu
# added by this module.
#
# Author:  Dan Haggerty
# Company: AltaTec
# Date:    10/4/2015
#
#################################################################################
from openerp.osv import fields, osv, orm
from lxml import etree

#####################################################################################
# Definición de la clase cross_company
#####################################################################################
class account_move(osv.osv):

    _inherit = 'account.move'

    #####################################################################################
    # Remove the create/duplicate/delete buttons when we're in the "Asientos contables"
    # menu, but leave them if we're in the "Asientos manuales" menu.
    #####################################################################################
    def fields_view_get(self, cr, uid, view_id=None, view_type=False, context=None, toolbar=False, submenu=False):

        if not context: context = {}
        res = super(account_move, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        if( view_type == "tree" ):
            nodes = doc.xpath("//tree")
            if context.get('default_manual_type', False) == 'normal':
                for node in nodes:
                    node.set('create',"false")
                    node.set('delete',"false")
                res['arch'] = etree.tostring(doc)

        elif( view_type == "form" ):
            nodes = doc.xpath("//form")
            if context.get('default_manual_type', False) == 'normal':
                for node in nodes:
                    node.set('create',"false")
                    node.set('duplicate',"false")
                    node.set('delete',"false")
                res['arch'] = etree.tostring(doc)
        return res

    #####################################################################################
    # Add a "type" field that can be either "manual" or "normal". Set "normal" as default
    #####################################################################################
    _columns = { "manual_type" : fields.selection(string="Tipo", selection=[("normal","Normal"),("manual","Manual")], required=True ),
               }

    _defaults = { "manual_type" : "normal",
                }