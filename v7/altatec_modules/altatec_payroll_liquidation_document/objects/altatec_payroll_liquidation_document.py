from openerp.osv import fields, osv, orm
import logging
import calendar
import time
from datetime import datetime

_logger = logging.getLogger(__name__)


class altatec_payroll_liquidation_document(osv.osv):
    _name= 'altatec.payroll.liquidation.document'

    def _borrar_lineas(self, cr, uid, ids, objeto, context):
        lista_a_borrar = []

        for linea in objeto.decimo_line:
            lista_a_borrar.append((3, linea.id))

            self.write(cr, uid, ids, {'decimo_line': lista_a_borrar}, context=context)

        return True

    def generar_reporte_payroll(self, cr, uid, ids, arg, context={}):

        obj=self.pool.get('altatec.payroll.liquidation.document').browse(cr, uid, ids, context)

        self._borrar_lineas(cr,uid,ids,obj[0],context)
        #
        move_lines_obj=self.pool.get('account.move.line')
        empleados_obj=self.pool.get('res.partner')

        employee_obj=self.pool.get('hr.employee')


        company = self.pool.get('res.users').browse(cr,uid,uid,context).company_id


        list_of_accounts=[]
        search_list=[]
        aniofiscalInicio=int(obj[0].fiscal_year.name)
        aniofinalFinal=int(obj[0].fiscal_year.name)+1
        fechaComienzo=None
        fechaFinal=None
        mes_inicio=None
        mes_final=None
        criteria_decimo_tercero=None
        criteria_decimo_cuarto=None
        criteria=None
        argumentos=[]



        if obj[0].decimo_filter=='decimo_tercero_pagado':
            list_of_accounts=company.cuenta_a
            argumentos.append(('contract_id.pagar_decimo_13','=',True))
        elif obj[0].decimo_filter=='decimo_tercero_provisional':
            list_of_accounts=company.cuenta_c
            argumentos.append(('contract_id.pagar_decimo_13','=',False))
        elif obj[0].decimo_filter=='decimo_cuarto_pagado':
            list_of_accounts=company.cuenta_b
            argumentos.append(('contract_id.pagar_decimo_14','=',True))
        else:
            list_of_accounts=company.cuenta_d
            argumentos.append(('contract_id.pagar_decimo_13','=',False))

        for cuenta in list_of_accounts:
            search_list.append(cuenta.id)

        list_moves_id=move_lines_obj.search(cr, uid, [('account_id', 'in', search_list)])
        list_moves_objs=move_lines_obj.browse(cr, uid, list_moves_id, context)
        full_list_empleados=[]

        for moves in list_moves_objs:
            if moves.partner_id:
                full_list_empleados.append(moves.partner_id.id)

        full_list_empleados2=list(set(full_list_empleados))
        argumentos.append(('address_home_id','in',full_list_empleados2))
        argumentos.append(('active','=',True))

        list_employee_active=employee_obj.search(cr, uid,argumentos)
        list_obj_employee_active=employee_obj.browse(cr,uid,list_employee_active,context)

        full_list_obj_empleados=empleados_obj.browse(cr, uid, full_list_empleados2, context)

        # for empleado in full_list_obj_empleados:
        for empleado in list_obj_employee_active:
            # Create line with name and no total (total=0)

            line= self.pool.get('decimo.line').create(cr,uid,{
                # 'cedula':empleado.name+"/"+str(empleado.id),
                'cedula':empleado.name,
                'total':0.0,
                'liquidation_line':obj[0].id
            })

            # Get account from company and dates from contract
            if obj[0].decimo_filter=='decimo_tercero_pagado':
                if empleado.address_home_id.region=='costa':
                    mes_inicio=int(company.inicio_tercero_costa)
                    if mes_inicio==1:
                        mes_final=12
                    else:
                        mes_final=mes_inicio-1
                    tuplaFinal=calendar.monthrange(aniofinalFinal,mes_final)
                    fechaComienzo=datetime.strptime(str(aniofiscalInicio)+"-"+str(mes_inicio)+"-1",'%Y-%m-%d')
                    fechaFinal=datetime.strptime(str(aniofinalFinal)+"-"+str(mes_final)+"-"+str(tuplaFinal[1]),'%Y-%m-%d')
                else:
                    mes_inicio=int(company.inicio_tercero_sierra)
                    mes_final=mes_inicio-1
                    tuplaFinal=calendar.monthrange(aniofinalFinal,mes_final)
                    fechaComienzo=datetime.strptime(str(aniofiscalInicio)+"-"+str(mes_inicio)+"-1",'%Y-%m-%d')
                    fechaFinal=datetime.strptime(str(aniofinalFinal)+"-"+str(mes_final)+"-"+str(tuplaFinal[1]),'%Y-%m-%d')
            elif obj[0].decimo_filter=='decimo_tercero_provisional':
                if empleado.address_home_id.region=='costa':
                    mes_inicio=int(company.inicio_tercero_costa)
                    if mes_inicio==1:
                        mes_final=12
                    else:
                        mes_final=mes_inicio-1
                    tuplaFinal=calendar.monthrange(aniofinalFinal,mes_final)
                    fechaComienzo=datetime.strptime(str(aniofiscalInicio)+"-"+str(mes_inicio)+"-1",'%Y-%m-%d')
                    fechaFinal=datetime.strptime(str(aniofinalFinal)+"-"+str(mes_final)+"-"+str(tuplaFinal[1]),'%Y-%m-%d')
                else:
                    mes_inicio=int(company.inicio_tercero_sierra)
                    mes_final=mes_inicio-1
                    tuplaFinal=calendar.monthrange(aniofinalFinal,mes_final)
                    fechaComienzo=datetime.strptime(str(aniofiscalInicio)+"-"+str(mes_inicio)+"-1",'%Y-%m-%d')
                    fechaFinal=datetime.strptime(str(aniofinalFinal)+"-"+str(mes_final)+"-"+str(tuplaFinal[1]),'%Y-%m-%d')
            elif obj[0].decimo_filter=='decimo_cuarto_pagado':
                if empleado.address_home_id.region=='costa':
                    mes_inicio=int(company.inicio_cuarto_costa)
                    if mes_inicio==1:
                        mes_final=12
                    else:
                        mes_final=mes_inicio-1
                    tuplaFinal=calendar.monthrange(aniofinalFinal,mes_final)
                    fechaComienzo=datetime.strptime(str(aniofiscalInicio)+"-"+str(mes_inicio)+"-1",'%Y-%m-%d')
                    fechaFinal=datetime.strptime(str(aniofinalFinal)+"-"+str(mes_final)+"-"+str(tuplaFinal[1]),'%Y-%m-%d')
                else:
                    mes_inicio=int(company.inicio_tercero_sierra)
                    mes_final=mes_inicio-1
                    tuplaFinal=calendar.monthrange(aniofinalFinal,mes_final)
                    fechaComienzo=datetime.strptime(str(aniofiscalInicio)+"-"+str(mes_inicio)+"-1",'%Y-%m-%d')
                    fechaFinal=datetime.strptime(str(aniofinalFinal)+"-"+str(mes_final)+"-"+str(tuplaFinal[1]),'%Y-%m-%d')
            else:
                if empleado.address_home_id.region=='costa':
                    mes_inicio=int(company.inicio_cuarto_costa)
                    if mes_inicio==1:
                        mes_final=12
                    else:
                        mes_final=mes_inicio-1
                    tuplaFinal=calendar.monthrange(aniofinalFinal,mes_final)
                    fechaComienzo=datetime.strptime(str(aniofiscalInicio)+"-"+str(mes_inicio)+"-1",'%Y-%m-%d')
                    fechaFinal=datetime.strptime(str(aniofinalFinal)+"-"+str(mes_final)+"-"+str(tuplaFinal[1]),'%Y-%m-%d')
                else:
                    mes_inicio=int(company.inicio_tercero_sierra)
                    mes_final=mes_inicio-1
                    tuplaFinal=calendar.monthrange(aniofinalFinal,mes_final)
                    fechaComienzo=datetime.strptime(str(aniofiscalInicio)+"-"+str(mes_inicio)+"-1",'%Y-%m-%d')
                    fechaFinal=datetime.strptime(str(aniofinalFinal)+"-"+str(mes_final)+"-"+str(tuplaFinal[1]),'%Y-%m-%d')

            total=0
            # Find move lines with the dates, partner_id and account_id as filters
            move_lines=self.pool.get('account.move.line').search(cr, uid,
                                                                 [
                                                                     ('move_id.period_id.date_start','>=',fechaComienzo.date()),
                                                                     ('move_id.period_id.date_stop', '<=', fechaFinal.date()),
                                                                     ('partner_id','=',empleado.address_home_id.id),
                                                                     ('account_id','in',search_list)
                                                                 ])
            for move in self.pool.get('account.move.line').browse(cr, uid, move_lines, context):
                id_detail=self.pool.get('decimo.detail').search(cr,uid,[('periodo','=',move.period_id.name),('decimo_line_rel','=',line)])
                detail_before=self.pool.get('decimo.detail').browse(cr,uid,id_detail)

                # If there is another line with period we update, else we create
                if detail_before:
                    total_anterior=detail_before[0].valor_periodo
                    total_acu=total_anterior+move.credit-move.debit
                    self.pool.get('decimo.detail').write(cr,uid,id_detail,{'valor_periodo':total_acu})
                else:
                    self.pool.get('decimo.detail').create(cr,uid,{
                        'periodo':move.period_id.name,
                        'valor_periodo':move.credit-move.debit,
                        'decimo_line_rel':line,
                    })
                if move.credit:
                    total+=move.credit
                if move.debit:
                    total-=move.debit
            if self.pool.get('decimo.line').browse(cr, uid, line, context).detalle_pagos:
                self.pool.get('decimo.line').write(cr,uid,line,{'total':total})
            else:
                # In case it didnt return move lines to this employee we "delete" the decimo_line
                self.pool.get('decimo.line').unlink(cr, uid, line, context)


        return True

    _columns={
        'fiscal_year'                   : fields.many2one('account.fiscalyear','Inicio del Ano Fiscal',required=True),
        'report_form'                   : fields.selection([('detallado', 'Detallado'),
                                                    ('consolidad', 'Consolidado')], 'Report Form'),
        'decimo_filter'                 : fields.selection([('decimo_cuarto_pagado', 'Decimo Cuarto Pagado'),
                                                    ('decimo_tercero_pagado', 'Decimo Tercero Pagado'),
                                                    ('decimo_tercero_provisional', 'Decimo Tercero Prov'),
                                                    ('decimo_cuarto_provisional', 'Decimo Cuarto Provisional')], 'Filtro de Decimo',required=True),
        'payroll_benefit'               : fields.selection([('mensual', 'Decimo Mensualizado'),
                                                    ('acumulado', 'Decimo Acumulado')], 'Tipo de pago'),
        'decimo_line'                   : fields.one2many('decimo.line', 'liquidation_line', "Detalle Decimo"),
        'state'                         : fields.selection([('draft', 'Borrador'), ('done', 'Aprobado')], 'State', readonly=True),
    }

    _defaults={
        'state':'draft'
    }

class decimo_line(osv.osv):
    _name='decimo.line'

    _columns={
        'cedula'    :fields.char('Cedula'),
        'total'    :fields.float('Total'),
        'retencion':fields.float('Retenciones J'),
        'detalle_pagos':fields.one2many('decimo.detail','decimo_line_rel', "Detalle Decimo", ondelete="cascade"),
        'liquidation_line'   : fields.many2one('altatec.payroll.liquidation.document',"Payroll Liquidation Document")
    }

class decimo_detail(osv.osv):
    _name='decimo.detail'

    _columns={
        'periodo'    :fields.char('Periodo'),
        'valor_periodo'    :fields.float('Valor'),
        'decimo_line_rel'   : fields.many2one('decimo.line',"Decimo Line")
    }

class payroll_benefit_consolidated_line(osv.osv):
     _name='payroll.benefit.consolidated.line'

     _columns={
        'cedula'                            : fields.char('Cedula',),
        'employee_id'                       : fields.many2one('hr.employee', "Empleado"),
        'ocupacion'                         : fields.char('Ocupacion',),
        'genero'                            : fields.char('Genero',),
        'dias_trabajados'                   : fields.char("Dias Trabajados"),
        'total_ganado'                      : fields.char("Total Ganado"),
        'retencion'                         : fields.char("Retencion J"),
        'valor_decimo'                      : fields.char("Valor Decimo"),
        'forma_pago'                        : fields.char("Forma de Pago"),
        'liquidation_line_consolidated'     :fields.many2one('altatec.payroll.liquidation.document',"Payroll Liquidation Document"),
    }