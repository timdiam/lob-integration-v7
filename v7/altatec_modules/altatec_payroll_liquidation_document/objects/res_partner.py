from openerp.osv import fields, osv, orm


class res_partner(osv.osv):

        _inherit= 'res.partner'

        _columns={
            'region'    : fields.selection([('costa', 'Costa'),
                                                    ('sierra', 'Sierra')], 'Region de trabajo'),
        }

        _defaults={
            'region'    :'costa',
        }