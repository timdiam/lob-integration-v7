from openerp.osv import fields, osv

class res_company(osv.osv):
    _inherit = "res.company"

    _columns = {
        'inicio_tercero_costa':fields.selection([
            ('1', 'Enero'),
            ('2', 'Febrero'),
            ('3', 'Marzo'),
            ('4', 'Abril'),
            ('5', 'Mayo'),
            ('6', 'Junio'),
            ('7', 'Julio'),
            ('8', 'Agosto'),
            ('9', 'Septiembre'),
            ('10', 'Octubre'),
            ('11', 'Noviembre'),
            ('12', 'Diciembre'),
        ], 'Incio del termino del Decimo Tercer Sueldo Costa'),
        'inicio_tercero_sierra':fields.selection([
            ('1', 'Enero'),
            ('2', 'Febrero'),
            ('3', 'Marzo'),
            ('4', 'Abril'),
            ('5', 'Mayo'),
            ('6', 'Junio'),
            ('7', 'Julio'),
            ('8', 'Agosto'),
            ('9', 'Septiembre'),
            ('10', 'Octubre'),
            ('11', 'Noviembre'),
            ('12', 'Diciembre'),
        ], 'Incio del termino del Decimo Tercer Sueldo Sierra'),
        'inicio_cuarto':fields.selection([
            ('1', 'Enero'),
            ('2', 'Febrero'),
            ('3', 'Marzo'),
            ('4', 'Abril'),
            ('5', 'Mayo'),
            ('6', 'Junio'),
            ('7', 'Julio'),
            ('8', 'Agosto'),
            ('9', 'Septiembre'),
            ('10', 'Octubre'),
            ('11', 'Noviembre'),
            ('12', 'Diciembre'),
        ], 'Incio del termino del Decimo Cuarto Sueldo'),
        'inicio_cuarto_costa':fields.selection([
            ('1', 'Enero'),
            ('2', 'Febrero'),
            ('3', 'Marzo'),
            ('4', 'Abril'),
            ('5', 'Mayo'),
            ('6', 'Junio'),
            ('7', 'Julio'),
            ('8', 'Agosto'),
            ('9', 'Septiembre'),
            ('10', 'Octubre'),
            ('11', 'Noviembre'),
            ('12', 'Diciembre'),
        ], 'Incio del termino del Decimo Cuarto Sueldo Costa'),
        'inicio_cuarto_sierra':fields.selection([
            ('1', 'Enero'),
            ('2', 'Febrero'),
            ('3', 'Marzo'),
            ('4', 'Abril'),
            ('5', 'Mayo'),
            ('6', 'Junio'),
            ('7', 'Julio'),
            ('8', 'Agosto'),
            ('9', 'Septiembre'),
            ('10', 'Octubre'),
            ('11', 'Noviembre'),
            ('12', 'Diciembre'),
        ], 'Incio del termino del Decimo Cuarto Sueldo Sierra'),

        'cuenta_a': fields.many2many(
                'account.account',
                'company_rel_cuenta_a',
                'company_id_a',
                'account_id_a',
                string="Decimo Tercero Pagado"
                            ),
        'cuenta_b': fields.many2many(
                'account.account',
                'company_rel_cuenta_b',
                'company_id_b',
                'account_id_b',
                string="Decimo Cuato Pagado"
                            ),
        'cuenta_c': fields.many2many(
                'account.account',
                'company_rel_cuenta_c',
                'company_id_c',
                'account_id_c',
                string="Decimo Tercero Prov"
                            ),
        'cuenta_d': fields.many2many(
                'account.account',
                'company_rel_cuenta_d',
                'company_id_d',
                'account_id_d',
                string="Decimo Cuarto Prov"
                            ),
    }


class account_account(osv.osv):
    _inherit = "account.account"
    _columns = {
        'compania_a': fields.many2many(
                            'res.company',
                            'company_rel_cuenta_a',
                            'account_id_a',
                            'company_id_a',
                            string="Compania A"
                                    ),
        'compania_b': fields.many2many(
                            'res.company',
                            'company_rel_cuenta_b',
                            'account_id_b',
                            'company_id_b',
                            string="Compania B"
                                    ),
        'compania_c': fields.many2many(
                            'res.company',
                            'company_rel_cuenta_c',
                            'account_id_c',
                            'company_id_c',
                            string="Compania C"
                                    ),
        'compania_d': fields.many2many(
                            'res.company',
                            'company_rel_cuenta_d',
                            'account_id_d',
                            'company_id_d',
                            string="Compania D"
                                    ),
}