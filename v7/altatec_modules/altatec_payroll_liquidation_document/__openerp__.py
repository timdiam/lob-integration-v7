{
    'name'         : 'AltaTec Payroll Liquidation Document',
    'version'      : '1.0',
    'description'  : """
                     Reporte de Beneficios Sociales
                     """,
    'author'       : 'Henry Lomas A',
    'website'      : 'www.altatec.ec',
    "depends"      : ['hr_payroll',
                      'account',
                      'hr',
                     ],
    "data"         : [ 'views/altatec_payroll_liquidation_document.xml',
                       'views/res_company.xml',

                     ],
    "installable"  : True,
    "auto_install" : False
}
