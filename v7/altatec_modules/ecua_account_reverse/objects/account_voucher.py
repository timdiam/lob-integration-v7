from osv import osv, fields
class account_voucher(osv.osv):
   
    _inherit = 'account.voucher'

    def _check_need_bank(self, cr, uid, ids, name, args, context=None):
        res = {}
        for voucher in self.browse(cr, uid, ids, context=context):
            if  voucher.journal_id.type=='bank':
                res[voucher.id] =True
        return res 
     
    _columns = {
        'need_bank_info': fields.function(_check_need_bank, string='Need Bank ', store=True,type='boolean', help="Show if need check or bank for button reverse."),
        }
    _default={ 
        'need_bank_info':False
              }

account_voucher()

