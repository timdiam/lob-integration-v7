# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 TRESCloud (<http://www.trescloud.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
###########################import sale_line_invoice,invoice###################################################

{
    "name" : "Account Reverse Advance",
    "version" : "0.1",
    "description" : """
    This module modify characteristic of account_reverse.
    
    Authors:
    Andrea García
        """,
    "author" : "TRESCloud Cia. Ltda.",
    "website" : "http://www.trescloud.com",
    "depends" : ['base','account_voucher_reverse'],
    "category" : "Custom Modules",
    "init_xml" : [],
    "demo_xml" : [],
    "update_xml" : [
                    'view/account_voucher_reverse_view.xml',
    ],
    "auto_install": False,
    "installable": True,
}
