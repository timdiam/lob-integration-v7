import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree
import openerp.addons.decimal_precision as dp
import openerp.exceptions


from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp import tools
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
from operator import itemgetter

import subprocess
import logging
import xml.etree.cElementTree as ET
import os
import pxssh

class odoo_servidor(osv.osv):
        _name= 'odoo.servidor'

        _columns = {
            "name":fields.char("Nombre"),
            "ip_static":fields.char("IP Static"),
            "url_domain":fields.char("URL Domain"),
            "user_linux_sudo":fields.char("Usuario linux sudo"),
            "pass_linux_sudo":fields.char("Contrasena linux sudo"),
            "company_hosting":fields.char("Compania de Hosting"),
            "user_hosting":fields.char("Usuario hosting"),
            "pass_hosting":fields.char("Contrasena hosting"),
            "puerto_ssh":fields.char("Puerto Ssh"),
            "odoo_systems":fields.one2many("odoo.system","servidor",string="Odoo Systems"),
        }




class odoo_system(osv.osv):
    _name="odoo.system"


    def pull_branch(self, cr, uid, ids,context=None):
        records=self.browse(cr,uid,ids)[0]

        usuario=records.user_odoo
        passw=records.pass_odoo
        puerto=records.servidor.puerto_ssh
	ip_static = records.ip_static
        #dominio=records.servidor.url_domain
        s = pxssh.pxssh()
        response = ''
        if not s.login (ip_static,usuario,passw,port=20122):
            print "SSH session failed on login."
            print str(s)
        else:
            print "SSH session login successful"
            s.sendline ('git status')
            s.prompt()
            response += s.before + '\n'
            s.sendline('git pull')
            s.prompt()
            response += s.before + '\n'
            s.sendline('Bj0n5t3rtT')
            s.prompt()
            response += s.before + '\n'
            s.logout()
            print response
            records.write({'output':response.decode('utf-8', 'ignore')})
        return True

    def restart_system(self, cr, uid, ids,context=None):
        records=self.browse(cr,uid,ids)[0]

        usuario=records.user_odoo
        passw=records.pass_odoo
        puerto=records.servidor.puerto_ssh
        #dominio=records.servidor.url_domain
        ip_static = records.ip_static
	s = pxssh.pxssh()
        response = ''
        if not s.login (ip_static,usuario,passw,port=20122):
            print "SSH session failed on login."
            print str(s)
        else:
            print "SSH session login successful"
            s.sendline (records.direccion_script + ' restart')
            s.prompt()
            response += s.before + '\n'
            s.logout()
            print response
            records.write({'output':response.decode('utf-8', 'ignore')})
        return True


    def ver_log(self, cr, uid, ids,context=None):
        records=self.browse(cr,uid,ids)[0]

        usuario=records.user_odoo
        passw=records.pass_odoo
        puerto=records.servidor.puerto_ssh
        #dominio=records.servidor.url_domain
        ip_static = records.ip_static
	s = pxssh.pxssh()
        response = ''
        if not s.login (ip_static,usuario,passw,port=20122):
            print "SSH session failed on login."
            print str(s)
        else:
            print "SSH session login successful"
            s.sendline ('tail -n 250 '+ records.direccion_var_log)
            s.prompt()
            response += s.before + '\n'
            s.logout()
            records.write({'output':response.decode('utf-8', 'ignore')})
            print response

        return True

    _columns={
                "name":fields.char("Nombre"),
                "ip_static":fields.related('servidor','ip_static',type='char',string="IP Static"),
                "project":fields.many2one("project.project",string="Proyecto",),
                'servidor':fields.many2one("odoo.servidor",string="Servidor"),
                "pass_odoo":fields.char("Contrasena Odoo"),
                "direccion_script":fields.char("Direccion a Reiniciar"),
                "direccion_var_log":fields.char("Direccion para Log"),
                "user_odoo":fields.char("usuario odoo"),
                "odoo_port":fields.char("odoo_port"),
                "branch":fields.char("branch"),
                "aeroo_ip":fields.char("aerooip"),
                "aeroo_port":fields.char("aeroo port"),
                "url_domain_name":fields.char("url domain-name"),
                "extra_info":fields.text("Informacion Extra"),
                "output":fields.text("Salida"),
            }

