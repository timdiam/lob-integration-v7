{
    'name': 'View invoice lines in form view',
    'version': '1.0',
    'description': """
    This modules changes the view of invoice lines at the time of creation to be of type form.
    """,
    'author': 'Tim Diamond',
    'website': 'www.altatececuador.com',
    "depends" : ['account'],
    "data" : [ 'account_invoice_view.xml'],
    "installable": True,
    "auto_install": False
}