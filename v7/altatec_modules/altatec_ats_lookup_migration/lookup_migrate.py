from openerp import tools, pooler
from openerp.osv import fields,osv

class lookup_migrate(osv.osv):
    _name = "lookup.migrate"
    _auto = False

    def init(self, cr):
        pool = pooler.get_pool(cr.dbname)
        uid = 1

        tax_code_ids = pool.get('account.tax.code').search(cr,uid,[])
        account_tax_lookup_obj = pool.get('account.tax.lookup.ats')

        for tax_code in pool.get('account.tax.code').browse(cr,uid,tax_code_ids):
            account_tax_lookup_obj.create(cr,uid,{
                                                    'date_start'  : '2015-01-01',
                                                    'date_end'    : '2015-04-01',
                                                    'tax_code_id' : tax_code.id,
                                                    'code'        : tax_code.code if tax_code.code else "VACIA",
                                                })
