{ 'name'         : 'AltaTec Migrate codigos ATS',
  'version'      : '1.0',
  'description'  : """
                   This module is used to migrate to new feature of ATS codigos using lookup table
                   """,
  'author'       : 'Tim Diamond',
  'website'      : 'www.altatec.ec',
  "depends"      : [ 'base','account','ecua_ats',
                   ],
  "data"         : [
                   ],
  "installable"  : True,
  "auto_install" : False
}
