{
    'name': 'Altatec RRHH Report',
    'version': '1.0',
    'description': """
        Reporte para novedades de empleados
    """,
    'author': 'Denisse Ochoa',
    'website': 'www.altatececuador.com',
    "depends" : [
                'base',
                'hr',


                ],
    "data" : [
                'views/rrhh_report_view.xml',
                'report/data/report.xml',
                'report/data/report_in_out_record.xml',
                'report/data/report_promotion.xml',


             ],
    "installable": True,
    "auto_install": False
}

