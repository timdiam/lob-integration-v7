#################################################################################
#
# This is the parser for the altatec_rrhh_report module.
#
# Author:  Denisse Ochoa
# Company: AltaTec Ecuador
# Date:    22/04/2015
#
#################################################################################
from report import report_sxw
import datetime

#################################################################################
# Parser class definition
#################################################################################
class Parser(report_sxw.rml_parse):

    #################################################################################
    # __init__() definition
    #################################################################################
    def __init__(self, cr, uid, name, context):

        super(Parser, self).__init__(cr, uid, name, context)

        self.localcontext.update( { 'cr'              : cr,
                                    'uid'             : uid,
                                    'date_start'      : context['date_start'],
                                    'date_end'        : context['date_end'],
                                    'get_lines'       : self.get_lines,
                                    'print_column'    : self.print_column,
                                    'get_date_start'  : self.get_date_start,
                                    'get_date_end'    : self.get_date_end,

                                    }
                                )


    #################################################################################
    # get_lines() returns data from news
    #################################################################################
    def get_lines(self, new):
        columns = []
        lines = []

        nov = self.pool.get( 'observaciones.rrhh' ).browse( self.localcontext['cr'],
                                                          self.localcontext['uid'],
                                                          new.id,
                                                          self.localcontext)
        columns.append(nov.fecha)
        columns.append(nov.empleado.name)
        columns.append(nov.notas)
        lines.append(columns)

        return lines



    #################################################################################
    # print_columns() returns column to fill spreadsheet
    #################################################################################
    def print_column(self, column):
        return column

    #################################################################################
    # get_date_start() returns date start
    #################################################################################
    def get_date_start(self):
        return self.localcontext['date_start']

    #################################################################################
    # get_date_end() returns date end
    #################################################################################

    def get_date_end(self):
        return self.localcontext['date_end']

