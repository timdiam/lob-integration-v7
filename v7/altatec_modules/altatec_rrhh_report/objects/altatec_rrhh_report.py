from openerp.osv import fields, osv, orm
import locale
import logging

_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL, 'en_US.utf8')


class altatec_rrhh_report_wizard(osv.osv_memory):
    _name = 'altatec.rrhh.report.wizard'

    def get_news(self, cr, uid, ids, context=None):
        records = self.browse(cr, uid, ids)[0]
        news = []

        context = {
                        'date_start':records.date_start,
                        'date_end'  :records.date_end,
                    }

        if records.date_start and records.date_end:
            '''busca las novedades en ese rango de fechas'''
            if records.employee_id:
                '''realiza la consulta de ese empleado'''

                if records.type == 'in_out':
                    '''busca las novedades de Historial de Entrada/Salida'''
                    news = self.pool.get('entrada.salida.historial.line').search(cr, uid,[('empleado', '=', records.employee_id.id),
                                                                                          ('fecha', '>=', records.date_start),
                                                                                          ('fecha', '<=', records.date_end)],
                                                                                            order='empleado')



                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_ent_sal',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')


                if records.type == 'promotion':
                    '''busca las novedades de Historial de Ascenso'''
                    news = self.pool.get('ascenso.line').search(cr, uid, [('empleado', '=', records.employee_id.id),
                                                                          ('fecha', '>=', records.date_start),
                                                                          ('fecha', '<=', records.date_end)], order='empleado')

                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_ascenso',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

                if records.type == 'observation':
                    '''busca las novedades de Observaciones'''
                    news = self.pool.get('observaciones.rrhh').search(cr, uid, [('empleado', '=', records.employee_id.id),
                                                                                ('fecha', '>=', records.date_start),
                                                                                ('fecha', '<=', records.date_end)],
                                                                                order='empleado')

                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_novedades',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

            else:
                '''realiza la consulta de todos los empleados'''
                if records.type == 'in_out':
                    '''busca las novedades de Historial de Entrada/Salida'''
                    news = self.pool.get('entrada.salida.historial.line').search(cr, uid,
                                                                                 [('fecha', '>=', records.date_start),
                                                                                 ('fecha', '<=', records.date_end)],
                                                                                 order='empleado')

                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_ent_sal',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

                if records.type == 'promotion':
                    '''busca las novedades de Historial de Ascenso'''
                    news = self.pool.get('ascenso.line').search(cr, uid, [('fecha', '>=', records.date_start),
                                                                          ('fecha', '<=', records.date_end)],
                                                                            order='empleado')

                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_ascenso',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

                if records.type == 'observation':
                    '''busca las novedades de Observaciones'''
                    news = self.pool.get('observaciones.rrhh').search(cr, uid, [('fecha', '>=', records.date_start),
                                                                                ('fecha', '<=', records.date_end)],
                                                                                order='empleado')


                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_novedades',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

        elif records.date_start:
            '''busca las novedades en ese rango de fechas'''
            if records.employee_id:
                '''realiza la consulta de ese empleado'''

                if records.type == 'in_out':
                    '''busca las novedades de Historial de Entrada/Salida'''
                    news = self.pool.get('entrada.salida.historial.line').search(cr, uid,[('empleado', '=', records.employee_id.id),
                                                                                          ('fecha', '>=', records.date_start)],
                                                                                            order='empleado')


                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_ent_sal',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

                if records.type == 'promotion':
                    '''busca las novedades de Historial de Ascenso'''
                    news = self.pool.get('ascenso.line').search(cr, uid, [('empleado', '=', records.employee_id.id),
                                                                          ('fecha', '>=', records.date_start)],
                                                                            order='empleado')

                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_ascenso',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

                if records.type == 'observation':
                    '''busca las novedades de Observaciones'''
                    news = self.pool.get('observaciones.rrhh').search(cr, uid, [('empleado', '=', records.employee_id.id),
                                                                                ('fecha', '>=', records.date_start)],
                                                                                order='empleado')



                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_novedades',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')


            else:
                '''realiza la consulta de todos los empleados'''
                if records.type == 'in_out':
                    '''busca las novedades de Historial de Entrada/Salida'''
                    news = self.pool.get('entrada.salida.historial.line').search(cr, uid, [('fecha', '>=', records.date_start)],
                                                                                 order='empleado')

                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_ent_sal',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

                if records.type == 'promotion':
                    '''busca las novedades de Historial de Ascenso'''
                    news = self.pool.get('ascenso.line').search(cr, uid, [('fecha', '>=', records.date_start)],order='empleado')

                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_ascenso',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

                if records.type == 'observation':
                    '''busca las novedades de Observaciones'''
                    news = self.pool.get('observaciones.rrhh').search(cr, uid, [('fecha', '>=', records.date_start)],
                                                                      order='empleado')

                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_novedades',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

        elif records.date_end:
            '''busca las novedades en ese rango de fechas'''
            if records.employee_id:
                '''realiza la consulta de ese empleado'''

                if records.type == 'in_out':
                    '''busca las novedades de Historial de Entrada/Salida'''
                    news = self.pool.get('entrada.salida.historial.line').search(cr, uid,[('empleado', '=', records.employee_id.id),
                                                                                          ('fecha', '<=', records.date_end)],
                                                                                            order='empleado')

                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_ent_sal',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

                if records.type == 'promotion':
                    '''busca las novedades de Historial de Ascenso'''
                    news = self.pool.get('ascenso.line').search(cr, uid, [('empleado', '=', records.employee_id.id),
                                                                          ('fecha', '<=', records.date_end)],
                                                                            order='empleado')

                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_ascenso',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

                if records.type == 'observation':
                    '''busca las novedades de Observaciones'''
                    news = self.pool.get('observaciones.rrhh').search(cr, uid, [('empleado', '=', records.employee_id.id),
                                                                                ('fecha', '<=', records.date_end)],
                                                                                order='empleado')

                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_novedades',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

            else:
                '''realiza la consulta de todos los empleados'''
                if records.type == 'in_out':
                    '''busca las novedades de Historial de Entrada/Salida'''
                    news = self.pool.get('entrada.salida.historial.line').search(cr, uid, [('fecha', '<=', records.date_end)],
                                                                                            order='empleado')

                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_ent_sal',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

                if records.type == 'promotion':
                    '''busca las novedades de Historial de Ascenso'''
                    news = self.pool.get('ascenso.line').search(cr, uid, [('fecha', '<=', records.date_end)], order='empleado')

                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_ascenso',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

                if records.type == 'observation':
                    '''busca las novedades de Observaciones'''
                    news = self.pool.get('observaciones.rrhh').search(cr, uid, [('fecha', '<=', records.date_end)],
                                                                      order='empleado')

                    if news:
                        return { 'type'        : 'ir.actions.report.xml',
                                 'context'     : context,
                                 'report_name' : 'reporte_novedades',
                                 'datas'       : { 'ids' : news }
                               }
                    else:
                        raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

    _columns = {
        'date_start': fields.date('Fecha Inicio', required=True),
        'date_end': fields.date('Fecha Final'),
        'employee_id': fields.many2one('hr.employee', string='Empleado'),
        'type': fields.selection([('in_out', 'Historial Entrada/Salida'),
                                  ('promotion', 'Historial de Ascenso'),
                                  ('observation', 'Observaciones')],
                                 string='Tipo de Novedad',
                                 required=True),
    }
