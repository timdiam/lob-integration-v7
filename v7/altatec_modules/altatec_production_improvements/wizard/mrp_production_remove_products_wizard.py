############################################################################################
#
############################################################################################
from openerp.osv import fields, osv, orm

############################################################################################
#
############################################################################################
class mrp_production_remove_products_wizard(osv.osv_memory):

    _name = "mrp.production.remove.products.wizard"

    ############################################################################################
    # Cancel the stock move, and if all of the stock moves that belongs to its picking are also
    # cancelled, then cancel the picking as well
    ############################################################################################
    def cancel_stock_move(self, cr, uid, move_line, context=None):

        move_line.write({'state':'cancel', 'move_dest_id':False}) #TODO: is this right? or should we call a function to cancel it?
        move_line = self.pool.get('stock.move').browse(cr, uid, move_line.id, context=context)

        if move_line.picking_id:

            cancel_picking = True
            for line in move_line.picking_id.move_lines:
                if line.state != 'cancel':
                    cancel_picking = False

            if cancel_picking:
                move_line.picking_id.write({'state':'cancel'}) #TODO: is this right? or should we call a function to cancel it?


    ############################################################################################
    #
    ############################################################################################
    def confirm_selection(self, cr, uid, ids, context={}):

        if not context or 'active_model' not in context or 'active_id' not in context or context['active_model'] != 'mrp.production':
            raise osv.except_osv("Error!", "Active model not found. Contact AltaTec.")

        production = self.pool.get('mrp.production').browse(cr, uid, context['active_id'], context=context)

        for wizard in self.browse(cr, uid, ids, context=context):

            products_to_return = {}

            for wizard_line in wizard.line_ids:

                if abs(wizard_line.qty_to_remove) < 0.00001:
                    continue

                # Figure out if we need to return products
                qty_cancelled = 0.0
                for move_line in production.procurement_moves:

                    if abs(qty_cancelled - wizard_line.qty_to_remove) < 0.00001:
                        break

                    if move_line.product_id.id == wizard_line.product_id.id and move_line.state != 'cancel' and move_line.state != 'done':
                        if move_line.product_qty - wizard_line.qty_to_remove <= 0.00001:
                            qty_cancelled += move_line.product_qty
                        else:
                            qty_cancelled += wizard_line.qty_to_remove

                # If we need to make a return, keep track of this in the products_to_return_dictionary
                if wizard_line.qty_to_remove - qty_cancelled > 0.0001:
                    if not wizard_line.product_id.id in products_to_return:
                        products_to_return[wizard_line.product_id.id] = 0.0
                    products_to_return[wizard_line.product_id.id] += wizard_line.qty_to_remove - qty_cancelled

             # If we need to return some products, create a return wizard
            if len(products_to_return):

                wizard_obj = self.pool.get("mrp.production.return.products.wizard")

                return_lines = []
                for product_id in products_to_return:
                    return_lines.append( (0, 0, { 'product_id'      : product_id,
                                                  'qty_to_return'   : products_to_return[product_id]
                                                } ) )

                remove_lines = []
                for wizard_line in wizard.line_ids:
                    if wizard_line.qty_to_remove > 0.00001:
                        remove_lines.append( (0, 0, { "product_id"    : wizard_line.product_id.id,
                                                      "qty_original"  : wizard_line.qty_original,
                                                      "qty_consumed"  : wizard_line.qty_consumed,
                                                      "qty_available" : wizard_line.qty_available,
                                                      "qty_to_remove" : wizard_line.qty_to_remove,
                                                    }
                                           ) )

                wizard_id = wizard_obj.create(cr, uid, { 'return_line_ids' : return_lines,
                                                         'production_id'   : production.id,
                                                         'remove_line_ids' : remove_lines,
                                                       })

                # Get the domain for the return locations
                user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
                location_ids_to_exclude = []
                if user.company_id:
                    if user.company_id.default_production_src_location:
                        location_ids_to_exclude.append(user.company_id.default_production_src_location.id)
                    if user.company_id.default_production_prod_location:
                        location_ids_to_exclude.append(user.company_id.default_production_prod_location.id)
                    if user.company_id.default_production_src_location:
                        location_ids_to_exclude.append(user.company_id.default_production_dest_location.id)

                location_domain = [('id','not in',location_ids_to_exclude),("active","=",True),("usage","in",["internal"])]

                # TODO: Figure out why this doesn't work.
                return { 'type'      : 'ir.actions.act_window',
                         'name'      : 'Devolver Productos',
                         'view_type' : 'form',
                         'view_mode' : 'form,tree',
                         'res_model' : 'mrp.production.return.products.wizard',
                         'res_id'    : wizard_id,
                         'target'    : 'new',
                         'domain'    : { 'return_line_ids': location_domain },
                       }

            # If we don't need to return any products, just remove the selected products
            else:
                for wizard_line in wizard.line_ids:

                    if abs(wizard_line.qty_to_remove) < 0.00001:
                        continue

                    # First remove the quantity from the production's move_lines
                    for move_line in production.move_lines:
                        if move_line.product_id.id == wizard_line.product_id.id:
                            if move_line.product_qty - wizard_line.qty_to_remove <= 0.00001:
                                self.cancel_stock_move(cr, uid, move_line, context)
                                production.write({'move_lines':[(3, move_line.id)]})
                            else:
                                move_line.write({'product_qty':move_line.product_qty - wizard_line.qty_to_remove})
                            break

                    # Now remove the quantity from the production's product_lines
                    for product_line in production.product_lines:
                        if product_line.product_id.id == wizard_line.product_id.id:
                            if product_line.product_qty - wizard_line.qty_to_remove <= 0.00001:
                                production.write({'product_lines':[(3, product_line.id)]})
                            else:
                                product_line.write({'product_qty':product_line.product_qty - wizard_line.qty_to_remove})
                            break

                    # Now remove the quantity from the production's picking_id
                    qty_removed = 0.0
                    for move_line in production.picking_id.move_lines:

                        if abs(qty_removed - wizard_line.qty_to_remove) < 0.00001:
                            break

                        if move_line.product_id.id == wizard_line.product_id.id and move_line.state != 'cancel':

                            if move_line.product_qty - wizard_line.qty_to_remove <= 0.00001:
                                qty_removed += move_line.product_qty
                                self.cancel_stock_move(cr, uid, move_line, context)
                            else:
                                qty_removed += wizard_line.qty_to_remove
                                move_line.write({'product_qty':move_line.product_qty - wizard_line.qty_to_remove})


                    # Finally, we need to subtract this amount from the procurement moves
                    qty_cancelled = 0.0
                    for move_line in production.procurement_moves:

                        if abs(qty_cancelled - wizard_line.qty_to_remove) < 0.00001:
                            break

                        if move_line.product_id.id == wizard_line.product_id.id and move_line.state != 'cancel' and move_line.state != 'done':
                            if move_line.product_qty - wizard_line.qty_to_remove <= 0.00001:
                                qty_cancelled += move_line.product_qty
                                self.cancel_stock_move(cr, uid, move_line, context)
                            else:
                                qty_cancelled += wizard_line.qty_to_remove
                                move_line.write({'product_qty':move_line.product_qty - wizard_line.qty_to_remove})

                # Now to be safe let's recalculate the stock move chains
                self.pool.get('mrp.production').create_move_chains(cr, uid, production.id, context=context)

        return { 'type'      : 'ir.actions.act_window',
                 'name'      : 'Orden de Produccion',
                 'view_type' : 'form',
                 'view_mode' : 'form,tree',
                 'res_model' : 'mrp.production',
                 'res_id'    : production.id,
               }


    _columns = { "line_ids" : fields.one2many("mrp.production.remove.products.wizard.line", "wizard_id", "Lines"),
               }

############################################################################################
#
############################################################################################
class mrp_production_remove_products_wizard_line(osv.osv_memory):

    _name = "mrp.production.remove.products.wizard.line"

    def onchange_qty_to_remove(self, cr, uid, ids, qty_to_remove, qty_available, context={}):
        res = { 'value': {} }

        if qty_to_remove - qty_available > 0.001:
            if qty_available > 0.001:
                res['value']['qty_to_remove'] = qty_available
            else:
                res['value']['qty_to_remove'] = 0.00
            res['warning'] = {'title':"Error", "message":"No se puede eliminar una cantidad mayor que la cantidad disponible."}

        elif qty_to_remove < 0.001:
            res['value']['qty_to_remove'] = 0.00
            res['warning'] = {'title':"Error", "message":"La cantidad a eliminar debe ser mayor que 0."}

        return res

    _columns = { "wizard_id"        : fields.many2one("mrp.production.remove.products.wizard", "Wizard ID"),
                 "return_wizard_id" : fields.many2one("mrp.production.return.products.wizard", "Return Wizard ID"),
                 "product_id"       : fields.many2one("product.product", "Producto"),
                 "qty_original"     : fields.float("Cantidad original"),
                 "qty_consumed"     : fields.float("Cantidad consumida"),
                 "qty_available"    : fields.float("Cantidad disponible"),
                 "qty_to_remove"    : fields.float("Cantidad a eliminar"),
               }

############################################################################################
#
############################################################################################
class mrp_production_return_products_wizard(osv.osv_memory):

    _name = "mrp.production.return.products.wizard"

    ############################################################################################
    #
    ############################################################################################
    def confirm(self, cr, uid, ids, context={}):

        remove_wizard_obj = self.pool.get("mrp.production.remove.products.wizard")

        for wizard in self.browse(cr, uid, ids, context=context):

            for line in wizard.return_line_ids:
                if not line.location_id:
                    raise osv.except_osv("Error", "Debe seleccionar las ubicaciones a que quiere devolver los productos eliminados.")

            production = self.pool.get('mrp.production').browse(cr, uid, wizard.production_id.id, context=context)

            for wizard_line in wizard.remove_line_ids:

                if abs(wizard_line.qty_to_remove) < 0.00001:
                    continue

                # First remove the quantity from the production's move_lines
                for move_line in production.move_lines:
                    if move_line.product_id.id == wizard_line.product_id.id:
                        if move_line.product_qty - wizard_line.qty_to_remove <= 0.00001:
                            remove_wizard_obj.cancel_stock_move(cr, uid, move_line, context)
                            production.write({'move_lines':[(3, move_line.id)]})
                        else:
                            move_line.write({'product_qty':move_line.product_qty - wizard_line.qty_to_remove})
                        break

                # Now remove the quantity from the production's product_lines
                for product_line in production.product_lines:
                    if product_line.product_id.id == wizard_line.product_id.id:
                        if product_line.product_qty - wizard_line.qty_to_remove <= 0.00001:
                            production.write({'product_lines':[(3, product_line.id)]})
                        else:
                            product_line.write({'product_qty':product_line.product_qty - wizard_line.qty_to_remove})
                        break

                # Now remove the quantity from the production's picking_id
                qty_removed = 0.0
                for move_line in production.picking_id.move_lines:

                    if abs(qty_removed - wizard_line.qty_to_remove) < 0.00001:
                        break

                    if move_line.product_id.id == wizard_line.product_id.id and move_line.state != 'cancel':

                        if move_line.product_qty - wizard_line.qty_to_remove <= 0.00001:
                            qty_removed += move_line.product_qty
                            remove_wizard_obj.cancel_stock_move(cr, uid, move_line, context)
                        else:
                            qty_removed += wizard_line.qty_to_remove
                            move_line.write({'product_qty':move_line.product_qty - wizard_line.qty_to_remove})


                # Finally, we need to subtract this amount from the procurement moves
                qty_cancelled = 0.0
                for move_line in production.procurement_moves:

                    if abs(qty_cancelled - wizard_line.qty_to_remove) < 0.00001:
                        break

                    if move_line.product_id.id == wizard_line.product_id.id and move_line.state != 'cancel' and move_line.state != 'done':
                        if move_line.product_qty - wizard_line.qty_to_remove <= 0.00001:
                            qty_cancelled += move_line.product_qty
                            remove_wizard_obj.cancel_stock_move(cr, uid, move_line, context)
                        else:
                            qty_cancelled += wizard_line.qty_to_remove
                            move_line.write({'product_qty':move_line.product_qty - wizard_line.qty_to_remove})

            move_obj     = self.pool.get('stock.move')
            picking_obj  = self.pool.get('stock.picking')
            sequence_obj = self.pool.get('ir.sequence')

            # Now create return moves and picking
            for return_line in wizard.return_line_ids:
                picking_id = picking_obj.create( cr, uid, { 'name'            : sequence_obj.get(cr, uid, 'stock.picking'),
                                                            'origin'          : (production.origin or '').split(':')[0] + ':' + production.name,
                                                            'type'            : 'internal',
                                                            'move_type'       : 'one',
                                                            'state'           : 'draft',
                                                            'auto_picking'    : False,
                                                            'company_id'      : production.company_id.id,
                                                          }, context=context )

                move_obj.create( cr, uid, { 'name'             : production.name,
                                            'product_id'       : return_line.product_id.id,
                                            'product_qty'      : return_line.qty_to_return,
                                            'product_uom'      : return_line.product_id.uom_id.id,
                                            'location_id'      : production.location_src_id.id,
                                            'location_dest_id' : return_line.location_id.id,
                                            'picking_id'           : picking_id,
                                            'account_analytic_id'  : production.project_id.id,
                                            'production_id_return' : production.id,
                                          }, context=context)


            # Now to be safe let's recalculate the stock move chains
            self.pool.get('mrp.production').create_move_chains(cr, uid, production.id, context=context)

        return { 'type'      : 'ir.actions.act_window',
                 'name'      : 'Orden de Produccion',
                 'view_type' : 'form',
                 'view_mode' : 'form,tree',
                 'res_model' : 'mrp.production',
                 'res_id'    : production.id,
               }

    _columns = { 'return_line_ids' : fields.one2many("mrp.production.return.products.wizard.line", "wizard_id", "Line IDs", required=True),
                 'production_id'   : fields.many2one("mrp.production", required=True),
                 'remove_line_ids' : fields.one2many("mrp.production.remove.products.wizard.line", "return_wizard_id", "Line IDs", required=True),
               }

############################################################################################
#
############################################################################################
class mrp_production_return_products_wizard_line(osv.osv_memory):

    _name = "mrp.production.return.products.wizard.line"

    _columns = { 'wizard_id'     : fields.many2one("mrp.production.return.products.wizard", "Wizard ID"),
                 'product_id'    : fields.many2one("product.product", "Producto", required=True),
                 'qty_to_return' : fields.float("Cantidad a devolver", required=True),
                 'location_id'   : fields.many2one("stock.location", "Ubicacion destino"),
               }