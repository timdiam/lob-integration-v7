############################################################################################
#
############################################################################################
from openerp.osv import fields, osv, orm

############################################################################################
#
############################################################################################
class mrp_production_cancel_shipments_wizard(osv.osv_memory):

    _name = "mrp.production.cancel.shipments.wizard"

    ############################################################################################
    # Cancel the stock move, and if all of the stock moves that belongs to its picking are also
    # cancelled, then cancel the picking as well
    ############################################################################################
    def cancel_stock_move(self, cr, uid, move_line, context=None):

        move_line.write({'state':'cancel', 'move_dest_id':False}) #TODO: is this right? or should we call a function to cancel it?
        move_line = self.pool.get('stock.move').browse(cr, uid, move_line.id, context=context)

        if move_line.picking_id:

            cancel_picking = True
            for line in move_line.picking_id.move_lines:
                if line.state != 'cancel':
                    cancel_picking = False

            if cancel_picking:
                move_line.picking_id.write({'state':'cancel'}) #TODO: is this right? or should we call a function to cancel it?

    ############################################################################################
    #
    ############################################################################################
    def confirm_selection(self, cr, uid, ids, context=None):
        for wizard_id in ids:

            wizard = self.browse(cr, uid, wizard_id, context=context)

            for wizard_line in wizard.product_lines:

                wizard = self.browse(cr, uid, wizard.id, context=context)

                if not wizard_line.is_selected:
                    continue

                for move_line in wizard.production_id.move_lines:

                    if move_line.product_id.id == wizard_line.product_id.id:

                        if move_line.product_qty - wizard_line.qty <= 0.001:
                            self.cancel_stock_move(cr, uid, move_line, context)
                            wizard.production_id.write({'move_lines':[(3,move_line.id)]})
                        else:
                            move_line.write({ 'product_qty': move_line.product_qty - wizard_line.qty })

                # Now remove the quantity from the production's product_lines
                for product_line in wizard.production_id.product_lines:

                    if product_line.product_id.id == wizard_line.product_id.id:

                        if product_line.product_qty - wizard_line.qty <= 0.001:
                            wizard.production_id.write({'product_lines':[(3, product_line.id)]})
                        else:
                            product_line.write({'product_qty':product_line.product_qty - wizard_line.qty})
                        break

                # Now let's remove this quantity of this product from the production order's picking_id
                qty_removed = 0.0
                for move_line in wizard.production_id.picking_id.move_lines:

                    if abs(qty_removed - wizard_line.qty) < 0.001:
                        break

                    if move_line.product_id.id == wizard_line.product_id.id and move_line.state != 'cancel':

                        if move_line.product_qty - wizard_line.qty <= 0.0001 :
                            qty_removed += move_line.product_qty
                            self.cancel_stock_move(cr, uid, move_line, context)
                        else:
                            qty_removed += wizard_line.qty
                            move_line.write({ 'product_qty': move_line.product_qty - wizard_line.qty })

                # Cancel the selected stock move
                self.cancel_stock_move(cr, uid, wizard_line.move_id, context)

            # Now to be safe let's recalculate the stock move chains
            self.pool.get('mrp.production').create_move_chains(cr, uid, wizard.production_id.id, context=context)

            return { 'type'      : 'ir.actions.act_window',
                     'name'      : 'Eliminar Consumibles',
                     'view_type' : 'form',
                     'view_mode' : 'form,tree',
                     'res_model' : 'mrp.production',
                     'res_id'    : wizard.production_id.id,
                   }

    _columns = { 'production_id' : fields.many2one('mrp.production', "Production ID"),
                 'product_lines' : fields.one2many('mrp.production.cancel.shipments.wizard.line', 'wizard_id', 'Productos para eliminar'),
               }

############################################################################################
#
############################################################################################
class mrp_production_cancel_shipments_wizard_line(osv.osv_memory):

    _name = "mrp.production.cancel.shipments.wizard.line"

    ############################################################################################
    #
    ############################################################################################
    def mark_for_deletion( self, cr, uid, ids, context=None ):

        dialog_obj = self.pool.get('mrp.production.cancel.shipments.wizard.confirm')

        for line in self.browse(cr, uid, ids, context=context):

            wizard_id = self.browse(cr, uid, ids[0], context=context).wizard_id.id

            self.write( cr, uid, [line.id], { 'is_selected': True } )

            return { 'type'      : 'ir.actions.act_window',
                     'name'      : 'Eliminar Consumibles',
                     'view_type' : 'form',
                     'view_mode' : 'form,tree',
                     'res_model' : 'mrp.production.cancel.shipments.wizard',
                     'res_id'    : wizard_id,
                     'target'    : 'new',
                   }

    ############################################################################################
    #
    ############################################################################################
    def unmark_for_deletion( self, cr, uid, ids, context=None ):

        self.write( cr, uid, ids , { 'is_selected': False } )

        wizard_id = self.browse(cr, uid, ids[0], context=context).wizard_id.id

        return { 'type'      : 'ir.actions.act_window',
                 'name'      : 'Eliminar Consumibles',
                 'view_type' : 'form',
                 'view_mode' : 'form,tree',
                 'res_model' : 'mrp.production.cancel.shipments.wizard',
                 'res_id'    : wizard_id,
                 'target'    : 'new',
               }

    ############################################################################################
    #
    ############################################################################################
    _columns = { 'wizard_id'    : fields.many2one("mrp.production.cancel.shipments.wizard", "Wizard ID"),
                 'move_id'      : fields.many2one("stock.move", "Production Stock Move"),
                 'product_id'   : fields.many2one("product.product", "Producto"),
                 'qty'          : fields.float("Cantidad"),
                 'uom_id'       : fields.many2one("product.uom", "Unidad(es)"),
                 'location_id'  : fields.many2one("stock.location", "Ubicacion de origen"),
                 'is_selected'  : fields.boolean("Is selected?"),
                 'move_state'   : fields.related( "move_id", "state", type="selection", selection=[('draft', 'Nuevo'),
                                                                                                   ('cancel', 'Anulado'),
                                                                                                   ('waiting', 'Esperando otro movimiento'),
                                                                                                   ('confirmed', 'Esperando Disponibilidad'),
                                                                                                   ('assigned', 'Reservado'),
                                                                                                   ('done', 'Realizado'),
                                                                                                   ],string="Estado"),
               }

    _defaults = { 'is_selected' : False,
                }