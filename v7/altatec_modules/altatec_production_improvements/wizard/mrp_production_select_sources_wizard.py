############################################################################################
#
############################################################################################
from openerp.osv import fields, osv, orm
from openerp import netsvc

############################################################################################
#
############################################################################################
class mrp_production_select_sources_wizard(osv.osv_memory):

    _name = "mrp.production.select.sources.wizard"

    ###################################################################################
    #
    ###################################################################################
    def update_product_qty_on_production_order( self, cr, uid, product_id, qty, production_id, context ):

        # If we're updating quantity to 0, throw error
        if qty < 0.001:
            raise osv.except_osv( "Programming Error", "Qty cannot be 0 when updating product quantity on a production order" )

        production = self.pool.get('mrp.production').browse(cr, uid, production_id, context=context)

        updated = False
        for line in production.product_lines:
            if line.product_id.id == product_id:
                if updated:
                    raise osv.except_osv( "Programming Error", "Product appears more than once in this production's product_lines.")
                line.write({'product_qty': qty})
                updated = True

        # Update quantity on the production's move_lines field
        updated = False
        for line in production.move_lines:
            if line.product_id.id == product_id:
                if updated:
                    raise osv.except_osv( "Programming Error", "Product appears more than once in this production's move_lines.")
                line.write({'product_qty': qty})
                updated = True

        # Update quantity on the production's picking_id field
        updated = False
        for line in production.picking_id.move_lines:
            if line.product_id.id == product_id:
                if updated:
                    raise osv.except_osv( "Programming Error", "Product appears more than once in this production's picking_id.")
                line.write({'product_qty': qty})
                updated = True

    ###################################################################################
    #
    ###################################################################################
    def add_product_to_production_order( self, cr, uid, product_id, qty, production_id, context ):

        if qty < 0.001:
            raise osv.except_osv( "Programming Error", "Qty cannot be 0 when adding product to a production order" )

        stock_move_obj = self.pool.get('stock.move')
        product_obj    = self.pool.get('product.product')
        production_obj = self.pool.get('mrp.production')

        # Get production
        production = production_obj.browse(cr, uid, production_id, context=context)

        # Get product_uom
        product = product_obj.browse( cr, uid, product_id, context=context)

        # Get the virtual locations / production ID
        production_loc_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'stock', 'location_production')

        # First add this product to the production's product_lines
        production.write({'product_lines': [ (0, 0, { 'name'            : product.name,
                                                      'product_id'      : product_id,
                                                      'product_qty'     : qty,
                                                      'product_uom'     : product.uom_id.id,
                                                      'product_uos_qty' : False,
                                                      'product_uos'     : False,
                                                    }
                                             )]})

        # Let's add this product to the production's production->virtual move_lines and the picking they belong to
        final_move_id = stock_move_obj.create( cr, uid, { 'name'             : production.name,
                                                          'state'            : 'waiting',
                                                          'type'             : 'internal',
                                                          'product_id'       : product_id,
                                                          'product_qty'      : qty,
                                                          'product_uom'      : product.uom_id.id,
                                                          'location_id'      : production.location_prod_id.id,
                                                          'location_dest_id' : production_loc_id[1],
                                                          'picking_id'          : production.consume_picking.id,
                                                          'account_analytic_id' : production.project_id.id if production.project_id else False,
                                                        }, context=context )

        production.write( { 'move_lines' : [ (4, final_move_id)] } )

        # Let's add a new stock move to the production's processing->consumed picking_id
        if not production.picking_id:
            production_obj._make_production_internal_shipment(cr, uid, production, context=context)
            production = production_obj.browse(cr, uid, production.id, context=context)

        stock_move_obj.create( cr, uid, { 'name'             : production.name,
                                          'state'            : 'waiting',
                                          'type'             : 'internal',
                                          'product_id'       : product_id,
                                          'product_qty'      : qty,
                                          'product_uom'      : product.uom_id.id,
                                          'location_id'      : production.location_src_id.id,
                                          'location_dest_id' : production.location_prod_id.id,
                                          'picking_id'          : production.picking_id.id,
                                          'account_analytic_id' : production.project_id.id if production.project_id else False,
                                          'move_dest_id'     : final_move_id,
                                        }, context=context )

    ###################################################################################
    #
    ###################################################################################
    def remove_product_from_production_order( self, cr, uid, product_id, production_id, context ):

        production = self.pool.get('mrp.production').browse(cr, uid, production_id, context=context)

        # Remove this product from the production's product_lines
        removed = False
        for line in production.product_lines:
            if line.product_id.id == product_id:
                if removed:
                    raise osv.except_osv( "Programming Error", "Product appears more than once in this production's product_lines.")
                line.write( {'state' : 'cancel',  'picking_id' : False } )
                production.write({'product_lines':[(2, line.id)]})
                removed = True

        # Get the stock move lines to be deleted from this production's move_lines field
        removed = False
        for line in production.move_lines:
            if line.product_id.id == product_id:
                if removed:
                    raise osv.except_osv( "Programming Error", "Product appears more than once in this production's move_lines.")
                line.write( {'state' : 'cancel',  'picking_id' : False } )
                production.write({'move_lines':[(3, line.id)]})
                removed = True

        # Get the stock move lines to be deleted from this production's picking_id field
        removed = False
        for line in production.picking_id.move_lines:
            if line.product_id.id == product_id:
                if removed:
                    raise osv.except_osv( "Programming Error", "Product appears more than once in this production's picking_id.")
                line.write( {'state': 'cancel', 'picking_id': False,} )
                removed = True

    ###################################################################################
    # Validate the data entered by the user
    ###################################################################################
    def validate_lines(self, wizard, context=None):

        if not context or 'active_model' not in context or 'active_id' not in context or context['active_model'] != 'mrp.production':
            raise osv.except_osv("Error!", "Active model not found. Contact AltaTec.")

        for line in wizard.source_lines:
            # Make sure the user provided a product, quantity and location for this line
            if not line.product_id:
                raise osv.except_osv("Error!", "Debe seleccionar un producto" )

            if not line.qty:
                raise osv.except_osv("Error!", "Debe seleccionar una cantidad para producto: " + str(line.product_id.name))

            if not line.location_id:
                raise osv.except_osv("Error!", "Debe seleccionar una ubicacion para producto: " + str(line.product_id.name))

            # Make sure they didn't do something stupid like putting a negative quantity
            if line.qty < 0:
                raise osv.except_osv("Error!", "Cantidad no puede ser negativo.")

    ###################################################################################
    # Create all of the Materia Prima Locations -> Production pickings that will be
    # validated by the warehouse managers
    ###################################################################################
    def create_warehouse_pickings(self, cr, uid, production_id, wizard, context=None ):

        move_obj       = self.pool.get('stock.move')
        picking_obj    = self.pool.get('stock.picking')
        sequence_obj   = self.pool.get('ir.sequence')
        production_obj = self.pool.get('mrp.production')

        production = production_obj.browse(cr, uid, production_id, context)

        # First create a data structure containing all of the product quantities by location
        prods_by_location = {}
        for line in wizard.source_lines:

            if line.location_id.id not in prods_by_location:
                prods_by_location[line.location_id.id] = {}

            if line.product_id.id not in prods_by_location[line.location_id.id]:
                prods_by_location[line.location_id.id][line.product_id.id] = { 'qty' : 0.0, 'uom' : line.uom_id.id }

            prods_by_location[line.location_id.id][line.product_id.id]['qty'] += line.qty

        # If the production order's processing->consumed picking (picking_id) has already been fulfilled or cancelled,
        # we'll set the moves to go directly to the consumed location
        if production.picking_id.state == 'done' or production.picking_id.state == 'cancel':
            location_dest_id = production.location_prod_id.id
        else:
            location_dest_id = production.location_src_id.id

        # Now let's create a stock picking for each location specified
        for location_id in prods_by_location:
            picking_id = picking_obj.create( cr, uid, { 'name'            : sequence_obj.get(cr, uid, 'stock.picking'),
                                                        'origin'          : (production.origin or '').split(':')[0] + ':' + production.name,
                                                        'type'            : 'internal',
                                                        'move_type'       : 'one',
                                                        'state'           : 'draft',
                                                        'auto_picking'    : False,
                                                        'company_id'      : production.company_id.id,
                                                      }, context=context )

            for product_id in prods_by_location[location_id]:
                move_obj.create( cr, uid, { 'name'             : production.name,
                                            'product_id'       : product_id,
                                            'product_qty'      : prods_by_location[location_id][product_id]['qty'],
                                            'product_uom'      : prods_by_location[location_id][product_id]['uom'],
                                            'location_id'      : location_id,
                                            'location_dest_id' : location_dest_id,
                                            'production_id_procurement' : production.id,
                                            'picking_id'          : picking_id,
                                            'account_analytic_id' : production.project_id.id,
                                          }, context=context)

    ###################################################################################
    #
    ###################################################################################
    def clear_bom(self, cr, uid, ids, context=None):

        if not isinstance(ids, list):
            ids = [ids]

        for bom in self.pool.get('mrp.bom').browse(cr, uid, ids, context):
            lines_to_delete = []
            for line in bom.bom_lines:
                lines_to_delete.append((2,line.id))
            bom.write({'bom_lines':lines_to_delete})

    ###################################################################################
    #
    ###################################################################################
    def add_remaining_products_to_consume_picking(self, cr, uid, production, context={}):

        move_obj = self.pool.get('stock.move')

        product_ids = []
        production_virtual_location_id = False
        for move_line in production.consume_picking.move_lines:
            product_ids.append(move_line.product_id.id)
            if move_line.location_dest_id.id == production.location_dest_id.id:
                production_virtual_location_id = move_line.location_id.id

        for product in production.product_ids:
            if product.product_id.id not in product_ids:
                move_obj.create( cr, uid, { 'name'             : production.name,
                                            'product_id'       : product.product_id.id,
                                            'product_qty'      : product.product_qty,
                                            'product_uom'      : product.uom_id.id,
                                            'location_id'      : production_virtual_location_id,
                                            'location_dest_id' : production.location_dest_id.id,
                                            'picking_id'          : production.consume_picking.id,
                                            'account_analytic_id' : production.project_id.id,
                                          }, context=context)

    ###################################################################################
    #
    ###################################################################################
    def confirm_selection(self, cr, uid, ids, context ):

        production_obj = self.pool.get('mrp.production')
        stock_move_obj = self.pool.get('stock.move')
        bom_obj        = self.pool.get('mrp.bom')

        for wizard in self.browse(cr, uid, ids, context ):

            # Validate the source lines given by the user
            self.validate_lines( wizard, context )

            production = production_obj.browse(cr, uid, context['active_id'], context=context)

            # If we're in confirm mode, we copy the BOM and update the copy with the products selected
            # by the user in this wizard, assign the copy to the MO, confirm the MO, then assign
            # the old BOM to the MO
            if wizard.mode == 'confirm':

                # Copy the BOM and clear the copy of products
                old_bom_id = production.bom_id.id
                new_bom_id = bom_obj.copy(cr, uid, production.bom_id.id, {'product_qty':production.product_qty}, context=context)
                self.clear_bom(cr, uid, new_bom_id, context)

                # Create BOM lines based on the wizard lines and assign them to the new BOM copy
                new_bom_lines = []
                for line in wizard.source_lines:
                    product_already_in_bom = False
                    for bom_line in new_bom_lines:
                        if bom_line[2]['product_id'] == line.product_id.id:
                            product_already_in_bom = True
                            bom_line[2]['product_qty'] += line.qty
                    if not product_already_in_bom:
                        new_bom_lines.append((0, 0, { 'product_id'  : line.product_id.id,
                                                      'product_qty' : line.qty,
                                                      'product_uom' : line.uom_id.id,
                                                    }))
                bom_obj.write(cr, uid, [new_bom_id], {'bom_lines':new_bom_lines})

                # Assign the updated copy of the BOM to the production order
                production.write({ 'bom_id':new_bom_id })

                # Confirm the MO
                wf_service = netsvc.LocalService("workflow")
                wf_service.trg_validate(uid, 'mrp.production', production.id, 'button_confirm', cr)

                # Now write the old bom_id back to the production order, and delete the new copy of the BOM
                production.write({'bom_id':old_bom_id})
                bom_obj.unlink(cr, uid, [new_bom_id], context=context)

                production = production_obj.browse( cr, uid, production.id, context=context )

                # Add the remaining products and their quantities from product_ids to the final output picking
                self.add_remaining_products_to_consume_picking(cr, uid, production, context)

                # Mark the two system generated pickings as "from_production", for filtering purposes
                if production.picking_id:
                    production.picking_id.write({"from_production" : True})

                if not wizard.source_lines:
                    return

                # Finally, create the materia prima location -> processing pickings
                self.create_warehouse_pickings(cr, uid, production.id, wizard, context)

                # Set up the relationship between all stock moves by setting each move's move_dest_id
                production_obj.create_move_chains(cr, uid, production.id, context)

            # If we're in add mode, let's either add or update the qty of the selected products to the consumed->final
            # picking (the production's move_lines). If the processing->consumed picking isn't done, we add it there too.
            else:

                if not wizard.source_lines:
                    return

                production_loc_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'stock', 'location_production')

                for wizard_line in wizard.source_lines:

                    production = production_obj.browse( cr, uid, production.id, context=context )

                    # First let's either update or add a line in the production's product_lines field
                    updated = False
                    for product_line in production.product_lines:
                        if product_line.product_id.id == wizard_line.product_id.id:
                            if updated:
                                raise osv.except_osv( "Programming Error", "Product appears more than once in this production's product_lines.")
                            product_line.write({'product_qty': product_line.product_qty + wizard_line.qty})
                            updated = True
                    if not updated:
                        production.write({'product_lines': [ (0, 0, { 'name'            : wizard_line.product_id.name,
                                                                      'product_id'      : wizard_line.product_id.id,
                                                                      'product_qty'     : wizard_line.qty,
                                                                      'product_uom'     : wizard_line.product_id.uom_id.id,
                                                                      'product_uos_qty' : False,
                                                                      'product_uos'     : False,
                                                                    }
                                                             )]})

                    # Add products to the consumed->final picking
                    product_already_exists_on_production = False
                    for move_line in production.move_lines:
                        if move_line.product_id.id == wizard_line.product_id.id:
                            move_line.write({ 'product_qty' : move_line.product_qty + wizard_line.qty })
                            product_already_exists_on_production = True
                            break

                    if not product_already_exists_on_production:
                        production.write( { 'move_lines' : [( 0, 0, { 'name'             : production.name,
                                                                      'state'            : 'waiting',
                                                                      'type'             : 'internal',
                                                                      'product_id'       : wizard_line.product_id.id,
                                                                      'product_qty'      : wizard_line.qty,
                                                                      'product_uom'      : wizard_line.uom_id.id,
                                                                      'location_id'      : production.location_prod_id.id,
                                                                      'location_dest_id' : production_loc_id[1],
                                                                      'picking_id'          : production.consume_picking.id,
                                                                      'account_analytic_id' : production.project_id.id if production.project_id else False,
                                                                    })]})

                if not production.picking_id:
                    production_obj._make_production_internal_shipment(cr, uid, production, context=context)
                    production = production_obj.browse(cr, uid, production.id, context=context)
                    production.picking_id.write({"from_production" : True,
                                                 "auto_picking"    : False,
                                                }) # Mark the two system generated pickings as "from_production", for filtering purposes

                # Now, if the production order's processing->consumed picking isn't done, we add it there too.
                if production.picking_id.state != 'done' and production.picking_id.state != 'cancel':

                    picking_additions = {}
                    for wizard_line in wizard.source_lines:
                        if wizard_line.product_id.id not in picking_additions:
                            picking_additions[wizard_line.product_id.id] = { 'qty': 0.0, 'uom_id': wizard_line.uom_id.id }
                        picking_additions[wizard_line.product_id.id][ 'qty' ] += wizard_line.qty

                    for product_id in picking_additions:
                        stock_move_obj.create( cr, uid, { 'name'             : production.name,
                                                          'state'            : 'waiting',
                                                          'type'             : 'internal',
                                                          'product_id'       : product_id,
                                                          'product_qty'      : picking_additions[product_id]['qty'],
                                                          'product_uom'      : picking_additions[product_id]['uom_id'],
                                                          'location_id'      : production.location_src_id.id,
                                                          'location_dest_id' : production.location_prod_id.id,
                                                          'picking_id'          : production.picking_id.id,
                                                          'account_analytic_id' : production.project_id.id if production.project_id else False,
                                                        }, context=context )

                # Finally, create the materia prima location -> production pickings
                self.create_warehouse_pickings(cr, uid, production.id, wizard, context)

                # Set up the relationship between all stock moves by setting each move's move_dest_id
                production_obj.create_move_chains(cr, uid, production.id, context)

    ###################################################################################
    #  Column and defaults definitions
    ###################################################################################
    _columns = { 'production_id' : fields.many2one("mrp.production", required=True),
                 'mode'          : fields.selection([("confirm","Confirm"),("add","Add")], string="Modo"),
                 'product_ids'   : fields.many2many( 'altatec.final.product.line', 'rel_altatec_final_product_line', 'wizard_id', 'product_line_id', 'Productos a Producir'),
                 'source_lines'  : fields.one2many("mrp.production.select.sources.wizard.line", "wizard_id", "Sources" ),
               }

    _defaults = { 'mode' : 'confirm',
                }

###################################################################################
#
###################################################################################
class mrp_production_select_sources_wizard_line(osv.osv_memory):

    _name = "mrp.production.select.sources.wizard.line"

    ###################################################################################
    # Remove the production locations from the available locations
    ###################################################################################
    def set_domains(self, cr, uid, ids, context=None):

        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        domain = [("active","=",True),("usage","in",["internal"])]
        if user.company_id:
            location_ids_to_exclude = []
            if user.company_id.default_production_src_location:
                location_ids_to_exclude.append(user.company_id.default_production_src_location.id)
            if user.company_id.default_production_prod_location:
                location_ids_to_exclude.append(user.company_id.default_production_prod_location.id)
            if user.company_id.default_production_src_location:
                location_ids_to_exclude.append(user.company_id.default_production_dest_location.id)
            domain.append( ('id','not in',location_ids_to_exclude) )
        return { 'domain' : {'location_id' : domain}}

    ###################################################################################
    #
    ###################################################################################
    def onchange_product_id(self, cr, uid, ids, product_id, availability=None, context=None):

        if not context:
            context = {}
        context.update({'states': ('done',), 'what': ('in', 'out')})

        res = { 'value' : { 'is_available': False , 'availability': [] } }

        if product_id:

            location_obj   = self.pool.get('stock.location')
            production_obj = self.pool.get('mrp.production')
            wizard_obj     = self.pool.get('mrp.production.select.sources.wizard')

            # Get the production
            if 'active_model' not in context or 'active_id' not in context:
                raise osv.except_osv("Error!", "Active model not found. Contact AltaTec.")
            if context['active_model'] == 'mrp.production':
                production = production_obj.browse(cr, uid, context['active_id'], context=context)
            elif context['active_model'] == 'mrp.production.select.sources.wizard':
                wizard = wizard_obj.browse(cr, uid, context['active_id'], context=context)
                production = production_obj.browse(cr, uid, wizard.production_id.id, context=context)
            else:
                raise osv.except_osv("Error!", "Active model not found. Contact AltaTec.")

            # Get the product's uom_id
            product_obj = self.pool.get('product.product')
            product = product_obj.browse(cr, uid, product_id, context=context )
            res['value']['uom_id'] = product.uom_id.id

            # Get the stock available of this product in all locations
            location_ids = location_obj.search( cr, uid, [("active","=",True),("usage","in",["internal"])], context=context )
            for location_id in location_ids:

                # If this location has any child locations, ignore it
                parent_ids = location_obj.search( cr, uid, [("location_id", "=", location_id)], context=context )
                if len(parent_ids) > 0:
                    continue

                # If this location is used as a production location on the production order, ignore it
                if location_id == production.location_src_id.id:  continue
                if location_id == production.location_prod_id.id: continue
                if location_id == production.location_dest_id.id: continue

                context.update({'location': location_id})
                qty = product_obj.get_product_available(cr, uid, [product_id], context=context)

                if( qty[product_id] > 0 ):
                    res['value']['availability'].append( ( 0, 0, { 'location_id'   : location_id,
                                                                   'qty_available' : qty[product_id],
                                                                   'uom_id'        : product.uom_id.id,
                                                                 }
                                                       ) )
                    res['value']['is_available'] = True

        return res

    ###################################################################################
    # Column and defaults definitions
    ###################################################################################
    _columns = { 'wizard_id'      : fields.many2one("mrp.production.select.sources.wizard"),
                 'product_id'     : fields.many2one("product.product", "Producto",),
                 'qty'            : fields.float("Cantidad"),
                 'uom_id'         : fields.related('product_id','uom_id',type="many2one", relation="product.uom", string="Unidad(es)"),
                 'location_id'    : fields.many2one("stock.location", "Ubicacion de origen", domain=[("active","=",True),("usage","in",["internal"])]),
                 'availability'   : fields.one2many("mrp.production.select.sources.availability.line", "wizard_line_id", "Disponibilidad"),
                 'is_available'   : fields.boolean( "Is available?" ),
               }

###################################################################################
#
###################################################################################
class mrp_production_select_sources_availability_line(osv.osv_memory):

    _name = "mrp.production.select.sources.availability.line"

    _columns = { 'wizard_line_id' : fields.many2one("mrp.production.select.sources.wizard.line", "Wizard Line ID"),
                 'location_id'    : fields.many2one("stock.location", "Ubicacion ID"),
                 'qty_available'  : fields.float("Cantidad disponible"),
                 'uom_id'         : fields.many2one("product.uom","Unidad(es)"),
               }