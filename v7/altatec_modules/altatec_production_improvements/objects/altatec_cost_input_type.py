
from openerp.osv import fields, osv, orm
import logging

_logger = logging.getLogger(__name__)

class altatec_cost_input_type(osv.osv):

    _name='altatec.cost.input.type'

    _columns = { 'name'              : fields.char('Nombre'),
                 'debit_account_id'  : fields.many2one('account.account','Cuenta de Debito'),
                 'credit_account_id' : fields.many2one('account.account','Cuenta de Credito'),
               }
