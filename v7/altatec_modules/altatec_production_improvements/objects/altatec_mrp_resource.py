############################################################################################
# This file defines the "altatec.mrp.resource" class that exists as a one2many on
# an mrp.workcenter.line.
############################################################################################
from openerp.osv import fields, osv, orm
import logging

_logger = logging.getLogger(__name__)

############################################################################################
# altatec.mrp.resource class definition
############################################################################################
class altatec_mrp_resource(osv.osv):

    _name = 'altatec.mrp.resource'

    ############################################################################################
    # Calculates the cost per hour of a resource based on an employee's salary.
    ############################################################################################
    def get_cost_from_salary(self, cr, uid, ids, field_name, arg, context={}):
        res = {}
        for resource in self.browse(cr, uid, ids, context=context):
            if not resource.cost_from_salary or not resource.employee:
                res[resource.id] = 0.00
                continue
            contract = resource.employee.contract_id
            if contract.legal_hours:
                res[ resource.id ] = contract.wage / contract.legal_hours
            else:
                res[ resource.id ] = contract.wage / 240.00

        return res

    ############################################################################################
    # Gets either cost_per_unit_func or _cost_per_unit depending on if cost_from_salary is selected
    ############################################################################################
    def get_cost_displayed(self, cr, uid, ids, field_name, arg, context={}):
        res = {}
        for resource in self.browse(cr, uid, ids, context=context):
            if resource.cost_from_salary:
                res[resource.id] = resource.cost_per_unit_func
            else:
                res[resource.id] = resource.cost_per_unit
        return res

    ############################################################################################
    # Default function for "unit" field. Returns the ID of the "Hora(s)" unit of measure
    ############################################################################################
    def _get_default_unit(self, cr, uid, context={}):
        return self.pool.get('ir.model.data').get_object_reference(cr, uid, 'product', 'product_uom_hour')[1]

    ############################################################################################
    # If "cost_from_salary" is set, it sets "unit" to the "Hora(s)" unit of measure
    ############################################################################################
    def onchange_cost_from_salary(self, cr, uid, ids, cost_from_salary, context={}):
        res = {'value':{}}
        if cost_from_salary:
            res['value']['unit'] = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'product', 'product_uom_hour')[1]
        return res

    ############################################################################################
    # Column and default definitions
    ############################################################################################
    _columns = { 'name'               : fields.char('Nombre'),
                 'cost_per_unit'      : fields.float('Costo por unidad'),
                 'cost_from_salary'   : fields.boolean("Costo viene de sueldo?"),
                 'employee'           : fields.many2one("hr.employee", "Empleado"),
                 'cost_per_unit_func' : fields.function(get_cost_from_salary, type="float", string="Costo por unidad" ),
                 'cost_displayed'     : fields.function(get_cost_displayed, type="float", string="Costo por unidad"),
                 'unit'               : fields.many2one('product.uom','Unidad de medida'),
                 'cost_input_type'    : fields.many2one('altatec.cost.input.type','Tipo de costo'),
               }

    _defaults = { 'unit': _get_default_unit,
                }





