############################################################################################
#
# TODO: We need to prevent the procurement engine from confirming the production orders that
# it creates, because we choose the source warehouses manually. This change is done in the
# grupo_sovi_ventas module, but it needs to be provided for other clients.
############################################################################################
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
from openerp import netsvc
from datetime import datetime
from datetime import date
from dateutil.relativedelta import relativedelta

############################################################################################
# Inherited mrp.production class definition
############################################################################################

class mrp_production(osv.osv):

    _inherit = "mrp.production"

    ###################################################################################
    # Return the sum of cost of materia prima and the cost of all recursos
    ###################################################################################
    def get_cost_of_production(self, cr, uid, production_id, context={}):

        production = self.browse(cr, uid, production_id, context=context)

        cost = 0.00

        # Get total cost of materia prima
        for move in production.move_lines:
            cost += move.product_id.standard_price * move.product_qty

        # Get total cost of recursos
        for work_order in production.workcenter_lines:
            for resource in work_order.resources:
                cost += resource.resource.cost_displayed * work_order.delay

        return cost

    ###################################################################################
    # Return the sum of cost of materia prima and the cost of all recursos
    ###################################################################################
    def cancel_shipments(self, cr, uid, ids, context={}):
        for production in self.browse(cr, uid, ids, context ):

            moves_available_to_cancel = False
            for move in production.procurement_moves:
                if move.state != 'cancel' and move.state != 'done':
                    moves_available_to_cancel = True
            if not moves_available_to_cancel:
                raise osv.except_osv("Error", "No hay albaranes internos para anular. No se puede anular albaranes internos en el estado 'Realizado' ni 'Anulado'.")

            wizard_obj      = self.pool.get('mrp.production.cancel.shipments.wizard')
            wizard_line_obj = self.pool.get("mrp.production.cancel.shipments.wizard.line")

            product_lines = []
            for line in production.procurement_moves:
                if line.state != 'cancel' and line.state != 'done':
                    product_lines.append( ( 0, 0, { 'product_id'  : line.product_id.id,
                                                    'qty'         : line.product_qty,
                                                    'uom_id'      : line.product_uom.id,
                                                    'move_state'  : line.state,
                                                    'location_id' : line.location_id.id,
                                                    'move_id'     : line.id,
                                                    'is_selected' : False
                                                  } ) )

            if len(product_lines) < 1:
                raise osv.except_osv("Error", "No hay productos para eliminar!")

            wizard_id = wizard_obj.create( cr, uid, { 'production_id' : production.id,
                                                      'product_lines' : product_lines,
                                                    }, context=context )

            return { 'type'      : 'ir.actions.act_window',
                     'name'      : 'Eliminar Consumibles',
                     'view_type' : 'form',
                     'view_mode' : 'form,tree',
                     'res_model' : 'mrp.production.cancel.shipments.wizard',
                     'res_id'    : wizard_id,
                     'target'    : 'new',
                   }

    ###################################################################################
    # Instead of popping up a wizard when hitting "Fabricar" let's create the wizard
    # and hit confirm automatically
    ###################################################################################
    def altatec_produce(self, cr, uid, ids, context=None):

        produce_obj = self.pool.get('mrp.product.produce')
        product_obj = self.pool.get('product.product')
        move_obj    = self.pool.get('stock.move')

        for production in self.browse(cr, uid, ids, context=context):

            # First get the cost price and qty available of this product
            avg_cost_before   = production.product_id.standard_price
            qty_avail_before  = production.product_id.qty_available

            # Get the price of all materia prima and recursos
            current_cost = self.get_cost_of_production(cr, uid, production.id, context)

            # Produce the product
            context.update({'active_model':'mrp.production', 'active_id':production.id, 'active_ids':[production.id]})
            produce_id = produce_obj.create(cr, uid, {'product_qty':production.product_qty, 'mode':'consume_produce'}, context=context)
            produce_obj.do_produce(cr, uid, [produce_id], context=context)

            production = self.browse(cr, uid, production.id, context=context)

            # Now set the Producto Terminado (Produccion -> Terminado) moves' move_dest_id fields to the
            # moves from the sale order, and make the sure moves from the sale order to the customer are
            # sourced from the Terminado location on the production order
            if production.picking_prod_id:
                for move in production.consume_picking.move_lines:
                    for final_move in production.picking_prod_id.move_lines:
                        if move.product_id.id == final_move.product_id.id:
                            move.write({'move_dest_id' : final_move.id})
                            final_move.write({'location_id': production.location_dest_id.id})

            # Make sure all of the moves in the production order's final picking are done
            for move in production.consume_picking.move_lines:
                if move.state != 'done' and move.state != 'cancel':
                    move_obj.action_done(cr, uid, [move.id], context=context)

            # Mark the picking that contains the materia prima consumed and the final
            # product to inventory as "Done"
            production.consume_picking.write({'state':'done'})

            # Now update the cost price of the final product
            cost_after = (avg_cost_before * qty_avail_before + current_cost * production.product_qty) \
                         / (qty_avail_before + production.product_qty)

            product_obj.write(cr, uid, [production.product_id.id], {'standard_price':cost_after}, context=context)

    ###################################################################################
    #
    ###################################################################################
    def create_move_chains(self, cr, uid, production_id, context=None):

        production_obj = self.pool.get('mrp.production')
        move_obj       = self.pool.get('stock.move')
        production     = production_obj.browse(cr, uid, production_id, context=context)

        warehouse_moves_assigned = []
        internal_moves_assigned = []

        for final_move in production.move_lines:

            move_dest_set = False
            for internal_move in production.picking_id.move_lines:
                if  internal_move.product_id.id == final_move.product_id.id \
                and internal_move.product_uom.id == final_move.product_uom.id \
                and internal_move.id not in internal_moves_assigned \
                and internal_move.state != 'cancel':
                    internal_move.write({'move_dest_id':final_move.id})
                    internal_moves_assigned.append(internal_move.id)
                    move_dest_set = True

            if not move_dest_set:
                for warehouse_move in production.procurement_moves:
                    if  warehouse_move.product_id.id == final_move.product_id.id \
                    and warehouse_move.product_uom.id == final_move.product_uom.id \
                    and warehouse_move.id not in warehouse_moves_assigned \
                    and warehouse_move.state != 'cancel':
                        warehouse_move.write({'move_dest_id':final_move.id})
                        warehouse_moves_assigned.append( warehouse_move.id )

        for internal_move in production.picking_id.move_lines:
            for warehouse_move in production.procurement_moves:
                if  warehouse_move.product_id.id == internal_move.product_id.id \
                and warehouse_move.product_uom.id == internal_move.product_uom.id \
                and warehouse_move.id not in warehouse_moves_assigned \
                and warehouse_move.state != "cancel":
                    warehouse_move.write({'move_dest_id':internal_move.id})
                    warehouse_moves_assigned.append( warehouse_move.id )

        # The last chain we need to setup is the moves of materia prima from consumido -> produccion to the move of
        # the final product from produccion -> terminado
        final_product_move_id = move_obj.search(cr, uid, [('picking_id', '=', production.consume_picking.id),
                                                          ('product_id', '=', production.product_id.id)
                                                         ], context=context)[0]

        for final_move in production.consume_picking.move_lines:
            if final_move.id != final_product_move_id:
                final_move.write({ 'move_dest_id' : final_product_move_id })

        # Set up the proceso->consumido as auto-picking
        production.picking_id.write({'auto_picking':True})

    ############################################################################################
    #
    ############################################################################################
    def remove_products(self, cr, uid, ids, context=None ):
        for production in self.browse(cr, uid, ids, context ):

            if not len(production.move_lines):
                raise osv.except_osv("Error", "No hay productos en este orden de produccion para eliminar.")

            wizard_obj      = self.pool.get('mrp.production.remove.products.wizard')
            wizard_line_obj = self.pool.get("mrp.production.remove.products.wizard.line")

            product_qtys = {}

            for move in production.move_lines:
                if not move.product_id.id in product_qtys:
                    product_qtys[move.product_id.id] = { 'qty_original'  : 0.0,
                                                         'qty_consumed'  : 0.0,
                                                         'qty_available' : 0.0,
                                                       }
                product_qtys[move.product_id.id]['qty_original']  += move.product_qty
                product_qtys[move.product_id.id]['qty_available'] += move.product_qty

            for move in production.move_lines2:
                if not move.product_id.id in product_qtys:
                    product_qtys[move.product_id.id] = { 'qty_original'  : 0.0,
                                                         'qty_consumed'  : 0.0,
                                                         'qty_available' : 0.0,
                                                       }
                product_qtys[move.product_id.id]['qty_original'] += move.product_qty
                product_qtys[move.product_id.id]['qty_consumed'] += move.product_qty

            product_lines = []

            for product_id in product_qtys:
                product_lines.append( ( 0, 0, { "product_id"    : product_id,
                                                "qty_original"  : product_qtys[product_id]['qty_original'],
                                                "qty_consumed"  : product_qtys[product_id]['qty_consumed'],
                                                "qty_available" : product_qtys[product_id]['qty_available'],
                                                "qty_to_remove" : 0.0
                                              } ) )

            if len(product_lines) < 1:
                raise osv.except_osv("Error", "No hay productos para eliminar!")

            wizard_id = wizard_obj.create( cr, uid, { 'line_ids' : product_lines,
                                                    }, context=context )

            return { 'type'      : 'ir.actions.act_window',
                     'name'      : 'Eliminar Consumibles',
                     'view_type' : 'form',
                     'view_mode' : 'form,tree',
                     'res_model' : 'mrp.production.remove.products.wizard',
                     'res_id'    : wizard_id,
                     'target'    : 'new',
                   }

    ############################################################################################
    #
    ############################################################################################
    def add_products(self, cr, uid, ids, context=None):
        for production in self.browse(cr, uid, ids, context ):

            wizard_obj      = self.pool.get('mrp.production.select.sources.wizard')
            wizard_line_obj = self.pool.get("mrp.production.select.sources.wizard.line")

            products = []
            for line in production.product_ids:
                products.append((4, line.id))

            wizard_id = wizard_obj.create( cr, uid, { 'production_id' : production.id,
                                                      'product_ids'   : products,
                                                      'mode'          : 'add',
                                                    }, context=context )


            return { 'type'      : 'ir.actions.act_window',
                     'name'      : 'Agregar Consumibles',
                     'view_type' : 'form',
                     'view_mode' : 'form,tree',
                     'res_model' : 'mrp.production.select.sources.wizard',
                     'res_id'    : wizard_id,
                     'target'    : 'new',
                   }

    ############################################################################################
    # generar_asiento()
    ############################################################################################
    def generar_asientos_contables(self, cr, uid, ids, context=None):

        if not isinstance(ids, list):
            ids = [ids]

        move_line_obj = self.pool.get('account.move.line')
        account_move_defaults = self.pool.get('account.move').default_get(cr,uid,['date','period_id'],context=context)
        account_move_line_defaults=self.pool.get('account.move.line').default_get(cr, uid,[ 'blocked',
                                                                                            'centralisation',
                                                                                            'date',
                                                                                            'date_created',
                                                                                            'state',
                                                                                            'currency_id',
                                                                                            'account_id',
                                                                                            'company_id'
                                                                                          ], context=context)
        for production_order in self.browse(cr, uid, ids, context=context):

            if production_order.workcenter_move_id:
                continue

            # If there are no resources used in this production order, then don't make an account move
            num_resources = 0
            for order in production_order.workcenter_lines:
                for resource in order.resources:
                    num_resources += 1
            if not num_resources:
                continue

            # Create the account move
            journal = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'altatec_production_improvements', 'account_journal_production')
            account_move = self.pool.get('account.move').create(cr, uid, { 'journal_id' : journal[1],
                                                                           'name'       : production_order.name,
                                                                           'date'       : account_move_defaults['date'],
                                                                           'period_id'  : account_move_defaults['period_id'],
                                                                           'ref'        : order.id,
                                                                         })
            production_order.write({ 'workcenter_move_id': account_move })

            # Now create a pair of move lines for each resource for each workcenter line
            for order in production_order.workcenter_lines:

                for resource in order.resources:
                    if resource.resource.cost_from_salary:
                        resource_cost = order.delay * resource.resource.cost_per_unit_func
                    else:
                        resource_cost = order.delay * resource.resource.cost_per_unit

                    debit_account = resource.resource.cost_input_type.debit_account_id
                    credit_account= resource.resource.cost_input_type.credit_account_id

                    for x  in range(0,2):
                        move_line_obj.create(cr, uid, { 'name'           : 'account_move_'+resource.name+'_'+('debit' if x== 0 else 'credit'),
                                                        'blocked'        : account_move_line_defaults['blocked'],
                                                        'centralisation' : account_move_line_defaults['centralisation'],
                                                        'date'           : account_move_line_defaults['date'],
                                                        'date_created'   : account_move_line_defaults['date_created'],
                                                        'state'          : account_move_line_defaults['state'],
                                                        'currency_id'    : account_move_line_defaults['currency_id'],
                                                        'journal_id'     : journal[1] ,
                                                        'credit'         : resource_cost if x==1 else 0.00 ,
                                                        'debit'          : resource_cost if x==0 else 0.00,
                                                        'account_id'     : debit_account.id if x ==0 else credit_account.id,
                                                        'period_id'      : account_move_defaults['period_id'],
                                                        'company_id'     : account_move_line_defaults['company_id'],
                                                        'move_id'        : account_move
                                                      })

    ############################################################################################
    # _make_picking_interno() create a stock picking containing all of the stock moves of
    # materia prima -> virtual/production and final product -> stock
    ############################################################################################
    def _make_picking_interno(self, cr, uid, production, context=None):
        pick_name = self.pool.get('ir.sequence').get(cr, uid, 'stock.picking')
        picking_id = self.pool.get('stock.picking').create(cr, uid, {
            'name': pick_name,
            'origin': (production.origin or '').split(':')[0] + ':' + production.name,
            'type': 'internal',
            'move_type': 'one',
            'state': 'confirmed',
            'auto_picking': False,
            'company_id': production.company_id.id,
            'from_production': True,
            })
        production.write({'consume_picking':picking_id})

        return picking_id

    ############################################################################################
    # _make_production_produce_line() override for mrp.production
    # Adds the account_analytic_id to the production produce stock move
    ############################################################################################
    def _make_production_produce_line(self, cr, uid, production, context=None):
        res=super(mrp_production, self)._make_production_produce_line(cr, uid, production,context=context)
        move=self.pool.get('stock.move').browse(cr,uid,res,context=context)
        picking_id=self._make_picking_interno(cr, uid, production, context=context)
        self.pool.get('stock.move').write(cr, uid,move.id, {'account_analytic_id':production.project_id.id,'picking_id':picking_id }, context=context)
        return res

    ############################################################################################
    # _make_production_consume_line() override for mrp.production
    # Adds the account_analytic_id to the production consume stock move
    ############################################################################################
    def _make_production_consume_line(self, cr, uid, production_line, parent_move_id, source_location_id=False, context=None):
        res=super(mrp_production, self)._make_production_consume_line(cr, uid, production_line,parent_move_id,source_location_id=source_location_id ,context=context)
        move=self.pool.get('stock.move').browse(cr,uid,res,context=context)
        picking=self.pool.get('stock.move').browse(cr,uid,parent_move_id,context=context)
        self.pool.get('stock.move').write(cr, uid,move.id, {'account_analytic_id':production_line.production_id.project_id.id,'picking_id':picking.picking_id.id}, context=context)
        return res

    ############################################################################################
    # _make_production_internal_shipment_line() override for mrp.production
    # Adds the account_analytic_id to the stock move created by the internal shipment
    ############################################################################################
    def _make_production_internal_shipment_line(self, cr, uid, production_line, shipment_id, parent_move_id, destination_location_id=False, context=None):
        res=super(mrp_production, self)._make_production_internal_shipment_line(cr, uid, production_line, shipment_id, parent_move_id, destination_location_id=destination_location_id, context=context)
        move=self.pool.get('stock.move').browse(cr,uid,res,context=context)
        self.pool.get('stock.move').write(cr, uid,move.id, {'account_analytic_id':production_line.production_id.project_id.id }, context=context)
        return res

    ############################################################################################
    # create() override for mrp.production
    ############################################################################################
    def create(self, cr, uid, vals, context={}):

        # If the user (or the caller of create) put nothing in "product_ids"
        if not 'product_ids' in vals or not len(vals['product_ids']):

            if not 'product_id' in vals or not vals['product_id']:
                raise osv.except_osv("Error", "Debe seleccionar al menos 1 producto para producir")

            # Copy the values from product_id, product_qty, and uom_id to a new element of product_ids
            vals['product_ids'] = [(0, 0, { 'product_id'  : vals['product_id'],
                                            'product_qty' : vals['product_qty'],
                                            'uom_id'      : vals['product_uom'],
                                          }
                                  )]
        else:

            # Validate the quantity they input for each product
            for product_line in vals['product_ids']:
                if product_line[2]['product_qty'] <= 0:
                    raise osv.except_osv("Error", "Debe seleccionar una cantidad para producir mayor que 0")

            # Copy the product_id, qty, and uom_id from the first element in product_ids to the product_id, qty and uom fields on this mrp.production
            vals['product_id']  = vals['product_ids'][0][2]['product_id']
            vals['product_qty'] = vals['product_ids'][0][2]['product_qty']
            vals['product_uom'] = vals['product_ids'][0][2]['uom_id']

        # If the user didn't choose a bom, try got get it from the product, else throw an error
        if not 'bom_id' in vals or not vals['bom_id']:
            onchange_dict = self.product_id_change(cr, uid, [], vals['product_id'], context=context)
            if 'value' in onchange_dict and 'bom_id' in onchange_dict['value'] and onchange_dict['value']['bom_id']:
                vals['bom_id'] = onchange_dict['value']['bom_id']

        if not 'bom_id' in vals or not vals['bom_id']:
            raise osv.except_osv("Error", "Debe seleccionar una Lista de material")

        # If the user didn't select a Ruta, try to get it from the BOM, else throw an error
        if not 'routing_id' in vals or not vals['routing_id']:
            bom = self.pool.get('mrp.bom').browse(cr, uid, vals['bom_id'], context=context)
            if bom.routing_id:
                vals['routing_id'] = bom.routing_id.id
            else:
                raise osv.except_osv("Error", "Debe seleccionar una Ruta")

        name_mrp_production=self.pool.get('ir.sequence').get(cr, uid, 'mrp.production')
        vals.update({'name':str(name_mrp_production)})
        res = super(mrp_production, self).create(cr, uid, vals, context)

        if 'move_prod_id' not in vals:
            return res
        else:
            procurement_id = self.pool.get('procurement.order').search(cr,uid,[('move_id.id', '=',vals['move_prod_id'] )])
            procurement=self.pool.get('procurement.order').browse(cr,uid,procurement_id)
            validacion=len(procurement)
            if (validacion>1):
                raise osv.except_osv(
                    _('Hay mas de una orden!'),
                    _('Contactar a Altatec!'))

            if (validacion==0):
                raise osv.except_osv(
                    _('No hay orden!'),
                    _('Contactar a Altatec!'))

            if (procurement[0].project_id):
                self.pool.get('mrp.production').write(cr, uid,res, { 'project_id': procurement[0].project_id.id }, context=context)

            return res

    ############################################################################################
    # write() override for mrp.production
    ############################################################################################
    def write(self, cr, uid, ids, vals, context={}, update=False):

        res = super(mrp_production, self).write(cr, uid, ids, vals, context=context, update=False)

        if not isinstance(ids, list):
            ids = [ids]

        for production in self.browse(cr, uid, ids, context=context):
            if not len(production.product_ids):
                raise osv.except_osv("Error", "Debe seleccionar al menos 1 producto para producir")

            for product_line in production.product_ids:
                if product_line.product_qty <= 0.0:
                    raise osv.except_osv("Error", "Debe seleccionar una cantidad para producir mayor que 0")

            if 'product_ids' in vals:
                self.write(cr, uid, [production.id], { 'product_id'  : production.product_ids[0].product_id.id,
                                                       'product_qty' : production.product_ids[0].product_qty,
                                                       'product_uom' : production.product_ids[0].uom_id.id,
                                                       'bom_id'      : production.product_ids[0].product_id.bom_ids[0].id,
                                                     }, context=context)

        return res

    ############################################################################################
    # New column definitions for mrp.production
    ############################################################################################
    def confirm_and_select_sources(self, cr, uid, ids, context=None):

        if not context:
            context = {}

        for production in self.browse(cr, uid, ids, context ):

            context['active_model'] = "mrp.production"
            context[   'active_id'] = production.id

            wizard_obj      = self.pool.get('mrp.production.select.sources.wizard')
            wizard_line_obj = self.pool.get("mrp.production.select.sources.wizard.line")

            bom_lines = []
            for line in production.bom_id.bom_lines:
                res = wizard_line_obj.onchange_product_id( cr, uid, ids=False, product_id=line.product_id.id, availability=False, context=context)
                bom_lines.append( ( 0, 0, { 'product_id'   : line.product_id.id,
                                            'qty'          : line.product_qty / production.bom_id.product_qty * production.product_qty,
                                            'uom_id'       : line.product_uom.id,
                                            'availability' : res['value']['availability'],
                                            'is_available' : res['value']['is_available'],
                                            } ) )

            products = []
            for line in production.product_ids:
                products.append((4, line.id))

            wizard_id = wizard_obj.create( cr, uid, { 'production_id' : production.id,
                                                      'product_ids'   : products,
                                                      'source_lines'  : bom_lines,
                                                      'mode'          : 'confirm',
                                                      }, context=context )

            return { 'type'      : 'ir.actions.act_window',
                     'name'      : 'Eligir Materia Prima',
                     'view_type' : 'form',
                     'view_mode' : 'form,tree',
                     'res_model' : 'mrp.production.select.sources.wizard',
                     'res_id'    : wizard_id,
                     'target'    : 'new',
                   }

    ############################################################################################
    # Get default source location from user's company
    ############################################################################################
    def _get_default_location_src( self, cr, uid, context=None):
        company = self.pool.get('res.users').browse(cr,uid,uid,context).company_id
        if company and company.default_production_src_location:
            return company.default_production_src_location.id
        else:
            raise osv.except_osv("Error!", "Hay que configurar la ubicacion materias primas para la compania: " + str(company.name))
        return False

    ############################################################################################
    # Get default destination location from user's company
    ############################################################################################
    def _get_default_location_dest( self, cr, uid, context=None):
        company = self.pool.get('res.users').browse(cr,uid,uid,context).company_id
        if company and company.default_production_dest_location:
            return company.default_production_dest_location.id
        else:
            raise osv.except_osv("Error!", "Hay que configurar la ubicacion productos finalizados para la compania: " + str(company.name))
        return False

    ############################################################################################
    # get invoices that have this ID as their origin
    ############################################################################################
    def get_facturas(self, cr, uid, ids, field_name, arg, context={}):
        res = {}
        ordenes=[]
        ordenes=self.browse(cr,uid,ids)
        for r in ordenes:
            res[r.id] = []
            if r.origin:
                res[r.id] = self.pool.get('account.invoice').search(cr, uid,[('origin', '=', r.origin)], context=context)
        return res

    ############################################################################################
    # return True if this production order has returns
    ############################################################################################
    def has_returns_func(self, cr, uid, ids, field_name, arg, context={}):
        res = {}
        for production in self.browse(cr, uid, ids, context=context):
            if len(production.return_moves):
                res[production.id] = True
            else:
                res[production.id] = False
        return res

    ############################################################################################
    # Return the picking id of this production order's move_prod_id
    ############################################################################################
    def get_picking_prod_id(self, cr, uid, ids, field_name, arg, context={}):
        res = {}
        for production in self.browse(cr, uid, ids, context=context):
            picking = False
            if production.move_prod_id and production.move_prod_id.picking_id:
                picking = production.move_prod_id.picking_id.id
            res[production.id] = picking
        return res

    ############################################################################################
    # New column definitions for mrp.production
    ############################################################################################
    _columns = { 'project_id'         : fields.many2one('account.analytic.account', 'Proyecto'),
                 'consume_picking'    : fields.many2one('stock.picking','Consume picking'),
                 'workcenter_move_id' : fields.many2one('account.move', "Asiento contable"),
                 'location_prod_id'   : fields.related("routing_id", "location_id", type="many2one", relation="stock.location", string="Ubicacion de produccion"),
                 'procurement_moves'  : fields.one2many( 'stock.move', 'production_id_procurement', "Fuentes de materia prima" ),
                 'workcenter_lines'   : fields.one2many('mrp.production.workcenter.line', 'production_id', 'Work Centers Utilisation',
                                                        readonly=True, states={'draft':[('readonly',False)],
                                                                               'confirmed':[('readonly',False)],
                                                                               'ready':[('readonly',False)],
                                                                               'in_production':[('readonly',False)]}),
                 'name'               : fields.char('Reference', size=64),
                 'create_date'        : fields.datetime('Fecha de Creacion', readonly=True),
                 'facturas'           : fields.function(get_facturas, type='one2many', obj='account.invoice', string='Facturas', method=True),
                 'fotomontaje'        : fields.char("Boceto / Fotomontaje",size=10),
                 'encargado'          : fields.many2one('altatec.mrp.resource','Encargado de Obra'),
                 'inspector'          : fields.many2one('altatec.mrp.resource','Inspector de Obra'),
                 'has_returns'        : fields.function(has_returns_func, type="boolean", string="Tiene Devueltos?", method=True),
                 'return_moves'       : fields.one2many('stock.move', 'production_id_return', "Albaranes devueltos"),
                 'product_ids'        : fields.one2many("altatec.final.product.line", "production_id", "Productos"),
                 'picking_prod_id'    : fields.function(get_picking_prod_id, type="many2one", obj="stock.picking", string="Movimiento de producto(s)"),
               }

    _defaults = { 'location_src_id'  : _get_default_location_src,
                  'location_dest_id' : _get_default_location_dest,
                  'name'             : None,
                  'has_returns'      : False,
                }
    _order = 'id desc'

    def read_group(self, cr, uid, domain, fields, groupby, offset=0, limit=None, context=None, orderby=False):
        if 'origin' in groupby:
            return super(mrp_production, self).read_group(cr,uid,domain,fields,groupby, offset=offset, limit=limit, context=context, orderby='origin DESC')
        else:
            return super(mrp_production, self).read_group(cr,uid,domain,fields,groupby, offset=offset, limit=limit, context=context, orderby=orderby)

    ############################################################################################
    # Inherited to loop each line and add the resources from routing
    ############################################################################################
    def _action_compute_lines(self, cr, uid, ids, properties=None, context=None):
        result = super(mrp_production, self)._action_compute_lines( cr, uid, ids, properties=properties, context=context)
        lista_mrp_production = self.browse(cr,uid,ids)
        for record in lista_mrp_production:
            for linea in record.workcenter_lines:
                for plantilla_line in linea.plantilla_id.resource_line:
                    self.pool.get('altatec.mrp.resource.instance.line').create(cr,uid,{
                                    'mrp_production_workcenter_id':linea.id,
                                    'name':plantilla_line.name,
                                    'resource':plantilla_line.resource.id,

                })

        return result

############################################################################################
# altatec.final.product.line class definition
############################################################################################
class altatec_final_product_line(osv.osv):

    _name = "altatec.final.product.line"

    def altatec_onchange_product_id(self, cr, uid, ids, product_id, context=None):
        retval = {}
        if product_id:
            product = self.pool.get('product.product').browse(cr, uid, product_id, context=context)
            retval['value'] = { 'uom_id' : product.uom_id.id }
        return retval

    _columns = { "production_id" : fields.many2one("mrp.production", "Produccion ID"),
                 "product_id"    : fields.many2one("product.product", "Producto", domain="[('bom_ids','!=',False),('bom_ids.bom_id','=',False)]", required=True),
                 "product_qty"   : fields.float("Cantidad", required=True),
                 "uom_id"        : fields.many2one("product.uom", "Unidad(es)", required=True),
               }

############################################################################################
# Inherited procurement.order class definition
############################################################################################
class procurement_order(osv.osv):

    _inherit = 'procurement.order'

    _columns = { 'project_id'      : fields.many2one('account.analytic.account', 'Proyecto'),
                 'message': fields.char('Latest error', help="Exception occurred while computing procurement orders."),

               }

 ############################################################################################
# Inherited mrp.production.workcenter.line class definition
############################################################################################
class mrp_production_workcenter_line(osv.osv):

    _inherit = 'mrp.production.workcenter.line'

    _columns = { 'resources' : fields.one2many('altatec.mrp.resource.instance.line','mrp_production_workcenter_id','Resources'),
                 'asiento'   : fields.related('production_id', 'workcenter_move_id', type="many2one", relation="account.move", string='Asiento Relacionado'),
               }

############################################################################################
# Inherited mrp.product.produce class definition
############################################################################################
class mrp_product_produce(osv.osv):

    _inherit = "mrp.product.produce"

    ############################################################################################
    # do_produce() override. Make sure all the moves in the "Fuentes de materia prima"
    # list are completed. Also make sure all the workcenter lines are completed.
    ############################################################################################
    def do_produce(self, cr, uid, ids, context=None):

        if not context or 'active_model' not in context or 'active_id' not in context or context['active_model'] != 'mrp.production':
            raise osv.except_osv("Error!", "Active model not found. Contact AltaTec.")

        for production in self.pool.get('mrp.production').browse( cr, uid, context['active_ids'], context=context):

            # Make sure all of the warehouse->processing stock moves are completed
            for move in production.procurement_moves:
                if move.state != "done" and move.state != "cancel":
                    raise osv.except_osv("Error!", "No se puede fabricar el producto hasta que todos los movimientos de stock en 'Ubicaciones de origen' estan realizados.")

            # Make sure all the workcenter lines are completed
            for line in production.workcenter_lines:
                if line.state != "done" and line.state != "cancel":
                    raise osv.except_osv("Error!", "No se puede fabricar el producto hasta que todos los ordenes de trabajo estan realizados.")

            # Generate the account moves from the workcenter lines
            self.pool.get('mrp.production').generar_asientos_contables(cr, uid, production.id, context=context)

        return super(mrp_product_produce,self).do_produce(cr, uid, ids, context=context)

############################################################################################
# Inherited account_invoice class definition
############################################################################################
class account_invoice(osv.osv):

    _inherit = "account.invoice"

    _columns ={ 'produccion'  : fields.many2one( "mrp.production", "Produccion"),
              }

#################################################################################################
# Prevent the user from making a BoM line with a unit other than the product's inventory unit
#################################################################################################
class mrp_bom(osv.osv):

    _inherit = "mrp.bom"

    #################################################################################################
    # Prevent the user from making a BoM line with a unit other than the product's inventory unit
    #################################################################################################
    def create(self, cr, uid, vals, context={}):
        if 'bom_lines' in vals:
            product_obj = self.pool.get('product.product')
            for line in vals['bom_lines']:
                product = product_obj.browse(cr, uid, line[2]['product_id'], context=context)
                if product.uom_id.id != line[2]['product_uom']:
                    raise osv.except_osv("Error", "La unidad de medida debe ser la unidad de inventario del producto." )
        return super(mrp_bom, self).create(cr, uid, vals, context=context)

    #################################################################################################
    # Prevent the user from making a BoM line with a unit other than the product's inventory unit
    #################################################################################################
    def write(self, cr, uid, ids, vals, context={}):
        res = super(mrp_bom, self).write(cr, uid, ids, vals, context=context)
        boms = self.browse(cr, uid, ids, context=context)
        for bom in boms:
            for line in bom.bom_lines:
                if line.product_uom.id != line.product_id.uom_id.id:
                    raise osv.except_osv("Error", "La unidad de medida debe ser la unidad de inventario del producto." )
        return res

    #################################################################################################
    # Prevent the user from making a BoM line with a unit other than the product's inventory unit
    #################################################################################################
    def onchange_uom(self, cr, uid, ids, product_id, product_uom, context=None):
        res = super(mrp_bom, self).onchange_uom(cr, uid, ids, product_id, product_uom, context=None)

        if product_id:
            product = self.pool.get('product.product').browse(cr, uid, product_id, context=context)
            if product.uom_id.id != product_uom:
                res['warning'] = {'title':'Aviso', 'message':'La unidad de medida debe ser la unidad de inventario del producto.'}
                res['value']   = {'product_uom' : product.uom_id.id}

        return res


