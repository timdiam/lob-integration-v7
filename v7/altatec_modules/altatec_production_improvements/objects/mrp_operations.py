from openerp.osv import fields
from openerp.osv import osv
import time
from datetime import datetime
from openerp import tools
from openerp.addons.product import _common

class mrp_production_workcenter_line(osv.osv):

    _inherit = 'mrp.production.workcenter.line'

    def action_start_working(self, cr, uid, ids, context=None):
        for line in self.browse(cr, uid, ids, context=context):
            if not line.resources:
                raise osv.except_osv("Error", "No se puede iniciar un orden de trabajo sin definir un recurso")
        return super(mrp_production_workcenter_line, self).action_start_working(cr, uid, ids, context=context)

    def action_done(self, cr, uid, ids, context=None):
        """ Sets state to done, writes finish date and calculates delay.
        @return: True
        """
        delay = 0.0
        date_now = time.strftime('%Y-%m-%d %H:%M:%S')
        obj_line = self.browse(cr, uid, ids[0])

        date_start = datetime.strptime(obj_line.date_start,'%Y-%m-%d %H:%M:%S')
        date_finished = datetime.strptime(date_now,'%Y-%m-%d %H:%M:%S')
        delay += (date_finished-date_start).days * 24
        delay += (date_finished-date_start).seconds / float(60*60)

        self.write(cr, uid, ids, {'state':'done', 'date_finished': date_now,'delay':delay}, context=context)
        #We don't want to modify the production order state...
        #self.modify_production_order_state(cr,uid,ids,'done')
        obj = self.browse(cr,uid,ids)[0]

        lista_recursos=obj.resources

        for linea in lista_recursos:
            costo=linea.resource.cost_displayed
            linea.write({'costo_trabajo':costo})

        # self.pool.get('altatec.mrp.resource.instance.line').write( cr, uid,  ids,{'costo_trabajo':costo}, context=context)

        calendario=obj.workcenter_id['calendar_id']
        fechaComienzo=datetime.strptime(obj.date_start,'%Y-%m-%d %H:%M:%S')
        fechaActual=datetime.strptime(date_now,'%Y-%m-%d %H:%M:%S')
        # '_interval_hours_get' calculates a range of date inside the working hours described in the resource calendar of the workcenter designed to this workcenter_lines
        #   It retunrs a float of the WORKING hours taken from the date_start to the actual moment it is changed to 'done'
        tiempo=self.pool.get('resource.calendar')._interval_hours_get( cr, uid, calendario.id,fechaComienzo ,fechaActual,timezone_from_uid=uid )
        self.write(cr, uid, ids, {'delay':tiempo}, context=context)
        return True

    def _get_default_workcenter(self,cr,uid,context={}):
        ids = self.pool.get('mrp.workcenter').search(cr,uid,[],context=context)
        if ids:
            return ids[0]
        else:
            return None



    _defaults = {
                'workcenter_id':_get_default_workcenter,
    }

    def action_pause(self, cr, uid, ids, context=None):
        obj = self.browse(cr,uid,ids)[0]
        calendario=obj.workcenter_id['calendar_id']
        date_now = time.strftime('%Y-%m-%d %H:%M:%S')
        fechaComienzo=datetime.strptime(obj.date_start,'%Y-%m-%d %H:%M:%S')
        fechaActual=datetime.strptime(date_now,'%Y-%m-%d %H:%M:%S')
        #   It retunrs a float of the WORKING hours taken from the date_start to the actual moment it is changed to 'paused'
        tiempo_actual=self.pool.get('resource.calendar').interval_hours_get( cr, uid, calendario.id,fechaComienzo ,fechaActual )

        self.write(cr, uid, ids, {'tiempo_actual':tiempo_actual}, context=context)
        return super(mrp_production_workcenter_line,self).action_pause(cr, uid, ids, context=context)

    # tiempo_actual is a debugg field to test the calculation of working hours
    _columns={
        'tiempo_actual': fields.float(string='Horas transcurridas post-pausa'),
        'hour': fields.float(string='Horas Planificadas', digits=(16,2)),
        'plantilla_id':fields.many2one('mrp.routing.workcenter', "Definition line"),

    }

class altatec_mrp_resource_definition_line(osv.osv):
     _name='altatec.mrp.resource.definition.line'

     _columns={
        'name'                        :fields.char('Descripcion',required=True),
        'resource'                    :fields.many2one('altatec.mrp.resource','Recurso',required=True),
        'unit'                        :fields.related('resource', 'unit',type='many2one',relation='product.uom',string='Unidad'),
        'mrp_routing_workcenter_id'   :fields.many2one('mrp.routing.workcenter','MPW'),
        'date'                        : fields.date('Fecha de registro',required=True),
        'costo_trabajo'               : fields.float("Costo del trabajo"),
    }

class mrp_routing_workcenter(osv.osv):

    _inherit = 'mrp.routing.workcenter'

    def _get_default_workcenter(self,cr,uid,context={}):
        ids = self.pool.get('mrp.workcenter').search(cr,uid,[],context=context)
        if ids:
            return ids[0]
        else:
            return None

    _columns={
                'resource_line': fields.one2many('altatec.mrp.resource.definition.line','mrp_routing_workcenter_id','Resources'),
    }

    _defaults = {
                'workcenter_id':_get_default_workcenter,
    }

class mrp_routing(osv.osv):

    _inherit = 'mrp.routing'

    ############################################################################################
    # Get default production location from user's company
    ############################################################################################
    def _get_default_location_prod( self, cr, uid, context=None):
        company = self.pool.get('res.users').browse(cr,uid,uid,context).company_id
        if company and company.default_production_prod_location:
            return company.default_production_prod_location.id
        else:
            raise osv.except_osv("Error!", "Hay que configurar la ubicacion de produccion para la compania: " + str(company.name) )
        return False


    _defaults = { 'location_id' : _get_default_location_prod,
                }

class mrp_bom(osv.osv):

    _inherit = 'mrp.bom'


    def _bom_explode(self, cr, uid, bom, factor, properties=None, addthis=False, level=0, routing_id=False):
        """ REWRITE for including wc_use.id as a plantilla_id to take resource from this object to workcenter_lines
        Finds Products and Work Centers for related BoM for manufacturing order.
        @param bom: BoM of particular product.
        @param factor: Factor of product UoM.
        @param properties: A List of properties Ids.
        @param addthis: If BoM found then True else False.
        @param level: Depth level to find BoM lines starts from 10.
        @return: result: List of dictionaries containing product details.
                 result2: List of dictionaries containing Work Center details.
        """
        routing_obj = self.pool.get('mrp.routing')
        factor = factor / (bom.product_efficiency or 1.0)
        factor = _common.ceiling(factor, bom.product_rounding)
        if factor < bom.product_rounding:
            factor = bom.product_rounding
        result = []
        result2 = []
        phantom = False
        if bom.type == 'phantom' and not bom.bom_lines:
            newbom = self._bom_find(cr, uid, bom.product_id.id, bom.product_uom.id, properties)

            if newbom:
                res = self._bom_explode(cr, uid, self.browse(cr, uid, [newbom])[0], factor*bom.product_qty, properties, addthis=True, level=level+10)
                result = result + res[0]
                result2 = result2 + res[1]
                phantom = True
            else:
                phantom = False
        if not phantom:
            if addthis and not bom.bom_lines:
                result.append(
                {
                    'name': bom.product_id.name,
                    'product_id': bom.product_id.id,
                    'product_qty': bom.product_qty * factor,
                    'product_uom': bom.product_uom.id,
                    'product_uos_qty': bom.product_uos and bom.product_uos_qty * factor or False,
                    'product_uos': bom.product_uos and bom.product_uos.id or False,
                })
            routing = (routing_id and routing_obj.browse(cr, uid, routing_id)) or bom.routing_id or False
            if routing:
                for wc_use in routing.workcenter_lines:
                    wc = wc_use.workcenter_id
                    d, m = divmod(factor, wc_use.workcenter_id.capacity_per_cycle)
                    mult = (d + (m and 1.0 or 0.0))
                    cycle = mult * wc_use.cycle_nbr
                    result2.append({
                        'plantilla_id':wc_use.id,
                        'name': tools.ustr(wc_use.name) + ' - '  + tools.ustr(bom.product_id.name),
                        'workcenter_id': wc.id,
                        'sequence': level+(wc_use.sequence or 0),
                        'cycle': cycle,
                        'hour': float(wc_use.hour_nbr*mult + ((wc.time_start or 0.0)+(wc.time_stop or 0.0)+cycle*(wc.time_cycle or 0.0)) * (wc.time_efficiency or 1.0)),
                    })
            for bom2 in bom.bom_lines:
                res = self._bom_explode(cr, uid, bom2, factor, properties, addthis=True, level=level+10)
                result = result + res[0]
                result2 = result2 + res[1]
        return result, result2