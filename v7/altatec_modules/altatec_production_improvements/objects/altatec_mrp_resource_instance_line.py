from openerp.osv import fields, osv, orm
import logging

_logger = logging.getLogger(__name__)


class altatec_mrp_resource_instance_line(osv.osv):

    _name='altatec.mrp.resource.instance.line'

    def onchange_resource(self, cr, uid, ids, resource, context={}):
        res = {'value':{}}
        if resource:
            resource = self.pool.get("altatec.mrp.resource").browse(cr, uid, resource, context=context)
            if not resource.unit:
                res['warning'] = {'title':'Error!', 'message':"La unidad de medida no esta configurada para recurso" + str(resource.name)}
                res['value']['unit'] = False
                res['value']['resource'] = False
            else:
                res['value']['unit'] = resource.unit.id
        return res

    def create(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        res =super(altatec_mrp_resource_instance_line, self).create(cr, uid, ids, context=context)
        resources = self.pool.get("altatec.mrp.resource.instance.line").browse(cr, uid, res, context=context)

        costo=resources.resource.cost_displayed
        resources.write({'costo_trabajo':costo})

        return res



    _columns={
        'name'                        :fields.char('Descripcion',required=True),
        'resource'                    :fields.many2one('altatec.mrp.resource','Recurso',required=True),
        'unit'                        :fields.related('resource', 'unit',type='many2one',relation='product.uom',string='Unidad'),
        'mrp_production_workcenter_id':fields.many2one('mrp.production.workcenter.line','MPW'),
        #'quantity'                    : fields.integer('Horas Planificadas',required=True),
        'horas_realizadas'            :fields.related('mrp_production_workcenter_id','delay',type='float',string='Horas Realizadas', readonly=True),
        'date'                        : fields.date('Fecha de registro',required=True),
        'costo_trabajo'               : fields.float("Costo del trabajo"),
    }

    def _default_date(self, cr, uid, context=None):
        now_date = fields.date.context_today(self, cr, uid, context=context)
        return now_date


    _defaults = {
       'date': _default_date,
    }












