############################################################################################
#
############################################################################################
from openerp.osv import fields, osv, orm

###################################################################################################
# Inherited stock.move class definition
###################################################################################################
class stock_move(osv.osv):

    _inherit = "stock.move"

    def show_picking(self, cr, uid, ids, context={}):
        for move in self.browse(cr, uid, ids, context=context):
            return { 'type'      : 'ir.actions.act_window',
                     'name'      : 'Transferencias de Bodegas',
                     'view_type' : 'form',
                     'view_mode' : 'form,tree',
                     'res_model' : 'stock.picking',
                     'res_id'    : move.picking_id.id,
                   }

    _columns = { 'production_id_procurement' : fields.many2one("mrp.production", "Production ID"),
                 'production_id_return'      : fields.many2one("mrp.production", "Production ID Return"),

               }

###################################################################################################
# Inherited stock.move.consume class definition
###################################################################################################
class stock_move_consume(osv.osv_memory):

    _inherit = "stock.move.consume"

    def do_move_consume(self, cr, uid, ids, context=None):

        if not context:
            context = {}

        if 'active_model' not in context or context['active_model'] != 'stock.move' or 'active_id' not in context:
            raise osv.except_osv("Error", "Active Model Error, Contacte a AltaTec")

        for wizard in self.browse(cr, uid, ids, context=context):

            move_obj       = self.pool.get('stock.move')
            picking_obj    = self.pool.get('stock.picking')
            production_obj = self.pool.get('mrp.production')

            # Get the production associated with this move we're about to consume
            stock_move = move_obj.browse(cr, uid, context['active_id'], context=context)
            picking    = picking_obj.browse(cr, uid, stock_move.picking_id.id, context=context)
            production_ids = production_obj.search(cr, uid, [('consume_picking', '=', picking.id)], context=context)

            # Make sure the user isn't trying to consume more than what's on the production order
            if wizard.product_qty - stock_move.product_qty > 0.001:
                raise osv.except_osv("Error", "No se puede consumir una cantidad mayor que la cantidad en el orden de produccion")

            if not len(production_ids) == 1:
                raise osv.except_osv("Error", "Couldn't find production order. Contacte a AltaTec")

            production = production_obj.browse(cr, uid, production_ids[0], context=context)

            # Figure out if enough product has been sent from the source warehouses to facilitate this consumption
            product = wizard.product_id
            qty_to_consume = wizard.product_qty
            qty_received_from_warehouses = 0.0

            for move in production.procurement_moves:
                if move.state == 'done' and move.product_id.id == product.id:
                    qty_received_from_warehouses += move.product_qty

            qty_already_consumed = 0.0

            for move in production.move_lines2:
                if move.product_id.id == product.id:
                    qty_already_consumed += move.product_qty

            qty_available_to_consume = qty_received_from_warehouses - qty_already_consumed

            if qty_to_consume - qty_available_to_consume > 0.001:
                raise osv.except_osv("Error", "No se puede consumir una cantidad de este producto hasta que el movimiento stock desde"
                                              + " la bodega de materia prima con este producto se ha realizado.")

        return super(stock_move_consume, self).do_move_consume(cr, uid, ids, context=context)