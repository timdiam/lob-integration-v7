############################################################################################
#
############################################################################################
from openerp.osv import fields, osv, orm

############################################################################################
# Inherited stock.picking class definition
############################################################################################
class stock_picking(osv.osv):

    _inherit = "stock.picking"

    _columns = { 'production_id'        : fields.many2one( "mrp.production", "Production ID" ),
                 'from_production'      : fields.boolean("Is from a manufacturing order?", required=True),
               }

    _defaults = { 'from_production' : False,
                }