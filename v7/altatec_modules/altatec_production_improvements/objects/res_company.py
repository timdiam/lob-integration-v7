############################################################################################
#
############################################################################################
from openerp.osv import fields, osv, orm

############################################################################################
# Inherited mrp.production class definition
############################################################################################
class res_company(osv.osv):

    _inherit = "res.company"

    _columns = { 'default_production_src_location'  : fields.many2one("stock.location", "Ubicacion materias primas"),
                 'default_production_prod_location' : fields.many2one("stock.location", "Ubicacion de produccion"),
                 'default_production_dest_location' : fields.many2one("stock.location", "Ubicacion productos finalizados"),
               }