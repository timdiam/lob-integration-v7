import locale
from openerp.osv import fields, osv, orm
import logging
from openerp import netsvc

_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')

class sale_order(osv.osv):

    _inherit = 'sale.order'

    #########################################################################################################
    # Returns true if the system is configured to create just ONE manufacturing order from a sale order
    # or false if we want to create a separate manufacturing order for each product on the sale
    #########################################################################################################
    def should_combine_manufacturing_orders(self, cr, uid, ids, context={}):
        return True

    #########################################################################################################
    # We overwrite this method to create the procurement/sale_order based on if the system is configured
    # to combine production orders from a sale order into a single production order or not. If it IS configured
    # that way, we use the "Subproducts" feature of Odoo to create extra output products. If not, we just
    # call super.
    #########################################################################################################
    def _create_pickings_and_procurements(self, cr, uid, order, order_lines, picking_id=False, context=None):

        if not self.should_combine_manufacturing_orders(cr, uid, order.id, context={}):
            return super(sale_order, self)._create_pickings_and_procurements(cr, uid, order, order_lines, picking_id, context=context)

        else:
            move_obj        = self.pool.get('stock.move')
            picking_obj     = self.pool.get('stock.picking')
            procurement_obj = self.pool.get('procurement.order')
            production_obj  = self.pool.get('mrp.production')
            proc_ids = []
            production_procurement_id = False
            subproduct_order_lines = []

            for line in order_lines:

                if line.state == 'done':
                    continue

                date_planned = self._get_date_planned(cr, uid, order, line, order.date_order, context=context)

                if line.product_id:
                    if line.product_id.type in ('product', 'consu'):
                        if not picking_id:
                            picking_id = picking_obj.create(cr, uid, self._prepare_order_picking(cr, uid, order, context=context))
                        move_id = move_obj.create(cr, uid, self._prepare_order_line_move(cr, uid, order, line, picking_id, date_planned, context=context))
                    else:
                        # a service has no stock move
                        move_id = False

                    if line.type == 'make_to_order':
                        if not production_procurement_id:
                            proc_id = procurement_obj.create(cr, uid, self._prepare_order_line_procurement(cr, uid, order, line, move_id, date_planned, context=context))
                            production_procurement_id = proc_id
                            proc_ids.append(proc_id)
                            line.write({'procurement_id': proc_id})
                            self.ship_recreate(cr, uid, order, line, move_id, proc_id)
                        else:
                            subproduct_order_lines.append(line)
                    else:
                        proc_id = procurement_obj.create(cr, uid, self._prepare_order_line_procurement(cr, uid, order, line, move_id, date_planned, context=context))
                        proc_ids.append(proc_id)
                        line.write({'procurement_id': proc_id})
                        self.ship_recreate(cr, uid, order, line, move_id, proc_id)

            wf_service = netsvc.LocalService("workflow")
            if picking_id:
                wf_service.trg_validate(uid, 'stock.picking', picking_id, 'button_confirm', cr)
            for proc_id in proc_ids:
                wf_service.trg_validate(uid, 'procurement.order', proc_id, 'button_confirm', cr)

            procurement = self.pool.get('procurement.order').browse(cr, uid, proc_id, context=context)
            products = []
            for product in subproduct_order_lines:
                products.append((0, 0, { 'product_id'  : product.product_id.id,
                                         'product_qty' : product.product_uom_qty,
                                         'uom_id'      : product.product_uom.id,
                                       }))
            production_obj.write(cr, uid, procurement.production_id.id, {'product_ids': products}, context=context)

            val = {}
            if order.state == 'shipping_except':
                val['state'] = 'progress'
                val['shipped'] = False

                if (order.order_policy == 'manual'):
                    for line in order.order_line:
                        if (not line.invoiced) and (line.state not in ('cancel', 'draft')):
                            val['state'] = 'manual'
                            break
            order.write(val)
            return True


    def _prepare_order_line_move(self, cr, uid, order, line, picking_id, date_planned, context=None):
        res=super(sale_order, self)._prepare_order_line_move(cr, uid, order,line,picking_id,date_planned, context=context)
        res.update({'project_id':line.project_id.id})
        return res

    def _prepare_order_line_procurement(self, cr, uid, order, line, move_id, date_planned, context=None):
        res=super(sale_order, self)._prepare_order_line_procurement(cr, uid, order,line,move_id,date_planned, context=context)
        res.update({ 'project_id'      : line.project_id.id,
                     'from_sale_order' : True,
                   })
        return res

    def action_button_confirm(self, cr, uid, ids, context=None):

        order = self.pool.get('sale.order').browse(cr,uid,ids,context)[0]

        for line in order.order_line:

            context.update({'stock_move_id':line.project_id.id})

            tipo_abastecimiento = line.product_id.altatec_product_type.tipo_abastecimiento
            tipo_movimiento     = line.product_id.altatec_product_type.tipo_movimiento

            if tipo_abastecimiento == 'make_to_order' and tipo_movimiento == 'produce':

                product_id=line.product_id.id
                lista_materiales_id= self.pool.get('mrp.bom').search(cr,uid,[('product_id','=',product_id)])

                if not len(lista_materiales_id):
                    default_route = self.pool.get('mrp.routing').search(cr,uid,[],context=context)[0]
                    self.pool.get('mrp.bom').create(cr, uid, { 'product_id'  : line.product_id.id,
                                                               'cantidad'    : 1,
                                                               'product_uom' : line.product_uom.id ,
                                                               'type'        : 'normal',
                                                               'routing_id'  : default_route,
                                                               'name'        : "GENERADO AUTOMATICAMENTE",
                                                             }, context=context)

        return super(sale_order,self).action_button_confirm(cr, uid, ids,context=context)

