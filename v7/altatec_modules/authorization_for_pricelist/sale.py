# -*- coding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#    Copyright (C) 2011-2013 Serpent Consulting Services Pvt. Ltd. (<http://www.serpentcs.com>).

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
##############################################################################

from openerp.osv import osv, fields
from openerp.tools.translate import _

class sale_order(osv.Model):
    
    _inherit = 'sale.order'
    
    _columns = {
        'pricelist_approver_id' : fields.many2one('res.users', 'Pricelist Approver')
    }
    
    def copy(self, cr, uid, id, default=None, context=None):
        default = default or {}
        default.update({'pricelist_approver_id' : False})
        return super(sale_order, self).copy(cr, uid, id, default, context)
    
    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        res = super(sale_order, self).onchange_partner_id(cr, uid, ids, part, context=context)
        res['value'].update({'pricelist_approver_id' : False})
        return res
    
    def onchange_pricelist_id(self, cr, uid, ids, pricelist_id, order_lines, context=None):
        res = super(sale_order, self).onchange_pricelist_id(cr, uid, ids, pricelist_id, order_lines, context=context)
        #self.action_confirm(cr, uid, ids, context)
        res['value'].update({'pricelist_approver_id' : False})
        return res
    
    def action_wait(self, cr, uid, ids, context=None):
  
        context = context or {}
        for o in self.browse(cr, uid, ids):
            #DR correccion de tipo de lista
            if o.pricelist_id.requires_aaproval:
                if not o.pricelist_approver_id:
                    raise osv.except_osv(_('Error!'),_('You cannot confirm a sales order which has no user aprove.'))
            if not o.order_line:
                raise osv.except_osv(_('Error!'),_('You cannot confirm a sales order which has no line.'))
            noprod = self.test_no_product(cr, uid, o, context)
            if (o.order_policy == 'manual') or noprod:
                self.write(cr, uid, [o.id], {'state': 'manual', 'date_confirm': fields.date.context_today(self, cr, uid, context=context)})
            else:
                self.write(cr, uid, [o.id], {'state': 'progress', 'date_confirm': fields.date.context_today(self, cr, uid, context=context)})
            self.pool.get('sale.order.line').button_confirm(cr, uid, [x.id for x in o.order_line])
        return True
    
    def action_confirm(self, cr, uid, ids, context=None):
        
        """ This method pop up authentication wizard based if Requires Approval
        field which set true in Pricelist.
        
        @param self: Point current object
        @param cr: A database cursor
        @param uid: ID of the user currently logged in
        @param ids: list of ids for which name should be read
        @param context: context arguments, like lang, time zone
        
        Returns authentication pop up.
        """
        if context is None:
            context = {}
        sale_obj = self.pool.get('sale.order')           
        sale_data = sale_obj.browse(cr, uid,context.get('active_id'))
        sale_data = self.browse(cr, uid, ids[0], context=context)
        # Check that Requires Approval field set true in pricelist or not.
        if sale_data.pricelist_id.requires_aaproval:
            sale_obj.write(cr, uid, ids[0], {'pricelist_approver_id' : ''})
            if sale_data.pricelist_id.manager_readonly == False:
                view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'authorization_for_pricelist', 'authentication_form_view')
                result = {
                    'name': _('Authentication Check'),
                    'view_type': 'form',
                    'view_mode': 'form',
                    'view_id': view_id and view_id[1] or False,
                    'res_model': 'authentication.check',
                    'type': 'ir.actions.act_window',
                    'nodestroy': True,
                    'target': 'new',
                }
                return result
        else:
            sale_obj.write(cr, uid, ids[0], {'pricelist_approver_id' : uid})
                #return self.action_button_confirm(cr, uid, ids, context)
