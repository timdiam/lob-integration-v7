# -*- coding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#    Copyright (C) 2011-2013 Serpent Consulting Services Pvt. Ltd. (<http://www.serpentcs.com>).

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _

class authentication_check(osv.osv_memory):
    
    _name = 'authentication.check'
    
    _columns = {
        'login' : fields.char('Username', size=32),
        'password' : fields.char('Password', size=32)
    }
    
    def authentication_check(self, cr, uid, ids, context=None):
        """ This method check the authentication of Sale Manager, if return true
        then set Pricelist Approval otherwise do the standard Sale Order process.
        
        @param self: Point current object
        @param cr: A database cursor
        @param uid: ID of the user currently logged in
        @param ids: list of ids for which name should be read
        @param context: context arguments, like lang, time zone
        
        Returns True.
        """
        if context is None:
            context =  {}
        sale_obj = self.pool.get('sale.order')
        user_obj = self.pool.get('res.users')
        for wiz_data in self.browse(cr, uid, ids, context=context):
            sales_manager_group = self.pool.get('ir.model.data').get_object(cr, uid, 'base', 'group_sale_manager')
            # Check that current Login user are Sale Manager or not.
            sales_manager_users = [user.id for user in sales_manager_group.users]
            if sales_manager_users:
                user_ids = user_obj.search(cr, uid, [('login', '=', wiz_data.login)], context=context)
                sale_obj.write(cr, uid, ids[0], {'pricelist_approver_id' : ''})
                if user_ids:
                    for user in user_ids:
                        if user in sales_manager_users:
                            # Check the User credential.
                            try:
                                user_obj.check_credentials(cr, user, wiz_data.password)
                            except:
                                raise osv.except_osv(_('Validation Error!'), _('Invalid Username or Password.'))
                            # Set the Pricelist Approver
                            sale_obj.write(cr, uid, context.get('active_ids'), {'pricelist_approver_id' : user}, context=context)
                            #sale_obj.action_button_confirm(cr, uid, context.get('active_ids'), context=context)
                        else:
                            raise osv.except_osv(_('Error!'), _('User does not belogs to Pricelist Approval group!'))
                else:
                    raise osv.except_osv(_('Error!'), _('User does not exist!'))
        return True