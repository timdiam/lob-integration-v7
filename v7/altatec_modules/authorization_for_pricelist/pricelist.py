# -*- coding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#    Copyright (C) 2011-2013 Serpent Consulting Services Pvt. Ltd. (<http://www.serpentcs.com>).

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
##############################################################################

from openerp.osv import osv, fields

class product_pricelist(osv.Model):
    
    _inherit = 'product.pricelist'
    
    def _manager_readonly(self, cr, uid, ids, name, args, context=None):
        res = {}
        justread = True
        admin_group = self.pool.get('ir.model.data').get_object(cr, uid, 'base', 'group_sale_manager')
        admin_users = [user.id for user in admin_group.users]
        if uid in admin_users:
            justread = False
            res[ids[0]] = justread
            return res
        return res
        
    def _get_manager_readonly(self, cr, uid, context=None):
        res = True
        return res
    
    _columns = {
        'requires_aaproval' : fields.boolean('Requires Approval'),
        'manager_readonly' : fields.function(_manager_readonly, string='Manager Readonly', type='boolean'),
    }