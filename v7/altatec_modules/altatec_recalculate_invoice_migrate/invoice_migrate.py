from openerp import tools, pooler
from openerp.osv import fields,osv

class invoice_migrate(osv.osv):
    _name = "invoice.migrate"
    _auto = False

    def init(self, cr):
        pool = pooler.get_pool(cr.dbname)
        uid = 1

        account_invoice_ids = pool.get('account.invoice').search(cr,uid,[('type','=','in_invoice')])
        account_tax_lookup_obj = pool.get('account.invoice').browse(cr,uid,account_invoice_ids)

        tax_line_list=[]

        res_dict = {}

        for invoice in account_tax_lookup_obj:
            if invoice.tax_line and (len(invoice.tax_line) > 0):
                res_dict[invoice.id] = {'old_residual':invoice.residual,
                                        'old_total'   :invoice.amount_total,
                                        'old_total_to_withhold' : invoice.total_to_withhold,
                                        }
                for tax in invoice.tax_line:
                    tax_line_list.append(tax.id)

        pool.get('account.invoice.tax').write(cr,uid,tax_line_list,{})

        for invoice in pool.get('account.invoice').browse(cr,uid,account_invoice_ids):
            if invoice.id in res_dict:
                print "Total:", invoice.amount_total, "Old Total:", res_dict[invoice.id]['old_total']
                print "Saldo:", invoice.residual, "Old Residual:", res_dict[invoice.id]['old_residual']
                print "Withhold:", invoice.total_to_withhold, "Old Withhold:", res_dict[invoice.id]['old_total_to_withhold']
                print ""