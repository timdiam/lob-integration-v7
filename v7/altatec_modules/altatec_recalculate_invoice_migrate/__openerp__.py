{ 'name'         : 'AltaTec Recalculate "Saldo Pendiente" field',
  'version'      : '1.0',
  'description'  : """
                   This module is used to recalculate the invoice 'residual' field by forcing a write to the move lines.
                   """,
  'author'       : 'Tim Diamond',
  'website'      : 'www.altatec.ec',
  "depends"      : [ 'base','account','ecua_invoice',
                   ],
  "data"         : [
                   ],
  "installable"  : True,
  "auto_install" : False
}
