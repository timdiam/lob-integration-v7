from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
import logging
import traceback
import re
_logger = logging.getLogger(__name__)





class account_invoice(osv.osv):
    
    _inherit = "account.invoice"

    def invoice_print(self, cr, uid, ids, context=None):
        '''
        This function prints the invoice and mark it as sent, so that we can see more easily the next step of the workflow
        '''
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        self.write(cr, uid, ids, {'sent': True}, context=context)
        datas = {
             'ids': ids,
             'model': 'account.invoice',
             'form': self.read(cr, uid, ids[0], context=context)
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'fact_elmilon_report',
            'datas': datas,
            'nodestroy' : True
        }
   

    def cmpt_disc(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
	prov_totals = 0

        for r in records:

		for entry in r.invoice_line:
			prov_totals = prov_totals+(entry.price_unit * entry.quantity)
		result[r.id] = prov_totals

       	return result


    def cmpt_disc2(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
        for r in records:

		prov_totals = 0
		ss_tots=0
		for entry in r.invoice_line:
			ss_tots = entry.price_subtotal + ss_tots
			prov_totals = prov_totals + (entry.price_unit * entry.quantity)
		result[r.id] = prov_totals-ss_tots

       	return result
 
    def cmpt_disc_str(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
		result[r.id] = str(r.invoice_line[0].discount) + " %"

       	return result
 
    def cmpt_after(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
		result[r.id] = r.total_before_disc - r.discu

       	return result

    def get_trans(self,cr,uid,ids,field_name,field_value,arg, context={}):
	
	records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
		so = self.pool.get('sale.order').search(cr,uid,[('name','=',r.origin)])
		if(len(so) == 1):
			result[r.id] = self.pool.get('sale.order').browse(cr,uid,so)[0].transporte
		else:
			result[r.id] = ""

       	return result
	
    
    def cmpt_sign(self,cr,uid,ids,field_name,field_value,arg, context={}):

	records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
		if r.type == "out_invoice":
			result[r.id] = r.amount_total
		elif r.type == "out_refund":
			result[r.id] = r.amount_total * -1
		else:
			result[r.id] = r.amount_total
       	return result


    def cmpt_sign_sub(self,cr,uid,ids,field_name,field_value,arg, context={}):

	records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
		if r.type == "out_invoice":
			result[r.id] = r.amount_untaxed
		elif r.type == "out_refund":
			result[r.id] = r.amount_untaxed * -1
		else:
			result[r.id] = r.amount_untaxed
       	return result


    def cmpt_sign_bal(self,cr,uid,ids,field_name,field_value,arg, context={}):

	records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
		if r.type == "out_invoice":
			result[r.id] = r.residual
		elif r.type == "out_refund":
			result[r.id] = r.residual * -1
		else:
			result[r.id] = r.residual
       	return result

    def cmpt_name_r(self,cr,uid,ids,field_name,field_value,arg,context={}):

	records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
                if r.type == "out_invoice":
                        result[r.id] = 'ORDEN DE PEDIDO'
                elif r.type == "out_refund":
                        result[r.id] = 'NOTA DE CREDITO'
                else:
                        result[r.id] = 'ORDEN DE PEDIDO'
        return result

    _columns = {'report_name':fields.function(cmpt_name_r,type='char',method=True,string="rname"),    
		'old_num':fields.char('Numero Antiguo'),
		'city_r':fields.related('partner_id','city',type='char',string="Ciudad"),
		'country_r':fields.related('partner_id','country_id',type='many2one',relation='res.country',string='Pais'),
		'state_r':fields.related('partner_id','state_id',type='many2one',relation='res.country.state',string='Estado de Pais'),
		'ruc':fields.related("partner_id","ref",type='char', string="RUC"),
		'subtotal_sign':fields.function(cmpt_sign_sub,type='float',method=True,string="Total Sin IVA"),
		'balance_sign':fields.function(cmpt_sign_bal,type='float',method=True,string="Sueldo (+/-)"),
		'total_sign':fields.function(cmpt_sign,type='float',method=True,string="Total Con IVA"),
		'str_disc':fields.function(cmpt_disc_str,type='char',method=True,string="Descuento"),
		'total_after_disc':fields.function(cmpt_after,type='float',method=True,string="Antes IVA"),	
		'total_before_disc':fields.function(cmpt_disc, type='float', method=True, string='Subtotal'),	
		'discu':fields.function(cmpt_disc2, type='float', method=True, string='Descuento ($)'),
                'client_order_ref': fields.char('C.I./RUC', size=64, readonly=True),
                'discount': fields.float('Descuento (%)', digits_compute=dp.get_precision('Product Price')),
                 'transporte' : fields.function( get_trans, type='char', method=True, string = "Transporte"),       
		'extra':fields.char('Detalles Misc.'),	
		 'vat': fields.many2many('account.tax', 'sale_order_vat_rel', 'tax_id', 'order_id', 'I.V.A', readonly=True),
		'codigo':fields.related('partner_id','codigo',type='char',string="Cod."),
        	'type': fields.selection([
            ('out_invoice','Customer Invoice'),
            ('in_invoice','Supplier Invoice'),
            ('out_refund','Customer Refund'),
            ('in_refund','Supplier Refund'),
            ],'Type', select=True, change_default=True, track_visibility='always'),
                 'container' : fields.many2one( 'purchase.container', "Contenedor" ),
                 'category'  : fields.many2one( 'purchase.container.category', "Tipo gasto por defecto" ),
                }


class account_invoice_line(osv.osv):

    _inherit = 'account.invoice.line'

    def change_container(self, cr, uid, ids, context={}):

        for line in self.browse(cr, uid, ids, context=context):
            wizard_id = self.pool.get('el.millon.edit.invoice.line.wizard').create(cr, uid, { 'mode'         : 'container',
                                                                                              'container_id' : line.container.id,
                                                                                              'category_id'  : line.category.id,
                                                                                              'line_id'      : line.id,
                                                                                            }, context=context)
            return { 'type'      : 'ir.actions.act_window',
                     'name'      : 'Editar Contenedor',
                     'view_type' : 'form',
                     'view_mode' : 'form,tree',
                     'res_model' : 'el.millon.edit.invoice.line.wizard',
                     'res_id'    : wizard_id,
                     'target'    : 'new',
                   }

    def change_category(self, cr, uid, ids, context={}):

        for line in self.browse(cr, uid, ids, context=context):
            wizard_id = self.pool.get('el.millon.edit.invoice.line.wizard').create(cr, uid, { 'mode'         : 'category',
                                                                                              'container_id' : line.container.id,
                                                                                              'category_id'  : line.category.id,
                                                                                              'line_id'      : line.id,
                                                                                            }, context=context)
            return { 'type'      : 'ir.actions.act_window',
                     'name'      : 'Editar Categoria',
                     'view_type' : 'form',
                     'view_mode' : 'form,tree',
                     'res_model' : 'el.millon.edit.invoice.line.wizard',
                     'res_id'    : wizard_id,
                     'target'    : 'new',
                   }

    def cpy_over(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
	prov_totals = 0

        for r in records:
		result[r.id] = r.price_unit*r.quantity

       	return result


    def cpy_for_str(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
	prov_totals = 0

        for r in records:
		result[r.id] = '$'+format(r.price_unit,',.2f')

       	return result


    def cpy_for_str2(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
	prov_totals = 0

        for r in records:
		result[r.id] = '$'+format(r.price_subtotal,',.2f')

       	return result

    def get_default_container(self,cr,uid,context=None):
        return context.get( 'container', False )

    def get_default_category(self,cr,uid,context=None):
        return context.get( 'category', False )

    _columns = {
		'prod_ref':fields.related('product_id','sup_ref',type='char',readonly=True, string='Ref.'),
		'old_price':fields.function(cpy_over, type='float', method=True, string='Subtotal'),
		'the_date':fields.date("Fecha"),
		'str_price':fields.function(cpy_for_str,type='char',method=True, string="Subtotal"),
		'price_subtotal_str':fields.function(cpy_for_str2,type='char',method=True, string="Subtotal"),
        'container' : fields.many2one( 'purchase.container', "Contenedor" ),
        'category'  : fields.many2one( 'purchase.container.category', "Tipo gasto importacion" ),
               }

    _defaults = { 'container' : get_default_container,
                  'category'  : get_default_category,
                }

class millon_pago_report_wizard (osv.osv_memory):

	_name='millon.pago.report.wizard'

	_columns = {
			'fecha':fields.date(string='Fecha')
		}


	def generate_pago_report(self, cr, uid, ids, context=None):
		records=self.browse(cr,uid,ids)[0]	
       		pago_obj=self.pool.get('account.voucher')	
		pago= pago_obj.search(cr,uid,[('date','=',records.fecha),('state','=','posted')],) #hacemos una lista de los id con esa fecha 	

		result = {'type' :'ir.actions.report.xml',
		'context':context,
		'report_name':'reporte_pagos',
		'datas':
		{'ids':pago}
		}
        	
		return result
                
class update_invoice_number_wizard(osv.osv_memory):

        _name='update.invoice.number.wizard'

        
        def submit_next_number(self, cr, uid, ids, context=None):
                obj = self.browse(cr,uid,ids,context)[0]
                obj.journal_id.sequence_id.write({'number_next':obj.next})
                
                
                
        def get_def_journal_id(self,cr,uid,context=None):
                if 'factura' in context:
                        journal_id = self.pool.get('account.journal').search(cr,uid,[('type','=','sale')])
                        if journal_id:
                                return journal_id[0]
                elif 'nota' in context:
                        journal_id = self.pool.get('account.journal').search(cr,uid,[('type','=','sale_refund')])
                        if journal_id:
                                return journal_id[0]
                else:
                        return
                
        def get_def_journal_id(self,cr,uid,context=None):
                if 'factura' in context:
                        journal_id = self.pool.get('account.journal').search(cr,uid,[('type','=','sale')])
                        if journal_id:
                                return journal_id[0]
                elif 'nota' in context:
                        journal_id = self.pool.get('account.journal').search(cr,uid,[('type','=','sale_refund')])
                        if journal_id:
                                return journal_id[0]
                elif 'sucursal' in context:
                        journal_id = self.pool.get('account.journal').search(cr,uid,[('code','=','SAJS')])
                        if journal_id:
                                return journal_id[0]
                else:
                        return None
                
        def on_change_journal_id(self,cr,uid,ids,journal_id,context=None):
                if journal_id:
                        res = {}
                        res['value'] = {'next':self.pool.get('account.journal').browse(cr,uid,journal_id,context).sequence_id.number_next}
                        return res
                

        _columns = {
                        'journal_id':fields.many2one('account.journal',string='Journal ID'),
                        'next':fields.integer(string='Numero Siguiente'),
                }
        
        _defaults = {
                        'journal_id':get_def_journal_id,
                     }

