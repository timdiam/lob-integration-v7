# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP SA (<http://openerp.com>).
#    Copyright (C) 2013-TODAY Synconics (<http://synconics.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp import netsvc
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP, float_compare
# from datetime import datetime
import logging
import time

_logger = logging.getLogger(__name__)



class stock_invoice_onshipping(osv.osv_memory):

    _inherit = "stock.invoice.onshipping"

    def open_invoice(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        #context.update({'force_printer_id':1})
        invoice_ids = []
        data_pool = self.pool.get('ir.model.data')
        res = self.create_invoice(cr, uid, ids, context=context)

        invoice_ids += res.values()
        inv_obj = self.pool.get('account.invoice')
        invoice=self.pool.get('account.invoice').browse(cr,uid,invoice_ids)[0]

        date_invoice_o=context.get('date_invoice',False)
        # Values to be set after the invoice is created, this is for SRI authorizations
        printer_id = inv_obj._default_printer_point(cr, uid, context=context)
        printer_obj = self.pool.get('sri.printer.point').browse(cr,uid,printer_id,context=context)
        editable_num = True
        if printer_obj.refund_sequence_id:
            editable_num = False

        prefix = printer_obj.prefix

        is_electronic_document = False
        if printer_obj.allow_electronic_credit_note:
            is_electronic_document = True


        #Set documento modified
        modified_invoice_id = False
        if context['active_id']:
            picking_id = self.pool.get('stock.picking').search(cr, uid, [ ( 'id','=',context['active_id'] ) ], context=context)
            if picking_id and (len(picking_id)==1):
                picking_obj = self.pool.get('stock.picking').browse(cr,uid,picking_id[0],context=context)
                sale_id = self.pool.get('sale.order').search(cr,uid, [ ('name','=',picking_obj.origin) ],context=context)
                if sale_id and (len(sale_id)==1):
                    sale_obj = self.pool.get('sale.order').browse(cr,uid,sale_id[0],context=context)
                    if sale_obj.first_inv:
                        modified_invoice_num = sale_obj.first_inv
                        invoice_id = self.pool.get('account.invoice').search(cr, uid, [('internal_number','=',modified_invoice_num),('state','not in',['draft','cancel'])])
                        if invoice_id and (len(invoice_id)==1):
                            modified_invoice_id = invoice_id[0]
        
        authorization_id = False
        internal_number = False

        header = inv_obj._prepare_invoice_header(cr, uid, invoice.partner_id.id, 'out_refund', inv_date=time.strftime('%Y-%m-%d'), printer_id=printer_id, context=context)
        if modified_invoice_id:
            header.update({
                            'invoice_rectification_id':modified_invoice_id,
                            })
        header.update({
                       'document_invoice_type_id':8,
                        'date_invoice':time.strftime('%Y-%m-%d'),
                        'editable_internal_number':editable_num,
                       'allow_electronic_document':is_electronic_document,
                        'internal_number':prefix,
            })

        self.pool.get('account.invoice').write(cr,uid,invoice_ids,header)

        inv_type = context.get('inv_type', False)
        action_model = False
        action = {}
        if not invoice_ids:
            raise osv.except_osv(_('Error!'), _('Please create Invoices.'))
        if inv_type == "out_invoice":
            action_model,action_id = data_pool.get_object_reference(cr, uid, 'account', "action_invoice_tree1")
        elif inv_type == "in_invoice":
            action_model,action_id = data_pool.get_object_reference(cr, uid, 'account', "action_invoice_tree2")
        elif inv_type == "out_refund":
            action_model,action_id = data_pool.get_object_reference(cr, uid, 'account', "action_invoice_tree3")
        elif inv_type == "in_refund":
            action_model,action_id = data_pool.get_object_reference(cr, uid, 'account', "action_invoice_tree4")
        if action_model:
            action_pool = self.pool.get(action_model)
            action = action_pool.read(cr, uid, action_id, context=context)
            action['domain'] = "[('id','in', ["+','.join(map(str,invoice_ids))+"])]"
        return action

class stock_picking(osv.osv):
    
    _inherit = "stock.picking"
   

    # def get_order_details(self,cr,uid,ids,field_name,field_value,arg, context={}):
    #
     #    records = self.browse(cr, uid, ids)
     #    result = {}
     #    for r in records:
     #        so = self.pool.get('sale.order').search(cr,uid,[('name','=',r.origin)])
     #        if(len(so) == 1):
     #            result[r.id] = self.pool.get('sale.order').browse(cr,uid,so[0])
     #        else:
     #            result[r.id] = False
    #
     #    return result
    #
    #
    # def get_fact_char(self,cr,uid,ids,field_name,field_value,arg, context={}):
     #    records = self.browse(cr, uid, ids)
     #    result = {}
    #
     #    for r in records:
     #        so = self.pool.get('account.invoice').search(cr,uid,[('origin','=',r.origin),('state','!=','cancel')])
     #        if(len(so) >= 1):
     #            result[r.id] = self.pool.get('account.invoice').browse(cr,uid,so[0]).internal_number
     #        else:
     #            result[r.id] = ""
    #
     #    return result
    #
    # def get_factura_details(self,cr,uid,ids,field_name,field_value,arg, context={}):
     #    records = self.browse(cr, uid, ids)
     #    result = {}
     #    for r in records:
     #        so = self.pool.get('account.invoice').search(cr,uid,[('origin','=',r.origin)])
     #        if(len(so) == 1):
     #            result[r.id] = self.pool.get('account.invoice').browse(cr,uid,so[0])
     #        else:
     #            result[r.id] = False
     #    return result
    #
    #
    # def get_date(self,cr,uid,ids,field_name,field_value,arg,context={}):
    	# records=self.browse(cr,uid,ids)
	# result={}
	# for r in records:
	# 	id_docs=self.pool.get('account.invoice').search(cr,uid,[('internal_number','=',r.factura_char_hack)])
	# 	traer_docs=self.pool.get('account.invoice').browse(cr,uid,id_docs)
	# 	if len(id_docs) >= 1:
	# 		for d in traer_docs:
	# 			if(d.date_invoice):
	# 				a=datetime.strptime(d.date_invoice, "%Y-%m-%d")
	# 				b=a.strftime("%Y-%m-%d")
	# 				result[r.id]=b
	# 			else:
	# 				result[r.id] = False
	# 	else:
	# 		result[r.id] = False
    #
	# return result



    
    _columns = {
                'container_id': fields.many2one('purchase.container', 'Contenedor'),
                'returned':fields.boolean("Returned"),
		# 'order_origin_hack':fields.function(get_order_details, type='many2one', obj="sale.order", method=True, string='sale_order'),
         #        'factura_origin_hack':fields.function(get_factura_details, type='many2one',obj="account.invoice",method=True, string='factura'),
		# 'factura_char_hack':fields.function(get_fact_char,type='char',method=True,string="Factura"),
		# 'date_change':fields.function(get_date,type='datetime',method=True,string="Fecha Factura"),
		}
    
    def _check_metho(self, cr, uid, ids): 
    #TODO : check condition and return boolean accordingly
        return True
    
    _constraints = [(_check_metho, 'Error: Invalid Message', ['field_name']), ] 
    
stock_picking() 



# class stock_picking_out(osv.osv):
#
#     _inherit = "stock.picking.out"
#
#     def __init__(self, pool, cr):
#         super(stock_picking_out, self).__init__(pool, cr)
#         self._columns['factura_char_hack'] = self.pool['stock.picking']._columns['factura_char_hack']
#         self._columns['order_origin_hack'] = self.pool['stock.picking']._columns['order_origin_hack']
#         self._columns['date_change'] = self.pool['stock.picking']._columns['date_change']
#         self._columns['factura_origin_hack'] = self.pool['stock.picking']._columns['factura_origin_hack']





class stock_picking_in(osv.osv):
    
    _inherit = "stock.picking.in"
    
    _columns = {
                'container_id': fields.many2one('purchase.container', 'Pedido'),
                'returned':fields.boolean("Returned"),
                }
    _defaults = {"returned":False}
    
    def _check_metho(self, cr, uid, ids): 
    #TODO : check condition and return boolean accordingly
        return True
    
    _constraints = [(_check_metho, 'Error: Invalid Message', ['field_name']), ] 
    
stock_picking_in() 






class stock_move(osv.osv):
    _inherit = "stock.move"

    _order = 'date asc'
    
    def get_signed_qty(self,cr,uid,ids,field_name,field_value,arg, context={}):
	
	records = self.browse(cr, uid, ids)
        results = {}
        for r in records:
		if r.state == 'done':

			if r.type == 'out':
				results[r.id] = r.product_qty * -1
			elif r.type == 'in':
				results[r.id] = r.product_qty * 1
			elif r.type == 'internal':
				results[r.id] = 0
			elif (not r.type):
				if(r.location_dest_id.real == True):
					results[r.id] = r.product_qty * 1
				else:
					results[r.id] = r.product_qty * -1
		else:
			results[r.id] = 0


			
       	return results

    _columns = {
		'prod_avail' : fields.float("Available to Move"),
        'prod_ref': fields.related('product_id','sup_ref', type='char', string="Referencia"), 
        'container_id':fields.many2one('purchase.container', 'Contenedor'), 
	'factura_char':fields.related('picking_id','factura_char_hack',type='char',string="Factura"),
        'kardex_qty':fields.float("Kardex Cantidad"),
        'old_qty':fields.float("Candidad Antiguo"),
	'signed_qty':fields.function(get_signed_qty,method=True,string="+/- Inv",type='float'),
	}



    def onchange_product_id(self, cr, uid, ids, product_id, location_id, location_dest_id, part_id):
	first = super(stock_move,self).onchange_product_id(cr, uid, ids, product_id, location_id, location_dest_id, part_id)
	first['value']['prod_avail'] = self.get_quantity_at_location(cr,uid,location_id,product_id)
	return first

    def locs_changed(self, cr, uid, ids, location_id, location_dest_id, product_id, context=None):

	if not product_id:
		return {}

	return {'value':{'prod_avail':self.get_quantity_at_location(cr,uid,location_id,product_id)}}
			
    def get_quantity_at_location(self,cr,uid,lid,p):
	ls = ['stock_real','stock_virtual','stock_real_value','stock_virtual_value']
	move_avail = self.pool.get('stock.location')._product_value(cr,uid,[lid],ls,0,{'product_id':p})
	return move_avail[lid]['stock_real']

    def onchange_move_type(self, cr, uid, ids, type, context=None):
	locs_ids = self.pool.get('stock.location').search(cr,uid,[])
	_logger.debug("ttt")
	_logger.debug(locs_ids)
	locs = self.pool.get('stock.location').browse(cr,uid,locs_ids)
	for l in locs:
		_logger.debug("ttt")
		_logger.debug(l.real)
		if l.real == True:
			return {'value':{'location_id': l.id, 'location_dest_id': l.id}}
	return False

    def _default_location_source(self, cr, uid, context=None):
	locs_ids = self.pool.get('stock.location').search(cr,uid,[])

	locs = self.pool.get('stock.location').browse(cr,uid,locs_ids)
	for l in locs:
		if l.real == True:
			return l.id
	return False

    _defaults = {
        'location_id': _default_location_source,}


class stock_return_picking(osv.osv_memory):
    _inherit = 'stock.return.picking'

    def default_get(self, cr, uid, fields, context=None):
        res = super(stock_return_picking, self).default_get(cr, uid, fields, context=context)
        res.update({'invoice_state': '2binvoiced'})
        return res

class stock_location(osv.osv):
    
    _inherit = "stock.location"
    
    def _product_reserve(self, cr, uid, ids, product_id, product_qty, context=None, lock=False):
        """
        Attempt to find a quantity ``product_qty`` (in the product's default uom or the uom passed in ``context``) of product ``product_id``
        in locations with id ``ids`` and their child locations. If ``lock`` is True, the stock.move lines
        of product with id ``product_id`` in the searched location will be write-locked using Postgres's
        "FOR UPDATE NOWAIT" option until the transaction is committed or rolled back, to prevent reservin
        twice the same products.
        If ``lock`` is True and the lock cannot be obtained (because another transaction has locked some of
        the same stock.move lines), a log line will be output and False will be returned, as if there was
        not enough stock.

        :param product_id: Id of product to reserve
        :param product_qty: Quantity of product to reserve (in the product's default uom or the uom passed in ``context``)
        :param lock: if True, the stock.move lines of product with id ``product_id`` in all locations (and children locations) with ``ids`` will
                     be write-locked using postgres's "FOR UPDATE NOWAIT" option until the transaction is committed or rolled back. This is
                     to prevent reserving twice the same products.
        :param context: optional context dictionary: if a 'uom' key is present it will be used instead of the default product uom to
                        compute the ``product_qty`` and in the return value.
        :return: List of tuples in the form (qty, location_id) with the (partial) quantities that can be taken in each location to
                 reach the requested product_qty (``qty`` is expressed in the default uom of the product), of False if enough
                 products could not be found, or the lock could not be obtained (and ``lock`` was True).
        """
        result = []
        amount = 0.0
        if context is None:
            context = {}
        uom_obj = self.pool.get('product.uom')
        uom_rounding = self.pool.get('product.product').browse(cr, uid, product_id, context=context).uom_id.rounding
        if context.get('uom'):
            uom_rounding = uom_obj.browse(cr, uid, context.get('uom'), context=context).rounding

        locations_ids = self.search(cr, uid, [('location_id', 'child_of', ids)])
        if locations_ids:
            # Fetch only the locations in which this product has ever been processed (in or out)
            cr.execute("""SELECT l.id FROM stock_location l WHERE l.id in %s AND
                        EXISTS (SELECT 1 FROM stock_move m WHERE m.product_id = %s
                                AND ((state = 'done' AND m.location_dest_id = l.id)
                                    OR (state in ('done','assigned') AND m.location_id = l.id))) ORDER BY l.posx
                       """, (tuple(locations_ids), product_id,))
            locations_ids = [i for (i,) in cr.fetchall()]
        for id in locations_ids:
            if lock:
                try:
                    # Must lock with a separate select query because FOR UPDATE can't be used with
                    # aggregation/group by's (when individual rows aren't identifiable).
                    # We use a SAVEPOINT to be able to rollback this part of the transaction without
                    # failing the whole transaction in case the LOCK cannot be acquired.
                    cr.execute("SAVEPOINT stock_location_product_reserve")
                    cr.execute("""SELECT id FROM stock_move
                                  WHERE product_id=%s AND
                                          (
                                            (location_dest_id=%s AND
                                             location_id<>%s AND
                                             state='done')
                                            OR
                                            (location_id=%s AND
                                             location_dest_id<>%s AND
                                             state in ('done', 'assigned'))
                                          )
                                  FOR UPDATE of stock_move NOWAIT""", (product_id, id, id, id, id), log_exceptions=False)
                except Exception:
                    # Here it's likely that the FOR UPDATE NOWAIT failed to get the LOCK,
                    # so we ROLLBACK to the SAVEPOINT to restore the transaction to its earlier
                    # state, we return False as if the products were not available, and log it:
                    cr.execute("ROLLBACK TO stock_location_product_reserve")
                    _logger.warning("Failed attempt to reserve %s x product %s, likely due to another transaction already in progress. Next attempt is likely to work. Detailed error available at DEBUG level.", product_qty, product_id)
                    _logger.debug("Trace of the failed product reservation attempt: ", exc_info=True)
                    return False

            # XXX TODO: rewrite this with one single query, possibly even the quantity conversion
            cr.execute("""SELECT product_uom, sum(product_qty) AS product_qty
                          FROM stock_move
                          WHERE location_dest_id=%s AND
                                location_id<>%s AND
                                product_id=%s AND
                                state='done'
                          GROUP BY product_uom
                       """,
                       (id, id, product_id))
            results = cr.dictfetchall()
            cr.execute("""SELECT product_uom,-sum(product_qty) AS product_qty
                          FROM stock_move
                          WHERE location_id=%s AND
                                location_dest_id<>%s AND
                                product_id=%s AND
                                state in ('done', 'assigned')
                          GROUP BY product_uom
                       """,
                       (id, id, product_id))
            results += cr.dictfetchall()
            total = 0.0
            results2 = 0.0
            for r in results:
                amount = uom_obj._compute_qty(cr, uid, r['product_uom'], r['product_qty'], context.get('uom', False))
                results2 += amount
                total += amount
            if total <= 0.0:
                continue

            amount = results2
            compare_qty = float_compare(amount, 0, precision_rounding=uom_rounding)
            if compare_qty == 1:
                if amount > min(total, product_qty):
                    amount = min(product_qty, total)
                result.append((amount, id))
                product_qty -= amount
                total -= amount
                if product_qty <= 0.0:
                    return result
                if total <= 0.0:
                    continue
        return False
    
    _columns = {
                'real':fields.boolean('Real')
                }

    _defaults = {'real':False}
    
class millon_bodega_report_wizard (osv.osv_memory):

	_name='millon.bodega.report.wizard'

	
	def generate_bodega_report(self, cr, uid, ids, context=None):
		records=self.browse(cr,uid,ids)[0]	
       		prod_obj=self.pool.get('product.product')
                
                if records.all_products != True:
                        context.update({'location':int(records.ubi.id)})
                        context.update({'location_name':records.ubi.name})
                else:
                        context.update({'location_name':"Todos Las Ubicaciones"})

		#context.update({'compute_child':False})
		prod= prod_obj.search(cr,uid,[]) #hacemos una lista de todos los ids


		ids_with_cant = []
		#move_obj = self.pool.get('stock.move')
		for p in prod_obj.browse(cr,uid,prod,context):
			if p.qty_available > 0:
				ids_with_cant.append(p.id)

                if(records.all_products):
                        result = {'type' :'ir.actions.report.xml',
                        'context':context,
                        'report_name':'cant_por_ubi_prod_spread',
                        'datas':
                        {'ids':ids_with_cant}
                        }
                        
                else:
        		result = {'type' :'ir.actions.report.xml',
        		'context':context,
        		'report_name':'cant_por_ubi_prod',
        		'datas':
        		{'ids':ids_with_cant}
        		}
        	
		return result

	_columns = {
			'ubi':fields.many2one('stock.location',domain=[('real','=',True)],string='Ubicacion'),
                        'all_products':fields.boolean(string="Todos los Productos"),
		}
