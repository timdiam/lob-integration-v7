# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
import pytz
from openerp import SUPERUSER_ID
from datetime import datetime
from dateutil.relativedelta import relativedelta

from openerp.osv import fields, osv
from openerp import netsvc
from openerp import pooler
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.osv.orm import browse_record, browse_null
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP

import logging

_logger = logging.getLogger(__name__)
class res_partner(osv.osv):
    
    _inherit = 'res.partner'
    


    def cmpt_balance(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
	bal_totals = 0

        for r in records:
		bal_totals=0
		invoices = self.pool.get('account.invoice').search(cr,uid,['&',('state','=','open'),('partner_id','=',r.id)])
		for invoice in self.pool.get('account.invoice').browse(cr,uid,invoices,context):
			if (invoice.type == 'out_invoice'):
				bal_totals += invoice.residual
			elif (invoice.type == 'out_refund'):
				bal_totals = bal_totals - invoice.residual

		result[r.id] = bal_totals

       	return result

    def get_def_tax(self,cr,uid,context={}):
        
	sales_taxes=self.pool.get('account.tax').search(cr,uid,[('type_tax_use','=','sale'),('name','=','IVA COBRADO(12%)')])
	_logger.debug("JJJ")
	return [(6,0,[sales_taxes[0]])]

    _columns = {
		'nombre_de_rep':fields.char("Representante"),
		'f_local':fields.char("Nombre de Local"),
		'milon_balance':fields.function(cmpt_balance, type='float', method=True, string='Estado de Cuenta'),
                'codigo':fields.char("Codigo de Cliente"),
		'discount': fields.float('Descuento%)', digits_compute=dp.get_precision('Product Price')),
                'vat_tax': fields.many2many('account.tax', 'partner_vat_rel', 'tax_id', 'partner_id', 'I.V.A'),
                'ref': fields.char('C.I./RUC', size=64, select=1),
                'user_id': fields.many2one('res.users', 'Represtator', help='The internal user that is in charge of communicating with this contact if any.'),
               'tipo_c' : fields.selection([('nat','Natural'),('c_esp','Contribuyente Especial')], 'Tipo de Contribuyentes'),
		 }

    _defaults = {  
        'discount': 0,
	'vat_tax' : get_def_tax  
        }
    def get_tax(self, cr,uid,id,context=None):
        the_tax = self.pool.get('res.partner').browse(cr,uid,id).vat_tax
        if len(the_tax) != 0:
            return the_tax[0].id
        else:
            return -11
		
		

res_partner()


class res_partner_bank(osv.osv):
    
    _inherit = 'res.partner.bank'
    


    _columns = {
                'state': fields.selection([('corriente','Cuenta Corriente'),('ahorros','Cuenta de Ahorros')], "Tipo de cuenta bancaria"),
		 }

		
		
