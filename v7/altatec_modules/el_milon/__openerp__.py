# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP SA (<http://openerp.com>).
#    Copyright (C) 2013-TODAY Synconics (<http://synconics.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name'         : 'El Millon',
    'version'      : '1.0',
    'description'  : """
                     Wholesale ERP Addons
                     """,
    'author'       : 'Tim Diamond',
    'website'      : 'www.altatec.ec',
    "depends"      : [ 'account_asset',
                       'altatec_import',
                       'sale_order_for_retail',
                       'authorization_for_pricelist',
                       'sale_pricelist_recalculation',
                       'sale_crm',
                       'sale',
                       'ecua_invoice_type',
                       'stock',
                       'sale_stock',
                       'purchase',
                       'email_template',
                       'account',
                       'altatec_waybill',
                       'web_hide_duplicate',
                     ],
    "data"         : [ 'data/categories.xml',
                       'security/ir.model.access.csv',
                       'product/product_view.xml',
                       'partner/partner_view.xml',
                       'purchase/purchase_view.xml',
		               'stock/stock_view.xml',
		               'sale/sale_view.xml',
		               'account/account_view.xml',
                       'wizard/purchase_container_wizard.xml',
                       'wizard/edit_invoice_line_wizard.xml',
                     ],
    'css'          : [ 'static/src/css/my_css.css'
                     ],
    "installable"  : True,
    "auto_install" : False
}
