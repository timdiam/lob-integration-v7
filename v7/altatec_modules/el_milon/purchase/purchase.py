# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP SA (<http://openerp.com>).
#    Copyright (C) 2013-TODAY Synconics (<http://synconics.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
from datetime import datetime
from openerp.osv import fields, osv
from openerp import netsvc
import openerp.addons.decimal_precision as dp
import logging
from openerp.tools.translate import _


_logger = logging.getLogger(__name__)

##########################################################################################
# container.tag class definition
##########################################################################################
class container_tag(osv.osv):
    
    _name        = 'container.tag'
    _description = "Purchase Container Tag"

    _columns = { 'name': fields.char('Tag Name', size=64, required=True),
               }

##########################################################################################
# Inherited purchase.container class definition
##########################################################################################
class purchase_container(osv.osv):

    _inherit     = "purchase.container"
    _description = "Purchase Container Info"

    ##########################################################################################
    # Get the total tax amount (float) from an account.invoice.line
    ##########################################################################################
    def get_tax_amount(self, cr, uid, line ):

        amount_tax = 0.0

        for tax_line in line.invoice_line_tax_id:
            amount_tax += tax_line.amount

        return amount_tax * line.price_subtotal

    ##########################################################################################
    # Get all account.invoices marked as "Mercaderia" and marked with this container,
    # calculate a bunch of fields, and populate a list of invoices on the container
    ##########################################################################################
    def sacar_facturas(self, cr, uid, ids, context={}):

        containers = self.browse( cr, uid, ids, context )

        for container in containers:

            # Get all invoices with this container ID and category = 'Mercaderia'
            invoice_ids = self.pool.get( 'account.invoice' ).search( cr, uid, [('container','=',container.id)] )

            # Do nothing if we don't find any relevant invoices
            if( len(invoice_ids) < 1 ):
                raise osv.except_osv( 'Warning!', "No se puede encontrar una factura con 'Tipo gasto por defecto: Mercaderia, y contenedor: " + container.name )

            # Delete all provider_invoice_entry's in this purchase.container
            for line in container.c_provider_lines:
                self.pool.get( 'provider_invoice_entry' ).write( cr, uid, [line.id], {'my_calc':False}, context=context )

            invoices = self.pool.get( 'account.invoice' ).browse( cr, uid, invoice_ids )

            category_sums = { 'mercaderia'              : 0.0,
                              'gastos_factura'          : 0.0,
                              'elaboracion_y_tramites'  : 0.0,
                              'gastos_bancarios'        : 0.0,
                              'liquidacion_de_aduana'   : 0.0,
                              'comision_de_despacho'    : 0.0,
                              'flete_maritimo'          : 0.0,
                              'seguro'                  : 0.0,
                              'inspeccion_verificadora' : 0.0,
                              'demora_conten_puerto'    : 0.0,
                              'transp_puerto_bodega'    : 0.0,
                              'gastos_de_aprobacion'    : 0.0,
                              'gastos_docum_naviera'    : 0.0,
                              'corpei'                  : 0.0,
                              'gastos_en_puerto'        : 0.0,
                              'gastos_varios'           : 0.0,
                           }

            total_tax = 0.0

            # Create a provider_invoice_entry for this invoice if it's a merchandise invoice, and get totals for each category
            for invoice in invoices:
                if( invoice.category.code_name == "mercaderia"):
                    self.pool.get( 'provider_invoice_entry' ).create( cr, uid, { 'my_calc' : container.id,
                                                                                 'p_name'  : invoice.partner_id.name,
                                                                                 'invoice' : invoice.internal_number,
                                                                                 'value'   : invoice.amount_untaxed,
                                                                               }
                                                                    )

                for line in invoice.invoice_line:
                    if( line.category and line.category.code_name ):
                        if line.category.code_name not in category_sums:
                            category_sums[ line.category.code_name ] = 0.0
                        else:
                            tax_amount = self.get_tax_amount( cr, uid, line )
                            category_sums[ line.category.code_name ] += line.price_subtotal# + tax_amount
                            total_tax += tax_amount

            calculations = { 'c_inv_fob_us'   : category_sums[ 'mercaderia'              ],
                             'c_taxes'        : total_tax,
                             'c_exp_invoices' : category_sums[ 'gastos_factura'          ],
                             'c_elab'         : category_sums[ 'elaboracion_y_tramites'  ],
                             'c_gastos_b'     : category_sums[ 'gastos_bancarios'        ],
                             'c_liqu_d'       : category_sums[ 'liquidacion_de_aduana'   ],
                             'c_comision'     : category_sums[ 'comision_de_despacho'    ],
                             'c_flete'        : category_sums[ 'flete_maritimo'          ],
                             'c_seguro'       : category_sums[ 'seguro'                  ],
                             'c_insp'         : category_sums[ 'inspeccion_verificadora' ],
                             'c_demora'       : category_sums[ 'demora_conten_puerto'    ],
                             'c_transp'       : category_sums[ 'transp_puerto_bodega'    ],
                             'c_gastos_a'     : category_sums[ 'gastos_de_aprobacion'    ],
                             'c_gastos_d'     : category_sums[ 'gastos_docum_naviera'    ],
                             'c_corpei'       : category_sums[ 'corpei'                  ],
                             'c_gastos_p'     : category_sums[ 'gastos_en_puerto'        ],
                             'c_gastos_v'     : category_sums[ 'gastos_varios'           ],
                           }

            container.write( calculations )

            #----------------------------------------------------------------------------------
            # Finally we need to update the container lines with their Venta Actual values
            #----------------------------------------------------------------------------------
            for line in container.container_lines:
                vals = self.pool.get( 'purchase.container.line' ).onchange_margin( cr, uid, [line.id],
                                                                                   cost_factor = container.factor_c,
                                                                                   margin      = container.sale_factor,
                                                                                   product     = line.product_id.id,
                                                                                   precio      = line.precio,
                                                                                   context     = context
                                                                                 )
                line.write( { 'sale_price':vals['value']['sale_price'] } )

    ##########################################################################################
    # c_fleet_exp function field
    ##########################################################################################
    def copy_vals(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
            result[r.id] = { "c_fleet_exp": r.c_flete, 'sale_factor_view' : (r.sale_factor+r.factor_t), "factor_c_view" : r.factor_c }
        return result

    ##########################################################################################
    # total_factor function field
    ##########################################################################################
    def cmpt_tfs(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
            result[r.id] = r.factor_c + r.factor_t/100
        return result

    ##########################################################################################
    # factor_c function field
    ##########################################################################################
    def cmpt_fs(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
            if(r.c_valor_fact ==0) :
                result[r.id] = r.c_f_total/1
            else:
                result[r.id] = r.c_f_total/r.c_valor_fact
        return result

    ##########################################################################################
    # c_valor_fact function field
    ##########################################################################################
    def cmpt_vf(self, cr, uid, ids, field_name, field_value, arg, context=None):

        records = self.browse(cr, uid, ids)
        result  = {}

        for r in records:
            valor_factura = 0.0
            line_ids = self.pool.get( 'account.invoice.line' ).search(cr, uid, [('container','=',r.id),('category.code_name','=','mercaderia')])
            for line in self.pool.get('account.invoice.line').browse(cr, uid, line_ids, context=context):
                valor_factura += line.price_subtotal
            result[r.id] = valor_factura

        return result

    ##########################################################################################
    # c_f_total function field
    ##########################################################################################
    def cmpt_taxes(self, cr, uid, ids, field_name, field_value, arg, context=None):

        records = self.browse(cr, uid, ids)
        result  = {}
        b_tax   = 0
        a_tax   = 0

        for r in records:
            b_tax        =   r.c_valor_fact \
                           + r.c_flete \
                           + r.c_gastos_a \
                           + r.c_elab \
                           + r.c_seguro \
                           + r.c_gastos_d \
                           + r.c_gastos_b \
                           + r.c_insp \
                           + r.c_corpei \
                           + r.c_liqu_d \
                           + r.c_demora \
                           + r.c_gastos_p \
                           + r.c_comision \
                           + r.c_transp \
                           + r.c_gastos_v

            a_tax        = b_tax - r.c_taxes

            result[r.id] = { "c_f_total" : b_tax , "c_s_total" : a_tax }

        return result

    ##########################################################################################
    # c_c_f_us function field
    ##########################################################################################
    def cmpt_rs(self, cr, uid, ids, field_name, field_value, arg, context=None):

        records = self.browse(cr, uid, ids)
        result  = {}
        c_total = 0
        s_total = 0
        f_total = 0
        top_total = 0

        for r in records:
            top_total = r.c_inv_fob_us+r.c_exp_invoices+r.c_fleet_exp

            for line in r.container_lines:
                c_total = c_total + line.product_qty * line.costo
                s_total = s_total + line.product_qty * line.sale_price
                f_total = f_total + line.product_qty * line.precio

            if(r.c_inv_fob_us == 0):
                result[r.id] = {"c_r_cost" : r.c_f_total, "c_r_fob": f_total, "c_r_sale" : s_total, "c_c_f_us":top_total}
            else:
                result[r.id] = {"c_r_cost" : r.c_f_total, "c_r_fob": f_total, "c_r_sale" : s_total, "c_c_f_us":top_total}

        return result

    ##########################################################################################
    # Get the company from the user
    ##########################################################################################
    def get_default_company(self, cr, uid, context={}):
        user = self.pool.get('res.users').browse(cr, uid, uid)
        return user.company_id.id or False

    ##########################################################################################
    # Get today's date
    ##########################################################################################
    def _get_good_date(self,cr,uid,context=None):
        now_date = fields.date.context_today(self, cr, uid, context=context)
        return now_date

    ##########################################################################################
    # Update the expected quantity field on all products in the container
    ##########################################################################################
    def update_expected_quantities(self,cr,uid,ids,context={}):
        for container in self.browse(cr,uid,ids):
            for line in container.container_lines:
                line.product_id.write( { 'expected_qty_draft_container' : line.product_qty } )

    ##########################################################################################
    # For each container line, make sure a product exists in the system for it, then calculate
    # the other values on the contaner line
    ##########################################################################################
    def pull_from_references(self,cr,uid,ids,context={}):

        for container in self.browse(cr,uid,ids):
            for line in container.container_lines:

                product_id = self.pool.get('product.product').search(cr,uid,[('sup_ref','=',line.code),('purchased','=',False)])

                if(len(product_id) == 0):
                    raise osv.except_osv( _('Unknown Product.'), _('There is no item in the system with this codigo.'))

                if(len(product_id) > 1):
                     raise osv.except_osv( _('Multiple Products'), _('There are multiple products in the system with this reference code!  Please removed duplicate product with ref: '+line.code))

            for line in container.container_lines:

                product_id = self.pool.get('product.product').search(cr,uid,[('sup_ref','=',line.code),('purchased','=',False)])
                product = self.pool.get('product.product').browse(cr,uid,product_id[0])

                if(product.def_provider.id) :
                    line.write({'margin':container.sale_factor_view/100,'supplier_id':product.def_provider.id, 'product_id': product_id[0], 'categ':product.categ_id.id, 'disp_name':product.name, 'precio' : product.fob_price, 'costo': product.fob_price * container.factor_c, 'total_price' : product.fob_price * container.factor_c * (1+container.sale_factor_view/100), 'sale_price':product.fob_price * container.factor_c * (1+container.sale_factor_view/100), 'code':product.sup_ref, 'pack':product.samir_packaging})
                else:
                    line.write({'margin':container.sale_factor_view/100,'product_id': product_id[0], 'categ':product.categ_id.id, 'disp_name':product.name, 'precio' : product.fob_price, 'costo': product.fob_price * container.factor_c, 'total_price' : product.fob_price * container.factor_c * (1+container.sale_factor_view/100), 'sale_price':product.fob_price * container.factor_c * (1+container.sale_factor_view/100), 'code':product.sup_ref, 'pack':product.samir_packaging})

    ##########################################################################################
    # Set the container's state to unconfirmed
    ##########################################################################################
    def unconfirm_container(self,cr,uid,ids,context={}):
        for container in self.browse(cr,uid,ids):
            p_o_objs = self.pool.get('purchase.order').search(cr,uid,[('container_id', '=' ,container.id)])
            for p_o in self.pool.get('purchase.order').browse(cr,uid,p_o_objs):
                if p_o.state != 'cancel':
                    raise osv.except_osv( _('Invalid Action!'), _('First Cancel All Purchase Orders Tied to this container'))
                    
            container.write({'state':'draft'})
            for line in container.container_lines:
                line.product_id.write({ 'container_id': None,
                                        'amount_purchased': 0,
                                        'purchased':False
                                     })
                line.write({'purchase_id':None})

    ##########################################################################################
    # Confirm the order
    ##########################################################################################
    def confirm_order(self, cr, uid, ids, context={}):

        pur_obj = self.pool.get('purchase.order')
        pur_line_obj = self.pool.get('purchase.order.line')

        for container in self.browse(cr, uid, ids):

            if not container.container_lines:
                raise osv.except_osv( _('Invalid Action!'), _('Please define some Product Lines.'))

            for line in container.container_lines:
                if line.product_id.purchased == True:
                    raise osv.except_osv( _('Invalid Action!'), _('You cannot purchase an item that has already been in a container!  Please check item code: ' +
                                          str(line.product_id.default_code).zfill(5)))

            seen = []
            for line in container.container_lines:
                if line.product_id in seen:
                    raise osv.except_osv( _('Invalid Action!'), _('Cannot add same product to multiple lines in the container') )
                else:
                    seen.append(line.product_id)

            
            for line in container.container_lines:
                next_code = self.pool.get('ir.sequence').next_by_code(cr, uid, 'samir_codigo')
                line.write( {'codigo':next_code} )
                line.product_id.write( { 'container_id'     : container.id,
                                         'amount_purchased' : line.product_qty,
                                         'fob_price'        : line.precio,
                                         'standard_price'   : line.costo,
                                         'purchased'        : True,
                                         'list_price'       : line.sale_price,
                                         'default_code'     : next_code,
                                         'date_purchased'   : datetime.now(),
                                         'codigo_in_num'    : int(next_code),
                                       } )

            container.write({'state': 'done'})

        return True  

    ##########################################################################################
    # This function returns an action that display existing pîcking orders of given container order ids.
    ##########################################################################################
    def view_picking(self, cr, uid, ids, context=None):

        mod_obj = self.pool.get('ir.model.data')
        pick_ids = []
        for container in self.browse(cr, uid, ids):
            for line in container.container_lines:
                if line.purchase_id:
                    for po in self.pool.get('purchase.order').browse(cr, uid, [line.purchase_id.id], context=context):
                        pick_ids += [picking.id for picking in po.picking_ids]
            
        action_model, action_id = tuple(mod_obj.get_object_reference(cr, uid, 'stock', 'action_picking_tree4'))
        action = self.pool.get(action_model).read(cr, uid, action_id, context=context)
        ctx = eval(action['context'])
        ctx.update({
            'search_default_container_id': ids[0],
        })
        action.update({'domain': [('container_id', '=', ids[0])]})
        action.update({
            'context': ctx,
        })
        return action

    ##########################################################################################
    # Column definitions
    ##########################################################################################
    _columns = { 'name'               : fields.char('Pedido', required=True),
                 'warehouse_id'       : fields.many2one('stock.warehouse', 'Bodega', required=True, domain=[('name','!=','Fake Warehouse')]),
                 'container_lines'    : fields.one2many('purchase.container.line', 'container_id', 'Product Lines'),
                 'state'              : fields.selection([('draft', 'Draft'), ('done', 'Confirmed')], string="State", readonly=True),
                 'date'               : fields.date('Fecha'),
                 'company_id'         : fields.many2one('res.company', 'Compañía'),
                 'tags'               : fields.many2many('container.tag', 'containe_tag_rel', 'tag_id', 'container_id', 'Tags'),
                 'c_provider_lines'   : fields.one2many('provider_invoice_entry', 'my_calc', 'Facturas Proveedores'),
                 'c_items'            : fields.char('Descripcion', size=64),
                 'c_order'            : fields.char('Contenedor', size=64),
                 'c_dau'              : fields.char('DAU', size=64),
                 'c_file_num'         : fields.char('File #', size=64),
                 'c_shipper'          : fields.char('Embarcador', size=64),
                 'c_origin'           : fields.char('Origin', size=64),
                 'c_date'             : fields.char('Date', size=64),
                 'c_port'             : fields.char('Puerto', size=64),
                 'c_warehouse'        : fields.char('Warehouse', size=64),
                 'c_valor_fact'       : fields.function(cmpt_vf, type='float', method=True, string='Valor Factura'),
                 'c_flete'            : fields.float('Flete Maritimo', digits_compute=dp.get_precision('Product Price')),
                 'c_gastos_a'         : fields.float('Gastos de Aprobacion', digits_compute=dp.get_precision('Product Price')),
                 'c_elab'             : fields.float('Elaboracion Y Tramites', digits_compute=dp.get_precision('Product Price')),
                 'c_seguro'           : fields.float('Seguro', digits_compute=dp.get_precision('Product Price')),
                 'c_gastos_d'         : fields.float('Gastos Docum. Naviera', digits_compute=dp.get_precision('Product Price')),
                 'c_gastos_b'         : fields.float('Gastos Bancarios', digits_compute=dp.get_precision('Product Price')),
                 'c_insp'             : fields.float('Inspeccion Verificadora', digits_compute=dp.get_precision('Product Price')),
                 'c_corpei'           : fields.float('Corpei', digits_compute=dp.get_precision('Product Price')),
                 'c_liqu_d'           : fields.float('Liquidacion de Aduana', digits_compute=dp.get_precision('Product Price')),
                 'c_demora'           : fields.float('Demora Conten./Puerto', digits_compute=dp.get_precision('Product Price')),
                 'c_gastos_p'         : fields.float('Gastos en Puerto', digits_compute=dp.get_precision('Product Price')),
                 'c_comision'         : fields.float('Comision de Despacho', digits_compute=dp.get_precision('Product Price')),
                 'c_transp'           : fields.float('Transp. Puerto/Bodega', digits_compute=dp.get_precision('Product Price')),
                 'c_gastos_v'         : fields.float('Gastos Varios', digits_compute=dp.get_precision('Product Price')),
                 'c_inv_fob_us'       : fields.float('Valor FOB US$', digits_compute=dp.get_precision('Product Price') ),
                 'c_exp_invoices'     : fields.float('Gastos Factura', digits_compute=dp.get_precision('Product Price') ),
                 'c_fleet_exp'        : fields.function(copy_vals,type='float', multi = 'c', method=True, string="Flete"),
                 'c_c_f_us'           : fields.function(cmpt_rs , multi = "a", type='float',  method=True, string='C & F US$'),
                 'c_f_total'          : fields.function(cmpt_taxes, multi = "b", type='float', method=True, string='Total Antes IVA'),
                 'c_taxes'            : fields.float('IVA', digits_compute=dp.get_precision('Product Price') ),
                 'c_s_total'          : fields.function(cmpt_taxes, multi='b', type='float', method=True, string='Total Despues IVA'),
                 'c_c_cost'           : fields.float('Cost', readonly = "True", digits_compute=dp.get_precision('Product Price') ),
                 'c_c_tax'            : fields.float('Tax', digits_compute=dp.get_precision('Product Price') ),
                 'c_c_total'          : fields.float('Tax', readonly = "True", digits_compute=dp.get_precision('Product Price') ),
                 'c_m_o_c'            : fields.float('Margin Over Cost',  digits_compute=dp.get_precision('Product Price') ),
                 'c_m_o_fob'          : fields.float('Margin Over FOB',  digits_compute=dp.get_precision('Product Price') ),
                 'c_r_cost'           : fields.function(cmpt_rs, multi = "a",   type='float', method=True, string='Cost'),
                 'c_r_sale'           : fields.function(cmpt_rs, multi = "a",   type='float',  method=True, string='Sale'),
                 'c_r_fob'            : fields.function(cmpt_rs, multi = "a", type="float", method = True, string="FOB"),
                 'factor_c'           : fields.function(cmpt_fs, type='float',  method=True, string='Cost Factor'),
                 'factor_t'           : fields.float("ISD        (%)", digits_compute=dp.get_precision('Product Price') ),
                 'total_factor'       : fields.function(cmpt_tfs, type='float', method=True, string='Total Factor'),
                 'sale_factor'        : fields.float("Sale Factor", digits_compute=dp.get_precision('Product Price') ),
                 'sale_factor_view'   : fields.function(copy_vals,type='float', multi = 'c', method=True, string="Factor Venta"),
                 'factor_c_view'      : fields.function(copy_vals,type='float', multi = 'c', method=True, string="Cost Factor"),
                 'product_ids'        : fields.one2many('product.product','container_id',"Product Ids"),
                 'purchase_order_ids' : fields.one2many('purchase.order', 'container_id', 'Purchase Orders', required=False),
                 'p_label'            : fields.char("test"),
               }

    ##########################################################################################
    # Default definitions
    ##########################################################################################
    _defaults = { 'state': 'draft',
                  'date': _get_good_date,
                  'company_id': get_default_company,
                  'name':'xxx',
                  'c_c_f_us':1,
                  'p_label':"%",
                }


##########################################################################################
# purchase.container.line definition
##########################################################################################
class purchase_container_line(osv.osv):
    
    _name        = "purchase.container.line"
    _description = "Purchased Container Materials"

    ##########################################################################################
    # Get the default sale margin
    ##########################################################################################
    def get_margin(self, cr, uid, context={}):

        container_id = context['container_id'] if context and 'container_id' in context else False

        if container_id == False :
            return 0
        else:
            default_margin = self.pool.get('purchase.container').browse(cr, uid, container_id, context=None).sale_factor_view/100
            return default_margin

    ##########################################################################################
    # costo function field
    ##########################################################################################
    def cmpt_precios(self, cr, uid, ids, field_name, field_value, arg, context=None):
        results = {}
        for r in self.browse(cr,uid,ids):
            results[r.id] = {"costo": r.precio * r.container_id.factor_c, "total_price": r.precio * r.container_id.factor_c * (r.margin+1)}
        return results

    ##########################################################################################
    # onchange_reference()
    ##########################################################################################
    def onchange_reference(self, cr, uid, ids, reference, cost_factor, sale_factor, context={}):
        
        if not reference:
            _logger.debug("ppp")
            _logger.debug(reference)
            return {'value': {'costo': 0.0}}
        
        product_id = self.pool.get('product.product').search(cr,uid,[('sup_ref','=',reference),('purchased','=',False)])
        if(len(product_id) == 0):
            raise osv.except_osv(
                        _('Unknown Product.'),
                        _('There is no item in the system with this codigo.'))


        if(len(product_id) > 1):
            raise osv.except_osv(
                        _('Multiple Products'),
                        _('There are multiple products in the system with this reference code!  Please removed duplicate ref: '+reference))


        product = self.pool.get('product.product').browse(cr,uid,product_id[0])
        
        
        val = {'supplier_id':product.def_provider.id, 'product_id': product_id[0], 'categ':product.categ_id.id, 'disp_name':product.name, 'precio' : product.fob_price, 'costo': product.fob_price * cost_factor, 'total_price' : product.fob_price * cost_factor * (1+sale_factor/100), 'sale_price':product.fob_price * cost_factor * (1+sale_factor/100), 'code':product.sup_ref, 'pack':product.samir_packaging}
        return {'value': val}

    ##########################################################################################
    # onchange_precio()
    ##########################################################################################
    def onchange_precio(self, cr, uid, ids, cost_factor, margin, product, precio, context={}):
        if not product:
            return {'value': {'costo': 0.0}}
        
        product = self.pool.get('product.product').browse(cr, uid, product)
        val = {'costo': precio * cost_factor, 'total_price' : precio * cost_factor * (1+margin), 'sale_price':precio * cost_factor * (1+margin)}
        return {'value': val}

    ##########################################################################################
    # onchange_margin()
    ##########################################################################################
    def onchange_margin(self, cr, uid, ids, cost_factor, margin, product, precio, context={}):
        if not product:
            return {'value': {'costo': 0.0}}
        val = {'total_price' : precio * cost_factor * (1+margin), 'sale_price':precio * cost_factor * (1+margin)}
        return {'value': val}

    ##########################################################################################
    # Column and default definitions
    ##########################################################################################
    _columns = { 'container_id' : fields.many2one('purchase.container', 'Contenedor', required=True),
                 'product_id'   : fields.many2one('product.product', 'Producto', required=True, domain=[('purchase_ok', '=', True), ('purchased','!=',True)]),
                 'pack'         : fields.related('product_id', 'samir_packaging', string="Embalaje", type="char"),
                 'codigo'       : fields.char('Codigo'),
                 'categ'        : fields.many2one('product.category',"Categoria"),
                 'disp_name'    : fields.char('Descripcion'),
                 'code'         : fields.char("Referencia"),
                 'precio'       : fields.float('Precio FOB', digits_compute=dp.get_precision('Product Price')),
                 'product_qty'  : fields.float('Cantidad', digits_compute=dp.get_precision('Product Price')),
                 'costo'        : fields.function(cmpt_precios, type='float', multi='cc', method=True, string='Precio Costo'),
                 'margin'       : fields.float('Margen Venta', digits_compute=dp.get_precision('Product Price')),
                 'total_price'  : fields.function(cmpt_precios, multi='cc', string = 'Venta Ref', type='float'),
                 'sale_price'   : fields.float('Venta Actual', digits_compute=dp.get_precision('Precio Venta')),
                 'supplier_id'  : fields.many2one('res.partner','Proveedor', required=True),
                 'purchase_id'  : fields.many2one('purchase.order', 'Purchase Order'),
               }
    
    _defaults = { 'product_qty': 1,
                  'margin'     : get_margin,
                }



##########################################################################################
# Inherited purchase.order.line class. Add a sale price and container line id.
##########################################################################################
class purchase_order_line(osv.osv):
    
    _inherit = 'purchase.order.line'
    
    _columns = { 'sale_price'        : fields.float('Precio de Venta', digits_compute=dp.get_precision('Product Price')),
                 'container_line_id' : fields.many2one('purchase.container.line', "Linea de Contenedor"),
               }
    
    defaults = { 'sale_price': 0.0,
               }

##########################################################################################
# Inherited purchase.order class definition
##########################################################################################
class purchase_order(osv.osv):
    
    _inherit = "purchase.order"

    ##########################################################################################
    # TODO CHECK IF THIS IS RIGHT? SHOULD BE FOB PRICE?
    # fob_price_sold function field
    ##########################################################################################
    def cmpt_fob_price_sold(self, cr, uid, ids, field_name, field_value, arg, context=None):
 
        records = self.browse(cr, uid, ids)
        result = {}
        prov_totals = 0

        for r in records:
            prov_totals=0
            for entry in r.order_line:
                prov_totals += round(entry.product_id.standard_price,2) * (entry.product_qty - entry.product_id.amount_sold_f)
            result[r.id] = prov_totals

        return result

    ##########################################################################################
    # _prepare_order_picking() override. Add warehouse and container ID's
    ##########################################################################################
    def _prepare_order_picking(self, cr, uid, order, context=None):
        res = super(purchase_order, self)._prepare_order_picking(cr, uid, order, context=context)
        res.update({'warehouse': order.warehouse_id and  order.warehouse_id.id, 'container_id': order.container_id and order.container_id.id})
        return res

    ##########################################################################################
    # _prepare_order_picking() override. Add sale price and warehouse, container, and container line  ID's
    ##########################################################################################
    def _prepare_order_line_move(self, cr, uid, order, order_line, picking_id, context=None):
        res = super(purchase_order, self)._prepare_order_line_move(cr, uid, order, order_line, picking_id, context=context)
        res.update( { 'warehouse': order.warehouse_id and  order.warehouse_id.id,
                      'container_id': order.container_id and order.container_id.id,
                      'sale_price': order_line.sale_price,
                      'container_line_id': order_line.container_line_id.id
                    } )
        return res

    ##########################################################################################
    # action_cancel() override
    ##########################################################################################
    def action_cancel(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        for purchase in self.browse(cr, uid, ids, context=context):
            for pick in purchase.picking_ids:
                if pick.state not in ('draft','cancel'):
                    raise osv.except_osv(
                        _('Unable to cancel this purchase order.'),
                        _('First cancel all receptions related to this purchase order.'))
            for pick in purchase.picking_ids:
                wf_service.trg_validate(uid, 'stock.picking', pick.id, 'button_cancel', cr)
            for inv in purchase.invoice_ids:
                if inv and inv.state not in ('cancel','draft'):
                    raise osv.except_osv(
                        _('Unable to cancel this purchase order.'),
                        _('You must first cancel all receptions related to this purchase order.'))
                if inv:
                    wf_service.trg_validate(uid, 'account.invoice', inv.id, 'invoice_cancel', cr)

        self.write(cr,uid,ids,{'state':'cancel'})

        for (id, name) in self.name_get(cr, uid, ids):
            wf_service.trg_validate(uid, 'purchase.order', id, 'purchase_cancel', cr)
        return True

    ##########################################################################################
    # Column definitions. Add container_id and fob_price_sold
    ##########################################################################################
    _columns = { 'container_id'   : fields.many2one('purchase.container', 'Contenedor'),
                 'fob_price_sold' : fields.function(cmpt_fob_price_sold, type='float', method=True, string='Fob Exist.'),
               }

##########################################################################################
# provider_invoice_entry class definition
##########################################################################################
class provider_calc_line(osv.osv):

    _name        = 'provider_invoice_entry'
    _description = "invoice helper"

    _columns = { 'my_calc' : fields.many2one('purchase.container', 'Calc Object'),
                 'p_name'  : fields.char('Proveedor', size=64, required=True),
                 'invoice' : fields.char('# de Factura', size=64, required=True),
                 'value'   : fields.float('Valor sin IVA', digits_compute=dp.get_precision('Product Price') ),
               }

##########################################################################################
# Container analysis report class definition
##########################################################################################
class container_analysis_report(osv.osv_memory):

    _name        = 'purchase.container.report'
    _description = "Purchase Container Report"

    _columns = { 'lines' : fields.one2many('purchase.container.report.line','report',string="Lines"),
               }

    ##########################################################################################
    # Generate lines
    ##########################################################################################
    def generate_lines(self,cr,uid,ids, context=None):
        container_obj = self.pool.get('purchase.container').browse(cr,uid,context['active_ids'])
        provider_list = {}
        container_list = {}
        for c in container_obj:
            big_list = {}
            for line in c.container_lines:
                my_list = big_list.get(line.supplier_id,{})
                my_list['fi'] = c.c_file_num
                my_list['date'] = c.date
                my_list['description'] =c.c_items

                p = line.product_id

                total_sold = p.total_val_sold_f
                total_invent = p.qty_available * p.list_price
                total_cost = p.amount_purchased * p.standard_price
                discount_loss = p.discount_correction_f

                my_list['venta'] = my_list.get('venta',0) + total_sold
                my_list['venta_inv'] = my_list.get('venta_inv',0) + total_invent
                my_list['costo'] = my_list.get('costo',0) + total_cost
                my_list['discount_loss'] = my_list.get('discount_loss',0) + discount_loss
                my_list['q_sold'] = my_list.get('q_sold',0) + p.amount_sold_f
                my_list['q_purchased'] = my_list.get('q_purchased',0) + p.amount_purchased
                big_list[line.supplier_id] = my_list

            container_list[c.id] = big_list

        for key in container_list:
            for key2 in container_list[key]:
                prov = container_list[key][key2]
                realization = float(prov['q_sold'])/float(prov['q_purchased'])
                self.pool.get('purchase.container.report.line').create(cr,uid,{'proveedor':key2.name, 'file': prov['fi'], 'fecha':prov['date'], 'description':prov['description'],'discount_loss':prov['discount_loss'],'realization':realization, 'venta':prov['venta'], 'venta_invent':prov['venta_inv'], 'costo':prov['costo'], 'gana':prov['venta']-prov['costo']-prov['discount_loss'],'report':ids[0]})

        return { 'type'      : 'ir.actions.act_window',
                 'name'      : 'Form heading',
                 'view_mode' : 'tree,form',
                 'view_type' : 'form',
                 'res_model' : 'purchase.container.report.line',
                 'domain'    : [('report','=',ids[0])]
               }


##########################################################################################
# purchase.container.report.line class definition
##########################################################################################
class container_analysis_report_line(osv.osv_memory):

    _name        = 'purchase.container.report.line'
    _description = "Purchase Container Report"

    _columns = { 'file'          : fields.char('File', size=32),
                 'factura'       : fields.char('Facura',size=32),
                 'codigo_init'   : fields.integer('Cod. Init'),
                 'codigo_final'  : fields.integer('Cod. Fin'),
                 'fecha'         : fields.date("Fecha"),
                 'proveedor'     : fields.char('Proveedor',size=32),
                 'description'   : fields.char('Description',size=64),
                 'fob'           : fields.float("FOB"),
                 'fob_invent'    : fields.float("FOB Invent."),
                 'costo'         : fields.float("Costo"),
                 'venta'         : fields.float("Vendido"),
                 'venta_invent'  : fields.float("Venta Invent."),
                 'realization'   : fields.float("Realization"),
                 'purchased'     : fields.float("Sales Val Purchased"),
                 'discount_loss' : fields.float("Descuento Correccion"),
                 'gana'          : fields.float("Gana/Perd."),
                 'report'        : fields.many2one("purchase.container.report", "Report"),
              }





















