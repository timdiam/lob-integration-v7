from report import report_sxw
from report.report_sxw import rml_parse
import pooler

class Parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_product_by_location':self.get_product_by_location,
            'get_ubi':self.get_ubi,
            'cr':cr,
            'uid':uid,
            'g_context':context,
        })

    def get_ubi(self):
        return self.localcontext['location_name']

    def get_product_by_location(self, which, p):
        self.localcontext['g_context']['location'] = which
        return p.qty_available
