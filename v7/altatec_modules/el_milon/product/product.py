# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
import logging
import traceback
import re
_logger = logging.getLogger(__name__)

class product_view_image(osv.osv_memory):
    _name='product.view.image'

    _columns={'product_id':fields.many2one('product.product',string="Product"),
		'image':fields.related('product_id','image',type='binary',string=""),}


class product_product(osv.osv):
    
    _inherit = 'product.product'
   
    def open_image_millon(self,cr,uid,ids,context):

        this = self.browse(cr, uid, ids, context=context)[0]
	obj = self.pool.get('product.view.image').create(cr,uid,{'product_id':ids[0]})

        return {
                'type': 'ir.actions.act_window',
                'name': 'Open Image',
                'view_mode': 'form,tree',
                'view_type': 'form',
                'res_model': 'product.view.image',
                'nodestroy': 'false',
		'target':'new',
                'res_id': obj,}

    def cmpt_fpa(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
	prov_totals = 0

        for r in records:

		result[r.id] = r.fob_price * r.qty_available

       	return result


    def cmpt_cpa(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
	prov_totals = 0

        for r in records:

		result[r.id] = r.standard_price * r.qty_available

       	return result

    def cmpt_spa(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
	prov_totals = 0

        for r in records:

		result[r.id] = r.list_price * r.qty_available

       	return result

    def find_bodega(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
	prov_totals = 0

        for r in records:
		#_logger.debug('LLL' + r.original_bodega + ' ' + r.original_bodega_i)
		
		if(r.original_bodega == False):
			if(r.original_bodega_i):
				result[r.id] = 'B' + r.original_bodega_i
			else:
				result[r.id] = ""
		else:
			result[r.id] = r.original_bodega

       	return result


    def calc_prod_history(self, cr, uid, ids, field_name, field_value, arg, context=None):
	
	records = self.browse(cr, uid, ids)
        result = {}


        for product in records:
		sold_d = 0
		price_d=0
		discount_d = 0
		
		inv_obj = self.pool.get('account.invoice.line').search(cr,uid,[('invoice_id.state','in',['open','paid']),('product_id','=',product.id),('invoice_id.type','=','out_invoice')])

		for line in self.pool.get('account.invoice.line').browse(cr,uid,inv_obj):
			sold_d += line.quantity
			price_d += (line.quantity * line.price_unit)
			discount_d += ((line.discount/100) * (line.quantity * line.price_unit))

		result[product.id] = {"amount_sold_f": sold_d,'discount_correction_f' : discount_d, "total_val_sold_f":price_d}

       	return result	
 
    _columns = {
        'cant_por_mayor':fields.integer('Cantidad por Descuento Mayor'),
		'parent_categ_id3':fields.related('categ_id','parent_id',type='many2one',relation='product.category', string="Categoria Pariente"),
                'default_code' : fields.char('Codigo', size=64, select=True),
		'sup_ref' : fields.char('Referencia', size=64, select=True),
		'dup_avail':fields.integer('Estimated Qty'),
		'container_id':fields.many2one('purchase.container',string='Pedido', readonly=True),
		'samir_packaging':fields.char('Embalaje', size=32),
		'amount_purchased':fields.integer('Cantidad Comprado'),
		'amount_sold':fields.integer('Cantidad Vendido old', readonly=True),
		'amount_sold_f':fields.function(calc_prod_history,type='float',method=True,multi='cph',string='Cantidad Vendido'),
		'total_val_sold':fields.float('Valor Vendido old'),
		'total_val_sold_f':fields.function(calc_prod_history,type='float',method=True,multi='cph',string='Valor Vendido'),
		'discount_correction':fields.float('Correccion de Descuento old', readonly=True),
		'discount_correction_f':fields.function(calc_prod_history,type='float',method=True,multi='cph',string='Correccion de Descuento'),
		'purchased':fields.boolean('Fue Comprado'),
		'cost_price':fields.float('Precio Costo OLD'),
                 'fob_price':fields.float("Precio FOB (new)"),
		'standard_price':fields.float('Precio Costo NEW (old fob price)'),
               'def_provider':fields.many2one('res.partner', "Proveedor"),
		'date_purchased':fields.date("Fecha de Ingreso"),
		'expected_qty_draft_container':fields.integer('Por Comprar'),		
		'codigo_in_num':fields.integer("Rango (Codigo)"),
		#THIS SHOULD CHANGE SHOULD NOT BE RELATIONAL
		'original_bodega':fields.related('container_id','warehouse_id','name',type='char', string="ZBodega Origen"),
		'original_bodega_i':fields.char("IMPORT"),
		'original_bodega_h':fields.function(find_bodega, type="char",method=True,string="Bodega Origen"),
		'seller_ids': fields.one2many('product.supplierinfo', 'product_id', 'Depr sup.'),
		'fob_por_avail':fields.function(cmpt_fpa,type='float',method=True,string='FOB Total'),
		'costo_por_avail':fields.function(cmpt_cpa,type='float',method=True,string='Costo Total'),
		'sales_por_avail':fields.function(cmpt_spa,type='float',method=True,string='Precio Total'),
		'old_prod':fields.integer('Old?'),
		'codigo_antiguo':fields.char('Codigo Antiguo'),
		 }
    _defaults = {
                 'cant_por_mayor':12,
                 }





    def name_search(self, cr, user, name='', args=None, operator='ilike', context=None, limit=100):
        if not args:
            args = []
        if name:

            ids = self.search(cr, user, ['|',('default_code','=',name.zfill(5)),('sup_ref','=',name)]+ args, limit=limit, context=context)
            _logger.debug(ids)
	    if not ids:
                ids = self.search(cr, user, [('ean13','=',name)]+ args, limit=limit, context=context)
            if not ids:
                # Do not merge the 2 next lines into one single search, SQL search performance would be abysmal
                # on a database with thousands of matching products, due to the huge merge+unique needed for the
                # OR operator (and given the fact that the 'name' lookup results come from the ir.translation table
                # Performing a quick memory merge of ids in Python will give much better performance
                ids = set()
                ids.update(self.search(cr, user, args + [('default_code',operator,name)], limit=limit, context=context))
                if not limit or len(ids) < limit:
                    # we may underrun the limit because of dupes in the results, that's fine
                    ids.update(self.search(cr, user, args + [('name',operator,name)], limit=(limit and (limit-len(ids)) or False) , context=context))
                ids = list(ids)
            if not ids:
                ptrn = re.compile('(\[(.*?)\])')
                res = ptrn.search(name)
                if res:
                    ids = self.search(cr, user, [('default_code','=', res.group(2))] + args, limit=limit, context=context)
        else:
            ids = self.search(cr, user, args, limit=limit, context=context)
        result = self.name_get(cr, user, ids, context=context)
        return result

    def copy_over_dups(self, cr, uid, context=None):
	prod_ids = self.pool.get('product.product').search(cr,uid, [('active','=','True')], context)
	for prod in self.pool.get('product.product').browse(cr,uid,prod_ids, context) :
		prod.write({'dup_avail' : prod.qty_available})

    def create(self, cr, uid, vals, context={}):
	print "TEST"
	
	return super(product_product, self).create(cr, uid, vals, context)

    
