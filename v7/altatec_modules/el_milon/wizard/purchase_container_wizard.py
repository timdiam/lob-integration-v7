# -*- coding: utf-8 -*-
##############################################################################
#
# This file defines the purchase_container_wizard class.
#
# Author:   Dan Haggerty
# Date:     Feb. 24th, 2015
#
##############################################################################

from openerp.osv import fields, osv
from   StringIO    import StringIO
import base64
import csv
from openerp import netsvc

##############################################################################
# Class definition for purchase_container_wizard
##############################################################################
class purchase_container_wizard( osv.osv ):

    _name = "purchase.container.wizard"

    #################################################################################
    # Read a CSV file's encoded contents and return a list of dictionaries containing
    # the formatted data
    #################################################################################
    def get_data_from_csv( self, filestring ):

        CSV_NUM_COLUMNS = 7

        data = []
        error_string = ""

        file        = StringIO( base64.decodestring( filestring ) )
        file_reader = csv.reader( file, delimiter=',', quotechar='"')

        for i, row in enumerate( file_reader ):

            line = {}

            # Validate number of columns
            if( len( row ) != CSV_NUM_COLUMNS ):
                raise osv.except_osv( 'Error!', "Fila " + str( i+1 ) + ": " + "Esta fila debe tener " + str( CSV_NUM_COLUMNS ) + " columnas\n" )

            # Strip spaces at the start and end of each field, and all spaces and commas from values
            input_supplier_ref  = row[ 0 ].strip()
            input_product_name  = row[ 1 ].strip()
            input_category_name = row[ 2 ].strip()
            input_price         = row[ 3 ].replace( " ", "" ).replace( ",", "" )
            input_supplier_name = row[ 4 ].strip()
            input_packaging     = row[ 5 ].strip()
            input_quantity      = row[ 6 ].replace( " ", "" ).replace( ",", "" )

            # Validate the price in the 3rd column
            try:    float( input_price )
            except: error_string += "Fila " + str( i+1 ) + ": " + "Columna 3 debe ser un precio\n"

            # Validate the quantity in the 6th column
            try:    int( input_quantity )
            except: error_string += "Fila " + str( i+1 ) + ": " + "Columna 6 debe ser una cantidad\n"

            # Store the data in a dictionary
            line[ 'supplier_ref'  ] = input_supplier_ref
            line[ 'product_name'  ] = input_product_name
            line[ 'category_name' ] = input_category_name
            line[ 'fob_price'     ] = float( input_price )
            line[ 'supplier_name' ] = input_supplier_name
            line[ 'packaging'     ] = input_packaging
            line[ 'quantity'      ] = int( input_quantity )
            data.append( line )

        if( error_string != "" ):
            raise osv.except_osv( 'Error!', 'Se encontraron los siguientes errores con el archivo:\n' + error_string )

        return data

    ##############################################################################
    # import_container_button()
    ##############################################################################
    def import_container_button(self, cr, uid, ids, context=None ):

        wizard = self.browse( cr, uid, ids[ 0 ], context=context )

        #----------------------------------------------------------------------------------
        # Validate the parameters of the wizard
        #----------------------------------------------------------------------------------
        container_obj = self.pool.get( 'purchase.container' )

        #----------------------------------------------------------------------------------
        # Obtain and validate the container CSV file
        #----------------------------------------------------------------------------------
        if( wizard.input_file == False ):
            raise osv.except_osv( 'Error!', "El archivo esta vacio!" )

        # Extract data from the csv file into a list of dictionaries
        data = self.get_data_from_csv( wizard.input_file )

        #----------------------------------------------------------------------------------
        # If user selected "Create new container", create a purchase.container with the
        # attributes given by the user
        #----------------------------------------------------------------------------------
        if( wizard.mode == 'create' ):

            container_ids = container_obj.search( cr, uid, [('name','=',wizard.container_name)], context=context )

            if( len(container_ids) > 0 ):
                raise osv.except_osv( 'Error!', "Un contenedor ya existe con el nombre: " + wizard.container_name )

            container_id = container_obj.create( cr,
                                                 uid,
                                                 { 'name'         : wizard.container_name,
                                                   'warehouse_id' : wizard.warehouse.id,
                                                   'date'         : wizard.date,
                                                   'sale_factor'  : wizard.default_sales_margin,
                                                   'factor_t'     : 5,
                                                   'state'        : 'draft',
                                                 },
                                                 context=context
                                               )
        else:
            container_id = wizard.existing_container.id

        #----------------------------------------------------------------------------------
        # Create a purchase.container.line for each line in the file
        #----------------------------------------------------------------------------------
        container_line_obj = self.pool.get( 'purchase.container.line' )
        product_obj        = self.pool.get( 'product.product'         )
        category_obj       = self.pool.get( 'product.category'        )
        partner_obj        = self.pool.get( 'res.partner'             )

        # We need to keep a dictionary of supplier_id -> list of dictionaries for the purchase order we'll create later
        supplier_line_dict = {}

        # Iterate over all rows in the file
        for row in data:

            # Lookup the supplier by name, if it doesn't exist, throw an error
            supplier_ids = partner_obj.search( cr, uid, [('name','=',row['supplier_name'])], context=context )

            # If supplier doesn't exist in the system, throw an error
            if( len(supplier_ids) < 1 ):
                raise osv.except_osv( 'Error!', "El proveedor: " + row['supplier_name'] + " no existe en el sistema." )
            elif( len(supplier_ids) > 1 ):
                raise osv.except_osv( 'Error!', "Hay mas que 1 proveedor que se llama: " + row['supplier_name'] )
            else:
                supplier_id = supplier_ids[ 0 ]

            # Lookup the product category based on its name
            category_ids = category_obj.search( cr, uid, [('name', '=', row['category_name'])], context=context )

            # If the product category doesn't exist in the system, throw an error
            if( len(category_ids) < 1 ):
                raise osv.except_osv( 'Error!', "La categoria: " + row['category_name'] + " no existe en el sistema." )

            elif( len(category_ids) > 1 ):
                found_categories = category_obj.browse( cr, uid, category_ids, context=context )
                num_categories_with_this_name = 0

                for category in found_categories:
                    if( category.name == row['category_name'].decode('utf-8') ):
                        category_id = category.id
                        num_categories_with_this_name += 1

                if( num_categories_with_this_name > 1 ):
                    raise osv.except_osv( 'Error!', "Hay mas que 1 categoria que se llama: " + row['category_name'] )
                elif( num_categories_with_this_name == 0 ):
                    raise osv.except_osv( 'Error!', "La categoria: " + row['category_name'] + " no existe en el sistema." )

            else:
                category_id = category_ids[0]

            # Lookup the product based on its supplier reference
            prod_ids = product_obj.search( cr, uid, [('sup_ref', '=', row['supplier_ref'])], context=context )

            # If product doesn't exist in the system, create it
            if( len(prod_ids) < 1 ):
                product_id = product_obj.create( cr,
                                                 uid,
                                                 { 'name'            : row['product_name'],
                                                   'sup_ref'         : row['supplier_ref'],
                                                   'categ_id'        : category_id,
                                                   'sale_ok'         : True,
                                                   'purchase_ok'     : True,
                                                   'samir_packaging' : row['packaging'],
                                                   'hr_expense_ok'   : False,
                                                   'type'            : 'product',
                                                   'valuation'       : 'real_time',
                                                   'fob_price'       : row['fob_price' ],
                                                 },
                                                 context=context
                                               )
            else:
                raise osv.except_osv( 'Error!', "Un producto con la referencia proveedor: " + row['supplier_ref'] + " ya existe en el sistema." )



            container_line_id = container_line_obj.create( cr,
                                                           uid,
                                                           { 'categ'        : category_id,
                                                             'code'         : row[ 'supplier_ref' ],
                                                             'disp_name'    : row[ 'product_name' ],
                                                             'supplier_id'  : supplier_id,
                                                             'product_id'   : product_id,
                                                             'container_id' : container_id,
                                                             'product_qty'  : row[ 'quantity' ],
                                                             'precio'       : row[ 'fob_price' ],
                                                             'margin'       : wizard.default_sales_margin,
                                                           },
                                                           context=context,
                                                         )

            # Add the information gathered from this line to our supplier_line_dict for use later
            if( supplier_id not in supplier_line_dict ):
                supplier_line_dict[ supplier_id ] = []

            supplier_line_dict[ supplier_id ].append( { 'category_id'       : category_id,
                                                        'supplier_id'       : supplier_id,
                                                        'supplier_ref'      : row['supplier_ref'],
                                                        'product_id'        : product_id,
                                                        'container_line_id' : container_line_id,
                                                        'fob_price'         : row['fob_price'],
                                                        'quantity'          : row['quantity'],
                                                        'margin'            : wizard.default_sales_margin,
                                                        'container_id'      : container_id,
                                                        'product_name'      : row['product_name'],
                                                      }
                                                    )

        #----------------------------------------------------------------------------------
        # Now we have a purchase.container, let's make a purchase order for each supplier
        #----------------------------------------------------------------------------------
        purchase_order_obj      = self.pool.get( 'purchase.order'      )
        purchase_order_line_obj = self.pool.get( 'purchase.order.line' )
        wf_service              = netsvc.LocalService("workflow")

        purchase_order_ids = []

        container = container_obj.browse( cr, uid, container_id )

        for supplier_id in supplier_line_dict:

            onchange_data = purchase_order_obj.onchange_partner_id( cr, uid, [], supplier_id )

            # Create the purchase order
            purchase_order_id = purchase_order_obj.create( cr,
                                                           uid,
                                                           { 'partner_id'   : supplier_id,
                                                             'date_order'   : wizard.date,
                                                             'warehouse_id' : container.warehouse_id.id,
                                                             'container_id' : container_id,
                                                             'location_id'  : container.warehouse_id.lot_stock_id.id,
                                                             'pricelist_id' : onchange_data['value']['pricelist_id'],
                                                           },
                                                           context=context
                                                         )

            purchase_order_ids.append( purchase_order_id )

            # Create the purchase order lines
            for line in supplier_line_dict[ supplier_id ]:

                purchase_order_line_obj.create( cr,
                                                uid,
                                                { 'order_id'          : purchase_order_id,
                                                  'date_planned'      : wizard.date,
                                                  'product_id'        : line[ 'product_id'        ],
                                                  'name'              : line[ 'product_name'      ],
                                                  'product_qty'       : line[ 'quantity'          ],
                                                  'price_unit'        : line[ 'fob_price'         ],
                                                  'container_line_id' : line[ 'container_line_id' ],
                                                },
                                                context=context
                                              )

            # Validate and confirm the purchase order
            wf_service.trg_validate(uid, 'purchase.order', purchase_order_id, 'purchase_confirm', cr)
            wf_service.trg_validate(uid, 'purchase.order', purchase_order_id, 'purchase_approve', cr)
            purchase_order_obj.wkf_confirm_order(cr, uid, [purchase_order_id])
            purchase_order_obj.wkf_approve_order(cr, uid, [purchase_order_id])

        #----------------------------------------------------------------------------------
        # Now we've made our purchase orders, let's set the container/category related
        # stuff in the invoices that those purchase orders generated
        #----------------------------------------------------------------------------------
        invoice_obj      = self.pool.get( 'account.invoice'             )
        invoice_line_obj = self.pool.get( 'account.invoice.line'        )
        category_obj     = self.pool.get( 'purchase.container.category' )

        for purchase_order_id in purchase_order_ids:

            purchase_order = purchase_order_obj.browse( cr, uid, purchase_order_id, context=context )

            # Get the supplier invoice for this purchase order
            invoice_ids = invoice_obj.search( cr, uid, [('origin','=',purchase_order.name)], context=context )

            if( len(invoice_ids) < 1 ):
                raise osv.except_osv( 'Error!', "No se puede encontrar la factura de la pedido de compra: " + purchase_order.name )
            elif( len(invoice_ids) > 1 ):
                raise osv.except_osv( 'Error!', "Se encuentro mas que 1 factura de la pedido de compra: " + purchase_order.name )

            # Get purchase.container.category ID of "mercaderia" category
            category_ids = category_obj.search( cr, uid, [('code_name','=','mercaderia')], context=context )

            if( len(category_ids) < 1 ):
                raise osv.except_osv( 'Error!', "No se puede encontrar el Tipo Gasto: Mercaderia" )

            # Update "Contenedor" and "Tipo Categoria por defecto" for this invoice
            # TODO: We might need to update more things in this invoice
            invoice_obj.write( cr, uid, invoice_ids[0], { 'container':container_id, 'category': category_ids[0] }, context=context )

            invoice = invoice_obj.browse( cr, uid, invoice_ids[0], context=context)

            # Now set the "Tipo Gasto" for all invoice lines in this invoice
            for line in invoice.invoice_line:
                invoice_line_obj.write( cr, uid, line.id, {'category':category_ids[0], 'container':container_id} )

        #----------------------------------------------------------------------------------
        # Now let's update the "Calculations" section of this container
        #----------------------------------------------------------------------------------
        container_obj.sacar_facturas( cr, uid, [container_id], context=context )

        return { 'type'      : 'ir.actions.act_window',
                 'view_type' : 'form',
                 'view_mode' : 'form,tree',
                 'res_model' : 'purchase.container',
                 'res_id'    : container_id,
               }


    ##############################################################################
    # Column definitions
    ##############################################################################
    _columns = { 'mode'                 : fields.selection( [('create', 'Crear nuevo contenedor'), ('update', 'Agregar mercaderia a un contenedor existente')], "Modo", required=True ),
                 'container_name'       : fields.char( "Nombre de Contenedor" ),
                 'existing_container'   : fields.many2one( "purchase.container", "Contenedor" ),
                 'date'                 : fields.date( "Fecha", required = True ),
                 'warehouse'            : fields.many2one( "stock.warehouse", "Bodega" ),
                 'default_sales_margin' : fields.float( "Margen de ventas por defecto", required = True ),
                 'input_file'           : fields.binary( "Archivo", required = True ),
               }

    _defaults = { 'mode'                 : "create",
                  'date'                 : fields.date.context_today,
                }