############################################################################################
#
############################################################################################
from openerp.osv import fields, osv, orm

############################################################################################
#
############################################################################################
class el_millon_edit_invoice_line_wizard(osv.osv_memory):

    _name = "el.millon.edit.invoice.line.wizard"

    def confirm(self, cr, uid, ids, context={}):

        for wizard in self.browse(cr, uid, ids, context=context):
            if not wizard.line_id:
                raise osv.except_osv("Programming Error", "Contacte a Altatec")
            if wizard.mode == 'container':
                wizard.line_id.write({'container':wizard.container_id.id if wizard.container_id else False})
            elif wizard.mode == 'category':
                wizard.line_id.write({'category':wizard.category_id.id if wizard.category_id else False})


    _columns = { 'mode'         : fields.selection([('container','Container'),('category','Category')], "Modo"),
                 'container_id' : fields.many2one('purchase.container', "Contenedor"),
                 'category_id'  : fields.many2one('purchase.container.category', "Tipo gasto importacion"),
                 'line_id'      : fields.many2one('account.invoice.line', "Line ID"),
               }