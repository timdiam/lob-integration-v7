# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP SA (<http://openerp.com>).
#    Copyright (C) 2013-TODAY Synconics (<http://synconics.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp import netsvc
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP, float_compare
from datetime import datetime
import logging
import time
import pytz
_logger = logging.getLogger(__name__)

# class nota_de_credito_tim(osv.osv):
#
#     _name = 'nota.de.credito.tim'
#     _description = "Notas de Credito"
#
#
#
#
#     def confirm_nota(self,cr,uid,ids,context={}):
# 	for nota in self.browse(cr,uid,ids):
# 		nota.write({'state':'open'})
#
#     def close_nota(self,cr,uid,ids,context={}):
# 	for nota in self.browse(cr,uid,ids):
#
# 		nota.write({'state':'closed','date_closed':datetime.now().strftime('%Y-%m-%d')})
#
#
#
#
#
#
#     _columns = {
#                 'credito': fields.float('Credito', required=True),
# 		'cliente': fields.many2one('res.partner','Cliente', required=True),
# 		'ref':fields.char('Referencia', required=True),
# 		'state':fields.selection([('draft', 'Borrador'),('open','Abierto'),('closed','Cerrado')], string="Estado", readonly=True),
# 		'date_opened':fields.date('Fecha Initial', readonly=True, required=True),
# 		'date_closed':fields.date('Fecha de Cerrar', readonly=True),
#
#                 }
#     _defaults = {
# 		'date_opened': datetime.now().strftime('%Y-%m-%d'),
# 		'state': 'draft',
# 		}
#

class sale_order(osv.osv):
    
    _inherit = "sale.order"
    
    def create(self,cr,uid,vals,context=None):
        
        taxes = []
        part = self.pool.get('res.partner').browse(cr,uid,vals['partner_id'],context=context)
        for tax in part.vat_tax:
            taxes.append(tax.id)
        if taxes:
            vals.update( {'vat': [(6,0, taxes)]} )
        return super(sale_order,self).create(cr,uid,vals,context=context)
    
    def write(self, cr, uid, ids, vals, context = None):
        taxes = []
        _logger.debug(vals)
        if 'partner_id' in vals:
            part = self.pool.get('res.partner').browse(cr,uid,vals['partner_id'],context=context)
            for tax in part.vat_tax:
                taxes.append(tax.id)
            vals.update( {'vat': [(6,0, taxes)]} )
         
        return super(sale_order, self).write(cr, uid, ids, vals, context = context)
        
    def action_invoice_create(self, cr, uid, ids, grouped=False, states=['confirmed', 'done', 'exception'], date_invoice = False, context={}):
        
        objs = self.browse(cr,uid,ids,context)
        for obj in objs:
            if obj.retail_type == 'quick':
                return super(sale_order,self).action_invoice_create( cr, uid, ids, grouped=grouped, states=states, date_invoice = obj.date_order, context=context)
            else:
                return super(sale_order,self).action_invoice_create( cr, uid, ids, grouped=grouped, states=states, date_invoice = date_invoice, context=context)
    

    def cmpt_disc(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
	prov_totals = 0

        for r in records:

		for entry in r.order_line:
			prov_totals = prov_totals+(entry.price_unit * entry.product_uom_qty)
		result[r.id] = prov_totals

       	return result


    def cmpt_disc2(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
	prov_totals = 0
	ss_tots=0
        for r in records:

		for entry in r.order_line:
			ss_tots = entry.price_subtotal + ss_tots
			prov_totals = prov_totals + (entry.price_unit * entry.product_uom_qty)
		result[r.id] = prov_totals-ss_tots

       	return result
 
    def cmpt_disc_str(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
		result[r.id] = str(r.discount) + " %"

       	return result



    def cmpt_after(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
		result[r.id] = r.total_before_disc - r.discu

       	return result


    def cmpt_inv_num(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
            so = self.pool.get('account.invoice').search(cr,uid,[('origin','=',r.name),('state','!=','cancel')])

            if(len(so) >= 1):
                result[r.id] = self.pool.get('account.invoice').browse(cr,uid,so[0]).internal_number
            else:
                result[r.id] = False

        return result
 
    def cmpt_shop_shadow(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
            result[r.id] = r.shop_id.id

        return result
 
    _columns = {
		'first_inv':fields.function(cmpt_inv_num,type='char',method=True,string="Num. Factura"),
		'str_disc':fields.function(cmpt_disc_str,type='char',method=True,string="Descuento"),	
		'total_before_disc':fields.function(cmpt_disc, type='float', method=True, string='Subtotal'),	
		'discu':fields.function(cmpt_disc2, type='float', method=True, string='Descuento ($)'),
                'client_order_ref': fields.char('C.I./RUC', size=64),
		#'discount_display':fields.float('Descuento (%)',digits_compute=dp.get_precision('Product Price'), readonly=True),
	      
		'total_after_disc':fields.function(cmpt_after,type='float',method=True,string="Antes IVA"),	
          'discount': fields.float('Descuento (%)', digits_compute=dp.get_precision('Product Price')),
                 'transporte' : fields.char("Transporte",size=64),       
		 'vat': fields.many2many('account.tax', 'sale_order_vat_rel', 'tax_id', 'order_id', 'I.V.A', readonly=True),
         'shop_shadow': fields.function(cmpt_shop_shadow, type='many2one', relation='sale.shop', string="Tienda"),
		
                }
    
    def manual_invoice(self, cr, uid, ids, context=None):
        """ create invoices for the given sales orders (ids), and open the form
            view of one of the newly created invoices
        """
        if 'retail_type' in context:
            if context['retail_type'] == 'quick':
                return super(sale_order, self).manual_invoice(cr,uid,ids,context)

        res = super(sale_order, self).manual_invoice(cr,uid,ids,context)
        inv_obj = self.pool.get('account.invoice').browse(cr,uid,res['res_id'])
        
        #printer = inv_obj.printer_id.id

        #pos_config_id = self.pool.get('pos.config').search(cr,uid,[('sri_printer_point_id','=',printer)])
        #pos_config = self.pool.get('pos.config').browse(cr,uid,pos_config_id)[0]
        #TODO MAKE SURE THIS IS THE CORRECT METHOD.  ALSO ADD NOTAS DE CREDITO
        
        #sequence = inv_obj.printer_id.invoice_sequence_id.id
         
        #number = self.pool.get('ir.sequence').next_by_id(cr, uid, sequence, context)
        
        #inv_obj.write({'internal_number': number})
        #inv_obj.write({'number':number})
        
        if "fixed_date" in context:
            inv_obj.write({'date_invoice':context['fixed_date']})
        else:
            inv_obj.write({'date_invoice':time.strftime('%Y-%m-%d')})

        return res
    
#     def manual_invoice(self,cr,uid,ids, context=None):
# 
#         invoice_res = sale_obj.manual_invoice(cr, user, sale_ids, context)
#         invoice_list = [invoice_res.get('res_id')]
#         self.pool.get('account.invoice').write(cr, user, invoice_list, {'retail_type':'quick'})
#         avl_obj = self.pool.get('account.voucher.line')
#         if invoice_list:
#             wf_service.trg_validate(user, 'account.invoice',
#                                     invoice_list[0], 'invoice_open', cr)


    def action_button_confirm(self, cr, uid, ids, context=None):
        my_sale_order = self.pool.get('sale.order').browse(cr,uid,ids[0])
        results = {}
        list_product_id={}
        # if len(my_sale_order.order_line) > 30:
        #         raise osv.except_osv(
        #                 _('Invalid Action!'),
        #                 _('Enter 30 or less rows'))

        for l in my_sale_order.order_line:
                if l.product_uom_qty>l.product_id.qty_available:
                        raise osv.except_osv(
                                _('ERROR'),
                                _('Hay producto(s) que excede la existencia'))
                        
                if l.product_id.id not in list_product_id:
                    list_product_id[l.product_id.id]=l.product_uom_qty
                else:
                    list_product_id[l.product_id.id]=list_product_id[l.product_id.id]+l.product_uom_qty
                    
        for p in  self.pool.get('product.product').browse(cr,uid,list_product_id.keys()):
            if list_product_id[p.id]>p.qty_available:
                raise osv.except_osv(
                        _('ERROR'),
                        _('Hay producto(s) que excede la existencia\nProducto: '+ p.default_code + ' solo tiene existencias de: '+ str(p.qty_available)))
             

        res = super(sale_order, self).action_button_confirm(cr,uid,ids,context)

        for p in my_sale_order.picking_ids:
            self.pool.get('stock.picking').action_assign(cr,uid,[p.id],[])

        return res



    def convert_into_cancelled(self,cr,uid,ids,context={}):
	my_sale_order = self.pool.get('sale.order').browse(cr,uid,ids[0])
	my_sale_order.write({'state':'cancel'})



    def discount_change(self,cr,uid,ids,nd, context=None):


	_logger.debug("XXX")		

	for so in self.pool.get('sale.order').browse(cr,uid,ids):

		_logger.debug(so.name)
		for l in so.order_line:
			l.write({'discount':nd})

	return {'value':{}}

    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        if not part:
            return {'value': {'partner_invoice_id': False, 'partner_shipping_id': False,  'payment_term': False, 'fiscal_position': False}}

        part = self.pool.get('res.partner').browse(cr, uid, part, context=context)
        addr = self.pool.get('res.partner').address_get(cr, uid, [part.id], ['delivery', 'invoice', 'contact'])

        payment_term = part.property_payment_term and part.property_payment_term.id or False
        fiscal_position = part.property_account_position and part.property_account_position.id or False
        dedicated_salesman = part.user_id and part.user_id.id or uid
        val = {
            'partner_invoice_id': addr['invoice'],
            'partner_shipping_id': addr['delivery'],
            'payment_term': payment_term,
            'fiscal_position': fiscal_position,
            #'user_id': dedicated_salesman,
            'discount': part.discount,
	       #'discount_display': part.discount,
            'client_order_ref': part.vat[2:] if part.vat[:2] == "EC" else part.vat,
        }
        taxes = []

        for tax in part.vat_tax:
            taxes.append(tax.id)
        val['vat'] = [(6,0, taxes)]

        if taxes:
            return {'value': val, 'domain': {'vat': [('id', 'in', taxes)]}}
        return {'value': val}

    def _get_default_shadow(self, cr, uid, context=None):

        user_obj = self.pool.get('res.users')
        user = user_obj.browse(cr, uid, uid, context)
        if user.printer_id:
            if user.printer_id.shop_id:
                return user.printer_id.shop_id.id

    _defaults = {
                 'shop_shadow':_get_default_shadow,
                 }

    
sale_order()


class sale_order_line(osv.osv):

    _inherit = 'sale.order.line'



    def cpy_over(self, cr, uid, ids, field_name, field_value, arg, context=None):
	records = self.browse(cr, uid, ids)
        result = {}
	prov_totals = 0

        for r in records:
		result[r.id] = r.price_unit*r.product_uom_qty

       	return result


    _columns = {
		'old_price':fields.function(cpy_over, type='float', method=True, string='Subtotal'),
		'the_date':fields.date("Fecha"),
		'p_ref' : fields.related("product_id", 'sup_ref', type="char",string="Referencia", readonly=True),
                }
    def product_id_change(self, cr, uid, ids, pricelist, product, qty=0,
            uom=False, qty_uos=0, uos=False, name='', partner_id=False,
            lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False, flag=False, context=None):

	first_result=super(sale_order_line,self).product_id_change(cr, uid, ids, pricelist, product, qty,
            uom, qty_uos, uos, name, partner_id,
            lang, update_tax, date_order, packaging, fiscal_position, True, context)
	if product and (not('isqty' in context)):
		first_result['value']['name'] = self.pool.get('product.product').browse(cr,uid,product).name
		first_result['value']['p_ref'] = self.pool.get('product.product').browse(cr,uid,product).sup_ref
	
	return first_result

    def get_taxes(self, cr, uid, context={}):
    
        partner_id = context['partner_id'] if context and 'partner_id' in context else False
    
        if partner_id == False :
            return []
    
        else:
            t = self.pool.get('res.partner').get_tax(cr,uid,partner_id,context=context)
            if t == -11:
                return []
            else:
                return [(6,0,[t])]

    def get_discount(self, cr, uid, context={}):
        print context
        partner_id = context['sales_order'] if context and 'sales_order' in context else False
        if partner_id == False :
            return 0
        else:
            return context['sales_order']

    def _get_good_date(self,cr,uid,context=None):
        now_date = fields.date.context_today(self, cr, uid, context=context)
        return now_date

    _defaults = {
                 'the_date': _get_good_date,
		'tax_id': get_taxes,
        'discount':get_discount
                 }

    def create(self, cr, uid, vals, context={}):
	_logger.debug("BBB")
	_logger.debug(context)
	return super(sale_order_line, self).create(cr, uid, vals, context)

sale_order_line()

class account_voucher(osv.osv):
    _inherit = 'account.voucher'


    def _get_writeoff_amount2(self, cr, uid, ids, name, args, context=None):
        if not ids: return {}
        currency_obj = self.pool.get('res.currency')
        res = {}
        
        for voucher in self.browse(cr, uid, ids, context=context):
            debit = credit = 0.0
            sign = voucher.type == 'payment' and -1 or 1
            for l in voucher.line_dr_ids:
                debit += l.amount
            for l in voucher.line_cr_ids:
                credit += l.amount
            currency = voucher.currency_id or voucher.company_id.currency_id
            res[voucher.id] =  currency_obj.round(cr, uid, currency, voucher.amount - sign * (credit - debit))
        return res
    
    _columns = {
                'codigo_partner_id':fields.related('partner_id','codigo',type='char', string='Codigo de Cliente', readonly=True),
		'dep_date':fields.date('Fecha de Registro'),
                'memo_2':fields.char("Banco"),
                'memo_3':fields.char("Numero de cheque"),
                'write_uid':fields.many2one('res.users', string="Hecha Por"),
                'writeoff_amount2': fields.function(_get_writeoff_amount2, string='Difference Amount', type='float', readonly=True, help="Computed as the difference between the amount stated in the voucher and the sum of allocation on the voucher lines."),
                }

    def _get_good_date(self,cr,uid,context=None):
        now_date = fields.date.context_today(self, cr, uid, context=context)
        return now_date

    _defaults = {
                    'dep_date':_get_good_date,
                }

    _order = 'dep_date desc'

class account_voucher_line(osv.osv):
    _inherit = 'account.voucher.line'
    _columns = {
		'old_num':fields.related('move_line_id','invoice','old_num',type='char',string="Antiguo",readonly=True),
		}

    def onchange_move_line_id(self, cr, user, ids, move_line_id, context=None):
        """
        Returns a dict that contains new values and context

        @param move_line_id: latest value from user input for field move_line_id
        @param args: other arguments
        @param context: context arguments, like lang, time zone

        @return: Returns a dict which contains new values, and context
        """
        res = super(account_voucher_line,self).onchange_move_line_id(cr,user,ids,move_line_id,context)
        move_line_pool = self.pool.get('account.move.line')
        if move_line_id:
            res.update({
                'old_num': move_line.invoice.old_num,
            })
        return {
            'value':res,
        }

 
