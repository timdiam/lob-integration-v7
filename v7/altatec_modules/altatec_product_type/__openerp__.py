{
    'name': 'Altatec Product Type',
    'version': '1.0',
    'description': """
        Altatec Product Type Addons
    """,
    'author': 'Harry Alvarez',
    'website': 'www.altatececuador.com',
    "depends" : [
                    'product',
                    'hr_expense',
                ],
    "data" : [
                'altatec_product_type_view.xml',
                'altatec_default_product_type.xml',
                "security/ir.model.access.csv",

             ],
    "installable": True,
    "auto_install": False
}
