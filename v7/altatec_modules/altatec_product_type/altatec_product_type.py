import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale

from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging

_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL, 'en_US.utf8')


class product_product(osv.osv):

    _inherit="product.product"

    def _get_odoo_product_type(self, control_stock, stock_negativo):
        tipo=""
        if (control_stock==True and stock_negativo==True):
            tipo="consu"
        elif (control_stock==True and stock_negativo==False):
            tipo="product"
        elif (control_stock==False):
            tipo="service"
        else:
            raise osv.except_osv( _( 'Error' ), _( 'Invalid configuracion de producto' ) )

        return tipo


    def onchange_product_type(self,cr, uid, ids, altatec_product_type,context={}):
        product_type= self.pool.get('altatec.product.type').browse(cr,uid,altatec_product_type)
        prueba=product_type.puede_ser_vendido

        tipo= self._get_odoo_product_type(product_type.control_stock,product_type.stock_negativo)

        return {"value":{"supply_method":product_type.tipo_movimiento,
                         "procure_method":product_type.tipo_abastecimiento,
                         "type":tipo,
                         "sale_ok": product_type.puede_ser_vendido,
                         'purchase_ok':product_type.puede_ser_comprado,
                         'cost_method':product_type.metodo_costeo,
                         'valuation':product_type.valoracion} }




    def write(self, cr, uid, ids, vals, context=None):

        for record in self.browse(cr,uid,ids, context=context):
            if (record.altatec_product_type and ('altatec_product_type' in vals.keys())):
                raise osv.except_osv(_('Accion Invalida!'), _('Tipo de Producto no es modificable'))

        return super(product_product, self).write(cr, uid, ids, vals, context=context)

    def get_category_code(self, cr, uid, category_id, context={}):
        category_list = []
        category_obj = self.pool.get('product.category')

        #first we build a list of ids of categories going from child --> parent,
        #then we reverse the list to build the code
        while(True):
            category_list.append(category_id)
            cat = category_obj.browse(cr,uid,category_id,context=context)
            # if cat.parent_id:
            if cat.parent_id:
                category_id = cat.parent_id.id
            else:
                break

        category_list.reverse()
        result = ""
        for category in category_obj.browse(cr,uid,category_list,context=context):
            if category.codigo_ec:
                result += '-'+category.codigo_ec

        #To remove leading and trailing '-', if there are any...
        result = result.strip('-')

        return result

    def _get_default_altatec_type(self,cr,uid,context={}):
        data_obj = self.pool.get('ir.model.data').get_object_reference(cr,uid,'altatec_product_type','altatec_default_product_type_1')
        if len(data_obj) >= 2:
            product_type_id = data_obj[1]
        else:
            raise osv.except_osv( _( 'Error' ), _( 'Falta un xml id por default que se llama altatec_default_product_type_1.  Consulte con Altatec' ) )
        return product_type_id

    def create(self, cr, uid, vals, context={}):
        bkp=True
        if "altatec_product_type" not in vals:
            product_type_id = self._get_default_altatec_type(cr,uid,context=context)

        else:
            product_type_id = vals['altatec_product_type']

        product_type= self.pool.get('altatec.product.type').browse(cr,uid,product_type_id)

        if 'categ_id' in vals:
            category=vals['categ_id']
            category_code = self.get_category_code(cr,uid,vals['categ_id'], context=context)
        else:
            category=self.pool.get("product.category").search(cr,uid,[])[0]
            category_code = self.get_category_code(cr,uid,category, context=context)
            vals.update({'categ_id':category})
        referencia=self.pool.get('ir.sequence')._next(cr, uid, [product_type.secuencia.id], context=context)
        tipo= self._get_odoo_product_type(product_type.control_stock,product_type.stock_negativo)
        vals.update({"supply_method":product_type.tipo_movimiento,
                     "procure_method":product_type.tipo_abastecimiento,
                     "default_code":referencia,
                     'sale_ok':product_type.puede_ser_vendido,
                     'purchase_ok':product_type.puede_ser_comprado,
                     'cost_method':product_type.metodo_costeo,
                     'type':tipo,
                     'valuation':product_type.valoracion,
                    })


        vals.update({'codigo_categoria4':category_code})
        _logger.debug(vals)	
        return super(product_product, self).create(cr, uid, vals, context)

    _columns={
                "modelo":fields.char(size=20, string="Modelo"),
                'default_code':fields.char('Internal Reference', size=64, readonly="1"),
                "codigo_categoria4":fields.char(string="Codigo de Categoria", readonly="1"),
                "altatec_product_type":fields.many2one("altatec.product.type", required=True, string="Codigo Altatec"),
                'sale_ok':fields.boolean('Puede ser Vendido', readonly="1"),
                'purchase_ok':fields.boolean('Puede ser comprado', readonly="1"),
                'hr_expense_ok':fields.boolean('Puede ser un gastoaa', readonly="1"),
                'procure_method':fields.selection([('make_to_stock','Obtener para Stock'),('make_to_order','Obtener bajo Pedido')], readonly="1", string='Tipo de Abastecimiento', required=True, help="Make to Stock: When needed, the product is taken from the stock or we wait for replenishment. \nMake to Order: When needed, the product is purchased or produced."),
                'supply_method':fields.selection([('produce','Fabricar'),('buy','Comprar')], readonly="1", string='Tipo de Movimiento', required=True, help="Manufacture: When procuring the product, a manufacturing order or a task will be generated, depending on the product type. \nBuy: When procuring the product, a purchase order will be generated."),
                'cost_method': fields.selection([('standard','Precio estandar'), ('average','Precio medio')], 'Metodo de Coste', readonly="1",
                        help="Standard Price: The cost price is manually updated at the end of a specific period (usually every year). \nAverage Price: The cost price is recomputed at each incoming shipment."),
                'type': fields.selection([('consu', 'Cosumible'),('service','Servicio'),('product', 'Almacenable'),], 'Product Type',readonly="1", required=True, help="Consumable are product where you don't manage stock, a service is a non-material product provided by a company or an individual."),
                'valuation':fields.selection([('manual_periodic', 'Periodical (manual)'),
                                        ('real_time','Real Time (automated)'),], 'Inventory Valuation',
                                        help="If real-time valuation is enabled for a product, the system will automatically write journal entries corresponding to stock moves." \
                                             "The inventory variation account set on the product category will represent the current inventory value, and the stock input and stock output account will hold the counterpart moves for incoming and outgoing products."
                                        , required=True, readonly=True),

                # 'categ_id': fields.many2one('product.category','Categoria', required=True, change_default=True, domain="[('type','=','normal')]" ,help="Select category for the current product"),
            }

    _defaults = {
    #                'altatec_product_type':_get_default_altatec_type,
                    'categ_id':None
    }

class product_category(osv.osv):
    _inherit = "product.category"


    _columns={
                "codigo_ec":fields.char(string="Codigo", size=10, required=True),
    }

    _sql_constraints = [
        ('codigo_ec_uniq', 'unique(codigo_ec)', 'Codigo Debe ser Unico!'),
    ]


class altatec_product_type(osv.osv):
    _name="altatec.product.type"

    def create(self, cr, uid, vals, context={}):
        prueba=len(str(vals['numero_inicial']))
        vals.update({'field_id':True})
        if vals['padding']<(len(str(vals['numero_inicial']))):
            raise osv.except_osv(_('Accion Invalida!'), _('Padding deber ser Mayor que el de digitios de Numero Inicial'))

        sequence_id=self.pool.get('ir.sequence').create(cr,uid,{
                                                                    'name':'Altatec Product Type',
                                                                    'prefix':vals['digito_inicial'],
                                                                    'implementation':'standard',
                                                                    'number_next_actual':vals['numero_inicial'],
                                                                    'number_next':vals['numero_inicial'],
                                                                    'number_increment':1,
                                                                    'padding':vals['padding'],
                                         })
        vals['secuencia']=sequence_id

        return super(altatec_product_type, self).create(cr, uid, vals, context)

    def update_products(self, cr, uid, ids, context={}):
        if len(ids) > 1:
            raise osv.except_osv( _( 'ERROR' ), _( 'No se puede ejectuar por varias tipos de productos' ) )

        prod_obj = self.pool.get('product.product')
        for product_type in self.browse(cr, uid, ids, context=context):
            prod_ids = prod_obj.search(cr,uid,[('altatec_product_type','=',product_type.id)])
            vals = {}
            tipo= prod_obj._get_odoo_product_type(product_type.control_stock,product_type.stock_negativo)
            vals.update({"supply_method":product_type.tipo_movimiento,
                     "procure_method":product_type.tipo_abastecimiento,
                     'sale_ok':product_type.puede_ser_vendido,
                     'purchase_ok':product_type.puede_ser_comprado,
                     'cost_method':product_type.metodo_costeo,
                     'valuation':product_type.valoracion,
                    'type':tipo,
                    })
            prod_obj.write(cr,uid,prod_ids,vals,context=context)

        return True

    _columns={

                "active":fields.boolean("Activo"),
                "field_id":fields.char("token"),
                "name":fields.char(string="Nombre"),
                "descripcion":fields.char(string="Descripcion"),
                "puede_ser_vendido":fields.boolean(string="Puede ser Vendido"),
                "puede_ser_comprado":fields.boolean(string="Puede ser Comprado"),
                "control_stock":fields.boolean(string="Control Stock"),
                "stock_negativo":fields.boolean(string="Permitir Stock Negativo"),
                "metodo_costeo":fields.selection([('standard','Precio estandar'), ('average','Precio medio')], 'Metodo de Coste'),
                'valoracion':fields.selection([('manual_periodic', 'Periodical (manual)'),('real_time','Real Time (automated)'),], 'Valoracion',),
                "digito_inicial":fields.char(string="Prefijo"),
                "numero_inicial":fields.integer(string="Numero Inicial"),
                "padding":fields.integer(string="Padding"),
                "secuencia":fields.many2one("ir.sequence",string="Secuencia", readonly="1"),
                'tipo_abastecimiento':fields.selection([('make_to_stock','Obtener para Stock'),('make_to_order','Obtener bajo Pedido')], 'Tipo de Abastecimiento', required=True),
                'tipo_movimiento':fields.selection([('produce','Fabricar'),('buy','Comprar')], 'Tipo de Movimiento', required=True),
    }

    _defaults = {
                'active':True,
    }

class line_product_reclassification(osv.osv):

    _name="line.product.reclassification"

    _columns={
                "producto":fields.many2one("product.product", string="Producto"),
                "codigo_antiguo":fields.char(string="Codigo Antiguo"),
                "codigo_nuevo":fields.char(string="Codigo Nuevo"),
                "product_reclassification":fields.many2one("product.reclassification", string="Producto Reclasificado"),
            }


class product_reclassification(osv.osv):

    _name="product.reclassification"

    def confirm_reclassification(self,cr,uid,ids,context):
        records = self.browse(cr, uid, ids)[0]
        bkp=True
        for line in records.line_reclassification:
            probando=line.producto.id
            codigo_actual=self.pool.get('product.product').get_category_code(cr,uid,line.producto.categ_id.id,context=context)
            if codigo_actual!=line.codigo_nuevo:
                bkp=False
                raise osv.except_osv(_('Accion Invalida!'), _('El codigo ha sido modificado, no se puede reclasificar'))
            if bkp==True:
                line.producto.write({'codigo_categoria4':line.codigo_nuevo})

        records.write({'state':'confirm','fecha_confirmar':datetime.now()})

        return


    def cancel_reclassification(self,cr,uid,ids,context):
        records = self.browse(cr, uid, ids)[0]
        bkp=True
        for line in records.line_reclassification:
            probando=line.producto.id

            codigo_actual=self.pool.get('product.product').get_category_code(cr,uid,line.producto.categ_id.id,context=context)
            if codigo_actual!=line.codigo_nuevo:
                bkp=False
                raise osv.except_osv(_('Accion Invalida!'), _('El codigo ha sido modificado, no se puede reclasificar'))
            if bkp==True:
                line.producto.write({'codigo_categoria4':line.codigo_antiguo})


        records.write({'state':'cancel'})
        return

    def _get_user(self, cr, uid, context):
        x = 1
        return self.pool.get('res.users').browse(cr, uid, uid, context).id

    _columns={
                "fecha_confirmar":fields.datetime(string="Fecha de Confirmacion",readonly="1"),
                "fecha":fields.date("Fecha", readonly="1"),
                "usuario":fields.many2one("res.users", string="Usuario"),
                "state":fields.selection([('draft',"Borrador"),('confirm','Confirmado'),('cancel','Anulado')],string='Estado'),
                "line_reclassification":fields.one2many("line.product.reclassification","product_reclassification", string="Linea" ),
            }

    _defaults={
                'state':'draft',
                'usuario':_get_user,

    }


class product_recla_wizard(osv.osv):
    _name="product.recla.wizard"

    def make_codigo_categoria(self,cr,uid,ids,context=None):

        products=self.pool.get('product.product').search(cr, uid, [])
        productos=self.pool.get('product.product').browse(cr,uid,products)
        product_reclasi_id=self.pool.get('product.reclassification').create(cr,uid,{
                                                                                "fecha":time.strftime('%Y-%m-%d'),
                                                                                    })
        for p in productos:
            codigo_nuevo=self.pool.get('product.product').get_category_code(cr,uid,p.categ_id.id,context=context)
            if codigo_nuevo!=p.codigo_categoria4:
                line_reclassi_id=self.pool.get('line.product.reclassification').create(cr,uid,{
                                                                                "producto":p.id,
                                                                                "codigo_antiguo":p.codigo_categoria4,
                                                                                "codigo_nuevo":codigo_nuevo,
                                                                                "product_reclassification":product_reclasi_id,
                                          })


        view = self.pool.get("ir.ui.view").search(cr,uid,[("name","=","altatec.product.reclassification.form")])[0]

        return {
                'type':'ir.actions.act_window',
                'name':'Reclasificacion de Productos',
                'view_mode':'form',
                'view_type':'form',
                'view_id':view,
                "res_id" : product_reclasi_id,
                'res_model': 'product.reclassification',
                'nodestroy':'true',
                }



