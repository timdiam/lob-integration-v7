{
    'name': 'Altatec Skip Wizard',
    'version': '1.0',
    'description': """
        Reporte para novedades de empleados
    """,
    'author': 'Juan Romero',
    'website': 'www.altatececuador.com',
    "depends" : [
                'sale',



                ],
    "data" : [
                'altatec_sale_wizard_skip.xml'
             ],
    "installable": True,
    "auto_install": False
}

