################################################################################################
#
# This file contains the model for the General Payment Order (Orden de Pago) and its wizard.
# This module allows a user to reconcile multiple invoices with one payment.
#
# Author:  Tim Diamond
# Company: AltaTec Ecuador
# Date:    May. 8th, 2015
#
################################################################################################
from   openerp.osv             import fields, osv, orm
from   openerp.tools.translate import _

################################################################################################
# General Payment Order class definition
################################################################################################

class account_cross_debit(osv.osv):

    _name="account.cross.debit"

    _columns={
        "name":fields.char("Nombre"),
        "journal_id":fields.many2one("account.journal", "Diario"),
        "period_id":fields.many2one("account.period", "Periodo"),
        "move_id":fields.many2one("account.move", "Asiento Contable"),
        "account_cross_id":fields.many2one("account.cross", "Cruce de Cuentas"),
        "account_id":fields.many2one("account.account", "Cuenta"),
        "debit":fields.float("Debe"),
        }

class account_cross_credit(osv.osv):

    _name="account.cross.credit"

    _columns={
        "name":fields.char("Nombre"),
        "journal_id":fields.many2one("account.journal", "Diario"),
        "period_id":fields.many2one("account.period", "Periodo"),
        "move_id":fields.many2one("account.move", "Asiento Contable"),
        "account_cross_id":fields.many2one("account.cross", "Cruce de Cuentas"),
        "account_id":fields.many2one("account.account", "Cuenta"),
        "credit":fields.float("Haber"),
        }


class account_cross(osv.osv):

    _name = 'account.cross'

    def search_account_move_line(self,cr,uid,ids,context=None):
        cross = self.browse(cr,uid,ids,context)[0]
        move_line_pool = self.pool.get('account.move.line')

        #####################################################
        # Se llenan los argumentos dependiendo de los campos que se llenan en la vista form
        ####################################################
        arg=[]

        arg.append((('partner_id', '=', cross.partner_id.id)))

        if (cross.period_id.id)!=False:
            arg.append((('periodo_id', '>', cross.period_id)))

        move_line_ids = move_line_pool.search( cr,uid,arg)
        move_lines=move_line_pool.browse(cr,uid,move_line_ids)

        credit=[]
        debit=[]

        ####################################################
        # Se separan los account.move.line por Debe y Haber
        ####################################################

        for line in move_lines:
            if (line.credit!=0 and line.debit==0):
                credit.append(line)
            if (line.credit==0 and line.debit!=0):
                debit.append(line)
                # if (line.credit!=0 and line.debit!=0):
                #     raise osv.except_osv(_('Invalid Action!'), _('No puede tener las dos cuentas con valores'))

        ####################################################
        # Creacion de los account.cross.credit con la lista que de credit
        ####################################################

        for c in credit:
            new_line_cross_credit=self.pool.get('account.cross.credit').create(cr,uid,{
                "name":c.name,
                "account_id":c.account_id.id,
                "journal_id":c.journal_id.id,
                "period_id":c.period_id.id,
                "move_id":c.move_id.id,
                "account_cross_id":ids[0],
                "credit":c.credit,
                }),


        ####################################################
        # Creacion de los account.cross.debit con la lista que de debit
        ####################################################

        for d in debit:
            new_line_cross_credit=self.pool.get('account.cross.debit').create(cr,uid,{
                "name":d.name,
                "account_id":d.account_id.id,
                "journal_id":d.journal_id.id,
                "period_id":d.period_id.id,
                "move_id":d.move_id.id,
                "account_cross_id":ids[0],
                "debit":d.debit,
                }),

        return 0

    def create_account_cross(self,cr,uid,ids,context=None):
        return 0



    ################################################################################################
    # Unlink method override
    ################################################################################################
    def unlink(self, cr, uid, ids, context=None):
        records = self.browse(cr,uid,ids,context)
        for r in records:
            if r.state != 'anulado':
                raise osv.except_osv(_('Invalid Action!'), _('No puedes borrar un orden de pago en el estado realizado'))
        return super(orden_de_pago_general,self).unlink(cr, uid, r.id, context=context)


    ################################################################################################
    # Cancelar Orden de Pago button. Unreconcile every line
    ################################################################################################
    def cancelar_account_cross(self,cr,uid,ids,context=None):

        reconcile_pool = self.pool.get('account.move.reconcile')
        move_pool      = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')

        for record in self.browse(cr,uid,ids):

            if( record.withhold_id ):
                raise osv.except_osv( "Error!", "No se puede anular un orden de pago que ya tiene una retencion." )

            for orden_line in record.lineas_de_pago:
                if orden_line.contrapartida.reconcile_id:
                    reconcile_pool.unlink(cr,uid,[orden_line.contrapartida.reconcile_id.id])
                elif orden_line.contrapartida.reconcile_partial_id:
                    reconcile_pool.unlink(cr,uid,[orden_line.contrapartida.reconcile_partial_id.id])

            move_pool.button_cancel(cr,uid,[record.move_id.id],context=context)
            move_pool.write(cr,uid,[record.move_id.id],{'ref':'ANULADO'},context=context)

            for line in record.move_id.line_id:
                move_line_pool.unlink(cr,uid,[line.id],context=context)

            self.write(cr,uid,[record.id], {'state':'anulado'})

        return True


    ################################################################################################
    # Columns definitions
    ################################################################################################
    _columns = {
        'partner_id': fields.many2one('res.partner',string="Persona a Reconciliar"),
        "period_id": fields.many2one("account.period", string="Periodo"),
        'line_debit' : fields.one2many('account.cross.debit','account_cross_id',string='Debe', ),
        'line_credit' : fields.one2many('account.cross.credit','account_cross_id',string='Haber', ),

        # 'name'           : fields.char('Nombre / Razon' ,required=True),
        # "type"           : fields.selection( [ ( "caja", "Caja / Anticipos" ),
        #                                        ( "nomina" , "Beneficios del Nomina"  ),
        #                                        ( "otro"           , "Otro"          ),
        #                                        ], "Tipo", required=True
        #                                      ),
        # 'diario'         : fields.related('move_id','journal_id',type='many2one',relation='account.journal', string="Diario", required=True ),
        # 'periodo'        : fields.related('move_id', 'period_id',type='many2one',relation='account.period', string = "Periodo", required=True ),
        # 'date'           : fields.related('move_id','date', type='date', string='Fecha'),
        #
        # 'move_id'        : fields.many2one('account.move',string='Asiento contable',),
        # 'lineas_de_cruce' : fields.one2many('account.cross.line','account_cross_id',string='Lineas de Cruce', ),
        # 'state'          : fields.selection([('realizado','Realizado'), ('anulado','Anulado')], 'Estado', size=32 ),
    }


################################################################################################
# General Payment Order line class definition
################################################################################################
class account_cross_line(osv.osv):

    _name = 'account.cross.line'


    ################################################################################################
    # Column definitions
    ################################################################################################
    _columns = {
        'account_cross_id'      : fields.many2one('account.cross', string='Documento Cruce de Cuenta'),
        'partner_id'            : fields.many2one('res.partner', string='Empresa / Persona'),
        'counterpart'           : fields.many2one('account.move.line',string='Contrapartida'),
        'cross_line'            : fields.many2one('account.move.line', string='Linea del Cruce'),
        'name'                  : fields.char("Descripcion"),
        'reconcile_id'          : fields.related('contrapartida','reconcile_id',type='many2one',relation='account.move.reconcile',string='ID de Reconciliacion'),
        'reconcile_partial_id'  : fields.related('contrapartida','reconcile_partial_id',type='many2one',relation='account.move.reconcile',string='ID de part. Reconciliacion'),
        }