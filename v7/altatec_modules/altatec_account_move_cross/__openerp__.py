{   'name': 'Account Move Crossing',
    'version': '1.0',
    'description': """
    This module adds a new wizard to aid in the conciliacion / crossing of account moves.
    """,
    'author': 'Tim Diamond & Harry Alvarez',
    'website': 'www.altatececuador.com',
    "depends" : [ 'account',
                  'web_hide_duplicate',
                ],
    "data" : [ 'views/account_cross.xml',
               'security/ir.model.access.csv',
             ],
    "installable": True,
    "auto_install": False
}