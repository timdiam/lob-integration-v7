# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors: Carlos Yumbillo                                                                         
#Copyright (C) 2013 TRESCLOUD CIA. LTDA.                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import fields,osv
import decimal_precision as dp
from tools.translate import _
from trc_mod_python import amount_to_words_spanish

class stock_picking(osv.osv):
    
    _inherit = "stock.picking"
    
    def _suggested_tax_support(self, cr, uid, context=None):
        '''valor por defecto para el sustento tributario en documentos nuevos
        '''
        
        tax_support_id = None
        #las guias de remision no tienen sustento tributario
        return tax_support_id
   
    _columns = {
        'sri_tax_support_id': fields.many2one('sri.tax.support', 'Tax Support', help='Indicates the tax support for this document.',),
    }
   
    _defaults = {
        'sri_tax_support_id': _suggested_tax_support,        
    }
    
stock_picking()
