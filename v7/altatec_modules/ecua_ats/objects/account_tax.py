from osv import fields,osv

class account_tax_code(osv.osv):

    _inherit = "account.tax.code"

    _columns = {
                'tax_code_lookup_ats':fields.one2many('account.tax.lookup.ats','tax_code_id',string='Codigos ATS'),
                'tax_code_lookup_103':fields.one2many('account.tax.lookup.103','tax_code_id',string='Codigo 103'),
                'tax_code_lookup_104':fields.one2many('account.tax.lookup.104','tax_code_id',string='Codigo 104'),
    }

class account_tax_lookup_ats(osv.osv):

    _name='account.tax.lookup.ats'

    _columns = {
                'tax_code_id':fields.many2one('account.tax.code',string="Tax Code"),
                'date_start':fields.date('Fecha Inicio', required=True),
                'date_end' :fields.date('Fecha Fin', required=True),
                'code':fields.char('Codigo', required=True),
    }

class account_tax_lookup_103(osv.osv):

    _name='account.tax.lookup.103'

    _columns = {
                'tax_code_id':fields.many2one('account.tax.code',string="Tax Code"),
                'date_start':fields.date('Fecha Inicio', required=True),
                'date_end' :fields.date('Fecha Fin', required=True),
                'code':fields.char('Codigo', required=True),
    }

class account_tax_lookup_104(osv.osv):

    _name='account.tax.lookup.104'

    _columns = {
                'tax_code_id':fields.many2one('account.tax.code',string="Tax Code"),
                'date_start':fields.date('Fecha Inicio', required=True),
                'date_end' :fields.date('Fecha Fin', required=True),
                'code':fields.char('Codigo', required=True),
    }

