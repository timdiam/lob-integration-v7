# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors: Carlos Yumbillo                                                                         
#Copyright (C) 2013 TRESCLOUD Cia. Ltda.                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import fields,osv

class account_voucher(osv.osv):
    
    _inherit = "account.voucher"
    
    _columns = {
        'payment_method_id': fields.many2one('account.journal.payment.method', 'Payment Method', help='Indicates the payment method for this voucher.',)
    }
    
    def onchange_journal(self, cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount, ttype, company_id, context=None):

        '''
        Función que muestra las formas de pago de acuerdo al diario escogido. Hereda la función onchange_journal del objeto account.voucher.
        Retorna un diccionario que contiene nuevos valores y el contexto, ademas le asigna a la clave payment_method_id por defecto
        el primer valor de la lista payment_method.
        '''
        vals = {'value':{} }

        if journal_id:
            vals = super(account_voucher, self).onchange_journal(cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount, ttype, company_id, context=context)
            journal = self.pool['account.journal'].browse(cr, uid, journal_id, context)

            # TODO: Cambiar las iteraciones por una funcion search de openerp
            if journal.payment_method_ids:
                payment_method = []
                for item in journal.payment_method_ids:
                    payment_method.append(item.id)

                if vals.has_key('domain'):
                    # TODO: Falta verificar este escenario con datos reales
                    vals['domain'].update({'payment_method_id': [('id', 'in', payment_method)]})
                else:
                    vals['domain'] = {'payment_method_id': [('id', 'in', payment_method)]}

                vals['value'].update({'payment_method_id': payment_method[0]})
            else:
                vals['value'].update({'payment_method_id': False})
                vals['domain'] = {'payment_method_id': [('id', 'in', [])]}
        else:
            vals['value'].update({'payment_method_id': False})
            vals['domain'] = {'payment_method_id': [('id', 'in', [])]}

        return vals
    
account_voucher()
