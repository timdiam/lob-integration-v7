# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors: Carlos Yumbillo                                                                         
#Copyright (C) 2013 TRESCLOUD CIA. LTDA.                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import fields,osv
import decimal_precision as dp
from tools.translate import _
from trc_mod_python import amount_to_words_spanish

class account_invoice(osv.osv):
    
    _inherit = "account.invoice"
    
    def _suggested_tax_support(self, cr, uid, context=None):
        '''valor por defecto para el sustento tributario en documentos nuevos
        '''
        
        tax_support_id = None
        if context and 'type' in context:

            if context['type'] in ['out_invoice','out_refund']:
                tax_support_id = None
            elif context['type'] in ['in_invoice','in_refund']:               
                ''' Función que ordena el sustento tributario y devuelve el de menor prioridad.'''
                cr.execute('''select sts.priority as priority, sts.id as id
                         from sri_tax_support sts
                         group by id, priority
                         order by priority''')
                res = cr.dictfetchone()
                if res:
                    tax_support_id = res['id']
            else:
                tax_support_id = None
        
        return tax_support_id
   
    def _assign_code_transaction_type(self, cr, uid, ids, field, arg, context=None):
        
        ''' Funcion que asigna un código tipo de transaccion al partner de acuerdo a su identificacion (cedula, ruc, pasaporte, etc)
            y transaccion que realice (compra o venta).
            Toma como parametros el campo tipo función code de res.partner y el campo type de account.invoice
        '''        
        
        res = {}
        code = ''
        
        for invoice in self.browse(cr, uid, ids, context):
            if invoice:
                try:
                    if invoice.partner_id.active==True:
                        code = invoice.partner_id.code
                    else:
                        if invoice.internal_number == False:
                            print 'Cliente en estado inactivo. '
                        else:
                            print 'Cliente en estado inactivo en la factura Nro. '+str(invoice.internal_number)
                except:            
                    raise orm.except_orm(_('Configuration Error!'),_('Cliente en estado inactivo en factura Nro. ')+str(invoice.internal_number)) 
                
                invoice_type = invoice.type
                
                if code:
                    if invoice_type == 'in_invoice' or invoice_type == 'in_refund': # COMPRAS            
                        if code == 'R': # Ruc
                            res[invoice.id] = '01'
                        if code == 'C': # Cedula
                            res[invoice.id] = '02'
                        if code == 'P': # Pasaporte / Identificación tributaria del exterior
                            res[invoice.id] = '03' 
                        if code == 'O': # Otros
                            if invoice.internal_number == False:
                                res[invoice.id] = "Proveedor no tiene asignado una identificacion (Cedula, RUC, Pasaporte) correcta. Documento de Compra. "+str(invoice.partner_id.name)
                            else:
                                res[invoice.id] = "Proveedor no tiene asignado una identificacion (Cedula, RUC, Pasaporte) correcta. Documento de Compra "+str(invoice.internal_number or invoice.partner_id.name)    
                            
                    if invoice_type == 'out_invoice' or invoice_type == 'out_refund':  # VENTAS
                        if code == 'R':   # Ruc
                            res[invoice.id] = '04'
                        if code == 'C':   # Cedula
                            res[invoice.id] = '05'
                        if code == 'P':   # Pasaporte / Identificación tributaria del exterior
                            res[invoice.id] = '06' 
                        if code == 'F':   # Consumidor final
                            res[invoice.id] = '07' 
                        if code == 'O':   # Otros
                            if invoice.internal_number == False:
                                res[invoice.id] = "Cliente no tiene asignado una identificacion (Cedula, RUC, Pasaporte) correcta. Documento de Venta. "+str(invoice.partner_id.name)
                            else:
                                res[invoice.id] = "Cliente no tiene asignado una identificacion (Cedula, RUC, Pasaporte) correcta. Documento de Venta "+str(invoice.internal_number or invoice.partner_id.name)
                else:
                    if invoice.internal_number == False:
                        res[invoice.id] = 'Cliente/Proveedor no tiene asignado una identificacion correcta (Cedula, RUC, Pasaporte).'+str(invoice.partner_id.name)   
                    else:
                        res[invoice.id] = 'Cliente/Proveedor no tiene asignado una identificacion correcta (Cedula, RUC, Pasaporte). Documento '+str(invoice.internal_number or invoice.partner_id.name)

        return res     
    
    def _amount_to_word(self, cr, uid, ids, name, args, context=None):
        """
        This function transform the amount total in text
        """
        res = {}
        
        for invoice in self.browse(cr, uid, ids, context=context):
            res[invoice.id] = {
                'amount_to_words': amount_to_words_spanish.amount_to_words_es(invoice.total_with_vat),
            }
            
        return res  
    
    _columns = {
        'sri_tax_support_id': fields.many2one('sri.tax.support', 'Tax Support', help='Indicates the tax support for this document.',),
        'transaction_type': fields.function(_assign_code_transaction_type, string='Transaction type', type='char', store=False, method=True, help='Indicate the transaction type that performer the partner. Supplier Invoice [01-RUC,02-CEDULA,03-PASAPORTE],Customer Invoice [04-RUC,05-CEDULA,06-PASAPORTE,07-CONSUMIDOR FINAL, 0-OTROS].',),
        'amount_to_words': fields.function(_amount_to_word, type='text', method=True, string='Amount in words',
                    store=False,
                    multi='all1'),
        'update_field': fields.integer('Update', ),
    }
   
    _defaults = {
        'sri_tax_support_id': _suggested_tax_support,        
    }
    
    def onchange_tax_support_id(self, cr, uid, ids, type=False, date_invoice=False, partner_id=False, tax_support_id=False, document_invoice_type_id=False, printer_id=False, authorizations_id=False, company_id=False, context=None):       
        ''''
        Función que muestra los tipos de documentos (in_invoice, in_refund) de acuerdo al sustento tributario escogido.
        Retorna un diccionario que contiene nuevos valores y el contexto, además le asigna a la clave document_invoice_type_id
        por defecto el primer valor de la lista document_type.        
        '''
       
        res = {'value': {},'warning':{},'domain':{}}
        
        #TODO: parece que se debe agregar las notas de debito y credito, el dimm permite en compras registrar ambas
        if type in ['in_invoice', 'in_refund']: #las facturas de venta no tienen sustento tributario... 
            if tax_support_id:
                
                document_type_obj = self.pool.get('account.invoice.document.type');               
                document_type_ids = document_type_obj.search(cr, uid, [('type','=', type),('use','=', True),],)
                document_type = document_type_obj.browse(cr, uid, document_type_ids, context=None)          
    
                # TODO: Cambiar las iteraciones por una funcion search de openerp
                document_type_list = []
    
                for item in document_type:
                    if item.sri_tax_support_ids:
                        for j in item.sri_tax_support_ids:
                            if j.id == tax_support_id:
                                if not item.id in document_type_list:
                                    document_type_list.append(item.id)
    
                res['domain'] = {'document_invoice_type_id': [('id', 'in', document_type_list)]}
                
                # Actualizamos con un valor apropiado el campo tipo de documento
                if len(document_type_list)>0:
                    res['value'] = {'document_invoice_type_id':document_type_list[0]}
                else:
                    res['value'] = {'document_invoice_type_id':False}
            
            else:
                #si no hay datos validos vaciamos el formulario
                res["value"].update({'document_invoice_type_id':False,
                                     })
                res["domain"].update({'document_invoice_type_id':[('id', '<', 0)],
                                     })

            #si el cambio de sustento tributario no genera cambio del tipo de documento entonces forzamos uno a fin de actualizar el formulario
            if document_invoice_type_id == res["value"]["document_invoice_type_id"]:
                document_invoice_type_res = self.onchange_document_invoice_type_id(cr, uid, ids, type, date_invoice, partner_id, tax_support_id, document_invoice_type_id, printer_id, authorizations_id, company_id, context)
                if document_invoice_type_res["value"]:
                    res["value"].update(document_invoice_type_res["value"])
                if document_invoice_type_res["domain"]:
                    res["domain"].update(document_invoice_type_res["domain"])
                if document_invoice_type_res["warning"]:
                    res["warning"]["message"] = str(res.get('warning') and res.get('warning').get('message')) + "\n" + str(document_invoice_type_res.get('warning') and document_invoice_type_res.get('warning').get('message')) 
            
        return res
    
    def _prepare_invoice_header(self, cr, uid, partner_id, type, inv_date=None, printer_id=None, context=None):
        """Retorna los valores ecuatorianos para el header de una factura
           Puede ser usado en ordenes de compra
           @partner_id es un objeto partner
           @type es el tipo de factura, ej. out_invoice
           @inv_date es la fecha prevista de la factura, si no se provee se asume hoy
        """

        if context is None:
            context = {}
        context.update({
                     'type': type,
                     })
        
        invoice_vals = {}
        
        invoice_vals = super(account_invoice, self)._prepare_invoice_header(cr, uid, partner_id, type, inv_date=inv_date, printer_id=printer_id, context=context)
        
        inv_obj=self.pool.get('account.invoice')
        sri_tax_support_id = inv_obj._suggested_tax_support(cr, uid, context)
        
        invoice_vals.update({
                             'sri_tax_support_id': sri_tax_support_id,
                             })
        return invoice_vals

    def invoice_validate(self, cr, uid, ids, context=None):
        '''
        Valida la factura previo aprobacion:
        1. Que tenga un tipo de transaccion valido
        '''
        if context is None:
            context = {}
        res = True
        for invoice in self.browse(cr, uid, ids, context):
            res = super(account_invoice, self).invoice_validate(cr, uid, ids, context)
            if invoice.type in ['in_invoice','out_invoice','in_refund','out_refund']:
                if invoice.transaction_type not in ['01','02','03','04','05','06','07']:
                    raise osv.except_osv(_('Invalid action!'), _('El tipo de transaccion del SRI no es valido, verifique RUC/Cedula, y tipo de documento'))
        return res
    
account_invoice()
