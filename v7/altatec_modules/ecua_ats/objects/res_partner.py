# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors: Carlos Yumbillo                                                                         
#Copyright (C) 2013 TRESCLOUD CIA. LTDA.                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import fields,osv

class res_partner(osv.osv):
    _inherit='res.partner'
    _name = 'res.partner'
    
    def _assign_code(self, cr, uid, ids, field, arg, context=None):
        ''' Función que asigna un código a cada tipo de identificación.'''        
        
        res = {}

        for partner in self.browse(cr, uid, ids, context):
            if partner.vat:
                code_country = self.get_code_country(partner.vat)
                identification = self.get_number_identification(partner.vat)
                if identification:
                    if code_country =='EC':
                        if (len(identification)==13):
                            if self.verifica_ruc_pnat(identification) or self.verifica_ruc_spri(identification) or self.verifica_ruc_spub(identification):
                                res[partner.id]= 'R'
                            elif self.verifica_id_cons_final(identification):
                                res[partner.id]= 'F'                   
                        if identification!='9999999999' and len(identification)==10:
                            res[partner.id]= 'C'
                    else:
                        res[partner.id]= 'P'
            else:
                res[partner.id]= 'O'                
    
        return res
    
    _columns = {
           'code': fields.function(_assign_code, string='Code identification type', type='char', store=True, method=True, help='Indicate the identification type of the partner.',),
        }
    
res_partner()