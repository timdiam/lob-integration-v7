# -*- encoding: utf-8 -*-
########################################################################
#                                                                       
# @authors: Carlos Yumbillo                                                                           
# Copyright (C) 2013  TRESCLOUD Cia Ltda.                                 
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import fields,osv

class payment_method(osv.osv):
     """ Payment method """
     _name = 'account.journal.payment.method' 
     _order = 'priority'
    
     _columns = {
         'code':fields.char('Code', size=2, required=True, help='Field of two digits that indicate the tax code for this payment method.',),
         'priority':fields.integer('Priority', required=True, help='Priority of payment method.',), 
         'name':fields.char('Name', size=255, required=True, help='Name of payment method.',),          
      }

payment_method()

class account_journal(osv.osv):
    
     _inherit = "account.journal"
     _description = "Journal"
     
     _columns = {       
        'payment_method_ids':fields.many2many('account.journal.payment.method', 'journal_payment_method_rel', 'journal_id', 'payment_method_id','Payment methods lines',),
     }
          
account_journal()