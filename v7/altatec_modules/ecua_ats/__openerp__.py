# -*- encoding: utf-8 -*-
########################################################################
#                                                                       
# @authors: Carlos Yumbillo, Patricio Rangles                                                                         
# Copyright (C) 2013  TRESCLOUD CIA. LTDA.                                  
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see <http://www.gnu.org/licenses/>.  
########################################################################

{
        "name" : "Ecuadorian Anexos",
        "author" :  "TRESCLOUD CIA. LTDA.",
        "maintainer": 'TRESCLOUD CIA. LTDA.',
        "website" : "http://www.trescloud.com",        
        'complexity': "medium",
        "description": """ATS para el SRI
        
        El Anexo Transaccional Simplificado es un documento digital solicitado por el SRI
        y que se lo genera para subirlo a travez del portal web del Servicio de Rentas Internas  
        
        TRESCLOUD Cia. Ltda.
        """,
        "category" : "Ecuadorian Legislation",   
        "version" : "1.0",
        "depends" : ['base',
                     'account',
                     'account_voucher',
                     'account_payment',
                     'report_aeroo',
                     'report_aeroo_ooo', 
                     'ecua_invoice_type',
                     'ecua_autorizaciones_sri',
                     'ecua_partner',
                     ],
        "init_xml" : [ ],
        "demo_xml" : [ ],
        "update_xml" : [
                        'security/ir.model.access.csv',
                        'data/payment_method.xml',
                        'report/report_data.xml',
                        'wizard/ecua_ats_view.xml',
                        'view/country_view.xml',
                        'view/payment_method_view.xml',
                        'view/account_invoice_view.xml',                       
                        'view/account_journal_view.xml',
                        'view/account_voucher_view.xml',
                        'view/res_partner_view.xml',
                        'view/account_tax_code_view.xml',
                        #'view/stock_picking_view.xml',
                        #'view/stock_picking_out_view.xml',
                         ],
        "installable": True
}