# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors: Carlos Yumbillo, Patricio Rangles                                                                         
#Copyright (C) 2013 TRESCLOUD Cia. Ltda.                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from report import report_sxw
from datetime import datetime
from tools.translate import _
from osv import orm,osv
from decimal import *

class Parser(report_sxw.rml_parse):
    
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context)
        self.cr = cr
        self.uid = uid
        self.localcontext.update({
            'get_identification': self._get_identification,
            'get_total': self._get_sum_account_subtotal,
            'get_total_shop': self._get_sum_account_subtotal_for_shop,
            'get_format_dates': self._get_format_dates,
            'get_sales_customer': self._get_list_sales_for_customer,
            'get_code_payment_method': self._get_code_payment_method,
            'get_code_payment_method_priority': self._get_code_payment_method_priority,
            'get_start_end_date_withhold_from_period': self._get_start_end_date_withhold,
            'get_start_end_date_invoice_from_period': self._get_start_end_date_invoice,
            'get_sum_no_tax_iva' : self.get_sum_no_tax_iva,
            'get_sum_ice' : self.get_sum_ice,
            'get_code_from_lookup_table':self.get_code_from_lookup_table,
            'get_code_from_lookup_table_from_tax':self.get_code_from_lookup_table_from_tax,
        })
    
#    def _get_child_code(self, tuple_parent_code):
#        """
#        Search child related with the list of parents code send, return a tuple of code(s) related included
#        the list of original parents  
#        """
#
#        docu_child_related_code = []
#        docu_type_obj = self.pool.get('account.invoice.document.type')
#
#        #Append the original parent list 
#        docu_child_related_code.extend(tuple_parent_code)
#        
#        for parent_code in tuple_parent_code:
#            # get the id(s) of parent code
#            docu_parent_ids = docu_type_obj.search(self.cr, self.uid, [('code', '=', parent_code)])
#            # get the child ids related to the parent
#            docu_child_list_id = docu_type_obj.search(self.cr, self.uid, [('parent_id', 'in', docu_parent_ids)])
#            # read the code of the childs
#            docu_child_list_code = docu_type_obj.read(self.cr, self.uid, docu_child_list_id, ['code'])
#            
#            for child in docu_child_list_code:
#                # append the child code to the list
#                docu_child_related_code.append(child['code'])
#        
#        return tuple(docu_child_related_code)
    
    #Using the period id return the start and end date to use in search statement
    def _get_start_end_date_withhold(self, period_id):
        """
        return a list with the start and end date to use in search statement of
        retention, must be added
        """
        res = []
        
        if period_id:
            period = self.pool.get('account.period').browse(self.cr, self.uid, period_id)
            res.append(('creation_date','>=',period.date_start))
            res.append(('creation_date','<=',period.date_stop))
        
        return res         

    #Using the period id return the start and end date to use in search statement for invoice
    def _get_start_end_date_invoice(self, period_id):
        """
        return a list with the start and end date to use in search statement of
        invoice, must be added
        """
        res = []
        
        if period_id:
            period = self.pool.get('account.period').browse(self.cr, self.uid, period_id)
            res.append(('date_invoice','>=',period.date_start))
            res.append(('date_invoice','<=',period.date_stop))
        
        return res         
    
    def _get_identification(self, vat):
        if vat!=False:
            if vat[:2] == 'EC':
                partner_vat = vat[2:]
            if vat[:2] != 'EC':
                partner_vat = vat
        else:
            partner_vat = 'Especifique identificacion correcta.'
        
        return partner_vat         
        
    def _get_sum_account_subtotal(self, period):
        total = 0.00
        obj_invoice = self.pool.get('account.invoice')
        list_docu_code = ('02','04','05','18')
        invoice_id_list = obj_invoice.search(self.cr, self.uid,[('period_id','=',period),('type','in',('out_invoice','out_refund')),('state','in',('open','paid')),('document_invoice_type_id.code','in', list_docu_code),])
        
        if invoice_id_list:
            invoices = obj_invoice.browse(self.cr, self.uid, invoice_id_list)           
            for lines in invoices:               
                conta1=0
                conta2=0
                if lines.type == 'out_invoice':
                    total = total + lines.amount_untaxed
                    conta1=conta1 + 1
                if lines.type == 'out_refund':
                    total = total - lines.amount_untaxed
                    conta2=conta2 + 1
                    
                print conta1
                print conta2
        return {'total':total}
    
    def _get_sum_account_subtotal_for_shop(self, period, codShop):
        total_for_shop = 0.00
        obj_invoice = self.pool.get('account.invoice')
        list_docu_code = ('02','04','05','18')
        invoice_id_list = obj_invoice.search(self.cr, self.uid,[('period_id','=',period),('type','in',('out_invoice','out_refund')),('state','in',('open','paid')),('printer_id.shop_id','=',codShop),('document_invoice_type_id.code','in',list_docu_code),])
        
        if invoice_id_list:
            invoices = obj_invoice.browse(self.cr, self.uid, invoice_id_list)           
            for lines in invoices:    
                if lines.type == 'out_invoice':        
                    total_for_shop = total_for_shop + lines.amount_untaxed
                if lines.type == 'out_refund':
                    total_for_shop = total_for_shop - lines.amount_untaxed
                    
        return {'total':total_for_shop}
    
    def _get_format_dates(self, fecha):
        admission_date = (str(fecha))
        admission_date = admission_date.split('-')
        date_end = admission_date[2]+"/"+admission_date[1]+"/"+admission_date[0]
          
        return {'end_date':date_end}       
    
    def valor(self,tup):
        if tup:
            return tup[0]
        else:
            return 0.0
    
    def _get_list_sales_for_customer(self, invoices, period, type):
        list_client = []
        list_sales = []
        res = {}
        cli_vat = ''   

        if invoices:
            for inv in invoices:

                client_id = []
                try:
                    if inv.partner_id.active==True:
                        client_id = self.pool.get('res.partner').search(self.cr, self.uid, [('id','=',inv.partner_id.id),])
                    else:
                        print 'Existe un cliente en estado inactivo en la factura de venta Nro. '+inv.number
                except:            
                    raise orm.except_orm(_('Configuration Error!'),_('Existe un cliente en estado inactivo en la factura de venta Nro. ')+inv.number)

                if client_id:
                    if not client_id[0] in list_client:                             
                        list_client.append(client_id[0])
                     
            customers = self.pool.get('res.partner').browse(self.cr, self.uid, list_client)
            num = len(list_client)
                   
            # Analysis by client
            for cli in customers:                
                #Facturas en Ventas
                if type=='out_invoice':
                    # Analysis by document type
                    # In Sale add the documents used for refund, the code is "41"
                    list_docu_code = ('02','18','41')
                    
                    for document_type_code in list_docu_code:        

                        # TODO: De donde saco la compañia?
                        tax_mapping_brow = False
                        
#                        if document_type_code == '41':
#                            company_id = 1
#                            tax_mapping_obj = self.pool.get('account.config.settings')
#                            tax_mapping_list = tax_mapping_obj.search(self.cr, self.uid, [('company_id','=',company_id)])
#                            if tax_mapping_list:
#                                tax_mapping_brow = self.pool.get('account.config.settings').browse(self.cr, self.uid, tax_mapping_list[0])
#                            else:
#                                raise orm.except_orm(_('Configuration Error!'),_('Taxes for refund are missing. Please go to Configuration -> Account and select the refund taxes'))

                        num_comprobantes_facturas=0
                        base=0.0
                        base_0=0.0
                        iva=0.0
                        iva_no_round=0.0
                        iva_ret=0.0
                        renta_ret=0.0        
                        
                        inv_vent_ids = self.pool.get('account.invoice').search(self.cr, self.uid, [('partner_id.id','=',cli['id']),
                                                                                                   ('period_id.id','=',period),
                                                                                                   ('type','=','out_invoice'),
                                                                                                   ('state','in',('open','paid')),
                                                                                                   ('document_invoice_type_id.code','=',document_type_code),])
                        num_comprobantes = len(inv_vent_ids)
                        inv_ventas = self.pool.get('account.invoice').browse(self.cr, self.uid, inv_vent_ids)
                        
#                         for inv in inv_ventas:
#                             # is other process and fields in the "dimm formularios" for refund
#                             if document_type_code == '41':
#                                 base = base + self.valor([n.base for n in inv.tax_line if n.amount != 0.0])
#                                 base_0 = base_0 + self.valor([n.base for n in inv.tax_line if n.amount == 0.0])
#                                 iva = iva + self.valor([n.amount for n in inv.tax_line if n.tax_code_id.code in ("444 (IVA 12%)","444 (IVA 0%)")]) # por que son infiormativos ambos y pueden estar cruzados enm la configuracion
#                             else:
#                                 base = base + self.valor([n.base for n in inv.tax_line if n.base_code_id.code in ("411",)])
#                                 base_0 = base_0 + self.valor([n.base for n in inv.tax_line if n.base_code_id.code in ("415","416","413","414")])
#                                 iva = iva + self.valor([n.amount for n in inv.tax_line if n.tax_code_id.code in ("421",)])
# 
#                             if inv.withhold_id:
#                                 iva_ret = iva_ret + abs(inv.withhold_id.total_iva)
#                                 renta_ret = renta_ret + abs(inv.withhold_id.total_renta)

                        for inv in inv_ventas:
                            # is other process and fields in the "dimm formularios" for refund
                            base = base + inv.base_doce_iva
                            base_0 = base_0 + inv.base_cero_iva
                            iva = iva + inv.vat_doce_subtotal
                            iva_no_round = iva_no_round + inv.base_doce_iva * .12

                            if inv.withhold_id:
                                iva_ret = iva_ret + abs(inv.withhold_id.total_iva)
                                renta_ret = renta_ret + abs(inv.withhold_id.total_renta)

                            elif hasattr(inv,'credit_card') and inv.credit_card:
                                orden_lines_obj = self.pool.get('orden.de.pago.general.line')
                                orden_lines = orden_lines_obj.search(self.cr,self.uid,[('invoice_id','=',inv.id)])
                                if len(orden_lines) > 1:
                                    raise orm.except_orm( ( 'Error' ), ( 'La factura numero: '+inv.internal_number+' esta en mas que un orden de pago.  Por favor, revise, o consulte con AltaTec' ) )
                                elif len(orden_lines) == 0:
                                    pass
                                    #raise orm.except_orm( ( 'Error' ), ( 'La factura numero: '+inv.internal_number+' No se encuentra en ningun orden de pago, pero tiene seleccionado una tarjeta de credito.  Por favor, revise.' ) )
                                else:
                                    order_line = orden_lines_obj.browse(self.cr,self.uid,orden_lines)[0]
                                    iva_ret = iva_ret + abs(order_line.ret_iva)
                                    renta_ret = renta_ret + abs(order_line.ret)

                        #Send only the total of sale
                        if inv_ventas:
                            #Check to make sure the difference isn't too big in iva and iva_no_round
                            
                            if(abs(iva_no_round - iva) >= .10):
                                raise orm.except_orm(_('Calulation Error!'),_('Existe un cliente con una diferencia en IVA Calculado.  Consulta tu admin de sistemas ')+self._get_identification(cli.vat))

                            
                            res = {'tpIdCliente': inv.transaction_type,
        						   'idCliente': self._get_identification(cli.vat), 
                                   'tipoComprobante': inv.document_invoice_type_id.code,
                                   'numeroComprobantes': num_comprobantes, 
                                   'baseNoGraIva':'0.00', 
                                   'baseImponible': base_0, 
                                   'baseImpGrav': base, 
                                   'montoIva': round(iva_no_round,2), 
                                   'valorRetIva': iva_ret, 
                                   'valorRetRenta': renta_ret}
                            
                            # Append the summary for client and document type
                            list_sales.append(res)
                    
                #Notas de Credito en Ventas
                if type=='out_refund':
                    # Analysis by document type
                    # In Sale add the documents used for refund, the code is "41"
                    list_docu_code = ('04','05')

                    for document_type_code in list_docu_code:        

                        num_comprobantes_nc=0
                        base_nc=0.0
                        base_nc_0=0.0
                        iva_nc=0.0
                        iva_ret_nc=0.0
                        renta_ret_nc=0.0

                        inv_notas_credito_ids = self.pool.get('account.invoice').search(self.cr, self.uid, [('partner_id.id','=',cli['id']),
                                                                                                            ('period_id.id','=',period),
                                                                                                            ('type','=','out_refund'),
                                                                                                            ('state','in',('open','paid')),
                                                                                                            ('document_invoice_type_id.code','=',document_type_code),])
                        num_comprobantes = len(inv_notas_credito_ids)
                        inv_notas_credito = self.pool.get('account.invoice').browse(self.cr, self.uid, inv_notas_credito_ids)
                        for inv in inv_notas_credito:
                            base_nc = base_nc + inv.base_doce_iva
                            base_nc_0 = base_nc_0 + inv.base_cero_iva
                            #base_nc = base_nc + inv.vat_doce_subtotal
                            #base_nc_0 = base_nc_0 + inv.vat_cero_subtotal
                            iva_nc = iva_nc + inv.vat_doce_subtotal
                            if inv.withhold_id:
                                iva_ret_nc = iva_ret_nc + abs(withhold_id.total_iva)
                                renta_ret_nc = renta_ret_nc + abs(withhold_id.total_renta)
                        
                        # Send the summary
                        if inv_notas_credito:                          
                            res = {'tpIdCliente': inv.transaction_type,
                                   'idCliente': self._get_identification(cli.vat), 
                                   'tipoComprobante': inv.document_invoice_type_id.code,
                                   'numeroComprobantes': num_comprobantes, 
                                   'baseNoGraIva':'0.00', 
                                   'baseImponible': base_nc_0, 
                                   'baseImpGrav': base_nc, 
                                   'montoIva': iva_nc, 
                                   'valorRetIva': iva_ret_nc, 
                                   'valorRetRenta': renta_ret_nc}

                            # Append the summary for client and document type
                            list_sales.append(res)
                    
                    #Notas de Débito en Ventas
                    #Por desarrollar               
                    
        
        return list_sales
    
    def _get_code_payment_method(self, move_id):
        code = ''
        obj_voucher = self.pool.get('account.voucher')
        voucher_id_list = obj_voucher.search(self.cr, self.uid,[('move_id','=',move_id),])

        if voucher_id_list:
            vouchers = obj_voucher.browse(self.cr, self.uid, voucher_id_list)
            for line in vouchers:
                code = line.payment_method_id.code

        return code
    
    def _get_code_payment_method_priority(self):
        code = False

        self.cr.execute('''select min(pm.priority) as priority, pm.code as code
                 from account_journal_payment_method pm
                 group by code
                 order by priority''')
        res = self.cr.dictfetchone()

        if res:
            code = res['code']

        return code

    def get_code_from_lookup_table_from_tax(self, in_retair_line, period):
        tax_code_obj = self.pool.get('account.tax.code')
        lookup_obj = self.pool.get('account.tax.lookup.ats')

        tax_code = tax_code_obj.browse(self.cr,self.uid, in_retair_line.base_code_id.id)

        end_date_period = datetime.strptime(period.date_stop, '%Y-%m-%d')

        found_code = False
        code = ""

        for lookup in tax_code.tax_code_lookup_ats:
            start_lookup = datetime.strptime(lookup.date_start, '%Y-%m-%d')
            end_lookup = datetime.strptime(lookup.date_end, '%Y-%m-%d')
            if (end_date_period <= end_lookup) and (end_date_period >= start_lookup):
                if(found_code):
                    raise osv.except_osv( ( 'ERROR' ), ( 'Se encuentran varias codigos validas durante el period.  Revisa codigo impuesto: '+tax_code.name+' ' + tax_code.code ) )
                found_code = True
                code = lookup.code

        if found_code == False:
            raise osv.except_osv( ( 'ERROR' ), ( 'No hay codigo valida durante el periodo.  Revisa codigo impuesto: '+tax_code.name+' ' + tax_code.code ) )
        else:
            return code

    def get_code_from_lookup_table(self, in_retair_line, period):
        tax_code_obj = self.pool.get('account.tax.code')
        lookup_obj = self.pool.get('account.tax.lookup.ats')

        tax_code = tax_code_obj.browse(self.cr,self.uid, in_retair_line.tax_id.id)

        end_date_period = datetime.strptime(period.date_stop, '%Y-%m-%d')

        found_code = False
        code = ""

        for lookup in tax_code.tax_code_lookup_ats:
            start_lookup = datetime.strptime(lookup.date_start, '%Y-%m-%d')
            end_lookup = datetime.strptime(lookup.date_end, '%Y-%m-%d')
            if (end_date_period <= end_lookup) and (end_date_period >= start_lookup):
                if(found_code):
                    raise osv.except_osv( ( 'ERROR' ), ( 'Se encuentran varias codigos validas durante el period.  Revisa codigo impuesto: '+tax_code.name+' ' + tax_code.code ) )
                found_code = True
                code = lookup.code

        if found_code == False:
            raise osv.except_osv( ( 'ERROR' ), ( 'No hay codigo valida durante el periodo.  Revisa codigo impuesto: '+tax_code.name+' ' + tax_code.code ) )
        else:
            return code


    def get_sum_no_tax_iva(self, invoice_id):
        no_tax_total = 0.00
        obj_invoice = self.pool.get('account.invoice')

        inv = obj_invoice.browse(self.cr, self.uid, invoice_id)

        if inv:
            for line in inv.invoice_line:
                if hasattr(line,'no_tax_iva') and line.no_tax_iva:
                    no_tax_total = no_tax_total + line.price_subtotal

        res = str(no_tax_total)
        if res == '0.0':
            res = '0.00'

        return {'no_tax_total': res}


    def get_sum_ice(self, invoice_id):
        total_ice = 0.00
        obj_invoice = self.pool.get('account.invoice')

        inv = obj_invoice.browse(self.cr, self.uid, invoice_id)

        if inv:
            for line in inv.invoice_line:
                if hasattr(line.product_id,'is_ice'):
                    if line.product_id.is_ice:
                        total_ice = total_ice + line.price_subtotal
                else:
                    if line.product_id.name == 'ICE':
                        total_ice = total_ice + line.price_subtotal

        res = str(total_ice)

        if res == '0.0':
            res = '0.00'

        return {'total_ice': res}
