# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Electrocom (www.electrocom.com). All rights reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import xlwt
from report_xls.report_xls import report_xls, AttrDict
from parser import ecua_anexo_xls_parser
import cStringIO


class ecua_anexo_ats_xls(report_xls):

    def get_total_lines(self, list_objects):
        i = 0
        totales = [0]*22
        for c_specs_01 in list_objects:
            totales[0] += c_specs_01[11][4]
            totales[1] += c_specs_01[12][4]
            totales[2] += c_specs_01[13][4]
            totales[3] += c_specs_01[14][4]
            totales[4] += c_specs_01[15][4]
            totales[5] += c_specs_01[16][4]
            totales[6] += c_specs_01[17][4]
            totales[7] += c_specs_01[18][4]
            totales[8] += c_specs_01[19][4]
            totales[9] += c_specs_01[20][4]
            totales[10] += c_specs_01[21][4]
            totales[11] += c_specs_01[22][4]
            totales[12] += c_specs_01[23][4]
            totales[13] += c_specs_01[24][4]
            totales[14] += c_specs_01[25][4]
            totales[15] += c_specs_01[26][4]
            totales[16] += c_specs_01[27][4]
            totales[17] += c_specs_01[28][4]
            totales[18] += c_specs_01[32][4] or 0.0
            totales[19] += c_specs_01[34][4] or 0.0
            totales[20] += c_specs_01[36][4] or 0.0
            totales[21] += 1
        totales[21] = str(totales[21])
        return totales

    def make_invoice_lines(self, cr, uid, inv, objects, auto_name, cell_style_2, cell_style_3, cell_style_date, cell_style_decimal):
        withhold_obj = objects.get('account.withhold')
        withhold = withhold_obj.browse(cr, uid, withhold_obj.search(cr, uid, [('invoice_id','=',inv.id)]))
        serie = secuencia = ''
        subtotal = inv.amount_untaxed
        no_sujeto_retenido = 0.0
        if withhold:
            serie = withhold[0].number.replace('-','')[:6]
            secuencia = withhold[0].number.replace('-','')[6:]
        compras = servicios = activos = reembolsos = 0.0
        crf = amount = porcentaje = ''
        porcentaje_i = ''
        porc = 0
        for line in inv.invoice_line:
            if line.ecua_shadow_retentions:
                for retention in line.ecua_shadow_retentions:
                    if retention.type_ec == 'renta':
                        crf += retention.description+","
                        amount += str(abs(line.price_subtotal*retention.amount))+","
                        porcentaje += retention.domain +","
                    else:
                        porc = int(retention.domain.split("%")[0])/100.0
                        porcentaje_i = retention.domain
            if line.ecua_shadow_taxes and line.ecua_shadow_taxes[0].name == 'IVA(12%) PAGADO EN COMPRAS LOCALES':
                compras += line.price_subtotal
            elif line.ecua_shadow_taxes and line.ecua_shadow_taxes[0].name == 'IVA(12%) PAGADO EN SERVICIOS LOCALES':
                servicios += line.price_subtotal
            elif line.ecua_shadow_taxes and line.ecua_shadow_taxes[0].name == 'IVA(12%) PAGADO EN COMPRAS LOCALES DE ACTIVOS FIJOS':
                activos += line.price_subtotal
            elif line.ecua_shadow_taxes and line.ecua_shadow_taxes[0].name == 'IVA 0% EN PAGOS POR REEMBOLSO DE GASTOS':
                reembolsos += line.price_subtotal
        if inv.document_invoice_type_id.code == '41':
            subtotal = 0.0
            no_sujeto_retenido = inv.amount_untaxed
        crf1 = crf and crf.split(',')[0]
        crf2 = crf and crf.split(',')[1]
        amount1 = amount and float(amount.split(',')[0] or '0.0')
        amount2 = amount and float(amount.split(',')[1] or '0.0')
        porcentaje1 = porcentaje and porcentaje.split(',')[0]
        porcentaje2 = porcentaje and porcentaje.split(',')[1]
        porcentaje1_i = porcentaje_i and porcentaje_i.split(',')[0]
        c_specs_01 = [
                (auto_name(), 1, 0, 'text', inv.partner_id.vat[2:], None, cell_style_2),
                (auto_name(), 7, 0, 'text', inv.partner_id.name, None, cell_style_2),
                (auto_name(), 1, 0, 'text', 'NO', None, cell_style_2),
                (auto_name(), 1, 0, 'text', inv.date_invoice, None, cell_style_date),
                (auto_name(), 1, 0, 'text', inv.date_invoice, None, cell_style_date),
                (auto_name(), 4, 0, 'text', inv.authorizations_id.name, None, cell_style_3),
                (auto_name(), 1, 0, 'text', serie, None, cell_style_3),
                (auto_name(), 1, 0, 'text', secuencia, None, cell_style_3),
                (auto_name(), 1, 0, 'text', inv.internal_number.replace('-','')[:6], None, cell_style_3),
                (auto_name(), 1, 0, 'text', inv.internal_number.replace('-','')[6:], None, cell_style_3),
                (auto_name(), 1, 0, 'text', inv.sri_tax_support_id.code, None, cell_style_3),
                (auto_name(), 1, 0, 'number', subtotal, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', (serie and inv.amount_untaxed) or no_sujeto_retenido, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', compras, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', compras * 0.12, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', servicios, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', servicios * 0.12, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', activos, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', activos * 0.12, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', reembolsos, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', reembolsos * 0.12, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', inv.base_doce_iva, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', inv.vat_doce_subtotal, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', inv.base_cero_iva, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', inv.base_no_grava_iva, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', 0, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', 0, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', 0, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', 0, None, cell_style_decimal),
                (auto_name(), 1, 0, 'text', crf1, None, cell_style_3),
                (auto_name(), 1, 0, 'text', crf2, None, cell_style_3),
                (auto_name(), 1, 0, 'text', porcentaje1, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', amount1, None, cell_style_decimal),
                (auto_name(), 1, 0, 'text', porcentaje2, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', amount2, None, cell_style_decimal),
                (auto_name(), 1, 0, 'text', porcentaje1_i, None, cell_style_decimal),
                (auto_name(), 1, 0, 'number', inv.vat_doce_subtotal * porc, None, cell_style_decimal),
              ]

        return c_specs_01

    def create_source_xls(self, cr, uid, ids, data, context=None):
        if not context: context = {}
        parser_instance = self.parser(cr, uid, self.name2, context)
        self.parser_instance = parser_instance
        objs = self.getObjects(cr, uid, ids, context)
        parser_instance.set_context(objs, data, ids, 'xls')
        objs = parser_instance.localcontext['objects']
        objs = {
            'cr': cr,
            'uid':uid,
            'objs':objs,
            'account.invoice': self.pool.get('account.invoice'),
            'account.withhold':self.pool.get('account.withhold'),
        }
        n = cStringIO.StringIO()
        wb = xlwt.Workbook(encoding='utf-8')
        _p = AttrDict(parser_instance.localcontext)
        _xs = self.xls_styles
        self.generate_xls_report(_p, _xs, data, objs, wb)
        wb.save(n)
        n.seek(0)
        return (n.read(), 'xls')

    def generate_xls_report(self, _p, _xs, data, objects, wb):
        """
        :param _p:
        :param _xs:
        :param data:
        :param objects: This is not used for this case, we need differents objects to print an excel file
        :param wb:
        :return:
        """
        self.sheet_compra(_p, _xs, data, objects, wb)

        self.sheet_venta(_p, _xs, data, objects, wb)
        """self.sheet_ret_fte(_p, _xs, data, objects, wb)
        self.sheet_ret_iva(_p, _xs, data, objects, wb)
        self.sheet_ret_iva_cltes(_p, _xs, data, objects, wb)
        self.sheet_ret_fte_clte(_p, _xs, data, objects, wb)
        self.sheet_res_ret(_p, _xs, data, objects, wb)
        self.sheet_res_transac(_p, _xs, data, objects, wb)
        self.sheet_sec_ret(_p, _xs, data, objects, wb)
        self.sheet_ret_anulada(_p, _xs, data, objects, wb)
        self.sheet_balance(_p, _xs, data, objects, wb)"""

    def sheet_compra(self, _p, _xs, data, objects, wb):
        """
        :param _p:
        :param _xs:
        :param data:
        :param objects: Contains all account_invoice objects with the given period_id
        :param wb:
        :return:
        """

        # Database Cursor
        cr = objects.get('cr')

        # User ID
        uid = objects.get('uid')
        invoice_obj = objects.get('account.invoice')
        objs = objects.get('objs')
        invoice_ids = invoice_obj.browse(cr, uid, invoice_obj.search(cr, uid, [('period_id','=',objs.period_id.id[0]),('type','in',('in_invoice','in_refund')),('state','in',('open','paid')),('document_invoice_type_id.code','in',('01','02','09','03','04','05','11','12','19','20','21','41','43'))], order='date_invoice asc'))
        ws = wb.add_sheet("COMPRA")

        #STYLE DEFINITIONS
        # Here we define some useful styles for the sheet.  At any point in time, we can grab one of these to use
        cell_format = _xs['borders_all']
        cell_format_2 = "borders: top thin, bottom thin, left thin, right thin;"
        cell_style_2 = xlwt.easyxf(cell_format)
        cell_style_3 = xlwt.easyxf(cell_format + _xs['left'])
        cell_style_date = xlwt.easyxf(cell_format + _xs['center'], num_format_str = report_xls.date_format)
        cell_style_decimal = xlwt.easyxf(cell_format + _xs['right'], num_format_str = report_xls.decimal_format)
        cell_style_decimal_bold = xlwt.easyxf(cell_format + _xs['right']+_xs['bold'], num_format_str = report_xls.decimal_format)
        cell_style_bold = xlwt.easyxf(cell_format + _xs['bold'])
        cell_style_bold_2 = xlwt.easyxf(cell_format + _xs['bold'] + _xs['left'])
        cell_style_wrap_center_bold = xlwt.easyxf('font: height 180, name Arial, colour_index black, bold on, italic on; align: wrap on, vert centre, horiz centre;' + cell_format)


        #Starting at row 0
        row_pos = 0

        #Utility function.  The write row function needs a unique name for each column each time a row is written
        #We define here a function that will generate a unique id each time we call it.  It is a simple incrementer.
        self.auto_gen_id = 0
        def auto_name():
            self.auto_gen_id = self.auto_gen_id + 1
            return self.auto_gen_id

        #This is how we can force the height of a row, if the default does not suit us
        #For whatever reason, we have to turn the height_mismatch flag on, and then set the height.
        #ws is our sheet object.
        ws.row(0).height_mismatch = True
        ws.row(0).height = 256*2
        c_specs = [(auto_name(), 1, 0, 'text', 'Documento', None, cell_style_wrap_center_bold),
                   (auto_name(), 7, 0, 'text', 'Proveedor', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Rise', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Fecha Emision', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Fecha Contable', None,cell_style_wrap_center_bold ),
                   (auto_name(), 4, 0, 'text', 'Autorización Dcto', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Serie Retencion', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Secuencia Retencion', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Serie Dcto', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Secuencia Dcto', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Sust', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Base Imponible', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'No Sujeta Retención', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Compra Bienes', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Iva Bienes', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Compra Servicios', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Iva Servicios', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Compra Activos', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Iva Activos', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Compra Reembolso', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Iva Reembolso', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Base Iva', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Monto Iva', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Base Cero', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Base No Iva', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Base Cero Bienes', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Base Cero Servicios', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Base Cero Reembolso', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'IRBP', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'CRF1', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'CRF2', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Porcentaje Retencion', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Valor Retencion Renta', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Porcentaje Retencion', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Valor Retencion Renta', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', '% Retencion Iva', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Valor Retencion Iva', None,cell_style_wrap_center_bold ),

                  ]

        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold)

        c_specs = [
                    (auto_name(), 9, 0, 'text', '01 - COGRALET S A   01 - GUAYAS   01 - GUAYAQUIL', None,cell_style_bold_2),
                  ]

        row_data = self.xls_row_template(c_specs,[x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold_2)

        c_specs = [
                    (auto_name(), 9, 0, 'text', '01 Factura', None, cell_style_bold_2),
                  ]

        row_data = self.xls_row_template(c_specs,[x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold_2)
        c_specs_02, c_specs_03,  c_specs_04, c_specs_12, c_specs_20, c_specs_41 = [], [], [], [], [], []
        len_inv_01 = 0
        sum_subtotal1 = sum_no_retencion = sum_bienes = sum_iva_bienes = sum_servicios = sum_iva_servicios = 0.0
        sum_activos = sum_iva_activos = sum_reembolsos = sum_iva_reembolsos = base_iva = monto_iva =  base_cero = 0.0
        sum_base_no = sum_cero_bienes = sum_cero_servicios = sum_cero_activos = sum_cero_reembolsos = 0.0
        sum_ret_fte = sum_ret_fte2 = sum_ret_iva = sum_ret_iva2 = 0.0
        for inv in invoice_ids:
            if inv.document_invoice_type_id.code == '01':
                c_specs_01 = self.make_invoice_lines(cr, uid, inv, objects, auto_name, cell_style_2, cell_style_3,
                                                     cell_style_date, cell_style_decimal)
                sum_subtotal1 += c_specs_01[11][4]
                sum_no_retencion += c_specs_01[12][4]
                sum_bienes += c_specs_01[13][4]
                sum_iva_bienes += c_specs_01[14][4]
                sum_servicios += c_specs_01[15][4]
                sum_iva_servicios += c_specs_01[16][4]
                sum_activos += c_specs_01[17][4]
                sum_iva_activos += c_specs_01[18][4]
                sum_reembolsos += c_specs_01[19][4]
                sum_iva_reembolsos += c_specs_01[20][4]
                base_iva += c_specs_01[21][4]
                monto_iva += c_specs_01[22][4]
                base_cero += c_specs_01[23][4]
                sum_base_no += c_specs_01[24][4]
                sum_cero_bienes += c_specs_01[25][4]
                sum_cero_servicios += c_specs_01[26][4]
                sum_cero_activos += c_specs_01[27][4]
                sum_cero_reembolsos += c_specs_01[28][4]
                sum_ret_fte += c_specs_01[32][4] or 0.0
                sum_ret_fte2 += c_specs_01[34][4] or 0.0
                sum_ret_iva += c_specs_01[36][4] or 0.0
                len_inv_01 += 1
                row_data = self.xls_row_template(c_specs_01,[x[0] for x in c_specs_01])
                row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            elif inv.document_invoice_type_id.code == '02':
                c_specs_02 += [self.make_invoice_lines(cr, uid, inv, objects, auto_name, cell_style_2, cell_style_3,
                                                       cell_style_date, cell_style_decimal)]

            elif inv.document_invoice_type_id.code == '03':
                c_specs_03 += [self.make_invoice_lines(cr, uid, inv, objects, auto_name, cell_style_2, cell_style_3, cell_style_date, cell_style_decimal)]

            elif inv.document_invoice_type_id.code == '04':
                c_specs_04 += [self.make_invoice_lines(cr, uid, inv, objects, auto_name, cell_style_2, cell_style_3, cell_style_date, cell_style_decimal)]

            elif inv.document_invoice_type_id.code == '12':
                c_specs_12 += [self.make_invoice_lines(cr, uid, inv, objects, auto_name, cell_style_2, cell_style_3, cell_style_date, cell_style_decimal)]

            elif inv.document_invoice_type_id.code == '20':
                c_specs_20 += [self.make_invoice_lines(cr, uid, inv, objects, auto_name, cell_style_2, cell_style_3, cell_style_date, cell_style_decimal)]

            elif inv.document_invoice_type_id.code == '41':
                c_specs_41 += [self.make_invoice_lines(cr, uid, inv, objects, auto_name, cell_style_2, cell_style_3, cell_style_date, cell_style_decimal)]


        c_specs = [
                    (auto_name(), 1, 0, 'text', '', None, cell_style_bold_2),
                    (auto_name(), 7, 0, 'text', 'SUBTOTAL DE REGISTROS: '+str(len_inv_01) , None, cell_style_bold_2),
                    (auto_name(), 12, 0, 'text', '', None, cell_style_bold_2),
                    (auto_name(), 1, 0, 'number', sum_subtotal1 , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_no_retencion, None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_bienes , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_iva_bienes, None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_servicios , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_iva_servicios , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_activos , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_iva_activos , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_reembolsos , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_iva_reembolsos, None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', base_iva, None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', monto_iva , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', base_cero , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_base_no , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_cero_bienes , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_cero_servicios , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_cero_activos , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_cero_reembolsos , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_ret_fte, None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_ret_fte2 , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', sum_ret_iva , None, cell_style_decimal_bold),
                  ]
        row_data = self.xls_row_template(c_specs,[x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

        c_specs = [
                    (auto_name(), 9, 0, 'text', '', None, cell_style_bold_2),
                  ]
        row_data = self.xls_row_template(c_specs,[x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

        if c_specs_02:
            c_specs = [
                        (auto_name(), 9, 0, 'text', '02  Nota o boleta de venta', None, cell_style_bold_2),
                      ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)
            for c_specs in c_specs_02:
                row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
                row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)
            result = self.get_total_lines(c_specs_02)

            c_specs = [
                    (auto_name(), 1, 0, 'text', '', None, cell_style_bold_2),
                    (auto_name(), 7, 0, 'text', 'SUBTOTAL DE REGISTROS: '+result[21] , None, cell_style_bold_2),
                    (auto_name(), 12, 0, 'text', '', None, cell_style_bold_2),
                    (auto_name(), 1, 0, 'number', result[0] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[1], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[2] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[3], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[4] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[5] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[6] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[7] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[8] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[9], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[10], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[11] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[12] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[13] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[14] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[15] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[16] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[17] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[18], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[19] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[20] , None, cell_style_decimal_bold),
                  ]
            row_data = self.xls_row_template(c_specs,[x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)


            c_specs = [
                        (auto_name(), 9, 0, 'text', '', None, cell_style_bold_2),
                      ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

        if c_specs_03:
            c_specs = [
                        (auto_name(), 9, 0, 'text', '03  Liquidación de compra de bienes o prestación de servicios',
                         None, cell_style_bold_2),
                      ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            for c_specs in c_specs_03:
                row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
                row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            result = self.get_total_lines(c_specs_03)

            c_specs = [
                    (auto_name(), 1, 0, 'text', '', None, cell_style_bold_2),
                    (auto_name(), 7, 0, 'text', 'SUBTOTAL DE REGISTROS: '+result[21] , None, cell_style_bold_2),
                    (auto_name(), 12, 0, 'text', '', None, cell_style_bold_2),
                    (auto_name(), 1, 0, 'number', result[0] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[1], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[2] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[3], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[4] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[5] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[6] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[7] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[8] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[9], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[10], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[11] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[12] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[13] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[14] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[15] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[16] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[17] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[18], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[19] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[20] , None, cell_style_decimal_bold),
                  ]
            row_data = self.xls_row_template(c_specs,[x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            c_specs = [
                        (auto_name(), 9, 0, 'text', '', None, cell_style_bold_2),
                      ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

        if c_specs_04:
            c_specs = [
                        (auto_name(), 9, 0, 'text', '04  Nota de crédito', None, cell_style_bold_2),
                      ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            for c_specs in c_specs_04:
                row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
                row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            result = self.get_total_lines(c_specs_04)

            c_specs = [
                    (auto_name(), 1, 0, 'text', '', None, cell_style_bold_2),
                    (auto_name(), 7, 0, 'text', 'SUBTOTAL DE REGISTROS: '+result[21] , None, cell_style_bold_2),
                    (auto_name(), 12, 0, 'text', '', None, cell_style_bold_2),
                    (auto_name(), 1, 0, 'number', result[0] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[1], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[2] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[3], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[4] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[5] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[6] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[7] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[8] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[9], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[10], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[11] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[12] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[13] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[14] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[15] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[16] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[17] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[18], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[19] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[20] , None, cell_style_decimal_bold),
                  ]
            row_data = self.xls_row_template(c_specs,[x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            c_specs = [
                        (auto_name(), 9, 0, 'text', '', None, cell_style_bold_2),
                      ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

        if c_specs_12:
            c_specs = [
                        (auto_name(), 9, 0, 'text', '12  Documento emitido por una institución financiera',
                         None, cell_style_bold_2),
                      ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            for c_specs in c_specs_12:
                row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
                row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            result = self.get_total_lines(c_specs_12)

            c_specs = [
                    (auto_name(), 1, 0, 'text', '', None, cell_style_bold_2),
                    (auto_name(), 7, 0, 'text', 'SUBTOTAL DE REGISTROS: '+result[21] , None, cell_style_bold_2),
                    (auto_name(), 12, 0, 'text', '', None, cell_style_bold_2),
                    (auto_name(), 1, 0, 'number', result[0] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[1], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[2] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[3], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[4] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[5] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[6] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[7] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[8] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[9], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[10], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[11] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[12] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[13] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[14] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[15] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[16] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[17] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[18], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[19] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[20] , None, cell_style_decimal_bold),
                  ]
            row_data = self.xls_row_template(c_specs,[x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            c_specs = [
                        (auto_name(), 9, 0, 'text', '', None, cell_style_bold_2),
                      ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

        if c_specs_20:
            c_specs = [
                        (auto_name(), 9, 0, 'text', '20  Documento por servicio administrativo emitido por una ' +
                                                    'institución del estado', None, cell_style_bold_2),
                      ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            for c_specs in c_specs_20:
                row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
                row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            result = self.get_total_lines(c_specs_20)

            c_specs = [
                    (auto_name(), 1, 0, 'text', '', None, cell_style_bold_2),
                    (auto_name(), 7, 0, 'text', 'SUBTOTAL DE REGISTROS: '+result[21] , None, cell_style_bold_2),
                    (auto_name(), 12, 0, 'text', '', None, cell_style_bold_2),
                    (auto_name(), 1, 0, 'number', result[0] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[1], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[2] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[3], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[4] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[5] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[6] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[7] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[8] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[9], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[10], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[11] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[12] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[13] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[14] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[15] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[16] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[17] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[18], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[19] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[20] , None, cell_style_decimal_bold),
                  ]
            row_data = self.xls_row_template(c_specs,[x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            c_specs = [
                        (auto_name(), 9, 0, 'text', '', None, cell_style_bold_2),
                      ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

        if c_specs_41:
            c_specs = [
                        (auto_name(), 9, 0, 'text', '41  Comprobante de venta emitido por reembolso', None,
                         cell_style_bold_2),
                      ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            for c_specs in c_specs_41:
                row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
                row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)
            result = self.get_total_lines(c_specs_41)

            c_specs = [
                    (auto_name(), 1, 0, 'text', '', None, cell_style_bold_2),
                    (auto_name(), 7, 0, 'text', 'SUBTOTAL DE REGISTROS: '+result[21] , None, cell_style_bold_2),
                    (auto_name(), 12, 0, 'text', '', None, cell_style_bold_2),
                    (auto_name(), 1, 0, 'number', result[0] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[1], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[2] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[3], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[4] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[5] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[6] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[7] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[8] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[9], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[10], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[11] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[12] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[13] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[14] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[15] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[16] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[17] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[18], None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[19] , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'text','' , None, cell_style_decimal_bold),
                    (auto_name(), 1, 0, 'number', result[20] , None, cell_style_decimal_bold),
                  ]
            row_data = self.xls_row_template(c_specs,[x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

    def sheet_venta(self, _p, _xs, data, objects, wb):
        """
        :param _p:
        :param _xs:
        :param data:
        :param objects: Contains all account_invoice objects with the given period_id
        :param wb:
        :return:
        """

        invoice_obj = objects.get('account.invoice')
        # Database Cursor
        cr = objects.get('cr')

        # User ID
        uid = objects.get('uid')
        o = objects.get('objs')
        invoice_ids = invoice_obj.search(cr, uid, [('period_id','=',o.period_id.id),
                                                                      ('type','=','out_invoice'),
                                                                      ('state','in',('open','paid', 'cancel')),
                                                                      ('document_invoice_type_id.code','in',('02','18','41'))],
                                                                      order = 'date_invoice asc')

        invoice_ids = invoice_obj.browse(cr, uid, invoice_ids)

        ws = wb.add_sheet("venta")

        #STYLE DEFINITIONS
        # Here we define some useful styles for the sheet.  At any point in time, we can grab one of these to use
        cell_format = _xs['borders_all']
        cell_format_2 = "borders: top thin, bottom thin, left thin, right thin;"
        cell_format_3 = "borders: top thin;"
        cell_style = xlwt.easyxf(cell_format_2)
        cell_style_2 = xlwt.easyxf(cell_format)
        cell_style_3 = xlwt.easyxf(cell_format + _xs['left'])
        cell_style_center = xlwt.easyxf(cell_format_2 + _xs['center'])
        cell_style_date = xlwt.easyxf(cell_format + _xs['center'], num_format_str = report_xls.date_format)
        cell_style_decimal = xlwt.easyxf(cell_format + _xs['right'], num_format_str = report_xls.decimal_format)
        cell_style_bold = xlwt.easyxf(cell_format + _xs['bold'])
        cell_style_bold_2 = xlwt.easyxf(cell_format + _xs['bold'] + _xs['left'])
        cell_style_wrap_center_bold = xlwt.easyxf('font: height 180, name Arial, colour_index black, bold on, italic on; align: wrap on, vert centre, horiz centre;' + cell_format)
        cell_style_center_bold = xlwt.easyxf(cell_format_2 + _xs['center'] + 'font: bold on')
        cell_style_center_bold_2 = xlwt.easyxf(cell_format + _xs['center'] + 'font: bold on')
        cell_style_decimal_centered = xlwt.easyxf(cell_format + _xs['center'], num_format_str = report_xls.decimal_format)
        cell_style_red = xlwt.easyxf('font: height 180, name Arial, colour red, bold off, italic off; align: wrap on, vert centre, horiz left;' + cell_format)

        #Starting at row 0
        row_pos = 0

        self.auto_gen_id = 0
        def auto_name():
            self.auto_gen_id = self.auto_gen_id + 1
            return self.auto_gen_id

        #This is how we can force the height of a row, if the default does not suit us
        #For whatever reason, we have to turn the height_mismatch flag on, and then set the height.
        #ws is our sheet object.
        ws.row(0).height_mismatch = True
        ws.row(0).height = 256*2
        c_specs = [(auto_name(), 2, 0, 'text', 'Identificación', None, cell_style_wrap_center_bold),
                   (auto_name(), 7, 0, 'text', 'Cliente', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'F. Emisión', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'F. Contable', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Serie Dcto', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'No. Dcto', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Base Cero', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Base Iva', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Valor Iva', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Base No Objeto Iva', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Base Ice', None,cell_style_wrap_center_bold ),
                   (auto_name(), 1, 0, 'text', 'Valor Ice', None,cell_style_wrap_center_bold ),
                  ]

        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold)

        c_specs = [
                    (auto_name(), 9, 0, 'text', '01 - COGRALET S A   01 - GUAYAS    01 - GUAYAQUIL',None, cell_style_bold_2)
                  ]

        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold)

        c_specs = [
                    (auto_name(), 9, 0, 'text', '18-DOCUMENTO AUTRIZADO UTILIZADO EN VENTAS EXPECTO NC/ND',None, cell_style_bold_2)
                  ]

        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_bold)

        c_specs_02, c_specs_41 = [], []
        tam = 0
        for inv in invoice_ids:
            no_cancel = True
            red_style = None
            if inv.state == 'cancel':
                no_cancel = ''
                tam -= 1
                red_style = cell_style_red
            if inv.document_invoice_type_id.code == '18':
                c_specs_01 = [
                                (auto_name(), 2, 0, 'text', no_cancel and inv.partner_id.vat[2:], None, cell_style_2),
                                (auto_name(), 7, 0, 'text', no_cancel and inv.partner_id.name, None, cell_style_2),
                                (auto_name(), 1, 0, 'text', no_cancel and inv.date_invoice, None, cell_style_2),
                                (auto_name(), 1, 0, 'text', no_cancel and inv.date_invoice, None, cell_style_2),
                                (auto_name(), 1, 0, 'text', inv.internal_number.replace('-','')[:6], None, red_style or cell_style_2),
                                (auto_name(), 1, 0, 'text', inv.internal_number.replace('-','')[6:], None, red_style or cell_style_2),
                                (auto_name(), 1, 0, 'number', no_cancel and inv.base_cero_iva, None, red_style or cell_style_decimal),
                                (auto_name(), 1, 0, 'number', no_cancel and inv.base_doce_iva, None, red_style or cell_style_decimal),
                                (auto_name(), 1, 0, 'number', no_cancel and inv.vat_doce_subtotal, None, red_style or cell_style_decimal),
                                (auto_name(), 1, 0, 'number', no_cancel and inv.base_no_grava_iva, None, red_style or cell_style_decimal),
                                (auto_name(), 1, 0, 'number', no_cancel and inv.total_ice, None, red_style or cell_style_decimal),
                                (auto_name(), 1, 0, 'number', no_cancel and inv.total_ice, None, red_style or cell_style_decimal),
                             ]
                tam += 1
                row_data = self.xls_row_template(c_specs_01,[x[0] for x in c_specs_01])
                row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            elif inv.document_invoice_type_id.code == '02':
                c_specs_02 += [
                            (auto_name(), 2, 0, 'text', no_cancel and inv.partner_id.vat[2:], None, cell_style_2),
                            (auto_name(), 7, 0, 'text', no_cancel and inv.partner_id.name, None, cell_style_2),
                            (auto_name(), 1, 0, 'text', no_cancel and inv.date_invoice, None, cell_style_2),
                            (auto_name(), 1, 0, 'text', no_cancel and inv.date_invoice, None, cell_style_2),
                            (auto_name(), 1, 0, 'text', inv.internal_number.replace('-','')[:6], None, red_style or cell_style_2),
                            (auto_name(), 1, 0, 'text', inv.internal_number.replace('-','')[6:], None, red_style or cell_style_2),
                            (auto_name(), 1, 0, 'number', no_cancel and inv.base_cero_iva, None, red_style or cell_style_decimal),
                            (auto_name(), 1, 0, 'number', no_cancel and inv.base_doce_iva, None, red_style or cell_style_decimal),
                            (auto_name(), 1, 0, 'number', no_cancel and inv.vat_doce_subtotal, None, red_style or cell_style_decimal),
                            (auto_name(), 1, 0, 'number', no_cancel and inv.base_no_grava_iva, None, red_style or cell_style_decimal),
                            (auto_name(), 1, 0, 'number', no_cancel and inv.total_ice, None, red_style or cell_style_decimal),
                            (auto_name(), 1, 0, 'number', no_cancel and inv.total_ice, None, red_style or cell_style_decimal),
                            ]

            elif inv.document_invoice_type_id.code == '41':
                c_specs_41 += [
                            (auto_name(), 2, 0, 'text', no_cancel and inv.partner_id.vat[2:], None, cell_style_2),
                            (auto_name(), 7, 0, 'text', no_cancel and inv.partner_id.name, None, cell_style_2),
                            (auto_name(), 1, 0, 'text', no_cancel and inv.date_invoice, None, cell_style_2),
                            (auto_name(), 1, 0, 'text', no_cancel and inv.date_invoice, None, cell_style_2),
                            (auto_name(), 1, 0, 'text', inv.internal_number.replace('-','')[:6], None, red_style or cell_style_2),
                            (auto_name(), 1, 0, 'text', inv.internal_number.replace('-','')[6:], None, red_style or cell_style_2),
                            (auto_name(), 1, 0, 'number', no_cancel and inv.base_cero_iva, None, red_style or cell_style_decimal),
                            (auto_name(), 1, 0, 'number', no_cancel and inv.base_doce_iva, None, red_style or cell_style_decimal),
                            (auto_name(), 1, 0, 'number', no_cancel and inv.vat_doce_subtotal, None, red_style or cell_style_decimal),
                            (auto_name(), 1, 0, 'number', no_cancel and inv.base_no_grava_iva, None, red_style or cell_style_decimal),
                            (auto_name(), 1, 0, 'number', no_cancel and inv.total_ice, None, red_style or cell_style_decimal),
                            (auto_name(), 1, 0, 'number', no_cancel and inv.total_ice, None, red_style or cell_style_decimal),
                            ]

        c_specs = [
                    (auto_name(), 1, 0, 'text', '', None, cell_style_bold_2),
                  ]
        row_data = self.xls_row_template(c_specs,[x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

        c_specs = [
                    (auto_name(), 7, 0, 'text', 'TOTAL 18-DOCUMENTO AUTRIZADO UTILIZADO EN VENTAS EXPECTO NC/ND: '+str(tam) , None, cell_style_bold_2),
                  ]
        row_data = self.xls_row_template(c_specs,[x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

        if c_specs_02:
            c_specs = [
                        (auto_name(), 9, 0, 'text', '02  Nota o boleta de venta', None,
                         cell_style_bold_2),
                      ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            for c_specs in c_specs_02:
                row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
                row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

        if c_specs_41:
            c_specs = [
                        (auto_name(), 9, 0, 'text', '41  Comprobante de venta emitido por reembolso', None,
                         cell_style_bold_2),
                      ]
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)

            for c_specs in c_specs_41:
                row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
                row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_2)



    def sheet_ret_fte(_p, _xs, data, objects, wb):
        """
        :param _p:
        :param _xs:
        :param data:
        :param objects: Contains all account_invoice objects with the given period_id
        :param wb:
        :return:
        """

        # Database Cursor
        cr = objects.get('cr')

        # User ID
        uid = objects.get('uid')

        # account_invoice object
        invoice_obj = objects.get('account.invoice')

        # model sri.ats
        o = objects.get('objects')

        ws = wb.add_sheet("venta")

        #STYLE DEFINITIONS
        # Here we define some useful styles for the sheet.  At any point in time, we can grab one of these to use
        cell_format = _xs['borders_all']
        cell_format_2 = "borders: top thin, bottom thin, left thin, right thin;"
        cell_format_3 = "borders: top thin;"
        cell_style = xlwt.easyxf(cell_format_2)
        cell_style_2 = xlwt.easyxf(cell_format)
        cell_style_3 = xlwt.easyxf(cell_format + _xs['left'])
        cell_style_center = xlwt.easyxf(cell_format_2 + _xs['center'])
        cell_style_date = xlwt.easyxf(cell_format + _xs['center'], num_format_str = report_xls.date_format)
        cell_style_decimal = xlwt.easyxf(cell_format + _xs['right'], num_format_str = report_xls.decimal_format)
        cell_style_bold = xlwt.easyxf(cell_format + _xs['bold'])
        cell_style_bold_2 = xlwt.easyxf(cell_format + _xs['bold'] + _xs['left'])
        cell_style_wrap_center_bold = xlwt.easyxf('font: height 180, name Arial, colour_index black, bold on, italic on; align: wrap on, vert centre, horiz centre;' + cell_format)
        cell_style_center_bold = xlwt.easyxf(cell_format_2 + _xs['center'] + 'font: bold on')
        cell_style_center_bold_2 = xlwt.easyxf(cell_format + _xs['center'] + 'font: bold on')
        cell_style_decimal_centered = xlwt.easyxf(cell_format + _xs['center'], num_format_str = report_xls.decimal_format)
        cell_style_decimal_right = xlwt.easyxf(cell_format_2 + _xs['right'], num_format_str = report_xls.decimal_format)

        #Starting at row 0
        row_pos = 0

        invoice_ids = invoice_obj.search(cr, uid, [('period_id','=',o.period_id.id),('type','=','out_invoice'),
                                                   ('state','in',('open','paid', 'cancel')),('document_invoice_type_id.code','in',('02','18','41'))])

    def shet_ret_iva(_p, _xs, data, period_id, wb):
        pass

    def sheet_ret_iva_ctles(_p, _xs, data, period_id, wb):
        pass

    def sheet_fte_clte(_p, _xs, data, period_id, wb):
        pass

    def sheet_res_ret(_p, _xs, data, period_id, wb):
        pass

    def sheet_sec_ret(_p, _xs, data, period_id, wb):
        pass

    def ret_anulada(_p, _xs, data, period_id, wb):
        pass

    def sheet_balance(_p, _xs, data, period_id, wb):
        pass


ecua_anexo_ats_xls('report.anexo.ats.xls',
    'sri.ats',
    parser=ecua_anexo_xls_parser)