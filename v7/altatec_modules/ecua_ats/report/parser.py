import time
from report import report_sxw
from osv import osv
#import logging
#_logger = logging.getLogger(__name__)

class ecua_anexo_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(ecua_anexo_xls_parser, self).__init__(cr, uid, name, context=context)
        self.context = context
        self.localcontext.update({
            'time': time,
            'cr': cr,
            'uid': uid,
            'lang': context.get('lang', 'en_US'),
        })
