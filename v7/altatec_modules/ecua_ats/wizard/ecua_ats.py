# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors: Carlos Yumbillo                                                                         
#Copyright (C) 2013 TRESCLOUD Cia. Ltda.                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from osv import fields,osv
from datetime import *

########################################################################
# sri_ats class definition
########################################################################
class sri_ats(osv.osv_memory):

    _name = 'sri.ats'

    ########################################################################
    # act_cancelar()
    ########################################################################
    def act_cancelar(self, cr, uid, ids, context=None):
        return {'type':'ir.actions.act_window_close' }

    ########################################################################
    # act_destroy()
    ########################################################################
    def act_destroy(self, *args):
        return {'type':'ir.actions.act_window_close' }

    ########################################################################
    # act_generar_ats()
    ########################################################################

    def generate_report(self, cr, uid, ids, context=None):
        context = context or {}
        wizard = self.browse( cr, uid, ids[0], context )


        return { 'type'        : 'ir.actions.report.xml',
                 'context'     : context,
                 'report_name' : 'anexo.ats.xls',
                 'datas'       : { 'model'   : 'sri.ats',    # Report Model
                                   'res_ids' : ids
                                 },
                 'nodestroy': True
               }

    def act_generar_ats(self, cr, uid, ids, context=None):

        wizard = self.browse( cr, uid, ids[0], context )

        wizard_period_begin        = datetime.strptime( wizard.period_id.date_start, "%Y-%m-%d" )
        version_period_start_begin = datetime.strptime( wizard.version_id.period_start.date_start, "%Y-%m-%d" )

        if( wizard_period_begin < version_period_start_begin ):
            raise osv.except_osv( "Error!", "El periodo esta fuera de los periodos del version" )

        if( wizard.version_id.period_stop ):
            wizard_period_end         = datetime.strptime( wizard.period_id.date_stop,  "%Y-%m-%d" )
            version_period_stop_end   = datetime.strptime( wizard.version_id.period_stop.date_stop,  "%Y-%m-%d" )
            if( wizard_period_end > version_period_stop_end ):
                raise osv.except_osv( "Error!", "El periodo esta fuera de los periodos del version" )

        return { 'type'        : 'ir.actions.report.xml',
                 'report_name' : wizard.version_id.report.report_name,
                 'datas'       : { 'model'   : 'sri.ats',    # Report Model
                                   'res_ids' : ids
                                 },
                 'nodestroy': True
               }



    ########################################################################
    # Columns
    ########################################################################
    _columns = { 'period_id'  : fields.many2one('account.period', 'Period', required=True),
                 'state'      : fields.selection([('choose','Choose'), ('get','Get'), ], 'state', required=True, readonly=True),
                 'version_id' : fields.many2one('sri.ats.version', 'Version', required=True),
               }

    _defaults = { 'state': lambda *a: 'choose'
                }

    def default_get(self, cr, uid, fields_list, context=None):
        context = context or {}
        values = super(sri_ats, self).default_get(cr, uid, fields_list, context)
        if context.get('excel'):
            values['version_id'] = self.pool.get('sri.ats.version').search(cr, uid, [('name','=','REPORTE ATS xls')])[0]

        return values

########################################################################
# sri_ats_version class definition
########################################################################
class sri_ats_version(osv.osv):

    _name = 'sri.ats.version'

    _columns = { 'name'         : fields.char( "Nombre", required=True ),
                 'period_start' : fields.many2one( 'account.period', 'Periodo inicial', required=True),
                 'period_stop'  : fields.many2one( 'account.period', 'Periodo final', required=False ),
                 'report'       : fields.many2one( 'ir.actions.report.xml', 'Reporte', required=True ),
               }