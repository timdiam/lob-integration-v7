from openerp.osv import fields, osv

class sale_order(osv.osv):

    _inherit = 'sale.order'

    def manual_invoice(self, cr, uid, ids, context=None):

        res = super(sale_order, self).manual_invoice(cr,uid,ids,context)
        inv_obj = self.pool.get('account.invoice').browse(cr,uid,res['res_id'])
        #ADD CHECK.... IF WE HAVE A SEQUENCE DEFINED, MAKE THE FIELD READONLY
        if( inv_obj.printer_id and inv_obj.printer_id.invoice_sequence_id ):
            inv_obj.write( { 'editable_internal_number' : False } )

        #DISALLOW IF THE USERS PRINTER POINT IS NOT THE SAME AS THE ONE DEFINED ON THE SALE ORDER
        user_printer = self.pool.get('res.users').browse(cr,uid,uid,context=context).printer_id
        for sale in self.browse(cr,uid,ids, context=context):
            if sale.printer_id.id != user_printer.id:
                raise osv.except_osv(('Error!'), ('El punto de impresion de usuario no es lo mismo que la orden de venta'))
            
        return res