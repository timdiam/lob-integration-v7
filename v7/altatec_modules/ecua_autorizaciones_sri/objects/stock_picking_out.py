# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: TRESCloud Cia Ltda, Andres Calle
# Copyright (C) 2013  wwww.trescloud.com
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from openerp.osv import fields, osv
import re
from openerp.tools.translate import _
import time

class stock_picking_out(osv.osv):
    #TODO: Haerlo DRY consumiendo los metodos ya definidos en stock.picking
    
    _inherit = "stock.picking.out"
    
#     #TODO: Implementar
#     def _suggested_authorizations_id(self, cr, uid, context=None):
#         '''valor por defecto para la autorizacion en documentos nuevos
#         '''
#         
#         authorizations_id = None
#         if context and 'type' in context:
#             if context['type'] in ['out_invoice','out_refund']: #solo para ventas la autorizacion no depende de la empresa que se desconoce al momento de creacion
#                 #cargamos el dominio para la autorizacion
#                 auth_obj = self.pool.get('sri.authorizations')
#                 auth_res = auth_obj.authorization_onchange_helper(cr,uid,
#                                                                   self._doc_type(cr, uid, context), 
#                                                                   None, 
#                                                                   time.strftime('%Y-%m-%d'), 
#                                                                   self._default_printer_point(cr, uid, context)
#                                                                   ) 
#                 authorizations_id = auth_res["value"]["authorizations_id"]
#         
#         return authorizations_id
  
    _columns = { 
        'authorizations_id':fields.many2one('sri.authorizations', 'Authorization', required=False,help="It is for the authoritzation to issue the document, select a release from the list. Only existing authorizations are displayed according to the date of the document"),
     }

#     #TODO: Implementar
#     
#     _defaults = {
#         'authorizations_id': _suggested_authorizations_id,        
#     }
# 
#     #TODO: Implementar
#     def onchange_date_invoice(self, cr, uid, ids, type, partner_id=False, date_invoice=False, payment_term=False, partner_bank_id=False, company_id=False,document_invoice_type_id=False,printer_id=False):
#         '''
#         El onchange de la fecha, actualiza los valores de los campos:
#         - Autorizacion
#         - (el numero de documento en proveedores es manejado en el onchange de autorizacion)
#         '''
#         
#         res = super(account_invoice, self).onchange_date_invoice(cr, uid, ids, date_invoice)
#         if not "domain" in res:
#             res["domain"]={}
#         if not "value" in res:
#             res["value"]={}
#         if (document_invoice_type_id and type in ['out_invoice','out_refund']) or (partner_id and document_invoice_type_id and type in ['in_invoice','in_refund']):
#             #cargamos el dominio para la autorizacion
#             auth_obj = self.pool.get('sri.authorizations')
#             auth_res = auth_obj.authorization_onchange_helper(cr,uid,document_invoice_type_id, partner_id, date_invoice, printer_id, None, company_id)
#             res["value"].update({'authorizations_id':auth_res["value"]["authorizations_id"],
#                                  })
#             res["domain"].update(auth_res["domain"])
#             if auth_res["warning"]:
#                 res["warning"].update(auth_res["warning"]) 
#         else:
#             #si no hay datos validos vaciamos el formulario
#             res["value"].update({'authorizations_id':False,
#                                  })
#         return res
#     
#     #TODO: Implementar       
#     def onchange_partner_id(self, cr, uid, ids, payment_term=False, partner_bank_id=False, type=False, date_invoice=False, partner_id=False, sri_tax_support_id=False, document_invoice_type_id=False, printer_id=False, authorizations_id=False, company_id=False, context=None):
#         '''
#         El onchange del partner, actualiza los valores de los campos:
#         - Sustento Tributario
#         - (el sustento tributario actualiza el tipo de documento que a su vez actualiza la autorizacion la cual actualiza el numero de factura)
#         #TODO: Evaluar si esta funcion se puede reemplazar con un domain en el sustento tributario!
#         '''
#         
#         res = super(account_invoice, self).onchange_partner_id(cr, uid, ids, type, partner_id, date_invoice, payment_term, partner_bank_id, company_id)
#         if "value" not in res:
#             res["value"] = {}
#         if "domain" not in res:
#             res["domain"] = {}
#         if "warning" not in res:
#             res["warning"] = {}
#         
#         if partner_id:
#             #cargamos la cabecera de la factura
#             tax_support_id = False
#             invoice_header = self._prepare_invoice_header(cr, uid, partner_id, type, inv_date=date_invoice, printer_id=printer_id)
#             tax_support_ids = self.get_valid_tax_supports(cr, uid, ids, type, partner_id)
#             if tax_support_ids:
#                 tax_support_id = tax_support_ids[0]
#             
#             res["value"].update({'invoice_address':invoice_header["invoice_address"],
#                                  'invoice_phone':invoice_header["invoice_phone"],
#                                  'sri_tax_support_id': tax_support_id
#                                  })
#             #TODO: Cargar el domain de sustento tributario
#             #res["domain"]....
# 
#         else:
#             #si no hay datos validos vaciamos el formulario
#             res["value"].update({'invoice_address':False,
#                                  'invoice_phone':False,
#                                  'sri_tax_support_id':False,
#                                  })
#             #TODO: Activar el domain nulo del sustento tributario
#             #res["domain"].update({'sri_tax_support_id':[('id', '<', 0)],
#             #                     })
#         
#         #si el cambio de partner no genera cambio del sustento tributario entonces forzamos uno a fin de actualizar el formulario
#         if sri_tax_support_id == res["value"]["sri_tax_support_id"] and type in ['in_invoice','in_refund']:
#             tax_support_res = self.onchange_tax_support_id(cr, uid, ids, type, date_invoice, partner_id, sri_tax_support_id, document_invoice_type_id, printer_id, authorizations_id, company_id, context)
#             if tax_support_res["value"]:
#                 res["value"].update(tax_support_res["value"])
#             if tax_support_res["domain"]:
#                 res["domain"].update(tax_support_res["domain"])
#             if tax_support_res["warning"]:
#                 res["warning"]["message"] = str(res.get('warning') and res.get('warning').get('message')) + "\n" + str(tax_support_res.get('warning') and tax_support_res.get('warning').get('message')) 
#         return res
#     
#     #TODO: Implementar
#     def onchange_document_invoice_type_id(self, cr, uid, ids, type=False, date_invoice=False, partner_id=False, sri_tax_support_id=False, document_invoice_type_id=False, printer_id=False, authorizations_id=False, company_id=False, context=None):
#         '''
#         El onchange del tipo de documento actualiza los valores de los campos:
#         - Autorizacion
#         - (el numero de documento en proveedores es manejado en el onchange de autorizacion)
#         #TODO: Esta funcion es llamada al cambiar varios campos de la factura, debe separarse el escenario para caso
#         '''
#         res = {'value': {},'warning':{},'domain':{}}
#         if (date_invoice and document_invoice_type_id and type in ['out_invoice','out_refund']) or (date_invoice and document_invoice_type_id and partner_id and type in ['in_invoice','in_refund']):
#             #cargamos el dominio para la autorizacion
#             auth_obj = self.pool.get('sri.authorizations')
#             auth_res = auth_obj.authorization_onchange_helper(cr,uid,document_invoice_type_id, partner_id, date_invoice, printer_id, None, company_id)
#             res["value"].update({'authorizations_id':auth_res["value"]["authorizations_id"],
#                                  })
#             if not "domain" in res:
#                 res["domain"]={}
#             res["domain"].update(auth_res["domain"])
#             if auth_res["warning"]:
#                 res["warning"].update(auth_res["warning"]) 
#         else:
#             #si no hay datos validos vaciamos el formulario
#             res["value"].update({'authorizations_id':False,
#                                  })
#             res["domain"].update({'authorizations_id':[('id', '<', 0)],
#                                  })
#         return res
#     

    def onchange_printer_id(self, cr, uid, ids, type=False, date_invoice=False, partner_id=False, sri_tax_support_id=False, document_invoice_type_id=False, printer_id=False, authorizations_id=False, company_id=False, context=None):
        '''
        El onchange del punto de impresion actualiza los valores de los campos:
        - Autorizacion
        - (el numero de documento en proveedores es manejado en el onchange de autorizacion)
        #TODO: Esta funcion es llamada al cambiar varios campos de la factura, debe separarse el escenario para caso
        '''
        res = {'value': {},'warning':{},'domain':{}}

        #cargamos el dominio para la autorizacion
        auth_obj = self.pool.get('sri.authorizations')
        auth_res = auth_obj.authorization_onchange_helper(cr,uid,document_invoice_type_id, partner_id, date_invoice, printer_id, None, company_id)
        res["value"].update({'authorizations_id':auth_res["value"]["authorizations_id"]})
                
        #modificamos el prefijo en caso de ser requerido
        if 'internal_number' in auth_res['value']:
            res['value'].update({'waybill_number':auth_res["value"]["internal_number"]})
        
        if not "domain" in res:
            res["domain"]={}
        res["domain"].update(auth_res["domain"])
        if auth_res["warning"]:
            res["warning"].update(auth_res["warning"]) 
            
        return res
#     
    #TODO: Implementar
    def onchange_authorizations_id(self, cr, uid, ids, type=False, date_invoice=False, partner_id=False, sri_tax_support_id=False, document_invoice_type_id=False, printer_id=False, authorizations_id=False, company_id=False, context=None): 
        '''
        Actualiza el numero de documento
        #TODO: Centralizar este metodo, quiza en el objeto document_invoice_type_id
        '''
        res = {'value': {},'warning':{},'domain':{}}
        if date_invoice and partner_id and document_invoice_type_id and authorizations_id and type in ['in_invoice','out_invoice','in_refund','out_refund']:
            number = False
            aut = self.pool.get('sri.authorizations').browse(cr, uid, authorizations_id, context)
            if aut.document_invoice_type_id.number_format_validation:
                number = aut.shop + "-" + aut.printer_point + "-"
            res["value"].update({'internal_number': number})
        return res
# 
#     #TODO: Implementar
#     def get_valid_tax_supports(self, cr, uid, ids, type, partner_id=False, context=None):
#         '''
#         Retorna un listado de ids de sustentos tributarios validos dado un id de empresa
#         Los sustentos tributarios validos dependen del tipo de contribuyente acorde al DIMM
#         '''
#         if context is None:
#             context = {}
#         context['type'] = type
#         #TODO: Validarla en el workflow
#         if type in ['in_invoice','in_refund']:
#             #TODO: Implementar esta funcion, temporalmente retornamos el sustento por defecto
#             tax_support_id = self._suggested_tax_support(cr, uid, context)
#             return [tax_support_id] 
#         else:
#             return []
#     
#     #TODO: Implementar
#     def action_date_assign(self, cr, uid, ids, *args):
#         '''
#         Validamos la fecha dentro de la autorizacion
#         '''
#         auth_obj = self.pool.get('sri.authorizations')
#         date=False
#         res = super(account_invoice, self).action_date_assign(cr, uid, ids, args)
#         for inv in self.browse(cr, uid, ids):
#             if inv.document_invoice_type_id.number_format_validation==True or inv.document_invoice_type_id.sri_authorization_validation==True or inv.document_invoice_type_id.sri_authorization_validation_owner==True:
#                 if not auth_obj.check_date_document(cr, uid, inv.date_invoice or time.strftime('%Y-%m-%d'),inv.authorizations_id.id,context=None):
#                    raise osv.except_osv(_('Invalid action!'), _('La fecha ingresada no es valida para la autorizacion. Posiblemente la validez del documento que trata de registrar se encuentra expirado')) 
#         return res
#     
#     #TODO: Implementar
#     def invoice_validate(self, cr, uid, ids, context=None):
#         '''
#         Agrega validacion del numero de factura en base a la autorizacion
#         - fecha dentro de la autorizacion
#         - rango de autorizacion y punto de impresion (metodo check_number)
#         - tipo de documento
#         '''
#         if context is None:
#             context = {}
#         res = True
#         advances=False
#         res = super(account_invoice, self).invoice_validate(cr, uid, ids, context)
#         aut_obj=self.pool.get('sri.authorizations')
# 
#         for invoice in self.browse(cr,uid,ids,context):
#             #si el tipo de documento (ej factura) requiere validacion de la autorizacion
#             if invoice.document_invoice_type_id.sri_authorization_validation_owner or invoice.document_invoice_type_id.sri_authorization_validation_owner:
#                 
#                 if not aut_obj.check_date_document(cr, uid, invoice.date_invoice, invoice.authorizations_id.id, context):
#                     raise osv.except_osv(_('Error de Autorizacion!'), 
#                                          _('La fecha ingresada no esta entre el rango permitido por la autorizacion, debe estar entre %s y %s')%(invoice.authorizations_id.start_date,invoice.authorizations_id.expiration_date,))
#     
#                 if not aut_obj.check_number_document(cr, uid, invoice.authorizations_id, invoice.document_invoice_type_id.code, invoice.company_id, invoice.internal_number, invoice.printer_id, invoice.printer_id.shop_id, context):
#                     raise osv.except_osv(_('Error de Autorizacion!'), 
#                                          _('El numero de factura no esta entre el rango de la autorizacion esta debe estar entre %s y %s')%(invoice.authorizations_id.first_sequence,invoice.authorizations_id.last_sequence,))
# 
#                 if not aut_obj.check_type(cr, uid, invoice.authorizations_id, invoice.document_invoice_type_id, invoice.company_id, invoice.internal_number, invoice.printer_id, invoice.printer_id.shop_id, context):
#                     raise osv.except_osv(_('Error de Autorizacion!'), 
#                                          _('La autorizacion corresponde a un documento tributario distinto'))
#         return res
#         
#     #TODO: Implementar
#     #TODO: Unir con la funcion onchange helper de autorizaciones
#     def onchange_data(self, cr, uid, ids,date_invoice,partner_id,document_invoice_type_id, type=None, printer_id=None,company_id=None, context=None):
#         if context is None: context = {}
#         if not printer_id:
#             return {}
#         domain=[]
#         dom=[]
#         actual_date=time.strftime("%Y-%m-%d %H:%M:%S")
#         invoice_type2=self.pool.get('account.invoice.document.type')
#         aut_obj=self.pool.get('sri.authorizations')
#         invoice_type2_obj=invoice_type2.browse(cr,uid,document_invoice_type_id)
#         warning = {}
#         values={}
#         domain.append(('id', 'in', dom))
#         if date_invoice:
#             actual_date=date_invoice
#         if not document_invoice_type_id:
#             warning = {
#                            'title': _('Warning!!!'),
#                            'message': _('You need to choose the type of document!!!'),
#                            }
#             return {'warning': warning,'domain':{'authorizations_id': domain}}
# 
#         # Check if the document type select have a parent, in this case must use the info of parent
#         if invoice_type2_obj.parent_id:
#             document_invoice_type_id = invoice_type2_obj.parent_id.id 
#             invoice_type2_obj=invoice_type2_obj.parent_id
#             
#         if invoice_type2_obj.sri_authorization_validation==True or invoice_type2_obj.sri_authorization_validation_owner==True:
#             printer_obj = self.pool.get('sri.printer.point')
#             printer=printer_obj.browse(cr, uid, printer_id, context)
#             values = {
#                         'authorizations_id': None,
#                         'date_invoice': None,
#                       }
#             if not printer_id:
#                 warning = {
#                            'title': _('Warning!!!'),
#                            'message': _('Document must have a printer point selected to validate number!!!'),
#                            }
#                 return {'warning': warning,'domain':{'authorizations_id': domain}}
#             
#             company=self.pool.get('res.company')
#             company_obj=company.browse(cr,uid,company_id)
#             auth_line_id = aut_obj.search(cr, uid, [('document_invoice_type_id','=',document_invoice_type_id),('partner_id','=',company_obj.partner_id.id)])
#             if invoice_type2_obj.sri_authorization_validation_owner==True:
#                 company_obj=company.browse(cr,uid,company_id)
#                 partner_id=company_obj.partner_id.id
#             if auth_line_id:
#                 for line in aut_obj.browse(cr, uid, auth_line_id,context):
#                     if line.printer_point==printer.name and line.shop==printer.shop_id.number:
#                         if line.expiration_date:
#                             if line.expiration_date> actual_date:
#                                 if line.partner_id.id == partner_id:
#                                     values['authorizations_id'] = line.id
#                                     dom.append(line.id) 
#                         else:
#                              if line.partner_id.id == partner_id:
#                                     values['authorizations_id'] = line.id
#                                     dom.append(line.id) 
#                 values['internal_number'] = printer.shop_id.number+"-"+printer.name + "-"
#             else:
#                 values['authorizations_id']=False
#                 warning = {
#                            'title': _('Warning!!!'),
#                            'message': _('There is not authorization for this document type!!!'),
#                            }
#                 return {'warning': warning,'domain':{'authorizations_id': domain},'value':values}
#             domain.append(('id', 'in', dom))
#         return {'value': values,'warning':warning,'domain':{'authorizations_id': domain,'authorizations_id': domain}}
# 
#     #TODO: Implementar
#     def _prepare_invoice_header(self, cr, uid, partner_id, type, inv_date=None, printer_id=None, context=None):
#         """Retorna los valores ecuatorianos para el header de una factura
#            Puede ser usado en facturacion desde ordenes de compra, venta, proyectos, bodegas
#            @partner_id es un objeto partner
#            @type es el tipo de factura, ej. out_invoice
#            @inv_date es la fecha prevista de la factura, si no se provee se asume hoy
#         """
# 
#         if context is None:
#             context = {}
#         invoice_vals = {}
#         
#         invoice_vals = super(account_invoice, self)._prepare_invoice_header(cr, uid, partner_id, type, inv_date=inv_date, printer_id=printer_id, context=context)
#         
#         authorizations_id = None
#         if invoice_vals['printer_id']:
#             auth_obj = self.pool.get('sri.authorizations')
#             date_invoice = time.strftime("%Y-%m-%d") #TODO: Usar el formato configurado en OpenERP
#             if inv_date:
#                 date_invoice = inv_date 
#             res = auth_obj.authorization_onchange_helper(cr,uid,invoice_vals['document_invoice_type_id'], partner_id, date_invoice, invoice_vals['printer_id'], None)
#             if res['value']['authorizations_id']: #verificamos que no este vacio
#                 authorizations_id = res['value']['authorizations_id']
#                 
#         #sustento tributario es definido en el modulo ats 
#         
#         invoice_vals.update({
#                              'authorizations_id': authorizations_id,
#                              })
#         return invoice_vals
#     
    def copy(self, cr, uid, id, default=None, context=None):
        ''' 
        No copia la autorizacion electronica
        Pues esta diseniado para pertenecer a un solo documento
        '''
        
        if default is None:
            default = {}

        if context is None:
            context = {}
        result = False
        waybill = self.browse(cr, uid, id, context=context)
        if waybill.authorizations_id:
            if waybill.authorizations_id.invoice_ele:
                default.update({
                    'authorizations_id': False, #El documento electronico no debe copiarse 
                    #pues esta diseniado para pertenecer a un solo documento
                })

        return super(stock_picking_out, self).copy(cr, uid, id, default, context)
            
stock_picking_out()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
