# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: TRESCloud Cia Ltda, Andrea Garcia, Patricio Rangles
# Copyright (C) 2013  wwww.trescloud.com
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from openerp.osv import fields, osv
from openerp.tools.translate import _
from string import split
from datetime import datetime
from dateutil.relativedelta import relativedelta
import logging

_logger = logging.getLogger(__name__)
class sri_authorization(osv.osv):
    """ 
    Model for SRI Authorization, mantenido por compatibilidad
    Objeto deprecado en favor del nuevo objeto sri_authorizations (con 's' al final) 
    """
    _name = "sri.authorization"

    _columns={
              'company_id':fields.many2one('res.company', 'Company', required=False),
              'auto_printer':fields.boolean('Auto Printer',help="Computerized system that allows the direct emission of bills of sale, retention and additional documents"), 
              'name':fields.char('Authorization Number', size=37, required=True, readonly=False),
              'creation_date': fields.date('Creation Date'),
              'start_date': fields.date('Start Date',help="Date authorized for use this authorization number"),
              'expiration_date': fields.date('Expiration Date',help="Date of expiry of the authorization number"),
              # Duplicated
              #'company_id':fields.many2one('res.company', 'Company', required=False),
              'partner_id':fields.many2one('res.partner','Partner',required=False, help="Partner assigned this authorization"),
              'type_document_ids':fields.one2many('sri.type.document', 'sri_authorization_id', 'Documents Types', required=True),
              'autorization_partners':fields.boolean('Authorization for Partners',help="This field specifies that authorization is a supplier type"),
              'document_invoice_type_id':fields.many2one('account.invoice.document.type', 'Type Document'),
              #Validacion de autorizaciones a travez de campos calculados
              #'condition': fields.function(_dynamic_condition, type='boolean', string='Condition', method=True),
              }

sri_authorization()

class sri_authorizations(osv.osv):
    """ Model for SRI Authorization """
    _name = "sri.authorizations"
    _inherit = ['mail.thread']
   # _inherit="sri.authorization"
    
    def name_get(self,cr, uid, ids, context=None):
        
        """
        Funcion que permite generar una lista de autorizaciones del SRI en base 
        al sistema de autorizaciones mas sencillo para el usuario.
        
        Ahora, el tipo de documento esta ligado a la autorizacion y si se requiere 
        mas documentos para una autorizacion simplemente debe duplicarse el numero de
        autorizacion
        """

        #
        # funciones para cada uno de los casos posibles
        #-----------------------------------------------------------------------------
        def _sale_withhold(cr, uid, ids, type, partner_id):
            
            # para rertenciones en venta se requiere que la autorizacion
            # sea del partner, es decir de la empresa a la que vendo
            
            res= []
            type_id = False
            docu_obj = self.pool.get('account.invoice.document.type')
            type_ids = docu_obj.search(cr, uid, [('code', '=', type),
                                                 ('type', '=', 'out_invoice')])
            
            if type_ids:
                type_id = type_ids[0]
            else:
                return res
            list_aut = self.search(cr, uid, [('autorization_partners', '=', True),
                                             ('partner_id', '=', partner_id),
                                             ('document_invoice_type_id', '=', type_id)])
            if not module_active:           
                    for aut in self.browse(cr,uid,list_aut):
                        if aut.expiration_date and aut.start_date:
                            if aut.expiration_date>=creation_date and aut.start_date<=creation_date:
                                res.append((aut.id, aut.name))
            else:
                for aut in self.browse(cr, uid, list_aut):
                    res.append((aut.id, aut.name))
                    
            return res

        def _purchase_withhold(cr, uid, ids, type, shop, printer):
            
            # para retenciones en compra se requiere que la autorizacion
            # sea de la empresa, requiero el punto de venta y de impresion
            # TODO: Filtrar por compania
            
            res= []
            type_id = False
            docu_obj = self.pool.get('account.invoice.document.type')
            type_ids = docu_obj.search(cr, uid, [('code', '=', type),
                                                 ('type', '=', 'in_invoice')])
            
            if type_ids:
                type_id = type_ids[0]
            else:
                return res
            
            if shop and printer:
                
                shop_brow = self.pool.get('sale.shop').browse(cr, uid, shop)
                printer_brow = self.pool.get('sri.printer.point').browse(cr, uid, printer)
                
                list_aut = self.search(cr, uid, [('autorization_partners', '=', False),
                                                 ('shop', '=', shop_brow.number),
                                                 ('printer_point', '=', printer_brow.name),
                                                 ('document_invoice_type_id', '=', type_id)])
                
                if not module_active:           
                        for aut in self.browse(cr,uid,list_aut):
                            if aut.expiration_date and aut.start_date:
                                if aut.expiration_date>=creation_date and aut.start_date<=creation_date:
                                    res.append((aut.id, aut.name))
                else:
                    for aut in self.browse(cr, uid, list_aut):
                        res.append((aut.id, aut.name))

            return res

    
        def _purchase_refund_client(cr, uid, ids, partner_id, document_invoice_type_id):

            # Se requiere buscar los documentos asociados al partner y tipo de documento
            # enviado, para esto se busca primero el id por el tipo de documento
            # y luego busco en la lista de autorizaciones
            
            res= []

            if not document_invoice_type_id:
                return res
            
            list_aut = self.search(cr, uid, [('partner_id', '=', partner_id),
                                             ('document_invoice_type_id', '=', document_invoice_type_id)])
            if not module_active and creation_date:           
                for aut in self.browse(cr,uid,list_aut):
                    if aut.expiration_date and aut.start_date:
                        if aut.expiration_date>=creation_date and aut.start_date<=creation_date:
                            res.append((aut.id, aut.name))
            else:              
                for aut in self.browse(cr, uid, list_aut):
                    res.append((aut.id, aut.name))

            return res

            #
            #-----------------------------------------------------------------------------
            #

        def _invoice(cr, uid, ids, transaction, type, shop, printer, partner_id, supplier):
            return []

        #
        # Funcion Master
        #-----------------------------------------------------------------------------
        #
        res = []
        res1=[]
        if not context:
            context = {}
        if not len(ids):
            return []
        
        # Estas variables pueden enviarse a travez del context y ayudan a identificar
        # la lista de autorizaciones que el sistema puede mostrar
        transaction = context.get('transaction_type')
        document = context.get('document')
        type = context.get('document_type_code')
        shop = context.get('shop_id')
        printer = context.get('printer_id')
        partner_id = context.get('partner_id')
        supplier = context.get('is_supplier')
        document_invoice_type_id = context.get('document_invoice_type_id')
        invoice_type = context.get('type')
        creation_date=context.get('creation_date')
        module_ids = self.pool.get('ir.module.module').search(cr, uid, [('name','=','ecua_auto_fields')])
        module = self.pool.get('ir.module.module').browse(cr, uid, module_ids, context)
        module_active=False
        for mod in module:
            if mod['state']=='installed':
                module_active=True
        # si se envia estas 2 variables significa que usa el nuevo metodo 
        if transaction and document:
            
            # casos generales:
            if transaction == 'sale' and document == 'withhold' and type:
                # primer caso, es retenciones en ventas
                res = _sale_withhold(cr, uid, ids, type, partner_id)
        
            if transaction == 'purchase' and document == 'withhold':
                # segundo caso, es retenciones en compras
                res = _purchase_withhold(cr, uid, ids, type, shop, printer)
            
            if transaction == 'purchase' and document == 'refund_client':
                # caso para autorizaciones relacionadas con reemblsos para clientes
                res = _purchase_refund_client(cr, uid, ids, partner_id, document_invoice_type_id)

            # Estos 2 casos no estan implementados y pueden incluso utilizarse como uno solo                
            if transaction == 'sale' and document == 'invoice':
                # tercer caso, es facturas de venta
                res = _invoice(cr, uid, ids, transaction, type, shop, printer, partner_id, supplier)
                
            if transaction == 'purchase' and document == 'invoice':
                # cuarto caso, es facturas de compra
                res = _invoice(cr, uid, ids, transaction, type, shop, printer, partner_id, supplier)

        else: # compatibilidad con implementaciones anteriores
            '''SE COMENTO ESTA PARTE PORQUE EL BUG ESTA CORREGIDO CON NUEVAS FUNCIONALIDADES'''
#            if not ids:
#                # Este caso se da cuando se edita una factura guardada,
#                # para corregirlo se busca la lista de documentos y las 
#                # autorizaciones correspondientes
#                if document_invoice_type_id:
#                    docu_obj = self.pool.get('account.invoice.document.type')
#                    docu = docu_obj.browse(cr, uid, document_invoice_type_id)
#
#                    # Verifico si este documento tiene un padre, de ser el caso se analiza en base al padre
#                    if docu.parent_id:
#                        document_invoice_type_id = docu.parent_id.id
#                        docu = docu.parent_id
#
#                    # Condiciones a verificarse:
#                    # Tipo de factura
#                    # Tipo de documento SRI
#                    # Dueño de la autorizacion
#                    search = [('autorization_partners', '=', not docu.sri_authorization_validation_owner),
#                              ('document_invoice_type_id', '=', document_invoice_type_id)]
#                    
#                    if not docu.sri_authorization_validation_owner:
#                        search.append(('partner_id','=',partner_id))
#                    else:
#                        # Si se da el caso de ids = 0, solo se tiene el printer_id
#                        # igual se verifica en caso de no enviarse este dato
#                        if printer:
#                            printer_brow = self.pool.get('sri.printer.point').browse(cr, uid, printer)
#                            shop_brow = printer_brow.shop_id
#                            search.append(('shop', '=', shop_brow.number))
#                            search.append(('printer_point', '=', printer_brow.name))
#                        else:
#                            # si no hay el punto de impresion, no se puede conseguir el listado
#                            return res
#
#                    # Busco las autorizaciones de cliente o proveedor segun los parametros de busqueda
#                    ids = self.search(cr, uid, search)
#           
            for r in self.browse(cr, uid, ids, context):
                
                res.append((r.id, r.name))   
        if ids: # total anulacion del name_get, hasta realizar el QA
            res = []
            for r in self.browse(cr, uid, ids, context):
                res.append((r.id, r.name))   
            
        for data in res:
            r=self.browse(cr,uid,data[0])
            if r.name not in ['9999999999','0000000000']: #autorizacion por defecto y autorizacion pendiente
                name=r.name
                if r.shop and r.printer_point:
                    name=r.name+', Sucursal '+r.shop +'-'+ r.printer_point
                if r.expiration_date and not r.invoice_ele:
                    name=name+', Expira el '+r.expiration_date
                    if r.last_sequence and not r.auto_printer and not r.invoice_ele:
                        name=name+' en el numero '+str(r.last_sequence)  
                res1.append((r.id, name))
        if res1:
            res=res1
        return res  
    
    def onchange_name(self, cr, uid, ids,name,date_invoice, context=None):
            if context is None: context = {}
            warning={}
            value={}
            tam= len(name)
            warning = {
                           'title': _('Error Usuario !!!'),
                           'message': _('Usted Necesita ingresar un valor de 10 digitos o 37 digitos para el numero de autorizacion!!!'),
                           }
            if tam<10: 
                 return {'warning':warning}
            if tam<37 and tam>10:
                 return {'warning':warning}
            if tam==37:
                value['invoice_ele']= True
                value['start_date']=date_invoice
                value['expiration_date']=date_invoice
            else:
                value['invoice_ele']= False
            return {'value':value}
    
    def verify_len(self, cr, uid,name, context=None):
        tam= len(name)
        if tam<10: 
            return False
        if tam<37 and tam>10:
            return False
        return True
        
    def onchange_date_end(self, cr, uid, ids,start_date, context=None):
            if context is None: context = {}
            context['start_date']=start_date
            value={}
            if start_date:
                date_end= datetime.strptime(start_date, '%Y-%m-%d') + relativedelta(years=1)
                value['expiration_date']=date_end.strftime("%Y-%m-%d")
            else:
                value['expiration_date']=False
           
            return {'value':value}
            
    def _get_document(self, cr, uid, context=None):
        if context is None:
            context = {}
        res=[]
        invoice_type=self.pool.get('account.invoice.document.type')
        document_invoice_type_id=context.get('document_invoice_type_id') or False
        document_type_code=context.get('document_type_code') or False
        # both option are correct and work together:
        # Use the id because the user select the document type or
        # Use the code because we create a know document, for example, a retention
        invoice_type_obj = False 
        if document_invoice_type_id:
            invoice_type_obj=invoice_type.browse(cr,uid,context.get('document_invoice_type_id'))
        elif document_type_code:
            if context.get('document')=='withhold':
                if context.get('transaction_type')=='purchase':
                    invoice_type_withhold=invoice_type.search(cr,uid,[('code','=',document_type_code),('type','=','in_invoice')])
                else:
                    invoice_type_withhold=invoice_type.search(cr,uid,[('code','=',document_type_code),('type','=','out_invoice')])
                invoice_type_obj=invoice_type.browse(cr,uid,invoice_type_withhold[0])
        # Check the parent document asignement
        if invoice_type_obj and invoice_type_obj.parent_id:
             invoice_type_obj = invoice_type_obj.parent_id
        if invoice_type_obj:
            res=[invoice_type_obj.id]
        
        return res and res[0] or False  
    
    def _get_partner(self, cr, uid, context=None):
        
        if context is None:
            context = {}
        
        res=[]
        invoice_type=self.pool.get('account.invoice.document.type')
        res_partner=self.pool.get('res.partner')
        company=self.pool.get('res.company')
        invoice_type_obj=invoice_type.browse(cr,uid,context.get('document_invoice_type_id'))
        
        # In withholds the document_invoice_type_id don't exist,
        # in this case check if a sale or a purchsae withhold
        document = False #'document':'refund_client'
        transaction_type = False
        partner_id = False
        
        if 'document' in context:
            document = context['document']

        if 'transaction_type' in context:
            transaction_type = context['transaction_type']
        
        if 'partner_id' in context:
            partner_id = context['partner_id']
        
        # check condition to work with client refund 
        if document != 'refund_client' and (invoice_type_obj.sri_authorization_validation_owner or (document == 'withhold' and transaction_type == 'purchase')):
            partner_id=company.browse(cr,uid,context.get('company_id')).partner_id
            if partner_id:
                res=[partner_id.id]
        else:
            res_partner_obj=res_partner.browse(cr, uid, partner_id)
            if res_partner_obj:
                res=[res_partner_obj.id]
        return res and res[0] or False    
    
    def _get_date(self, cr, uid, context=None):
        if context is None:
            context = {}
        res=[] 
        date_invoice=context.get('date_invoice') or False 
        res=[date_invoice]   
        return res and res[0] or False  
    
    def _get_dates_start(self, cr, uid, context=None):
        if context is None:
            context = {}
        res=[] 
        date_invoice=context.get('date_invoice') or False 
        res=[date_invoice]
        return res and res[0] or False  
    
    def _get_dates_end(self, cr, uid, context=None):
        if context is None:
            context = {}
        res=[] 
        date_invoice=context.get('date_invoice') or False 
        if date_invoice:
            date_invoice= datetime.strptime(date_invoice, '%Y-%m-%d') + relativedelta(years=1)
            res=[date_invoice.strftime("%Y-%m-%d")]
        return res and res[0] or False
    def _get_fn_type(self,cr,uid,context=None):
        if context is None:
            context = {}
        res=[] 
        ttype=context.get('transaction_type') or False 
        res=[ttype] 
        return res and res[0] or False 
    def _get_type(self,cr,uid,context=None):
        if context is None:
            context = {}
        res=[] 
        type=context.get('type') or False 
        res=[type]   
        return res and res[0] or False  
    def _get_ttype(self, cr, uid, ids, field, arg, context=None):
        res = dict.fromkeys(ids, False)
        if context is None:
            context = {}
        for inv in self.browse(cr, uid, ids, context=context):
            type=context.get('transaction_type') or False 
            res[inv.id]=type   
        return res    
    _columns={
              'company_id':fields.many2one('res.company', 'Company', required=False, track_visibility='onchange'),
              'auto_printer':fields.boolean('Auto Printer',track_visibility='onchange',help="Computerized system that allows the direct emission of bills of sale, retention and additional documents"), 
              'name':fields.char('Authorization Number', size=37, required=True, readonly=False,track_visibility='onchange',help="Authorization Document issued by the SRI, 10 digits for  pre printed documents and 37 digits electronic documents"),
              'creation_date': fields.date('Creation Date'),
              'start_date': fields.date('Start Date',required=False,track_visibility='onchange',help="Date authorized for use this authorization number"),
              'expiration_date': fields.date('Expiration Date ',required=False,track_visibility='onchange',help="Date of expiry of the authorization number"),
              'partner_id':fields.many2one('res.partner','Partner',required=False,track_visibility='onchange', help="This field is automatically selected for facturadeuna.com should not modify it. It is for the company issuing authorized to own documents will be 'business' for customer documents (withholding) and suppliers (invoices) will be the name of the customer or supplier and a document."),
             # 'type_document_ids':fields.many2one('sri.type.document', 'sri_authorization_id', 'Documents Types', required=True),
              'autorization_partners':fields.boolean('Authorization for Partners',track_visibility='onchange',help="This field specifies that authorization is a supplier type"),
              'document_invoice_type_id':fields.many2one('account.invoice.document.type', 'Type Document', track_visibility='onchange',help="Es seleccionado automaticamente por facturadeuna.com, usualmente corresponde a una factura o una retencion"),
              'first_sequence': fields.integer('Initial Sequence',required=False,track_visibility='onchange',help="The number from which the authorized start sequence can be copied or printed invoice retention"),
              'last_sequence': fields.integer('Last Sequence ',required=False,track_visibility='onchange',help="The number to which the authorized ending sequence, can be copied or printed invoice retention."),
              'sequence_id':fields.many2one('ir.sequence', 'Sequence',track_visibility='onchange', required=False),
              'expired':fields.boolean('Expired?',track_visibility='onchange'),
              'automatic':fields.boolean('automatic?', required=False,track_visibility='onchange'),
              'printer_point':fields.char('Printer Point',required=False,size=3,track_visibility='onchange',help="The second set of digits of the document (if the document is 001-022-123456789 number then corresponds to 022)"),
              'shop':fields.char('Shop ',required=False,size=3,track_visibility='onchange',help="The first group digits of document (if the document is 001-022-123456789 number then corresponds to 001)"),
              'filer':fields.integer('Filer'),
              'invoice_ele':fields.boolean('Electronic Invoicing',track_visibility='onchange',help="Se marcara automaticamente si la autorizacion es de 37 digitos"),
              'date_invoice':fields.date('Date',help="Date of invoice"),
              'type':fields.char('Type'),
              'trans_type':fields.function(_get_ttype,type='char',method=True,string='Transaction type'),
              }
    _defaults={
               'filer':9,
               'invoice_ele':False,
               'partner_id':_get_partner,
               'document_invoice_type_id':_get_document,
               'date_invoice':_get_date,
               'start_date':_get_dates_start,
               'expiration_date':_get_dates_end,
               'type':_get_type,
               'shop':'001',
               'printer_point':'001',
               'trans_type':_get_fn_type,
               }
    
    def _check_name(self, cr, uid, ids, context=None):
        '''
        Verifica que el numero de autorizacion sea valido
        Por ahora solo valida numero de digitos
        a futuro validara tambien sus valores, ej. en autorizaciones electronicas
        '''
        lines = self.browse(cr, uid, ids, context=context)[0]
        n=self.search(cr,uid,[('name','=',lines.name),('document_invoice_type_id','=',lines.document_invoice_type_id.id),
                            ('shop','=',lines.shop),('printer_point','=',lines.printer_point),
                            ('autorization_partners','=',lines.autorization_partners),
                            ])
        if len(n)>1:
                return False
        return True
    _constraints = [
        (_check_name, 'You cannot create a autorization with this characteristics because there already are.', ['name']),
        ]
#    _sql_constraints = [
#                        ('name_uniq','unique(name,document_invoice_type_id,shop,printer_point,autorization_partners)','The number of authorization must be unique per partner')
#                        ]
    def check_number_document(self, cr, uid, authorization_id, document_type_code, company_id, sequence, printer_id, shop_id, context=None):
        '''
        Valida que un numero de documento este entre la secuencia inicial y final de la autorizacion
        Valida que el numero de documento concuerde el punto de impresion y la sucursal
        El numero debe ser provisto en formato 001-001-000000001
        Este metodo ha sido pensado para que sea consumido por objetos externos
        Si la autorizacion es 0000000000 se bypasea la validacion (se retorna true)
        '''
        #TODO: Si se instala el modulo ecua_auto_fields_seq se debe redefinireste metodo

        if authorization_id.name in ['0000000000','9999999999']: #util para bypasear la autorizacion electronica mientras esta en proceso
            return True
        try:
            number = sequence.split('-')

            if len(number) != 3:
                return False
            for chunk in number:
                if len(chunk) == 0:
                    return False

            #TODO: Eliminar codigo sobrante, para que vovler a obtener el objeto autorizacion si fue pasado como parametro
            document = authorization_id.document_invoice_type_id or False
            sri_auth=self.pool.get('sri.authorizations')
            authorization=sri_auth.search(cr,uid,[('name','=',authorization_id.name)])
            auts=sri_auth.browse(cr,uid,authorization)
            # se compara con el codigo, debria enviarse siempre el codigo no el id
            # ya que existe mas de un documento con el mismo codigo pero lo importante
            # es el codigo
            #_logger.debug("XXXY")
            for autorization in auts:
                document=autorization.document_invoice_type_id
                old_document_code=document.code
                _logger.debug(old_document_code)
                if document:
                    if document.parent_id: # In case that this document have parent, need check the parent
                       document = document.parent_id

                    #TODO This code is such a hack.  Why are we switching the parent above, and then just checking with the doc type that was
                    #send to the function.  This seems like something half-developed...  Fixed temporarily.  Also, terrible messages from this function.
                    # there are NUMEROUS checks in this function, but the user will only see a warning that the number is out of range...
                    _logger.debug(old_document_code)
                    _logger.debug(document_type_code)
                    _logger.debug(document.code)
                    if (document.code == document_type_code) or (old_document_code == document_type_code):#validamos el tipo de documento
                        if autorization.invoice_ele or autorization.auto_printer:
                            if (number[0] == autorization.shop) and (number[1] == autorization.printer_point):
                                ok_number = True
                                if document.sri_authorization_validation_owner: #si la autorizacion es emitida por mi empresa valido el punto de impresion
                                    if autorization.printer_point == printer_id.name and autorization.shop == printer_id.shop_id.number:
                                        ok_number = True
                                    else:
                                        ok_number = False
                        else:
                            if (int(number[2])>= autorization.first_sequence and int(number[2])<= autorization.last_sequence): #validamos la secuencia
                                if (number[0] == autorization.shop) and (number[1] == autorization.printer_point):
                                    ok_number = True
                                    if document.sri_authorization_validation_owner: #si la autorizacion es emitida por mi empresa valido el punto de impresion
                                        if autorization.printer_point == printer_id.name and autorization.shop == printer_id.shop_id.number:
                                            ok_number = True
                                        else:
                                            ok_number = False
                                                                                  
            return ok_number
        
        except:
          
            return False


    def check_type(self, cr, uid, authorization, document_type, company_id, sequence, printer_id, shop_id, context=None):
        '''
        Valida que el tipo de documento de la autorizacion corresponda al del tipo de documento
        #TODO: Validar resto de campos de la autorizacion 
        '''        
        if authorization.name in ['0000000000','9999999999']: #util para bypasear la autorizacion electronica mientras esta en proceso
            return True
        if document_type.parent_id: #si tiene documento padre se valida con el codigo padre
            document_type = document_type.parent_id
        if authorization.document_invoice_type_id.code == document_type.code:
            return True
        return False


    def create(self, cr, uid, values, context=None):
        name=values.get('name') or False
        type=context.get('type') or False
        auto_printer=values.get('auto_printer') or False
        invoice_ele=values.get('invoice_ele') or False
        if name:
            if not name.isdigit():
                raise osv.except_osv( _('Advertencia de Error!'),
                    _("En el campo nombre solo puede ingresar numeros."))
            verify=self.verify_len(cr, uid,name, context)
            if not verify:
                raise osv.except_osv( _('Error de Usuario!'),
                    _("Usted Necesita ingresar un valor de 10 digitos o 37 digitos para el numero de autorizacion!!!"))
        '''CODIGO ESPECIFICO PARA AUTOIMPRESOR.- se setea en false los valores de las secuencias
        ya que puede exister errores de usuarios de escribir la secuencia antes de seleccionar la opcion autoimpresor'''
        if auto_printer:
            first_sequence=values.get('first_sequence') or False
            last_sequence=values.get('last_sequence') or False
            if first_sequence or last_sequence:
                first_sequence=False
                last_sequence=False
        
        #si no esta instalado documentos electronicos no se pueden emitir autorizaciones electronicas
        module_ids = self.pool.get('ir.module.module').search(cr, uid, [('name','=','ecua_electronic_documents')])
        module = self.pool.get('ir.module.module').browse(cr, uid, module_ids, context)
        electronic_documents_installed=False
        for mod in module:
            if mod['state']=='installed':
                electronic_documents_installed=True
                        
        if type == 'out_invoice' and invoice_ele==True and not electronic_documents_installed:
           raise osv.except_osv( _('Error de Usuario!'),
                    _("Su empresa no está configurada como emisora de documentos electrónicos, sino únicamente de documentos pre-impresos que tienen \
                            un número de autorización del SRI de 10 dígitos"))
        elif type == 'purchase' and invoice_ele==True and not electronic_documents_installed:
            raise osv.except_osv( _('Error de Usuario!'),
                    _("Su empresa no está configurada como emisora de documentos electrónicos, sino únicamente de documentos pre-impresos que tienen \
                            un número de autorización del SRI de 10 dígitos"))

        return super(sri_authorizations, self).create(cr, uid, values, context=context)  
      
    def write(self, cr, uid, ids, values, context=None):
        if context is None:
            context = {}
        if not ids:
            return True
        name=values.get('name') or False
        auto_printer=values.get('auto_printer') or False
        if name:
            if not name.isdigit():
                raise osv.except_osv( _('Advertencia de Error!'),
                    _("En el campo nombre solo puede ingresar numeros."))
            verify=self.verify_len(cr, uid,name, context)
            if not verify:
                raise osv.except_osv( _('Error de Usuario!'),
                    _("Usted Necesita ingresar un valor de 10 digitos o 37 digitos para el numero de autorizacion!!!"))
        self._check_name(cr, uid, ids, context)
        '''CODIGO ESPECIFICO PARA AUTOIMPRESOR.- se setea en false los valores de las secuencias
        ya que puede exister errores de usuarios de escribir la secuencia antes de seleccionar la opcion autoimpresor'''
        if auto_printer:
            first_sequence=values.get('first_sequence') or False
            last_sequence=values.get('last_sequence') or False
            if first_sequence or last_sequence:
                first_sequence=False
                last_sequence=False
        '''FIN CODIGO'''
        return super(sri_authorizations, self).write(cr, uid, ids, values, context=context)
    
    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        invoice_obj=self.pool.get('account.invoice')
        invoices = invoice_obj.search(cr,uid,[('authorizations_id','in',ids),('state','not in',['draft'])])
        retention_obj=self.pool.get('account.withhold')
        retention = retention_obj.search(cr,uid,[('authorizations_id','in',ids),('state','not in',['draft'])])
        unlink_ids = []
        if invoices or retention:
             raise osv.except_osv(_('Advertencia!'), _('Usted no puede eliminar esta autorizacion por que esta asociada a una factura o retencion.'))
        else:
            for aut in self.browse(cr, uid, ids, context):
                unlink_ids.append(aut.id)
            osv.osv.unlink(self, cr, uid, unlink_ids, context=context)
        return True 

    def check_date_document(self, cr, uid, date_document=None, auth_id=None, context=None):
        '''
        Valida que un documento este dentro del periodo de validez de una autorizacion
        '''
        if not context: context = {}
        auth = self.browse(cr, uid, auth_id, context)
        if not auth_id: return True #ej. documentos electronicos emitidos no tienen autorizacion al aprobar
        if not auth.start_date and not auth.expiration_date:
            return True
        elif auth.start_date and not auth.expiration_date:
            return date_document >= auth.start_date 
        elif not auth.start_date and auth.expiration_date:
            return date_document <= auth.expiration_date
        elif auth.start_date and auth.expiration_date:
            if (date_document >= auth.start_date) and (date_document <= auth.expiration_date):
                return True
        else:
            return False
        
    def authorization_onchange_helper(self, cr, uid, document_type_id, partner_id=False, date=False, printer_id=False, sequence=False, company_id=False, context=None):
        '''
        1. Retorna el prefijo del documento cuando amerita
        2. Retorna los un diccionario (similar a un onchange) de las autorizaciones validas para las condiciones dadas
        Las condicioens pueden incluir:
        - Tipo de Documento
        - Empresa duena de la autorizacion
        - Fecha
        - Sucursal
        - Punto de Impresion
        - Secuencia (no implementado todavia)
        
        Esta funcion esta pensada para evitar construir domains repetitivamente en cada lugar donde se utiliza una autorizacion
        Se usa un diccionario para enriquecer el resultado con domain, ids
        '''
        domain=[]
        warning={}
        auth_id=[]
        res = {'domain':{'authorizations_id':[('id', '<', 0)],},
               'value':{},
               }
        document_type = False
        #verificamos si el documento requiere autorizacion, sino retornamos la 9999999999
        if document_type_id: 
            document_type_obj = self.pool.get('account.invoice.document.type')
            document_type=document_type_obj.browse(cr,uid,document_type_id)
            if not (document_type.sri_authorization_validation_owner or document_type.sri_authorization_validation):
                '''Retornar autorizacion 9999999999'''
                auth_obj = self.pool.get('sri.authorizations')
                auth_id=auth_obj.search(cr,uid,[('name','=','9999999999')])
                res["value"].update({"authorizations_id":auth_id[0]})
                domain.append(('id', 'in', auth_id))
                res["domain"]={'authorizations_id':domain}
                return res
                
        #construimos el domain a retornar
        if document_type_id:
            document_type_obj = self.pool.get('account.invoice.document.type')
            document_type=document_type_obj.browse(cr,uid,document_type_id)
            document_type_bk = document_type
            if document_type.parent_id:
                #si tiene un parent cambiamos al objeto padre
                document_type=document_type_obj.browse(cr,uid,document_type.parent_id.id)
            domain.append(('document_invoice_type_id', '=', document_type.id))
            
        if partner_id and document_type_id: #el document_type_id esta en el if porque es requerido para la evaluacion
            #verificamos si el partner somos nosotros mismos
            document_type_obj = self.pool.get('account.invoice.document.type')
            document_type=document_type_obj.browse(cr,uid,document_type_id)
            if document_type.sri_authorization_validation_owner: #sobreescribimos el partner_id
                company_obj = self.pool.get('res.company')
                if not company_id:
                    company_id = company_obj.search(cr,uid,[])[0]
                company = company_obj.browse(cr,uid,company_id)
                partner_id = company.partner_id.id
            domain.append(('partner_id', '=', partner_id))

        if date:
            #Si la autorizacion no tiene fechas se muestra
            #se coloca un OR para ese escenario
            '''
            https://answers.launchpad.net/openobject-server/+question/184864
            Here are 2 proper way to OR 3 operands:
            1. [ '|', '|', A, B, C ] <== ((A OR B) OR C)
            2. [ '|', A, '|', B, C ] <== (A OR (B OR C))
            self.search(cr, uid, ['|', '|', ('from_pty_id', 'in', (1, 2)), ('to_pty_id', 'in', (3, 4)), ('pty_rel_tye_id', 'in', (5, 6))],
                                limit=limit, context=context)
            '''
            domain.append('|')
            domain.append(('start_date', '<=', date))
            domain.append(('start_date','=',False))
            domain.append('|')
            domain.append(('expiration_date', '>=', date))
            domain.append(('expiration_date', '=', False))

        if printer_id and document_type_id: #el document_type_id esta en el if porque es requerido para la evaluacion
            #solo si el documento es propio (somos propietarios de la autorizacion)
            document_type_obj = self.pool.get('account.invoice.document.type')
            document_type=document_type_obj.browse(cr,uid,document_type_id)
            if document_type.sri_authorization_validation_owner: #sobreescribimos el partner_id
                printer_obj = self.pool.get('sri.printer.point')
                printer = printer_obj.browse(cr,uid,printer_id)
                domain.append(('shop', '=', printer.shop_id.number))
                domain.append(('printer_point', '=', printer.name))                        

        #domain = []
        if res["domain"]['authorizations_id'] != domain: #si el dominio ha cambiado y ya no es nulo
            res["domain"].update({'authorizations_id':domain})
        
        #obtenemos el valor actual y lo guardamos en auth_id
        #seleccionamos la autorizacion mas nueva
        #TODO: Ordenar por fecha
        auth_obj = self.pool.get('sri.authorizations')
        auth_ids=auth_obj.search(cr,uid,domain)

        #cargamos el prefijo del numero de documento
        if document_type and document_type.sri_authorization_validation_owner:
            if printer_id: #si no hay punto de impresion se vacia el prefijo
                if not document_type_bk.parent_id: #si el cambio es al documento padre no se toca el internal number
                    prefix = printer.shop_id.number + '-' + printer.name + '-'
                    res["value"].update({'internal_number':prefix})
            else:
                res["value"].update({'internal_number':''})
        elif not auth_ids: #si no hay autorizacion de terceros valida vaciamos el prefijo (si es propia ya puedo anticipar el prefijo del documento)
            res["value"].update({'internal_number':''})
            
                        
        if not auth_ids:
            res["value"].update({'authorizations_id':False})
            return res
        
        auth_id = auth_ids[0] if auth_ids else False
        res["value"].update({'authorizations_id':auth_id})
        
        return res
        
sri_authorizations()
