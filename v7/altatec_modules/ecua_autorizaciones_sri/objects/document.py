from openerp.osv import fields, osv

class sri_type_document(osv.osv):
  
    _name = 'sri.type.document'
    
    def _get_autoprinter(self,cr,uid,context=None):
        if context is None:
            context = {}
        res=context.get('auto_printer')
        return res or False  
    
    def _get_type_document(self, cr, uid, context=None):
        if context is None:
            context = {}
        res=[]
        invoice_type=self.pool.get('account.invoice.document.type')
        invoice_type_obj=invoice_type.browse(cr,uid,context.get('document_invoice_type_id'))
        res=[invoice_type_obj.id]
        return res and res[0] or False  
    
    _columns = {
                'first_sequence': fields.integer('Initial Sequence'),
                'last_sequence': fields.integer('Last Sequence'),
                'counter': fields.integer('counter'),
                'sri_authorization_id':fields.many2one('sri.authorization', 'Authorization', required=True),
                'sequence_id':fields.many2one('ir.sequence', 'Sequence', required=False),
                'expired':fields.boolean('Expired?',),
                'automatic':fields.boolean('automatic?', required=False),
                'name':fields.many2one('account.invoice.document.type', 'Name', required=True),
                'printer_point':fields.char('Printer Point',size=3),
                'shop':fields.char('Shop ',size=3),
                'auto_printer':fields.boolean('Auto printer'),
                }
    
    _defaults = {
                 'counter': lambda *a: 0,
                 'printer_point':'001',
                 'shop':'001',
                 'name':_get_type_document,
                 'auto_printer':_get_autoprinter,
                 }
    
sri_type_document()