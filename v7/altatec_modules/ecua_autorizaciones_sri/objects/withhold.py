# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: TRESCloud Cia Ltda, Patricio Rangles
# Copyright (C) 2013  wwww.trescloud.com
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from openerp.osv import fields, osv
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class account_withhold(osv.osv):
    
    _inherit = "account.withhold"
    _name = "account.withhold"

    def create(self, cr, uid, vals, context={}):
        if 'transaction_type' in vals and vals['transaction_type'] != 'canceled':
            vals['transaction_type_orig'] = vals['transaction_type']
        return super(account_withhold, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context={}):
        if not isinstance(ids, list):
            ids = [ids]
        if 'transaction_type' in vals and vals['transaction_type'] != 'canceled':
            vals['transaction_type_orig'] = vals['transaction_type']

        if 'state' in vals and vals['state'] == 'canceled':
            for retention in self.browse(cr, uid, ids, context=context):
                if retention.transaction_type != "canceled":
                    retention.write({'transaction_type_orig' : retention.transaction_type})

        return super(account_withhold, self).write(cr, uid, ids, vals, context=context)

    def button_set_draft(self, cr, uid, ids, context=None):
        for retention in self.browse(cr, uid, ids, context=context):
            if retention.transaction_type_orig:
                retention.write({ 'transaction_type' : retention.transaction_type_orig })
        return super(account_withhold,self).button_set_draft(cr, uid, ids, context=context)
        
    def get_first_authorization(self, cr, uid, ids, transaction_type, printer_id, partner_id, context=None):
        
        if not context:
            context = {}
            
        val = {}
        type = '07'
        type_id = False
        
        docu_obj = self.pool.get('account.invoice.document.type')
        auth_obj = self.pool.get('sri.authorizations')
        type_ids = docu_obj.search(cr, uid, [('code', '=', type )])
        
        if type_ids:
            # Se obtiene el primer registro, code debria ser unico por tipo de docuemnto
            type_id = type_ids[0]  
        else:
           # Caso de no existir el documento, se retorna 
           return val
       
        if 'value' in context:
            val = context['value']
                
        shop_id = False
        auth_id = False
       
        if transaction_type == 'purchase' or transaction_type == 'canceled':

            if printer_id:
            
                printer = self.pool.get('sri.printer.point').browse(cr, uid, printer_id, context=context)
                
                if printer.shop_id:
                    
                    shop_id = printer.shop_id.id

                    # All the information exist, search the authorization
                    shop = self.pool.get('sale.shop').browse(cr, uid, shop_id)
                    company_id = 1

                    if 'company_id' in val:
                        company_id = val['company_id']
                    else:
                        val['company_id'] = company_id 

                    aut_list = auth_obj.search(cr, uid, [('autorization_partners', '=', False),
                                                         ('shop', '=', shop.number),
                                                         ('printer_point', '=', printer.name),
                                                         ('document_invoice_type_id', '=', type_id),
                                                         ('partner_id','=',self.pool.get('res.company').browse(cr, uid, company_id).partner_id.id)], context=context)
                    
                    if aut_list:
                        auth_id = aut_list[0] 
                        
                    val.update({
                                'shop_id': shop_id,
                                'printer_id': printer_id,
                                'authorizations_id': auth_id,
                                })

        elif transaction_type == "sale":

            aut_list = auth_obj.search(cr, uid, [('autorization_partners', '=', True),
                                                 ('partner_id', '=', partner_id),
                                                 ('document_invoice_type_id', '=', type_id)])

            if aut_list:
                auth_id = aut_list[0]

            val.update({
                        'authorizations_id': auth_id,
                        })


        #TODO ALTATEC add logic for massive sale retentions
        return val

    def default_get(self, cr, uid, fields, context=None):
        """ use the inherit default, then add the authorization value """
        if not context:
            context = {}

        res = {}

        autorization_partners = False
        if 'active_id' in context:
            inv_obj = self.pool.get('account.invoice').browse(cr, uid, context['active_id'])
            res.update({'comment':inv_obj.comment,})
        if 'transaction_type' in context:

            if context['transaction_type'] == 'canceled':

                printer_id = False
                partner_id = 1 # Multicompany default

                user = self.pool.get('res.users').browse(cr, uid, uid, context=context)

                if user.printer_id:
                    printer_id = user.printer_id.id

                # need complete the data for the authorization
                res.update({'state': 'draft',
                            'transaction_type':context['transaction_type'],
                            'printer_id': printer_id,
                            'partner_id': partner_id,
                            })


            else:
                res = super(account_withhold, self).default_get(cr, uid, fields, context=context)
        # this way the function first authorization update the values,
        # sending the fields values in the context
        context['value'] = res

        if 'transaction_type' in res:
            res = self.get_first_authorization(cr, uid, False, res['transaction_type'], res['printer_id'], res['partner_id'], context=context)

            if res['transaction_type'] == 'sale':
                autorization_partners = True

        res.update({'autorization_partners': autorization_partners})

        return res
        
     
    _columns = { 'authorizations_id': fields.many2one('sri.authorizations', 'Authorization', required=False, help=""),
                 #Campo utilizado para identificar el tipo de documento de origen y alterar su funcionamiento.
                 # en este casose resscribir para agregar un tipo nuevo para los documentos cancelados
                 'transaction_type_orig' : fields.selection([
                                           ('purchase','Purchases'),
                                           ('sale','Sales'),
                                           ('sale_massive', 'Venta Masiva'),
                                           ],  'Original Transaction Type', readonly=True),
                 'transaction_type':fields.selection([
                                          ('purchase','Purchases'),
                                          ('sale','Sales'),
                                          ('canceled','Canceled'),
                                          ],  'Transaction type', required=True, readonly=True),
                 # Used to set if autorizaction is for partner o for the company
                 'autorization_partners': fields.boolean('Authorization for Partners',help="This field specifies that authorization is a supplier type"),
                 'shop_id':fields.related('printer_id','shop_id',type='many2one',relation='sale.shop',string='Shop',store=True),
                 'editable_number' : fields.boolean( "Editable Number" ),
                 'cancel_massive'  : fields.boolean('Cancelar Rango de Retenciones', default=False),
                 'from_number_retention' : fields.char('Desde Retencion', size=17, help='Inicio de retenciones a anular'),
                 'to_number_retention'   : fields.char('Hasta Retencion', size=17, help='Fin de retenciones a anular'),
               }

    _defaults = { 'editable_number' : True,
                }

    def onchange_printer_id(self, cr, uid, ids, transaction_type, printer_id, partner_id, creation_date, context=None):
        '''
        Actualizacion del dominio y valor de autorizaion para retenciones emitidas
        '''
        res = super(account_withhold, self).onchange_printer_id(cr, uid, ids, transaction_type, printer_id, partner_id, creation_date, context)
        if transaction_type in ['purchase','canceled'] and printer_id and partner_id and creation_date:
            #El dominio de autorizaciones no se puede construir por filtro en vista, usamos python
            document_type_obj = self.pool.get('account.invoice.document.type')
            document_type_id = document_type_obj.search(cr,uid,[('code','=','07'),('type','=','in_invoice')])
            assert len(document_type_id) == 1, _('A unique document type for withholds was not found') #verificamos que solo retorne un id de documento
            auth_obj = self.pool.get('sri.authorizations')
            auth_res = auth_obj.authorization_onchange_helper(cr,uid,
                                                              document_type_id[0], 
                                                              partner_id, 
                                                              creation_date, 
                                                              printer_id, 
                                                              None, None)
            res["value"].update({'authorizations_id':auth_res["value"]["authorizations_id"]})
            res["domain"].update({'authorizations_id':auth_res["domain"]["authorizations_id"]})

        elif transaction_type == 'sale' and partner_id and creation_date: #si es venta no requiere punto de impresion
            #El dominio de autorizaciones no se puede construir por filtro en vista, usamos python
            document_type_obj = self.pool.get('account.invoice.document.type')
            document_type_id = document_type_obj.search(cr,uid,[('code','=','07'),('type','=','out_invoice')])
            assert len(document_type_id) == 1, _('A unique document type for withholds was not found') #verificamos que solo retorne un id de documento
            auth_obj = self.pool.get('sri.authorizations')
            auth_res = auth_obj.authorization_onchange_helper(cr,uid,
                                                              document_type_id[0], 
                                                              partner_id, 
                                                              creation_date, 
                                                              printer_id, 
                                                              None, None)
            res["value"].update({'authorizations_id':auth_res["value"]["authorizations_id"]})
            res["domain"].update({'authorizations_id':auth_res["domain"]["authorizations_id"]})
            if "warning" in auth_res and auth_res["warning"]:
                res["warning"]["message"] = str(res.get('warning') and res.get('warning').get('message')) + "\n" + str(auth_res.get('warning') and auth_res.get('warning').get('message'))

        else:
            res['value'].update({
                        'authorizations_id': False,
                        })
            res['domain'].update({
                        'authorizations_id': "[('id', '<', 0)]", #Ninguna autorizacion
                        })

        # If this printer point has a sequence_id for the corresponding document type, make the
        # internal number non-editable
        if( printer_id ):
            printer = self.pool.get('sri.printer.point').browse( cr, uid, printer_id, context )

            editable_number = True

            if( printer.withhold_sequence_id ):
                editable_number = False

            res['value'].update({'editable_number':editable_number})

        #Mostrar desde donde inicia el punto de las retenciones
        if printer_id and partner_id and creation_date:
            printer = self.pool.get('sri.printer.point').browse(cr, uid, printer_id, context=context)
            number = printer.shop_id.number + "-" + printer.name + "-"
            res['value'].update({
                        'shop_id': printer.shop_id.id,
                        'from_number_retention': number,
                        'to_number_retention': number,
                        })
        else:
            res['value'].update({
                        'shop_id': False,
                        'from_number_retention': False,
                        'to_number_retention': False,
                        })
        return res

    def onchange_creation_date(self, cr, uid, ids, transaction_type, printer_id, partner_id, creation_date, invoice_id, context=None):    
        '''
        Actualizacion del dominio y valor de autorizaion para retenciones emitidas
        '''
        res = super(account_withhold, self).onchange_creation_date(cr, uid, ids, transaction_type, printer_id, partner_id, creation_date, invoice_id, context)
        res2 = self.onchange_printer_id(cr, uid, ids, transaction_type, printer_id, partner_id, creation_date, context)
        res["value"].update(res2["value"])
        res["domain"].update(res2["domain"])

        if "warning" in res2 and res2["warning"]:
            if ids: 
                #  Si el registro se esta creando entonces el warning generado por el
                #  onchange del default de printer_id es ignoarado pues el mismo fue 
                #  ya considerado en el onchange del printer point y no queremos dos advertencias de lo mismo 
                res["warning"]["message"] = str(res.get('warning') and res.get('warning').get('message')) + "\n" + str(res2.get('warning') and res2.get('warning').get('message')) 

        return res
    
    def onchange_authorization_id(self, cr, uid, ids, transaction_type, printer_id, partner_id, authorizations_id, context=None):
        """This function auto complete the field number with the respective value, if authorization exist """

        if not context:
            context = {}
        
        val = {}
        type = '07'
       
        if 'value' in context:
            val = context['value']

        number = False
        
        if (transaction_type == "purchase" or transaction_type == "canceled") and printer_id and authorizations_id:
            
            # use the printer for the number
            printer = self.pool.get('sri.printer.point').browse(cr, uid, printer_id, context=context)
            number = printer.name
            
            if printer.shop_id:
                
                number = printer.shop_id.number + "-" + number + "-"
            
        elif transaction_type == "sale" and authorizations_id:

            aut = self.pool.get('sri.authorizations').browse(cr, uid, authorizations_id, context=context)

            if aut.document_invoice_type_id.code == type :
                number = aut.shop + "-" + aut.printer_point + "-"
        
        val.update({'number': number})
        
        return {'value': val}
    
    
    def button_cancel_ok(self, cr, uid, ids, context=None):
        '''
        Agrega validacion del numero de factura en base a la autorizacion
        - fecha dentro de la autorizacion
        - rango de autorizacion y punto de impresion (metodo check_number)
        - #TODO: tipo de documento
        '''
        aut_obj = self.pool.get('sri.authorizations')
        withhold_cancel = self.browse(cr, uid, ids, context)[0]

        #Si va a cancelar en rango
        if withhold_cancel.cancel_massive:
            auth_withhold = withhold_cancel.from_number_retention[:9]          #dígitos que corresponden a la autorización
            int_from_withhold = int(withhold_cancel.from_number_retention[9:]) #últimos 9 dígitos de retención Desde
            int_to_withhold   = int(withhold_cancel.to_number_retention[9:])   #últimos 9 dígitos de retención Hasta

            if int_to_withhold <= int_from_withhold:
                raise osv.except_osv(_('Error!'),
                                     _('El rango de retenciones a anular debe ser de menor (Desde) a mayor (Hasta).'))

            #generar y chequear uno a uno a las retenciones
            for i in range(int_from_withhold, int_to_withhold + 1):
                fullfill_withhold = ''
                fulfill_spaces = 9 - len(str(i)) #saber cuántos espacios hay que rellenar

                for j in range(0, fulfill_spaces - 1): #rellenar los ultimos 9 dígitos
                    fullfill_withhold = fullfill_withhold + '0'
                withhold_check = auth_withhold + fullfill_withhold + str(i) #armar el número de retención

                if not aut_obj.check_date_document(cr, uid, withhold_cancel.creation_date, withhold_cancel.authorizations_id.id, context):
                    raise osv.except_osv(_('Error de Autorizacion!'),
                                     _('La fecha ingresada no esta entre el rango permitido por la autorizacion, debe estar entre %s y %s')%(withhold_cancel.authorizations_id.start_date,withhold_cancel.authorizations_id.end_date,))
                if not aut_obj.check_number_document(cr, uid, withhold_cancel.authorizations_id, '07', withhold_cancel.company_id, withhold_check, withhold_cancel.printer_id, withhold_cancel.shop_id, context):
                    raise osv.except_osv(_('Error de Autorizacion!'),
                                     _('El numero de retencion no esta entre el rango de la autorizacion esta debe estar entre %s y %s')%(withhold_cancel.authorizations_id.first_sequence,withhold_cancel.authorizations_id.last_sequence,))
                prov_withhold = {
                    'number' : withhold_check,
                    'creation_date' : withhold_cancel.creation_date,
                    'authorization_sri' : withhold_cancel.authorizations_id,

                }
                #creamos la retención para que exista una a una en Retenciones Anuladas
                created_withhold_id = self.create(cr, uid, prov_withhold, context)
                #editamos la retención como cancelada una a una
                self.write(cr, uid, created_withhold_id, {'state':'canceled'}, context)
            #si queremos que la retención que contiene el rango de las retenciones a anular esté como cancelado
            self.write(cr, uid, withhold_cancel.id, {'state':'canceled'}, context)
        else:
            for withhold in self.browse(cr, uid, ids, context):

                if not aut_obj.check_date_document(cr, uid, withhold.creation_date, withhold.authorizations_id.id, context):
                    raise osv.except_osv(_('Error de Autorizacion!'),
                                         _('La fecha ingresada no esta entre el rango permitido por la autorizacion, debe estar entre %s y %s')%(withhold.authorizations_id.start_date,withhold.authorizations_id.end_date,))
                if not aut_obj.check_number_document(cr, uid, withhold.authorizations_id, '07', withhold.company_id, withhold.number, withhold.printer_id, withhold.shop_id, context):
                    raise osv.except_osv(_('Error de Autorizacion!'),
                                         _('El numero de retencion no esta entre el rango de la autorizacion esta debe estar entre %s y %s')%(withhold.authorizations_id.first_sequence,withhold.authorizations_id.last_sequence,))

                self.write(cr, uid, withhold.id, {'state':'canceled'}, context)

        return True
    
    def action_aprove(self, cr, uid, ids, context=None):
        '''
        Agrega validacion del numero de factura en base a la autorizacion
        - fecha dentro de la autorizacion
        - rango de autorizacion y punto de impresion (metodo check_number)
        - #TODO: tipo de documento
        '''
        
        if not context:
            context = {}

        aut_obj = self.pool.get('sri.authorizations')

        for withhold in self.browse(cr, uid, ids, context):

            if not aut_obj.check_date_document(cr, uid, withhold.creation_date, withhold.authorizations_id.id, context):
                company_id = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
                if not company_id.retention_bypass_date:
                    raise osv.except_osv(_('Invalid action!'), _('The date entered is not valid for the authorization'))

            if not aut_obj.check_number_document(cr, uid, withhold.authorizations_id, '07', withhold.company_id, withhold.number, withhold.printer_id, withhold.shop_id, context):
                raise osv.except_osv(_('Error de Usuario!'), _('El número de retención: ' + str(withhold.number) + ' esta fuera del rango de la autorización, debe cambiar el número de la retención o seleccionar una autorización diferente'))

            res = super(account_withhold, self).action_aprove(cr, uid, ids, context)
                
        return True

    #rellenar con ceros Desde y Hasta
    def onchange_fullfill(self, cr, uid, ids, from_retention, to_retention, context=None):

        value = {}

        if not (from_retention or to_retention):
            return {'value': value}

        from_number_split = str.split(from_retention,"-")
        to_number_split = str.split(to_retention,"-")

        if len(from_number_split) == 3 and from_number_split[2] !="":
            if len(from_number_split[2]) < 17:
                #require auto complete
                pos = 0
                fill = 9 - len(from_number_split[2])
                for car in from_number_split[2]:
                    if car != '0':
                        break
                    pos = pos + 1

                from_number_split[2] = from_number_split[2][:pos] + "0" * fill + from_number_split[2][pos:]

                value.update({
                    'from_number_retention': from_number_split[0] + "-" + from_number_split[1] + "-" + from_number_split[2],
                            })
        if len(to_number_split) == 3 and to_number_split[2] !="":
            if len(to_number_split[2]) < 17:
                #require auto complete
                pos = 0
                fill = 9 - len(to_number_split[2])
                for car in to_number_split[2]:
                    if car != '0':
                        break
                    pos = pos + 1

                to_number_split[2] = to_number_split[2][:pos] + "0" * fill + to_number_split[2][pos:]

                value.update({
                    'to_number_retention': to_number_split[0] + "-" + to_number_split[1] + "-" + to_number_split[2],
                            })

        return {'value': value}

account_withhold()
