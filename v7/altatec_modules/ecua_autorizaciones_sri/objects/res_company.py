from openerp.osv import fields, osv, orm

class res_company(orm.Model):

    _inherit = "res.company"

    _columns = { "retention_bypass_date" : fields.boolean("Permitir retenciones con fechas afueras")
               }

    _defaults = { "retention_bypass_date" : False
                }