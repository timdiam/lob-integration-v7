# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: TRESCloud Cia Ltda - Andrea García
# Copyright (C) 2013  wwww.trescloud.com
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

{
    "name" : "Documents Authorized by SRI",
    "version" : "3.0",
    'sequence': 4,
    'complexity': "easy",
    "author" : "TRESCloud Cia Ltda.",
    "website" : "http://www.trescloud.com",
    "category" : "Ecuadorian Regulations",
    "depends" : [
                 'base',
                 'account',
                 'sale',
                 'ecua_invoice',
                 'ecua_invoice_type',
                 'ecua_tax_withhold',
                 'base_optional_quick_create',
                 ],
    "description": """
    SRI is the regulator of the tax laws in Ecuador, 
    the agency issued permits for the printing of 
    bills, withholds, etc, for each company and each agency stated,
    this is a generic authorization for all documents in Ecuador
    
    Developer:
    Andrea García
    Patricio Rangles
    
    """,
    "init_xml": [],
    "update_xml": ['security/access_data.xml',
                   'security/ir.model.access.csv',
                   'data/data.xml',
                   'views/withhold_view.xml',
                   'views/sri_authorizations_view.xml',
                   'views/sri_document_type_view.xml',
                   'views/invoice_view.xml',
                   'views/res_company.xml',
                   #'views/stock_picking_view.xml',
                   #'views/stock_picking_out_view.xml',
    ],
    "installable": True,
    "auto_install": False,
    "application": True,
}
