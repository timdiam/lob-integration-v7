from openerp.osv import fields, orm, osv

class account_voucher(osv.osv):
    _inherit = 'account.voucher'

    #TODO on March, 2nd, we comment this out
    # I'm making this decision becuase we are changing our best practice.
    # From now on, a voucher cannot return to the state Borrador, instead,
    # The user will be forced to create a new voucher.  This is the only way
    # To assure no gaps in the sequences.
    # _columns = {
    #             'fue_confirmado':fields.boolean('Ya Fue Confirmado'),
    #             'diario_anterior':fields.many2one('account.journal',string="Diario Anterior"),
    #
    #             }

    # def account_move_get(self, cr, uid, voucher_id, context=None):
    #     '''
    #     This method prepare the creation of the account move related to the given voucher.
    #
    #     :param voucher_id: Id of voucher for which we are creating account_move.
    #     :return: mapping between fieldname and value of account move to create
    #     :rtype: dict
    #     '''
    #     seq_obj = self.pool.get('ir.sequence')
    #     voucher = self.pool.get('account.voucher').browse(cr,uid,voucher_id,context)
    #
    #     #FIX FOR CHANGING JOURNAL SO MOVE DOESN"T GET TAGGED WITH THE WRONG NAME
    #     if voucher.number and (voucher.journal_id.id == voucher.diario_anterior.id):
    #         name = voucher.number
    #
    #     elif voucher.journal_id.sequence_id:
    #         if not voucher.journal_id.sequence_id.active:
    #             raise osv.except_osv(_('Configuration Error !'),
    #                 _('Please activate the sequence of selected journal !'))
    #         c = dict(context)
    #         c.update({'fiscalyear_id': voucher.period_id.fiscalyear_id.id})
    #         name = seq_obj.next_by_id(cr, uid, voucher.journal_id.sequence_id.id, context=c)
    #     else:
    #         raise osv.except_osv(_('Error!'),
    #                     _('Please define a sequence on the journal.'))
    #     if not voucher.reference:
    #         ref = name.replace('/','')
    #     else:
    #         ref = voucher.reference
    #
    #     move = {
    #         'name': name,
    #         'journal_id': voucher.journal_id.id,
    #         'narration': voucher.narration,
    #         'date': voucher.date,
    #         'ref': ref,
    #         'period_id': voucher.period_id.id,
    #     }
    #     return move


    #FIXES NEEDING TO SAVE THE DOCUMENTO SO A DIFFERENCE APPEARS POR CRUZAR
    def onchange_line_ids(self, cr, uid, ids, line_dr_ids, line_cr_ids, amount, voucher_currency, type, context=None):
        context = context or {}
#         if not line_dr_ids and not line_cr_ids:
#             return {'value':{'writeoff_amount': 0.0}}
        line_osv = self.pool.get("account.voucher.line")
        line_dr_ids = resolve_o2m_operations(cr, uid, line_osv, line_dr_ids, ['amount'], context)
        line_cr_ids = resolve_o2m_operations(cr, uid, line_osv, line_cr_ids, ['amount'], context)
        #compute the field is_multi_currency that is used to hide/display options linked to secondary currency on the voucher
        is_multi_currency = False
        #loop on the voucher lines to see if one of these has a secondary currency. If yes, we need to see the options
        for voucher_line in line_dr_ids+line_cr_ids:
            line_id = voucher_line.get('id') and self.pool.get('account.voucher.line').browse(cr, uid, voucher_line['id'], context=context).move_line_id.id or voucher_line.get('move_line_id')
            if line_id and self.pool.get('account.move.line').browse(cr, uid, line_id, context=context).currency_id:
                is_multi_currency = True
                break
        return {'value': {'writeoff_amount': self._compute_writeoff_amount(cr, uid, line_dr_ids, line_cr_ids, amount, type), 'is_multi_currency': is_multi_currency}}

    #FIXES CHECK INCREMENT PROBLEM
    #ADDS TAGS TO KNOW IF A CHECK HAS ALREADY BEEN CONFIRMED
    # def proforma_voucher(self, cr, uid, ids, context=None):
    #
    #     super(account_voucher, self).proforma_voucher(cr, uid, ids, context)
    #
    #     vo_mo_li_obj = self.pool.get('account.move')
    #
    #     for voucher in self.browse(cr, uid, ids, context):
    #         #TODO FIX THIS HACK.  WE ARE MANUALLY SUBTRACTING A NUMBER FROM THE SEQUENCE BECUASE CHECKS ARE AUTO ASSIGNED.
    #         if voucher.need_check_info and voucher.fue_confirmado:
    #             current_num = self.pool.get('ir.sequence').read(cr, uid,  voucher.journal_id.sequence_id.id, ['number_next'], context=context)['number_next']
    #             self.pool.get('ir.sequence').write(cr, uid, [voucher.journal_id.sequence_id.id,], {'number_next': current_num-1}, context=context)
    #
    #         # #TODO Decide if we should move this out of this module to be much more general
    #         # moves = self.pool.get('account.move').search(cr, uid, [('name', '=', voucher.number)])
    #         # if moves:
    #         #     raise osv.except_osv(('Error!'),
    #         #         ('Ya hay un asiento con el numero: '+voucher.number))
    #
    #     return True

    def cancel_voucher(self, cr, uid, ids, context={}):
        if not context:
            context={}
        reconcile_pool = self.pool.get('account.move.reconcile')
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        
        for id in ids:
            voucher = self.browse(cr, uid, id, context=context)
            for line in voucher.move_ids:
                if line.reconcile_id:
                    move_lines = [move_line.id for move_line in line.reconcile_id.line_id]
                    move_lines.remove(line.id)
                    reconcile_pool.unlink(cr, uid, [line.reconcile_id.id])
                    if len(move_lines) >= 2:
                        move_line_pool.reconcile_partial(cr, uid, move_lines, 'auto',context=context)
            if voucher.move_id:
                context.update({'force_cancel':1})
                move_pool.button_cancel(cr, uid, [voucher.move_id.id],context)
                move_pool.write(cr,uid,[voucher.move_id.id],{'ref':"ANULADO"})
                for ml in voucher.move_id.line_id:
                    move_line_pool.unlink(cr,uid,[ml.id])
        res = {
            'state':'cancel',
        }
        self.write(cr, uid, ids, res)
        return True
    
#This is necessary so the above function will work...
def resolve_o2m_operations(cr, uid, target_osv, operations, fields, context):
    results = []
    for operation in operations:
        result = None
        if not isinstance(operation, (list, tuple)):
            result = target_osv.read(cr, uid, operation, fields, context=context)
        elif operation[0] == 0:
            # may be necessary to check if all the fields are here and get the default values?
            result = operation[2]
        elif operation[0] == 1:
            result = target_osv.read(cr, uid, operation[1], fields, context=context)
            if not result: result = {}
            result.update(operation[2])
        elif operation[0] == 4:
            result = target_osv.read(cr, uid, operation[1], fields, context=context)
        if result != None:
            results.append(result)
    return results
