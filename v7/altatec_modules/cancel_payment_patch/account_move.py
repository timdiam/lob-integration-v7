
from openerp.osv import fields, osv, orm

#Override create to make sure we never create an account move that shares a name with another account move
#TODO decide whether or not this should be implemented as a database constraint
class account_move(orm.Model):

    _inherit = 'account.move'

    def create(self, cr, uid, vals, context=None):

        if not context:
            context = {}

        # If this move is being created by an invoice, then later down the call chain, Odoo sets the name
        # of this account move to the name of the invoice. We don't want that so we delete the invoice
        # from the context.
        if 'invoice' in context:
            del context[ 'invoice' ]

        res_id = super(account_move,self).create(cr,uid,vals,context)

        move_obj = self.browse(cr,uid,res_id,context=context)
        if move_obj.name != '/':
            moves_with_name = self.search(cr,uid,[('name','=',move_obj.name)])
            if len(moves_with_name) > 1:
                return res_id
                #raise osv.except_osv( 'Error!', 'Ya hay un asiento con el numero: ' + move_obj.name +". Por favor, revisar la secuencia del diario: "+move_obj.journal_id.name )

        return res_id

    #Override write method to make sure we never change the name of a move to one that already exists
    def write(self,cr,uid,ids,vals,context=None):
        res = super(account_move,self).write(cr,uid,ids,vals,context)

        if 'name' in vals:
            moves_with_name = self.search(cr,uid,[('name','=',vals['name'])])
            if len(moves_with_name) > 1:
                return res
                #raise osv.except_osv( 'Error!', 'Ya hay un asiento con el numero: ' + vals['name'] )

        return res


    #Override the button cancel to make sure there are none of the following connected to the move:
    # 1.invoices
    # 2.vouchers
    # 3.payslips
    # 4.hr_advances
    def button_cancel(self, cr, uid, ids, context={}):

        super(account_move,self).button_cancel(cr,uid,ids,context=context)

        if not context:
            context = {}

        if('force_cancel' not in context):
            inv_obj = self.pool.get('account.invoice')
            advance_obj = self.pool.get('hr.advance')
            payslip_obj = self.pool.get('hr.payslip')
            voucher_obj = self.pool.get('account.voucher')

            for move in self.browse(cr, uid, ids, context=context):
                inv_ids = inv_obj.search(cr,1 ,[('move_id','=',move.id)],context=context)
                payslip_ids = payslip_obj.search(cr,1 ,[('move_id','=',move.id)],context=context)
                voucher_ids = voucher_obj.search(cr,1 ,[('move_id','=',move.id)],context=context)
                advance_ids = advance_obj.search(cr,1 ,[('move_id','=',move.id)],context=context)

                if len(inv_ids) > 0:
                    raise osv.except_osv( 'Error!', 'No puedes anular este asiento.  Hay una factura ligada! ' )
                if len(payslip_ids) > 0:
                    raise osv.except_osv( 'Error!', 'No puedes anular este asiento.  Hay un Rol de pago ligada! ' )
                if len(voucher_ids) > 0:
                    raise osv.except_osv( 'Error!', 'No puedes anular este asiento.  Hay un pago ligada! ' )
                if len(advance_ids) > 0:
                    raise osv.except_osv( 'Error!', 'No puedes anular este asiento.  Hay un Anticipo / Prestamo ligada! ' )

        return True
