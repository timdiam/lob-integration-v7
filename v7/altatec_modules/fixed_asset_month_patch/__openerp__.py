# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2013 Agile Business Group sagl
#    (<http://www.agilebg.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': "Activo Fijo Month Fix",
    'version': '0.1',
    'description': """
This module allows is a patch to correctly handle depreciations by month
""",
    'author': 'AltaTec',
    'website': 'www.altatececuador.com',
    'license': 'AGPL-3',
    "depends": ['account_asset'],
    "data": [],
    "demo": [],
    "active": False,
    "installable": True
}
