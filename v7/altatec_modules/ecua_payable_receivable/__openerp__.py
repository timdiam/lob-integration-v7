{ 'name'         : 'Ecua Payable Receivable',
  'version'      : '1.0',
  'description'  : """
                   This module adds a menu to Accounting/Reporting/GenericReporting called Reporte Cartera
                   which pops up a wizard when clicked. The wizard takes a fiscal year, start and end period, and
                   a list of res.partners, and generates a report summarizing the accounts payable and receivable
                   that meet those criteria.
                   """,
  'author'       : 'Dan Haggerty',
  'website'      : 'www.altatececuador.com',
  "depends"      : [ 'base',
                     'account',
                   ],
  "data"         : [ 'views/wizard.xml',
                     'data/report.xml',
                     'data/report_by_acc_type_by_partner.xml',
                     'data/report_paid_invoices.xml',
                   ],
  "installable"  : True,
  "auto_install" : False,
}