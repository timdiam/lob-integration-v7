#################################################################################
#
# This is the parser for the ecua_payable_receivable module.
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    4/2/2015
#
#################################################################################
from report      import report_sxw
import datetime
from openerp.osv import fields, osv, orm

#################################################################################
# Parser class definition
#################################################################################
class Parser(report_sxw.rml_parse):

    column_dict = { 'EMISION' : 0, 'VENCEMIENTO'   : 1, 'PAGO'          : 2,  'DIAS'          : 3,
                    'TIPO'    : 4, 'NUM_DOCUMENTO' : 5, 'COTIZACION/OC' : 6,  'NUM_RETENCION' : 7,
                    'VALOR'   : 8, 'ABONO'         : 9, 'RETENCION'     : 10, 'SALDO'         : 11
                  }

    #################################################################################
    # __init__() definition
    #################################################################################
    def __init__(self, cr, uid, name, context):

        super(Parser, self).__init__(cr, uid, name, context)

        self.localcontext.update( { 'cr'                   : cr,
                                    'uid'                  : uid,
                                    'fiscal_year_id'       : context[ 'fiscal_year_id'  ],
                                    'period_start_id'      : context[ 'period_start_id' ],
                                    'period_end_id'        : context[ 'period_end_id'   ],
                                    'exclude_paid'         : context[ 'exclude_paid'    ],
                                    'account_type'         : context[ 'account_type'    ],
                                    'customer_sel'         : context[ 'customer_sel'    ],
                                    'supplier_sel'         : context[ 'supplier_sel'    ],
                                    'employee_sel'         : context[ 'employee_sel'    ],
                                    'filter_sel'           : context[ 'filter_sel'      ],
                                    'date_from'            : context[ 'date_from'       ],
                                    'date_to'              : context[ 'date_to'        ],
                                    'invoice_data'         : {},
                                    'cus_refund_data'      : {},
                                    'sup_refund_data'      : {},
                                    'emp_advance_data'     : {},
                                    'in_invoice_data'      : {},
                                    'lines_to_review'      : [],
                                    'ignore_list'          : [],
                                    'cus_refunds'          : [],
                                    'sup_refunds'          : [],
                                    'emp_advances'         : [],
                                    'in_invoices_resume'   : [],
                                    'get_date_today'       : self.get_date_today,
                                    'get_fiscal_year'      : self.get_fiscal_year,
                                    'get_period_start'     : self.get_period_start,
                                    'get_period_end'       : self.get_period_end,
                                    'get_partner_name'     : self.get_partner_name,
                                    'get_invoices'         : self.get_invoices,
                                    'get_move_lines'       : self.get_move_lines,
                                    'print_column'         : self.print_column,
                                    'get_total'            : self.get_total,
                                    'get_grand_total'      : self.get_grand_total,
                                    'get_lines_to_review'  : self.get_lines_to_review,
                                    'get_cus_refunds'          : self.get_cus_refunds,
                                    'get_sup_refunds'          : self.get_sup_refunds,
                                    'get_emp_advances'         : self.get_emp_advances,
                                    'get_in_invoices_resume'   : self.get_in_invoices_resume,
                                    'get_total_cus_refunds'    : self.get_total_cus_refunds,
                                    'get_total_sup_refunds'    : self.get_total_sup_refunds,
                                    'get_total_emp_advances'   : self.get_total_emp_advances,
                                    'get_partner_fiscal_position'  : self.get_partner_fiscal_position,
                                    'get_total_in_invoices_resume' : self.get_total_in_invoices_resume,
                                    'get_grand_total_cus_refund' : self.get_grand_total_cus_refund,
                                    'get_grand_total_sup_refund' : self.get_grand_total_sup_refund,
                                    'get_grand_total_emp_advances' : self.get_grand_total_emp_advances,
                                    'get_grand_total_in_invoices_resume' : self.get_grand_total_in_invoices_resume,
                                    'get_grand_total_move_lines' : self.get_grand_total_move_lines,
                                  }
                                )

    #################################################################################
    # Converts the invoice.type (out_invoice, in_invoice, etc) to a nicer name
    #################################################################################
    def convert_type_to_name(self, type ):
        if( type == 'out_invoice' ):
            return 'FACTURA DE CLIENTE'
        elif( type == 'in_invoice' ):
            return 'FACTURA DE PROVEEDOR'
        elif( type == 'out_refund' ):
            return 'NOTA DE CREDITO DE CLIENTE'
        elif( type == 'in_refund' ):
            return 'NOTA DE CREDITO DE PROVEEDOR'
        else:
            return 'FACTURA'

    #################################################################################
    # Since so many of these lines are inherited from invoice, we're gonna use
    # one function, gather_invoice_data() to do the calculations
    #################################################################################
    def gather_invoice_data(self, invoice, type, partner, lines, lines_cus_ref, lines_sup_ref, lines_emp_adv, lines_in_invoices ):

        invoice_data = []

        # Get emision
        emision = datetime.datetime.strptime( invoice.date_invoice, '%Y-%m-%d' )
        invoice_data.append( emision.strftime('%d/%m/%Y') )

        # Get vencimiento
        if( invoice.date_due ):
            vencimiento = datetime.datetime.strptime( invoice.date_due, '%Y-%m-%d' )
        else:
            vencimiento = datetime.datetime.strptime( invoice.date_invoice, '%Y-%m-%d' )
        invoice_data.append( vencimiento.strftime('%d/%m/%Y') )

        # Get pago
        pago_td = vencimiento - emision
        invoice_data.append( pago_td.days )

        # Get Dias
        dias_td =  datetime.datetime.today() - vencimiento
        invoice_data.append( dias_td.days )

        # Get Tipo
        invoice_data.append( type )

        # Get Documento Number
        num_doc = invoice.internal_number if invoice.internal_number else ''
        invoice_data.append( num_doc )

        #Obtener Cotizacion / OC
        num_cot_oc = invoice.origin if invoice.origin else ''
        invoice_data.append( num_cot_oc )

        # TODO: referencia?

        # Get Retencion Number
        num_ret = invoice.withhold_id if invoice.withhold_id else ''
        invoice_data.append( num_ret )

        # Get valor
        total_with_vat = invoice.total_with_vat if invoice.total_with_vat else 0.00
        invoice_data.append( total_with_vat )

        # Gather all account.move.line IDS from this invoice's retention
        retention_move_line_ids = []
        if( invoice.withhold_id ):
            for line in invoice.withhold_id.account_voucher_ids:
                self.localcontext[ 'ignore_list' ].append( line.id )
                retention_move_line_ids.append( line.id )

        # Get Abono by summing all of the payments that aren't included in the
        # retentions
        abono = 0.00
        for payment in invoice.payment_ids:

            self.localcontext[ 'ignore_list' ].append( payment.id )

            if( payment.id not in retention_move_line_ids ):
                if( type == 'FACTURA DE CLIENTE' or type == "NOTA DE CREDITO DE PROVEEDOR" ):
                    abono += payment.credit
                else:
                    abono += payment.debit

        invoice_data.append( abono )

        # Add all move lines from this invoice's account move to the ignore list
        if( invoice.move_id ):
            for move_line in invoice.move_id.line_id:
                self.localcontext[ 'ignore_list' ].append( move_line.id )

        # Get Retencion
        retencion = invoice.withhold_id.total if invoice.withhold_id else 0.00
        invoice_data.append( retencion )

        # Get Saldo
        saldo = total_with_vat - abono - retencion
        if( abs( saldo ) < 0.01 ): saldo = 0.00
        invoice_data.append( saldo )

        #Agregar las lineas de los payments solamente si el reporte es de cuentas pagadas
        if self.localcontext['account_type'] == 'acc_paid':

            flag = False
            for payment in invoice.payment_ids:
                if flag:
                    invoice_data = [None] * 12 #si hay mas de un pago debe mostrar las primeras 12 columnas vacias para estar alineadas en la seccion de pagos
                # Get fecha virgencia
                date = datetime.datetime.strptime( payment.date, '%Y-%m-%d' )
                invoice_data.append( date.strftime('%d/%m/%Y') )


                # Get asiento contable
                invoice_data.append( payment.move_id )

                # Get nombre
                invoice_data.append( payment.name )

                # Get diario
                invoice_data.append( payment.journal_id )

                #Valor depende de si es factura de cliente (Haber) o factura de proveedor (Debe)
                if (type == 'FACTURA DE CLIENTE'):
                    invoice_data.append(payment.credit)

                elif (type == 'FACTURA DE PROVEEDOR'):
                    invoice_data.append(payment.debit)
                else:
                    invoice_data.append(None)
                flag = True
                lines.append(invoice_data)

        elif self.localcontext['account_type'] == 'acc_rec_pay_by_partner':
            #Guardaremos las facturas por cobrar para este caso
            if (type == 'FACTURA DE CLIENTE'):
                self.localcontext['in_invoices_resume'].append(invoice_data) #para que las guarde y las imprima luego aparte
                lines_in_invoices.append(invoice_data)
                return

            elif( type != 'ANTICIPO/ROL DE PAGO' and abs( saldo - invoice.residual ) >= 0.01 ):
                invoice_data.insert( 0, partner.name )
                self.localcontext[ 'lines_to_review' ].append( invoice_data )
                return

            elif( self.localcontext['exclude_paid'] and abs( saldo ) < 0.01 ):
                return

            #Separar las notas de credito del cliente de la facturacion normal
            elif ( type == 'NOTA DE CREDITO DE CLIENTE' ):
                self.localcontext['cus_refunds'].append(invoice_data) #para que las guarde y las imprima luego aparte
                lines_cus_ref.append(invoice_data)

            #Separar las notas de debito del proveedor de la facturacion normal
            elif ( type == 'NOTA DE CREDITO DE PROVEEDOR' ):
                self.localcontext['sup_refunds'].append(invoice_data) #para que las guarde y las imprima luego aparte
                lines_sup_ref.append(invoice_data)

            #Separar los anticipos de los empleados de la facturas
            elif ( type == 'ANTICIPO/ROL DE PAGO' ):
                self.localcontext['emp_advances'].append(invoice_data) #para que las guarde y las imprima luego aparte
                lines_emp_adv.append(invoice_data)
            else:
                lines.append( invoice_data )

        # Sanity check for saldo, ignore anticipos rol de pago because saldo pendiente makes no sense for them
        # If the saldo we calculate doesn't equal invoice.residual, place this line under the "Lineas para Revisar"
        # section of the report
        elif( type != 'ANTICIPO/ROL DE PAGO' and abs( saldo - invoice.residual ) >= 0.01 ):
            invoice_data.insert( 0, partner.name )
            self.localcontext[ 'lines_to_review' ].append( invoice_data )
            return

        # If the user selected "Excluir facturas/anticipos pagados" and the saldo is 0,
        # don't put this data in the report
        elif( self.localcontext['exclude_paid'] and abs( saldo ) < 0.01 ):
            return

        #Separar las notas de credito del cliente de la facturacion normal
        elif ( type == 'NOTA DE CREDITO DE CLIENTE' ):
            self.localcontext['cus_refunds'].append(invoice_data) #para que las guarde y las imprima luego aparte
            lines_cus_ref.append(invoice_data)

        #Separar las notas de debito del proveedor de la facturacion normal
        elif ( type == 'NOTA DE CREDITO DE PROVEEDOR' ):
            self.localcontext['sup_refunds'].append(invoice_data) #para que las guarde y las imprima luego aparte
            lines_sup_ref.append(invoice_data)

        #Separar los anticipos de los empleados de la facturas
        elif ( type == 'ANTICIPO/ROL DE PAGO' ):
            self.localcontext['emp_advances'].append(invoice_data) #para que las guarde y las imprima luego aparte
            lines_emp_adv.append(invoice_data)

        # Otherwise, add this data to the invoices section of the report
        else:
            lines.append( invoice_data )


    #################################################################################
    # get_invoices() returns a list of lines containing the data for a row
    # This is the main logic of the parser
    #################################################################################
    def get_invoices( self, partner ):

        lines = []
        args  = []
        lines_cus_ref = []
        lines_sup_ref = []
        lines_emp_adv = []
        lines_in_invoices = []

        self.localcontext[ 'cus_refunds' ]  = []
        self.localcontext[ 'sup_refunds' ]  = []
        self.localcontext[ 'emp_advances' ] = []
        self.localcontext[ 'in_invoices_resume' ] = []

        #Preguntar que tipo de cuenta es
        #Cuenta por Cobrar solo debe incluir facturas de cliente
        if self.localcontext['account_type'] == 'acc_receivable':
            args = [ ( 'partner_id',    '=', partner.id ),
                     ( 'state',        '=', 'open'    ), #no facturas de saldo cero por eso excluimos a las pagadas
                     ('type', 'in', ['out_invoice', 'out_refund']) #Incluye notas de credito por el momento
                  ]


        #Cuenta por Pagar solo debe incluir facturas de proveedor
        if self.localcontext['account_type'] == 'acc_payable':
            args = [ ( 'partner_id',    '=', partner.id ),
                     ( 'state',        '=', 'open'    ), #no facturas de saldo cero por eso excluimos a las pagadas
                     ('type', 'in', ['in_invoice', 'in_refund']) #Incluye notas de credito por el momento
                  ]

        if self.localcontext['account_type'] == 'acc_paid':
            args = [ ( 'partner_id',    '=', partner.id ),
                 ( 'state',        '=', 'paid'    ), #Facturas pagadas solamente
            ]

        if self.localcontext['account_type'] == 'acc_rec_pay_by_partner':
            args = [ ( 'partner_id',    '=', partner.id ),
                 ( 'state',        '=', 'open'    ), #Facturas en estado abierto y de cliente y proveedor
            ]

        if self.localcontext['filter_sel'] == 'date_range':
            args.append(('date_invoice', '>=', self.localcontext['date_from']))
            args.append(('date_invoice', '<=', self.localcontext['date_to']))

        if self.localcontext['filter_sel'] == 'date_end':
            args.append(('date_invoice', '<=', self.localcontext['date_to']))

        if self.localcontext['filter_sel'] == 'period':
            end_date   = self.pool.get( 'account.period' ).browse( self.localcontext['cr'], self.localcontext['uid'], self.localcontext[  'period_end_id'] ).date_stop
            start_date = self.pool.get( 'account.period' ).browse( self.localcontext['cr'], self.localcontext['uid'], self.localcontext['period_start_id'] ).date_start
            args.append(('date_invoice', '<=', end_date))
            args.append( ( 'date_invoice', '>=', start_date ) )

        #-----------------------------------------------------------------------------
        # Let's start by gathering all the relevant invoices and refunds
        #-----------------------------------------------------------------------------
        invoice_obj = self.pool.get( 'account.invoice' )

        ids = invoice_obj.search( self.localcontext['cr'],
                                  self.localcontext['uid'],
                                  args,
                                  context = self.localcontext
                                )

        invoices = invoice_obj.browse( self.localcontext['cr'], self.localcontext['uid'], ids, self.localcontext )

        if invoices:
            for invoice in invoices:
                result = self.gather_invoice_data( invoice, self.convert_type_to_name(invoice.type), partner, lines, lines_cus_ref, lines_sup_ref, lines_emp_adv, lines_in_invoices )
        # else:
        #     raise osv.except_osv( 'Error', "No hay datos que mostrar para: " + partner.name + ". Por favor realice otra consulta." )

        #-----------------------------------------------------------------------------
        # Now let's gather all the relevant Payroll Advances
        #-----------------------------------------------------------------------------
        advance_obj = self.pool.get( 'hr.advance' )

        ids = advance_obj.search( self.localcontext['cr'],
                                  self.localcontext['uid'],
                                  args,
                                  context = self.localcontext
                                )

        advances = advance_obj.browse( self.localcontext['cr'], self.localcontext['uid'], ids, self.localcontext )

        for advance in advances:
            result = self.gather_invoice_data( advance, 'ANTICIPO/ROL DE PAGO', partner, lines, lines_cus_ref, lines_sup_ref, lines_emp_adv, lines_in_invoices )


        # Now we need to sort the lines by Emision date
        # TODO: COME UP WITH A METHOD OF SORTING lines


        # We'll need to sum up some of these columns later, so save them in localcontext
        invoice_data = self.localcontext[ 'invoice_data' ]
        invoice_data[ partner.id ] = lines
        self.localcontext.update( { 'invoice_data' : invoice_data } )

        # Para sumar al final los refunds del cliente
        cus_refund_data = self.localcontext[ 'cus_refund_data' ]
        cus_refund_data[ partner.id ] = lines_cus_ref
        self.localcontext.update( { 'cus_refund_data' : cus_refund_data } )

        # Para sumar al final los refunds del proveedor
        sup_refund_data = self.localcontext[ 'sup_refund_data' ]
        sup_refund_data[ partner.id ] = lines_sup_ref
        self.localcontext.update( { 'sup_refund_data' : sup_refund_data } )

        # Para sumar al final los anticipos del empleado
        emp_advance_data = self.localcontext[ 'emp_advance_data' ]
        emp_advance_data[ partner.id ] = lines_emp_adv
        self.localcontext.update( { 'emp_advance_data' : emp_advance_data } )

        # Para sumar las facturas por cobrar si la seleccion fue "Cuentas por cobrar y pagar"
        in_invoice_data = self.localcontext[ 'in_invoice_data' ]
        in_invoice_data[ partner.id ] = lines_in_invoices
        self.localcontext.update( { 'in_invoice_data' : in_invoice_data } )

        return lines

    #################################################################################
    # get_move_lines() returns a list of lines containing the account.move.line data
    #################################################################################
    def get_move_lines( self, partner ):

        cr              = self.localcontext[              'cr' ]
        uid             = self.localcontext[             'uid' ]
        period_end_id   = self.localcontext[   'period_end_id' ]
        period_start_id = self.localcontext[ 'period_start_id' ]

        lines = []

        args = [ ( 'partner_id', '=', partner.id ),
                 ( 'id', 'not in', self.localcontext[ 'ignore_list' ] ),
                 ( 'account_id.type' , 'in', ['receivable','payable'] ),
                 ( 'account_id.reconcile', '=', True),
                 # ( 'reconcile_partial_id', '!=', False ),
               ]

        if self.localcontext['filter_sel'] == 'date_range':
            args.append(('date', '>=', self.localcontext['date_from']))
            args.append(('date', '<=', self.localcontext['date_to']))

        if self.localcontext['filter_sel'] == 'date_end':
            args.append(('date', '<=', self.localcontext['date_to']))

        if self.localcontext['filter_sel'] == 'period':
            end_date   = self.pool.get( 'account.period' ).browse( cr, uid, period_end_id ).date_stop
            start_date = self.pool.get( 'account.period' ).browse( cr, uid, period_start_id ).date_start
            args.append(('date', '<=', end_date))
            args.append( ( 'date', '>=', start_date ) )

         #-----------------------------------------------------------------------------
        # Now let's gather all the relevant account.move.line's
        #-----------------------------------------------------------------------------
        move_line_obj = self.pool.get( 'account.move.line' )

        ids        = move_line_obj.search( cr, uid, args )
        move_lines = move_line_obj.browse( cr, uid, ids )

        for line in move_lines:

            move_line_data = []

            # Get fecha virgencia
            date = datetime.datetime.strptime( line.date, '%Y-%m-%d' )
            move_line_data.append( date.strftime('%d/%m/%Y') )

            # Get diario
            move_line_data.append( line.journal_id )

            # Get nombre
            move_line_data.append( line.name )

            # Get referencia
            move_line_data.append( line.ref )

            # Get cuenta
            move_line_data.append( line.account_id )

            # Get asiento contable
            move_line_data.append( line.move_id )

            # Get tipo and document
            invoice_ids       = self.pool.get('account.invoice').search( cr, uid, [('move_id', '=', line.move_id.id)] )
            voucher_ids       = self.pool.get('account.voucher').search( cr, uid, [('move_id', '=', line.move_id.id)] )
            anticipo_rrhh_ids = self.pool.get(     'hr.advance').search( cr, uid, [('move_id', '=', line.move_id.id)] )

            if( invoice_ids ):
                move_line_data.append( 'FACTURA' )
                move_line_data.append( self.pool.get('account.invoice').browse(cr,uid,invoice_ids[0]).internal_number )
            elif( voucher_ids ):
                move_line_data.append( 'PAGO' )
                move_line_data.append(self.pool.get('account.voucher').browse(cr,uid,voucher_ids[0]).number )
            elif( anticipo_rrhh_ids ):
                move_line_data.append( 'ANTICIPO ROL DE PAGO' )
                move_line_data.append( self.pool.get('hr.advance').browse(cr,uid,anticipo_rrhh_ids[0]).number )
            else:
                move_line_data.append( '' )
                move_line_data.append( '' )

            # Get conciliacion
            move_line_data.append( line.reconcile_partial_id )

            # Get debe
            move_line_data.append( line.debit )

            # Get haber
            move_line_data.append( line.credit )

            lines.append( move_line_data )

            # COMMENTING ALL OF THIS OUT FOR NOW
            # # Now get all account.move.lines that share this line's partial reconcile id:
            # ids = move_line_obj.search( cr,
            #                             uid,
            #                             [ ( 'reconcile_partial_id',  '=', line.reconcile_partial_id.id ),
            #                               ( 'id'                  , '!=', line.id                      )
            #                             ],
            #                           )
            #
            # other_move_lines = move_line_obj.browse( cr, uid, ids )
            # other_move_lines.insert( 0, line )
            #
            # # Make a row in the report for all account.move.lines that share this partial reconcile id
            # for move_line in other_move_lines:
            #
            #     move_line_data = []
            #
            #     # Get fecha virgencia
            #     date = datetime.datetime.strptime( move_line.date, '%Y-%m-%d' )
            #     move_line_data.append( date.strftime('%d/%m/%Y') )
            #
            #     # Get diario
            #     move_line_data.append( move_line.journal_id )
            #
            #     # Get nombre
            #     move_line_data.append( move_line.name )
            #
            #     # Get referencia
            #     move_line_data.append( move_line.ref )
            #
            #     # Get cuenta
            #     move_line_data.append( move_line.account_id )
            #
            #     # Get asiento contable
            #     move_line_data.append( move_line.move_id )
            #
            #     # Get tipo and document
            #     invoice_ids       = self.pool.get('account.invoice').search( cr, uid, [('move_id', '=', move_line.move_id.id)] )
            #     voucher_ids       = self.pool.get('account.voucher').search( cr, uid, [('move_id', '=', move_line.move_id.id)] )
            #     anticipo_rrhh_ids = self.pool.get(     'hr.advance').search( cr, uid, [('move_id', '=', move_line.move_id.id)] )
            #
            #     if( invoice_ids ):
            #         move_line_data.append( 'FACTURA' )
            #         move_line_data.append( self.pool.get('account.invoice').browse(cr,uid,invoice_ids[0]).internal_number )
            #     elif( voucher_ids ):
            #         move_line_data.append( 'PAGO' )
            #         move_line_data.append(self.pool.get('account.voucher').browse(cr,uid,voucher_ids[0]).number )
            #     elif( anticipo_rrhh_ids ):
            #         move_line_data.append( 'ANTICIPO ROL DE PAGO' )
            #         move_line_data.append( self.pool.get('hr.advance').browse(cr,uid,anticipo_rrhh_ids[0]).number )
            #     else:
            #         move_line_data.append( '' )
            #         move_line_data.append( '' )
            #
            #     # Get conciliacion
            #     move_line_data.append( move_line.reconcile_partial_id )
            #
            #     # Get debe
            #     move_line_data.append( move_line.debit )
            #
            #     # Get haber
            #     move_line_data.append( move_line.credit )
            #
            #     lines.append( move_line_data )

        return lines

    #################################################################################
    # get_lines_to_review() returns a list of all the lines that were erroneous
    #################################################################################
    def get_lines_to_review( self ):
        return self.localcontext[ 'lines_to_review' ]

    #################################################################################
    # REFUNDS CLIENTE
    #################################################################################
    def get_cus_refunds( self ):
        return self.localcontext[ 'cus_refunds' ]

    #################################################################################
    # REFUNDS PROVEEDOR
    #################################################################################
    def get_sup_refunds( self ):
        return self.localcontext[ 'sup_refunds' ]

    #################################################################################
    # ANTICIPOS DEL EMPLEADO
    #################################################################################
    def get_emp_advances( self ):
        return self.localcontext[ 'emp_advances' ]

    #################################################################################
    # FACTURAS POR COBRAR POR SEPARADO SI SE ELIGIO "Cuentas por Cobrar y Pagar"
    #################################################################################
    def get_in_invoices_resume( self ):
        return self.localcontext[ 'in_invoices_resume' ]

    #################################################################################
    # get_date_today() returns current date as string
    #################################################################################
    def get_date_today( self ):
        return datetime.date.today().strftime( '%d/%m/%Y' )

    #################################################################################
    # get_fiscal_year() returns the user selected fiscal year as a string
    #################################################################################
    def get_fiscal_year( self ):
        if( self.localcontext[ 'fiscal_year_id' ]):
            return self.pool.get( 'account.fiscalyear' ).browse( self.localcontext['cr'],
                                                                 self.localcontext['uid'],
                                                                 self.localcontext['fiscal_year_id'],
                                                                 self.localcontext
                                                               ).name
        else:
            return False

    #################################################################################
    # get_period_start() returns the user selected period_start as a string
    #################################################################################
    def get_period_start( self ):
        if( self.localcontext[ 'period_start_id' ] ):
            return self.pool.get( 'account.period' ).browse( self.localcontext['cr'],
                                                             self.localcontext['uid'],
                                                             self.localcontext['period_start_id'],
                                                             self.localcontext
                                                           ).name
        else:
            return False

    #################################################################################
    # get_period_end() returns the user selected period_end as a string
    #################################################################################
    def get_period_end( self ):
        if( self.localcontext[ 'period_end_id' ] ):
            return self.pool.get( 'account.period' ).browse( self.localcontext['cr'],
                                                             self.localcontext['uid'],
                                                             self.localcontext['period_end_id'],
                                                             self.localcontext
                                                           ).name
        else:
            return False

    #################################################################################
    # get_partner_name() returns the partner's name as a string
    #################################################################################
    def get_partner_name( self, partner ):
        return self.pool.get( 'res.partner' ).browse( self.localcontext['cr'],
                                                      self.localcontext['uid'],
                                                      partner.id,
                                                      self.localcontext
                                                    ).name

    #################################################################################
    # get_lines_to_review() returns a list of all the lines that were erroneous
    #################################################################################
    def get_partner_fiscal_position( self, partner ):
        if partner.property_account_position:
            return partner.property_account_position
        else:
            return None

    #################################################################################
    # get_lines() returns a list of lines (dictionaries) containing the data for a row
    #################################################################################
    def print_column( self, column ):
        return column

    #################################################################################
    # get_tota() sums the specified column for the specified partner
    #################################################################################
    def get_total( self, partner, column ):

        total = 0.00

        for row in self.localcontext[ 'invoice_data' ][ partner.id ]:
            if( self.column_dict[ column ] < len(row) ):
                total += row[ self.column_dict[ column ] ]

        return total

    #################################################################################
    # get_total_refunds() sums the specified column for the specified partner for refunds
    #################################################################################
    def get_total_cus_refunds( self, partner, column ):

        total = 0.00

        for row in self.localcontext[ 'cus_refund_data' ][ partner.id ]:
            if( self.column_dict[ column ] < len(row) ):
                total += row[ self.column_dict[ column ] ]

        return total

    #################################################################################
    # get_total_refunds() sums the specified column for the specified partner for refunds
    #################################################################################
    def get_total_sup_refunds( self, partner, column ):

        total = 0.00

        for row in self.localcontext[ 'sup_refund_data' ][ partner.id ]:
            if( self.column_dict[ column ] < len(row) ):
                total += row[ self.column_dict[ column ] ]

        return total

    #################################################################################
    # get_total_emp_advances() sums the specified column for the specified employee for advances
    #################################################################################
    def get_total_emp_advances( self, partner, column ):

        total = 0.00

        for row in self.localcontext[ 'emp_advance_data' ][ partner.id ]:
            if( self.column_dict[ column ] < len(row) ):
                total += row[ self.column_dict[ column ] ]

        return total

    #################################################################################
    # get_total_in_invoices_resume() sums the specified column for the specified partner for in invoices
    #################################################################################
    def get_total_in_invoices_resume( self, partner, column ):

        total = 0.00

        for row in self.localcontext[ 'in_invoice_data' ][ partner.id ]:
            if( self.column_dict[ column ] < len(row) ):
                total += row[ self.column_dict[ column ] ]

        return total


    #################################################################################
    # get_grand_total() returns the total saldo across all clients
    #################################################################################
    def get_grand_total( self ):

        total = 0.00

        for partner_id in self.localcontext[ 'invoice_data' ]:
            for row in self.localcontext[ 'invoice_data' ][ partner_id ]:
                if( self.column_dict[ 'SALDO' ] < len( row ) ):
                    total += row[ self.column_dict[ 'SALDO' ] ]

        return total

     #################################################################################
    # get_grand_total_refunds() retorna el valor total de las N/C N/D de los clientes
    #################################################################################
    def get_grand_total_cus_refund( self ):

        total = 0.00

        for partner_id in self.localcontext[ 'cus_refund_data' ]:
            for row in self.localcontext[ 'cus_refund_data' ][ partner_id ]:
                if( self.column_dict[ 'SALDO' ] < len( row ) ):
                    total += row[ self.column_dict[ 'SALDO' ] ]

        return total

     #################################################################################
    # get_grand_total_refunds() retorna el valor total de las N/C N/D de los proveedores
    #################################################################################
    def get_grand_total_sup_refund( self ):

        total = 0.00

        for partner_id in self.localcontext[ 'sup_refund_data' ]:
            for row in self.localcontext[ 'sup_refund_data' ][ partner_id ]:
                if( self.column_dict[ 'SALDO' ] < len( row ) ):
                    total += row[ self.column_dict[ 'SALDO' ] ]

        return total

     #################################################################################
    # get_grand_total_emp_advances() retorna el valor total de los anticipos
    #################################################################################
    def get_grand_total_emp_advances( self ):

        total = 0.00

        for partner_id in self.localcontext[ 'emp_advance_data' ]:
            for row in self.localcontext[ 'emp_advance_data' ][ partner_id ]:
                if( self.column_dict[ 'SALDO' ] < len( row ) ):
                    total += row[ self.column_dict[ 'SALDO' ] ]

        return total

    #################################################################################
    # get_grand_total_emp_advances() retorna el valor total de los anticipos
    #################################################################################
    def get_grand_total_in_invoices_resume( self ):

        total = 0.00

        for partner_id in self.localcontext[ 'in_invoice_data' ]:
            for row in self.localcontext[ 'in_invoice_data' ][ partner_id ]:
                if( self.column_dict[ 'SALDO' ] < len( row ) ):
                    total += row[ self.column_dict[ 'SALDO' ] ]

        return total

    #################################################################################
    # get_grand_total_emp_advances() retorna el valor total de los anticipos
    #################################################################################
    def get_grand_total_move_lines( self ):

        total = 0.00

        for partner_id in self.localcontext[ 'in_invoice_data' ]:
            for row in self.localcontext[ 'in_invoice_data' ][ partner_id ]:
                if( self.column_dict[ 'SALDO' ] < len( row ) ):
                    total += row[ self.column_dict[ 'SALDO' ] ]

        return total