# -*- coding: utf-8 -*-
#################################################################################
#
# This module generates a report showing the state of the accounts payable/receivable
# ordered by clients/providers.
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    4/2/2015
#
#################################################################################
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime

#################################################################################
# ecua.payable.receivable object definition
#################################################################################
class ecua_payable_receivable_wizard( osv.osv_memory ):

    _name= 'ecua.payable.receivable.wizard'

    #################################################################################
    # Cancelar button
    #################################################################################
    def cancelar(self, cr, uid, ids, context=None):
        return


    #################################################################################
    # Crear button
    #################################################################################
    def crear_reporte(self, cr, uid, ids, context=None):

        wizard = self.browse( cr, uid, ids )[ 0 ]

        #if( not wizard.period_end ):
            #raise orm.except_orm( 'Error', "Debe elegir un periodo final" )
        if( not wizard.partners ):
            raise orm.except_orm( 'Error', "Debe elegir al menos uno cliente/proveedor" )

        context.update( { 'fiscal_year_id'  : wizard.fiscal_year.id,
                          'period_start_id' : wizard.period_start.id,
                          'period_end_id'   : wizard.period_end.id,
                          'exclude_paid'    : wizard.exclude_paid,
                          'account_type'    : wizard.account_type,
                          'customer_sel'    : wizard.customer_sel,
                          'supplier_sel'    : wizard.supplier_sel,
                          'employee_sel'    : wizard.employee_sel,
                          'filter_sel'      : wizard.filter,
                          'date_from'       : wizard.date_from,
                          'date_to'         : wizard.date_to,

                        }
                      )

        partner_id_list = []
        for partner in wizard.partners:
            partner_id_list.append( partner.id )

        if wizard.account_type == 'acc_paid':
            return { 'type'        : 'ir.actions.report.xml',
                     'context'     : context,
                     'report_name' : 'cuentas_pagadas',
                     'datas'       : { 'ids' : partner_id_list }
                   }
        elif wizard.account_type == 'acc_rec_pay_by_partner':
            return { 'type'        : 'ir.actions.report.xml',
                     'context'     : context,
                     'report_name' : 'cuentas_cobrar_pagar',
                     'datas'       : { 'ids' : partner_id_list }
                    }
        else:
            return { 'type'        : 'ir.actions.report.xml',
                     'context'     : context,
                     'report_name' : 'ecua_payable_receivable',
                     'datas'       : { 'ids' : partner_id_list }
                   }


    def onchange_partner(self, cr, uid, ids, customer_sel, supplier_sel, employee_sel, context=None):
        res = {}
        args = []

        if customer_sel and supplier_sel:
            res = {'domain': {'partners': []}}

        elif supplier_sel or employee_sel:
            res = {'domain': {'partners': [('supplier', '=', True)]}}

        elif customer_sel:
            res = {'domain': {'partners': [('customer', '=', True)]}}

        return res

    #Retorna el último id del año fiscal
    def _get_fiscal_year(self, cr, uid, ids, context=None):

        ids = self.pool.get( 'account.fiscalyear' ).search(cr, uid, [], context)
        last_id = len(ids)-1

        return ids[last_id]


    #################################################################################
    # Column definition
    #################################################################################
    _columns = { "fiscal_year"  : fields.many2one( "account.fiscalyear", string="Ejercicio Fiscal"),
                 "period_start" : fields.many2one( "account.period", string="Periodo de inicio"),
                 "period_end"   : fields.many2one( "account.period", string="Periodo final"),
                 "date_from"    : fields.date("Desde:"),
                 "date_to"      : fields.date("Hasta:"),
                 "exclude_paid" : fields.boolean( "Excluir Facturas/Anticipos Pagados" ),
                 "partners"     : fields.many2many('res.partner','partners_rel','wizard_id','partner_id', required = True, string='Partners'),
                 "account_type" : fields.selection([('acc_receivable','Cuentas por Cobrar'),
                                                    ('acc_payable','Cuentas por Pagar'),
                                                    ('acc_paid','Cuentas Pagadas'),
                                                    ('acc_rec_pay_by_partner','Cuentas por Cobrar y Pagar')], required = True, string='Consultar por'),
                 "customer_sel" : fields.boolean("Cliente"),
                 "supplier_sel" : fields.boolean("Proveedor"),
                 "employee_sel" : fields.boolean("Empleado"),
                 "filter"       : fields.selection([('no_filter', 'Sin Filtro'),
                                                    ('date_range', 'Rango de Fecha'),
                                                    ('date_end', 'Fecha de Corte'),
                                                    ('period', 'Periodo')], 'Tipo de Filtro'),
               }

    _defaults = {
        "fiscal_year" : _get_fiscal_year,
        "date_from"   : datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT),
        "date_to"     : datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT),

    }