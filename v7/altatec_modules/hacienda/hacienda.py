# -*- coding: utf-8 -*-
import time

import datetime

from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree 
import openerp.addons.decimal_precision as dp
import openerp.exceptions



from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp.osv import fields, osv, orm

from openerp import tools
from openerp.tools.translate import _

import logging
import xml.etree.cElementTree as ET


_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')
        
class ha_crop(osv.osv):
        _name= 'ha.crop'
        
        _columns = {
                    'name':fields.char(string="Cosecha"),
                    
                    }
        

class ha_measurement_type(osv.osv):
        _name= 'ha.measurement.type'
        
        _columns = {
                    'name':fields.char(string="Nombre"),
                    'note':fields.text(string="Notas"),
                    'result_template':fields.many2one("ha.result.template",string="Plantilla de Resultados"),
                    
                    }
        
class ha_result(osv.osv):
        _name= 'ha.result'
        
        _columns = {
                    'name':fields.char(string="Nombre"),
                    'param1':fields.float(string="Parametro 1"),
                    'param2':fields.float(string="Parametro 2"),
                    'param3':fields.float(string="Parametro 3"),
                    'measurement':fields.many2one("ha.measurement", string="Muestras"),
                    'cycle':fields.many2one("ha.cycle",string="Ciclo"),
                    'lotes':fields.many2one("ha.lote",string='Lote'),
                    }
        
        def on_change_lote(self,cr,uid,ids,lotes,context=None):
            if len (self.pool.get('ha.cycle').search(cr,uid,[('lotes','=',lotes),('activo','=', True),]))==0:
                raise osv.except_osv(
                                     _('El ciclo no esta activo'),
                                     _('No hay ningun ciclo asigando a ese lote')
                                     )
                
            else:                
                ciclo_id = self.pool.get('ha.cycle').search(cr,uid,[('lotes','=',lotes),('activo','=', True),])
                ciclo=self.pool.get('ha.cycle').browse(cr,uid,ciclo_id)
                for c in ciclo:
                    return {'value':{'cycle': c.id},}
            
        
class ha_measurement(osv.osv):
        _name= 'ha.measurement'
        
        def on_change_lote(self,cr,uid,ids,lotes,context=None):
            if len (self.pool.get('ha.cycle').search(cr,uid,[('lotes','=',lotes),('activo','=', True),]))==0:
                raise osv.except_osv(
                                     _('El ciclo no esta activo'),
                                     _('No hay ningun ciclo asigando a ese lote')
                                     )
                
            else:                
                ciclo_id = self.pool.get('ha.cycle').search(cr,uid,[('lotes','=',lotes),('activo','=', True),])
                ciclo=self.pool.get('ha.cycle').browse(cr,uid,ciclo_id)
                for c in ciclo:
                    return {'value':{'cycle': c.id},}
            
        
        _columns = {
                    'start_date':fields.date(string="Fecha de Inicio"),
                    'start_end':fields.date(string="Fecha de Vencimiento"),
                    'crop':fields.many2one("ha.crop",string="Cosecha"),
                    'measurement_type':fields.many2one("ha.measurement.type",string=" Tipo de Muestra"),
                    'cycle':fields.many2one("ha.cycle",string="Ciclo"),
                    'result_list':fields.one2many("ha.result", 'measurement', string="Resultado"),
                    'notes':fields.text(string="Notas"),
                    'responsable':fields.many2one("res.users", string="Persona Responsable"),
                    'state':fields.selection([('draft','Borrador'),('confirm','Confirmado'),('rechazed',"Rechazado")] ,string = "Estado"),
                    'lotes':fields.many2one("ha.lote",string='Lote'),
                    }
        _defaults={'state':'draft'}

        
class ha_product_need_line(osv.osv):
        _name= 'ha.product.need.line'
        
        _columns = {
                    'product_id':fields.many2one('product.product',string="Producto"),
                    'qty':fields.float('Cantidad/Hectarea'),
                    'unidad':fields.many2one('product.uom', string="Unidad"),
                    'app_type':fields.many2one('ha.job.type', string='Necesidades'),
                    }
        


class ha_application_type_line(osv.osv):
        _name= 'ha.application.type.line'
        
        _columns = {
                    'application_type':fields.many2one("ha.job.type",string="Tipo de Aplicacion"),
                    'days_after_start':fields.integer(string="Dias despues de empezar"),
                    'cycle_plan':fields.many2one("ha.cycle.plan", string="Plan de Ciclo"),
                    'dosis':fields.float (string="Dosis"),
                    'producto':fields.many2one("product.product",string="Producto"),
                    'note':fields.text(string="Notas"),
                    }
        
class ha_measurement_type_line(osv.osv):
        _name= 'ha.measurement.type.line'
        
        _columns = {
                    'measurement_type':fields.many2one("ha.measurement.type",string="Tipo de Muestra"),
                    'days_after_start':fields.integer(string="Dias despues de empezar"),
                    'note':fields.text(string="Notas"),
                    'cycle_plan':fields.many2one("ha.cycle.plan", string="Plan de Ciclo"),
                    }

class ha_cycle_plan(osv.osv):
        _name= 'ha.cycle.plan'
        
        def on_change_lote(self,cr,uid,ids,lotes,context=None):
            if len (self.pool.get('ha.cycle').search(cr,uid,[('lotes','=',lotes),('activo','=', True),]))==0:
                raise osv.except_osv(
                                     _('El ciclo no esta activo'),
                                     _('No hay ningun ciclo asigando a ese lote')
                                     )
                
            else:                
                ciclo_id = self.pool.get('ha.cycle').search(cr,uid,[('lotes','=',lotes),('activo','=', True),])
                ciclo=self.pool.get('ha.cycle').browse(cr,uid,ciclo_id)
                for c in ciclo:
                    return {'value':{'cycle': c.id},}
            
                  
        _columns = {
                    'name':fields.char(string="Plan de Ciclo"),
                    'application_type_line':fields.one2many("ha.application.type.line", 'cycle_plan', string="Lista de Aplicaciones"),
                    'measurement_type_line':fields.one2many("ha.measurement.type.line", 'cycle_plan', string="Lista de Muestras"),
                    'state':fields.selection([('planned','Planeado'),('starred','Iniciado'),('ended',"Terminado"),] ,string = "Estado", required=True),
                    'lotes':fields.many2one("ha.lote",string='Lote'),
                    'cycle':fields.many2one("ha.cycle",string="Ciclo"),
        }

        _defaults={'state':'planned'}
                    
                    
                      

class ha_seed_type(osv.osv):
        _name= 'ha.seed.type'
        
        _columns = {
                    'date_purchased':fields.date(string="Fecha de Venta"),
                    'pais':fields.many2one("res.country",string="Pais"),
                    'proveedor':fields.many2one("res.partner",string="Proveedor"),
                    'size':fields.char(string="Talla"),
                    
                    }  
        
class ha_lote(osv.osv):
        _name= 'ha.lote'
        
        _columns = {
                    'name':fields.char(string="Nombre"),
                    'cycle_lists':fields.one2many("ha.cycle", 'lotes', string="Lista de Ciclos"),
                    'coordinate_x':fields.integer(string="Coordinada X"),
                    'coordinate_y':fields.integer(string="Coordinada Y"),
                    'medidor':fields.many2one("ha.agua.counter", string= "Medidor"),         
                    }  
        
class ha_days(osv.osv):
        _name='ha.days'
        
        _columns = {
                    "fecha":fields.date(string="Fecha"),
                    'agua':fields.float(string="Agua"),
                    'param1':fields.float(string="Parametro 1"),
                    'param2':fields.float(string="Parametro 2"),
                    'cycle':fields.many2one("ha.cycle", string="Ciclo"),
                    
                    }

class ha_result_template_line(osv.osv):
        _name= 'ha.result.template.line'
        
        _columns = {
                    'name':fields.char(string="Nombre"),
                    'result_template':fields.many2one("ha.result.template",string="Plantilla de Resultados"),
                    }


class ha_result_template(osv.osv):
        _name= 'ha.result.template'
        
        _columns = {
                    'name':fields.char(string="Nombre"),
                    'result_template_line':fields.one2many('ha.result.template.line','result_template',string="Line de Resultados"),
                    }




class ha_job_type(osv.osv):
    _name ='ha.job.type'
    
    _columns={
              'name':fields.char(string="Nombre"),
              'product_need_lines':fields.one2many('ha.product.need.line','app_type', string='Necesidades'),
              'note':fields.text(string="Notas"),
              }
        
class stock_move(osv.osv):
        _inherit ="stock.move"
        
        _columns = {
                    'job':fields.many2one("ha.job",string="Trabajo"),
                    'maintenance':fields.many2one('ha.maintenance.type', string="Tipo de Mantenimiento"),
                    'dosis':fields.float(string="Cantidad/Hectarea"),
                    }
        
class ha_res_users(osv.osv):
        _inherit ="res.users"
        
        _columns = {
                    'trab_hacienda':fields.boolean(string="Trabajador de Hacienda"),
                    'recive_maquinaria_alertas':fields.boolean(string="Recibe Alertas de Maquinaria"),
                    'recive_plagas_alertas':fields.boolean(string="Recibe Alertas de Plagas"),
                    'recive_nutricion_alertas':fields.boolean(string="Recibe Alertas de Nutricion"),
                    }

class ha_job(osv.osv):
        _name= 'ha.job'
        
        
        def get_all_products_delivered(self,cr,uid,ids,field_name,field_value,arg,context=None):
                result={}
                aux=None
                
                
                records = self.browse(cr, uid, ids)        
                for r in records:
                        cont=0
                        result[r.id]= True
                        for list in r.stock_move:
                                if not (list.state == 'done'):
                                        result[r.id]=False
                        
                return result
        
        
        def sent_job(self,cr,uid,ids,context):
                
                return 1
        
        def return_job(self,cr,uid,ids,context):
                
                return 1
            
        def on_change_type(self,cr,uid,ids,type,context=None):
            result=[]
            records = self.browse(cr, uid, ids)
            
            
            move_obj = self.pool.get('stock.move')
            locs = move_obj.default_get(cr,uid,['location_id','location_dest_id'],{'picking_type':'out'})
            
            tipo_obj = self.pool.get('ha.job.type').browse(cr,uid,type,context)
            r = []
            for line in tipo_obj.product_need_lines:
                r.append({'product_id':line.product_id.id,
                        'state': 'draft',
                        'type':'internal',
                        'company_id':1,
                        'location_id':locs['location_id'],
                        'location_dest_id':locs['location_dest_id'],
                        'product_uom':line.unidad.id,
                        'name':line.product_id.name, 
                        'product_qty':line.qty,
                        'date_expected':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                        'date':datetime.now().strftime('%Y-%m-%d %H:%M:%S')})                
            return {'value':{'stock_move':r}}
            
        
        def on_change_lote(self,cr,uid,ids,lotes,context=None):
            if len (self.pool.get('ha.cycle').search(cr,uid,[('lotes','=',lotes),('activo','=', True),]))==0:
                raise osv.except_osv(
                                     _('El ciclo no esta activo'),
                                     _('No hay ningun ciclo asigando a ese lote')
                                     )
                
            else:                
                ciclo_id = self.pool.get('ha.cycle').search(cr,uid,[('lotes','=',lotes),('activo','=', True),])
                ciclo=self.pool.get('ha.cycle').browse(cr,uid,ciclo_id)
                for c in ciclo:
                    return {'value':{'cycle': c.id},}
            
        
        _columns={
                  'name':fields.char(string="Nombre"),
                  'type':fields.many2one("ha.job.type",string="Tipo de Trabajo"),
                  'machine':fields.many2one("ha.machine",string="Maquinaria"),
                  'cycle':fields.many2one("ha.cycle",string="Ciclo"),
                  'datetime_start':fields.datetime(string="Tiempo de Inicio"),
                  'datetime_end':fields.datetime(string="Tiempo de Fin"),
                  'responsable':fields.many2one("res.users",string="Responsable"), 
                  'stock_move':fields.one2many("stock.move", "job", string="Moviemientos de Stock"),
                  'state':fields.selection([('draft','Borrador'),('confirm','Confirmado'),('rechazed',"Rechazado")] ,string = "Estado"),                  
                  'lotes':fields.many2one("ha.lote",string='Lote'),
                  'notes':fields.text(string="Notas"),
                  'all_products_delivered':fields.function(get_all_products_delivered,string="Productos Entregados",type="boolean"),

                  }
        _defaults={'state':'draft'}


class ha_gas_line(osv.osv):
        _name= 'ha.gas.line'
        
        _columns={
                  'cantidad':fields.float(string="Cantidad"),
                  'fecha':fields.datetime(string="Fecha"),
                  'responsable':fields.many2one("res.users",string="Responsable"),
                  'machine':fields.many2one("ha.machine",string="Maquinaria"),
        
                  }
        
class ha_maintenance_list(osv.osv):
        _name= 'ha.maintenance.list'
        
        _columns={
                  'tipo':fields.many2one('ha.maintenance.type', string='Tipo'),
                  'hours':fields.integer(string="Horas"),
                  'fecha':fields.date(string="Fecha"),
                  'company':fields.many2one("res.partner",string="Compañia"),
                  'machine':fields.many2one("ha.machine",string="Maquinaria"),
                  'Precio':fields.float(string="Precio"),
                  'product_necesarios':fields.char(string="Productos Necesarios"),
                  }

class ha_machine(osv.osv):
        _name= 'ha.machine'
        
        def get_usage(self,cr,uid,ids,field_name,field_value,arg,context=None):
                result={}
                cont=0
                aux=0
                
                records = self.browse(cr, uid, ids)
                
                for r in records:
                        if (r.state == 'comprado'):
                                result[r.id]=0
                                continue
                        elif (r.state == 'usando'):
                                if(r.date_start):
                                        date_m1=r.date_start
                                        date_m2=datetime.now().strftime("%Y-%m-%d")
                                else:
                                        date_m1=datetime.now().strftime("%Y-%m-%d")
                                        date_m2=datetime.now().strftime("%Y-%m-%d")
                                #fecha inicio y fecha de hoy
                        elif (r.state == 'vencido'):
                                if(r.date_start and r.date_end):
                                        date_m1=r.date_start
                                        date_m2=r.date_end
                                else:
                                        date_m1=datetime.now().strftime("%Y-%m-%d")
                                        date_m2=datetime.now().strftime("%Y-%m-%d")
                                
                                
                        cont=0
                        aux=0
                        #date_m1=r.date_purchased
                        #date_m2=r.date_end
                                
                        datem1=datetime.strptime(date_m1,"%Y-%m-%d")
                        datem2=datetime.strptime(date_m2,"%Y-%m-%d")
                        days=datem2-datem1
                        daysf=(days.days)*8

                        for list in r.job_list:
                                date_1=list.datetime_start
                                date_2=list.datetime_end
                                date1=datetime.strptime(date_1,"%Y-%m-%d %H:%M:%S")
                                date2=datetime.strptime(date_2,"%Y-%m-%d %H:%M:%S")
                                a=date2-date1
                                if(a.seconds*a.days==0):
                                    cont=cont+0
                                else:
                                    b=(a.seconds)/3600 + a.days*24
                                    cont=cont+b
                        #b=a.days*8
                        if(daysf == 0 and cont==0):
                                aux = 0
                        else:
                                aux=float(cont)/float(daysf)
                                
                        result[r.id]=aux
                
                return result
        
        def get_total_gas(self,cr,uid,ids,field_name,field_value,arg,context=None):
                result={}
                cont=0
                
                records = self.browse(cr, uid, ids)
                
                for r in records:
                        cont=0
                        for lines in r.gas_lines:
                                cont=cont+lines.cantidad
                        result[r.id]=cont
                                
                
                return result
        
        def get_hours_used(self,cr,uid,ids,field_name,field_value,arg,context=None):
                result={}
                cont=0
                
                records = self.browse(cr, uid, ids)
                
                for r in records:
                        cont=0
                        for list in r.job_list:
                                date_1=list.datetime_start
                                date_2=list.datetime_end
                                date1=datetime.strptime(date_1,"%Y-%m-%d %H:%M:%S")
                                date2=datetime.strptime(date_2,"%Y-%m-%d %H:%M:%S")
                                a = date2-date1
                                b = float((a.seconds)/3600) + a.days*24
                                cont=float(cont)+float(b)
                                #b=a.days*8
                        result[r.id]=cont
                        
                return result
        
        def get_maintenance(self,cr,uid,ids,field_name,field_value,arg,context=None):
                result={}
                
                records = self.browse(cr, uid, ids)        
                for r in records:
                        date_max= datetime.date(datetime(1990,2,3))
                        hour_max=1
                        for list in r.maintenance_list:
                                
                                date1=datetime.strptime(list.fecha,"%Y-%m-%d")
                                date2=datetime.date(date1)
                                
                                if date_max<date2:
                                        date_max=date2
                                        hour_max=list.hours
                                        
                        if (r.horas_cada_mantenimiento+hour_max)<r.hours_used:
                                valor="Necesita Mantenimiento"
                        else:
                                valor="Bien"
                                
                        result[r.id]=valor
                        
                return result
        
        
        def get_mean_gas(self,cr,uid,ids,field_name,field_value,arg,context=None):
                result={}
                
                records = self.browse(cr, uid, ids)        
                for r in records:
                        if (r.state == 'comprado'):
                                result[r.id]=0
                        else:
                                if (r.state == 'usando'):
                                        date_m1=r.date_start
                                        date_m2=datetime.now().strftime("%Y-%m-%d")
                                        #fecha inicio y fecha de hoy
                                if (r.state == 'vencido'):
                                        date_m1=r.date_start
                                        date_m2=r.date_end
                                        
                                cont=0
                                datem1=datetime.strptime(date_m1,"%Y-%m-%d")
                                datem2=datetime.strptime(date_m2,"%Y-%m-%d")
                                days=datem2-datem1
                                #daysf=(days.days)*8
                                for gas in r.gas_lines:
                                        cont=cont+gas.cantidad
                                if (days.days==0):
                                    result[r.id]=0
                                else:
                                    valor=float(cont)/float(days.days)        
                                    result[r.id]=valor
                                
                return result

        def _get_image(self, cr, uid, ids, name, args, context=None):
                result = dict.fromkeys(ids, False)
                for obj in self.browse(cr, uid, ids, context=context):
                        result[obj.id] = tools.image_get_resized_images(obj.image, avoid_resize_medium=True)
                return result
        
        def _set_image(self, cr, uid, id, name, value, args, context=None):
                return self.write(cr, uid, [id], {'image': tools.image_resize_image_big(value)}, context=context)

        _columns={
                  'name':fields.char(string="Nombre"),
                  'proveedor':fields.many2one("res.partner",string="Proveedor"),
                  'responsable':fields.many2one("res.users",string="Responsable"),
                  'job_list':fields.one2many("ha.job", 'machine', string="Lista de Trabajos"),
                  'usage':fields.function(get_usage,string="Utilizacion (Horas/Dia)", type='float'),
                  'date_purchased':fields.date(string="Fecha de Compra"),
                  'date_start':fields.date(string="Fecha de Inicio"),
                  'date_end':fields.date(string="Fecha de Vencimiento"),
                  'gas_lines':fields.one2many("ha.gas.line", 'machine', string="Lineas de Gas"),
                  'total_gas':fields.function(get_total_gas,string="Total de Gas (Galones)", type='integer'),
                  'hours_used':fields.function(get_hours_used,string="Horas de Uso (Total)", type='float'),
                  'mean_gas':fields.function(get_mean_gas,string="Promedio de Gas (Galones / Dia)", type='float'),
                  'maintenance_horas_list':fields.one2many('ha.maintenance.hours.list','machine',string="Hora de cada Mantenimiento"),
                  'maintenance_list':fields.one2many("ha.maintenance.list", 'machine', string="Lista de Mantenimiento"),
                  'state':fields.selection([('comprado','Comprado'),('usando','Usando'),('vencido',"Vencido")] ,string = "Estado"),
                  'estadisticas':fields.char(string="Estadisticas"),
                  'image': fields.binary("Image",
                                help="This field holds the image used as image for the product, limited to 1024x1024px."),
                  
                  'date_manufactured':fields.char(string="Año Fabricado"),
                  'proveedor':fields.char(string="Proveedor"),
                  'modelo':fields.char(string="Modelo"),
                  'marca':fields.char(string="Marca"),
                  'serie':fields.char(string="# Serie"),
                  'serie_motor':fields.char(string="# Serie de Motor"),
                  'Placa':fields.char(string="Placa"),
        
                  }

        

class ha_type_plaga_ec(osv.osv):
        _name= 'ha.type.plaga.ec'
        
        _columns={
                  'name':fields.char(string="Nombre"),
                  'name_cientificio':fields.char(string="Nombre Cientifico"),
                  'note':fields.text(string="Notas"),
                  }
        

class ha_plaga_muetsra_line(osv.osv):
        _name= 'ha.plaga.muestra.line'
        
        def get_ha_tot(self,cr,uid,ids,field_name,field_value,arg,context=None):
            result={}    
            records = self.browse(cr, uid, ids)        
            for r in records:
                tot = r.s1+r.s2+r.s3+r.s4+r.s5+r.s6+r.s7+r.s8+r.s9+r.s10
                result[r.id]=tot                                
            return result
        
        def get_ha_porcent(self,cr,uid,ids,field_name,field_value,arg,context=None):
            result={}    
            records = self.browse(cr, uid, ids)        
            for r in records:
                muestra_id = self.pool.get('ha.plaga.muestra').search(cr,uid,[('line_plaga','=',r.id)])
                muestra=self.pool.get('ha.plaga.muestra').browse(cr,uid,muestra_id)
                for m in muestra:
                    if(r.tot != 0):
                        por= (float(m.muestra)*float(m.plants))/(float(r.tot))
                        result[r.id]=por
                    else:
                        result[r.id]=0                                
            return result
        
        _columns={
                  'plaga':fields.many2one("ha.plaga.muestra",string="Plaga"),
                  'etapa':fields.integer(string = "DDE",help="Dias Despues de Emergencia"),
                  'type_plaga':fields.many2one("ha.type.plaga.ec", string=" Tipo de Plaga"),
                  'state':fields.selection([('l1','L1'),('l2','L2'),('l3',"L3"),('l4',"L4")] ,string = "Estado"),
                  's1':fields.integer(string="S1"),
                  's2':fields.integer(string="S2"),
                  's3':fields.integer(string="S3"),
                  's4':fields.integer(string="S4"),
                  's5':fields.integer(string="S5"),
                  's6':fields.integer(string="S6"),
                  's7':fields.integer(string="S7"),
                  's8':fields.integer(string="S8"),
                  's9':fields.integer(string="S9"),
                  's10':fields.integer(string="S10"),
                  'tot':fields.function(get_ha_tot, string="TOT", type="float"),
                  'porcentaje':fields.function(get_ha_porcent, type="float", string="%"),
                  'nc':fields.float(string="N.C"),
                  }  
        
class ha_plaga_muetsra(osv.osv):
        _name= 'ha.plaga.muestra'
        
        
        def on_change_lote(self,cr,uid,ids,lotes,context=None):
            if len (self.pool.get('ha.cycle').search(cr,uid,[('lotes','=',lotes),('activo','=', True),]))==0:
                raise osv.except_osv(
                                     _('El ciclo no esta activo'),
                                     _('No hay ningun ciclo asigando a ese lote')
                                     )
                
            else:                
                ciclo_id = self.pool.get('ha.cycle').search(cr,uid,[('lotes','=',lotes),('activo','=', True),])
                ciclo=self.pool.get('ha.cycle').browse(cr,uid,ciclo_id)
                
                for c in ciclo:
                    return {'value':{'cycle': c.id, 'area':c.area}}
       
        
        _columns={
                  'cycle':fields.many2one("ha.cycle",string="Ciclo"),
                  'monitoreo':fields.many2one("res.users",string="Monitoreo"),
                  'type_monitoreo':fields.selection([('m1','M1'),('m2','M2'),('m3',"M3")] ,string = "Tipo de Monitoreo"),
                  'plants':fields.integer(string="Numero de Plantas"),
                  'area':fields.integer(string="Hectareas"),
                  'line_plaga':fields.one2many("ha.plaga.muestra.line","plaga",string="Linea de Plagas"),
                  'fecha':fields.date(string="Fecha"),
                  'lotes':fields.many2one("ha.lote",string='Lote'),
                  'muestra':fields.integer(string="Numero de Muestras"),
                  }
        _defaults={'muestra':10,
                   'plants':25
                   }      

        
class ha_cycle(osv.osv):
        _name= 'ha.cycle'
        
        def on_change_lote(self,cr,uid,ids,lotes,context=None):
            if len (self.pool.get('ha.cycle').search(cr,uid,[('lotes','=',lotes),('activo','=', True),]))==0:
                raise osv.except_osv(
                                     _('El ciclo no esta activo'),
                                     _('No hay ningun ciclo asigando a ese lote')
                                     )
                
            else:                
                ciclo_id = self.pool.get('ha.cycle').search(cr,uid,[('lotes','=',lotes),('activo','=', True),])
                ciclo=self.pool.get('ha.cycle').browse(cr,uid,ciclo_id)
                
                for c in ciclo:
                    return {'value':{'cycle': c.id, 'area':c.area}}
        
        def generar_application_measurement(self,cr,uid,ids,context):

                records = self.browse(cr, uid, ids)
                for r in records:
                        date_1=r.start_date
                        date=datetime.strptime(date_1,"%Y-%m-%d")
                        for app in r.cycle_plan.application_type_line:
                                app_type= app.application_type.id
                                date_final_app=date+timedelta(days=app.days_after_start)
                                date_final_app_1=datetime.strftime(date_final_app,"%Y-%m-%d")
                                app_id = self.pool.get('ha.job').create(cr,uid,{'start_date':date_final_app_1,
                                                                        'cycle':r.id,
                                                                        'application_type':app_type,
                                                                        'dosis':app.dosis,
                                                                        'producto':app.producto.id,
                                                                        
                                                                       })
                                move_obj = self.pool.get('stock.move')
                                locs = move_obj.default_get(cr,uid,['location_id','location_dest_id'],{'picking_type':'out'})
                                for need in app.application_type.product_need_lines:
                                        self.pool.get('stock.move').create(cr,uid,{
                                                                                   'product_id': need.product_id.id,
                                                                                   'type': 'out' ,
                                                                                   'product_qty' : need.qty,
                                                                                   'product_uom':need.product_id.uom_id.id,
                                                                                   'name' : need.product_id.name,
                                                                                   'application' : app_id,
                                                                                   'location_id':locs['location_id'] ,
                                                                                   'location_dest_id':locs['location_dest_id'] ,
                                                                                   })
                        
                        for medi in r.cycle_plan.measurement_type_line:
                                medi_type= medi.measurement_type.id
                                date_final_medi=date+timedelta(days=medi.days_after_start)
                                date_final_medi_1=datetime.strftime(date_final_medi,"%Y-%m-%d")
                                medida_id=self.pool.get('ha.measurement').create(cr,uid,{'start_date':date_final_medi_1,
                                                                        'cycle':r.id,
                                                                        'measurement_type':medi_type,
                                                                       })
                                id_medida=int(medida_id)
                                
                                for template_line in medi.measurement_type.result_template.result_template_line:
                                        self.pool.get('ha.result').create(cr,uid,{'measurement':id_medida,
                                                                        'name':template_line.name,
                                                                       })
                                        
        _columns = {
                    'name': fields.char(string="Ciclo"),
                    'start_date':fields.date(string="Fecha de Inicio"),
                    'start_end':fields.date(string="Fecha de Vencimiento"),
                    'date_crop':fields.date(string="Fecha de Cosecha"),
                    'date_seed':fields.date(string="Fecha de Siembra"),
                    'date_germination':fields.date(string="Fecha de Germinacion"),
                    'crop':fields.many2one("ha.crop",string="Cosecha"),
                    'measurement_list':fields.one2many("ha.measurement", 'cycle', string="Lista de Muestras"),
                    'cycle_plan':fields.many2one("ha.cycle.plan",string="Plan de Ciclo"),
                    'seed_type':fields.many2one("ha.seed.type",string="Tipo de Semilla"),
                    'days_list':fields.one2many("ha.days", 'cycle', string="Lista de Dias"),
                    'plaga_muestra':fields.one2many("ha.plaga.muestra", 'cycle', string="Lista de Plagas"),
                    'nutricional_plantacion': fields.one2many("ha.nutricional.plantacion", "cycle", string= "Nutricional Plantacion"),
                    'lotes':fields.many2one("ha.lote",string='Lote'),
                    'state':fields.selection([('planned','Planeado'),('starred','Iniciado'),('ended',"Terminado"),] ,string = "Estado"),
                    'area':fields.integer("Area"),
                    'activo':fields.boolean(string="Activo"),
                    'job':fields.one2many("ha.job", 'cycle', string="Trabajos"),
        }

        _defaults={'state':'planned'}
        
       
class ha_maintenance_type(osv.osv):
        _name= 'ha.maintenance.type'
        
        _columns = {
                        'name':fields.char("Nombre"),
                        'stock':fields.one2many("stock.move","maintenance",string="Moviemientos de Stock")
                    }

class ha_nutricional_line(osv.osv):
        
        _name='ha.nutricional.line'
        
        _columns = {
                    'u': fields.char(string="U"),
                    'n': fields.float(string="N"),
                    'p': fields.float(string="P"),
                    'k': fields.float(string="K"),
                    'ca': fields.float(string="Ca"),
                    'mg': fields.float(string="Mg"),
                    'so4': fields.float(string="SO4"),
                    'zn': fields.float(string="Zn"),
                    'cu': fields.float(string="Cu"),
                    'fe': fields.float(string="Fe"),
                    'mn': fields.float(string="Mn"),
                    'b': fields.float(string="B"),
                    'nutricional': fields.many2one("ha.nutricional.plantacion",string="Nutricional Plantacion"),
                    'nutricional_line_extremo': fields.many2one("ha.nutricional.plantacion",  string= "Valores Rangos"),

                    }

class ha_nutricional_plantacion(osv.osv):
        
        _name='ha.nutricional.plantacion'
        
        def on_change_lote(self,cr,uid,ids,lotes,context=None):
            if len (self.pool.get('ha.cycle').search(cr,uid,[('lotes','=',lotes),('activo','=', True),]))==0:
                raise osv.except_osv(
                                     _('El ciclo no esta activo'),
                                     _('No hay ningun ciclo asigando a ese lote')
                                     )
                
            else:                
                ciclo_id = self.pool.get('ha.cycle').search(cr,uid,[('lotes','=',lotes),('activo','=', True),])
                ciclo=self.pool.get('ha.cycle').browse(cr,uid,ciclo_id)
                for c in ciclo:
                    return {'value':{'cycle': c.id},}
        
        _columns = {
                    'name': fields.char(string="Nombre"),
                    'fecha':fields.date(string="Fecha"),
                    'responsable': fields.many2one("res.users",string="Responsable"),
                    'proveedor': fields.many2one("res.partner",string="Proveedor"),
            'nutricional_line': fields.one2many("ha.nutricional.line", 'nutricional', string= "Lineas de Nutricional"),
                    'nutricional_extremos': fields.one2many("ha.nutricional.line", 'nutricional_line_extremo', string= "Rangos de Alertas"),
                    'cycle': fields.many2one("ha.cycle",string="Ciclo"), 
                    'lotes':fields.many2one("ha.lote",string='Lote'),
                    }
        
        _defaults = {
            'nutricional_extremos':[(4,1),(4,2)],
            }
        
class ha_nutricional_values(osv.osv):
        
        _name='ha.nutricional.values'
        
        _columns = {  
                    'values_max': fields.many2one("ha.nutricional.line",string="Valores Maximos"),
                    'values_min': fields.many2one("ha.nutricional.line",string="Valores Minimos"),
                    }
        

class ha_maintenance_hours_list(osv.osv):
        _name= 'ha.maintenance.hours.list'
        
        def get_maintenance(self,cr,uid,ids,field_name,field_value,arg,context=None):
                result={}
                
                records = self.browse(cr, uid, ids)        
                for r in records:
                    
                    
                        
                        m_ids = self.pool.get('ha.maintenance.list').search(cr,uid,[('tipo','=',r.tipo.id),('machine','=',r.machine.id)])
                        
                        max_hours = 0
                        
                        for l in self.pool.get('ha.maintenance.list').browse(cr,uid,m_ids):
                                if l.hours > max_hours:
                                        max_hours = l.hours
                                        
                                        
                        if (r.hours+max_hours)<r.machine.hours_used:
                                valor="need_maintenance"
                        else:
                                valor="good"
                                
                        result[r.id]=valor
                        
                return result
        
        def get_next_maintenance(self,cr,uid,ids,field_name,field_value,arg,context=None):
                result={}
                
                records = self.browse(cr, uid, ids)        
                for r in records:
                        
                        m_ids = self.pool.get('ha.maintenance.list').search(cr,uid,[('tipo','=',r.tipo.id),('machine','=',r.machine.id)])
                        
                        max_hours = 0
                        
                        for l in self.pool.get('ha.maintenance.list').browse(cr,uid,m_ids):
                                if l.hours > max_hours:
                                        max_hours = l.hours
                                
                        result[r.id]=max_hours + r.hours
                        
                return result
        
        def get_last_maintenance(self,cr,uid,ids,field_name,field_value,arg,context=None):
                result={}
                
                records = self.browse(cr, uid, ids)        
                for r in records:
                        
                        m_ids = self.pool.get('ha.maintenance.list').search(cr,uid,[('tipo','=',r.tipo.id),('machine','=',r.machine.id)])
                        
                        max_hours = 0
                        
                        for l in self.pool.get('ha.maintenance.list').browse(cr,uid,m_ids):
                                if l.hours > max_hours:
                                        max_hours = l.hours
                                
                        result[r.id]=max_hours
                        
                return result
        
        _columns = {
                        'tipo':fields.many2one("ha.maintenance.type",string='Tipo'),
                        'hours':fields.integer("Cada Horas"),
                        'last_maintenance':fields.function(get_last_maintenance, method=True, type='integer', string='Ultimo Mantenimiento'),
                        'proximo':fields.function(get_next_maintenance, method=True, type='integer',string= "Proximo Mantenimiento"),
                        'state':fields.function(get_maintenance, type='selection', selection = [('good','Bien'),('need_maintenance','Necesita Mantenimiento')] ,string = "Estado", method=True),
                        'machine':fields.many2one('ha.machine',string='Maquinaria'),
                    }
         

class ha_mensaje_wizard(osv.osv):
        _name= 'ha.mensaje.wizard'
        
        
        def send_men(self,cr,uid, subject,mensaje,email, ids,context=None):
            email_template_obj = self.pool.get('email.template')
            template_ids = email_template_obj.search(cr, uid, [('name', '=','Test Template')])
            if template_ids:
                values = email_template_obj.generate_email(cr, uid, template_ids[0], ids, context=context)
                values['subject'] = subject
                values['email_to'] = email
                values['body'] = mensaje
                values['res_id'] = None
                mail_mail_obj = self.pool.get('mail.mail')
                msg_id = mail_mail_obj.create(cr, uid, values, context=context)
                if msg_id:
                    mail_mail_obj.send(cr, uid, [msg_id], context=context)
    
            return
        
        def send_mensaje(self,cr,uid,ids,context=None):
            result={}    
            records = self.browse(cr, uid, ids)
            
            p_mant= self.pool.get('res.users').search(cr,uid,[('recive_maquinaria_alertas','=', True)])
            p_nutri= self.pool.get('res.users').search(cr,uid,[('recive_nutricion_alertas','=', True)])
            p_plag= self.pool.get('res.users').search(cr,uid,[('recive_plagas_alertas','=', True)])
            
                        
            p_ids=[]
                   
            for r in records:
                
                if (r.tipo_mensaje=='maintenance'):
                    mensaje="Necesita Mantenimiento"
                    for mant in p_mant:
                        p_ids.append((4,mant))
                        
                    machine= self.pool.get('res.users').browse(cr,uid,p_mant)
                    for ma in machine:
                        subject="Maquinaria"
                        if ma.email :
                            email= ma.email
                            self.send_men(cr, uid, subject,mensaje,email, ids, context)
                        else:
                            raise osv.except_osv(
                                     _('ERROR'),
                                     _('falta correo electronico')
                                     )
                                
                else:
                    if (r.tipo_mensaje=='nutrition'):
                        mensaje="Nutricion"
                        for nutri in p_nutri:
                            p_ids.append((4,nutri))
                        
                        nutricion= self.pool.get('res.users').browse(cr,uid,p_nutri)
                        for nu in nutricion:
                            subject="Nutricion"
                            if nu.email:
                                email= nu.email
                                self.send_men(cr, uid, subject,mensaje,email, ids, context)
                            else:
                                raise osv.except_osv(
                                     _('ERROR'),
                                     _('falta correo electronico')
                                     )  
                    else:
                        if (r.tipo_mensaje=='plague'):
                            mensaje="Plaga"    
                            for plag in p_plag:
                                p_ids.append((4,plag))
                            
                            plaga= self.pool.get('res.users').browse(cr,uid,p_plag)
                            for pa in plaga:
                                subject="Nutricion"
                                if pa.email :
                                    email= pa.email
                                    self.send_men(cr, uid, subject,mensaje,email, ids, context)
                                else:
                                    raise osv.except_osv(
                                     _('ERROR'),
                                     _('falta correo electronico')
                                     )  
                                                        
                post_vars = {'subject': "Message subject",
                            'body': mensaje,
                            'partner_ids': p_ids,} # Where "4" adds the ID to the list 
                            # of followers and "3" is the partner ID 
                thread_pool = self.pool.get('mail.thread')
                thread_pool.message_post(cr, uid, False,
                                            type="notification",
                                            subtype="mt_comment",
                                            context=context,
                                            **post_vars)
                
                        
    
            return True 
        
                    
        _columns = {
                        'tipo_mensaje':fields.selection([('maintenance','Mantenimiento'),('nutrition','Nutricion'),('plague',"Plaga")] ,string = "Tipo de Mensaje"),
                    }

class product_product(osv.osv):
        _inherit = 'product.product'

        _columns = {
                        'ingrediente':fields.char('Ingrediente Activo'),   
                }
        
class ha_agua_counter(osv.osv):
        _name='ha.agua.counter'
        
        _columns = {
                    "name":fields.char(string="Medidor"),
                    'lote':fields.one2many("ha.lote", "medidor", string='Lote'),
                    'agua_counter_line': fields.one2many("ha.line.agua.counter", "agua_counter", string= "Lineas de Medidas Diarias"),                    
                    }
        
class ha_line_agua_counter(osv.osv):
        _name='ha.line.agua.counter'
        
        _columns = {
                    "agua_counter":fields.many2one('ha.agua.counter',string='Medidor'),
                    "fecha":fields.date(string="Fecha"),
                    "medida":fields.integer(string="Cantidad"),                    
                    }
        
