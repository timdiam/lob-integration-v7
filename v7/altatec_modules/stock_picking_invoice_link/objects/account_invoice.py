# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2013 Agile Business Group sagl (<http://www.agilebg.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, orm

class account_invoice(orm.Model):
    _inherit = "account.invoice"
    
    #===========================================================================
    # Se calcula todas las lineas de relacionadas con los stock pickings si tienen
    # una sale order asociada.
    #===========================================================================
    
    def _compute_lines(self, cr, uid, ids, name, args, context=None):
        result = {}
        sale_order_line_pool = self.pool.get('sale.order.line')
        stock_move_pool = self.pool.get('stock.move')
        for invoice in self.browse(cr, uid, ids, context=context):
            src = []
            lines = []
            if invoice.invoice_line:                
                for m in invoice.invoice_line:
                    temp_lines=[]
                    sale_id = sale_order_line_pool.search(cr, uid, [('invoice_lines.id','=',m.id)],0)
                    stock_move_id = stock_move_pool.search(cr, uid, [('sale_line_id','in',sale_id)],0)
                    if stock_move_id:
                        temp_lines = stock_move_id
                    lines += [x for x in temp_lines if x not in lines]
            result[invoice.id] = lines
        return result
    
    #===========================================================================
    # El primer campo picking_ids aniade los stock moves asociados a los pickings
    # El segundo campo stock_move_ids aniade los stock moves asociados a las ordenes de venta
    # Se controla en la vista cual es el campo que se muestra.
    #===========================================================================
    _columns = {
                'picking_ids': fields.one2many(
                                               'stock.picking', 'invoice_id', 'Related Pickings', readonly=True,
                                               help="Related pickings (only when the invoice has been generated from the picking)."),
                'stock_move_ids': fields.function(_compute_lines, relation='stock.move', type="many2many", string='Stock Moves',
                                                  help="Related pickings (only when the invoice has been generated from the sale order)."),
               }
    
    _defaults = {
        'stock_move_ids': False
    }


class account_invoice_line(orm.Model):
    _inherit = "account.invoice.line"
    #===========================================================================
    # Metodo para calcular los stock moves que estan asociados a sale orders o
    # a albaranes
    #===========================================================================
    def _compute_lines(self, cr, uid, ids, name, args, context=None):
        result = {}
        for invoice_line in self.browse(cr, uid, ids, context=context):
            src = []
            lines = []
            temp_lines=[]
            sale_moves_id = self.read(cr, uid, [invoice_line.id],['sale_move_ids'])
            if not sale_moves_id:
                picking_move_id = self.read(cr, uid, [invoice_line.id],['picking_move_line_ids'])
                if picking_move_id:
                    temp_lines = picking_move_id
            else:
                temp_lines = sale_moves_id 
            lines += [x for x in temp_lines if x not in lines]
            result[invoice_line.id] = lines[0].get('sale_move_ids', lines[0].get('picking_move_line_ids',[]))
        return result
    
    _columns = {
                'order_line_ids': fields.many2many('sale.order.line', 
                                                   'sale_order_line_invoice_rel', 
                                                   'invoice_id', 
                                                   'order_line_id', 
                                                   'Sale Order Lines', readonly=True,
                                                   help="Related sale order lines to this invoice line."),
                'sale_move_ids': fields.related('order_line_ids',
                                           'move_ids', 
                                           relation='stock.move', 
                                           type="one2many", 
                                           string='Stock Moves',
                                           help="Related stock moves (only when the invoice has been generated from sale order)."),
                'picking_move_line_ids': fields.one2many(
                                                 'stock.move', 'invoice_line_id', 'Related Stock Moves',
                                                 readonly=True,
                                                 help="Related stock moves (only when the invoice has been generated from the picking)."),
                'stock_move_ids': fields.function(_compute_lines, relation='stock.move', type="many2many", string='Stock Moves',
                                                  help="Related stock moves in general."),
                }
