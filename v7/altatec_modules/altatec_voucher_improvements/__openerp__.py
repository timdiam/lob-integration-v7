{
    'name'         : 'AltaTec Voucher Improvements',
    'version'      : '1.0',
    'description'  : """

                     """,
    'author'       : 'Harry Alvarez',
    'website'      : 'www.altatec.ec',
    "depends"      : [ 'account',
                       'account_voucher',
                     ],
    "data"         : [ 'views/account_account_view.xml',
                       'views/account_voucher_view.xml',
                     ],

    "css"          : [ 'static/src/css/account_voucher.css',
                     ],


    "installable"  : True,
    "auto_install" : False
}
