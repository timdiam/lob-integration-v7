from openerp.osv import fields, osv
import re
from openerp.tools.translate import _
import time
from string import split

class sri_authorizations(osv.osv):
    _inherit = 'sri.authorizations'
    
    _columns={
              'start_date': fields.date('Start Date',track_visibility='onchange',required=False,help="Date authorized for use this authorization number"),
              'expiration_date': fields.date('Expiration Date ',track_visibility='onchange',required=False,help="Date of expiry of the authorization number"),
              }
     
sri_authorizations()