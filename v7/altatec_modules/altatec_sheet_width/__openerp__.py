{ 'name'         : 'AltaTec Sheet Width',
  'version'      : '1.0',
  'description'  : """
                   This module widens all sheets that appear in form views.
                   """,
  'author'       : 'Dan Haggerty',
  'website'      : 'www.altatec.ec',
  "depends"      : [ 'web',
                   ],
  "css"          : [ 'static/src/css/sheet.css',
                   ],
  "installable"  : True,
  "auto_install" : False
}
