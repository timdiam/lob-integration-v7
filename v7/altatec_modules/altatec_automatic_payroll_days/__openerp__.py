{
    'name': 'Altatec Automatic Payroll',
    'version': '1.0',
    'description': """
        Altatec Automatic Payroll
    """,
    'author': 'Harry Alvarez',
    'website': 'www.altatececuador.com',

    "depends" : [
        "hr_contract",
        "hr_payroll",
        "ecua_hr",
        ],

    "data" : [
        'views/hr_contract_view.xml',
        'views/hr_payslip_view.xml',
        ],
    "installable": True,
    "auto_install": False
}