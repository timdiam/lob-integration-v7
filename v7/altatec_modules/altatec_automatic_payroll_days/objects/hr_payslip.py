import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree
import openerp.addons.decimal_precision as dp
import openerp.exceptions

from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp import tools
from operator import itemgetter
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging
import xml.etree.cElementTree as ET

_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL, 'en_US.utf8')


class hr_payslip(osv.osv):

    _inherit= 'hr.payslip'


    def get_inputs(self, cr, uid, contract_ids, date_from, date_to, context=None):
        res = super(hr_payslip, self).get_inputs(cr, uid, contract_ids, date_from, date_to, context=context)
        contrato=self.pool.get('hr.contract').browse(cr,uid,contract_ids)[0]

        if contrato.schedule_pay==False:
            days=30.0
        if contrato.schedule_pay=='quincena':
            days=15.0
        if contrato.schedule_pay=='monthly':
            days=30.0
        if contrato.schedule_pay=='quarterly':
            days=90.0
        if contrato.schedule_pay=='semi-annually':
            days=180.0
        if contrato.schedule_pay=='annually':
            days=360.0
        if contrato.schedule_pay=='weekly':
            days=7.0
        if contrato.schedule_pay=='bi-weekly':
            days=14.0
        if contrato.schedule_pay=='bi-monthly':
            days=60.0
        prueba=map(itemgetter('code'), res).index('DIAS_TRABAJADOS')
        res[prueba].update({
            'amount': days,
            'name': 'Numero de d\xc3\xadas calendario trabajados (1-'+str(int(days))+') [d\xc3\xadas]',
            })
        return  res

    _columns={

    }