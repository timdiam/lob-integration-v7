import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree
import openerp.addons.decimal_precision as dp
import openerp.exceptions

from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp import tools
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging
import xml.etree.cElementTree as ET

_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL, 'en_US.utf8')


class hr_contract(osv.osv):

    _inherit= 'hr.contract'

    _columns={
        'schedule_pay': fields.selection([
            ('quincena', 'Quincenalmente'),
            ('monthly', 'Mensual'),
            ('quarterly', 'Trimestralmente'),
            ('semi-annually', 'Semestralmente'),
            ('annually', 'Anualmente'),
            ('weekly', 'Semanalmente'),
            ('bi-weekly', 'Bisemanal'),
            ('bi-monthly', 'Bimensual'),
            ], 'Pago Planificado', select=True),
    }