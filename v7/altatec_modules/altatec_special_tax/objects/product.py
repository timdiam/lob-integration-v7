from osv import fields, osv
import logging
_logger = logging.getLogger(__name__)

class product_product(osv.osv):
    _inherit = 'product.product'

    def get_id_default_ice(self, cr, uid, ids, context={}):
        ice_products = self.pool.get('product.product').search(cr,uid, [('is_ice','=', True)])
        if ice_products:
            return ice_products[0]
        else:
            return False


    def on_change_tiene_ice(self, cr, uid, ids, tiene_ice, context={}):
        if tiene_ice:
            return {'value':{'ice_product':self.get_id_default_ice(cr, uid, ids, context=context)}}
        else:
            return {'value':{'ice_product':False}}

    _columns = {
                'is_ice'         : fields.boolean('Es ICE'),
                'tiene_ice'      : fields.boolean("Tiene ICE"),
                'ice_product'    : fields.many2one('product.product', string = 'ICE', domain=[('is_ice','=', True)]),
                'ice_percentage' : fields.float("Porcentaje"),
    }