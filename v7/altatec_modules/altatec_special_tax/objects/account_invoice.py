
from openerp.osv import fields, osv, orm

class account_invoice(osv.osv):

    _inherit = "account.invoice"

    def get_total_ice(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in self.browse(cr,uid,ids,context=context):
            total = 0
            for line in record.invoice_line:
                if line.product_id.is_ice:
                    total += line.price_unit * line.quantity

            res[record.id] = total

        return res

    _columns = {
                'total_ice': fields.function(get_total_ice, method=True, string='Total ICE',
                            store=True,),
    }

    def button_reset_taxes(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        old_ice_line_ids = []
        products_with_ice_ids = []

        for invoice in self.browse(cr,uid,ids,context=context):
            for line in invoice.invoice_line:
                if line.product_id.is_ice:
                    old_ice_line_ids.append(line.id)
                elif line.product_id.tiene_ice:
                    products_with_ice_ids.append(line.id)

        #unlink all old ice lines... to update prices, etc if they have changed..
        self.pool.get('account.invoice.line').unlink(cr,uid,old_ice_line_ids,context=context)

        #build ice lines again
        for line in self.pool.get('account.invoice.line').browse(cr,uid,products_with_ice_ids,context=context):
            ice_percent = line.product_id.ice_product.ice_percentage
            line_subtotal = line.quantity * line.price_unit

            ice_amount = line_subtotal * ice_percent / 100

            ice_vals = {
                        'product_id'         :line.product_id.ice_product.id ,
                        'quantity'           : 1 ,
                        'price_unit'         : ice_amount,
                        'name'               : line.product_id.ice_product.name,
                        'invoice_id'         : line.invoice_id.id,
                        'uos_id'             : line.product_id.ice_product.uom_id.id,
            }

            if line.invoice_id.type in ['in_invoice','in_refund']:

                taxes = []
                for tax in line.product_id.ice_product.supplier_tax_shadow:
                    taxes.append((4,tax.id))

                ice_vals.update({
                        'account_id'          : line.product_id.ice_product.property_account_expense.id,
                        'ecua_shadow_taxes'   : taxes,
                })

            elif line.invoice_id.type in ['out_invoice','out_refund']:
                taxes = []
                for tax in line.product_id.ice_product.customer_tax_shadow:
                    taxes.append((4,tax.id))

                ice_vals.update({
                        'account_id'          : line.product_id.ice_product.property_account_income.id,
                        'ecua_shadow_taxes'   : taxes,
                })

            self.pool.get('account.invoice.line').create(cr,uid,ice_vals,context=context)



        return super(account_invoice, self).button_reset_taxes(cr,uid,ids,context=context)