{
    'name': 'Altatec Special Tax',
    'version': '0.1',
    'license': 'AGPL-3',
    'author': 'Tim Diamond - AltaTec',
    'description': """
     This module adds the special type of tax ICE (this is treated much like a product, as the tax is included in the base for IVA.
     This module adds to each product a boolean that says "Is ICE?"  If the product is ice, a user is allowed to specify a percentage.
     Products also have an ICE field, (many2one with product.product) to link to an ICE product.  In the ATS Report, these values are considered""",
    'depends': ['account', 'product','ecua_split_tax_and_retentions','ecua_invoice'],
    'data': ['views/product_view.xml',
             'views/account_invoice_view.xml'],
}