# -*- coding: utf-8 -*-
#################################################################################
#
#################################################################################
from openerp.osv import fields, osv, orm

#################################################################################
#
#################################################################################
class stock_picking(orm.Model):

    _inherit = "stock.picking"

    _columns = { "account_move_ids" : fields.one2many( "account.move", "picking_id", "Asientos contables"),
                 'create_date'      : fields.datetime('Date Created', readonly=True),
                 'create_uid'       : fields.many2one('res.users', 'Owner', readonly=True),
                 'write_date'       : fields.datetime('Date Last Modified', readonly=True),
                 'write_uid'        : fields.many2one('res.users', 'Last Modification User', readonly=True),
               }

#################################################################################
#
#################################################################################
class stock_picking_out(osv.osv):

    _inherit = "stock.picking.out"

    fix_field_list = [ "account_move_ids",
                       "create_date",
                       "create_uid",
                       "write_date",
                       "write_uid",
                     ]

    def __init__(self, pool, cr):
        super(stock_picking_out, self).__init__(pool, cr)
        for fix_field in self.fix_field_list:
            self._columns[fix_field] = self.pool['stock.picking']._columns[fix_field]

#################################################################################
#
#################################################################################
class stock_picking_in(osv.osv):

    _inherit = "stock.picking.in"

    fix_field_list = [ "create_date",
                       "create_uid",
                       "write_date",
                       "write_uid",
                     ]

    def __init__(self, pool, cr):
        super(stock_picking_in, self).__init__(pool, cr)
        for fix_field in self.fix_field_list:
            self._columns[fix_field] = self.pool['stock.picking']._columns[fix_field]