# -*- coding: utf-8 -*-
#################################################################################
#
#################################################################################
from openerp.osv import fields, osv, orm

#################################################################################
#
#################################################################################
class product_product(orm.Model):
    _inherit = "product.product"

    def order_by_location(self, cr, uid, ids, context):
         return {
                'type':'ir.actions.act_window',
                'name':'Existencias por ubicacion',
                 'view_type' : 'form',
                'view_mode' : 'tree',
                'res_model': 'stock.location',
                'context':"{'product_id': active_id}",
                }



    _columns ={
        'ordenar': fields.function(order_by_location, string='Existencias por ubicacion', type='integer'),
    }

