# -*- coding: utf-8 -*-
#################################################################################
#
#################################################################################
from openerp.osv import fields, osv, orm

#################################################################################
#
#################################################################################
class account_move(orm.Model):

    _inherit = "account.move"

    _columns = { "picking_id"    : fields.many2one("stock.picking", "Orden de entrega"),
                 "stock_move_id" : fields.many2one("stock.move", "Movimiento de stock"),
               }