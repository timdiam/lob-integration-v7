# -*- coding: utf-8 -*-
#################################################################################
#
#################################################################################
from openerp.osv import fields, osv, orm

#################################################################################
#
#################################################################################
class stock_move(orm.Model):

    _inherit = "stock.move"

    def _create_product_valuation_moves(self, cr, uid, move, context=None):
        """
        Generate the appropriate accounting moves if the product being moves is subject
        to real_time valuation tracking, and the source or destination location is
        a transit location or is outside of the company.
        """
        if move.product_id.valuation == 'real_time': # FIXME: product valuation should perhaps be a property?
            if context is None:
                context = {}
            src_company_ctx = dict(context,force_company=move.location_id.company_id.id)
            dest_company_ctx = dict(context,force_company=move.location_dest_id.company_id.id)
            account_moves = []
            # Outgoing moves (or cross-company output part)
            if move.location_id.company_id \
                and (move.location_id.usage == 'internal' and move.location_dest_id.usage != 'internal'\
                     or move.location_id.company_id != move.location_dest_id.company_id):
                journal_id, acc_src, acc_dest, acc_valuation = self._get_accounting_data_for_valuation(cr, uid, move, src_company_ctx)
                reference_amount, reference_currency_id = self._get_reference_accounting_values_for_valuation(cr, uid, move, src_company_ctx)

                # ALTATEC ODOO BASE CODE CHANGE price_unit_fix ##################################################################
                # For a move that's leaving the company, set the price unit and currency id for the move
                move.write({'price_unit':reference_amount / move.product_qty, 'price_currency_id':reference_currency_id})
                # ALTATEC ODOO BASE CODE CHANGE price_unit_fix ##################################################################

                #returning goods to supplier
                if move.location_dest_id.usage == 'supplier':
                    account_moves += [(journal_id, self._create_account_move_line(cr, uid, move, acc_valuation, acc_src, reference_amount, reference_currency_id, context))]
                else:
                    account_moves += [(journal_id, self._create_account_move_line(cr, uid, move, acc_valuation, acc_dest, reference_amount, reference_currency_id, context))]

            # Incoming moves (or cross-company input part)
            if move.location_dest_id.company_id \
                and (move.location_id.usage != 'internal' and move.location_dest_id.usage == 'internal'\
                     or move.location_id.company_id != move.location_dest_id.company_id):
                journal_id, acc_src, acc_dest, acc_valuation = self._get_accounting_data_for_valuation(cr, uid, move, dest_company_ctx)
                reference_amount, reference_currency_id = self._get_reference_accounting_values_for_valuation(cr, uid, move, src_company_ctx)

                # ALTATEC ODOO BASE CODE CHANGE price_unit_fix ##################################################################
                # For a move that's leaving the company, set the price unit and currency id for the move
                move.write({'price_unit':reference_amount / move.product_qty, 'price_currency_id':reference_currency_id})
                # ALTATEC ODOO BASE CODE CHANGE price_unit_fix ##################################################################

                #goods return from customer
                if move.location_id.usage == 'customer':
                    account_moves += [(journal_id, self._create_account_move_line(cr, uid, move, acc_dest, acc_valuation, reference_amount, reference_currency_id, context))]
                else:
                    account_moves += [(journal_id, self._create_account_move_line(cr, uid, move, acc_src, acc_valuation, reference_amount, reference_currency_id, context))]

            move_obj = self.pool.get('account.move')
            for j_id, move_lines in account_moves:
                account_move_id = move_obj.create(cr, uid, { 'journal_id'   : j_id,
                                                             'line_id'      : move_lines,
                                                             'stock_move_id': move.id,
                                                             'picking_id'   : move.picking_id and move.picking_id.id,
                                                             'ref'          : move.picking_id and move.picking_id.name,
                                                           }, context=context)
                move.write( { "account_move_id" : account_move_id } )


    _columns = { "account_move_id" : fields.many2one("account.move", "Asiento contable"),
               }