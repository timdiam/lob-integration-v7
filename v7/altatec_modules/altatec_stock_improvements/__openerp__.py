{
    'name'         : 'AltaTec Stock Improvements',
    'version'      : '1.0',
    'description'  : """
                     Cambia la vista de mrp.production en el campo stock.moves y elimina el boton desechar producto
                     """,
    'author'       : 'Henry Lomas A, Dan Haggerty',
    'website'      : 'www.altatec.ec',
    "depends"      : [ 'mrp',
                       'stock',
                       'account',
                       'product',
                       'stock_location',
                     ],
    "data"         : [ 'views/mrp_view.xml',
                       'views/stock_move.xml',
                       'views/stock_picking.xml',
                       'views/product.xml',
                       'views/stock_partial_picking_view.xml',
                     ],
    "installable"  : True,
    "auto_install" : False
}
