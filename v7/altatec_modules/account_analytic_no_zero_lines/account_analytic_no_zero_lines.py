# -*- coding: utf-8 -*-
#################################################################################
#
# A module that contains a credit card model can be attached to other
# objects (e.g. invoices).
#
# Author:  Tim Diamond
# Company: AltaTec Ecuador
# Date:    Jan. 17th, 2014
#
#################################################################################
from openerp.osv import fields, osv, orm
import logging

_logger = logging.getLogger(__name__)

class account_move(osv.osv):
    _inherit = 'account.move'

    def create(self , cr, uid, vals, context={}):
        _logger.debug(vals)
        _logger.debug(context)
        return super(account_move,self).create(cr,uid,vals,context=context)

class account_move_line(osv.osv):

    _inherit = "account.move.line"

    def create(self, cr, uid, vals, context={}):
        _logger.debug(vals)
        _logger.debug(context)
        return super(account_move_line,self).create(cr,uid,vals,context=context)

    def create_analytic_lines(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        super(account_move_line, self).create_analytic_lines(cr, uid, ids, context=context)
        analytic_line_obj = self.pool.get('account.analytic.line')
        for line in self.browse(cr, uid, ids, context=context):
            remove_zeros = analytic_line_obj.search(cr, uid, [('move_id','=',line.id),('amount','<=',.00001),('amount','>=',-.00001)], context=context)
            if remove_zeros:
                analytic_line_obj.unlink(cr, uid, remove_zeros, context=context)

        return True



