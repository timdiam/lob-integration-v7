{
    'name': 'Account analytic remove zero lines',
    'version': '1.0',
    'description': """
        This modules restricts the creation of analytic account lines when they have zero in debit and credit
    """,
    'author': 'Tim Diamond',
    'website': 'www.altatececuador.com',
    "depends" : [ 'account_analytic_plans' ],
    "data" : [],
    "installable": True,
    "auto_install": False
}