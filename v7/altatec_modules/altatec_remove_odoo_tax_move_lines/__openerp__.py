{
    'name': 'Altatec remove odoo tax lines',
    'version': '0.1',
    'license': 'AGPL-3',
    'author': 'Tim Diamond - AltaTec',
    'description': """
     This module removes the tax lines that show up in vanilla odoo.  We will be not using the odoo motor for tax calculations""",
    'depends': ['account'],
    'data': [],
}
