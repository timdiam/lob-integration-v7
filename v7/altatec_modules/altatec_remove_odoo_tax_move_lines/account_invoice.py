from osv import fields, osv
import logging
_logger = logging.getLogger(__name__)

class account_invoice(osv.osv):
    _inherit = 'account.invoice'

    #Here we remove the nasty Odoo Tax lines from the account move, since we dont really use the tax engine anyways...
    def finalize_invoice_move_lines(self, cr, uid, invoice_browse, move_lines):
        good_lines = []
        for line in move_lines:
            if (line[2]['credit'] == False) and (line[2]['debit'] == False):
                pass
            else:
                good_lines.append(line)

        good_lines.sort(key= lambda x: x[2]['debit'] if x[2]['debit'] else 0)
        return good_lines