# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Patricio Rangles
# Copyright (C) 2013  TRESCLOUD CIA LTDA
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

{
    "name" : "Ecuador Invoices Sequence Improve",
    "version" : "1.0",
   # 'sequence': 4,
    'complexity': "easy",
    "author" : "TRESCLOUD CIA LTDA",
    "website" : "http://www.trescloud.com/",
    "category" : "Ecuadorian Regulations",
    "depends" : [
                 'base',
                 'ecua_invoice',
                 ],
    "description": """
    
    This module improve the sequence on invoice, adding the proper secuence of diary to the number of invoice manually added.
    
    This module is part of a bigger framework of "ecua" modules developed by 
    TRESCLOUD, EcuadorEnLinea y 4RSOFT.
    
    Author,
    
    Patricio Rangles
    TRESCLOUD Cía Ltda.
    """,
    "init_xml": [],
    "update_xml": [],
    "installable": True,
    "auto_install": False,
    "application": False,
}
