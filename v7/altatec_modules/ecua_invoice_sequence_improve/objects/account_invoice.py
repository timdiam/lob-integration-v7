# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Patricio Rangles
# Copyright (C) 2013  TRESCLOUD Cia Ltda
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import osv
from osv import fields
from tools.translate import _

class account_invoice(osv.osv):

    _inherit = "account.invoice"
    
    def action_number(self, cr, uid, ids, context=None):
        """
        Complementary function to resolve the issue in the sequence number on invoice
        """
        setting_exist = True

        if setting_exist:
            for inv in ids:
                inv = self.browse(cr, uid, inv)
                internal_number = inv.internal_number
                super(account_invoice, self).action_number(cr, uid, ids, context)
                self.write(cr, uid, ids, {'internal_number': internal_number})
        else:
            super(account_invoice, self).action_number(cr, uid, ids, context)
            
        return True 

account_invoice()
