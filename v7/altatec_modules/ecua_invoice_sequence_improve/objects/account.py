# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Patricio Rangles
# Copyright (C) 2013  TRESCLOUD Cia Ltda
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import osv
from osv import fields
from tools.translate import _

class account_move(osv.osv):

    _inherit = "account.move"
    _name = "account.move"
    
    def post(self, cr, uid, ids, context=None):
        """
        Complementary function to resolve the issue in the sequence number on invoice
        """

        # here read the setting to use or not the correction
        setting_exist = True

        if context is not None:
            if setting_exist:
                invoice = context.get('invoice', False)
                inv_obj = self.pool.get('account.invoice')
                number_cp = False
                if invoice and invoice.internal_number:
                    number_cp = invoice.internal_number
                    invoice.internal_number = False
                    context['invoice'] = invoice  
                    super(account_move, self).post(cr, uid, ids, context)
                    inv_obj.write(cr, uid, invoice.id, {'internal_number':number_cp})
                    #move = self.browse(cr, uid, ids[0])
                    #new_name = number_cp + "/" + move.name
                    #self.write(cr, uid, [ids[0]], {'name':new_name})
                else:
                    super(account_move, self).post(cr, uid, ids, context)
            else:
                super(account_move, self).post(cr, uid, ids, context)
        else:
            super(account_move, self).post(cr, uid, ids, context)
            
        return True


account_move()

class account_move_line(osv.osv):
    
    _inherit = "account.move.line"
    _name = "account.move.line"

    def name_get(self, cr, uid, ids, context=None):
        """
        Complementary function to resolve the issue in the sequence number on invoice
        This name get help to show the number of invoice in payments.
        """
        if not ids:
            return []
        result = []
        for line in self.browse(cr, uid, ids, context=context):
            if line.invoice and line.ref:
                result.append((line.id, (line.invoice.internal_number or '') + '/' + (line.move_id.name or '')+' ('+line.ref+')'))
            elif line.invoice:
                result.append((line.id, (line.invoice.internal_number or '') + '/' + (line.move_id.name or '')))
            elif line.ref:
                result.append((line.id, (line.move_id.name or '')+' ('+line.ref+')'))
            else:
                result.append((line.id, line.move_id.name))
        return result

account_move_line()