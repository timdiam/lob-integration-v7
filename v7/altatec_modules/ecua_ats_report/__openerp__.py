# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Andres Calle, Patricio Rangles, Castor Calle
# Copyright (C) 2013  TRESCLOUD CÍA LTDA
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

{
    "name" : "Reporte ATS",
    "version" : "1.1",
   # 'sequence': 4,
    'complexity': "hard",
    "author" : "TRESCLOUD CÍA LTDA",
    "website" : "http://www.trescloud.com/",
    "category" : "Ecuadorian Regulations",
    # TODO agregar dependencia a aeroo
    "depends" : [
                 'base',
                 'ecua_ats',
                 ],
    "description": """
    Este modulo agrega el reporte ATS (Anexo Transaccional Simplificado), el mismo debe ser presentado de forma mensual
    a la autoridad tributaria del Ecuador, el SRI.
    
    This module adds the ATS (Anexo Transaccional Simplificado) report, this must be sent
    to the tax authority of Ecuador, the SRI.
    
    This module is part of a bigger framework of "ecua" modules developed by 
    TRESCLOUD
    
    Author,
    
    Andres Calle,
    Patricio Rangles,
    Castor Calle,
    TRESCLOUD Cía Ltda.
    """,
    "init_xml": [],
    "update_xml": [ 
                    'report/report_data.xml',
    ],
    "installable": True,
    "auto_install": False,
    "application": False,
}
