##########################################################################################
# This file contains the parser for the 'Reporte por productos' report.
#
# Author:   Harry Alvarez, Dan Haggerty
# Company:  AltaTec (www.altatec.ec)
# Date:     18/6/2015
##########################################################################################
from report import report_sxw
import logging

_logger = logging.getLogger(__name__)

##########################################################################################
# Parser class override
##########################################################################################
class Parser(report_sxw.rml_parse):

    ##########################################################################################
    # __init__() override
    ##########################################################################################
    def __init__(self, cr, uid, name, context):

        super(Parser, self).__init__(cr, uid, name, context)

        self.localcontext.update({ 'get_bodegas'         : self.get_bodegas,
                                   'location_has_moves'  : self.location_has_moves,
                                   'warehouse_has_moves' : self.warehouse_has_moves,
                                   'get_total'           : self.get_total,
                                   'get_total_cantidad_inv'  : self.get_total_cantidad_inv,
                                   'get_total_cantidad_orig' : self.get_total_cantidad_orig,
                                   'cr'                  : cr,
                                   'uid'                 : uid,
                                   'b_context'           : context,
                                 })

    ##########################################################################################
    # Get the total cost for a given product and location
    ##########################################################################################
    def get_total(self, product, location):
        total = 0.00
        for move in location['moves']:
            for line in move:
                total += line['total']
        if abs(total) < 0.01:
            total = "0.00"
        return total

    ##########################################################################################
    # Get the total quantity (inventory) for a given product and location
    ##########################################################################################
    def get_total_cantidad_inv(self, product, location):
        total = 0.00
        for move in location['moves']:
            for line in move:
                total += line['cantidad_inv']
        if abs(total) < 0.01:
            total = "0.00"
        return total

    ##########################################################################################
    # Get the total quantity (origin) for a given product and location
    ##########################################################################################
    def get_total_cantidad_orig(self, product, location):
        total = 0.00
        for move in location['moves']:
            for line in move:
                total += line['cantidad']
        if abs(total) < 0.01:
            total = "0.00"
        return total

    ##########################################################################################
    # Return True if there are moves in this warehouse
    ##########################################################################################
    def warehouse_has_moves(self, warehouse):
        if not warehouse or not 'ubicacion' in warehouse or not len(warehouse['ubicacion']):
            return False
        for location in warehouse['ubicacion']:
            if len(location['moves']):
                return True
        return False

    ##########################################################################################
    # Return True if there are moves in this location
    ##########################################################################################
    def location_has_moves(self, location):
        if not 'moves' in location or not len(location['moves']):
            return False
        return True

    ##########################################################################################
    # Format the type name properly
    ##########################################################################################
    def format_type(self, type):
        if type == "out":
            return "Egreso"
        elif type == "internal":
            return "Interno"
        elif type == "in":
            return "Ingreso"
        else:
            return ""

    ##########################################################################################
    # Get a data structure containing all of the data of the report
    ##########################################################################################
    def get_bodegas(self, bodegas):

        product_uom_obj = self.pool.get('product.uom')
        producto = []

        cr      = self.localcontext['cr']
        uid     = self.localcontext['uid']
        context = self.localcontext

        # obtengo los id de los prodcutos que se han seleccionado
        if context['producto']:
            productos = context['producto']

        for produ in productos:

            # en el caso de que las condiciones esten se las agrega, caso contracio el arg=[] ira vacio
            arg = [ ('state', '=', 'done'),
                    ('product_id.id', '=', produ),
                  ]
            if context['fecha_inicio']:
                arg.append( ('date', '>', context['fecha_inicio']) )
            if context['fecha_fin']:
                arg.append( ('date', '<', context['fecha_fin']) )

            # busqueda de de todos los movimientos con las condiciones que se piden
            stock_ids= self.pool.get('stock.move').search(cr, uid, arg)
            stock_obj=self.pool.get('stock.move').browse(cr, uid, stock_ids)

            # se crea la lista de todas las bodegas seleccionadas, al momento de pasar, no pasan como lista sino como browse()
            list_ids = []
            for b in bodegas:
                list_ids.append(b.id)

            bodega = []

            # se inicia la busqueda por bodegas
            for bode in list_ids:
                bodega_obj = self.pool.get('stock.warehouse').browse(cr,uid,bode)

                if context['warehouse_managers_installed'] == True:
                    if not bodega_obj.group_location:
                        raise osv.except_osv("Error", "No hay una ubicacion de agrupacion configurada para bodega: " + bodega_obj.name)
                    stock_location_ids = self.pool.get('stock.location').search(cr,uid,[('location_id.id','=',bodega_obj.group_location.id)])
                else:
                    stock_location_ids       = [bodega_obj.lot_stock_id.id, bodega_obj.lot_input_id.id]
                    stock_location_stock_ids = self.pool.get('stock.location').search(cr,uid,[('location_id.id','=',bodega_obj.lot_stock_id.id)])
                    stock_location_in_ids    = self.pool.get('stock.location').search(cr,uid,[('location_id.id','=',bodega_obj.lot_input_id.id)])
                    stock_location_ids       = list(set(stock_location_ids+stock_location_stock_ids + stock_location_in_ids))

                ubicacion=[]

                for ubi in stock_location_ids:
                    stock_l_objs = self.pool.get('stock.location').browse(cr,uid,ubi)
                    moves = []

                    for move in stock_obj:
                        datos = []

                        cantidad_inv = product_uom_obj._compute_qty(cr,uid, move.product_uom.id, move.product_qty, move.product_id.uom_id.id)

                        # Get ingresos
                        if ubi == move.location_dest_id.id:
                            datos.append({ 'fecha'        : move.date,
                                           'origen'       : move.origin,
                                           'destino'      : move.partner_id.name,
                                           'ref'          : move.product_id.default_code,
                                           'producto'     : move.product_id.name,
                                           'modelo'       : move.product_id.modelo if hasattr(move,'modelo') else "",
                                           'cantidad'     : move.product_qty,
                                           'unidad'       : move.product_uom.name,
                                           'cantidad_inv' : cantidad_inv,
                                           'unidad_inv'   : move.product_id.uom_id.name,
                                           'tipo'         : self.format_type(move.type),
                                           'ubi_origen'   : move.location_id.name,
                                           'ubi_destino'  : move.location_dest_id.name,
                                           'precio_costo' : move.price_unit,
                                           'total'        : move.price_unit * move.product_qty,
                                         })
                            moves.append(datos)

                        # Get egresos
                        if ubi == move.location_id.id:
                            datos.append({ 'fecha'        : move.date,
                                           'origen'       : move.origin,
                                           'destino'      : move.partner_id.name,
                                           'ref'          : move.product_id.default_code,
                                           'producto'     : move.product_id.name,
                                           'modelo'       : move.product_id.modelo if hasattr(move,'modelo') else "",
                                           'cantidad'     : -1 * move.product_qty,
                                           'unidad'       : move.product_uom.name,
                                           'cantidad_inv' : -1 * cantidad_inv,
                                           'unidad_inv'   : move.product_id.uom_id.name,
                                           'tipo'         : self.format_type(move.type),
                                           'ubi_origen'   : move.location_id.name,
                                           'ubi_destino'  : move.location_dest_id.name,
                                           'precio_costo' : move.price_unit,
                                           'total'        : -1 * move.price_unit * move.product_qty,
                                         })
                            moves.append(datos)

                    ubicacion.append({ 'nombre_ubicacion' : stock_l_objs.name,
                                       'moves'            : moves,
                                     })

                bodega.append({ 'nombre_bodega' : bodega_obj.name,
                                'ubicacion'     : ubicacion,
                              })

            producto_obj = self.pool.get('product.product').browse(cr,uid,produ)

            producto.append({ 'nombre_producto' : producto_obj.name,
                              'ref_producto'    : producto_obj.default_code,
                              'bodega'          : bodega,
                            })
        return producto