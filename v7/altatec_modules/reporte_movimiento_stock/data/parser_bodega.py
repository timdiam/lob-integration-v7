##########################################################################################
# This file contains the parser for the "Reporte por bodega" report.
#
# Author:   Harry Alvarez, Dan Haggerty
# Company:  AltaTec (www.altatec.ec)
# Date:     18/6/2015
##########################################################################################
from report import report_sxw
from openerp.osv import fields, osv, orm
import logging

_logger = logging.getLogger(__name__)

##########################################################################################
# Parser class definition
##########################################################################################
class Parser(report_sxw.rml_parse):

    ##########################################################################################
    # __init__() override
    ##########################################################################################
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context)
        self.localcontext.update({ 'get_bodegas' : self.get_bodegas,
                                   'warehouse_has_no_moves' : self.warehouse_has_no_moves,
                                   'location_has_ingresos'  : self.location_has_ingresos,
                                   'location_has_egresos'   : self.location_has_egresos,
                                   'cr'  : cr,
                                   'uid' : uid,
                                   'b_context' : context
                                 })

    ##########################################################################################
    # Return true if there have been no moves in any of the locations of the warehouse
    ##########################################################################################
    def warehouse_has_no_moves(self, warehouse):
        if not warehouse or not 'ubicacion' in warehouse or not len(warehouse['ubicacion']):
            return True
        for location in warehouse['ubicacion']:
            if len(location['ingreso']) or len(location['egreso']):
                return False
        return True

    ##########################################################################################
    # Return true if there have been ingresos in the given location
    ##########################################################################################
    def location_has_ingresos(self, location):
        if location and 'ingreso' in location and len(location['ingreso']):
            return True
        return False

    ##########################################################################################
    # Return true if there have been egresos in the given location
    ##########################################################################################
    def location_has_egresos(self, location):
        if location and 'egreso' in location and len(location['egreso']):
            return True
        return False

    ##########################################################################################
    # Return a data structure containing all of the data needed for the report
    ##########################################################################################
    def get_bodegas(self, bodegas):

        context = self.localcontext
        cr = context['cr']
        uid = context['uid']

        # en el caso de que las condiciones esten se las agrega, caso contracio el arg=[] ira vacio
        product_uom_obj = self.pool.get('product.uom')

        arg=[]

        arg.append((('state', '=', 'done')))

        if context['fecha_inicio']:
            arg.append((('date', '>', context['fecha_inicio'])))

        if context['fecha_fin']:
            arg.append((('date', '<', context['fecha_fin'])))

        # busqueda de de todos los movimientos con las condiciones que se piden
        stock_ids = self.pool.get('stock.move').search(cr, uid, arg)
        stock_obj = self.pool.get('stock.move').browse(cr, uid, stock_ids)

        #se crea la lista de todas las bodegas seleccionadas, al momento de pasar, no pasan como lista sino como browse()
        list_ids = []
        for b in bodegas:
            list_ids.append(b.id)

        bodega = []

        for bode in list_ids:
            bodega_obj = self.pool.get('stock.warehouse').browse(cr,uid,bode)

            if context['warehouse_managers_installed'] == True:
                if not bodega_obj.group_location:
                    raise osv.except_osv("Error", "No hay una ubicacion de agrupacion configurada para bodega: " + bodega_obj.name)
                stock_location_ids = self.pool.get('stock.location').search(cr,uid,[('location_id.id','=',bodega_obj.group_location.id)])
            else:
                stock_location_ids       = [bodega_obj.lot_stock_id.id, bodega_obj.lot_input_id.id]
                stock_location_stock_ids = self.pool.get('stock.location').search(cr,uid,[('location_id.id','=',bodega_obj.lot_stock_id.id)])
                stock_location_in_ids    = self.pool.get('stock.location').search(cr,uid,[('location_id.id','=',bodega_obj.lot_input_id.id)])
                stock_location_ids       = list(set(stock_location_ids+stock_location_stock_ids + stock_location_in_ids))

            ubicacion = []

            for ubi in stock_location_ids:
                stock_l_objs = self.pool.get('stock.location').browse(cr,uid,ubi)
                egreso  = []
                ingreso = []

                for move in stock_obj:
                    datos = []
                    if ubi == move.location_dest_id.id:
                        datos.append({ 'fecha'        : move.date,
                                       'origen'       : move.origin,
                                       'destino'      : move.partner_id.name,
                                       'ref'          : move.product_id.default_code,
                                       'producto'     : move.product_id.name,
                                       'modelo'       : move.product_id.modelo if hasattr(move,'modelo') else "",
                                       'cantidad'     : move.product_qty,
                                       'unidad'       : move.product_uom.name,
                                       'cantidad_inv' : product_uom_obj._compute_qty(cr,uid, move.product_uom.id, move.product_qty, move.product_id.uom_id.id),
                                       'unidad_inv'   : move.product_id.uom_id.name,
                                       'tipo'         : move.type,
                                       'ubi_origen'   : move.location_id.name,
                                       'ubi_destino'  : move.location_dest_id.name
                                     })

                        ingreso.append(datos)

                    if ubi == move.location_id.id:
                        datos.append({ 'fecha'        : move.date,
                                       'origen'       : move.origin,
                                       'destino'      : move.partner_id.name,
                                       'ref'          : move.product_id.default_code,
                                       'producto'     : move.product_id.name,
                                       'modelo'       : move.product_id.modelo if hasattr(move,'modelo') else "",
                                       'cantidad'     : move.product_qty,
                                       'unidad'       : move.product_uom.name,
                                       'cantidad_inv' : product_uom_obj._compute_qty(cr,uid, move.product_uom.id, move.product_qty, move.product_id.uom_id.id),
                                       'unidad_inv'   : move.product_id.uom_id.name,
                                       'tipo'         : move.type,
                                       'ubi_origen'   : move.location_id.name,
                                       'ubi_destino'  : move.location_dest_id.name
                                     })

                        egreso.append(datos)

                ubicacion.append({ 'nombre_ubicacion' : stock_l_objs.name,
                                   'egreso'           : egreso,
                                   'ingreso'          : ingreso,
                                 })

            bodega.append({ 'nombre_bodega' : bodega_obj.name,
                            'ubicacion'     : ubicacion,
                          })

        return bodega