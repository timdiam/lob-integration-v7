##########################################################################################
# This file contains the model definitions for 'reporte.bodega.wizard' and
# 'reporte.producto.wizard'. These are the wizards that appear when the user clicks the
# Reporte por Bodega and Reporte por Producto menus under the Almacen/Trazabilidad menu.
# These wizards allow the user to generate excel reports that are either on the warehouse
# or product level, showing stock movements in and out of selected warehouses.
#
# Author:   Harry Alvarez, Dan Haggerty
# Company:  AltaTec (www.altatec.ec)
# Date:     18/6/2015
##########################################################################################
from datetime import date
import locale
from openerp.osv import fields, osv, orm
import logging

_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')

##########################################################################################
# reporte.bodega.wizard model definition
##########################################################################################
class reporte_bodega_wizard(osv.osv):

    _name = 'reporte.bodega.wizard'

    ##########################################################################################
    # Generate the report with the selected parameters
    ##########################################################################################
    def generar_reporte(self,cr,uid,ids,context=None):

        if not context:
            context = {}

        records = self.browse(cr,uid,ids)[0]

        if records.all_bodegas == True:
            bodega = self.pool.get('stock.warehouse').search(cr,uid,[])
            if not len(bodega):
                raise osv.except_osv("Error!", "No se encuentran algunas bodegas en el sistema.")
        else:
            bodega = []
            for bode in records.bodegas:
                bodega.append(bode.id)
            if not len(bodega):
                raise osv.except_osv("Error!", "Debe seleccionar algunas bodegas.")

        if hasattr(self.pool.get('stock.warehouse').browse(cr, uid, bodega[0], context=context), 'group_location'):
            warehouse_managers_installed = True
        else:
            warehouse_managers_installed = False

        context.update({ 'fecha_inicio' : records.fecha_inicio,
                         'fecha_fin'    : records.fecha_fin,
                         'warehouse_managers_installed' : warehouse_managers_installed,
                       })

        result = { 'type'        : 'ir.actions.report.xml',
                   'context'     : context,
                   'report_name' : 'reporte_stock_bodega',
                   'datas'       : { 'ids' : bodega }
                 }

        return result

    ##########################################################################################
    # Column and defaults definition
    ##########################################################################################
    _columns = { 'fecha_inicio' : fields.date("Fecha de Inicio"),
                 'fecha_fin'    : fields.date("Fecha Fin"),
                 'bodegas'      : fields.many2many('stock.warehouse', 'stock_bodega_wizard', 'wizard_id','bodega_id', select=True, string="Bodegas"),
                 "all_bodegas"  : fields.boolean("Todas las Bodegas"),
               }

    _defaults = { "fecha_fin" : date.today().strftime('%Y-%m-%d')
                }

##########################################################################################
# reporte.producto.wizard model definition
##########################################################################################
class reporte_producto_wizard(osv.osv):

    _name = 'reporte.producto.wizard'

    ##########################################################################################
    # Generate the report with the selected parameters
    ##########################################################################################
    def generar_reporte(self, cr, uid, ids, context=None):

        if not context:
            context={}

        records=self.browse(cr,uid,ids)[0]

        if records.all_bodegas==True:
            bodega= self.pool.get('stock.warehouse').search(cr,uid,[])
        else:
            bodega=[]
            for bode in records.bodegas:
                bodega.append(bode.id)

        producto=[]
        for produ in records.productos:
            producto.append(produ.id)

        if hasattr(self.pool.get('stock.warehouse').browse(cr, uid, bodega[0], context=context), 'group_location'):
            warehouse_managers_installed = True
        else:
            warehouse_managers_installed = False

        context.update({ 'fecha_inicio' : records.fecha_inicio,
                         'fecha_fin'    : records.fecha_fin,
                         'producto'     : producto,
                         'warehouse_managers_installed' : warehouse_managers_installed,
                       })

        result = { 'type'        : 'ir.actions.report.xml',
                   'context'     : context,
                   'report_name' : 'reporte_stock_bodega_producto',
                   'datas'       : { 'ids' : bodega }
                 }

        return result

    ##########################################################################################
    # Column and default definitions
    ##########################################################################################
    _columns = { 'fecha_inicio' : fields.date("Fecha de Inicio"),
                 'fecha_fin'    : fields.date("Fecha Fin"),
                 'bodegas'      : fields.many2many('stock.warehouse', 'stock_bodega_producto_wizard', 'wizard_id','bodega_id', select=True, string="Bodegas"),
                 "productos"    : fields.many2many("product.product", 'stock_producto_bodega_wizard', 'wizard_id', 'product_id', select=True, string="Producto"),
                 "all_bodegas"  : fields.boolean("Todas las Bodegas"),
                 "stock_move"   : fields.many2one("stock.move", "Stock Move"),
                 "tipo"         : fields.related('stock_move','type', type='selection', selection=[('out', 'Envio mercancias'), ('in', 'Recepcion mercancias'), ('internal', 'Interno')],string="Motivo"),
               }

    _defaults = { "fecha_fin" : date.today().strftime('%Y-%m-%d')
                }