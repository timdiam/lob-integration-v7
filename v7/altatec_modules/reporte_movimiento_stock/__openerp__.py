{
    'name': "Reporte Movimiento Stock",
    'version': '1.0',
    'description': """
            Movimiento Stock Report
""",
    'author': 'Harry Alvarez, Dan Haggerty',
    'website': 'www.altatec.ec',
    "depends": ['stock'],
    "data": ['reporte_movimiento_stock_view.xml',
             'data/report_bodega.xml',
             'data/report_bodega_producto.xml',
             'security/ir.model.access.csv',

    ],
    "demo": [],
    "active": False,
    "installable": True

}