import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree
import openerp.addons.decimal_precision as dp
import openerp.exceptions

from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging
import xml.etree.cElementTree as ET


_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')


class ec_grupo_codigo(osv.osv):
        _name= 'ec.grupo.codigo'


        def name_get(self, cr, uid, ids, context=None):
            records=self.browse(cr,uid,ids)[0]
            res = super(ec_grupo_codigo, self).name_get(cr, uid, ids, context=context)
            if records:
                if (records.name and records.desc):
                    nombre=records.name+"-"+records.desc
                    res[0]=(1,nombre)

            return res

        _columns={
                  'name':fields.char(string="Nombre"),
                  'desc':fields.char(string="Descripcion"),
                  }


class ec_grupo_linea(osv.osv):
        _name= 'ec.grupo.linea'


        def name_get(self, cr, uid, ids, context=None):
            records=self.browse(cr,uid,ids)[0]
            res = super(ec_grupo_linea, self).name_get(cr, uid, ids, context=context)
            if records:
                if (records.name and records.desc):
                    nombre=records.name+"-"+records.desc
                    res[0]=(1,nombre)

            return res

        _columns={
                  'name':fields.char(string="Nombre"),
                  'desc':fields.char(string="Descripcion"),
                  }


class product_product(osv.osv):
        _inherit = 'product.product'


        def get_codigo(self,cr,uid,ids,field_name,field_value,arg,context=None):
                records = self.browse(cr,uid,ids)
                result ={}
                codigo=""

                for r in records:
                        if( r.ec_grupo.name != None):
                            codigo+=str(r.ec_grupo.name)

                        if ( r.ec_linea.name != False):
                            codigo+=str(r.ec_linea.name)

                        if( r.ec_identificacion != False):
                            codigo += str(r.ec_identificacion)

                result[r.id]=codigo

                return result


        def get_ids(self,cr,uid,ids,context={}):
                return ids

        _columns = {

                        'default_code': fields.function(get_codigo, type="char", size=64, select=True, readonly=True, string="Referencia Interna",
                                        store = {'product.product':(get_ids,['ec_grupo','ec_grupo','ec_grupo',],10),} ),

                        'ec_grupo':fields.many2one('ec.grupo.codigo',"Grupo", required=True),
                        'ec_linea':fields.many2one('ec.grupo.linea',"Linea", required=True),
                        'ec_identificacion':fields.char("Identificacion",required=True),


                }



