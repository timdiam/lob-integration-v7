from openerp import tools, pooler
from openerp.osv import fields,osv

class minuto_bug_fix(osv.osv):
    _name = "minuto.bug.fix"
    _auto = False

    def init(self, cr):
        pool = pooler.get_pool(cr.dbname)
        account_invoice_tax_list = pool.get('account.invoice.tax').search(cr,1,[])
        account_invoice_line_objs = pool.get('account.invoice.tax').browse(cr,1,account_invoice_tax_list)

        tax_obj = pool.get('account.tax')
        taxes = tax_obj.search(cr,1,[])
        taxes_objs = tax_obj.browse(cr,1,taxes)

        for tax in taxes_objs:
            if (tax.name == 'RETENCIONES 0% IVA BASE') or \
            (tax.name == 'RETENCIONES 0% IVA') or \
            (tax.name == 'RETENCIONES 70% IVA') or \
            (tax.name == 'RETENCIONES 70% IVA BASE') or \
            (tax.name == "RETENCIONES 30% IVA") or \
            (tax.name == "RETENCIONES 30% IVA BASE") or \
            (tax.name == "RETENCIONES 100% IVA") or \
            (tax.name == "RETENCIONES 100% IVA BASE"):
                tax_obj.write(cr,1,[tax.id],{'is_retention_of_iva':True})

        for line in account_invoice_line_objs:
            if line.name == "RETENCIONES 0% IVA":
                pool.get('account.invoice.tax').write(cr,1,[line.id],{'is_retention_of_iva':True})
                #pool.get('account.invoice').write(cr,1,[line.invoice_id.id],{})
            elif (line.name == 'RETENCIONES 70% IVA') or \
            (line.name == 'RETENCIONES 70% IVA BASE') or \
            (line.name == "RETENCIONES 30% IVA") or \
            (line.name == "RETENCIONES 30% IVA BASE") or \
            (line.name == "RETENCIONES 100% IVA") or \
            (line.name == "RETENCIONES 100% IVA BASE"):
                pool.get('account.invoice.tax').write(cr,1,[line.id],{'is_retention_of_iva':True})