{ 'name'         : 'AltaTec Minuto tax line Fix',
  'version'      : '1.0',
  'description'  : """
                   This module is used to fix issues related to minuto tax line from May 2015
                   """,
  'author'       : 'Tim Diamond',
  'website'      : 'www.altatec.ec',
  "depends"      : [ 'base','account','l10n_ec_niif_minimal'
                   ],
  "data"         : [
                   ],
  "installable"  : True,
  "auto_install" : False
}
