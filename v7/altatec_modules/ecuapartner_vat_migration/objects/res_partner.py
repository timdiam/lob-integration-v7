# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Andres Calle, Patricio Rangles, Pablo Vizhnay
# Copyright (C) 2013  TRESCLOUD Cia Ltda
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from openerp.osv import osv
from openerp.osv import fields
from openerp.tools.translate import _
from openerp.tools.misc import ustr
import time
import re #para busqueda por cedula
import unicodedata
from openerp import pooler



class res_partner(osv.osv):
    _inherit = "res.partner"




    def init(self,cr):
        pool = pooler.get_pool(cr.dbname)
        uid=1
        res_partner_id_list=pool.get('res.partner').search(cr,uid,[])

        for cliente in  pool.get('res.partner').browse(cr,uid,res_partner_id_list):

            if(cliente.vat):
                new_values=self.migrate_legacy_vat(cliente.vat)
                self.write(cr,uid,[cliente.id],{
                    'tipo_vat'      : new_values[0] ,
                    'identificacion': new_values[1] ,

                })
                print "Cliente id: ",cliente.id

    def migrate_legacy_vat(self,vat):
        aux  = vat
        modifications=[]
        if aux:
             if len(aux) <= 2:
                 modifications.append('OT')
                 modifications.append(aux)
             elif aux[0:2] == "EC":
                 if len(aux[2:])== 10:
                     modifications.append('EC')
                     modifications.append(aux[2:])
                 elif len(aux[2:])== 13:
                     modifications.append('RUC')
                     modifications.append(aux[2:])
                 else:
                     modifications.append('OT')
                     modifications.append(aux[2:])
             else:
                  modifications.append('PAS')
                  modifications.append(aux)

        return  modifications

    def check_vat(self, cr, uid, ids, context=None):
         return True

    def _avoid_duplicated_vat(self, cr, uid, ids, context=None):
        return True

        









