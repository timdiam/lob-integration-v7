from openerp.osv import fields, osv, orm

class res_partner(osv.osv):

    _inherit = 'res.partner'

    _columns = {  "fecha_factura_cliente":fields.char("Ultima Factura Cliente"),
                  "fecha_factura_proveedor":fields.char("Ultima Factura Proveedor")
               }