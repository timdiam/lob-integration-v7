# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 TRESCloud (<http://www.trescloud.com>)
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from osv import fields,osv
import re

class mrp_production(osv.osv):

    _inherit = 'mrp.production'

    def get_partner_ec(self, cr, uid, ids,cliente, field_value, context=None ):
        res = {}
        records=self.browse(cr, uid, ids)
        for record in records:
            origen=record.origin
            res[record.id]=False

            if origen:
                id_origen=self.pool.get('sale.order').search(cr, uid, [('name','=',origen)], context=context)
                partner_from_origin=self.pool.get('sale.order').browse(cr,uid,id_origen,context=context)
                if partner_from_origin:
                    for partner in partner_from_origin:
                        res[record.id]=partner.partner_id.id
                else:
                    res[record.id]=False

        return res


    _columns={
        'cliente':fields.function(get_partner_ec, method=True, type="many2one", relation="res.partner", string="Partner")
    }