from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp import tools
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging
import xml.etree.cElementTree as ET



class hr_contract(osv.osv):
    _inherit = "hr.contract"


    def onchange_employee_id(self, cr, uid, ids, employee_id, context=None):
        res={}
        if employee_id:
            employee_obj=self.pool.get('hr.employee').browse(cr, uid, employee_id, context)

            nombre=employee_obj.name
            job=employee_obj.job_id
            res['value']={'name': nombre,'job_id':job.id}

            return res
        else:
            return res