import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree
import openerp.addons.decimal_precision as dp
import openerp.exceptions

from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp import tools
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging
import xml.etree.cElementTree as ET


class historial_notificaciones(osv.osv):
    _name= "historial.notificaciones"

    _columns ={
        "fecha":fields.date("Fecha"),
        "tipo":fields.char("Tipo"),
        "notas":fields.char("Notas"),
        "empleado":fields.many2one("hr.employee", "Empleado"),
        }

class hr_employee(osv.osv):
    _inherit = "hr.employee"

    _columns={
        "historial_notificaciones":fields.one2many('historial.notificaciones', 'empleado', string="Datos"),
        }

