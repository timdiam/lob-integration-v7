# -*- coding: utf-8 -*-
#################################################################################
#
# Add a "Department" field to purchase.requisition
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    Jan 28th 2015
#
#################################################################################
from openerp.osv     import fields, osv, orm
from tools.translate import _

#####################################################################################
# Inherited purchase_requisition class definition
#####################################################################################
class purchase_requisition(osv.osv):

    _inherit = 'purchase.requisition'

    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = { 'department'  : fields.many2one( 'hr.department', 'Departamento' )
               }

    _order = 'id desc'