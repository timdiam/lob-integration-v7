# -*- coding: utf-8 -*-
#################################################################################
#
# A purchase order should only be made through a solicitud, so ensure that
# the create method of purchase_order checks for a solicitud
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    Jan 19th 2015
#
#################################################################################
from openerp.osv     import fields, osv, orm
from tools.translate import _


#####################################################################################
# Inherited purchase_order class definition
#####################################################################################
class purchase_order(osv.osv):

    _inherit = 'purchase.order'


    #####################################################################################
    # Throw error if this was not made with a solicitud ( requisition_id )
    #####################################################################################
    def create( self, cr, uid, vals, context=None ):

        if( 'requisition_id' not in vals or vals[ 'requisition_id' ] == False ):
            raise osv.except_osv( _( 'Invalid Action!' ),
                                  _( "You can't create a purchase order without first creating a quotation." )
                                )

        return super( purchase_order, self ).create( cr, uid, vals, context=context )


    #####################################################################################
    # Override the wkf_approve_order method so that it fills 'approved_by' with the current
    # user ID
    #####################################################################################
    def wkf_approve_order(self, cr, uid, ids, context=None):

        self.write( cr, uid, ids, { 'approved_by' : uid } )

        return super( purchase_order, self ).wkf_approve_order( cr, uid, ids, context=context )


    #####################################################################################
    # Column definitions
    #####################################################################################
    _columns = { 'solicited_by'  : fields.related( 'requisition_id', 'user_id', type="many2one", relation='res.users', string="Solicited by", store=False),
                 'approved_by'   : fields.many2one( 'res.users', 'Approved by' ),
                 'forma_de_pago' : fields.char( 'Forma de pago' ),


               }

    _order = 'id desc'