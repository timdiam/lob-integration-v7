# -*- coding: utf-8 -*-
import time
from report import report_sxw
from osv import fields,osv
import pooler
import logging
from operator import itemgetter
_logger = logging.getLogger(__name__)

class Parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_facturas':self.get_facturas,
            'get_products_to_elaborar':self.get_products_to_elaborar,
            'get_prod_moves_consumed':self.get_prod_moves_consumed,
            'get_cost_all_resources':self.get_cost_all_resources,
            'get_prod_moves':self.get_prod_moves,
            'get_work_orders':self.get_work_orders,
            'get_foto':self.get_foto,
            'get_inspector':self.get_inspector,
            'get_encargado':self.get_encargado,
            'get_total':self.get_total,
            'get_total_to_consume':self.get_total_to_consume,
            'get_total_consumed':self.get_total_consumed,
            'get_totcr':self.get_totcr,
            'get_tothr':self.get_tothr,
            'get_totsuma':self.get_totsuma,
            'get_create_date':self.get_create_date,
            'get_order_date':self.get_order_date,
            'cr':cr,
            'uid':uid,
            'g_context':context,
        })

    def get_create_date(self, order):
        cr = self.localcontext['cr']
        pool = pooler.get_pool(cr.dbname)
        uid = self.localcontext['uid']

        prod_order_list = pool.get('mrp.production').search(cr,uid,[('origin','=', order.name)], order="id")
        prod_objs = pool.get('mrp.production').browse(cr,uid,prod_order_list)
        return prod_objs[len(prod_objs)-1].create_date.split(" ")[0] if prod_objs[len(prod_objs)-1].create_date else ""

    def get_order_date(self, order):
        cr = self.localcontext['cr']
        pool = pooler.get_pool(cr.dbname)
        uid = self.localcontext['uid']

        prod_order_list = pool.get('mrp.production').search(cr,uid,[('origin','=', order.name)], order="id")
        prod_objs = pool.get('mrp.production').browse(cr,uid,prod_order_list)
        return prod_objs[len(prod_objs)-1].date_planned.split(" ")[0] if prod_objs[len(prod_objs)-1].date_planned else ""

    def get_facturas(self, sale_order_name):
        cr = self.localcontext['cr']
        pool = pooler.get_pool(cr.dbname)
        uid = self.localcontext['uid']

        invoice_list = pool.get('account.invoice').search(cr,uid,[('origin','=',sale_order_name)])
        inv_objs = pool.get('account.invoice').browse(cr,uid,invoice_list)
        return inv_objs

    def get_products_to_elaborar(self, sale_order_name):
        cr = self.localcontext['cr']
        pool = pooler.get_pool(cr.dbname)
        uid = self.localcontext['uid']

        prod_order_list = pool.get('mrp.production').search(cr,uid,[('origin','=',sale_order_name)])
        prod_objs = pool.get('mrp.production').browse(cr,uid,prod_order_list)
        products_list = []
        for p in prod_objs:
            for prod in p.product_ids:
                products_list.append({'production':p, 'product':prod.product_id, 'obs':p.observation, 'product_qty':prod.product_qty, 'description':p.description})

        return products_list

    def get_prod_moves(self, sale_order_name):
        cr = self.localcontext['cr']
        pool = pooler.get_pool(cr.dbname)
        uid = self.localcontext['uid']

        prod_order_list = pool.get('mrp.production').search(cr,uid,[('origin','=',sale_order_name)])
        prod_objs = pool.get('mrp.production').browse(cr,uid,prod_order_list)
        move_list = []
        for p in prod_objs:
            for m in p.move_lines:
                move_list.append(m)

        return move_list

    def get_prod_moves_consumed(self, sale_order_name):
        cr = self.localcontext['cr']
        pool = pooler.get_pool(cr.dbname)
        uid = self.localcontext['uid']

        prod_order_list = pool.get('mrp.production').search(cr,uid,[('origin','=',sale_order_name)])
        prod_objs = pool.get('mrp.production').browse(cr,uid,prod_order_list)
        move_list = []
        for p in prod_objs:
            for m in p.move_lines2:
                move_list.append(m)

        return move_list

    def get_work_orders(self, sale_order_name):
        cr = self.localcontext['cr']
        pool = pooler.get_pool(cr.dbname)
        uid = self.localcontext['uid']

        prod_order_list = pool.get('mrp.production').search(cr,uid,[('origin','=',sale_order_name)])
        prod_objs = pool.get('mrp.production').browse(cr,uid,prod_order_list)
        wo_list = []
        for p in prod_objs:
            for wo in p.workcenter_lines:
                wo_list.append(wo)

        return wo_list

    def get_foto(self, sale_order_name):
        cr = self.localcontext['cr']
        pool = pooler.get_pool(cr.dbname)
        uid = self.localcontext['uid']

        prod_order_list = pool.get('mrp.production').search(cr,uid,[('origin','=',sale_order_name)], order="id")
        prod_objs = pool.get('mrp.production').browse(cr,uid,prod_order_list)
        if len(prod_objs) == 0:
            raise osv.except_osv( ( 'ERROR' ), ( 'Esta venta no tiene ningun orden de produccion!' ) )

        return prod_objs[len(prod_objs)-1].fotomontaje

    def get_inspector(self, sale_order_name):
        cr = self.localcontext['cr']
        pool = pooler.get_pool(cr.dbname)
        uid = self.localcontext['uid']

        prod_order_list = pool.get('mrp.production').search(cr,uid,[('origin','=',sale_order_name)], order="id")
        prod_objs = pool.get('mrp.production').browse(cr,uid,prod_order_list)
        return prod_objs[len(prod_objs)-1].inspector


    def get_encargado(self, sale_order_name):
        cr = self.localcontext['cr']
        pool = pooler.get_pool(cr.dbname)
        uid = self.localcontext['uid']

        prod_order_list = pool.get('mrp.production').search(cr,uid,[('origin','=',sale_order_name)], order="id")
        prod_objs = pool.get('mrp.production').browse(cr,uid,prod_order_list)
        return prod_objs[len(prod_objs)-1].encargado

    def get_total(self,cant,precio):
        enviafe=cant*precio
        return enviafe

    def get_total_to_consume(self, objects, ref):
        moves = self.get_prod_moves(ref)
        sum=0
        for m in moves:
            sum += m.product_id.standard_price * m.product_qty
        return sum

    def get_total_consumed(self, objects, ref):
        moves = self.get_prod_moves_consumed(ref)
        sum=0
        for m in moves:
            sum += m.price_unit * m.product_qty
        return sum

    def get_totcr(self, objects, ref):
        work_orders = self.get_work_orders(ref)
        sum=0
        for wo in work_orders:
            sum += (self.get_cost_all_resources(wo.resources) * wo.delay)
        return sum

    def get_tothr(self, objects, ref):
        work_orders = self.get_work_orders(ref)
        sum=0
        for wo in work_orders:
            sum += wo.delay
        return sum

    def get_cost_all_resources(self, lrecursos):
        sum=0
        for l in lrecursos:
            sum += l.resource.cost_displayed
        return sum

    def get_totsuma(self,objects,ref):

        total_to_consume = self.get_total_to_consume(objects, ref)
        total_consumed   = self.get_total_consumed(objects, ref)
        trab             = self.get_totcr(objects,ref)

        valor = total_to_consume + total_consumed + trab

        return valor