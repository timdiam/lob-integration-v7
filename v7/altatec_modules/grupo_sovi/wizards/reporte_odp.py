# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2011 Enapps LTD (<http://www.enapps.co.uk>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv

class grupo_sovi_wizard_reporte_odp(osv.osv_memory):

    _name = 'grupo_sovi.wizard_reporte_opd'

    def generar(self, cr, uid, ids, context=None):
        records=self.browse(cr,uid,ids)[0]

        ids_for_report=[records.orden.id]

        if(records.include_costs):
            result = {'type' :'ir.actions.report.xml',
                      'context':context,
                      'report_name':'orden_produccion_conc',
                      'datas':
                                {
                                 'ids':ids_for_report}
                                }

        else:
            result = {'type' :'ir.actions.report.xml',
                      'context':context,
                      'report_name':'orden_produccion_sin_costs',
                      'datas':
                                {
                                 'ids':ids_for_report}
                                }

        return result


    _columns = {
        'orden'     : fields.many2one('sale.order', 'Orden'),
        'include_costs' : fields.boolean(string= 'Incluir Costos?'),
    }

    _defaults = {
        'include_costs':True,
    }