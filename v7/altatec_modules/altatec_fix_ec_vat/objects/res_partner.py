# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Andres Calle, Patricio Rangles, Pablo Vizhnay
# Copyright (C) 2013  TRESCLOUD Cia Ltda
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from openerp.osv import osv
from openerp.osv import fields
from openerp.tools.translate import _
from openerp.tools.misc import ustr
import time
import re #para busqueda por cedula
import unicodedata
from openerp import pooler



class res_partner(osv.osv):
    _inherit = "res.partner"

    _columns = {
                'tipo_vat': fields.selection((
                                                ('RUC','RUC'),
                                                ('EC','CED'),
                                                ('PAS','PASAPORTE'),
                                                ('OT','OTROS'),
                                                                ),
                                                string='Tipo de identificación'
                ),

                'identificacion': fields.char('No. Identificación', size=13, help='Identificacion del cliente'),


    }

    def _check_ced_ruc(self, cr, uid, ids):
        partners = self.browse(cr, uid, ids)
        for partner in partners:

            if (partner.tipo_vat == 'PAS' or partner.tipo_vat == 'OT'  ):
                return True
            if partner.property_account_position.id in [6,7,8]:
                return self.check_ruc(partner.identificacion)
            else:
                return self.check_cedula(partner.identificacion,partner.tipo_vat)

    _constraints = [
        (_check_ced_ruc, 'Error en su Cedula/RUC/Pasaporte', ['identificacion'])
        ]



    def create(self, cr, uid, values, context=None):
        tipo_identificacion =   values['tipo_vat']
        identificacion      =   values['identificacion']
        legacy_vat=''
        if(tipo_identificacion == 'EC' or tipo_identificacion =='RUC' ):
            #
            # if(tipo_identificacion == 'EC'):
            #     if(self.check_cedula(identificacion)):
                    legacy_vat='EC'+identificacion
            #     else:
            #         raise osv.except_osv(_('Error de Validacion'), _('El valor: '+identificacion+' no es una identificacion valida'))
            #
            # else:
            #     if(self.check_ruc(identificacion)):
            #             legacy_vat='EC'+identificacion
            #     else:
            #         raise osv.except_osv(_('Error de Validacion'), _('El valor: '+identificacion+' no es una identificacion valida'))


        if(tipo_identificacion == 'PAS' or tipo_identificacion =='OT' ):
            legacy_vat=identificacion
        if(tipo_identificacion == 'CF'  ):
            legacy_vat='EC'+'9999999999999'
        values.update({'vat':legacy_vat})
        return super(res_partner,self).create(cr, uid,  values, context)


    def write(self, cr, uid, ids, values, context=None):

        cliente=self.browse(cr, uid, ids, context=context)[0]
        legacy_vat=''
        tipo_identificacion =   values['tipo_vat'] if 'tipo_vat' in values else None
        identificacion      =   values['identificacion'] if 'identificacion' in values else None
        if(tipo_identificacion !=None or identificacion !=None ):
            if(tipo_identificacion == None):
                tipo_identificacion= cliente.tipo_vat
            if(identificacion == None):
                identificacion = cliente.identificacion

        if(tipo_identificacion == 'EC' or tipo_identificacion =='RUC' ):

            # if(tipo_identificacion == 'EC'):
                # if(self.check_cedula(identificacion)):
                        legacy_vat='EC'+identificacion
            #     else:
            #         raise osv.except_osv(_('Error de Validacion'), _('El valor: '+identificacion+' no es una identificacion valida'))
            #
            # else:
            #     if(self.check_ruc(identificacion)):
            #             legacy_vat='EC'+identificacion
            #     else:
            #         raise osv.except_osv(_('Error de Validacion'), _('El valor: '+identificacion+' no es una identificacion valida'))



        elif(tipo_identificacion == 'PAS' or tipo_identificacion =='OT' ):
                legacy_vat=identificacion

        elif(tipo_identificacion == 'CF'  ):
                legacy_vat='EC'+'9999999999999'
        values.update({'vat':legacy_vat})

        return super(res_partner,self).write(cr, uid, ids, values, context)

    def check_cedula(self, identificacion,tipo_vat):

        if len(identificacion) == 13 and not identificacion[10:13] == '001':
            return False

        else:

            if len(identificacion) < 10:
                return False

            coef = [2,1,2,1,2,1,2,1,2]
            cedula = identificacion[:9]
            suma = 0
            for c in cedula:
                 val = int(c) * coef.pop()
                 suma += val > 9 and val-9 or val
            result = 10 - ((suma % 10)!=0 and suma%10 or 10)

            if(tipo_vat=='EC'):
                if ( (result == int(identificacion[9]))  and len(identificacion) ==10 ):
                    return True
                else:
                    return False
            else:
                if(tipo_vat=='RUC'):
                    if ( (result == int(identificacion[9]))  and len(identificacion) ==13 ):
                     return True
                    else:
                     return False


    def check_ruc(self, ruc):

        if not len(ruc) == 13:
            return False
        if ruc[2:3] == '9':
            coef = [4,3,2,7,6,5,4,3,2,0]
            coef.reverse()
            verificador = int(ruc[9:10])
        elif ruc[2:3] == '6':
            coef = [3,2,7,6,5,4,3,2,0,0]
            coef.reverse()
            verificador = int(ruc[8:9])
        else:
            raise osv.except_osv('Error', 'Cambie el tipo de persona')
        suma = 0
        for c in ruc[:10]:
            suma += int(c) * coef.pop()
        result = 11 - (suma>0 and suma % 11 or 11)
        if result == verificador:
            return True
        else:
            return False







