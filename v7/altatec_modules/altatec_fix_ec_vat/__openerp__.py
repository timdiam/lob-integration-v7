# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Andres Calle
# Copyright (C) 2013  TRESCLOUD CÍA LTDA
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

{
    "name" : "Altatec Mejoras res.partner  ",
    "version" : "1.0",
    'complexity': "medium",
    "author" : "Juan Romero",
    "website" : "http://www.altatec.ec/",
    "category" : "Regulaciones Ecuatorianas",
    "depends" : [
                 'base',
                 'sale',
                 'base_vat',
                 'account',
                 'crm',
                 'mail',
                 'l10n_ec_niif_minimal',
                 'purchase',
                 'ecua_partner',
                 ],
    "description": """
    Este modulo consta de las siguientes mejoras
    - Separacion de los campos vat y tipo_vat

    """,

    "data": [
                   'views/res_partner_view.xml',
                   ],
    "installable": True,
    "auto_install": False,
    "application": False,
}
