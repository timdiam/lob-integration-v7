# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: TRESCloud Cia Ltda, Andres Calle
# Copyright (C) 2013  wwww.trescloud.com
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from openerp.osv import fields, osv
import re
from openerp.tools.translate import _
import time

class stock_partial_picking(osv.osv_memory):
    _inherit = "stock.partial.picking"

    def do_partial(self, cr, uid, ids, context=None):
        '''
        Agrega posibilidad de lanzar wizards para completar la informacion de la guia
        y permite imprimir la guia de forma automatica
        '''
        if context is None:
            context = {}
        
        waybill_obj = self.pool.get('stock.picking')
        for waybill in waybill_obj.browse(cr, uid, context.get('active_ids',[]), context):
            invoice_obj = self.pool.get('account.invoice')
            if not invoice_obj.verify_partner_data(cr, uid, waybill.partner_id, waybill.document_invoice_type_id, context):
                return invoice_obj.launch_partner_view(cr, uid, waybill, waybill.partner_id, waybill.document_invoice_type_id, context)

        #TODO: Implementar la impresion automatica
        #if waybill.document_invoice_type_id.report_id:
        #    return self.invoice_print(cr, uid, [invoice.id], context)
        
        return super(stock_partial_picking, self).do_partial(cr, uid, ids, context)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
