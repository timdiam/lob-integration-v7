# -*- encoding: utf-8 -*-
########################################################################
#                                                                       
# @authors: Carlos Yumbillo                                                                           
# Copyright (C) 2013  TRESCLOUD CIA. LTDA.                                 
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import osv, fields
from openerp.tools.translate import _
import re
import decimal_precision as dp
import time
from openerp import netsvc

class documet_invoice_type(osv.osv):
    """ Type document """
    _name = 'account.invoice.document.type'
    _order = 'priority'
    _inherit = ['mail.thread']
    
    _columns = {
         'report_id':fields.many2one('ir.actions.report.xml','Name report', change_default=1, domain="[('model','=','account.invoice')]", help='Report format to use when printing.', ),
         'code':fields.char('Code', size=4, required=True,track_visibility='onchange', help='Used to generate the ats',),
         'name':fields.char('Name', size=255, required=True,track_visibility='onchange'),
         'use':fields.boolean('Activo', track_visibility='onchange',help='Indicates whether the document is to be active.',),
         'number_format_validation':fields.boolean('Number format ', track_visibility='onchange',help='Valid to the document number is of the form # # # - # # # - # # # # # # # # # # (example 001-001-0001234567).',),
         'sri_authorization_validation':fields.boolean('SRI authorization third',track_visibility='onchange', help='Requires an authorization from SRI, this authorization is provided by the customer or supplier.',),          
         'sri_authorization_validation_owner':fields.boolean('SRI authorization owner ', track_visibility='onchange',help='Requires an authorization from SRI, this authorization is provided by our company.',),
         'priority':fields.integer('Priority', required=True, track_visibility='onchange',help='Indicates the priority of the document.',),
         'type': fields.selection([
            ('out_invoice','Customer Document'),
            ('in_invoice','Supplier Document'),
            ('out_refund','Customer Refund'),
            ('in_refund','Supplier Refund'),
            ('hr_advance','Employee Advance'),
            ('out_waybill','Issued Waybill'),
            ],'Type', select=True, track_visibility='onchange',change_default=True, required=True, help='Indicates whether the document is of supplier or customer.',),         
         'parent_id':fields.many2one('account.invoice.document.type','Parent document type', help='This associate the document type with the parent.'),
         'vat_number': fields.boolean('Vat Number', help="If this option is selected then the system will force the registration of this data when creating sales or purchases with this document type"),
         'address': fields.boolean('Address', help="If this option is selected then the system will force the registration of this data when creating sales or purchases with this document type"),
         'phone':fields.boolean('Phone', help="If this option is selected then the system will force the registration of this data when creating sales or purchases with this document type"),
         'mail':fields.boolean('Email',help="If this option is selected then the system will force the registration of this data when creating sales or purchases with this document type"),
     
     } 
    
    def verify_partner_data(self, cr, uid, ids, partner_id, context=None):
        '''
        Verifica que la empresa benefeciaria del documento cuente con todos los datos necesarios
        Se utiliza por ejemplo para lanzar formularios de llenado de documentos
        '''
        if context is None:
            context = {}
        for document_type in self.browse(cr, uid, ids, context):
            partner_obj = self.pool.get('res.partner')
            #TODO: Asegurar que los modulos contables sean compatibles con el concepto de empresa padre/hijo 
            if not partner_obj._get_company_vat(cr, uid, partner_id, context):
                return False
            #if not partner_obj.get_company_address(cr, uid, invoice.partner_id.id, None, None, context):
            #    raise osv.except_osv(_('Invalid action!'), _('El cliente/proveedor no cuenta con datos completos, verifique la direccion'))
            #if not partner_obj.get_company_phone(cr, uid, invoice.partner_id.id, None, None, context):
            #    raise osv.except_osv(_('Invalid action!'), _('El cliente/proveedor no cuenta con datos completos, verifique el telefono'))
        return True
    
    _defaults = {
        'use': False,
        'number_format_validation': False,
        'sri_authorization_validation': False,       
     }
    
    def name_get(self,cr,uid,ids, context=None):

        if context is None:
            context = {}
        if not ids:
            return []
        reads = self.read(cr, uid, ids, ['name','code'], context=context)
        res = []
        for record in reads:
            name = record['name']
            if record['code']:
                name = '['+record['code']+'] '+name
            res.append((record['id'], name))
        return res
    
    def name_search(self, cr, user, name='', args=None, operator='ilike', context=None, limit=100):
        '''
        Permite buscar ya sea por nombre o por codigo
        hemos copiado la idea de product.product
        '''
        if not args:
            args = []
        if not context:
            context = {}
        ids = []
        if name: #no ejecutamos si el usaurio no ha tipeado nada

            #buscamos por codigo completo
            ids = self.search(cr, user, [('code','=',name)]+ args, limit=limit, context=context)
            if not ids: #buscamos por fraccion de palabra o fraccion de codigo
                # Do not merge the 2 next lines into one single search, SQL search performance would be abysmal
                # on a database with thousands of matching products, due to the huge merge+unique needed for the
                # OR operator (and given the fact that the 'name' lookup results come from the ir.translation table
                # Performing a quick memory merge of ids in Python will give much better performance
                ids = set()
                ids.update(self.search(cr, user, args + [('code',operator,name)], limit=limit, context=context))
                if not limit or len(ids) < limit:
                    # we may underrun the limit because of dupes in the results, that's fine
                    ids.update(self.search(cr, user, args + [('name',operator,name)], limit=(limit and (limit-len(ids)) or False) , context=context))
                ids = list(ids)
            if not ids:
                ptrn = re.compile('(\[(.*?)\])')
                res = ptrn.search(name)
                if res:
                    ids = self.search(cr, user, [('code','=', res.group(2))] + args, limit=limit, context=context)

        else: #cuando el usuario no ha escrito nada aun
            ids = self.search(cr, user, args, limit=limit, context=context)
        result = self.name_get(cr, user, ids, context=context)
        return result
        
documet_invoice_type()

class account_invoice(osv.osv):
    
    _inherit = "account.invoice"
    
    def _amount_all_2(self, cr, uid, ids, name, args, context=None):
        res = {}
        for invoice in self.browse(cr, uid, ids, context=context):
            res[invoice.id] = {
                'amount_untaxed': 0.0,
                'amount_tax': 0.0,
                'amount_total': 0.0,
                'total_undiscounted':0.0,
                'total_discount': 0.00,
            }
            for line in invoice.invoice_line:
                res[invoice.id]['amount_untaxed'] += line.price_unit * line.quantity
                res[invoice.id]['total_undiscounted'] += line.price_unit * line.quantity
                #res[invoice.id]['total_discount'] += line.price_unit * line.quantity * line.discount * 0.01
                res[invoice.id]['total_discount'] += line.quantity * (line.price_unit - (line.price_subtotal/line.quantity) )
            for line in invoice.tax_line:
                res[invoice.id]['amount_tax'] += line.amount               
            
            res[invoice.id]['amount_total'] = res[invoice.id]['amount_tax'] + res[invoice.id]['amount_untaxed']
                       
        return res
    
    def _get_invoice_tax(self, cr, uid, ids, context=None):
        result = {}
        for tax in self.pool.get('account.invoice.tax').browse(cr, uid, ids, context=context):
            result[tax.invoice_id.id] = True
        return result.keys()
    
    def _get_invoice_line(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('account.invoice.line').browse(cr, uid, ids, context=context):
            result[line.invoice_id.id] = True
        return result.keys()
     
    def _without_withhold(self,cr,uid,ids,field_name,arg,context=None):
        """
        Check if the invoice must have a withhold, return True or False
        """
        value = {}

        cr.execute('SELECT inv.id ' \
                'FROM account_invoice AS inv ' \
                'WHERE inv.total_to_withhold != 0 and inv.state NOT IN (\'draft\',\'cancel\') and inv.withhold_id is Null and inv.type in (\'in_invoice\')')
        ids_without_withhold = cr.fetchall()
        
        res = {}
        for id in ids:
            if (id,) in ids_without_withhold:
                res[id] = True
            else:
                res[id] = False
        return res

    def _without_withhold_search(self, cursor, user, obj, name, args, context=None):
        '''
        Permite filtrar las facturas sin retencion (metodo search necesario para campos store false)
        '''
        if not len(args):
            return []

        cursor.execute('SELECT inv.id ' \
                'FROM account_invoice AS inv ' \
                'WHERE inv.total_to_withhold != 0 and inv.state NOT IN (\'draft\',\'cancel\') and inv.withhold_id is Null and inv.type in (\'in_invoice\')')
        res = cursor.fetchall()
        
        if not res:
            return [('id', '=', 0)]
        return [('id', 'in', [x[0] for x in res])]

    def _get_warning_msgs(self, cr, uid, ids, field, arg, context=None):
        '''
        Retorna una explicación de porque la linea de factura está en color rojo en la vista tree
        (cuando el campo warning_msgs no esta vacio la linea de factura se pinta de color rojo) 
        '''        
        res = {}
        for invoice in self.browse(cr, uid, ids, context):
            res[invoice.id]= self._warning_msgs(cr, uid, invoice.id, context)
        return res

    def _warning_msgs(self, cr, uid, ids, context=None):
        '''
        Metodo helper: Ayuda a redefinir con facilidad el campo tipo funcion asociado al metodo _get_warning_msgs
        Retorna una explicacion de porque la linea de factura esta en color rojo en la vista tree
        '''
        warning_msgs = ''
        inv = self.browse(cr, uid, ids, context)
        if inv.without_withhold:
            warning_msgs += 'Acorde a los impuestos que usted ha seleccionado esta pendiente emitir la retencion, de no ser asi corriga los impuestos'
        return warning_msgs

    def _show_modified_document(self, cr, uid, ids, field, arg, context=None):
        '''
        Retorna True cuando se requiere una factura modificada, por ejemplo para notas de debito o credito.
        Se utiliza en la vista para hacer aparecer y desaparecer el campo de documento modificado.
        '''
        res = {}
        for invoice in self.browse(cr, uid, ids, context):
            result = False
            if invoice.type in ['out_invoice','out_refund'] and invoice.document_invoice_type_id.code in ['05','06']: #notas de debito y credito en ventas
                result = True
            res[invoice.id]= result
        return res


    _columns = {
        'document_invoice_type_id': fields.many2one('account.invoice.document.type', 'Document type', help='Indicates the type of accounting document authorized to issue when making a purchase or sale.',),
        'warning_msgs' : fields.function(_get_warning_msgs, string='Warnings',store=False,type='char',method=True, help='There are pending action in this transaction.'),
        'show_modified_document' : fields.function(_show_modified_document, 
                                                   string='Show Modified Document',
                                                   store=False,
                                                   type='boolean',
                                                   method=True, 
                                                   help='Indicates if you need to specify the modified document'),
        'total_undiscounted': fields.function(_amount_all_2, method=True, digits_compute=dp.get_precision('Account'), string='Total undiscount',
            store={
                'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line'], 20),
                'account.invoice.tax': (_get_invoice_tax, None, 20),
                'account.invoice.line': (_get_invoice_line, ['price_unit','invoice_line_tax_id','quantity','discount','invoice_id'], 20),
            },
            multi='all1'), 
        'total_discount': fields.function(_amount_all_2, method=True, digits_compute=dp.get_precision('Account'), string='Total discount',
            store={
                'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line'], 20),
                'account.invoice.tax': (_get_invoice_tax, None, 20),
                'account.invoice.line': (_get_invoice_line, ['price_unit','invoice_line_tax_id','quantity','discount','invoice_id'], 20),
            },
            multi='all1'),
        'without_withhold':fields.function(_without_withhold,type='boolean',fnct_search=_without_withhold_search,method=True,help="Show if a purchase invoice must have a withhold"),
    }
    def check_number(self, cr, uid, ids, context=None):
        '''
        Verifica si existe otra factura con el mismo numero
        Se exceptuan las facturas en estado borrador
        en estado borrador puede haber varias facturas repetidas
        ejemplo facturas sin asignar numero: 001-001- y otra 001-001-)
        '''
        lines = self.browse(cr, uid, ids, context=context)[0]
        search = [('internal_number','!=', False),
                    ('internal_number','=',lines.internal_number),
                  ('company_id','=',lines.company_id.id),
                  ('type','=',lines.type),
                  ('document_invoice_type_id','=',lines.document_invoice_type_id.id),
                  ('state','not in',['draft']),
                  ('id','not in',ids)
                  ]
      
        if lines.type == 'in_invoice':
            search.append(('partner_id','=',lines.partner_id.id))
        
        n=self.search(cr,uid,search)
        if len(n)>=1:
                return False
        return True

# Se eliman los constrains por que el control se hace en el cambio de flujo
#    _constraints = [
#        (_check_number, _('Invoice Number must be unique per Company!'), ['number']),
#        ]
#    _sql_constraints = [
#        ('number_uniq', 'unique(number,company_id, journal_id, type,document_invoice_type_id,partner_id)', _('Invoice Number must be unique per Company!')),
#    ]
    
    def _doc_type(self, cr, uid, context=None):
        '''
        El tipo de documento por defecto es el de menor prioridad
        de entre los del mismo tipo (campo type)
        '''

        doc_type_id = None
        if context and 'type' in context:
            invoice_type = context['type']
            cr.execute('''select min(ai.priority) as priority, ai.id as id
                      from account_invoice_document_type ai
                      where ai.type='%s'
                      group by id
                      order by priority''' %(invoice_type))
            res = cr.dictfetchone()
            
            if res:
               doc_type_id = res['id']
        
        return doc_type_id      
   
    def print_document_type(self, cr, uid, ids, context=None):
        invoice_obj = self.pool.get('account.invoice')
        invoice_list = invoice_obj.search(cr, uid, [('id','=',ids[0])])[0]
        
        invoice = invoice_obj.browse(cr,uid,invoice_list)
        invoice_type = invoice.type
        
        if invoice.document_invoice_type_id: 
            service_name = invoice.document_invoice_type_id.report_id.report_name
        else:
            cr.execute('''select min(ai.priority) as priority, ai.report_id as report_id, r.report_name as report_name 
                        from account_invoice_document_type ai, ir_act_report_xml r
                        where ai.report_id=r.id and ai.type='%s'
                        group by report_id, report_name
                        order by priority''' %(invoice_type))
            res = cr.dictfetchone()
            service_name = res['report_name']
        
        self.write(cr, uid, ids, {'sent': True}, context=context)            
        if service_name:
            return {
                 'type': 'ir.actions.report.xml',
                 'report_name': service_name,    # the 'Service Name' from the report
                 'datas' : {
                         'model' : 'account.invoice',    # Report Model
                         'ids' : ids
                           }        
                  }
        else:
            raise osv.except_osv(_('Warning!'), _("You do not have established a document format for printing."))

    def invoice_print(self, cr, uid, ids, context=None):
        '''
        Reemplazamos la funcion por defecto por la nueva funcion por compatiblidad con otros modulos
        '''
        return self.print_document_type(cr, uid, ids, context)


    def launch_partner_view(self, cr, uid, document, partner_id, document_invoice_type_id, context=None):
        '''
        Lanza el formulario de partner minificado cuando faltan datos en el documento tributario
        Este metodo funciona para varios objetos de forma independiente: facturas, retenciones, guias de remision
        '''
        ctx= dict(context)
        ctx.update({'type':'','default_type':'',})

        view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ecua_invoice_type', 'partner_accounting_simplified_form')[1]
        
        #preparamos la vista a retornar
        #configuramos la obligatoriedad de los campos
        if document_invoice_type_id.vat_number:
            ctx.update({'force_vat':True,})
        if document_invoice_type_id.address:
            ctx.update({'force_street':True,})
        if document_invoice_type_id.phone:
            ctx.update({'force_phone':True,})
        if document_invoice_type_id.mail:
            ctx.update({'force_email':True,})
            
        #revisamos la obligatoriedad de email si es de documento electronico
        module_ids = self.pool.get('ir.module.module').search(cr, uid, [('name','like','ecua_electronic_documents')])
        module = self.pool.get('ir.module.module').browse(cr, uid, module_ids, context)
        for mod in module:
            if mod['state']=='installed':
                if str(document._name) == 'sale.order':
                    if self._default_allow_electronic_document( cr, uid, context):
                        ctx.update({'force_email':True,})
                elif document.allow_electronic_document==True:
                    ctx.update({'force_email':True,})
                    
        commercial_partner_id = partner_id.commercial_partner_id.id
        partner_view = {
            'name': _('Partner Basic Accounting Info'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id or False,
            'res_model': 'res.partner',
            'res_id': commercial_partner_id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'context':ctx, #removidos para no tener problemas con el campo type de res.parner
        }
        return partner_view
            
    def invoice_open_enhanced(self, cr, uid, ids, context=None):
        '''
        Reemplaza al boton invoice_open pues estaba asociado al workflow directamente
        Agrega posibilidad de lanzar wizards para completar la informacion de la factura
        y permite imprimir la factura de forma automatica
        '''
        if context is None:
            context = {}
        for invoice in self.browse(cr, uid, ids, context):
            
            #lanzar formulario de partner minificado
            if not self.verify_partner_data(cr, uid, invoice.partner_id, invoice.document_invoice_type_id, context):
                return self.launch_partner_view(cr, uid, invoice, invoice.partner_id, invoice.document_invoice_type_id, context)
            
            #enviamos la senial invoice_open del boton reemplazado al flujo
            wf_service = netsvc.LocalService('workflow')
            wf_service.trg_validate(uid,'account.invoice',invoice.id,'invoice_open',cr)
            
            #imprimir el reporte de la factura
            if invoice.document_invoice_type_id.report_id:
                return self.invoice_print(cr, uid, [invoice.id], context)
        
        return
    #TODO ALTATEC This function is getting called twice on validating invoices...
    #seems to be working fine (its all checks for exceptions), but we should definitely revisit
    #to see if it is necessary.. maybe needs refactoring.
    def invoice_validate(self, cr, uid, ids, context=None):
        '''
        Valida la factura previo aprobacion:
        1. Que sea de contabilidad o de antcipos a empleados
        2. Validacion de duplicidad de factura
        3. Validacion del formato de numero de factura
        4. Validacion de campos minimos de la empresa asociada
        '''
        if context is None:
            context = {}
        res = True
        for invoice in self.browse(cr, uid, ids, context):
            #TODO: Dividir entre las funciones existentes de fecha, numeracion y validacion de factura
            if not self.verify_partner_data(cr, uid, invoice.partner_id, invoice.document_invoice_type_id, context):
                raise osv.except_osv(_('Invalid action!'), _('El cliente proveedor no cuenta con datos completos, verifique RUC/Cedula, Direccion, Telefono!'))
            if not invoice.internal_number and not invoice.advances:
                raise osv.except_osv(_('Invalid action!'), _('El numero de factura esta vacio!'))
            a = self.check_number(cr, uid, ids, context=context)
            if not self.check_number(cr, uid, ids, context=context):
                invoice.number
                ref = invoice.internal_number
                raise osv.except_osv(_('Invalid action!'), _('El numero de factura %s debe ser unico!') % (ref) ) 
            if invoice.document_invoice_type_id.number_format_validation:
                cadena='(\d{3})+\-(\d{3})+\-(\d{9})'
                validate_document_type = invoice.document_invoice_type_id.number_format_validation
                if validate_document_type==True:
                    ref = invoice.internal_number
                    if not re.match(cadena, ref):
                        raise osv.except_osv(_('Invalid action!'), _('El numero de factura %s esta incorrecto' ) % (ref) )

            #evitamos ventas a consumidor final de montos no permitidos
            if invoice.document_invoice_type_id.number_format_validation: #cualquier documento tribuatrio
                if invoice.partner_id.vat in ['EC9999999999999','9999999999999']:
                    if invoice.period_id.fiscalyear_id.name == '2014' and invoice.amount_untaxed >= 200.00:
                        raise osv.except_osv(_('Monto Excedido!'), _('Para el 2014 las ventas a consumidor no pueden exceder USD 200.00' ) )
                    elif invoice.period_id.fiscalyear_id.name == '2015' and invoice.amount_untaxed >= 200.00:
                        raise osv.except_osv(_('Monto Excedido!'), _('Para el 2015 las ventas a consumidor no pueden exceder USD 200.00' ) )

            #TODO Altatec.  Below altatec adds a new check, to make sure a client invoice never
            #has a total_to_withold greater than zero.  This is because on a client invoice,
            #the retention accounting move is made when we physically add the retention to the
            #invoice document.  On a purchase invoice, we want total_to_withhold to be equal to the
            #total value of all retentions placed on the line.  Therefore, we should also add a another
            #check in the future so that we never can save an account.invoice.line with a retention tax.
            #For now, we have solved this by making the retention field invisible on client invoices.

            #only in_invoice should have a total_to_withhold...
            if invoice.type != 'in_invoice':
                #if effectively greater than zero:
                if abs(invoice.total_to_withhold) > .001:
                    raise osv.except_osv( _( 'Error' ),
                                          _( 'Error en la calculacion de retenciones.  Consulte con AltaTec.' ) )

            res = super(account_invoice, self).invoice_validate(cr, uid, ids, context)
        return res

    def verify_partner_data(self, cr, uid, partner_id, document_invoice_type_id, context=None):
        '''
        Metodo temporal hasta migrar cedulas y nombres de empresa a la factura
        Valida que existan campos minimos de la empresa EN LA FACTURA
        #TODO: Hacer algo equivalente en retenciones
        '''
        if context is None:
            context = {}
        partner_obj = self.pool.get('res.partner') 
        if document_invoice_type_id.vat_number==True:
            if not partner_obj._get_company_vat(cr, uid, partner_id.id, context):
                return False
        if document_invoice_type_id.address==True:
            if not partner_obj.get_company_address(cr, uid, partner_id.id, None, None, context):
                return False
        if document_invoice_type_id.phone==True:
            if not partner_obj.get_company_phone(cr, uid, partner_id.id, None, None, context):
                return False
        if document_invoice_type_id.mail==True:
            if not partner_obj.browse(cr,uid,partner_id.id,context=context).email:
                return False

        return True

    #TODO ALTATEC Document this function
    def _get_internal_number_by_sequence(self, cr, uid, obj_inv, context=None):
        #NOTA DE DEBITO
        #TODO ALTATEC FIX THIS FOR NOTA DE DEBITO
        if obj_inv.document_invoice_type_id.code == '05' and obj_inv.type == 'out_invoice':
            raise osv.except_osv( _( 'Error!' ), _( 'Usted ha intendado a asignar una secuencia a un tipo de documento que no tiene esta funcionalidad.  Consulte con Altatec.' ) )
            #return self.pool.get('sri.printer.point').get_next_sequence_number(cr, uid, printer_id, 'debit',obj_inv.number, context)

        #Factura de Venta
        elif (obj_inv.document_invoice_type_id.code == '18' ) and obj_inv.type == 'out_invoice':
            return super(account_invoice,self)._get_internal_number_by_sequence(cr, uid, obj_inv, context)

        #Nota de Credito
        elif (obj_inv.document_invoice_type_id.code == '04' ) and obj_inv.type == 'out_refund':
            return super(account_invoice,self)._get_internal_number_by_sequence(cr, uid, obj_inv, context)

        #No Deberia Pasar Nunca
        else:
            raise osv.except_osv( _( 'Error!' ), _( 'Usted ha intendado a asignar una secuencia a un tipo de documento que no tiene esta funcionalidad.  Consulte con Altatec.' ) )


    def action_cancel(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}

        res = True

        for invoice in self.browse(cr, uid, ids, context):
        
            #se permite anular las facturas a pesar que esten duplicadas, necesario para corregir el problema de duplicacion
            if invoice.state not in ['open','cancel'] and not self.check_number(cr, uid, [invoice.id,], context=context):
                raise osv.except_osv(_('Invalid action!'), _('Invoice Number must be unique per Company!')) 
            
        res = super(account_invoice, self).action_cancel(cr, uid, ids, context)

        return res
    
    _defaults = {
       'document_invoice_type_id': _doc_type,        
    }  
      
    def _prepare_invoice_header(self, cr, uid, partner_id, type, inv_date=None, printer_id=None, context=None):
        """Retorna los valores ecuatorianos para el header de una factura
           Puede ser usado en facturacion desde ordenes de compra, venta, proyectos, bodegas
           @partner_id es un objeto partner
           @type es el tipo de factura, ej. out_invoice
           @inv_date es la fecha prevista de la factura, si no se provee se asume hoy
        """

        if context is None:
            context = {}
        invoice_vals = {}
        
        invoice_vals = super(account_invoice, self)._prepare_invoice_header(cr, uid, partner_id, type, inv_date=inv_date, printer_id=printer_id, context=context)
        
        inv_obj=self.pool.get('account.invoice')
        document_invoice_type_id = inv_obj._doc_type(cr, uid, context)
        
        #sustento tributario es definido en el modulo ats 
        #autorizaciones es definido y manejado en el modulo ecua_autorizaciones_sri
        
        invoice_vals.update({
                             'document_invoice_type_id': document_invoice_type_id,
                             })
        return invoice_vals
    
account_invoice()

class account_invoice_line(osv.osv):
    
    _inherit = 'account.invoice.line' 

    def _price_unit_final(self, cr, uid, ids, prop, unknow_none, unknow_dict):
        res = {}
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        
        for line in self.browse(cr, uid, ids):
            price = line.price_unit * (1-(line.discount or 0.0)/100.0)
            res[line.id] = price
            if line.invoice_id:
                cur = line.invoice_id.currency_id
                res[line.id] = cur_obj.round(cr, uid, cur, res[line.id])
                
        return res
    
    _columns = {
          'price_unit_final': fields.function(_price_unit_final, method=True, type='float', digits_compute= dp.get_precision('Invoice Line Price'), string='Price Unit Final', store=True), 
        }

account_invoice_line()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
