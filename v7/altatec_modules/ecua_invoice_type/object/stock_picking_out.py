# -*- encoding: utf-8 -*-
########################################################################
#                                                                       
# @authors: Andres Calle
# Copyright (C) 2013  TRESCLOUD CIA. LTDA.                                 
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import osv, fields
from openerp.tools.translate import _
import re
import decimal_precision as dp
import time
from openerp import netsvc

class stock_picking_out(osv.osv):
    
    _inherit = "stock.picking.out"
    
    def _get_warning_msgs(self, cr, uid, ids, field, arg, context=None):
        '''
        Retorna una explicación de porque la linea de factura está en color rojo en la vista tree
        (cuando el campo warning_msgs no esta vacio la linea de factura se pinta de color rojo) 
        '''        
        result = self.pool.get('stock.picking')._get_warning_msgs(cr, uid, ids, field, arg, context=context)
        return result

    _columns = {
        'document_invoice_type_id': fields.many2one('account.invoice.document.type', 'Document type', help='Indicates the type of accounting document authorized to issue when making a purchase or sale.',),
        'warning_msgs' : fields.function(_get_warning_msgs, string='Warnings',store=False,type='char',method=True, help='There are pending action in this transaction.'),
    }
    
    def check_number(self, cr, uid, ids, context=None):
        '''
        Verifica si existe otra guia de remision con el mismo numero
        Se exceptuan las guias en estado borrador
        en estado borrador puede haber varias guias repetidas
        ejemplo guia sin asignar numero: 001-001- y otra 001-001-)
        '''
        result = self.pool.get('stock.picking').check_number(cr, uid, ids, context=None)
        return result
    
    def _doc_type(self, cr, uid, context=None):
        '''
        El tipo de documento por defecto es el de menor prioridad
        de entre los del mismo tipo (campo type)
        '''
        result = self.pool.get('stock.picking')._doc_type(cr, uid, context=context)
        return result
   
#     def print_document_type(self, cr, uid, ids, context=None):
#         '''
#         Imprime el reporte asociado al tipo de documento tributario
#         '''
#         #TODO: Implmentar este metodo y agregarlo a la vista
#         waybill_obj = self.pool.get('stock.picking')
#         waybill_list = waybill_obj.search(cr, uid, [('id','=',ids[0])])[0]
#         
#         waybill = waybill_obj.browse(cr,uid,invoice_list)
#         invoice_type = invoice.type
#         
#         if invoice.document_invoice_type_id: 
#             service_name = invoice.document_invoice_type_id.report_id.report_name
#         else:
#             cr.execute('''select min(ai.priority) as priority, ai.report_id as report_id, r.report_name as report_name 
#                         from account_invoice_document_type ai, ir_act_report_xml r
#                         where ai.report_id=r.id and ai.type='%s'
#                         group by report_id, report_name
#                         order by priority''' %(invoice_type))
#             res = cr.dictfetchone()
#             service_name = res['report_name']
#         
#         self.write(cr, uid, ids, {'sent': True}, context=context)            
#         if service_name:
#             return {
#                  'type': 'ir.actions.report.xml',
#                  'report_name': service_name,    # the 'Service Name' from the report
#                  'datas' : {
#                          'model' : 'account.invoice',    # Report Model
#                          'ids' : ids
#                            }        
#                   }
#         else:
#             raise osv.except_osv(_('Warning!'), _("You do not have established a document format for printing."))
# 
#     def invoice_print(self, cr, uid, ids, context=None):
#         '''
#         Reemplazamos la funcion por defecto por la nueva funcion por compatiblidad con otros modulos
#         '''
#         return self.print_document_type(cr, uid, ids, context)
        
    def waybill_validate(self, cr, uid, ids, context=None):
        '''
        Valida la factura previo aprobacion:
        1. Que sea de contabilidad o de antcipos a empleados
        2. Validacion de duplicidad de factura
        3. Validacion del formato de numero de factura
        4. Validacion de campos minimos de la empresa asociada
        '''
        result = self.pool.get('stock.picking').waybill_validate(cr, uid, ids, context=context)
        return result

    def cancel_assign(self, cr, uid, ids, context=None):
        result = self.pool.get('stock.picking').cancel_assign(cr, uid, ids, context=context)
        return result
    
    _defaults = {
       'document_invoice_type_id': _doc_type,        
    }  
    
#     #TODO: Implementar  
#     def _prepare_waybill_header(self, cr, uid, partner_id, type, inv_date=None, context=None):
#         """Retorna los valores ecuatorianos para el header de una guia de remision
#            Puede ser usado en facturacion desde ordenes de compra, venta, proyectos, bodegas
#            @partner_id es un objeto partner
#            @type es el tipo de factura, ej. out_invoice
#            @inv_date es la fecha prevista de la factura, si no se provee se asume hoy
#         """
# 
#         if context is None:
#             context = {}
#         invoice_vals = {}
#         
#         invoice_vals = super(account_invoice, self)._prepare_invoice_header(cr, uid, partner_id, type, inv_date=inv_date, context=context)
#         
#         inv_obj=self.pool.get('account.invoice')
#         document_invoice_type_id = inv_obj._doc_type(cr, uid, context)
#         
#         #sustento tributario es definido en el modulo ats 
#         #autorizaciones es definido y manejado en el modulo ecua_autorizaciones_sri
#         
#         invoice_vals.update({
#                              'document_invoice_type_id': document_invoice_type_id,
#                              })
#         return invoice_vals

stock_picking_out()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
