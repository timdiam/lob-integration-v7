# -*- encoding: utf-8 -*-
########################################################################
#                                                                       
# @authors: Carlos Yumbillo                                                                           
# Copyright (C) 2013  TRESCLOUD Cia Ltda.                                 
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warran[01] Factura de compraty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import fields,osv
from tools.translate import _
import re

class tax_support(osv.osv):
    
    _name = 'sri.tax.support'
    _order = 'priority'
    _inherit = ['mail.thread']
    _columns = {
            'code':fields.char('Code', size=2, required=True,track_visibility='onchange', help='Field of two digits that indicate the code for this tax support.',),
            'priority':fields.integer('Priority', required=True,track_visibility='onchange', help='Priority of tax support.',),
            'name':fields.char('Name', size=255, required=True,track_visibility='onchange', help='Name of tax support.',),
            'active': fields.boolean('Active',track_visibility='onchange', help="If the active field is set to False."),
     }
    _defaults={
               "active": lambda *args: True,
               }
    
    def name_get(self,cr,uid,ids, context=None):

        if context is None:
            context = {}
        if not ids:
            return []
        reads = self.read(cr, uid, ids, ['name','code'], context=context)
        res = []
        for record in reads:
            name = record['name']
            if record['code']:
                name = '['+record['code']+'] '+name
            res.append((record['id'], name))
        return res

    def name_search(self, cr, user, name='', args=None, operator='ilike', context=None, limit=100):
        '''
        Permite buscar ya sea por nombre o por codigo
        hemos copiado la idea de product.product
        '''
        if not args:
            args = []
        if not context:
            context = {}
        ids = []
        if name: #no ejecutamos si el usaurio no ha tipeado nada

            #buscamos por codigo completo
            ids = self.search(cr, user, [('code','=',name)]+ args, limit=limit, context=context)
            if not ids: #buscamos por fraccion de palabra o fraccion de codigo
                # Do not merge the 2 next lines into one single search, SQL search performance would be abysmal
                # on a database with thousands of matching products, due to the huge merge+unique needed for the
                # OR operator (and given the fact that the 'name' lookup results come from the ir.translation table
                # Performing a quick memory merge of ids in Python will give much better performance
                ids = set()
                ids.update(self.search(cr, user, args + [('code',operator,name)], limit=limit, context=context))
                if not limit or len(ids) < limit:
                    # we may underrun the limit because of dupes in the results, that's fine
                    ids.update(self.search(cr, user, args + [('name',operator,name)], limit=(limit and (limit-len(ids)) or False) , context=context))
                ids = list(ids)
            if not ids:
                ptrn = re.compile('(\[(.*?)\])')
                res = ptrn.search(name)
                if res:
                    ids = self.search(cr, user, [('code','=', res.group(2))] + args, limit=limit, context=context)

        else: #cuando el usuario no ha escrito nada aun
            ids = self.search(cr, user, args, limit=limit, context=context)
        result = self.name_get(cr, user, ids, context=context)
        return result
            
tax_support()

class document_type(osv.osv):
    
    _inherit = 'account.invoice.document.type'
    
    _columns = {
         'sri_tax_support_ids':fields.many2many('sri.tax.support', 'document_type_sri_tax_support_rel', 'document_type_id', 'tax_support_id','Tax support lines',),       
     }
    
document_type()