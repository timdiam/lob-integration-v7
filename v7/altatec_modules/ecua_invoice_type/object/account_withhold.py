# -*- encoding: utf-8 -*-
########################################################################
#                                                                       
# @authors: Andres Calle                                                        
# Copyright (C) 2013  TRESCLOUD CIA. LTDA.                                 
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import osv, fields
from openerp.tools.translate import _
import re
import decimal_precision as dp
import time
from openerp import netsvc


class account_withhold(osv.osv):
    
    _inherit = "account.withhold"
    _name = "account.withhold"

class account_withhold(osv.osv):
    
    _inherit = "account.withhold"
    
    def _get_warning_msgs(self, cr, uid, ids, field, arg, context=None):
        '''
        Retorna una explicación de porque la linea de factura está en color rojo en la vista tree
        (cuando el campo warning_msgs no esta vacio la retencion se pinta de color rojo) 
        '''        
        res = {}
        for invoice in self.browse(cr, uid, ids, context):
            res[invoice.id]= self._warning_msgs(cr, uid, invoice.id, context)
        return res

    def _warning_msgs(self, cr, uid, ids, context=None):
        '''
        Metodo helper: Ayuda a redefinir con facilidad el campo tipo funcion asociado al metodo _get_warning_msgs
        Retorna una explicacion de porque la retencion esta en color rojo en la vista tree
        '''
        warning_msgs = ''
        return warning_msgs
    
    _columns = {
        #TODO ALTATEC.  SPEAK WITH ANDRES
        #TODO: Implementar el campo document_invoice_type_id para que sea homogeneo a facturas
        #'document_invoice_type_id': fields.many2one('account.invoice.document.type', 'Document type', help='Indicates the type of accounting document authorized to issue when making a purchase or sale.',),
        'warning_msgs' : fields.function(_get_warning_msgs, string='Warnings',store=False,type='char',method=True, help='There are pending action in this transaction.'),
    }
    
account_withhold()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
