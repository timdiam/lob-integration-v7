# -*- encoding: utf-8 -*-
########################################################################
#                                                                       
# @authors: Andres Calle
# Copyright (C) 2013  TRESCLOUD CIA. LTDA.                                 
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import osv, fields
from openerp.tools.translate import _
import re
import decimal_precision as dp
import time
from openerp import netsvc

class stock_picking(osv.osv):
    
    _inherit = "stock.picking"
    
    def _get_warning_msgs(self, cr, uid, ids, field, arg, context=None):
        '''
        Retorna una explicación de porque la linea de factura está en color rojo en la vista tree
        (cuando el campo warning_msgs no esta vacio la linea de factura se pinta de color rojo) 
        '''        
        res = {}
        for waybill in self.browse(cr, uid, ids, context):
            res[waybill.id]= self._warning_msgs(cr, uid, waybill.id, context)
        return res

    def _warning_msgs(self, cr, uid, ids, context=None):
        '''
        Metodo helper: Ayuda a redefinir con facilidad el campo tipo funcion asociado al metodo _get_warning_msgs
        Retorna una explicacion de porque la linea de factura esta en color rojo en la vista tree
        '''
        warning_msgs = ''
#         inv = self.browse(cr, uid, ids, context)
#         if inv.without_withhold:
#             warning_msgs += 'Acorde a los impuestos que usted ha seleccionado esta pendiente emitir la retencion, de no ser asi corriga los impuestos'
        return warning_msgs

    _columns = {
        'document_invoice_type_id': fields.many2one('account.invoice.document.type', 'Document type', help='Indicates the type of accounting document authorized to issue when making a purchase or sale.',),
        'warning_msgs' : fields.function(_get_warning_msgs, string='Warnings',store=False,type='char',method=True, help='There are pending action in this transaction.'),
    }
    
    def check_number(self, cr, uid, ids, context=None):
        '''
        Verifica si existe otra guia de remision con el mismo numero
        Se exceptuan las guias en estado borrador
        en estado borrador puede haber varias guias repetidas
        ejemplo guia sin asignar numero: 001-001- y otra 001-001-)
        '''
        waybill = self.browse(cr, uid, ids, context=context)[0]
        if waybill.is_waybill: #se valida solo si es una guia de remisión
            #TODO: deberia buscarse por codigo de tipo de documento, no por id pues puede haber varios id para el mismo codigo 
            search = [('is_waybill','=',waybill.is_waybill),
                      ('waybill_number','=',waybill.waybill_number),
                      ('company_id','=',waybill.company_id.id),
                      ('type','=','out_waybill'),
                      ('document_invoice_type_id','=',waybill.document_invoice_type_id.id),
                      ('state','not in',['draft']),
                      ]
            #no se valida guias de remision de proveedores
            #if lines.type == 'in_invoice':
            #    search.append(('partner_id','=',lines.partner_id.id))
            n=self.search(cr,uid,search)
            if len(n)>=1:
                    return False
        return True
    
    def _doc_type(self, cr, uid, context=None):
        '''
        El tipo de documento por defecto es el de menor prioridad
        de entre los del mismo tipo (campo type)
        '''
        doc_type_id = None
        #         if context and 'type' in context:
        #             waybill_type = context['type']
        #             if waybill_type in ['out','internal']:
        #                 waybill_type = 'out_waybill'
        cr.execute('''select min(ai.priority) as priority, ai.id as id
                  from account_invoice_document_type ai
                  where ai.type='%s'
                  group by id
                  order by priority''' %('out_waybill'))
        res = cr.dictfetchone()
        
        if res:
           doc_type_id = res['id']
        return doc_type_id
   
#     def print_document_type(self, cr, uid, ids, context=None):
#         '''
#         Imprime el reporte asociado al tipo de documento tributario
#         '''
#         #TODO: Implmentar este metodo y agregarlo a la vista
#         waybill_obj = self.pool.get('stock.picking')
#         waybill_list = waybill_obj.search(cr, uid, [('id','=',ids[0])])[0]
#         
#         waybill = waybill_obj.browse(cr,uid,invoice_list)
#         invoice_type = invoice.type
#         
#         if invoice.document_invoice_type_id: 
#             service_name = invoice.document_invoice_type_id.report_id.report_name
#         else:
#             cr.execute('''select min(ai.priority) as priority, ai.report_id as report_id, r.report_name as report_name 
#                         from account_invoice_document_type ai, ir_act_report_xml r
#                         where ai.report_id=r.id and ai.type='%s'
#                         group by report_id, report_name
#                         order by priority''' %(invoice_type))
#             res = cr.dictfetchone()
#             service_name = res['report_name']
#         
#         self.write(cr, uid, ids, {'sent': True}, context=context)            
#         if service_name:
#             return {
#                  'type': 'ir.actions.report.xml',
#                  'report_name': service_name,    # the 'Service Name' from the report
#                  'datas' : {
#                          'model' : 'account.invoice',    # Report Model
#                          'ids' : ids
#                            }        
#                   }
#         else:
#             raise osv.except_osv(_('Warning!'), _("You do not have established a document format for printing."))
# 
#     def invoice_print(self, cr, uid, ids, context=None):
#         '''
#         Reemplazamos la funcion por defecto por la nueva funcion por compatiblidad con otros modulos
#         '''
#         return self.print_document_type(cr, uid, ids, context)
        
    def waybill_validate(self, cr, uid, ids, context=None):
        '''
        Valida la guia previo aprobacion:
        1. Que sea de contabilidad o de antcipos a empleados
        2. Validacion de duplicidad de factura
        3. Validacion del formato de numero de factura
        4. Validacion de campos minimos de la empresa asociada
        '''
        if context is None:
            context = {}
        res = True
        invoice_obj = self.pool.get('account.invoice')

        for waybill in self.browse(cr, uid, ids, context):
            #TODO: Dividir entre las funciones existentes de fecha, numeracion y validacion de factura
            if not invoice_obj.verify_partner_data(cr, uid, waybill.partner_id, waybill.document_invoice_type_id, context):
                raise osv.except_osv(_('Invalid action!'), _('El cliente proveedor no cuenta con datos completos, verifique RUC/Cedula, Direccion, Telefono!'))
            if not self.check_number(cr, uid, ids, context=context):
                raise osv.except_osv(_('Invalid action!'), _('El numero de guia de remision debe ser unico por compañia!')) 
            if waybill.document_invoice_type_id.number_format_validation and waybill.is_waybill:
                cadena='(\d{3})+\-(\d{3})+\-(\d{9})' 
                ref = waybill.waybill_number
                if not re.match(cadena, ref):
                    raise osv.except_osv(_('Invalid action!'), _('El numero de guia de remision es incorrecto, deberia tener la forma 000-000-000000000'))
            
            res = super(stock_picking, self).waybill_validate(cr, uid, ids, context)
        return res

    def cancel_assign(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}

        res = True

        for waybill in self.browse(cr, uid, ids, context):
        
            #se permite anular las facturas a pesar que esten duplicadas, necesario para corregir el problema de duplicacion
            if waybill.state not in ['auto','confirmed','assigned','done','cancel'] and not self.check_number(cr, uid, [waybill.id,], context=context):
                raise osv.except_osv(_('Accion Invalida!'), _('El numero de guia de remision debe ser unico por compañia!')) 
            
            res = super(stock_picking, self).cancel_assign(cr, uid, ids, context)

        return res
    
    _defaults = {
       'document_invoice_type_id': _doc_type,        
    }  
    
#     #TODO: Implementar  
#     def _prepare_waybill_header(self, cr, uid, partner_id, type, inv_date=None, context=None):
#         """Retorna los valores ecuatorianos para el header de una guia de remision
#            Puede ser usado en facturacion desde ordenes de compra, venta, proyectos, bodegas
#            @partner_id es un objeto partner
#            @type es el tipo de factura, ej. out_invoice
#            @inv_date es la fecha prevista de la factura, si no se provee se asume hoy
#         """
# 
#         if context is None:
#             context = {}
#         invoice_vals = {}
#         
#         invoice_vals = super(account_invoice, self)._prepare_invoice_header(cr, uid, partner_id, type, inv_date=inv_date, context=context)
#         
#         inv_obj=self.pool.get('account.invoice')
#         document_invoice_type_id = inv_obj._doc_type(cr, uid, context)
#         
#         #sustento tributario es definido en el modulo ats 
#         #autorizaciones es definido y manejado en el modulo ecua_autorizaciones_sri
#         
#         invoice_vals.update({
#                              'document_invoice_type_id': document_invoice_type_id,
#                              })
#         return invoice_vals

stock_picking()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
