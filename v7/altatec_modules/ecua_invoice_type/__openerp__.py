# -*- coding: utf-8 -*-
########################################################################
#                                                                       
# @authors:TRESCLOUD Cia.Ltda                                                                           
# Copyright (C) 2013                                  
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
#ice
########################################################################
{
   "name" : "Módulo de facturación a proveedores/clientes para Ecuador",
   "author" : "TRESCLOUD Cia. Ltda.",
   "maintainer": 'TRESCLOUD Cia. Ltda.',
   "website": 'http://www.trescloud.com',
   'complexity': "easy",
   "description": """Sistema de gestión y control de compras y ventas 
   
   Este sistema permite el control del tipo de documento autorizado a emitir al momento de realizar una compra o venta, por ejemplo (Factura, nota de venta, liquidación de compra, etc).
   
   Agrega opciones contables locales de Ecuador en general, las mismas podrian ser migradas al modulo l10n_ec_niif_minimal:
   - Gestion de distintos tipos de documentos tributarios tales como facturas, notas de venta, etc.
   - Pedir cedula, direccion, email, cuando el documento amerite (ej. facturas, docs electronicos)
   - Reconfiguracion de moneda (tipo de cambio para la moneda local igual a 1)
   - Mensajes de advertencia en facturas, retenciones y guias de remision 
        
   Desarrollador:
   
   Andres Calle,
   Carlos Yumbillo,
   Andrea García
   
   """,
   "category": "Contabilidad",
   "version" : "2.1",
   'depends': ['base',
               'account',
               'report_aeroo',
               'report_aeroo_ooo',
               'account_voucher',
               'ecua_invoice',
               'ecua_check',
               'ecua_tax_withhold',
               #'ecua_stock',
               ],
   'data': [
       'security/ir.model.access.csv',
       'report/report.xml',
       'data/tax_support.xml',              
       'data/document_type.xml',
       'data/currency.xml',
       'view/ecua_invoice_type_view.xml', 
       'view/asiento_view.xml',
       'view/tax_support_view.xml',
       'view/document_type_view.xml',    
       'view/account_voucher_view.xml',
       'view/res_partner_view.xml',
       #'view/stock_picking_view.xml',
       #'view/stock_picking_out_view.xml',
   ],
   'installable': True,
}