# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Christopher Ormaza
# Copyright (C) 2013  Ecuadorenlinea.net
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from osv import osv
from osv import fields
from tools.translate import _

######################################################################################################
# account_invoice inherited class definition
######################################################################################################
class account_invoice(osv.osv):

    _inherit = "account.invoice"

    ######################################################################################################
    # Returns a dictionary of invoice_id's -> boolean. Boolean is True if that invoice has a withhold
    ######################################################################################################
    def _withhold_exist(self, cr, uid ,ids, filed_name, arg, context=None):

        value = {}
        exist = False
        
        for invoice in self.browse(cr, uid, ids, context):
            if invoice.withhold_id:
                 exist = True
            value[invoice.id] = exist
        
        return value

    ######################################################################################################
    # copy() override. Don't copy the withhold_ids, or withhold_line_ids
    # TODO: Why is this function here? It gets overridden below
    ######################################################################################################
    def copy(self, cr, uid, id, default={}, context=None):

        if context is None:
            context = {}

        default.update( { 'withhold_ids':[], #TODO: Shouldn't this be withhold_id? without the 's'
                          'withhold_line_ids':[], #TODO: This is a related field, so it shouldn't copy anyway
                        }
                      )

        return super(account_invoice, self).copy(cr, uid, id, default, context)

    ######################################################################################################
    # action_cancel() override. Throw an error if the invoice has a withhold
    ######################################################################################################
    def action_cancel(self, cr, uid, ids, *args):

        context={}

        for invoice in self.browse( cr, uid, ids, context=context ):
            if( invoice.withhold_id ):
                warn = _("Warning")
                message = _("Can't cancel a invoice with a associated withhold, please, delete the withhold first")
                raise osv.except_osv(warn, message)

        return super(account_invoice, self).action_cancel(cr, uid, ids, context)

    ######################################################################################################
    # copy() override. Doesn't copy the withhold_line_ids, or withhold_ids
    ######################################################################################################
    def copy(self, cr, uid, id, default=None, context=None):

        if default is None:
            default = {}

        if context is None:
            context = {}

        default = default.copy() #TODO: What does this do exactly?
        default['withhold_line_ids'] = False #TODO: This is a related field, so it shouldn't copy anyway
        default['withhold_id']       = False
        
        return super(account_invoice, self).copy(cr, uid, id, default, context=context)

    ######################################################################################################
    # Returns an action that opens a wizard to create a retention
    ######################################################################################################
    def add_witthold(self, cr, uid, ids, context=None):
        
        if ids:
            invoice = self.browse(cr, uid, ids[0])

            # check it's a purchase invoice
            if invoice.type == 'in_invoice':
                # Check if the invoice have retention value
                #if invoice.total_to_withhold == 0.0:
                if False:
                    warn = _("Warning")
                    message = _("The total value of withhold is zero")
                    raise osv.except_osv(warn, message)
                else:
                    data_obj = self.pool.get('ir.model.data')
                    view = data_obj.get_object_reference(cr, uid, 'ecua_tax_withhold', 'withhold_wizard_form_purchase')
                    view_id = view and view[1] or False
                    
                    if view_id:
                        res =  { 'name'         : _("Complete data Purchase Withhold"),
                                 'view_type'    : 'form',
                                 'view_mode'    : 'form',
                                 'res_model'    : 'account.withhold',
                                 'view_id'      : view_id,
                                 'type'         : 'ir.actions.act_window',
                                 'nodestroy'    : True,
                                 'auto_refresh' : True,
                                 'target'       : 'new',
                                 'context'      : {'transaction_type':'purchase'},
                               }
                
                        return res
        return {}

    ######################################################################################################
    # This function returns an action that display existing withhold of given invoice ids.
    # It can either be a in a list or in a form view, if there is only one invoice to show.
    ######################################################################################################
    def action_view_withhold(self, cr, uid, ids, context=None):

        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        invoice = self.browse(cr, uid, ids[0])

        #Depending if is a sale o purchase withhold search for the corresponding action
        action,view = "",""
        if invoice.type == 'in_invoice':
            action = "action_account_withhold_purchase"
            view = "view_account_withhold_form_purchase"
        else:
            action = "action_account_withhold_sale"
            view = "view_account_withhold_form_sale"
            
        result = mod_obj.get_object_reference(cr, uid, 'ecua_tax_withhold', action)
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]
        res = mod_obj.get_object_reference(cr, uid, 'ecua_tax_withhold', view)
        result['views'] = [(res and res[1] or False, 'form')]
        result['res_id'] = invoice.withhold_id and invoice.withhold_id.id or False
            
        return result

    ######################################################################################################
    # Column and defaults definition
    ######################################################################################################
    _columns = { 'withhold_id'       : fields.many2one('account.withhold', 'Withhold', states={'paid':[('readonly',True)]}, help="number of related withhold"),
                 'withhold_line_ids' : fields.related('withhold_id', 'withhold_line_ids',
                                                      type='one2many', relation='account.withhold.line',
                                                      string='Withhold Lines',
                                                      states={'paid':[('readonly',True)]},
                                                      help="Lines description of related withhold"),
                 'address_invoice'   : fields.char("Invoice address", help="Address of invoice"),
                 'withhold_exist'    : fields.function(_withhold_exist, type='boolean', method=True, store=False, help="Show internally if a withhold asociated exist"),
               }