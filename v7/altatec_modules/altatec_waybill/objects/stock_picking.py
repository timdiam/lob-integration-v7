# -*- coding: utf-8 -*-
########################################################################
#
########################################################################
from osv import fields,osv
from datetime import datetime


########################################################################
#
########################################################################
class stock_picking(osv.osv):

    _inherit = 'stock.picking'

    ########################################################################
    # send_electronic_waybill()
    ########################################################################
    def send_electronic_waybill( self, cr, uid, ids, context=None ):

        def get_identification(vat):
            if vat!=False or vat is None:
                if vat[:2] == 'EC':
                    partner_vat = vat[2:]
                if vat[:2] != 'EC':
                    partner_vat = vat
            else:
                partner_vat = 'Especifique identificacion correcta.'

            return partner_vat

        for picking in self.browse( cr, uid, ids, context ):

            # ERROR CHECKS
            if not picking.partner_id:
                raise osv.except_osv( "Error!", "Debe elegir un cliente." )
            if not picking.transportista:
                raise osv.except_osv( "Error!", "Debe elegir un transportista." )
            if picking.electronic_id and picking.electronic_id.state == 'sri_authorized':
                raise osv.except_osv( "Error!", "Esta guia de remision ya tiene un documento electronico que esta autorizado." )
            if not picking.date:
                raise osv.except_osv( "Error!", "Debe elegir una fecha de inicio." )
            if not picking.fecha_fin_transporte:
                raise osv.except_osv( "Error!", "Debe elegir una fecha fin transporte." )
            if not picking.fecha_fin_transporte:
                raise osv.except_osv( "Error!", "Debe elegir un tipo documento." )
            if not picking.printer_id:
                raise osv.except_osv( "Error!", "Debe elegir un punto de impresion." )
            if not picking.motivo:
                raise osv.except_osv( "Error!", "Debe elegir un motivo" )
            if not picking.factura_char_hack:
                raise osv.except_osv( "Error!", "No se puede encontrar una factura. Por favor, haga una factura para este pedido de venta." )
            if not picking.printer_id.waybill_sequence_id:
                raise osv.except_osv( "Error!", "El punto de impresion elegido no tiene una secuencia de Guias de Remision." )

            # Get the invoice
            invoice_ids = self.pool.get('account.invoice').search(cr,uid,[('internal_number','=',picking.factura_char_hack)])
            if(len(invoice_ids) > 1):
                raise osv.except_osv( "Error!", "Se encontraron mas que 1 factura con el numero: " + picking.factura_char_hack + ". Esta validada esta factura?")
            elif( len(invoice_ids) == 0 ):
                raise osv.except_osv( "Error!", "No se puede encontrar una factura con el numero: " + picking.factura_char_hack )
            else:
                picking.write({'invoice_id':invoice_ids[0]})

            elec_doc_obj = self.pool.get( 'electronic.document' )

            # Get the waybill_number from the printer_point's sequence
            waybill_number = self.pool.get('ir.sequence')._next(cr,uid,[picking.printer_id.waybill_sequence_id.id],context)
            picking.write({'waybill_number':waybill_number})
            picking = self.browse( cr, uid, picking.id, context )

            # Create the electronic document object
            if not picking.electronic_id:
                electronic_id = elec_doc_obj.create(cr, uid, {'document_id': str(picking._name) + ',' + str(picking.id)}, context)
                self.write(cr, uid, picking.id, {'electronic_id':electronic_id})
                picking = self.browse( cr, uid, picking.id, context=context )

            # Generate a clave
            elec_doc_obj.get_access_key(cr, uid, [picking.electronic_id.id])
            picking = self.browse( cr, uid, picking.id, context )

            # Send the electronic document
            elec_doc_obj.attempt_electronic_document(cr, uid, [picking.electronic_id.id], context)

            if( picking.electronic_id.state == 'sri_authorized' ):
                picking.write({'electronic_authorized':True})


    ########################################################################
    # El tipo de documento por defecto es el de menor prioridad
    # de entre los del mismo tipo (campo type)
    ########################################################################
    def _doc_type(self, cr, uid, ids, context=None):
        ref = self.pool.get( 'ir.model.data' ).get_object_reference( cr, uid, 'ecua_invoice_type', 'code06a' )
        return ref[1]

    def get_order_details(self,cr,uid,ids,field_name,field_value,arg, context={}):

        records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
            so = self.pool.get('sale.order').search(cr,uid,[('name','=',r.origin)])
            if(len(so) == 1):
                result[r.id] = self.pool.get('sale.order').browse(cr,uid,so[0])
            else:
                result[r.id] = False

        return result

    def get_fact_char(self,cr,uid,ids,field_name,field_value,arg, context={}):
        records = self.browse(cr, uid, ids)
        result = {}

        for r in records:
            so = self.pool.get('account.invoice').search(cr,uid,[('origin','=',r.origin),('state','!=','cancel')])
            if(len(so) >= 1):
                result[r.id] = self.pool.get('account.invoice').browse(cr,uid,so[0]).internal_number
            else:
                result[r.id] = ""

        return result

    def get_factura_details(self,cr,uid,ids,field_name,field_value,arg, context={}):
        records = self.browse(cr, uid, ids)
        result = {}
        for r in records:
            so = self.pool.get('account.invoice').search(cr,uid,[('origin','=',r.origin)])
            if(len(so) == 1):
                result[r.id] = self.pool.get('account.invoice').browse(cr,uid,so[0])
            else:
                result[r.id] = False
        return result


    def get_date(self,cr,uid,ids,field_name,field_value,arg,context={}):
        records=self.browse(cr,uid,ids)
        result={}
        for r in records:
            id_docs=self.pool.get('account.invoice').search(cr,uid,[('internal_number','=',r.factura_char_hack)])
            traer_docs=self.pool.get('account.invoice').browse(cr,uid,id_docs)
            if len(id_docs) >= 1:
                for d in traer_docs:
                    if(d.date_invoice):
                        a=datetime.strptime(d.date_invoice, "%Y-%m-%d")
                        b=a.strftime("%Y-%m-%d")
                        result[r.id]=b
                    else:
                        result[r.id] = False
            else:
                result[r.id] = False

        return result

    def _get_default_printer_id(self, cr, uid, context):
        return self.pool.get('account.invoice')._default_printer_point(cr, uid, context)

    ########################################################################
    # Column and default definitions
    ########################################################################
    _columns = { 'is_waybill'               : fields.boolean( "Es Guia de Remision?" ),
                 'manual_document'          : fields.boolean('Manual document'),
                 'printer_id'               : fields.many2one('sri.printer.point',string='Punto de impresion'),
                 'shop_id'                  : fields.related('printer_id', 'shop_id', type='many2one', relation='sale.shop', string='Shop', readonly=True),
                 'transportista'            : fields.many2one('altatec.transportista',string='Transportista'),
                 'fecha_fin_transporte'     : fields.date('Fecha fin transporte'),
                 'codigo_doc_sustento'      : fields.many2one('account.invoice.document.type',string='Codigo documento sustento'),
                 'waybill_number'           : fields.char('WayBill Number', size=17),
                 'ruta'                     : fields.char('Ruta'),
                 'electronic_id'            : fields.many2one('electronic.document', 'Documento Electronico', required=False, track_visibility='onchange'),
                 'electronic_authorized'    : fields.boolean( "Documento electronico autorizado" ),
                 'document_invoice_type_id' : fields.many2one('account.invoice.document.type', 'Tipo de documento'),
                 'motivo'                   : fields.many2one('altatec.waybill.motivo', "Motivo" ),
                 'order_origin_hack'        : fields.function(get_order_details, type='many2one', obj="sale.order", method=True, string='sale_order'),
                 'factura_origin_hack'      : fields.function(get_factura_details, type='many2one',obj="account.invoice",method=True, string='factura'),
                 'factura_char_hack'        : fields.function(get_fact_char,type='char',method=True,string="Factura"),
                 'date_change'              : fields.function(get_date,type='datetime',method=True,string="Fecha Factura"),
               }

    _defaults = { 'is_waybill' : False,
                  'manual_document' : True,
                  'printer_id' : _get_default_printer_id,
                  'document_invoice_type_id' : _doc_type,
                  'fecha_fin_transporte' : fields.date.context_today,
                  'electronic_authorized' : False,
                }

class stock_picking_out(osv.osv):

    _inherit = "stock.picking.out"

    fix_field_list = [ 'is_waybill',
                       'manual_document',
                       'printer_id',
                       'shop_id',
                       'transportista',
                       'codigo_doc_sustento',
                       'fecha_fin_transporte',
                       'waybill_number',
                       'ruta',
                       'electronic_id',
                       'electronic_authorized',
                       'document_invoice_type_id',
                       'motivo',
                       'order_origin_hack',
                       'factura_origin_hack',
                       'factura_char_hack',
                       'date_change',
                     ]

    def send_electronic_waybill( self, cr, uid, ids, context=None ):
        return self.pool.get('stock.picking').send_electronic_waybill(cr,uid,ids,context)

    def __init__(self, pool, cr):
        super(stock_picking_out, self).__init__(pool, cr)
        for fix_field in self.fix_field_list:
            self._columns[fix_field] = self.pool['stock.picking']._columns[fix_field]