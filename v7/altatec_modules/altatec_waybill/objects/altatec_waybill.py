# -*- coding: utf-8 -*-
########################################################################
#
########################################################################
from osv import fields,osv

########################################################################
#
########################################################################
class altatec_transportista(osv.Model):

    _name     = 'altatec.transportista'

    _inherits = { 'res.partner' : 'partner_id' }
    _inherit  = [ 'mail.thread' ]

    _columns = { 'partner_id' : fields.many2one('res.partner', string="Partner",),
                 'placa'      : fields.char('Placa', size=20, required=True),
               }

    _defaults = { 'is_contact' : True,
                }

########################################################################
#
########################################################################
class altatec_waybill_motivo(osv.osv):

    _name = 'altatec.waybill.motivo'

    _columns = { 'name'   : fields.char('Nombre', required=True),
               }