# -*- coding: utf-8 -*-
########################################################################
#                                                                       
# @authors:TRESCLOUD Cia.Ltda Andrea García                                                                          
# Copyright (C) 2013                                  
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
#ice
########################################################################
{
   "name" : "Datos para cargar las estructuras salariales",
   "author" : "TRESCLOUD Cia. Ltda.",
   "maintainer": 'TRESCLOUD Cia. Ltda.',
   "website": 'http://www.trescloud.com',
   'complexity': "easy",
   "description": """
      
   Este sistema permite agregar automaticamente estructuras salariales por defecto.
       
   Desarrollador:
   
   Andrea García
   
   
   """,
   "category": "Human Resources",
   "version" : "1.0",
   'depends': ['base','hr_contract','hr_payroll','account',
               'hr_payroll_account',
               'report_aeroo',
               'report_aeroo_ooo',],
   'init_xml': [],
   'update_xml': [
                  
                  'hr_category_data.xml',
                  'hr_contibution_register_data.xml',
                  'hr_salary_rule_data.xml',
                  'hr_payroll_structure_data.xml',
                  ],
   'installable': True,
}