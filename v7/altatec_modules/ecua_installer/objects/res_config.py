# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (C) 2004-2012 OpenERP S.A. (<http://openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv

class account_config_settings(osv.osv_memory):
    _inherit = 'account.config.settings'

    _columns = {
        'module_ecua_auto_fields': fields.boolean('Change to requires the dates of the authorization',
            help ="""This installs the module ecua_auto_fields."""),
        'module_ecua_auto_fields_seq': fields.boolean('Change to requires of the sequence of the authorization',
            help ="""This installs the module ecua_auto_fields_seq."""),
        'module_ecua_documento_electronico': fields.boolean('Add electronic invoice for Ecuador',
            help ="""This installs the module ecua_documento_electronico."""),
    }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
