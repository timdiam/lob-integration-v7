
import locale
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
import logging


_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')


class fixed_asset_wizard(osv.osv):

    _name="fixed.asset.wizard"

    def generar_reporte(self,cr,uid,ids,context=None):
        records=self.browse(cr,uid,ids)[0]

        arg=[]
        if records.state:
            arg.append((('state', '=', records.state)))
        else:
            arg.append(('state','!=', 'draft'))
        if records.date_start:
            arg.append((('purchase_date', '>=', records.date_start)))
        if records.date_end:
            arg.append((('purchase_date', '<=', records.date_end)))
        if records.category_id:
            arg.append((('category_id', '=', records.category_id.id)))

        activos=self.pool.get('account.asset.asset').search(cr,uid,arg)

        result = {'type' :'ir.actions.report.xml',
                  'context':context,
                  'report_name':'fixed_asset_wizard',
                  'datas':
                      {
                          'ids':activos}
                  }


        return result


    _columns={

        "date_start":fields.date("Fecha Inicio"),
        "date_end":fields.date("Fecha Fin"),
        'category_id': fields.many2one('account.asset.category', 'Categoria', ),
        'state': fields.selection([('open','En ejecucion'),('close','Cerrado')], 'Estado'),
        }