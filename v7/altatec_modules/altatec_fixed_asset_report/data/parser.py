from report import report_sxw
from report.report_sxw import rml_parse
import pooler
import logging
_logger = logging.getLogger(__name__)

class Parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_fecha_amort':self.get_fecha_amort,
            'get_sig_amort':self.get_sig_amort,
            'get_amort_acum':self.get_amort_acum,
            'get_no_dep':self.get_no_dep,
            'cr':cr,
            'uid':uid,
            'g_context':context,
            })

    def get_fecha_amort(self,nomina):

        #self.localcontext['g_context'].update({'states': ('done',), 'what': ('in', 'out'), 'location': 22})
        id_account= self.pool.get('account.move.line').search(self.localcontext['cr'], self.localcontext['uid'],[('asset_id','=',nomina.id)], order='date')
        obj_account=self.pool.get('account.move.line').browse(self.localcontext['cr'], self.localcontext['uid'],id_account)
        fecha=obj_account[-1].date_created

        return fecha

    def get_sig_amort(self,nomina):

        #self.localcontext['g_context'].update({'states': ('done',), 'what': ('in', 'out'), 'location': 22})
        id_account= self.pool.get('account.move.line').search(self.localcontext['cr'], self.localcontext['uid'],[('asset_id','=',nomina.id)], order='date')
        obj_account=self.pool.get('account.move.line').browse(self.localcontext['cr'], self.localcontext['uid'],id_account)

        amort=obj_account[-1].debit

        return amort

    def get_amort_acum(self,nomina):

        #self.localcontext['g_context'].update({'states': ('done',), 'what': ('in', 'out'), 'location': 22})
        id_account= self.pool.get('account.move.line').search(self.localcontext['cr'], self.localcontext['uid'],[('asset_id','=',nomina.id)], order='date')
        obj_account=self.pool.get('account.move.line').browse(self.localcontext['cr'], self.localcontext['uid'],id_account)
        suma_amort = 0
        for i in  obj_account :
            suma_amort=i.debit+suma_amort
        return suma_amort

    def get_no_dep(self,nomina):

        #self.localcontext['g_context'].update({'states': ('done',), 'what': ('in', 'out'), 'location': 22})
        id_account= self.pool.get('account.move.line').search(self.localcontext['cr'], self.localcontext['uid'],[('asset_id','=',nomina.id)], order='date')
        obj_account=self.pool.get('account.move.line').browse(self.localcontext['cr'], self.localcontext['uid'],id_account)
        suma_amort = 0
        for i in  obj_account :
            suma_amort=1+suma_amort
        return suma_amort