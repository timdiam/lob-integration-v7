{
    'name': "Report Fixed Asset",
    'version': '1.0',
    'description': """
            Report Fixed Asset
""",
    'author': 'Harry Alvarez',
    'website': 'www.altatececuador.com',
    "depends": ['account_asset',
                'account'],
    "data": [
        'wizard/asset_wizard_view.xml',
        'data/activo_fijo.ods',
        'data/activo_fijo.xml'


    ],
    "active": False,
    "installable": True

}