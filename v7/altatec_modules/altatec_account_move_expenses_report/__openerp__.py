# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2013 Noviat nv/sa (www.noviat.be). All rights reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Altatec Account Move Expenses Report',
    'version': '0.1',
    'license': 'AGPL-3',
    'author': 'Denisse Ochoa - AltaTec',
    'description': """
     This module generates a report in xls that allows us to get all expenses from account move.""",
    'depends': ['report_xls', 'account'],
    'data': ['views/account_move_expenses_wizard_view.xml', 'report/account_move_expenses_report_view.xml'],
}
