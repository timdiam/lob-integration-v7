from openerp.osv import fields, osv, orm


class altatec_account_move_wizard(osv.osv):
    _name = 'altatec.account.move.wizard'

    def generate_report(self, cr, uid, ids, context=None):
        records = self.browse(cr, uid, ids)[0]

        #Obtener todos los asientos contables que pertenezcan al periodo indicado
        #acc_ids = self.pool.get('account.move').search(cr, uid, [('period_id', '=', records.period_id.id)], context=context)
        acc_ids = self.pool.get('account.move.line').search(cr, uid, [('period_id', '=', records.period_id.id),
                                                                      ('move_id.state', '=', 'posted'),
                                                                      ('state', '=', 'valid')],
                                                                      context=context)


        return { 'type'        : 'ir.actions.report.xml',
                 'context'     : context,
                 'report_name' : 'reporte.gastos',
                 'datas'       : { 'ids' : acc_ids }

               }

    _columns = {
        'period_id': fields.many2one('account.period', string='Periodo', required=True),
    }