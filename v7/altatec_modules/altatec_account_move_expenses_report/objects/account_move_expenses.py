import xlwt
from report_xls.report_xls import report_xls
from report_xls.utils import rowcol_to_cell
from altatec_account_move_expenses_report.report.parser import altatec_account_move_expenses_parser
from tools.translate import _
import logging
import base64
from PIL import Image

_logger = logging.getLogger(__name__)

class account_move_expenses_report(report_xls):
    _name = 'account.move.expenses.report'

    def generate_xls_report(self, _p, _xs, data, objects, wb):
        #estilos
        cell_format = _xs['borders_all']
        cell_format_2 = "borders: top thin, bottom thin, left thin, right thin;"
        cell_style_2 = xlwt.easyxf(cell_format)
        cell_style_bold = xlwt.easyxf(cell_format_2 + _xs['bold'] + _xs['center'])
        cell_style_title = xlwt.easyxf(cell_format + _xs['italic'] + _xs['center'] + 'font: bold on')
        cell_style_resume = xlwt.easyxf(cell_format_2 + _xs['italic'] + _xs['center'] + _xs['fill_blue']+ 'font: bold on')
        cell_style_center_bold = xlwt.easyxf(cell_format_2 + _xs['center'] + _xs['fill_blue'] + 'font: bold on')
        cell_style = xlwt.easyxf(cell_format_2)
        c_hdr_cell_style_decimal = xlwt.easyxf(cell_format_2 + _xs['right'], num_format_str = report_xls.decimal_format)
        c_hdr_cell_style_decimal_resume = xlwt.easyxf(cell_format_2 + _xs['center'] + _xs['fill_blue'] + 'font: bold on', num_format_str = report_xls.decimal_format)

        ws = wb.add_sheet("Reporte de Gastos")

        #obtener las cuentas agrupadas y sus respectivos totales
        result = group_total_by_account(objects)

        self.auto_gen_id = 0
        def auto_name():
            self.auto_gen_id = self.auto_gen_id + 1
            return self.auto_gen_id

        row_pos = 0
        period = objects[0].period_id.name
        #Encabezado de reporte
        c_specs = [(auto_name(), 14, 0, 'text', 'REPORTE DE TIPO DE GASTOS'),
                  ]

        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_title)

        c_specs = [(auto_name(), 14, 0, 'text', period),
               ]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_title)

        #Encabezado de celdas
        ws.write_merge(3, 4, 0, 3, 'Cuenta Contable de Gastos', cell_style_center_bold) #Nombre de cuenta
        ws.write_merge(3, 4, 4, 4, 'Valor', cell_style_center_bold)                     #Valor
        ws.write_merge(3, 3, 5, 6, 'Gastos Deducibles', cell_style_center_bold)        #Gastos Deducibles
        ws.write(4, 5, 'Debe', cell_style_center_bold)                                 #Gastos Deducibles Debe
        ws.write(4, 6, 'Haber', cell_style_center_bold)                                #Gastos Deducibles Haber
        ws.write_merge(3, 4, 7, 7, 'Total Gastos No Deducibles', cell_style_center_bold) #Total Gastos
        ws.write_merge(3, 3, 8, 9, 'Gastos No deducibles Niif', cell_style_center_bold) #Gastos no deducibles Niif
        ws.write(4, 8, 'Debe', cell_style_center_bold)                                 #Gastos Deducibles Debe
        ws.write(4, 9, 'Haber', cell_style_center_bold)                                #Gastos Deducibles Haber
        ws.write_merge(3, 3, 10, 11, 'Gastos No deducibles', cell_style_center_bold)     #Gastos no deducibles
        ws.write(4, 10, 'Debe', cell_style_center_bold)                                 #Gastos Deducibles Debe
        ws.write(4, 11, 'Haber', cell_style_center_bold)                                #Gastos Deducibles Haber
        ws.write_merge(3, 3, 12, 13, 'Sin Clasifiacion', cell_style_center_bold)        #Sin clasificacion
        ws.write(4, 12, 'Debe', cell_style_center_bold)                                 #Gastos Deducibles Debe
        ws.write(4, 13, 'Haber', cell_style_center_bold)                               #Gastos Deducibles Haber

        ini_row = row_start = row_pos = 5

        #llenar el reporte
        for acc_res, exp_data in result.items():
            #Posicion de celdas rowcol_to_cell(row, column)
            ded_exp_db = rowcol_to_cell(row_start, 5)       #Gasto Deducible Debe
            ded_exp_cr = rowcol_to_cell(row_start, 6)       #Gasto Deducible Haber
            tot_no_ded_exp = rowcol_to_cell(row_start, 7)   #Total de Gastos No Deducibles
            no_ded_exp_niif_db = rowcol_to_cell(row_start, 8) #Gasto No Deducible Niif Debe
            no_ded_exp_niif_cr = rowcol_to_cell(row_start, 9) #Gasto No Deducible Niif Haber
            no_ded_exp_db = rowcol_to_cell(row_start, 10)    #Gasto No Deducible Debe
            no_ded_exp_cr = rowcol_to_cell(row_start, 11)    #Gasto No Deducible Haber
            no_clas_db = rowcol_to_cell(row_start, 12)       #Gasto Sin Clasificacion Debe
            no_clas_cr = rowcol_to_cell(row_start, 13)      #Gasto Sin Clasificacion Haber

            #formulas (Debe - Haber)
            sum_tot_no_ded_exp = no_ded_exp_niif_db + '-' + no_ded_exp_niif_cr + '+' + no_ded_exp_db + '-' + no_ded_exp_cr + '+' + no_clas_db + '-' + no_clas_cr
            sum_tot_formula    = ded_exp_db + '-' + ded_exp_cr + '+' + tot_no_ded_exp

            #columns
            c_specs = [(auto_name(), 4, 0, 'text', exp_data['name']),
                        (auto_name(), 1, 0, 'number', None, sum_tot_formula, c_hdr_cell_style_decimal),
                        (auto_name(), 1, 0, 'number', exp_data['ded_exp_debit']),
                        (auto_name(), 1, 0, 'number', exp_data['ded_exp_credit']),
                        (auto_name(), 1, 0, 'number', None, sum_tot_no_ded_exp, c_hdr_cell_style_decimal),
                        (auto_name(), 1, 0, 'number', exp_data['no_ded_exp_niif_debit']),
                        (auto_name(), 1, 0, 'number', exp_data['no_ded_exp_niif_credit']),
                        (auto_name(), 1, 0, 'number', exp_data['no_ded_exp_debit']),
                        (auto_name(), 1, 0, 'number', exp_data['no_ded_exp_credit']),
                        (auto_name(), 1, 0, 'number', exp_data['no_clas_debit']),
                        (auto_name(), 1, 0, 'number', exp_data['no_clas_credit']),
            ]

            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style)
            row_start = row_pos

        fin_row = row_start - 1

        #formula total
        ini_row = rowcol_to_cell(ini_row, 4)
        fin_row = rowcol_to_cell(fin_row, 4)
        total = 'SUM(' + ini_row + ':' + fin_row + ')'
        c_specs = [(auto_name(), 4, 0, 'text', 'TOTAL'),
                    (auto_name(), 1, 0, 'number', None, total, c_hdr_cell_style_decimal_resume),

            ]

        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style_resume)



#########################################################################################
# Agrupa las cuentas y suma sus respectivos Debe y Haber de acuerdo al periodo consultado
#########################################################################################
def group_total_by_account(objects):
    result = {}
    acc_dict = {'name': '', 'total': 0.0, 'ded_exp_debit': 0.0, 'ded_exp_credit': 0.0, 'tot_no_ded_exp': 0.0,
                'no_ded_exp_niif_debit': 0.0, 'no_ded_exp_niif_credit': 0.0, 'no_ded_exp_debit': 0.0,
                'no_ded_exp_credit': 0.0, 'no_clas_debit': 0.0, 'no_clas_credit': 0.0}

    account_id_list = [] #lleva el control de las cuentas que ya se van guardando


    #Agrupar cuentas por tipo y clasificar segun gastos
    for acc_line in objects:
        if acc_line.account_id.id in account_id_list:
            if acc_line.clasificacion == 'gastos_ded':
                if acc_line.debit:
                    new_value_exp = result[acc_line.account_id.id]['ded_exp_debit'] + acc_line.debit
                    result[acc_line.account_id.id]['ded_exp_debit'] = new_value_exp
                if acc_line.credit:
                    new_value_exp = result[acc_line.account_id.id]['ded_exp_credit'] + acc_line.credit
                    result[acc_line.account_id.id]['ded_exp_credit'] = new_value_exp
            elif acc_line.clasificacion == 'gastos_no_ded':
                if acc_line.debit:
                    new_value_exp = result[acc_line.account_id.id]['no_ded_exp_debit'] + acc_line.debit
                    result[acc_line.account_id.id]['no_ded_exp_debit'] = new_value_exp
                if acc_line.credit:
                    new_value_exp = result[acc_line.account_id.id]['no_ded_exp_credit'] + acc_line.credit
                    result[acc_line.account_id.id]['no_ded_exp_credit'] = new_value_exp
            elif acc_line.clasificacion == 'gastos_def_nid':
                if acc_line.debit:
                    new_value_exp = result[acc_line.account_id.id]['no_ded_exp_niff_debit'] + acc_line.debit
                    result[acc_line.account_id.id]['no_ded_exp_niff_debit'] = new_value_exp
                if acc_line.credit:
                    new_value_exp = result[acc_line.account_id.id]['no_ded_exp_niff_credit'] + acc_line.credit
                    result[acc_line.account_id.id]['no_ded_exp_niff_credit'] = new_value_exp
            else:
                if acc_line.debit:
                    new_value_exp = result[acc_line.account_id.id]['no_clas_debit'] + acc_line.debit
                    result[acc_line.account_id.id]['no_clas_debit'] = new_value_exp
                elif acc_line.credit:
                    new_value_exp = result[acc_line.account_id.id]['no_clas_credit'] + acc_line.credit
                    result[acc_line.account_id.id]['no_clas_credit'] = new_value_exp
        else:
            account_id_list.append(acc_line.account_id.id)
            result[acc_line.account_id.id] = acc_dict.copy()
            result[acc_line.account_id.id]['name'] = acc_line.account_id.code + ' ' + acc_line.account_id.name
            if acc_line.clasificacion == 'gastos_ded':
                if acc_line.debit:
                    result[acc_line.account_id.id]['ded_exp_debit'] = acc_line.debit
                if acc_line.credit:
                    result[acc_line.account_id.id]['ded_exp_credit'] = acc_line.credit
            elif acc_line.clasificacion == 'gastos_no_ded':
                if acc_line.debit:
                    result[acc_line.account_id.id]['no_ded_exp_debit'] = acc_line.debit
                if acc_line.credit:
                    result[acc_line.account_id.id]['no_ded_exp_credit'] = acc_line.credit
            elif acc_line.clasificacion == 'gastos_def_nid':
                if acc_line.debit:
                    result[acc_line.account_id.id]['no_ded_exp_niff_debit'] = acc_line.debit
                if acc_line.credit:
                    result[acc_line.account_id.id]['no_ded_exp_niff_credit'] = acc_line.credit
            else:
                if acc_line.debit:
                    result[acc_line.account_id.id]['no_clas_debit'] = acc_line.debit
                elif acc_line.credit:
                    result[acc_line.account_id.id]['no_clas_credit'] = acc_line.credit

    return result


#function_name(report.report_name, objects_model, parser = parser_class_name)
account_move_expenses_report('report.reporte.gastos', 'account.move.line', parser = altatec_account_move_expenses_parser)


