# -*- coding: utf-8 -*-
#################################################################################
#
# This module generates a report showing the state of the accounts payable/receivable
# ordered by clients/providers.
#
# Author:  Tim Diamond
# Company: AltaTec Ecuador
# Date:    4/2/2015
#
#################################################################################
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

#################################################################################
# payroll benefit report object definition
#################################################################################
class payroll_benefit_report_wizard( osv.osv_memory ):

    _name= 'payroll.benefit.report.wizard'

    #################################################################################
    # Cancelar button
    #################################################################################
    def cancelar(self, cr, uid, ids, context=None):
        return


    #################################################################################
    # Crear button
    #################################################################################
    def crear_reporte(self, cr, uid, ids, context=None):

        wizard = self.browse( cr, uid, ids )[ 0 ]

        if( not wizard.period):
            raise orm.except_orm( 'Error', "Debe elegir un periodo final" )

        ids = self.pool.get('hr.payslip').search(cr,uid,[('period_id','=',wizard.period.id),('state','not in',['draft'])],context=context)

        context={'periodo':wizard.period.name}

        return { 'type'        : 'ir.actions.report.xml',
                 'context'     : context,
                 'report_name' : 'suma_benef_payroll',
                 'datas'       : { 'ids' : ids }
               }


    #################################################################################
    # Column definition
    #################################################################################
    _columns = {
                 "period" : fields.many2one( "account.period", string="Periodo de inicio"),
               }
