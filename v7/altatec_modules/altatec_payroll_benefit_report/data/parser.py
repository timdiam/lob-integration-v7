# -*- coding: utf-8 -*-
import time
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta
from osv import osv, fields
import calendar
from report import report_sxw
from report.report_sxw import rml_parse
import pooler
import logging
from operator import itemgetter
from tools.translate import _
#from dateutil.relativedelta import relativedelta
_logger = logging.getLogger(__name__)

class Parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_employees':self.get_employees,
            'get_period':self.get_period,
            'get_now':self.get_now,
            'cr':cr,
            'uid':uid,
            'g_context':context,
        })


    def get_now(self):
        return time.strftime("%Y-%m-%d")

    def get_period(self):
        return self.localcontext['periodo']

    def get_rubro(self, codigo_rubro, lista_rubros):
        for rubro in lista_rubros:
            if rubro.code == codigo_rubro:
                return rubro.total
        return False

    def get_days(self, day, lista_inputs):
        for d in lista_inputs:
            if d.name == day:
                return d.amount
        return False

    BENEFITS = ['PRESTAMO_QUIROGRAFARIO','PROV FOND RESERV','HORA_EXTRA_REGULAR','BONO','COMISION','SUBT_INGRESOS','SUBT_TOTINGRESOS','PRESTAMOS_HIPOTECARIOS','Otros_Ingresos','IESSPERSONAL 9.45','IESSPATRONAL 12.15','PROV DTERCERO','BASIC']

    # suma SUBT_INGRESOS sin beneficios
    # suma SUBT_TOTINGRESOS con beneficios

    DAYS = [u'Numero de días calendario trabajados (1-30) [días]']

    def get_employees(self, payslips):
        employee_benefits = {}

        #create employee dictionary.. with all zeros
        for payslip in payslips:
            if payslip.employee_id.id not in employee_benefits:
                employee_benefits[payslip.employee_id.id] = {}
                employee_benefits[payslip.employee_id.id]['name'] = payslip.employee_id.name
                employee_benefits[payslip.employee_id.id]['cedula'] = payslip.employee_id.identification_id
                for benefit in self.BENEFITS:
                    employee_benefits[payslip.employee_id.id][benefit] = 0
                for d in self.DAYS:
                    employee_benefits[payslip.employee_id.id]['DIAS_TRABAJADOS']=0
                employee_benefits[payslip.employee_id.id]['DIAS_NO_TRABAJADOS']=0

        #build the sums
        for payslip in payslips:
            for benefit in self.BENEFITS:
                amount = self.get_rubro(benefit, payslip.details_by_salary_rule_category)
                if amount:
                    employee_benefits[payslip.employee_id.id][benefit] += amount

        #days
        for payslip in payslips:
            for day in self.DAYS:
                amount = self.get_days(day, payslip.input_line_ids)
                if amount:
                    employee_benefits[payslip.employee_id.id]['DIAS_TRABAJADOS'] += amount

            employee_benefits[payslip.employee_id.id]['DIAS_NO_TRABAJADOS']=30-employee_benefits[payslip.employee_id.id]['DIAS_TRABAJADOS']


        empleado=[]
        for keys,values in employee_benefits.items():
            empleado.append(values)

        empleado=sorted(empleado, key=itemgetter('name'))

        cont=1
        for secuencia in range(len(empleado)):
            empleado[secuencia].update({'secuencia':cont})
            cont+=1
        return empleado