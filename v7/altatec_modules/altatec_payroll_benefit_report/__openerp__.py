{ 'name'         : 'AltaTec Benefit Report Module',
  'version'      : '1.0',
  'description'  : """
                   This module adds a new report in the menu of hr.payslip that shows a grid of the sum of all benefits
                   """,
  'author'       : 'Tim Diamond',
  'website'      : 'www.altatec.ec',
  "depends"      : [ "hr_payroll",
                     "ecua_hr",
                     "hr",
                   ],
  "data"         : [ "data/report.xml",
                     "views/wizard.xml",
                   ],
  "installable"  : True,
  "auto_install" : False
}
