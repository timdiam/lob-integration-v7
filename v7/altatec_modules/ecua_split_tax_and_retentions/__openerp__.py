{ 'name'         : 'Split ecuadorian retentions and taxes into two separt many2many fields for usability purposes',
  'version'      : '1.0',
  'description'  : """
                   This module adds two shadow fields for taxes that allow the user to separately enter retentions and
                   taxes, and in the background, the system will add these to the account invoice line.
                   This module also splits on product the taxes from the applicable retention. It also adds a wizard that
                   will perform the corresponding migration.
                   """,
  'author'       : 'Tim Diamond',
  'website'      : 'www.altatececuador.com',
  "depends"      : [ 'ecua_tax_withhold',
                     'invoice_line_form_view',
                     'account',
                     'sale',
                     'purchase',
                     'purchase_analytic_plans',
                     'purchase_requisition',
                     'ecua_invoice',
                     'altatec_account_move_view_improvements',

                   ],
  "data"         : [ 'views/account_invoice_view.xml',
                     'views/product_view.xml',
                     'views/purchase_view.xml',
                     'views/account_move_view.xml',
                     'views/sale_view.xml',
                     'wizard/migration_wizard.xml',
                     'security/ir.model.access.csv',
                   ],
  "installable"  : True,
  "auto_install" : False
}