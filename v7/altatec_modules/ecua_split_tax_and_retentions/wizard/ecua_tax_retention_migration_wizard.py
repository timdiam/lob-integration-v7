# -*- coding: utf-8 -*-
################################################################################################
#
# This file contains an OpenERP wizard which performs the data migration of splitting
# taxes and retentions into two separate forms of taxes.
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    Jan. 20th, 2014
#
################################################################################################
from openerp.osv     import fields, osv, orm
from tools.translate import _
import logging

_logger = logging.getLogger(__name__)


################################################################################################
# Migration wizard class definition
################################################################################################
class ecua_tax_retention_migration_wizard(osv.osv_memory):

    _name = "ecua.tax.retention.migration.wizard"


    ################################################################################################
    # Determine if the browse_record of type account.tax is a retention or not
    ################################################################################################
    def is_retention( self, tax ):

        # List of taxes are are actually taxes, but don't have an amount > 0
        taxes = [ "IVA 0% EN INGRESOS POR REEMBOLSO COMO INTERMEDIARIO",
                  "IVA 0% EN PAGOS POR REEMBOLSO DE GASTOS",
                  "IVA 0%(SIN DERECHO A CREDITO TRIBUTARIO)",
                  "IVA 0%(ACTIVOS FIJOS SIN DERECHO A CREDITO TRIBUTARIO)",
                  "IVA 0%(CON DERECHO A CREDITO TRIBUTARIO)",
                  "IVA 0%(ACTIVOS FIJOS CON DERECHO A CREDITO TRIBUTARIO)",
                  "IVA 0% EN ADQUISICIONES LOCALES",
                ]

        # List of taxes are are retentions, but don't have an amount < 0
        retentions = [ "CON CONVENIO DE DOBLE TRIBUTACION - PORCENTAJE DEPENDE DEL CONVE",
                       "EN PAGOS CON DEBITO BANCARIO",
                       "EN PAGOS CON TARJETA DE CREDITO",
                       "PAGOS AL EXTERIOR  NO SUJETOS A RETENCIÓN",
                       "RETENCIONES 100% IVA BASE",
                       "RETENCIONES 30% IVA BASE",
                       "RETENCIONES 70% IVA BASE",
                       "OTRAS COMPRAS DE BIENES Y SERVICIOS NO SUJETAS A RETENCION(0%)",
                     ]

        if tax.name.encode( 'utf-8' ) in taxes:
            return False

        if tax.name.encode( 'utf-8' ) in retentions:
            return True

        if( tax.amount < 0 ):
            return True

        return False


    ################################################################################################
    # Cancel the migration
    ################################################################################################
    def cancel( self, cr, uid, ids, context=None ):
        return


    ################################################################################################
    # Ensure all account.tax objects have categ_id set
    ################################################################################################
    def migrate_account_taxes( self, cr, uid, context = None ):

        tax_obj = self.pool.get( 'account.tax' )
        tax_ids = tax_obj.search( cr, uid, [],      context=context )
        taxes   = tax_obj.browse( cr, uid, tax_ids, context=context )

        for tax in taxes:

            _logger.debug( "Migrating account.tax:" + tax.name.encode( 'utf-8' ) )

            if( self.is_retention( tax ) ):
                tax_obj.write( cr, uid, [ tax.id ], { 'categ_id' : 'retention' }, context=context )
            else:
                tax_obj.write( cr, uid, [ tax.id ], { 'categ_id' : 'tax' }, context=context )

        # Verify that all taxes were given a categ_id
        taxes   = tax_obj.browse( cr, uid, tax_ids, context=context )

        for tax in taxes:
            if( tax.categ_id != 'tax' and tax.categ_id != 'retention' ):
                raise ( _( 'Error!' ), _( "Wasn't able to set categ_id for: " + tax.name ) )


    ################################################################################################
    # Do all of the data migration for sale.order.lines
    ################################################################################################
    def migrate_sale_order_lines( self, cr, uid, context = None ):

        # Let's iterate over all sale.order.line's and add an ecua_shadow_tax or an ecua_shadow_retention
        # for each tax_id
        sale_order_line_obj = self.pool.get( 'sale.order.line' )
        sale_order_line_ids = sale_order_line_obj.search( cr, uid, [], context=context )
        sale_order_lines    = sale_order_line_obj.browse( cr, uid, sale_order_line_ids, context=context )

        for line in sale_order_lines:

            _logger.debug( "Migrating sale.order.line:" + line.name )

            shadow_taxes      = []
            shadow_retentions = []

            for tax in line.tax_id:

                if( tax.categ_id == 'tax' ):
                    shadow_taxes.append( ( 4, tax.id ) )
                elif( tax.categ_id == 'retention' ):
                    shadow_retentions.append( ( 4, tax.id ) )
                else:
                    raise ( _( 'Error!' ), _( "While migrating sale.order.line: " + line.name + " , wasn't able to get categ_id for: " + tax.name ) )

            # Let's first delete the existing ecua_shadow_taxes and ecua_shadow_retentions from the sale.order.line
            old_shadow_taxes      = []
            old_shadow_retentions = []

            for shadow_tax in line.ecua_shadow_taxes     : old_shadow_taxes.append(      ( 3, shadow_tax.id ) )
            for shadow_ret in line.ecua_shadow_retentions: old_shadow_retentions.append( ( 3, shadow_ret.id ) )

            sale_order_line_obj.write( cr,
                                       uid,
                                       [ line.id ],
                                       { 'ecua_shadow_retentions' : old_shadow_retentions,
                                         'ecua_shadow_taxes'      : old_shadow_taxes,
                                       },
                                       context=context
                                     )

            # Now add the new ecua_shadow_taxes and ecua_shadow_retentions to the sale.order.line
            sale_order_line_obj.write( cr,
                                       uid,
                                       [ line.id ],
                                       { 'ecua_shadow_retentions' : shadow_retentions,
                                         'ecua_shadow_taxes'      : shadow_taxes,
                                       },
                                       context=context
                                     )

            # Let's now check the updated sale.order.line to ensure there's a ecua_shadow_tax
            # or ecua_shadow_retention for each tax_id
            updated_line = sale_order_line_obj.browse( cr, uid, line.id, context=context )

            tax_ids              = []
            shadow_tax_ids       = []
            shadow_retention_ids = []

            for tax        in updated_line.tax_id                 : tax_ids.append(                     tax.id )
            for shadow_tax in updated_line.ecua_shadow_taxes      : shadow_tax_ids.append(       shadow_tax.id )
            for shadow_ret in updated_line.ecua_shadow_retentions : shadow_retention_ids.append( shadow_ret.id )

            for tax_id in tax_ids:
                if( ( tax_id not in shadow_tax_ids ) and ( tax_id not in shadow_retention_ids ) ):
                    raise ( _( 'Error!' ), _( "While migrating sale.order.line: " + line.name + " , wasn't able to generate shadow tax or shadow retention for all taxes" ) )


    ################################################################################################
    # Do all of the data migration for purchase.order.lines
    ################################################################################################
    def migrate_purchase_order_lines( self, cr, uid, context = None ):

        # Let's iterate over all purchase.order.line's and add an ecua_shadow_tax or an ecua_shadow_retention
        # for each tax_id
        purchase_order_line_obj = self.pool.get( 'purchase.order.line' )
        purchase_order_line_ids = purchase_order_line_obj.search( cr, uid, [], context=context )
        purchase_order_lines    = purchase_order_line_obj.browse( cr, uid, purchase_order_line_ids, context=context )

        for line in purchase_order_lines:

            _logger.debug( "Migrating purchase.order.line:" + line.name )

            shadow_taxes      = []
            shadow_retentions = []

            for tax in line.taxes_id:

                if( tax.categ_id == 'tax' ):
                    shadow_taxes.append( ( 4, tax.id ) )
                elif( tax.categ_id == 'retention' ):
                    shadow_retentions.append( ( 4, tax.id ) )
                else:
                    raise ( _( 'Error!' ), _( "While migrating purchase.order.line: " + line.name + " , wasn't able to get categ_id for: " + tax.name ) )

            # Let's first delete the existing ecua_shadow_taxes and ecua_shadow_retentions from the purchase.order.line
            old_shadow_taxes      = []
            old_shadow_retentions = []

            for shadow_tax in line.ecua_shadow_taxes     : old_shadow_taxes.append(      ( 3, shadow_tax.id ) )
            for shadow_ret in line.ecua_shadow_retentions: old_shadow_retentions.append( ( 3, shadow_ret.id ) )

            purchase_order_line_obj.write( cr,
                                           uid,
                                           [ line.id ],
                                           { 'ecua_shadow_retentions' : old_shadow_retentions,
                                             'ecua_shadow_taxes'      : old_shadow_taxes,
                                           },
                                           context=context
                                         )

            # Now add the new ecua_shadow_taxes and ecua_shadow_retentions to the purchase.order.line
            purchase_order_line_obj.write( cr,
                                           uid,
                                           [ line.id ],
                                           { 'ecua_shadow_retentions' : shadow_retentions,
                                             'ecua_shadow_taxes'      : shadow_taxes,
                                           },
                                           context=context
                                         )

            # Let's now check the updated purchase.order.line to ensure there's a ecua_shadow_tax
            # or ecua_shadow_retention for each tax_id
            updated_line = purchase_order_line_obj.browse( cr, uid, line.id, context=context )

            tax_ids              = []
            shadow_tax_ids       = []
            shadow_retention_ids = []

            for tax        in updated_line.taxes_id               : tax_ids.append(                     tax.id )
            for shadow_tax in updated_line.ecua_shadow_taxes      : shadow_tax_ids.append(       shadow_tax.id )
            for shadow_ret in updated_line.ecua_shadow_retentions : shadow_retention_ids.append( shadow_ret.id )

            for tax_id in tax_ids:
                if( ( tax_id not in shadow_tax_ids ) and ( tax_id not in shadow_retention_ids ) ):
                    raise ( _( 'Error!' ), _( "While migrating purchase.order.line: " + line.name + " , wasn't able to generate shadow tax or shadow retention for all taxes" ) )


    ################################################################################################
    # Do all of the data migration for account_invoice.line
    ################################################################################################
    def migrate_account_invoice_lines( self, cr, uid, context = None ):

        # Let's iterate over all account.invoices.lines's and add an ecua_shadow_tax or an ecua_shadow_retention
        # for each tax_id
        invoice_line_obj = self.pool.get( 'account.invoice.line' )
        invoice_line_ids = invoice_line_obj.search( cr, uid, [], context=context )
        invoice_lines    = invoice_line_obj.browse( cr, uid, invoice_line_ids, context=context )

        for line in invoice_lines:

            _logger.debug( "Migrating account.invoice.line from invoice:" + line.invoice_id.internal_number )

            shadow_taxes      = []
            shadow_retentions = []

            for tax in line.invoice_line_tax_id:

                if( tax.categ_id == 'tax' ):
                    shadow_taxes.append( ( 4, tax.id ) )
                elif( tax.categ_id == 'retention' ):
                    shadow_retentions.append( ( 4, tax.id ) )
                else:
                    raise ( _( 'Error!' ), _( "While migrating account.invoice.line: " + line.invoice_id.internal_number + " , wasn't able to get categ_id for: " + tax.name ) )

            # Let's first delete the existing ecua_shadow_taxes and ecua_shadow_retentions from the account.invoice.line
            old_shadow_taxes      = []
            old_shadow_retentions = []

            for shadow_tax in line.ecua_shadow_taxes     : old_shadow_taxes.append(      ( 3, shadow_tax.id ) )
            for shadow_ret in line.ecua_shadow_retentions: old_shadow_retentions.append( ( 3, shadow_ret.id ) )

            invoice_line_obj.write( cr,
                                    uid,
                                    [ line.id ],
                                    { 'ecua_shadow_retentions' : old_shadow_retentions,
                                      'ecua_shadow_taxes'      : old_shadow_taxes,
                                    },
                                    context=context
                                  )

            # Now add the new ecua_shadow_taxes and ecua_shadow_retentions to the account.invoice.line
            invoice_line_obj.write( cr,
                                    uid,
                                    [ line.id ],
                                    { 'ecua_shadow_retentions' : shadow_retentions,
                                      'ecua_shadow_taxes'      : shadow_taxes,
                                    },
                                    context=context
                                  )

            # Let's now check the updated account.invoice.line to ensure there's a ecua_shadow_tax
            # or ecua_shadow_retention for each tax_id
            updated_line = invoice_line_obj.browse( cr, uid, line.id, context=context )

            tax_ids              = []
            shadow_tax_ids       = []
            shadow_retention_ids = []

            for tax        in updated_line.invoice_line_tax_id   : tax_ids.append(                  tax.id )
            for shadow_tax in updated_line.ecua_shadow_taxes     : shadow_tax_ids.append(    shadow_tax.id )
            for shadow_ret in updated_line.ecua_shadow_retentions: shadow_retention_ids.append( shadow_ret.id )

            for tax_id in tax_ids:
                if( ( tax_id not in shadow_tax_ids ) and ( tax_id not in shadow_retention_ids ) ):
                    raise ( _( 'Error!' ), _( "While migrating account.invoice.line: " + line.invoice_id.internal_number + " , wasn't able to generate shadow tax or shadow retention for all taxes" ) )


    ################################################################################################
    # Do all of the data migration for product.products
    ################################################################################################
    def migrate_products( self, cr, uid, context = None ):

        # Let's iterate over all product.products's and add an ecua_shadow_tax or an ecua_shadow_retention
        # for each taxes_id and supplier_taxes_id
        product_obj = self.pool.get( 'product.product' )
        product_ids = product_obj.search( cr, uid, [], context=context )
        products    = product_obj.browse( cr, uid, product_ids, context=context )

        for product in products:

            _logger.debug( "Migrating product.product:" + product.name )

            shadow_taxes_customer      = []
            shadow_retentions_customer = []
            shadow_taxes_supplier      = []
            shadow_retentions_supplier = []

            for tax in product.taxes_id:

                if( tax.categ_id == 'tax' ):
                    shadow_taxes_customer.append( ( 4, tax.id ) )
                elif( tax.categ_id == 'retention' ):
                    shadow_retentions_customer.append( ( 4, tax.id ) )
                else:
                    raise ( _( 'Error!' ), _( "While migrating product.product: " + product.name + " , wasn't able to get categ_id for: " + tax.name ) )

            for tax in product.supplier_taxes_id:

                if( tax.categ_id == 'tax' ):
                    shadow_taxes_supplier.append( ( 4, tax.id ) )
                elif( tax.categ_id == 'retention' ):
                    shadow_retentions_supplier.append( ( 4, tax.id ) )
                else:
                    raise ( _( 'Error!' ), _( "While migrating product.product: " + product.name + " , wasn't able to get categ_id for: " + tax.name ) )

            # Let's first delete the existing ecua_shadow_taxes and ecua_shadow_retentions from the product.product
            old_shadow_taxes_customer      = []
            old_shadow_retentions_customer = []
            old_shadow_taxes_supplier      = []
            old_shadow_retentions_supplier = []

            for shadow_tax in product.customer_tax_shadow: old_shadow_taxes_customer.append(      ( 3, shadow_tax.id ) )
            for shadow_ret in product.customer_ret_shadow: old_shadow_retentions_customer.append( ( 3, shadow_ret.id ) )
            for shadow_tax in product.supplier_tax_shadow: old_shadow_taxes_supplier.append(      ( 3, shadow_tax.id ) )
            for shadow_ret in product.supplier_ret_shadow: old_shadow_retentions_supplier.append( ( 3, shadow_ret.id ) )

            product_obj.write( cr,
                               uid,
                               [ product.id ],
                               { 'customer_tax_shadow' : old_shadow_taxes_customer,
                                 'customer_ret_shadow' : old_shadow_retentions_customer,
                                 'supplier_tax_shadow' : old_shadow_taxes_supplier,
                                 'supplier_ret_shadow' : old_shadow_retentions_supplier,
                               },
                               context=context
                             )

            # Now add the new ecua_shadow_taxes and ecua_shadow_retentions to the product.product
            product_obj.write( cr,
                               uid,
                               [ product.id ],
                               { 'customer_tax_shadow' : shadow_taxes_customer,
                                 'customer_ret_shadow' : shadow_retentions_customer,
                                 'supplier_tax_shadow' : shadow_taxes_supplier,
                                 'supplier_ret_shadow' : shadow_retentions_supplier,
                               },
                               context=context
                             )

            # Let's now check the updated product.product to ensure there's a ecua_shadow_tax
            # or ecua_shadow_retention for each taxes_id and supplier_taxes_id
            updated_product = product_obj.browse( cr, uid, product.id, context=context )

            tax_ids_customer              = []
            shadow_tax_ids_customer       = []
            shadow_retention_ids_customer = []

            tax_ids_supplier              = []
            shadow_tax_ids_supplier       = []
            shadow_retention_ids_supplier = []

            for tax        in updated_product.taxes_id            : tax_ids_customer.append(                     tax.id )
            for shadow_tax in updated_product.customer_tax_shadow : shadow_tax_ids_customer.append(       shadow_tax.id )
            for shadow_ret in updated_product.customer_ret_shadow : shadow_retention_ids_customer.append( shadow_ret.id )

            for tax        in updated_product.supplier_taxes_id   : tax_ids_supplier.append(                     tax.id )
            for shadow_tax in updated_product.supplier_tax_shadow : shadow_tax_ids_supplier.append(       shadow_tax.id )
            for shadow_ret in updated_product.supplier_ret_shadow : shadow_retention_ids_supplier.append( shadow_ret.id )

            for tax_id in tax_ids_customer:
                if( ( tax_id not in shadow_tax_ids_customer ) and ( tax_id not in shadow_retention_ids_customer ) ):
                    raise ( _( 'Error!' ), _( "While migrating product.product: " + product.name + " , wasn't able to generate shadow tax or shadow retention for all customer taxes" ) )

            for tax_id in tax_ids_supplier:
                if( ( tax_id not in shadow_tax_ids_supplier ) and ( tax_id not in shadow_retention_ids_supplier ) ):
                    raise ( _( 'Error!' ), _( "While migrating product.product: " + product.name + " , wasn't able to generate shadow tax or shadow retention for all supplier taxes" ) )


    ################################################################################################
    # Perform the data migration
    ################################################################################################
    def migrate( self, cr, uid, ids, context=None ):

        self.migrate_account_taxes( cr, uid, context )

        self.migrate_sale_order_lines( cr, uid, context )

        self.migrate_purchase_order_lines( cr, uid, context )

        self.migrate_account_invoice_lines( cr, uid, context )

        self.migrate_products( cr, uid, context )

        # When we're all done, return a popup window saying "Migration Complete"
        return { 'type'      : 'ir.actions.act_window',
                 'name'      : 'Migrate Taxes/Retentions',
                 'view_type' : 'form',
                 'view_mode' : 'form',
                 'res_model' : 'ecua.tax.retention.migration.complete',
                 'target'    : 'new',
               }


################################################################################################
# Migration complete wizard class definition
################################################################################################
class ecua_tax_retention_migration_complete(osv.osv_memory):

    _name = 'ecua.tax.retention.migration.complete'

    ################################################################################################
    # Okay button in "migration complete" dialogue
    ################################################################################################
    def okay( self, cr, uid, ids, context=None ):
        return
