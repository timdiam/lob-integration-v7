# -*- coding: utf-8 -*-
#################################################################################
#
# The inherited class definitions for account_invoice_line and account_tax
#
# Author:  Tim Diamond
# Company: AltaTec Ecuador
# Date:    Jan. 18th, 2014
#
#################################################################################
from openerp.osv import fields, osv, orm
import decimal_precision as dp
from lxml import etree
from openerp.osv.orm import setup_modifiers

#################################################################################
# Inherited account_invoice_line class definition
#################################################################################


class account_invoice(orm.Model):

    _inherit = 'account.invoice'

    def line_get_convert(self, cr, uid, x, part, date, context=None):
        res=super(account_invoice,self).line_get_convert(cr,uid,x,part,date,context=context)
        if 'clasificacion' in x:
            dict={'clasificacion':x['clasificacion']}
            res.update(dict)
        return res

    def _amount_all_no_tax_iva(self, cr, uid, ids, name, args, context=None):
        res = {}

        for invoice in self.browse(cr, uid, ids, context=context):
            res[invoice.id] = 0.00

            for line in invoice.invoice_line:
                if line.no_tax_iva:
                    res[invoice.id] += line.price_subtotal

        return res

    _columns={
        'base_no_grava_iva': fields.function(_amount_all_no_tax_iva, method=True, digits_compute=dp.get_precision('Account'), string='No Objeto a IVA Base',
                            store=True,
                            ),
    }



class account_invoice_line(orm.Model):

    _inherit = 'account.invoice.line'

    def move_line_get_item(self, cr, uid, line, context=None):
        res=super(account_invoice_line,self).move_line_get_item(cr,uid,line,context=context)
        dict={'clasificacion':line.clasificacion}
        res.update(dict)
        return res

    def line_get_convert(self, cr, uid, x, part, date, context=None):
        res=super(account_invoice_line,self).move_line_get_item(cr,uid,line,context=context)


    # def fields_view_get(self, cr, uid, view_id=None, view_type=False, context=None, toolbar=False, submenu=False):
    #         """
    #             Add domain to invoice line form view so we dont have to make a separate view
    #             field because the dynamic domain is not allowed on such widget
    #         """
    #         if not context: context = {}
    #         res = super(account_invoice_line, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=submenu)
    #         doc = etree.XML(res['arch'])
    #         nodes = doc.xpath("//field[@name='ecua_shadow_taxes']")
    #         if context.get('dynamic_domain_purchase', False) :
    #             for node in nodes:
    #                 node.set('domain',"[('parent_id','=',False),('categ_id','=','tax'),('type_tax_use','in',['purchase','all']),('active','=',True)]")
    #             res['arch'] = etree.tostring(doc)
    #         elif context.get('dynamic_domain_sale', False):
    #             for node in nodes:
    #                 node.set('domain',"[('parent_id','=',False),('categ_id','=','tax'),('type_tax_use','in',['sale','all']),('active','=',True)]")
    #             res['arch'] = etree.tostring(doc)
    #
    #         doc2 = etree.XML(res['arch'])
    #         nodes = doc2.xpath("//field[@name='ecua_shadow_retentions']")
    #         if context.get('dynamic_domain_purchase', False) :
    #             for node in nodes:
    #                 node.set('domain',"[('parent_id','=',False),('categ_id','=','retention'),('type_tax_use','in',['purchase','all']),('active','=',True)]")
    #             res['arch'] = etree.tostring(doc2)
    #         elif context.get('dynamic_domain_sale', False):
    #             for node in nodes:
    #                 node.set('invisible',"0")
    #                 setup_modifiers(node, res['fields']['ecua_shadow_retentions'])
    #                 node.set('domain',"[('parent_id','=',False),('categ_id','=','retention'),('type_tax_use','in',['sale','all']),('active','=',True)]")
    #             res['arch'] = etree.tostring(doc2)
    #         return res


    #################################################################################
    # Product_id change
    #################################################################################
    def product_id_change(self, cr, uid, ids, product, uom_id, qty=0, name='', type='out_invoice', partner_id=False, fposition_id=False, price_unit=False, currency_id=False, context={}, company_id=None):
        
        res_prod = super(account_invoice_line, self).product_id_change(cr, uid, ids, product, uom_id, qty, name, type, partner_id, fposition_id, price_unit, currency_id=currency_id, context=context, company_id=company_id)
        
        if product:
            product_obj = self.pool.get('product.product').browse(cr,uid,product,context=context)
            
            if product_obj:
                ret_list=[]
                tax_list=[]
                
                if type:
                    if type == 'in_invoice':
                        for ret in product_obj.supplier_ret_shadow:
                            ret_list.append(ret.id)
                        for tax in product_obj.supplier_tax_shadow:
                            tax_list.append(tax.id)
                        
                    elif type == 'out_invoice':
                        for ret in product_obj.customer_ret_shadow:
                            ret_list.append(ret.id)
                        for tax in product_obj.customer_tax_shadow:
                            tax_list.append(tax.id)

                    elif type == 'out_refund':
                        for ret in product_obj.customer_ret_shadow:
                            ret_list.append(ret.id)
                        for tax in product_obj.customer_tax_shadow:
                            tax_list.append(tax.id)   

                    elif type == 'in_refund':
                        for ret in product_obj.supplier_ret_shadow:
                            ret_list.append(ret.id)
                        for tax in product_obj.supplier_tax_shadow:
                            tax_list.append(tax.id) 
                    elif type == 'hr_advance':
                        pass
                    else:
                        raise (_('Error!'), _('Not implemented.'))
                       
                res_prod['value'].update({'ecua_shadow_taxes': tax_list})
                res_prod['value'].update({'ecua_shadow_retentions':ret_list})

        return res_prod


    #################################################################################
    # Create override
    #################################################################################
    def create(self, cr, uid, values, context=None):    
        
        line_id = super(account_invoice_line, self).create(cr, uid, values, context=context)
        
        line = self.browse(cr,uid,line_id,context=context)

        # Delete all taxes from the new account.invoice.line
        if line.invoice_line_tax_id:
            del_list = []
            for i_tax in line.invoice_line_tax_id:
                del_list.append((3,i_tax.id))

            super(account_invoice_line, self).write(cr,uid,[line.id],{'invoice_line_tax_id':del_list})

        
        if (not line.ecua_shadow_taxes) and (not line.ecua_shadow_retentions):
            return line_id
        else:
            tax_list = []
            
            if line.ecua_shadow_taxes:
                
                for tax in line.ecua_shadow_taxes:
                    tax_list.append( (4, tax.id) )
                    
            if line.ecua_shadow_retentions:
                
                for tax in line.ecua_shadow_retentions:
                    tax_list.append((4,tax.id))
                    
            super(account_invoice_line, self).write(cr,uid,[line_id],{'invoice_line_tax_id':tax_list},context=context)
    
            return line_id


    #################################################################################
    # Write override
    #################################################################################
    def write(self, cr, uid, ids, values, context = {}):

        super(account_invoice_line, self).write(cr, uid, ids, values, context=context)
        
        for line in self.browse(cr,uid,ids,context=context):
                
            if line.invoice_line_tax_id:
                del_list = []
                for i_tax in line.invoice_line_tax_id:
                    del_list.append((3,i_tax.id))

                super(account_invoice_line, self).write(cr,uid,[line.id],{'invoice_line_tax_id':del_list})
            
            if (not line.ecua_shadow_taxes) and (not line.ecua_shadow_retentions):
                continue
            
            else:
                tax_list = []
                
                if line.ecua_shadow_taxes:
                    
                    for tax in line.ecua_shadow_taxes:
                        tax_list.append(tax.id)
                        
                if line.ecua_shadow_retentions:
                    
                    for tax in line.ecua_shadow_retentions:
                        tax_list.append(tax.id)
                        
                super(account_invoice_line, self).write(cr,uid,[line.id],{'invoice_line_tax_id':[(6,0,tax_list)]},context=context)
        return

    def set_domains(self, cr, uid, ids, parent_type, no_tax_iva, context=None):
        if no_tax_iva:
            return {
                        'domain':
                                {
                                    'ecua_shadow_taxes':
                                            [('id','=',-1)],
                                    'ecua_shadow_retentions':
                                            [('parent_id','=',False),
                                             ('categ_id','=','retention'),
                                             ('type_tax_use','in',['purchase','all']),
                                             ('active','=',True)],
                                },
                        'value':
                                {
                                   'ecua_shadow_taxes': [],
                                }
                    }

        elif parent_type in ['in_invoice','in_refund']:
            return {
                        'domain':
                                {
                                    "ecua_shadow_taxes":
                                            [('parent_id','=',False),
                                            ('categ_id','=','tax'),
                                            ('type_tax_use','in',['purchase','all']),
                                            ('active','=',True)],
                                    'ecua_shadow_retentions':
                                            [('parent_id','=',False),
                                             ('categ_id','=','retention'),
                                             ('type_tax_use','in',['purchase','all']),
                                             ('active','=',True)],
                                }
                    }

        elif parent_type in ['out_invoice','out_refund']:
            return {
                        'domain':
                            {
                                    'ecua_shadow_taxes':
                                            [('parent_id','=',False),
                                            ('categ_id','=','tax'),
                                            ('type_tax_use','in',['sale','all']),
                                            ('active','=',True)],
                                    'ecua_shadow_retentions':
                                            [('parent_id','=',False),
                                             ('categ_id','=','retention'),
                                             ('type_tax_use','in',['sale','all']),
                                             ('active','=',True)],
                            }
                    }
        else:
            return {}


    _columns = { 'ecua_shadow_taxes':fields.many2many('account.tax', 'tax_shadow_rel', 'tax_id', 'shadow_id', 'Impuesto'),
                 'ecua_shadow_retentions':fields.many2many('account.tax', 'ret_shadow_rel', 'ret_id', 'shadow_id', 'Ret.' ),
                  'clasificacion':fields.selection(
                     (('gastos_ded','Gastos Deducibles'),
                      ('gastos_no_ded','Gastos No Deducibles'),
                      ('gastos_def_nid','Gastos No Deducibles NIIF')),
                     'Clasificacion'),
                 'no_tax_iva': fields.boolean('No Objeto IVA', default=False),
    }


#################################################################################
# Inherited account_tax class definition
#################################################################################
class account_tax(osv.osv):
            
    _inherit = "account.tax"

    #################################################################################
    # Column definitions
    #################################################################################
    _columns = { 'categ_id' : fields.selection(string='Categorizacion',selection=[('tax','Impuesto'),('retention','Retencion')]),
               }
    
