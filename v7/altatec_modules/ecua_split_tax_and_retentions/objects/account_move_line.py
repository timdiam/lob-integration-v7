# -*- coding: utf-8 -*-
#################################################################################
#
# The inherited class definitions for account_invoice_line and account_tax
#
# Author:  Tim Diamond
# Company: AltaTec Ecuador
# Date:    Jan. 18th, 2014
#
#################################################################################
from openerp.osv import fields, osv, orm
from lxml import etree
from openerp.osv.orm import setup_modifiers

#################################################################################
# Inherited account_invoice_line class definition
#################################################################################
class account_move(osv.osv):

    _inherit = 'account.move'
    _columns= {
        'clasificacion':fields.selection(
                     (('gastos_ded','Gastos Deducibles'),
                      ('gastos_no_ded','Gastos No Deducibles'),
                      ('gastos_def_nid','Gastos No Deducibles NIIF')),
                     'Clasificacion')
    }

class account_move_line(osv.osv):
    _inherit = 'account.move.line'
    _columns= {
        'clasificacion':fields.selection(
                     (('gastos_ded','Gastos Deducibles'),
                      ('gastos_no_ded','Gastos No Deducibles'),
                      ('gastos_def_nid','Gastos No Deducibles NIIF')),
                     'Clasificacion')
    }

