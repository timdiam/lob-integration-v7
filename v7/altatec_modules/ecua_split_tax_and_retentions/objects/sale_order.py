# -*- coding: utf-8 -*-
#################################################################################
#
# The inherited class definitions for sales_order_line
#
# Author:  Tim Diamond
# Company: AltaTec Ecuador
# Date:    Jan. 18th, 2014
#
#################################################################################
from openerp.osv import fields, osv, orm


#################################################################################
# Inherited purchase_order_line class definition
#################################################################################
class sale_order_line(orm.Model):

    _inherit = 'sale.order.line'

    #Override to inject the shadow taxes and retentions...
    def _prepare_order_line_invoice_line(self, cr, uid, line, account_id=False, context=None):
        res = super(sale_order_line, self)._prepare_order_line_invoice_line(cr, uid, line, account_id=False, context=None)
        res.update(     {'ecua_shadow_taxes'     : [(6, 0, [x.id for x in line.ecua_shadow_taxes])],
                        'ecua_shadow_retentions' : [(6, 0, [x.id for x in line.ecua_shadow_retentions])],
                        } )

        return res

    #################################################################################
    # product_id_change_override
    #################################################################################
    def product_id_change( self, cr, uid, ids, pricelist, product, qty=0,
                           uom=False, qty_uos=0, uos=False, name='', partner_id=False,
                           lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False, flag=False, context=None
                         ):

        res_prod = super( sale_order_line, self ).product_id_change( cr, uid, ids, pricelist, product, qty, uom, qty_uos, uos, name,
                                                                     partner_id, lang, update_tax, date_order, packaging, fiscal_position, flag, context
                                                                   )

        if product:

            product_obj = self.pool.get('product.product').browse(cr,uid,product,context=context)

            if product_obj:

                ret_list=[]
                tax_list=[]

                for ret in product_obj.customer_ret_shadow:
                    ret_list.append(ret.id)

                for tax in product_obj.customer_tax_shadow:
                    tax_list.append(tax.id)

                res_prod[ 'value' ].update( { 'ecua_shadow_taxes'      : tax_list } )
                res_prod[ 'value' ].update( { 'ecua_shadow_retentions' : ret_list } )

        return res_prod


    #################################################################################
    # Create override
    #################################################################################
    def create( self, cr, uid, values, context=None ):

        line_id = super( sale_order_line, self ).create( cr, uid, values, context=context )

        line = self.browse( cr, uid, line_id, context=context )

        if line.tax_id:

            del_list = []

            for i_tax in line.tax_id:
                del_list.append((3,i_tax.id))

            super( sale_order_line, self ).write( cr, uid, [ line.id ], { 'tax_id' : del_list } )


        if ( not line.ecua_shadow_taxes ) and ( not line.ecua_shadow_retentions ):
            return line_id
        else:
            tax_list = []

            if line.ecua_shadow_taxes:

                for tax in line.ecua_shadow_taxes:
                    tax_list.append( ( 4, tax.id ) )

            if line.ecua_shadow_retentions:

                for tax in line.ecua_shadow_retentions:
                    tax_list.append( ( 4, tax.id ) )

            super( sale_order_line, self ).write( cr, uid, [ line_id ], { 'tax_id' : tax_list }, context=context )

            return line_id


    #################################################################################
    # Write override
    #################################################################################
    def write( self, cr, uid, ids, values, context = {} ):

        super( sale_order_line, self ).write( cr, uid, ids, values, context=context )

        for line in self.browse( cr, uid, ids, context=context ):

            if line.tax_id:

                del_list = []

                for i_tax in line.tax_id:
                    del_list.append( ( 3, i_tax.id ) )

                super( sale_order_line, self ).write( cr, uid, [ line.id ], { 'tax_id' : del_list } )

            if( not line.ecua_shadow_taxes ) and ( not line.ecua_shadow_retentions ):
                continue

            else:
                tax_list = []

                if line.ecua_shadow_taxes:

                    for tax in line.ecua_shadow_taxes:
                        tax_list.append( tax.id )

                if line.ecua_shadow_retentions:

                    for tax in line.ecua_shadow_retentions:
                        tax_list.append( tax.id )

                super( sale_order_line, self).write( cr, uid, [ line.id ], { 'tax_id' : [ ( 6, 0, tax_list ) ] }, context=context )


    #################################################################################
    # Column definition
    #################################################################################
    _columns = { 'ecua_shadow_taxes'      : fields.many2many( 'account.tax', 'tax_shadow_rel_so', 'tax_id', 'shadow_id', 'Impuesto' ),
                 'ecua_shadow_retentions' : fields.many2many( 'account.tax', 'ret_shadow_rel_so', 'ret_id', 'shadow_id', 'Ret.' ),
               }