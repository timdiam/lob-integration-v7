# -*- coding: utf-8 -*-
#################################################################################
#
# The inherited class definitions for purchase_order and purchase_order_line
#
# Author:  Tim Diamond
# Company: AltaTec Ecuador
# Date:    Jan. 18th, 2014
#
#################################################################################
from openerp.osv import fields, osv, orm


#################################################################################
# Inherited purchase_order class definition
#################################################################################
class purchase_order(osv.osv):

    _inherit = 'purchase.order'

    #################################################################################
    # _prepare_inv_line() override
    #################################################################################
    def _prepare_inv_line(self, cr, uid, account_id, order_line, context=None):

        res = super(purchase_order, self)._prepare_inv_line(cr, uid, account_id, order_line, context=context)
        res['project_id']  = order_line.project_id.id
        res['cost_center'] = order_line.cost_center.id

        if order_line.ecua_shadow_taxes:
            tax_list=[]
            for tax in order_line.ecua_shadow_taxes:
                    tax_list.append( (4, tax.id) )
            res['ecua_shadow_taxes'] = tax_list

        if order_line.ecua_shadow_retentions:
            tax_list=[]
            for tax in order_line.ecua_shadow_retentions:
                tax_list.append((4,tax.id))
            res['ecua_shadow_retentions'] = tax_list

        return res


#################################################################################
# Inherited purchase_order_line class definition
#################################################################################
class purchase_order_line(orm.Model):
    
    _inherit = 'purchase.order.line'

    #################################################################################
    # onchange_product_id
    #################################################################################
    def onchange_product_id( self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
                             partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
                             name=False, price_unit=False, context=None
                           ):

        res_prod = super(purchase_order_line, self).product_id_change( cr, uid, ids, pricelist_id, product_id, qty, uom_id,
                                                                       partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
                                                                       name=False, price_unit=False, context=None
                                                                     )
        
        if product_id:

            product_obj = self.pool.get('product.product').browse(cr,uid,product_id,context=context)
            
            if product_obj:

                ret_list=[]
                tax_list=[]

                for ret in product_obj.supplier_ret_shadow:
                    ret_list.append(ret.id)

                for tax in product_obj.supplier_tax_shadow:
                    tax_list.append(tax.id)

                res_prod[ 'value' ].update( { 'ecua_shadow_taxes'      : tax_list } )
                res_prod[ 'value' ].update( { 'ecua_shadow_retentions' : ret_list } )

        return res_prod


    #################################################################################
    # Create override
    #################################################################################
    def create(self, cr, uid, values, context=None):    
        
        line_id = super(purchase_order_line, self).create(cr, uid, values, context=context)
        
        line = self.browse(cr,uid,line_id,context=context)
            
        if line.taxes_id:

            del_list = []

            for i_tax in line.taxes_id:
                del_list.append((3,i_tax.id))

            super(purchase_order_line, self).write(cr,uid,[line.id],{'taxes_id':del_list})

        
        if (not line.ecua_shadow_taxes) and (not line.ecua_shadow_retentions):

            return line_id

        else:

            tax_list = []
            
            if line.ecua_shadow_taxes:
                
                for tax in line.ecua_shadow_taxes:
                    tax_list.append( (4, tax.id) )
                    
            if line.ecua_shadow_retentions:
                
                for tax in line.ecua_shadow_retentions:
                    tax_list.append((4,tax.id))
                    
            super(purchase_order_line, self).write(cr,uid,[line_id],{'taxes_id':tax_list},context=context)
    
            return line_id


    #################################################################################
    # Write override
    #################################################################################
    def write(self, cr, uid, ids, values, context = {}):

        super(purchase_order_line, self).write(cr, uid, ids, values, context=context)
        
        for line in self.browse(cr,uid,ids,context=context):
                
            if line.taxes_id:
                del_list = []
                for i_tax in line.taxes_id:
                    del_list.append((3,i_tax.id))

                super(purchase_order_line, self).write(cr,uid,[line.id],{'taxes_id':del_list})
            
            if (not line.ecua_shadow_taxes) and (not line.ecua_shadow_retentions):
                continue
            
            else:
                tax_list = []
                
                if line.ecua_shadow_taxes:
                    
                    for tax in line.ecua_shadow_taxes:
                        tax_list.append(tax.id)
                        
                if line.ecua_shadow_retentions:
                    
                    for tax in line.ecua_shadow_retentions:
                        tax_list.append(tax.id)
                        
                super(purchase_order_line, self).write(cr,uid,[line.id],{'taxes_id':[(6,0,tax_list)]},context=context)
        return


    #################################################################################
    # Column definition
    #################################################################################
    _columns = { 'ecua_shadow_taxes'      : fields.many2many('account.tax', 'tax_shadow_rel_po', 'tax_id', 'shadow_id', 'Impuesto' ),
                 'ecua_shadow_retentions' : fields.many2many('account.tax', 'ret_shadow_rel_po', 'ret_id', 'shadow_id', 'Ret.' ),
               }


#################################################################################
# Inherited purchase.requisition.partner class
#################################################################################
class purchase_requisition( orm.Model ):

    _inherit = 'purchase.requisition'

    #################################################################################
    # Inherited make_purchase_order() method
    #################################################################################
    def make_purchase_order(self, cr, uid, ids, partner_id, context=None):

        # Create the purchase order
        res = super( purchase_requisition, self ).make_purchase_order( cr, uid, ids, partner_id, context )

        # Edit the purchase.order.lines that were created, and write the correct project and cost center to them
        for requisition_id in res:

            purchase_order = self.pool.get( 'purchase.order' ).browse( cr, uid, res[ requisition_id ], context )

            for line in purchase_order.order_line:

                taxes      = []
                retentions = []

                for tax in line.product_id.supplier_tax_shadow:
                    taxes.append( (4,tax.id) )
                for ret in line.product_id.supplier_ret_shadow:
                    retentions.append( (4,ret.id) )

                self.pool.get( 'purchase.order.line' ).write( cr, uid, [ line.id ],
                                                              { 'ecua_shadow_taxes'      : taxes,
                                                                'ecua_shadow_retentions' : retentions,
                                                              }
                                                            )

        return res
