# -*- coding: utf-8 -*-
#################################################################################
#
# The inherited class definitions for product_product and product_template
#
# Author:  Tim Diamond
# Company: AltaTec Ecuador
# Date:    Jan. 18th, 2014
#
#################################################################################
from openerp.osv import fields, osv, orm


#################################################################################
# Inherited product_product class definition
#################################################################################
class product_product(osv.osv):

    _inherit = "product.product"

    #################################################################################
    # Create override
    #################################################################################
    def create(self, cr, uid, values, context=None):    
        
        prod_id = super(product_product, self).create(cr, uid, values, context=context)
        
        prod = self.browse(cr,uid,prod_id,context=context)
            
        if prod.supplier_taxes_id:
            del_list = []
            for i_tax in prod.supplier_taxes_id:
                del_list.append((3,i_tax.id))

            super(product_product, self).write(cr,uid,[prod.id],{'supplier_taxes_id':del_list})
       
        if prod.taxes_id:
            del_list = []
            for i_tax in prod.taxes_id:
                del_list.append((3,i_tax.id))

            super(product_product, self).write(cr,uid,[prod.id],{'taxes_id':del_list})

        tax_list = []
        
        if prod.customer_tax_shadow:
            for tax in prod.customer_tax_shadow:
                tax_list.append( (4, tax.id) )
                
        if prod.customer_ret_shadow:
            for tax in prod.customer_ret_shadow:
                tax_list.append((4,tax.id))
                
        super(product_product, self).write(cr,uid,[prod_id],{'taxes_id':tax_list},context=context)

        sup_tax_list = []
        
        if prod.supplier_tax_shadow:
            for tax in prod.supplier_tax_shadow:
                sup_tax_list.append( (4, tax.id) )
                
        if prod.supplier_ret_shadow:
            for tax in prod.supplier_ret_shadow:
                sup_tax_list.append((4,tax.id))
                
        super(product_product, self).write(cr,uid,[prod_id],{'supplier_taxes_id':sup_tax_list},context=context)

        return prod_id


    #################################################################################
    # Write override
    #################################################################################
    def write(self, cr, uid, ids, values, context = {}):

        super(product_product, self).write(cr, uid, ids, values, context=context)
        
        for prod in self.browse(cr,uid,ids,context=context):
                
            if prod.taxes_id:
                del_list = []
                for i_tax in prod.taxes_id:
                    del_list.append((3,i_tax.id))

                super(product_product, self).write(cr,uid,[prod.id],{'taxes_id':del_list})

            if prod.supplier_taxes_id:
                del_list = []
                for i_tax in prod.supplier_taxes_id:
                    del_list.append((3,i_tax.id))

                super(product_product, self).write(cr,uid,[prod.id],{'supplier_taxes_id':del_list})

            tax_list = []
            
            if prod.customer_tax_shadow:
                for tax in prod.customer_tax_shadow:
                    tax_list.append( (4, tax.id) )
                    
            if prod.customer_ret_shadow:
                for tax in prod.customer_ret_shadow:
                    tax_list.append((4,tax.id))
                    
            super(product_product, self).write(cr,uid,[prod.id],{'taxes_id':tax_list},context=context)
            
            sup_tax_list = []
            
            if prod.supplier_tax_shadow:
                for tax in prod.supplier_tax_shadow:
                    sup_tax_list.append( (4, tax.id) )
                    
            if prod.supplier_ret_shadow:
                for tax in prod.supplier_ret_shadow:
                    sup_tax_list.append((4,tax.id))
                    
            super(product_product, self).write(cr,uid,[prod.id],{'supplier_taxes_id':sup_tax_list},context=context)
        return True


#################################################################################
# Inherited product_template class definition
#################################################################################
class product_template(osv.osv):

    _inherit = "product.template"

    #################################################################################
    # Column definitions
    #################################################################################
    _columns = { 'customer_tax_shadow' : fields.many2many('account.tax', 'product_cust_tax_shadow_rel', 'prod_id', 'tax_id', 'Impuestos Cliente' ),
                 'customer_ret_shadow' : fields.many2many('account.tax', 'product_cust_ret_shadow_rel', 'prod_id', 'ret_id', 'Retenciones Cliente' ),
                 'supplier_tax_shadow' : fields.many2many('account.tax', 'product_supplier_tax_shadow_rel', 'prod_id', 'tax_id', 'Impuestos Proveedor' ),
                 'supplier_ret_shadow' : fields.many2many('account.tax', 'product_supplier_ret_shadow_rel', 'prod_id', 'ret_id', 'Retenciones Proveedor', ),
                 'mas_notas'           : fields.text(string="Mas Notas"),
               }
