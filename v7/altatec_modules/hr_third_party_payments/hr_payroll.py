# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Authors: TRESCloud
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from osv import osv, fields
import time

class contrib_register(osv.Model):
    
    _inherit = 'hr.contribution.register'
    
    _columns = {
        'debitor_partner_id' : fields.many2one('res.partner', 'Debitor Partner'),
        'creditor_partner_id' : fields.many2one('res.partner', 'Creditor Partner'),
        'type_id': fields.many2one('hr.contract.type', "Contract Type", required=False),
        'reminder':fields.boolean('Reminder'),
        'debit_acc_manu_di':fields.many2one('account.account', 'Debit Account Direct Manufacturing Cost'),
        'debit_acc_manu_in':fields.many2one('account.account', 'Debit Account Indirect Manufacturing Cost'),
        'debit_acc_administrative':fields.many2one('account.account', 'Debit Account for Administrative'),
        'debit_acc_sales':fields.many2one('account.account', 'Debit Account for Sales'),     
        'credit_acc_manu_di':fields.many2one('account.account', 'Credit Account Direct Manufacturing Cost'),
        'credit_acc_manu_in':fields.many2one('account.account', 'Credit Account Indirect Manufacturing Cost'),
        'credit_acc_administrative':fields.many2one('account.account', 'Credit Account for Administrative'),
        'credit_acc_sales':fields.many2one('account.account', 'Credit Account for Sales'),           
        'category_id':fields.many2one('hr.salary.rule.category', 'Category', required=True),
        'account_tax_id':fields.many2one('account.tax.code', 'Tax Code'),
    }

contrib_register()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: