from openerp.osv import fields, orm


class product_product(orm.Model):

    _inherit = 'product.product'

    _columns = {
        'cost_center': fields.many2one('account.analytic.account', string='Centro de Costo'),
    }