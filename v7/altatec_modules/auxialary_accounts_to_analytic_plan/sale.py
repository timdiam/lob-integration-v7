# -*- coding: utf-8 -*-
#################################################################################
#
#
# objects (e.g. invoices).
#
# Author:  Tim Diamond
# Company: AltaTec Ecuador
# Date:    Jan. 18th, 2014
#
#################################################################################
from openerp.osv import fields, osv, orm

class sale_order_line(orm.Model):

    _inherit = 'sale.order.line'

    #Override to inject the shadow taxes and retentions...
    def _prepare_order_line_invoice_line(self, cr, uid, line, account_id=False, context=None):
        res = super(sale_order_line, self)._prepare_order_line_invoice_line(cr, uid, line, account_id=False, context=None)

        if 'analytics_id' in res:
            del res['analytics_id']
        if 'account_analytic_id' in res:
            del res['account_analytic_id']

        res.update(     {
                        'project_id'   : line.project_id.id if line.project_id else False,
                        'cost_center'  : line.cost_center.id if line.cost_center else False,
                        }
                    )

        return res


    #################################################################################
    # Inherited create() method
    #################################################################################
    def create(self, cr, uid, values, context=None):

        so_id = super(sale_order_line, self).create(cr, uid, values, context=context)

        so_line = self.browse(cr,uid,so_id,context=context)

        account_plan_instance_obj = self.pool.get('account.analytic.plan.instance')
        account_plan_instance_line_obj = self.pool.get('account.analytic.plan.instance.line')

        if (not so_line.project_id) and (not so_line.cost_center):
            if ('analytics_id' in values) or ('account_analytic_id' in values):
                #raise osv.except_osv( ( 'Error' ), ( 'Consulta con altatec.  No deberia ingresar ese dato' ) )
                pass
            return so_id

        name=""

        if so_line.project_id:
            name=name+so_line.project_id.name
        if so_line.cost_center:
            name=name+ " / "+ so_line.cost_center.name


        new_instance_id = account_plan_instance_obj.create(cr,uid,{
                                                                   'name': name,
                                                                   'code': 'PCC',

                                                                   }, context=context)

        if so_line.project_id:
            account_plan_instance_line_obj.create(cr,uid,{
                                                 'plan_id':new_instance_id,
                                                 'analytic_account_id':so_line.project_id.id,
                                                 'type':'project',
                                                 }, context=context)

        if so_line.cost_center:
            account_plan_instance_line_obj.create(cr,uid,{
                                                 'plan_id':new_instance_id,
                                                 'analytic_account_id':so_line.cost_center.id,
                                                 'type':'cost_center',
                                                 }, context=context)

        super(sale_order_line, self).write(cr, uid, [so_line.id], {'analytics_id':new_instance_id}, context=context)

        return so_id


    #################################################################################
    # Inherited write() method
    #################################################################################
    def write(self, cr, uid, ids, values, context = {}):

        res = super(sale_order_line, self).write(cr, uid, ids, values, context=context)

        if ('analytics_id' in values) and (values['analytics_id'] != False):
            raise osv.except_osv( ( 'Error' ), ( 'Consulta con altatec.  No deberia ingresar ese dato' ) )

        if ('account_analytic_id' in values) and (values['account_analytic_id'] != False):
            raise osv.except_osv( ( 'Error' ), ( 'Consulta con altatec.  No deberia ingresar ese dato' ) )


        account_plan_instance_obj = self.pool.get('account.analytic.plan.instance')
        account_plan_instance_line_obj = self.pool.get('account.analytic.plan.instance.line')

        for line in self.browse(cr,uid,ids,context=context):

            if (not line.project_id) and (not line.cost_center):
                super(sale_order_line, self).write(cr, uid, [line.id], {'analytics_id':False}, context=context)
                continue

            name=""

            if line.project_id:
                name=name+line.project_id.name
            if line.cost_center:
                name=name+ " / "+ line.cost_center.name


            new_instance_id = account_plan_instance_obj.create(cr,uid,{
                                                                       'name': name,
                                                                       'code': 'PCC',

                                                                       }, context=context)

            if line.project_id:
                account_plan_instance_line_obj.create(cr,uid,{
                                                     'plan_id':new_instance_id,
                                                     'analytic_account_id':line.project_id.id,
                                                     'type':'project',
                                                     }, context=context)

            if line.cost_center:
                account_plan_instance_line_obj.create(cr,uid,{
                                                     'plan_id':new_instance_id,
                                                     'analytic_account_id':line.cost_center.id,
                                                     'type':'cost_center',
                                                     }, context=context)


            super(sale_order_line, self).write(cr, uid, [line.id], {'analytics_id':new_instance_id}, context=context)

        return res

    #################################################################################
    # Inherited onchange method to update cost_center value
    #################################################################################
    def product_id_change(self, cr, uid, ids, pricelist, product, qty=0, uom=False, qty_uos=0, uos=False, name='', partner_id=False,lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False, flag=False, context=None):

        res_prod = super(sale_order_line,self).product_id_change(cr, uid, ids, pricelist, product, qty, uom, qty_uos, uos, name, partner_id, lang, update_tax, date_order, packaging, fiscal_position, True, context)

        if product:
            product_obj = self.pool.get('product.product').browse(cr, uid, product, context=context)

            if product_obj:
                res_prod['value'].update({'cost_center': product_obj.cost_center.id})

        return res_prod

    #################################################################################
    # Column definitions
    #################################################################################
    _columns = { 'project_id'  : fields.many2one('account.analytic.account', string='Proyecto'),
                 'cost_center' : fields.many2one('account.analytic.account', string='Centro de Costo'),
                }

# class sale_order(osv.osv):
#     _name='sale.order'
#     _inherit='sale.order'
#
#     def _prepare_invoice(self, cr, uid, order, lines, context=None):
#         so_lin
#         return invoice_vals
#
# sale_order()

