{
    'name': 'Auxialary accounts to analytic plans',
    'version': '1.0',
    'description': """
        This module adds two fields two solicitud and purchase orders lines and sales orders lines.  One will store the project and one will store the cost center.  Upon confirming the purchase/sales order, an account analytic plan is created for each line representing 100% of each auxilary account.
    """,
    'author': 'Tim Diamond',
    'website': 'www.altatececuador.com',
    "depends" : ['account_analytic_plans','purchase_analytic_plans','sale_analytic_plans','invoice_line_form_view', 'purchase_requisition', 'account','hr_contract','hr_payroll_account'],
    "data" : [ 'purchase.xml','account_invoice_view.xml','sale_view.xml','hr_contract_view.xml', 'product_view.xml'],
    "installable": True,
    "auto_install": False
}