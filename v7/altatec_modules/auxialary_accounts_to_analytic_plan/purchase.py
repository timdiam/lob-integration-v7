# -*- coding: utf-8 -*-
#################################################################################
#
#
# objects (e.g. invoices).
#
# Author:  Tim Diamond
# Company: AltaTec Ecuador
# Date:    Jan. 18th, 2014
#
#################################################################################
from openerp.osv import fields, osv, orm


#################################################################################
# Inherited purchase.requisition.partner class
#################################################################################
class purchase_requisition( orm.Model ):

    _inherit = 'purchase.requisition'

    #################################################################################
    # Inherited make_purchase_order() method
    #################################################################################
    def make_purchase_order(self, cr, uid, ids, partner_id, context=None):

        product_dict = {}

        # Assert that there are no duplicate products in this requisition
        for requisition in self.browse( cr, uid, ids, context=context ):

            product_ids = []

            for line in requisition.line_ids:

                if line.product_id.id in product_ids:
                    raise osv.except_osv( 'Error!', 'Un producto no puede aparecer mas de una vez en una solicitud' )

                product_ids.append( line.product_id.id )

                product_dict[ line.product_id.id ] = { 'project_id' : line.project_id.id, 'cost_center' : line.cost_center.id }

        # Create the purchase order
        res = super( purchase_requisition, self ).make_purchase_order( cr, uid, ids, partner_id, context )

        # Edit the purchase.order.lines that were created, and write the correct project and cost center to them
        for requisition_id in res:

            purchase_order = self.pool.get( 'purchase.order' ).browse( cr, uid, res[ requisition_id ], context )

            for line in purchase_order.order_line:
                self.pool.get( 'purchase.order.line' ).write( cr, uid, [ line.id ],
                                                              { 'project_id'  : product_dict[ line.product_id.id ][  'project_id' ],
                                                                'cost_center' : product_dict[ line.product_id.id ][ 'cost_center' ],
                                                              }
                                                            )

        return res


#################################################################################
# Inherited purchase.requisition.line class
#################################################################################
class purchase_requisition_line( orm.Model ):

    _inherit = 'purchase.requisition.line'

    _columns = { 'project_id'  : fields.many2one('account.analytic.account', string='Proyecto'),
                 'cost_center' : fields.many2one('account.analytic.account', string='Centro de Costo'),
               }


#################################################################################
# Inherited purchase.order.line class
#################################################################################
class purchase_order_line(orm.Model):

    _inherit = 'purchase.order.line'

    #################################################################################
    # Inherited create() method
    #################################################################################
    def create(self, cr, uid, values, context=None):

        po_id = super(purchase_order_line, self).create(cr, uid, values, context=context)

        po_line = self.browse(cr,uid,po_id,context=context)

        account_plan_instance_obj = self.pool.get('account.analytic.plan.instance')
        account_plan_instance_line_obj = self.pool.get('account.analytic.plan.instance.line')

        if (not po_line.project_id) and (not po_line.cost_center):
            if ('analytics_id' in values) or ('account_analytic_id' in values):
                #raise osv.except_osv( ( 'Error' ), ( 'Consulta con altatec.  No deberia ingresar ese dato' ) )
                pass
            return po_id

        name=""

        if po_line.project_id:
            name=name+po_line.project_id.name
        if po_line.cost_center:
            name=name+ " / "+ po_line.cost_center.name


        new_instance_id = account_plan_instance_obj.create(cr,uid,{
                                                                   'name': name,
                                                                   'code': 'PCC',

                                                                   }, context=context)

        if po_line.project_id:
            account_plan_instance_line_obj.create(cr,uid,{
                                                 'plan_id':new_instance_id,
                                                 'analytic_account_id':po_line.project_id.id,
                                                 'type':'project',
                                                 }, context=context)

        if po_line.cost_center:
            account_plan_instance_line_obj.create(cr,uid,{
                                                 'plan_id':new_instance_id,
                                                 'analytic_account_id':po_line.cost_center.id,
                                                 'type':'cost_center',
                                                 }, context=context)

        super(purchase_order_line, self).write(cr, uid, [po_line.id], {'analytics_id':new_instance_id}, context=context)

        return po_id


    #################################################################################
    # Inherited write() method
    #################################################################################
    def write(self, cr, uid, ids, values, context = {}):

        res = super(purchase_order_line, self).write(cr, uid, ids, values, context=context)

        if ('analytics_id' in values) and (values['analytics_id'] != False):
            raise osv.except_osv( ( 'Error' ), ( 'Consulta con altatec.  No deberia ingresar ese dato' ) )

        if ('account_analytic_id' in values) and (values['account_analytic_id'] != False):
            raise osv.except_osv( ( 'Error' ), ( 'Consulta con altatec.  No deberia ingresar ese dato' ) )

        account_plan_instance_obj = self.pool.get('account.analytic.plan.instance')
        account_plan_instance_line_obj = self.pool.get('account.analytic.plan.instance.line')

        for line in self.browse(cr,uid,ids,context=context):

            if (not line.project_id) and (not line.cost_center):
                super(purchase_order_line, self).write(cr, uid, [line.id], {'analytics_id':False}, context=context)
                continue

            name=""

            if line.project_id:
                name=name+line.project_id.name
            if line.cost_center:
                name=name+ " / "+ line.cost_center.name


            new_instance_id = account_plan_instance_obj.create(cr,uid,{
                                                                       'name': name,
                                                                       'code': 'PCC',

                                                                       }, context=context)

            if line.project_id:
                account_plan_instance_line_obj.create(cr,uid,{
                                                     'plan_id':new_instance_id,
                                                     'analytic_account_id':line.project_id.id,
                                                     'type':'project',
                                                     }, context=context)

            if line.cost_center:
                account_plan_instance_line_obj.create(cr,uid,{
                                                     'plan_id':new_instance_id,
                                                     'analytic_account_id':line.cost_center.id,
                                                     'type':'cost_center',
                                                     }, context=context)


            super(purchase_order_line, self).write(cr, uid, ids, {'analytics_id':new_instance_id}, context=context)

        return res

    #################################################################################
    # Inherited onchange method to update cost_center value
    #################################################################################
    def product_id_change(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
                            name=False, price_unit=False, context=None):

        res_prod = super(purchase_order_line, self).product_id_change(cr, uid, ids, pricelist_id, product_id, qty, uom_id, partner_id, date_order, fiscal_position_id, date_planned,name, price_unit, context=context)

        if product_id:
            product_obj = self.pool.get('product.product').browse(cr, uid, product_id, context=context)

            if product_obj:
                res_prod['value'].update({'cost_center': product_obj.cost_center.id})

        return res_prod

    #################################################################################
    # Column definitions
    #################################################################################
    _columns = { 'project_id'  : fields.many2one('account.analytic.account', string='Proyecto'),
                 'cost_center' : fields.many2one('account.analytic.account', string='Centro de Costo'),
                }


#################################################################################
# Inherited account.analytic.plan.instance.line class definition
#################################################################################
class account_analytic_plan_instance_line(osv.osv):

    _inherit = "account.analytic.plan.instance.line"

    _columns = { 'type':fields.selection(string="Tipo",selection=[('cost_center','Centro de Costo'),('project','Proyecto'),('other','Otro')])
               }

    _defaults = { 'rate': 100.0
                }


#################################################################################
# Inherited account.analytic.line class definition
#################################################################################
class account_analytic_line(osv.osv):

    _inherit = "account.analytic.line"

    #################################################################################
    # Return ids
    #################################################################################
    def get_ids(self,cr,uid,ids,context=None):
        return ids


    #################################################################################
    # Get the type
    #################################################################################
    def get_ecua_type(self,cr,uid,ids,field_name,field_value,arg,context=None):

        records = self.browse(cr,uid,ids)
        result ={}

        for r in records:
            result[r.id] = r.account_id.ecua_type

        return result


    #################################################################################
    # Column definitions
    #################################################################################
    _columns = { 'ecua_type' : fields.function(get_ecua_type, type="selection", string="Tipo",
                                                store={'account.analytic.line':(get_ids,['account_id'],10),}   ),
               }


#################################################################################
# Inherited account.analytic.account class definition
#################################################################################
class account_analytic_account(osv.osv):

    _inherit = "account.analytic.account"

    #################################################################################
    # Default function for getting the type
    #################################################################################
    def get_ecua_type( self,cr,uid,context=None):

        if 'def_cost_center' in context:
            return 'cost_center'

        elif 'def_project' in context:
            return 'project'

        else:
            return False

    #################################################################################
    # Column and default definitions
    #################################################################################
    _columns = { 'ecua_type':fields.selection(string="Tipo",selection=[('cost_center','Centro de Costo'),('project','Proyecto'),('other','Otro')])
               }

    _defaults = { 'ecua_type':get_ecua_type,
                }

class purchase_order(osv.osv):
    _name='purchase.order'
    _inherit='purchase.order'

    def _prepare_inv_line(self, cr, uid, account_id, order_line, context=None):
        res = super(purchase_order, self)._prepare_inv_line(cr, uid, account_id, order_line, context=context)
        res['project_id'] = order_line.project_id.id
        res['cost_center'] = order_line.cost_center.id
        return res

purchase_order()