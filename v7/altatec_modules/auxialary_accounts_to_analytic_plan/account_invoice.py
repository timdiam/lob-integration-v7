# -*- coding: utf-8 -*-
#################################################################################
#
# A module that contains a credit card model can be attached to other
# objects (e.g. invoices).
#
# Author:  Tim Diamond
# Company: AltaTec Ecuador
# Date:    Jan. 18th, 2014
#
#################################################################################
from openerp.osv import fields, osv, orm

class account_invoice_line(orm.Model):
    _inherit = 'account.invoice.line'
    
    def create(self, cr, uid, values, context=None):    
        
        al_id = super(account_invoice_line, self).create(cr, uid, values, context=context)

        inv_line = self.browse(cr,uid,al_id,context=context)
        
        account_plan_instance_obj = self.pool.get('account.analytic.plan.instance')
        account_plan_instance_line_obj = self.pool.get('account.analytic.plan.instance.line')

        if (not inv_line.project_id) and (not inv_line.cost_center):
            if ('analytics_id' in values) or ('account_analytic_id' in values):
                #raise osv.except_osv( ( 'Error' ), ( 'Consulta con altatec.  No deberia ingresar ese dato' ) )
                pass
            return al_id
        
        name=""

        if inv_line.project_id:
            name=name+inv_line.project_id.name
        if inv_line.cost_center:
            name=name+ " / "+ inv_line.cost_center.name
        

        new_instance_id = account_plan_instance_obj.create(cr,uid,{
                                                                   'name': name,
                                                                   'code': 'PCC',
                                                     
                                                                   }, context=context)

        if inv_line.project_id:
            account_plan_instance_line_obj.create(cr,uid,{
                                                 'plan_id':new_instance_id,
                                                 'analytic_account_id':inv_line.project_id.id,
                                                 'type':'project',
                                                 }, context=context)
            
        if inv_line.cost_center:
            account_plan_instance_line_obj.create(cr,uid,{
                                                 'plan_id':new_instance_id,
                                                 'analytic_account_id':inv_line.cost_center.id,
                                                 'type':'cost_center',
                                                 }, context=context)

        super(account_invoice_line, self).write(cr, uid, [inv_line.id], {'analytics_id':new_instance_id}, context=context)
        return al_id

    def write(self, cr, uid, ids, values, context = {}):

        res = super(account_invoice_line, self).write(cr, uid, ids, values, context=context)

        if ('analytics_id' in values) and (values['analytics_id'] != False):
            #Can't raise error here unfortunately because a module writes this to field and hard to override that function...
            #raise osv.except_osv( ( 'Error' ), ( 'Consulta con altatec.  No deberia ingresar ese dato' ) )
            pass

        if ('account_analytic_id' in values) and (values['account_analytic_id'] != False):
            raise osv.except_osv( ( 'Error' ), ( 'Consulta con altatec.  No deberia ingresar ese dato' ) )

        account_plan_instance_obj = self.pool.get('account.analytic.plan.instance')
        account_plan_instance_line_obj = self.pool.get('account.analytic.plan.instance.line')

        for line in self.browse(cr,uid,ids,context=context):

            if (not line.project_id) and (not line.cost_center):
                super(account_invoice_line, self).write(cr, uid, [line.id], {'analytics_id':False}, context=context)
                continue

            name=""

            if line.project_id:
                name=name+line.project_id.name
            if line.cost_center:
                name=name+ " / "+ line.cost_center.name


            new_instance_id = account_plan_instance_obj.create(cr,uid,{
                                                                       'name': name,
                                                                       'code': 'PCC',

                                                                       }, context=context)

            if line.project_id:
                account_plan_instance_line_obj.create(cr,uid,{
                                                     'plan_id':new_instance_id,
                                                     'analytic_account_id':line.project_id.id,
                                                     'type':'project',
                                                     }, context=context)

            if line.cost_center:
                account_plan_instance_line_obj.create(cr,uid,{
                                                     'plan_id':new_instance_id,
                                                     'analytic_account_id':line.cost_center.id,
                                                     'type':'cost_center',
                                                     }, context=context)


            super(account_invoice_line, self).write(cr, uid, [line.id], {'analytics_id':new_instance_id}, context=context)

        return res

    #################################################################################
    # Inherited onchange method to update cost_center value
    #################################################################################

    def product_id_change(self, cr, uid, ids, product, uom_id, qty=0, name='', type='out_invoice', partner_id=False, fposition_id=False, price_unit=False, currency_id=False, context={}, company_id=None):

        res_prod = super(account_invoice_line, self).product_id_change(cr, uid, ids, product, uom_id, qty, name, type, partner_id, fposition_id, price_unit, currency_id=currency_id, context=context, company_id=company_id)

        if product:
            product_obj = self.pool.get('product.product').browse(cr, uid, product, context=context)

            if product_obj:
                res_prod['value'].update({'cost_center': product_obj.cost_center.id})

        return res_prod


    _columns = {
                'project_id':fields.many2one('account.analytic.account', string='Proyecto'),
                'cost_center':fields.many2one('account.analytic.account', string='Centro de Costo'),
                }
