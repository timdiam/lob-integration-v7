{
    'name' : 'AltaTec Informative Field Widget',
    'version': '1.0',
    'summary': 'This widget will allow the use of informative fields.',
    'sequence': '19',
    'category': 'Tools',
    'complexity': 'easy',
    'description':
        """
		Module that overcomes the restriction of not being able to update readonly fields via onchange.  We solve this problem by adding a widget of type informative.
		To achieve this, add the tag:  widget='informative' to the xml definition of the field.
        """,
    'data': [
        #"informative_fields.xml",
    ],
    'depends' : ['base', 'web'],
    'js': ['static/src/js/*.js'],
    'css': ['static/src/css/*.css'],
    'qweb': ['static/src/xml/*.xml'],
	'installable': True,
    'auto_install': False,
    'application': True,
}
