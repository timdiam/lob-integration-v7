
openerp.informative_fields = function(instance) {
    var _t = instance.web._t,
        _lt = instance.web._lt;
    var QWeb = instance.web.qweb;

    instance.informative_fields = {};
	
	instance.informative_fields.InformativeField = instance.web.form.FieldChar.extend({
			template: 'InformativeField',
		    render_value: function() {
				var show_value = this.format_value(this.get('value'), '');
/* 				if (!this.get("effective_readonly")) {
					this.$el.find('input').val(show_value);
				} else {
					if (this.password) {
						show_value = new Array(show_value.length + 1).join('*');
					} */
					this.$(".oe_form_char_content").text(show_value);
				}
        });

    instance.web.form.widgets.add('informative', 'instance.informative_fields.InformativeField');
	
} ;
