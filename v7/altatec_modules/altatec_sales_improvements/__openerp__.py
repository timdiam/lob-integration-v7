{
    'name': 'Altatec Sales Improvements',
    'version': '1.0',
    'description': """
        Changes for Module Sales
    """,
    'author': 'Harry Alvarez',
    'website': 'www.altatececuador.com',
    "depends" : [ 'sale',
                  'crm',
                  'ecua_split_tax_and_retentions',
                  'base',
                  'account_followup',
                  'account_accountant',
                ],
    "data" : [ 'views/sale_order.xml',
               'views/res_partner_view.xml',
               'security/ir.model.access.csv',
             ],
    "installable": True,
    "auto_install": False
}
