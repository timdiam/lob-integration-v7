import locale
from datetime import date
from openerp.osv import fields, osv, orm
import logging
from openerp.tools.translate import _
_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')

class seguimientos_pagos(osv.osv):

    _name="seguimientos.pagos"

    _columns={
        "payment_next_action":fields.char("Proxima Accion"),
        "payment_next_date":fields.char("Fecha de Proxima Accion"),
        "payment_note":fields.char("Promesa Pago Cliente"),
        "empresa":fields.many2one("res.partner"),
        "fecha_actual":fields.date("Fecha Actual"),
        }


class res_partner(osv.osv):

    _inherit = "res.partner"

    def write(self, cr, uid, ids, vals, context=None):
        res=super(res_partner, self).write(cr, uid, ids, vals, context=context)
        pna=False
        pnd=False
        pn=False
        if ('payment_next_action' in vals) or ('payment_next_action_date' in vals) or ('payment_note' in vals):
            if 'payment_next_action' in vals:
                pna=vals['payment_next_action']
            else:
                pna=self.browse(cr, uid, ids, context=context)[0].payment_next_action

            if 'payment_next_action_date' in vals:
                pnd=vals['payment_next_action_date']
            else:
                pnd=self.browse(cr, uid, ids, context=context)[0].payment_next_action_date

            if 'payment_note' in vals:
                pn=vals['payment_note']
            else:
                pn=self.browse(cr, uid, ids, context=context)[0].payment_note

            id_seguimientos_pagos= self.pool.get('seguimientos.pagos').create(cr,uid,{
                'fecha_actual':date.today().strftime('%Y-%m-%d'),
                'payment_next_action':pna,
                'payment_next_date':pnd,
                'payment_note':pn,
                'empresa':ids[0],
                })
        return res

    _columns={
        "seguimientos_pagos":fields.one2many('seguimientos.pagos', 'empresa', string="Seguimiento de Pagos"),

        }