######################################################################################################
# Improvements made to various aspects of the Sales modules
#
# Authors: Harry Alvarez, Dan Haggerty
# Date:    9/4/2015
######################################################################################################
import locale
from openerp.osv import fields, osv, orm
import logging
from openerp.tools.translate import _
_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')

######################################################################################################
# Inherited sale order class definition
######################################################################################################
class sale_order(osv.osv):

    _inherit = "sale.order"

    ######################################################################################################
    # action_button_confirm() override. Throws an error if a solicitud already has a confirmed
    # presupuesto
    ######################################################################################################
    def action_button_confirm(self, cr, uid, ids, context=None):

        r = self.browse(cr, uid, ids, context=context)[0]

        validacion=r.opportunity.presupuesto

        if validacion==None:
            return super(sale_order, self).action_button_confirm(cr, uid, ids, context=context)

        for presupuesto in r.opportunity.presupuesto:
            if r.opportunity.state=='done':
            #if presupuesto.state == 'manual':
                raise osv.except_osv( _('Accion Invalida!'), _('La solicitud ya esta en el estado "Ganado".  Por favor, revise la solicitud: '+r.opportunity.name))

        stage_id=self.pool.get('crm.case.stage').search(cr,uid,[("name","=","Won")])
        stage=self.pool.get('crm.case.stage').browse(cr,uid,stage_id)
        if (len(stage))>1:
            raise osv.except_osv( _('Accion Invalida!'), _('Hay mas de una Etapa con el mismo nombre!'))
        r.opportunity.write({'state': 'done','stage_id':stage[0].id})

        return super(sale_order, self).action_button_confirm(cr, uid, ids, context=context)


######################################################################################################
# Inherited sale order line class definition
######################################################################################################
class sale_order_line(osv.osv):

    _inherit = "sale.order.line"

    ######################################################################################################
    # Remove anything set in the 'name' field as a result of the product_id onchange.
    ######################################################################################################
    def product_id_change(self, cr, uid, ids, pricelist, product, qty=0,
                          uom=False, qty_uos=0, uos=False, name='', partner_id=False,
                          lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False, flag=False, context=None):

        res = super( sale_order_line, self ).product_id_change(cr ,uid, ids, pricelist, product, qty, uom, qty_uos, uos, name, partner_id, lang, update_tax, date_order, packaging, fiscal_position, flag, context )

        if( res and 'value' in res and 'name' in res['value'] ):
            del res['value']['name']

        return res