# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Andres Calle, TRESCloud Cia Ltda.
# Copyright (C) 2013  Ecuadorenlinea.net
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

import time
import netsvc
from datetime import date, datetime, timedelta

from osv import fields, osv
from tools import config
from tools.translate import _

    
class sri_printer_point(osv.osv):
    _inherit = 'sri.printer.point'
    
    _columns = {
        'allow_electronic_invoices':fields.boolean('Allow electronic Invoices', 
                                                   help='Enable the issuing of electronic invoices, this is always true according to current SRI rules'),
        'allow_electronic_credit_note':fields.boolean('Allow electronic Credit Notes', 
                                                   help='Enable the issuing of electronic Credit Notes'),
        'allow_electronic_debit_note':fields.boolean('Allow electronic Debit Notes', 
                                                   help='Enable the issuing of electronic Debit Notes'),
        'allow_electronic_waybill':fields.boolean('Allow electronic waybills', 
                                                   help='Enable the issuing of electronic Waybills'),
        'allow_electronic_withhold':fields.boolean('Allow electronic Withhold', 
                                                   help='Enable the issuing of electronic Withholds'),
                }
    
    _defaults = {
        'allow_electronic_invoices' : True,
        'allow_electronic_credit_note' : True,
        'allow_electronic_debit_note' : True,
        'allow_electronic_waybill' : True,
        'allow_electronic_withhold' : True,
        }
    
sri_printer_point()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: