# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: TRESCloud Cia Ltda, Andres Calle
# Copyright (C) 2013  wwww.trescloud.com
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from openerp.osv import fields, osv
from openerp.tools.translate import _
from string import split
from datetime import datetime
from dateutil.relativedelta import relativedelta

class sri_authorizations(osv.osv):

    _inherit = "sri.authorizations"
    
    def authorization_onchange_helper(self, cr, uid, document_type_id, partner_id=None, date=None, printer_id=None, sequence=None, company_id=None, context=None):
        '''
        Modifica el metodo removiendo las autorizaciones y advertencias d eautorizacion cuando el documento
        es electronico y emitido por nosotros.
        '''
        res = {'value': {'authorizations_id': False},
               'warning':{},
               'domain':{'authorizations_id':[]}}
        void_onchange = False
        if printer_id and document_type_id:            
            printer = self.pool.get('sri.printer.point').browse(cr, uid, printer_id, context=context)

            document_type_obj = self.pool.get('account.invoice.document.type')
            document_type=document_type_obj.browse(cr,uid,document_type_id)
            if document_type.parent_id: #si tiene un parent cambiamos al objeto padre
                document_type=document_type_obj.browse(cr,uid,document_type.parent_id.id)
                
            if document_type.code in ['01','18','04','05','06','07']: #documentos electronicos validos
                if document_type.sri_authorization_validation_owner == True: #emitidos por mi
                    #revisamos si el punto de impresion tiene activado la factura electronica
                    #para el documento en cuestion
                    if document_type.code in ['01','18'] and printer.allow_electronic_invoices:
                        void_onchange = True
                    elif document_type.code == '04' and printer.allow_electronic_credit_note:
                        void_onchange = True
                    elif document_type.code == '05' and printer.allow_electronic_debit_note:
                        void_onchange = True
                    elif document_type.code == '06' and printer.allow_electronic_waybill:
                        void_onchange = True
                    elif document_type.code == '07' and printer.allow_electronic_withhold:
                        void_onchange = True
        
        
        res = super(sri_authorizations, self).authorization_onchange_helper(cr, uid, document_type_id, partner_id, date, printer_id, sequence, company_id, context)
        
        #if not void_onchange:
            #res['value'].update({'authorizations_id': False}) #
            #res['domain'].update({'authorizations_id': []}) #
            
        return res
        
sri_authorizations()