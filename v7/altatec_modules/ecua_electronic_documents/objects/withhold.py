# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: TRESCloud Cia Ltda, Andres CALLE
# Copyright (C) 2013  wwww.trescloud.com
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from openerp.osv import fields, osv
from openerp.tools.translate import _
from general_settings import ELECTRONIC_DOCUMENT_STATES
from general_settings import ELECTRONIC_DOCUMENT_STATES_HELP
from general_settings import ELECTRONIC_PENDING_MSG
from general_settings import ELECTRONIC_REJECT_MSG
import logging

_logger = logging.getLogger(__name__)

###################################################################################################
# account_withhold class override
###################################################################################################
class account_withhold(osv.osv):
    
    _inherit = "account.withhold"

    ######################################################################################################
    # Break the relationship between this retention and its invoice
    ######################################################################################################
    def remove_from_invoice( self, cr, uid, ids, context=None ):

        withholds = self.browse( cr, uid, ids, context=context )

        for withhold in withholds:

            if( withhold.state != 'canceled' ):
                raise osv.except_osv( "Error!", "No se puede sacar la factura de una retencion que no esta en el estado 'Cancelado'" )
            if( withhold.allow_electronic_document != True ):
                raise osv.except_osv( "Error!", "No se puede sacar la factura de una retencion que no es electronica" )
            if( withhold.invoice_id == False ):
                raise osv.except_osv( "Error!", "Retencion ya no tiene una factura" )
            if( withhold.invoice_released ):
                raise osv.except_osv( "Error!", "La factura ya fue sacado" )

            # Set the withhold_id of the retention's invoice to False
            withhold.invoice_id.write( {'withhold_id' : False} )

            # Mark this withhold's invoice_released as True
            withhold.write( { 'invoice_released' : True } )

    ######################################################################################################
    # Default function for setting the allow_electronic_document field. If the printer point is
    # electronic, return True
    ######################################################################################################
    def _default_allow_electronic_document(self, cr, uid, context=None):

        result = False
                
        if context.get('transaction_type') and context.get('active_id'):
            
            transaction_type = context.get('transaction_type')
            if transaction_type == 'purchase':
                user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
                if user.printer_id:
                    printer_id = user.printer_id.id
                    printer_point_obj = self.pool.get('sri.printer.point')
                    printer_point = printer_point_obj.browse(cr,uid,[printer_point_id])[0]  
                    if printer_point.allow_electronic_withhold:
                        result = True
        
        #vemos si la tienda demanda documento electronico para el tipo de documento a emitir                    
        return result
    
    ######################################################################################################
    # copy() override. Set electronic_id to false when copying.
    ######################################################################################################
    def copy(self, cr, uid, id, default=None, context=None):

        if default is None:
            default = {}

        default.update( { 'electronic_id': False, } )

        return super(account_withhold, self).copy(cr, uid, id, default, context)

    ######################################################################################################
    # unlink() override. If it has an electronic document, throw an error.
    ######################################################################################################
    def unlink(self, cr, uid, ids, context=None, check=True):

        if context is None:
            context = {}

        result = False

        for withhold in self.browse(cr, uid, ids, context=context):
            if withhold.electronic_id and withhold.electronic_id.state != 'cancel':
                raise osv.except_osv(_('ERROR!'), _('No se puede borrar pues tiene un documento electronico asociado'))    

        return super(account_withhold, self).unlink(cr, uid, [withhold.id], context=context)

    ######################################################################################################
    # onchange_printer_id() override. Set the allow_electronic_document field if the printer point
    # is electronic.
    ######################################################################################################
    def onchange_printer_id(self, cr, uid, ids, transaction_type, printer_id, partner_id, creation_date, context=None):

        res = super(account_withhold, self).onchange_printer_id(cr, uid, ids, transaction_type, printer_id, partner_id, creation_date, context)
        res["value"].update({ 'allow_electronic_document' : False })
        
        if (printer_id and transaction_type in ['purchase']):
            
            #obtenemos la tienda
            printer_point_obj = self.pool.get('sri.printer.point')
            printer_point     = printer_point_obj.browse(cr,uid,[printer_id])[0]
            
            #vemos si la tienda demanda documento electronico para el tipo de documento a emitir
            if printer_point.allow_electronic_withhold and transaction_type == 'purchase': #facturas de venta
                res["value"].update({'allow_electronic_document' : True})

        return res

    ######################################################################################################
    # action_aprove() override. Creates the electronic document when approving the retencion, if the
    # printer point is electronic. Only for retencion en compras
    ######################################################################################################
    def action_aprove(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        result = False

        if( self.browse(cr,uid,ids[0], context=context).transaction_type == 'purchase' ):

            electronic_doc = self.pool.get('electronic.document')
            withhold_objs  = self.browse(cr, uid, ids, context)

            for withhold in withhold_objs:
                #TODO ALTATEC: For the below reason, this function should NOT be shared with purchase / sales retentions.
                if(withhold.transaction_type != 'purchase'):
                    raise osv.except_osv( _( 'Error de Logico' ),
                                          _( 'Error en este retencion.  Consulte con AltaTec.' ) )
                if(withhold.printer_id):
                    if withhold.printer_id.withhold_sequence_id:
                        num = self.pool.get('sri.printer.point').get_next_sequence_number(cr, uid, withhold.printer_id, 'withhold', withhold.number, context)
                        self.write(cr,uid,[withhold.id],{'number':num})
                        #TODO ALTATEC: Make sure below is tested.. We comment out the below code because
                        #It is a terrible coding practice to update the loop variable during a loop...
                        #withhold = self.browse(cr, uid, withhold.id, context) #actualizamos luego del write
                else:
                    raise osv.except_osv( _( 'Error' ), _( 'Este retencion no tiene definido un punto de impresion.' ) )

            #Start new loop because above had a very poor coding practice to update the
            #Loop variable during the loop!!!!
            for withhold in withhold_objs:
                #TODO ALTATEC: For the below reason, this function should NOT be shared with purchase / sales retentions.
                if(withhold.transaction_type != 'purchase'):
                    raise osv.except_osv( _( 'Error de Logico' ),
                                          _( 'Error en este retencion.  Consulte con AltaTec.' ) )

                if withhold.allow_electronic_document:

                    if( not withhold.partner_id.street ):
                        raise osv.except_osv( "Error!", "Direccion del cliente/proveedor es requerido para retenciones electronicas" )

                    if( not withhold.partner_id.email):
                        raise osv.except_osv( "Error!", "Correo del cliente/proveedor es requerido para retenciones electronicas" )

                    if not withhold.electronic_id:
                        values={
                                'document_id': str('account.withhold') + ',' + str(withhold.id)
                                }
                        electronic_id=electronic_doc.create(cr,uid,values,context)
                        self.write(cr,uid,withhold.id,{'electronic_id':electronic_id})
                        withhold = self.browse(cr, uid, withhold.id, context) #actualizamos luego del write
                    else:
                        electronic_id = withhold.electronic_id.id

                    #TODO ALTATEC: Make sure these states make sense to us.....
                    if withhold.electronic_id.state not in ['sri_authorized','sent','visualized']:
                        #conectamos al webservice y generamos el documento electronico
                        electronic_obj = self.pool.get('electronic.document')
                        electronic_obj.get_access_key(cr, uid, [electronic_id]) #generamos la clave de acceso

                        electronic_obj.attempt_electronic_document(cr, uid, [electronic_id], context)
        
        #TODO: Si el documento electronico no fue aprobado debe permitir volver a generarlo
        return super(account_withhold, self).action_aprove(cr, uid, ids, context)

    ######################################################################################################
    # _warning_msgs() override.
    ######################################################################################################
    def _warning_msgs(self, cr, uid, ids, context=None):
        '''
        Metodo helper: Ayuda a redefinir con facilidad el campo tipo funcion asociado al metodo _get_warning_msgs
        Retorna una explicacion de porque la linea de retencion esta en color rojo en la vista tree
        '''        
        warning_msgs = super(account_withhold, self)._warning_msgs(cr, uid, ids, context)
        withhold     = self.browse(cr, uid, ids, context)

        if withhold.electronic_document_state in ['draft','valid', 'ca_signed', 'sri_received']:
            if len(warning_msgs) >0:
                warning_msgs += ', '
            warning_msgs += ELECTRONIC_PENDING_MSG

        elif withhold.electronic_document_state in ['not_valid', 'ca_rejected', 'sri_rejected', 'sri_not_authorized']:
            if len(warning_msgs) >0:
                warning_msgs += ', '
            warning_msgs += ELECTRONIC_REJECT_MSG

        return warning_msgs

    ######################################################################################################
    # Column and defaults definitions
    ######################################################################################################
    _columns = { 'allow_electronic_document' : fields.boolean('Electronic Document', help= 'Indicates if this is an electronic document instead of a paper one'),
                 'invoice_released'          : fields.boolean('Invoice Released'),
                 'electronic_id'             : fields.many2one('electronic.document', 'Withhold Electronic Document', required=False,help="It is the relation between withhold and electronic document",track_visibility='onchange'),
                 'electronic_document_state' : fields.related('electronic_id',
                                                              'state',
                                                              type="selection",
                                                              selection=ELECTRONIC_DOCUMENT_STATES,
                                                              string="Electronic Document State",
                                                              readonly=True,
                                                              help=ELECTRONIC_DOCUMENT_STATES_HELP,
                                                              #track_visibility='onchange',
                                                              store=False),
                 'access_key'                : fields.related('electronic_id',
                                                              'access_key',
                                                              type="char",
                                                              relation="electronic.document",
                                                              string="Access Key",
                                                              readonly=True,
                                                              help='Unique code to identify this document, is generated based on date, vat code, serial number, and other related fields',
                                                              track_visibility='onchange',
                                                              store=True),
               }

    _defaults = { 'allow_electronic_document' : _default_allow_electronic_document,
                  'invoice_released'          : False,
                }