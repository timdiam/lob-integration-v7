#coding: utf-8
import os
from lxml import etree #para validaciones del xsd
from itertools import chain #para codificacion xml
from xml.sax.saxutils import escape #para codificacion xml
import re #para remocion de tildes y enies
import unicodedata #para remocion de tildes y enies

'''
Aqui se guardan las variables generales del modulo tales como:
- Estados del documento electronico, para que sean consumidos por todos los objetos
- Archivos XSD de cada tipo de documento electronico para las validaciones
- se definen las siguientes variables:
    @ELECTRONIC_DOCUMENT_STATES
    @XSD_SRI_110_FACTURA
    #TODO: Listar todas las variable definidas
    
Aqui se definen metodos generales como:
- Remocion de tildes
'''

ELECTRONIC_DOCUMENT_STATES = [('draft', 'Draft'),
                              ('valid', 'Valid'),
                              ('not_valid','Invalid Document'),
                              ('ca_signed','Signed by CA'),
                              ('ca_rejected','Rejected by CA'),
                              ('sri_received','Received by SRI'),
                              ('sri_rejected','Rejected by SRI'),
                              ('sri_not_authorized', 'Not Authorized by SRI'),
                              ('sri_authorized','Authorized by SRI'),
                              ('sent','Sent'),
                              ('visualized','Viewed'),
                              ('cancel', 'Anulado'),]

ELECTRONIC_DOCUMENT_STATES_HELP_OLD = '* When the document is created the status is \'Draft\'.\
\n* If the document is invalid (wrong setup) the status is set to \'Invalid Document\'.\
\n* If the document is signed but not yet authorized the status is set to \'Signed Document\'.\
\n* If the document is not authorized by SRI the status is set to \'Rejected by SRI\'.\
\n* If the document is authorized by SRI the status is set to \'Authorized by SRI\'.\
\n* If the document is sent to the customer, the status is set to \'Sent\'.\
\n* If the document is visualized by the customer/supplier the status is \'Visualized\'.'

ELECTRONIC_DOCUMENT_STATES_HELP = '*\'Valid\': The electronic proxy thinks the XML structure is ok, but have not yet signed it.\
\n*\'Invalid Document\' The XML structure is wrong, you should review the data or contact support.\
\n*\'Signed by CA\' The document has been properly signed, but have not yet being sent to the SRI.\
\n*\'Rejected by CA\' Impossible to sign the document, maybe the CA revoked your certificate.\
\n*\'Received by SRI\' The document has reached SRI, but has not yet been processed.\
\n*\'Rejected by SRI\' The document was rejected by SRI, see the detail to find why.\
\n*\'Not Authorized by SRI\' The document was not authorized by SRI, see detail to find why.\
\n*\'Authorized by SRI\' Great!, document is authorized!, transaction is finished.\
\n*\'Sent\' The electronic document has been sent to your partner.\
\n*\'Viewed\' The electronic document has been viewed by your partner.\
\n*\'Anulado\' El documento electronico fue anulado manualmente por algun usuario.'

ELECTRONIC_PENDING_MSG = 'El documento electronico esta pendiente de ser procesado, espere unos segundos y desde el formulario de documento electronico de click en \'Procesar Documento Electronico\'.' 
ELECTRONIC_REJECT_MSG = 'El documento electronico fue rechazado, debe reversar la transaccion y repetirla sin errores, contacte a soporte'

class XMLDocumentException(Exception):
    u"""
    Un error relacionado a la validacion de
    un documento xml.
    """
    pass

class XMLValidationException(XMLDocumentException):
    u"""
    Esta excepcion se gatilla cuando la validacion
    entre un xml y su xsd da mala. El mensaje incluido
    por esta excepcion es directamente el mensaje que
    proporciona la libreria lxml (por lo que el mensaje
    sale en ingles). Esta excepción muestra un mensaje
    y un log de validación, por lo que al encontrarla
    deberíamos obtener esos valores. El log de validación
    es una estructura propia de la librería lxml, por lo
    que los mensajes de validación estarán en inglés.
    Deberíamos capturar la excepción de esta manera:
         try:
             validate_xml_document(..., ..., ...)
         except XMLFormatException as e:
             mensaje = e.args[0]
             error_log = e.args[1]

    La estructura de error_log está documentada en:
        http://lxml.de/parsing.html
    """
    pass

def validate_xml_vs_xsd(xml_content, xsd_instance):
    """
    Dado un contenido XML de entrada, y un contenido XSD de validacion XML.
    Dado una funcion de validacion semantica (algo de lo que un XSD no se puede encargar).
    Primero ocurre una validacion a nivel XML/XSD.
    """
    try:
        root = etree.XML(xml_content.strip())
    except Exception as e:
        raise XMLFormatException(str(e), XMLFormatException.IN_XML)

    if not xsd_instance.validate(root):
        raise XMLValidationException("XML Error:", xsd_instance.error_log)


def _load_xsd(file_):
    '''
    Carga los archivos XSD a memoria
    '''
    try:
        return etree.XMLSchema(etree.parse(file_))
    except etree.XMLSchemaParseError as e:
        raise ImportError(
            "Cannot generate XSD Instance: %s, with data:\n%r" % (type(e).__name__, e.__dict__)
        )

#generamos las instancias XSD
path = os.path.abspath(os.path.dirname(__file__))
XSD_SRI_110_FACTURA = _load_xsd(path+'/../resources/sri_110_factura.xsd')
XSD_SRI_110_GUIA_REMISION = _load_xsd(path+'/../resources/sri_110_guia_remision.xsd')
XSD_SRI_110_NOTA_CREDITO = _load_xsd(path+'/../resources/sri_110_nota_credito.xsd')


#codificador para xml
class XmlNonAnsiReplacer(object):

    def __init__(self, encoding='utf-8'):
        self.encoding = encoding

    def __getitem__(self, key):
        key = unicode(key, encoding=encoding) if not isinstance(key, unicode) else key
        if len(key) > 1:
            raise KeyError(key)
        ord_ = ord(key)
        if ord_ < 128 and ord_ >= 32:
            raise KeyError(key)
        return key if ord_ < 128 and ord_ >= 32 else "&#%x" % ord_

    def iteritems(self):
        return ((k, self[k]) for k in (unichr(k_) for k_ in chain(xrange(0, 32),xrange(128, 0x10000))))

    def items(self):
        return list(self.iteritems())


def xml_escape(s, encoding='utf-8'):
    return escape(s if isinstance(s, unicode) else unicode(s, encoding=encoding), XmlNonAnsiReplacer(encoding))

#remocion de tildes y enies
#print strip_accents_and_ntildes('Las arañas son los arácnidos por excelencia.')

def multi_replace(text, replacements):
    rep = dict(('(%s)' % re.escape(k), v) for k, v in replacements.iteritems())
    pattern = re.compile("|".join(rep.keys()))
    return pattern.sub(lambda m: rep[re.escape(m.group(0))], text)

def strip_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

def strip_accents_and_ntildes(s):
    x = multi_replace( strip_accents( unicode(s, encoding='utf-8') if isinstance(s, str) else s), {'ñ': 'n', 'Ñ': 'N' })
    x = x.replace(u'&','y')
    return x

def clean_xml(s):
    return etree.tostring(etree.XML(s, parser=etree.XMLParser(remove_blank_text=True)), pretty_print=True)
    #print clean_xml(sys.stdin.read())

