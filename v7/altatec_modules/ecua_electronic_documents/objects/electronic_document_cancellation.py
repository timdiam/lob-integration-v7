from openerp.osv import fields, osv


class electronic_document_cancellation_wizard(osv.osv_memory):

    _name = "electronic.document.cancellation.wizard"

    def confirm(self, cr, uid, ids, context={}):

        if not context or not 'active_model' in context or context['active_model'] != 'electronic.document':
            raise osv.except_osv("Error", "Programming Error. Contacte a AltaTec")

        document = self.pool.get('electronic.document').browse(cr, uid, context['active_id'], context=context)

        document.write({ 'state'             : 'cancel',
                         'previous_state'    : document.state,
                         'cancellation_user' : uid,
                         'cancellation_date' : fields.date.context_today(self, cr, uid, context=context)
                      })


    _columns = { 'test' : fields.char("TESTFIELD"),
               }