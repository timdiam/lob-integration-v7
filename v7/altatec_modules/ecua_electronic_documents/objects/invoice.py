from openerp.osv import fields, osv
from openerp.tools.translate import _
from general_settings import ELECTRONIC_DOCUMENT_STATES
from general_settings import ELECTRONIC_DOCUMENT_STATES_HELP
from general_settings import ELECTRONIC_PENDING_MSG
from general_settings import ELECTRONIC_REJECT_MSG
import logging

_logger = logging.getLogger(__name__)


class account_invoice(osv.osv):
    
    _inherit = "account.invoice"

    def action_cancel(self, cr, uid, ids, context=None):

        invoices = self.browse( cr, uid, ids, context )

        # Si esta factura tiene una factura electronica, no permite anular
        for invoice in invoices:
            if invoice.electronic_id and invoice.electronic_id.state != 'cancel':
                raise osv.except_osv(_('Error!'), _( 'No se puede anular factura: ' + invoice.internal_number + " por que ya tiene un Document Electronico. Por favor anule el documento electronico." ))

        return super( account_invoice, self ).action_cancel( cr, uid, ids, context )

    
    def _default_allow_electronic_document(self, cr, uid, context=None):
        '''
        Si el punto de impresion es electronico y tiene marcado el tipo de documento
        correcto entonces el account.invoice se marca como electronico
        '''
        
        result = False
        
        #obtenemos el tipo de documento
        document_invoice_type_id = self._doc_type(cr, uid, context)
        document_invoice_type_obj = self.pool.get('account.invoice.document.type')
        document_invoice_type = document_invoice_type_obj.browse(cr,uid,[document_invoice_type_id])[0]  
        
        #obtenemos la tienda
        printer_point_id = self._default_printer_point(cr, uid, context)
        printer_point_obj = self.pool.get('sri.printer.point')
        printer_point = printer_point_obj.browse(cr,uid,[printer_point_id])[0]  
        
        if not document_invoice_type_id or not  printer_point_id: #para evitar errores al instalar el modulo
            return result
        
        #vemos si la tienda demanda documento electronico para el tipo de documento a emitir
        if document_invoice_type.code == '18' and printer_point.allow_electronic_invoices and document_invoice_type.type == 'out_invoice': #facturas de venta
            result = True
        elif document_invoice_type.code == '04' and printer_point.allow_electronic_credit_note and document_invoice_type.type == 'out_refund': #nota de credito
            result = True
        elif document_invoice_type.code == '05' and printer_point.allow_electronic_debit_note and document_invoice_type.type == 'out_refund': #nota de debito
            result = True
        
        return result
    
    _columns = {

        'allow_electronic_document':fields.boolean('Electronic Document', help= 'Indicates if this is an electronic document instead of a paper one'),
                        
        'electronic_id':fields.many2one('electronic.document', 'Invoice Electronic Document', required=False,help="It is the relation between invoice electronic document",track_visibility='onchange'),
        
        'electronic_document_state': fields.related(
                                     'electronic_id',
                                     'state',
                                     type="selection",
                                     selection=ELECTRONIC_DOCUMENT_STATES,
                                     string="Electronic Document State",
                                     readonly=True,
                                     help=ELECTRONIC_DOCUMENT_STATES_HELP,
                                     #track_visibility='onchange',
                                     store=False),
        
        'access_key': fields.related(
                                     'electronic_id',
                                     'access_key',
                                     type="char",
                                     relation="electronic.document",
                                     string="Access Key",
                                     readonly=True,
                                     help='Unique code to identify this document, is generated based on date, vat code, serial number, and other related fields',
                                     track_visibility='onchange',
                                     store=True),
                }
    
    _defaults = {
        'allow_electronic_document' : _default_allow_electronic_document,
        }
    
    
    def copy(self, cr, uid, id, default=None, context=None):
        ''' 
        No copia el documento electronico
        '''
        
        if default is None:
            default = {}
        
        default.update({
            'electronic_id': False, #El documento electronico no debe copiarse 
            #pues esta diseniado para pertenecer a un solo documento
        })
        return super(account_invoice, self).copy(cr, uid, id, default, context)
    
    def unlink(self, cr, uid, ids, context=None, check=True):
        '''
        No se puede eliminar una factura si tiene un documento electronico asociado
        '''
        if context is None:
            context = {}
        result = False
        for invoice in self.browse(cr, uid, ids, context=context):
            if invoice.electronic_id:
                raise osv.except_osv(_('ERROR!'), _('No se puede borrar pues tiene un documento electronico asociado'))    
        result = super(account_invoice, self).unlink(cr, uid, ids, context=context)
        return result
    
    def onchange_document_invoice_type_id(self, cr, uid, ids, type=False, date_invoice=False, partner_id=False, sri_tax_support_id=False, document_invoice_type_id=False, printer_id=False, authorizations_id=False, company_id=False, context=None):
        '''
        El onchange del tipo de documento actualiza los valores de los campos:
        - allow_electronic_document
        '''
        
        res = super(account_invoice, self).onchange_document_invoice_type_id(cr, uid, ids, type, date_invoice, partner_id, sri_tax_support_id, document_invoice_type_id, printer_id, authorizations_id, company_id, context)
        res["value"].update({
                             'allow_electronic_document' : False
                             })
        
        if (document_invoice_type_id and printer_id and type in ['out_invoice','out_refund']):
            
            #obtenemos el tipo de documento
            document_invoice_type_obj = self.pool.get('account.invoice.document.type')
            document_invoice_type = document_invoice_type_obj.browse(cr,uid,[document_invoice_type_id])[0]  
            
            #obtenemos la tienda
            printer_point_obj = self.pool.get('sri.printer.point')
            printer_point = printer_point_obj.browse(cr,uid,[printer_id])[0]
            
                  
            #vemos si la tienda demanda documento electronico para el tipo de documento a emitir
            if document_invoice_type.code == '18' and printer_point.allow_electronic_invoices and document_invoice_type.type == 'out_invoice': #facturas de venta
                res["value"].update({'allow_electronic_document' : True})
            elif document_invoice_type.code == '04' and printer_point.allow_electronic_credit_note and document_invoice_type.type == 'out_refund': #nota de credito
                res["value"].update({'allow_electronic_document' : True})
            elif document_invoice_type.code == '05' and printer_point.allow_electronic_debit_note and document_invoice_type.type == 'out_invoice': #nota de debito
                res["value"].update({'allow_electronic_document' : True})                
        return res

    def onchange_printer_id(self, cr, uid, ids, type=False, date_invoice=False, partner_id=False, sri_tax_support_id=False, document_invoice_type_id=False, printer_id=False, authorizations_id=False, company_id=False, context=None):
        '''
        El onchange del punto de impresion actualiza los valores de los campos:
        - allow_electronic_document
        '''
        
        res = super(account_invoice, self).onchange_printer_id(cr, uid, ids, type, date_invoice, partner_id, sri_tax_support_id, document_invoice_type_id, printer_id, authorizations_id, company_id, context)
        res["value"].update({
                             'allow_electronic_document' : False
                             })
        
        if (document_invoice_type_id and printer_id and type in ['out_invoice','out_refund']):
            
            #obtenemos el tipo de documento
            document_invoice_type_obj = self.pool.get('account.invoice.document.type')
            document_invoice_type = document_invoice_type_obj.browse(cr,uid,[document_invoice_type_id])[0]  
            
            #obtenemos la tienda
            printer_point_obj = self.pool.get('sri.printer.point')
            printer_point = printer_point_obj.browse(cr,uid,[printer_id])[0]
            
                  
            #vemos si la tienda demanda documento electronico para el tipo de documento a emitir
            if document_invoice_type.code == '18' and printer_point.allow_electronic_invoices and document_invoice_type.type == 'out_invoice': #facturas de venta
                res["value"].update({'allow_electronic_document' : True})
            elif document_invoice_type.code == '04' and printer_point.allow_electronic_credit_note and document_invoice_type.type == 'out_refund': #nota de credito
                res["value"].update({'allow_electronic_document' : True})
            elif document_invoice_type.code == '05' and printer_point.allow_electronic_debit_note and document_invoice_type.type == 'out_refund': #nota de debito
                res["value"].update({'allow_electronic_document' : True})                
        return res
    
    def validate_authorizations(self, cr, uid, ids, context=None):
        '''
        Al aprobar la factura se crea el documento electronico asociado
        Los metodos de validacion de facturas van en este orden:
            action_date_assign()
            action_move_create()
            action_number() authorization_id which comes from the reply from SRI 
            invoice_validate() validate number is in between range of authorization
        '''
        if context is None:
            context = {}
        
        electronic_doc=self.pool.get('electronic.document')
        inv_objs = self.browse(cr, uid, ids, context)
        for inv in inv_objs:                
            if inv.allow_electronic_document:
                
                if( not inv.invoice_address ):
                    raise osv.except_osv( "Error!", "El campo 'Direccion' es requerido para facturas electronicas" )
                if( not inv.partner_id.email):
                    raise osv.except_osv( "Error!", "Correo del cliente/proveedor es requerido para facturas electronicas" )
                
                if not inv.electronic_id:
                    inv.internal_number
                    values={
                            'document_id': str('account.invoice') + ',' + str(inv.id),
                            }
                    electronic_id=electronic_doc.create(cr,uid,values,context)
                    self.write(cr,uid,inv.id,{'electronic_id':electronic_id,
                                              })
                    inv = self.browse(cr, uid, inv.id, context) #actualizamos luego del write
                else:
                    electronic_id = inv.electronic_id.id
                
                if inv.electronic_id.state not in ['sri_authorized','sent','visualized']:    
                    #cvonectamos al webservice y generamos el documento electronico
                    electronic_obj = self.pool.get('electronic.document')
                    electronic_obj.get_access_key(cr, uid, [electronic_id]) #generamos la clave de acceso
                    context_for_electronic_docs = context

                    electronic_obj.attempt_electronic_document(cr, uid, [electronic_id], context)
                    #electronic = electronic_obj.browse(cr,uid,electronic_id)
                    #electronic_obj.write(cr,uid,electronic_id,{'document_id': inv.id})    
                else:
                    a = 1 #do nothing
                    
        #TODO: Si el documento electronico no fue aprobado debe permitir volver a generarlo
        result=super(account_invoice, self).validate_authorizations(cr, uid, ids, context)
#         if result != True:
#             return result #si las validaciones regulares fallan no generamos el doc electronico
        
        return result

    def invoice_validate(self, cr, uid, ids, context=None):
        '''
        Al aprobar la factura se emiten las guias de remision electronicas asociadas
        Los metodos de validacion de facturas van en este orden:
            action_date_assign()
            action_move_create()
            action_number() authorization_id which comes from the reply from SRI 
            invoice_validate() validate number is in between range of authorization
        '''
        if context is None:
            context = {}
        #TODO: Si el documento electronico no fue aprobado debe permitir volver a generarlo
        result=super(account_invoice, self).invoice_validate(cr, uid, ids, context)

        
#         if result != True:
#             return result #si las validaciones regulares fallan no generamos el doc electronico
#         
#         #ADVERTENCIA: Esta seccion aprobara las guias de remision pendientes
#         inv_objs = self.browse(cr, uid, ids, context)
#         for inv in inv_objs:                
#             #si existen guias de remision asociadas las procesamos, no se procesaron antes porque requerian conocer el numero de factura
#             if inv.picking_ids:
#             #no se requiere que la factura sea electronica sino que la guia sea electronica
#                 waybill_obj = self.pool.get('stock.picking.out') #la guia asociada siempre es de la clase stock.picking.out
#                 for waybill in waybill_obj.browse(cr, uid, inv.picking_ids, context):
#                     if waybill.state not in ['done']: #si no esta aprobada salta error
#                         raise osv.except_osv(_('ERROR!'), _('No se puede aprobar la factura pues tiene una guia de remision pendiente de aprobar'))
#                     
#                     if waybill.allow_electronic_document:
#                         #cvonectamos al webservice y generamos el documento electronico
#                         electronic_obj = self.pool.get('electronic.document')
#                         electronic_obj.get_access_key(cr, uid, [electronic_id]) #generamos la clave de acceso
#                         electronic_obj.attempt_electronic_document(cr, uid, [electronic_id], context)
#                         #electronic = electronic_obj.browse(cr,uid,electronic_id)
#                         #electronic_obj.write(cr,uid,electronic_id,{'document_id': inv.id})
#                 
#                 else:
#                     a = 1 #do nothing
        return result
    
    def _warning_msgs(self, cr, uid, ids, context=None):
        '''
        Metodo helper: Ayuda a redefinir con facilidad el campo tipo funcion asociado al metodo _get_warning_msgs
        Retorna una explicacion de porque la linea de factura esta en color rojo en la vista tree
        '''        
        warning_msgs = super(account_invoice, self)._warning_msgs(cr, uid, ids, context)
        inv = self.browse(cr, uid, ids, context)
        if inv.electronic_document_state in ['draft','valid', 'ca_signed', 'sri_received']:
            if len(warning_msgs) >0:
                warning_msgs += ', '
            warning_msgs += ELECTRONIC_PENDING_MSG 
        elif inv.electronic_document_state in ['not_valid', 'ca_rejected', 'sri_rejected', 'sri_not_authorized']:
            if len(warning_msgs) >0:
                warning_msgs += ', '
            warning_msgs += ELECTRONIC_REJECT_MSG
        return warning_msgs

account_invoice()
