from openerp.osv import fields, osv
import re
from openerp.tools.translate import _
import time
from string import split
from general_settings import ELECTRONIC_DOCUMENT_STATES
from general_settings import ELECTRONIC_DOCUMENT_STATES_HELP
from general_settings import ELECTRONIC_PENDING_MSG
from general_settings import ELECTRONIC_REJECT_MSG


class stock_picking(osv.osv):
    
    _inherit = "stock.picking"
    
    def _default_allow_electronic_document(self, cr, uid, context=None):
        '''
        Si el punto de impresion es electronico y tiene marcado el tipo de documento
        correcto entonces el stock.picking se marca como electronico
        '''
        if context is None:
            context = {}
        result = False
        
        if not context.get('is_waybill',False): #si no es guia de remision tampoco es documento electronico
            return result
        
        #obtenemos el tipo de documento
        document_invoice_type_id = self._doc_type(cr, uid, context)
        document_invoice_type_obj = self.pool.get('account.invoice.document.type')
        document_invoice_type = document_invoice_type_obj.browse(cr,uid,[document_invoice_type_id])[0]  
        
        #obtenemos la tienda
        printer_point_id = self._default_printer_point(cr, uid, context)
        printer_point_obj = self.pool.get('sri.printer.point')
        printer_point = printer_point_obj.browse(cr,uid,[printer_point_id])[0]  
        
        if not document_invoice_type_id or not  printer_point_id: #para evitar errores al instalar el modulo
            return result
        
        #vemos si la tienda demanda documento electronico para el tipo de documento a emitir
        if document_invoice_type.code == '06' and printer_point.allow_electronic_waybill and document_invoice_type.type == 'out_waybill': #guia de remision emitida
            result = True
        
        return result
    
    _columns = {

        'allow_electronic_document':fields.boolean('Electronic Document', help= 'Indicates if this is an electronic document instead of a paper one'),
                        
        'electronic_id':fields.many2one('electronic.document', 'Invoice Electronic Document', required=False,help="It is the relation between invoice electronic document",track_visibility='onchange'),
        
        'electronic_document_state': fields.related(
                                     'electronic_id',
                                     'state',
                                     type="selection",
                                     selection=ELECTRONIC_DOCUMENT_STATES,
                                     string="Electronic Document State", 
                                     readonly=True,
                                     help=ELECTRONIC_DOCUMENT_STATES_HELP,
                                     #track_visibility='onchange',
                                     store=False),
        
        'access_key': fields.related(
                                     'electronic_id',
                                     'access_key',
                                     type="char",
                                     relation="electronic.document",
                                     string="Access Key",
                                     readonly=True,
                                     help='Unique code to identify this document, is generated based on date, vat code, serial number, and other related fields',
                                     track_visibility='onchange',
                                     store=True),
                }
    
    _defaults = {
        'allow_electronic_document' : _default_allow_electronic_document,
        }
    
    
    def onchange_iswaybill(self, cr, uid, ids, waybill_number, is_waybill, context=None):
        """
        Evaluamos si requiere doc electronico
        """
        if context is None:
            context = {}
        res = super(stock_picking, self).onchange_iswaybill(cr, uid, ids, waybill_number, is_waybill, context)
        
        ctx = context
        ctx.update({'is_waybill':is_waybill})
        #TODO: Al usar la funcion por defecto solo funciona para un tipo de documento asociado a stock.picking
        allow_electronic_document = self._default_allow_electronic_document(cr, uid, ctx)
        res['value'].update({'allow_electronic_document': allow_electronic_document})
        return res
    
    def copy(self, cr, uid, id, default=None, context=None):
        ''' 
        No copia el documento electronico
        '''        
        if default is None:
            default = {}

        default.update({
            'electronic_id': False, #El documento electronico no debe copiarse 
            #pues esta diseniado para pertenecer a un solo documento
        })
        #TODO: Ponerlo para stock_picking y stock_picking_out 
        return super(stock_picking, self).copy(cr, uid, id, default, context)
    
    def unlink(self, cr, uid, ids, context=None, check=True):
        '''
        No se puede eliminar una factura si tiene un documento electronico asociado
        '''
        if context is None:
            context = {}
        result = False
        for waybill in self.browse(cr, uid, ids, context=context):
            if waybill.electronic_id:
                raise osv.except_osv(_('ERROR!'), _('No se puede borrar pues tiene un documento electronico asociado'))    
        result = super(stock_picking, self).unlink(cr, uid, [waybill.id], context=context)
        return result
    
    def onchange_document_invoice_type_id(self, cr, uid, ids, type=False, date_invoice=False, partner_id=False, sri_tax_support_id=False, document_invoice_type_id=False, printer_id=False, authorizations_id=False, company_id=False, context=None):
        '''
        El onchange del tipo de documento actualiza los valores de los campos:
        - allow_electronic_document
        '''
        
        res = super(account_invoice, self).onchange_document_invoice_type_id(cr, uid, ids, type, date_invoice, partner_id, sri_tax_support_id, document_invoice_type_id, printer_id, authorizations_id, company_id, context)
        res["value"].update({
                             'allow_electronic_document' : False
                             })
        
        if (document_invoice_type_id and printer_id and type in ['out_invoice','out_refund']):
            
            #obtenemos el tipo de documento
            document_invoice_type_obj = self.pool.get('account.invoice.document.type')
            document_invoice_type = document_invoice_type_obj.browse(cr,uid,[document_invoice_type_id])[0]  
            
            #obtenemos la tienda
            printer_point_obj = self.pool.get('sri.printer.point')
            printer_point = printer_point_obj.browse(cr,uid,[printer_id])[0]
            
                  
            #vemos si la tienda demanda documento electronico para el tipo de documento a emitir
            if document_invoice_type.code == '06' and printer_point.allow_electronic_invoices and document_invoice_type.type == 'out_invoice': #facturas de venta
                res["value"].update({'allow_electronic_document' : True})
        return res

    
    def onchange_printer_id(self, cr, uid, ids, type=False, date_invoice=False, partner_id=False, sri_tax_support_id=False, document_invoice_type_id=False, printer_id=False, authorizations_id=False, company_id=False, context=None):
        '''
        El onchange del punto de impresion actualiza los valores de los campos:
        - allow_electronic_document
        '''
        
        res = super(stock_picking, self).onchange_printer_id(cr, uid, ids, type, date_invoice, partner_id, sri_tax_support_id, document_invoice_type_id, printer_id, authorizations_id, company_id, context)
        res["value"].update({
                             'allow_electronic_document' : False
                             })
        
        if (document_invoice_type_id and printer_id and type in ['out_invoice','out_refund']):
            
            #obtenemos el tipo de documento
            document_invoice_type_obj = self.pool.get('account.invoice.document.type')
            document_invoice_type = document_invoice_type_obj.browse(cr,uid,[document_invoice_type_id])[0]  
            
            #obtenemos la tienda
            printer_point_obj = self.pool.get('sri.printer.point')
            printer_point = printer_point_obj.browse(cr,uid,[printer_id])[0]
            
                  
            #vemos si la tienda demanda documento electronico para el tipo de documento a emitir
            if document_invoice_type.code == '06' and printer_point.allow_electronic_invoices and document_invoice_type.type == 'out_invoice': #facturas de venta
                res["value"].update({'allow_electronic_document' : True})
        return res
    
    def waybill_attempt_electronic_document(self, cr, uid, ids, context=None):
        '''
        Al aprobar una guia de remision (stock.picking) se genera el documento electronico asociado
        '''
    
        if context is None:
            context = {}
            
        res = super(stock_picking,self).waybill_attempt_electronic_document(cr, uid, ids, context=context)
        
        for waybill in self.browse(cr, uid, ids, context=context):
            if waybill.allow_electronic_document and waybill.state in ['done']: #tomamos solo las transacciones completadas
                
                electronic_doc=self.pool.get('electronic.document')
                waybill_objs = self.browse(cr, uid, ids, context)

                if not waybill.electronic_id:
                    values={
                            'document_id': str(waybill._name) + ',' + str(waybill.id)
                            }
                    electronic_id=electronic_doc.create(cr,uid,values,context)
                    self.write(cr, uid, waybill.id, {'electronic_id':electronic_id})
                    waybill = self.browse(cr, uid, waybill.id, context) #actualizamos luego del write
                else:
                    electronic_id = waybill.electronic_id.id
                
                if waybill.electronic_id.state not in ['sri_authorized','sent','visualized']:    
                    #conectamos al webservice y generamos el documento electronico
                    electronic_obj = self.pool.get('electronic.document')
                    electronic_obj.get_access_key(cr, uid, [electronic_id]) #generamos la clave de acceso
                    context_for_electronic_docs = context

                    electronic_obj.attempt_electronic_document(cr, uid, [electronic_id], context)
                    #electronic = electronic_obj.browse(cr,uid,electronic_id)
                    #electronic_obj.write(cr,uid,electronic_id,{'document_id': inv.id})
                else:
                    a = 1 #do nothing
          
        return res

    def _warning_msgs(self, cr, uid, ids, context=None):
        '''
        Metodo helper: Ayuda a redefinir con facilidad el campo tipo funcion asociado al metodo _get_warning_msgs
        Retorna una explicacion de porque la linea de factura esta en color rojo en la vista tree
        '''        
        warning_msgs = super(stock_picking, self)._warning_msgs(cr, uid, ids, context)
        waybill = self.browse(cr, uid, ids, context)
        if waybill.electronic_document_state in ['draft','valid', 'ca_signed', 'sri_received']:
            if len(warning_msgs) >0:
                warning_msgs += ', '
            warning_msgs += ELECTRONIC_PENDING_MSG 
        elif waybill.electronic_document_state in ['not_valid', 'ca_rejected', 'sri_rejected', 'sri_not_authorized']:
            if len(warning_msgs) >0:
                warning_msgs += ', '
            warning_msgs += ELECTRONIC_REJECT_MSG
        return warning_msgs

stock_picking()
