from openerp.osv import fields, osv
import re
from openerp.tools.translate import _
import time
from string import split

class account_journal(osv.osv):
    _inherit = "account.journal"
    _columns = {
                'allow_electronic_document':fields.boolean('Create Electronic Document', help= 'If set to True then create a Invoice electronic document for this invoice'),
                }
    
account_journal()