import logging

from openerp.osv import fields, osv
from openerp import pooler
from openerp.tools.translate import _


class res_company(osv.Model):
    _inherit = 'res.company'
    _columns = {
        'environment_type':fields.selection([
                        ('1', 'Testing Environment (Ambiente de Pruebas)'),('2','Production Environmet (Ambiente de Produccion)')],'Type of Environment', required=False, help='1. Testing Environment (Ambiente de Pruebas), 2. Production Environmet (Ambiente de Produccion)'),
        'url':fields.char('URL'),
        'user':fields.char('User'),
        'password':fields.char('Password'),
        'authentication':fields.selection([('none','None'),('password_text','Password Text'),('password_digest','Password Digest')],'Authentication'),
        'waiting_msg': fields.text('Mensaje Pendiente', help = "Mensaje agregado al pie del documento impreso cuando este aun no ha sido procesado por el SRI"),
        'ok_msg': fields.text('Mensaje OK', help = "Mensaje agregado al pie del documento impreso"),
    }
    _defaults={
              'environment_type':'1',
              
              'waiting_msg': 'DOCUMENTO SIN VALIDEZ TRIBUTARIA\n\
              SU DOCUMENTO ELECTRONICO ESTA SIENDO PROCESADO, PRONTO RECIBIRA UNA NOTIFICACION EN SU CORREO ELECTRONICO',

              'ok_msg': 'Puede consultar su documento electronico en demo.facturadeuna.com',
              
              }
res_company()        