# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: TRESCloud Cia Ltda, Andrea Garcia, Patricio Rangles
# Copyright (C) 2015  wwww.trescloud.com
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from openerp.osv import fields, osv
import re
from openerp.tools.translate import _
import time
from string import split
from openerp import netsvc
from suds.client import Client  #para los webservices
from suds.wsse import * # para autenticacion texto plano para ndocs

from general_settings import ELECTRONIC_DOCUMENT_STATES
from general_settings import ELECTRONIC_DOCUMENT_STATES_HELP
from general_settings import XSD_SRI_110_FACTURA
from general_settings import XSD_SRI_110_GUIA_REMISION
from general_settings import XSD_SRI_110_NOTA_CREDITO
from general_settings import validate_xml_vs_xsd
from general_settings import strip_accents_and_ntildes
from general_settings import clean_xml

import logging

_logger = logging.getLogger(__name__)

class electronic_document(osv.osv):
    _name = 'electronic.document'    
    _inherit = ['mail.thread']
        
    def _links_get(self, cr, uid, context=None):
        """Gets links value for reference field"""
        return [(u'account.invoice', u'Invoice/Debit Note/Credit Note'), 
                (u'stock.picking', u'Waybill'),
                (u'account.withhold', u'WIthhold')]
 
    _rec_name = 'access_key'
    
    def _compute_type(self, cr, uid, ids, name, args, context=None):
        
        for electronic in self.browse(cr,uid,ids,context):
            #code_document_type=str(electronic.document_id.document_invoice_type_id.code)
            model = electronic.document_id._name

            if model == 'account.withhold':
                object='ecuadorian_electronic_withholding'
            else:

                code_document_type=str(electronic.document_id.document_invoice_type_id.code)
                if model == 'account.invoice':
                    if code_document_type in ['18','01']: #facturas para clientes
                        object='ecuadorian_electronic_invoice'
                    elif code_document_type == '04': #notas de credito
                        object='ecuadorian_electronic_credit_note'
                    elif code_document_type == '05': #notas de debito
                        object='ecuadorian_electronic_debit_note'
                elif model == 'stock.picking':
                    if code_document_type == '06': #facturas para clientes
                        object='ecuadorian_electronic_waybill'
                #elif model == 'account.withhold':
                #    if code_document_type == '07': #facturas para clientes
                #        object='ecuadorian_electronic_withholding'
            res = {}
            res[electronic.id] = object
        return res
    _columns={
              'url': fields.char('Url', size=1024,help="The URL in which the end user can query and download the electronic document"),
              'authorized_xml_file': fields.text('Authorized XML File',help='The XML that represents the electronic document already validated and aproved by the tax authority',track_visibility='onchange'),
              'authorizations_id':fields.many2one('sri.authorizations', 'Authorization', required=False,help="It is for the authoritzation to issue the document, select a release from the list. Only existing authorizations are displayed according to the date of the document"),
              
              'document_id':fields.reference('Document', selection=_links_get, size=128),
              'document_type': fields.function(_compute_type,string="Document Type",
                                               type='selection', store=True, method=True,
                                               selection= [('ecuadorian_electronic_invoice',_('Electronic Invoice')),
                        ('ecuadorian_electronic_credit_note', _('Electronic Credit Note')),
                        ('ecuadorian_electronic_debit_note', _('Electronic Debit Note')),
                        ('ecuadorian_electronic_waybill', _('Electronic Waybill')),
                        ('ecuadorian_electronic_withholding', _('Electronic Withholding'))],
                                               help="Document Type means if the document is:\n\
              - Invoices\n\
              - Waybills\n\
              - Refunds\n\
              - Withholdings\n\
              "),
              #TODO: Almacenar los datos de usuario y clave para entregarse al 
              #usuario final.
              
              'state': fields.selection(ELECTRONIC_DOCUMENT_STATES, 
                                        'Status', select=True, 
                                        readonly=True,
                                        help=ELECTRONIC_DOCUMENT_STATES_HELP,
                                        track_visibility='onchange'),
              
              'company_id': fields.many2one('res.company', 'Company', required=False),

              'access_key':fields.char('Access Key',readonly=True,
                                       help='Unique code to identify this document, is generated based on date, vat code, serial number, and other related fields',
                                       track_visibility='onchange'),

              'environment_type':fields.selection([('1', 'Testing Environment (Ambiente de Pruebas)'),('2','Production Environmet (Ambiente de Produccion)')],
                                                   'Type of Environment', 
                                                   required=False, 
                                                   help='1. Testing Environment (Ambiente de Pruebas), 2. Production Environmet (Ambiente de Produccion)',
                                                   track_visibility='onchange'
                                                  ),

              'emission_type':fields.selection([('normal', 'Normal'),('contingency','Contingency')],
                                               'Type of Emision', 
                                               required=False, 
                                               help='Normal, when all systems are online \n Contingency, When SRI systems are down',
                                               track_visibility='onchange'
                                               ),
              
              'authorization_date': fields.datetime('Authorization Date',
                                                    help="Date of authorization by SRI, in GMT -5", 
                                                    track_visibility='onchange'),
              'cancellation_date': fields.date("Fecha de anulacion"),
              'cancellation_user': fields.many2one("res.users", "Responsable"),
              'previous_state':    fields.selection(ELECTRONIC_DOCUMENT_STATES,
                                                    'Estado antes de anulacion', select=True,
                                                    readonly=True,
                                                    help=ELECTRONIC_DOCUMENT_STATES_HELP,),

              }
    _defaults={
               'state': 'draft',
               'company_id': lambda s, cr, uid, c: s.pool.get('res.company')._company_default_get(cr, uid, 'sale.shop', context=c),
               }
    
    _sql_constraints = [
        ('name_uniq', 'UNIQUE (access_key)',  'Ya existe otro documento electronico con la misma clave de acceso !')
    ]
    
    def unlink(self, cr, uid, ids, context=None, check=True):
        '''
        No se puede eliminar un documento electronico si el mes esta cerrado 
        Si se puede a pesar que no este en estado borrador pues 
        '''
        
     
        if context is None:
            context = {}
        result = False
        for document in self.browse(cr, uid, ids, context=context):
            if document.environment_type == "1": #ambiente de pruebas
                return super(electronic_document, self).unlink(cr, uid, [document.id], context=context)
        raise osv.except_osv(_('ERROR!'), _('Solo se permite eliminar documentos electronicos de pruebas'))    

    def cancel_document(self, cr, uid, ids, context=None):
        return { 'type'      : 'ir.actions.act_window',
                 'name'      : 'Anular Documento Electronico',
                 'view_type' : 'form',
                 'view_mode' : 'form,tree',
                 'res_model' : 'electronic.document.cancellation.wizard',
                 'target'    : 'new',
               }

    def electronic_print(self, cr, uid, ids, context=None):
        '''
        Retorna como string el contenido del reporte xml de documento electronico 
        El metodo es capaz de reconocer el modelo (ej retencion, factura, etc)
        '''
        #TODO: Mover a la funcion que genera el reporte
        if context is None:
            context = {}
        
        if not context.get('lang',False) or not context.get('tz',False) or not context.get('uid',False):  #TODO: Investigar el error, a veces el context llega incompleto y el reporte no se puede generar
            user_obj=self.pool.get('res.users')
            user = user_obj.browse(cr,uid,[uid],context)[0]
            if not context.get('lang',False):
                context.update({'lang': user.lang})
            if not context.get('tz',False):
                context.update({'tz': user.tz})
            if not context.get('uid',False):
                context.update({'uid': uid})

        
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        electronic=self.browse(cr,uid,ids,context)[0]
        model = electronic.document_id._name
        document_obj=self.pool.get(model)
        document_id=electronic.document_id.id
        electronic.access_key
        
        datas = {
             'ids': document_id,
             'model': model,
             'form': document_obj.read(cr, uid, [document_id], context=context)
        }
        
        #generamos y validamos el documento
        xsd_content = ''
        xml_content = '' 

        if model == 'account.invoice':
            #TODO: evaluar los subtipos de facturas
            code_document_type=str(electronic.document_id.document_invoice_type_id.code)
            if code_document_type == '18': #facturas para clientes
                object=unicode('ecuadorian_electronic_invoice')
            elif code_document_type == '04': #notas de credito
                object=unicode('ecuadorian_electronic_credit_note')
            elif code_document_type == '05': #notas de debito
                object=unicode('ecuadorian_electronic_debit_note')
            else:
                raise osv.except_osv(_('ERROR!'), _('No es un tipo de documento valido para electronicos'))
        elif model == 'account.withhold':
            object=unicode('ecuadorian_electronic_withhold')
        elif model in ['stock.picking','stock.picking.out']:
            object=unicode('ecuadorian_electronic_waybill')
        else:
            raise osv.except_osv(_('ERROR!'), _('No se ha implementado documentos electronicos para este modelo de datos'))

        obj = netsvc.LocalService('report.'+object)
        (xml_content, format) = obj.create(cr, uid, [document_id], datas, context)
        #print xml_content

        #Removemos las tildes, enies y formateamos el XML retornado
        xml_content = strip_accents_and_ntildes(xml_content)
        ####print xml_content
        xml_content = clean_xml(xml_content)
        ####print xml_content

        try: #validamos el XML contra el XSD
            if electronic.document_id._name == 'account.invoice':
                if electronic.document_id.document_invoice_type_id.code in ['18','01']:
                    validate_xml_vs_xsd(xml_content, XSD_SRI_110_FACTURA)
                elif electronic.document_id.document_invoice_type_id.code == '04':
                    validate_xml_vs_xsd(xml_content, XSD_SRI_110_NOTA_CREDITO)
                #TODO: Implementar validacion XSD para notas de debito
                #elif electronic.document_id.document_invoice_type_id.code == '05':
            elif electronic.document_id._name in ['stock.picking','stock.picking.out']:
                validate_xml_vs_xsd(xml_content, XSD_SRI_110_GUIA_REMISION)
            #TODO: Implementar validacion XSD para retenciones
            #elif electronic.document_id._name in ['account.withhold']:
            #    validate_xml_vs_xsd(xml_content, xsd_instance)
        except Exception,detail: 
            raise osv.except_osv(_('ERROR EN EL XML!'), _('No se ha enviado al servidor: ¿quiza los datos estan mal llenados?:')+str(detail))
            #xxx=3
        #agregamos cabecera xml
        #xml_content = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?> \n' + xml_content 
        
        return xml_content 
        #si quiero guardarlo en una variable del objeto: 
        #self.write(cr,uid,ids,{'text':result})
        #return True

    def directRequest(methodName, params):
        '''
        Funcion Helper que permite invocar metodos del webservice forma mas facil
        Calling a method:
           api - instance of the suds.Client class
           methodName - method name
           params - input parameters
        If an error occurs the program closes, otherwise it returns the result of calling the method
        '''
        try:
            result = api.service['APIPort'][methodName](params)
            return result
        except WebFault, err:
            print unicode(err)
        except:
            err = sys.exc_info()[1]
            print 'Other error: ' + str(err)
        exit(-1)

    def pre_create_authorization(self, cr, uid, ids, authorization_number, document_date, context=None):
        '''
        metodo helper que carga los valores (vals) para la autorizacion en base a la respuesta del
        SRI sobre documentos electronicos. El numero de autorizacion esta conformado 
        por los siguientes datos:
            1. Fecha y Hora de Autorizacion ddmmaaaahhmmss 14 digitos 
            2. Numero de RUC 1234567890001 13  digitos
            3. Codigo Numerico 1234567891 10 digitos
        
        @authorization_number: el numero de autorizacion retornado por el SRI
        @document_date: la fecha en la q se genero el documento pues puede diferir de la fecha de la autorizacion por demoras del SRI por ejemplo
        '''
        
        if len(authorization_number) != 37: #si es numero valido y electronico
            raise osv.except_osv(_('ERROR!'), _('El numero de autorizacion es invalido'))
        #TODO: Validar que no exista esta autorizacion previamente creada
        #instanciamos el documento electronico y el documento relacionado (ej. la factura)
        
        electronic=self.browse(cr,uid,ids)[0]
        
        #construimos el diccionario de los vals para la creacion de una autorizacion
        
        #obtenemos el tipo de documento
        document_invoice_type_id = False
        if electronic.document_id._name == 'account.invoice':
            document_invoice_type_id = electronic.document_id.document_invoice_type_id.id
            first_sequence = last_sequence = electronic.document_id.internal_number[9:18]
            type = electronic.document_id.type 
            
        elif electronic.document_id._name == 'account.withhold':
            #TODO ALTATEC FIX THIS DIRECT ASSIGNMENT OF ID OF PURCHASE WITHHOLD
            document_invoice_type_id = 9
            first_sequence = last_sequence = electronic.document_id.number[9:18]
            type = electronic.document_id.transaction_type
        elif electronic.document_id._name == 'stock.picking':
            document_invoice_type_id = 0 #TODO: Implementar
            first_sequence = last_sequence = electronic.document_id.waybill_number[9:18]
            type = electronic.document_id.type 
        else:
            raise osv.except_osv(_('ERROR!'), _('No se reconoce el objeto asociado al documento electronico'))
        
        
        #obtenemos la fecha, es igual para inicio y fin
        #start_end_date = time.strftime('%Y-%m-%d', time.strptime(authorization_number[0:14], '%d%m%Y%H%M%S'))
        start_end_date = document_date
        
        vals = {
            'name': authorization_number, 
            'auto_printer': False, 
            'invoice_ele': True, 
            'shop': electronic.document_id.printer_id.shop_id.number, 
            'printer_point': electronic.document_id.printer_id.name, 
                         
            'date_invoice': start_end_date,
            'start_date': start_end_date,
            'expiration_date': start_end_date,
            
            'first_sequence': first_sequence,
            'last_sequence': last_sequence, 
             
            'autorization_partners': False, 
            'type': type, 
            'partner_id': electronic.company_id.id, #siempre es emitido por mi empresa 
             
            'document_invoice_type_id': document_invoice_type_id,

            'message_ids': False, 
            'message_follower_ids': False, 
            }
        return vals
            
    def parse_error_messages(self, cr, uid, result, context=None):
        '''
        Convierte la respuesta del webservice en un string que se puede mostrar al cliente
        Ejemplo
            DocumentoRespuesta = {
            'ClaveAcceso' : "2112201401179134365400110010010000000060000000014",
            'ClaveContingencia' : None,
            'Detalle' : "ERROR SECUENCIAL REGISTRADO  ",
            'EstadoActual' : "NO AUTORIZADO",
            'FechaAutorizacion' : None,
            'Mensaje' : "El documento ya está registrado en el DocServer. ",
            'NumeroAutorizacion' : None,
            'ProcesoOK' : False,
            }
        '''
        text = "\n\n"
        #controlamos variables vacias
        if result['ClaveAcceso'] is None:
            result['ClaveAcceso'] = 'None'
        if result['ClaveContingencia'] is None:
            result['ClaveContingencia'] = 'None'
        if result['Detalle'] is None:
            result['Detalle'] = 'None'
        if result['ClaveAcceso'] is 'None':
            result['ClaveAcceso'] = 'None'
        if result['EstadoActual'] is None:
            result['EstadoActual'] = 'None'
        if result['FechaAutorizacion'] is None:
            result['FechaAutorizacion'] = 'None'
        if result['Mensaje'] is None:
            result['Mensaje'] = 'None'
        if result['NumeroAutorizacion'] is None:
            result['NumeroAutorizacion'] = 'None'
        if result['Xml'] is None:
            result['Xml'] = 'None'
                    
        text += 'ClaveContingencia: ' + str(result['ClaveContingencia'])  + '\n'
        text += 'Clave de Acceso: ' + strip_accents_and_ntildes(result['ClaveAcceso']).encode('ascii', 'ignore') + '\n'
        text += 'Detalle: ' + strip_accents_and_ntildes(result['Detalle']).encode('ascii', 'ignore')  + '\n'
        text += 'EstadoActual: ' + strip_accents_and_ntildes(result['EstadoActual']).encode('ascii', 'ignore') + '\n'
        text += 'FechaAutorizacion: ' + str(result['FechaAutorizacion']) + '\n'
        text += 'Mensaje: ' + strip_accents_and_ntildes(result['Mensaje']).encode('ascii', 'ignore') + '\n'
        text += 'NumeroAutorizacion: ' + str(result['NumeroAutorizacion']) + '\n'
        #text += 'Xml: ' + str(result['Xml']) + '\n'
        
        return text
    
    def attempt_electronic_document(self, cr, uid, ids, context=None):
        '''
        Intenta ejecutar el proceso completo de generar el documento electronico
        Retoma desde el estado actual del documento electronico. 
        retorna un diccionario con los siguientes valores:
            ClaveAcceso = None
            ClaveContingencia = None
            Detalle = "The 'fechaEmision' element is invalid - The value '2014-11-11' is invalid according to its datatype 'fechaEmision' - The Pattern constraint failed."
            EstadoActual = "NO VALIDADO"
            FechaAutorizacion = None
            Mensaje = "El xml no es valido para el esquema actual. Revise la estructura del xml. "
            NumeroAutorizacion = None
            ProcesoOK = False
            Xml = None
        '''
        
        # Iniciamos el api
        electronic_document=self.browse(cr,uid,ids)[0]
        WSDL_URL = electronic_document.company_id.url or "https://200.31.29.226:3405/docserver.test/facturacion.svc?wsdl"
        USERNAME = electronic_document.company_id.user or "ndev"
        PASSWORD = electronic_document.company_id.password  or "ndev"

        try:
            client = Client(WSDL_URL)
        except Exception,detail: 
            raise osv.except_osv(_('ERROR FUERA DE LINEA!'), _('El servidor de documentos electronicos parece estar fuera de linea, ¿quiza la url configurada es incorrecta?:')+str(detail))

        try:
            security = Security()
            token = UsernameToken(USERNAME, PASSWORD)
            security.tokens.append(token)
            client.set_options(wsse=security)
        except Exception,detail:
            # ejemplo el servidor remoto esta fuera de linea
            raise osv.except_osv(_('ERROR DE AUTENTICACION!'), _('La configuracion de autenticacion ha fallado, ¿quizá el usuario y clave?:')+str(detail))

        draft_electronic_document_in_xml = self.electronic_print(cr, uid, ids, context)

        try:
            _logger.debug(str(draft_electronic_document_in_xml))
            result = client.service.EnviarDocumento(draft_electronic_document_in_xml)
        except Exception,detail:
            error = strip_accents_and_ntildes(detail.args[0])
            raise osv.except_osv(_('ERROR!'), _('Error en la comunicacion con el servidor de documentos electronicos, contacte a soporte tecnico:')+error)
        
        #TODO: Si el documento ya existe debe bloquearse el metodo
        # se debe tambien crear un boton para consultas que no reescriba los datos
        
        if result['EstadoActual'] == "VALIDADO": #aprobamos la factura para continuar con las operaciones, posteriormente emitiremos el documento final
            self.pre_aprove_electronic_document(cr, uid, ids, result, electronic_document, context)
            self.set_valid(cr, uid, ids)
            return True
        
        elif result['EstadoActual'] == "NO VALIDADO":
            '''
            Cuando el proxy de docs electronicos ha marcado por mantenimiento un documento que
            previamente era VALIDO como NO VALIDO debemos poder actualizar el estado
            se hace con el if para evitar hacer un commit de una transaccion invalida
            You should NEVER call cr.commit() yourself, UNLESS you have created your own database cursor explicitly! And the situations where you need to do that are exceptional!            
            And by the way if you did create your own cursor, then you need to handle error cases and proper rollback, as well as properly close the cursor when you're done with it.
            '''

            document = self.browse(cr,uid,ids,context)[0]
            if document.state in ['valid']:
                self.set_not_valid(cr, uid, ids) #TODO: Se podria usar un cr diferente para no afectar gravemente otras transacciones
                cr.commit()
                raise osv.except_osv(_('NO VALIDADO!'), _('Por mantenimiento se ha cambiado el estado de VALIDO a NO VALIDO: ')+str(result['Detalle'].encode('ascii', 'ignore'))) 
            raise osv.except_osv(_('NO VALIDADO!'), _('No cumple con el esquema del SRI, no se ha procesado, debe revisar los datos ingresados:')+str(result['Detalle'].encode('ascii', 'ignore')))
        
        elif result['EstadoActual'] == "FIRMADO":
            self.pre_aprove_electronic_document(cr, uid, ids, result, electronic_document, context)
            self.set_signed(cr, uid, ids)
            return True

        elif result['EstadoActual'] == "NO FIRMADO":
            raise osv.except_osv(_('NO FIRMADO!'), _('Error al firmar digitalmente, revise los datos enviados:')+self.parse_error_messages(cr,uid,result,context))

        elif result['EstadoActual'] == "RECIBIDA":
            self.pre_aprove_electronic_document(cr, uid, ids, result, electronic_document, context)
            self.set_signed(cr, uid, ids)
            return True 
        
        elif result['EstadoActual'] == "DEVUELTA":
            raise osv.except_osv(_('DEVUELTA!'), _('Enviado al SRI pero devuelta, debe corregir los errores:')+self.parse_error_messages(cr,uid,result,context))
         
        elif result['EstadoActual'] == "AUTORIZADO":
            self.pre_aprove_electronic_document(cr, uid, ids, result, electronic_document, context)
            self.set_authorized(cr, uid, ids)            
            return True

        elif result['EstadoActual'] == "NO AUTORIZADO":
            document = self.browse(cr,uid,ids,context)[0]
            if document.state in ['ca_signed','sri_received']: #desde estos estados intermedios se puede llegar a un estado de NO AUTORIZADO
                self.set_not_authorized(cr, uid, ids) #TODO: Se podria usar un cr diferente para no afectar gravemente otras transacciones
                cr.commit() 

            raise osv.except_osv(_('NO AUTORIZADO!'), _('El SRI no autorizo el documento, debe revisar los datos ingresados:')+self.parse_error_messages(cr,uid,result,context))

        else:
            raise osv.except_osv(_('Error!'), _('Error no identificado:')+self.parse_error_messages(cr,uid,result,context))
        return True


    def pre_aprove_electronic_document(self, cr, uid, ids, result, electronic_document, context=None):
        '''
        La pre_aprobacion del documento electronico implica que se asume que sera aprobado en un futuro
        Es util para continuar con las transacciones cuando el SRI no esta disponible, es decir en los 
        estados:
        1       VALIDADO
        3       FIRMADO
        5       RECIBIDA
        7       AUTORIZADO
        '''
        
        emission_type = 'normal' #TODO: Usar tambien emision de contingencia
        if result['ClaveContingencia'] != None:
            emission_type = 'contingency'
        self.write(cr, uid, ids, {
                                  'environment_type' : electronic_document.company_id.environment_type,
                                  'emission_type' : emission_type,
                                  'authorization_date' : result['FechaAutorizacion'],
                                  'authorized_xml_file' : result['Xml'],
                                   })
                
        #seleccionamos o creamos la autorizacion y la asociamos al documento electronico y al documento relacionado (ejemplo a la factura o retencion)
        auth_obj = self.pool.get('sri.authorizations')
        authorizations_id = False
        if result['EstadoActual'] in ['VALIDADO','FIRMADO','RECIBIDA',]: #usamos la autorizacion 0000000000 que siginifica pendiente
            null_authorizations_ids=auth_obj.search(cr,uid,[('name','=','0000000000')])
            authorizations_id = null_authorizations_ids[0] #tomamos la primera
        elif result['EstadoActual'] in ['AUTORIZADO',]: #creamos la autorizacion
            #TODO: Definir si de pronto debe buscar la autorizaicon ya existente en caso de haberla
            authorizations_obj=self.pool.get('sri.authorizations')
            context_for_authorization = context
            document_date = False
            if electronic_document.document_id._name == 'account.invoice':
                context_for_authorization.update({'type': electronic_document.document_id.type})
                document_date = electronic_document.document_id.date_invoice
            elif electronic_document.document_id._name == 'account.withhold':
                context_for_authorization.update({'type': electronic_document.document_id.transaction_type})
                document_date = electronic_document.document_id.creation_date
            elif electronic_document.document_id._name == 'stock.picking':
                context_for_authorization.update({'type': electronic_document.document_id.type})
                document_date = electronic_document.document_id.date #TODO: Remover los minutos y segundos porq este es un datetime y requerimos un date
            
            vals = self.pre_create_authorization(cr, uid, ids, result['NumeroAutorizacion'], document_date, context)
            authorizations_id = authorizations_obj.create(cr, uid, vals, context_for_authorization)
            
        related_document_obj=self.pool.get(electronic_document.document_id._name)
        related_document_obj.write(cr, uid, electronic_document.document_id.id, 
                          {'authorizations_id' : authorizations_id})
        self.write(cr, uid, electronic_document.id,{'authorizations_id' : authorizations_id})
        
        return True

       
    def set_not_valid(self, cr, uid, ids):
        '''
        Cambia el estado a NO VALIDO,
        Ejemplo cuando el XML no es valido
        Metodo heredado e implementado por los modulos especificos de cada proveedor de docs electronicos
        '''
        self.write(cr, uid, ids, { 'state' : 'not_valid' })
        return True

    def set_valid(self, cr, uid, ids):
        '''
        Cambia el estado a VALIDADO,
        Ejemplo cuando el XML es valido pero no se ha podido firmar ni enviar al sri aun
        Metodo heredado e implementado por los modulos especificos de cada proveedor de docs electronicos
        '''
        self.write(cr, uid, ids, { 'state' : 'valid' })
        return True
    
    

    def set_signed(self, cr, uid, ids):
        '''
        Cambia el estado a FIRMADO,
        Ejemplo cuando el XML ha sido firmado pero el SRI esta offline y no se ha podido utilizar claves de contingencia
        Metodo heredado e implementado por los modulos especificos de cada proveedor de docs electronicos
        '''
        self.write(cr, uid, ids, { 'state' : 'ca_signed' })
        return True
    
    def set_received(self, cr, uid, ids):
        '''
        Cambia el estado a RECIBIDO,
        Ejemplo cuando el XML ha sido recibido por el SRI pero no ha sido procesado aun
        Metodo heredado e implementado por los modulos especificos de cada proveedor de docs electronicos
        '''
        self.write(cr, uid, ids, { 'state' : 'sri_received' })
        return True    
    
    def set_not_authorized(self, cr, uid, ids):
        '''
        Cambia el estado a NO AUTORIZADO
        Ejemplo cuando el XML es rechazado por el SRI
        Metodo heredado e implementado por los modulos especificos de cada proveedor de docs electronicos
        '''
        self.write(cr, uid, ids, { 'state' : 'sri_not_authorized' })
        return True

    def set_authorized(self, cr, uid, ids):
        '''
        Cambia el estado a autorizado
        Ejemplo cuando el XML ha sido validado por el SRI y se cuenta con el codigo de autorizacion
        Metodo heredado e implementado por los modulos especificos de cada proveedor de docs electronicos
        '''
        self.write(cr, uid, ids, { 'state' : 'sri_authorized' })
        return True

    def set_sent_to_partner(self, cr, uid, ids):
        '''
        Cambia el estado a enviado a la empresa
        Ejemplo cuando el XML validado ha sido enviado al cliente por email
        Metodo heredado e implementado por los modulos especificos de cada proveedor de docs electronicos
        '''
        self.write(cr, uid, ids, { 'state' : 'sent' })
        return True

    def set_visualized(self, cr, uid, ids):
        '''
        Cambia el estado a visualizado
        Ejemplo cuando el XML ha sido visto por el cliente, en la normativa vigente al 2013 era un requerimiento pero en 
        el 2014 se removio el requerimiento.
        Metodo heredado e implementado por los modulos especificos de cada proveedor de docs electronicos
        '''
        self.write(cr, uid, ids, { 'state' : 'visualized' })
        return True
    
    def action_cancel_draft(self, cr, uid, ids, *args):
        '''
        Permite volver al inicio del flujo de documento electronico
        Probablemente este metodo no deberia ser utilizado nunca
        '''
        self.write(cr, uid, ids, {'state':'draft'})
        wf_service = netsvc.LocalService("workflow")
        for elec_id in ids:
            wf_service.trg_delete(uid, 'electronic.document', elec_id, cr)
            wf_service.trg_create(uid, 'electronic.document', elec_id, cr)
        return True
    
    def GenerateModulus11(self,Numero):
        '''
        Input en la forma: 
        Numero=2334568734
        '''
        if str(Numero) != Numero:
           Numero = str(Numero)
    
        x = 0
        factor = 2
        for c in reversed(Numero):
    
            try:
                int(c)
            except ValueError:
                # not numeric
                continue           
            else:
                # numeric
                x += int(c) * factor
                factor += 1
                if factor == 8:
                    factor = 2
        #Calcula el digito de control.
        Control = (11 - (x % 11)) % 11
        return Control
    
    def get_access_key(self, cr, uid, ids, *args):
        '''
        Genera la clave de acceso del documento electronico
        '''
        #inv_obj=self.pool.get('account.invoice')
        electronic=self.browse(cr,uid,ids)[0]
        if not electronic.document_id:
            return False
        related_document=electronic.document_id
        
        #cargamos los datos generales
        cadena = ''
        data_model = related_document._name
        date = '' #fecha para la clave de acceso, primer valor
        code_document_type = ''
        environment_type = ''
        serie = ''
        cod_number = '00000000'
        type_emm='1'
        
        if data_model == 'account.invoice':
            date=related_document.date_invoice
            code_document_type=str(related_document.document_invoice_type_id.code)
            if code_document_type in ['18','01']: #facturas para clientes
                code_document_type = '01' #solo en facturas requiere redifinicion del codigo
            serie = related_document.internal_number
        elif data_model == 'account.withhold':
            date=related_document.creation_date
            code_document_type = '07'
            serie = related_document.number
        elif data_model == 'stock.picking':
            date= related_document.date
            code_document_type = '06' #TODO basarse en el tipo de documento
            serie = related_document.waybill_number
        else:
            raise osv.except_osv(_('ERROR!'), _('No se ha implementado documentos electronicos para este modelo de datos'))
        
        '''first value date'''
        fdate=date.find('-')
        fdateo=date.find('/')
        if fdate!=-1:
            date=date.split('-')
            cadena=""+date[2][:2]+date[1]+date[0] #si es un datetime se remueven las horas minutos y segundos
        elif fdateo!=-1:
            date=date.split('/')
            cadena=""+date[2][:2]+date[1]+date[0] #si es un datetime se remueven las horas minutos y segundos
        
        '''second value tipo de comprobante, tabla 4 de la ficha tecnica de docs electronicos'''
        cadena += code_document_type

        '''third value VAT number'''         
        vat=str(related_document.company_id.partner_id.vat) #Es el RUC de la empresa que emite el documento
        cadena=cadena+vat[2:] #TODO: Usar un metodo o variable que retorne el ruc ya limpio
        
        '''fourth value tipo de ambiente'''
        environment_type=related_document.company_id.environment_type
        if not environment_type:
            environment_type='1'
        cadena=cadena+environment_type
        
        '''fifth value serie and sixth value number'''
        try: #si no tiene guienes en la mitad en la forma numero-numero-numero da error
            serie=serie.split('-')
            cadena=cadena+serie[0]+serie[1]+serie[2]
        except Exception,detail: 
            raise osv.except_osv(_('ERROR EN NUMERO DE DOCUMENTO!'), _('El numero de documento ')+str(serie) + _('presenta errores:') + str(detail))
        
        '''seven value numeric code '''
        cadena += cod_number
        
        '''eigth value tipo emision siempre sera 1 '''
        cadena += type_emm
        
        number_c=int(cadena)
        digit_ver=self.GenerateModulus11(number_c)
        
        # Cuando el resultado del dígito verificador obtenido 
        # sea igual a once (11), el digito verificador será el cero (0) y cuando el resultado del dígito 
        # verificador obtenido sea igual a diez 10, el digito verificador será el uno (1). 

        if digit_ver == 10:
            digit_ver = 1
        elif digit_ver == 11:
            digit_ver = 0
        cadena=cadena+str(digit_ver)
        self.write(cr, uid, ids, {'access_key':cadena})
        return cadena 
    
    def download_xml(self, cr, uid, ids, context=None):

        return {
              'type': 'ir.actions.report.xml',
              'report_name': 'ecuadorian_electronic_xml_download',    # the 'Service Name' from the report
              'datas' : {
                  'model' : 'electronic.document',    # Report Model
                  'res_ids' : ids
             }
                }
    
    def process_stuck_electronic_documents(self, cr, uid, ids=None, context=None):
        '''
        Busca los documentos electronicos sin procesar de los ultimas horas
        e intenta procesarlos mediante consultas al proxy de documentos electronicos
            :param list ids: listado opcional de ids a procesar, si se provee no se 
                            realiza busqueda alguna.
            :parm dict context: si una llave 'filters' se encuentra presente en el context
                            dicho valor sera usado como un filtro adicional para restringir
                            aun mas los documentos a re-procesar
        '''
        if context is None:
            context = {}

        if not ids:
            filters = [('state', 'in', ['draft','valid', 'ca_signed', 'sri_received'])] #TODO: Agregar tupla para buscar las ultimas 48 horas, tener en cuenta el tz
            if 'filters' in context:
                filters.extend(context['filters'])
            ids = self.search(cr, uid, filters, context=context)
        res = None
        for id in ids:
            try:
                '''
                Forzar auto-commit, 
                no podemos permitirnos hacer roll-back del estatus de
                transacciones previas
                '''
                res = self.attempt_electronic_document(cr, uid, [id], context=context) #TODO: Probar si hace falta desarrollar una opcion como auto_commit=True
            except Exception:
                #TODO ADD BETTER LOG MESSAGES - ALTATEC
                #_logger.exception("Failed processing mail queue") 
                a = 1 #no hacemos nada porque llenariamos el log
        return res

electronic_document()
