# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors: Andres Calle                                                                     
#Copyright (C) 2013 TRESCLOUD Cia. Ltda.                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from report import report_sxw
from tools.translate import _
from osv import orm
import re

class Parser(report_sxw.rml_parse):
    
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context)
        self.cr = cr
        self.uid = uid
        self.localcontext.update({
            'get_identification': self._get_identification,
            'get_normalized_text': self._get_normalized_text,
            'get_bom_invoice_line': self._get_bom_invoice_line,
        })
    
    def _get_identification(self, vat):
        '''Remueve las letras EC en caso de haberlas del
        campo vat del partner'''
        if vat!=False or vat is None:
            if vat[:2] == 'EC':
                partner_vat = vat[2:]
            if vat[:2] != 'EC':
                partner_vat = vat
        else:
            partner_vat = 'Especifique identificacion correcta.'
        
        return partner_vat
    
    def _get_normalized_text(self, text):
        '''
        Remueve caracteres raros de un texto dado
        Deja la letra ENIE como letra valida
        '''
        
        text_without_weird_characters = re.sub('[^a-zA-ZñÑ0-9]',' ',text)
        text_without_leading_trailing_spaces = text_without_weird_characters.strip()
        return text_without_leading_trailing_spaces
    
    def _get_bom_invoice_line(self, invoice_line):
        '''
        Show the lines that integrate the product bom if exist
        '''
        product_id = False
        
        if invoice_line and invoice_line.product_id:
            product_id = invoice_line.product_id.id
        else:
            return [] 
        
        lines=[]
        
        #bom_object = self.pool.get('mrp.bom')
        #list = bom_object.search(self.cr, self.uid, [('product_id','=',product_id)])
        list=False
        if list :
            # use the first
            bom = bom_object.browse(self.cr, self.uid, list[0])
            for element in bom.bom_lines:
                if element.product_id.id != product_id:
                    if element.product_id.id == invoice_line.product_id.id:
                        continue
                    data={}
                    data['codigoPrincipal'] = element.product_id.default_code or ''
                    data['descripcion'] = element.product_id.name
                    data['cantidad'] = element.product_qty
                    data['precioUnitario'] = 0.0
                    data['descuento'] = 0.0
                    data['precioTotalSinImpuesto'] = 0.0
                    taxes=[]
                    for tax in invoice_line.invoice_line_tax_id:
                        tax_data={}
                        tax_data['codigoPorcentaje'] = 0
                        tax_data['tarifa'] = 0
                        if tax.amount == 0.12:
                            tax_data['codigoPorcentaje'] = 2
                            tax_data['tarifa'] = 12
                        tax_data['baseImponible'] = 0.0
                        tax_data['valor'] = 0.0
                        taxes.append(tax_data)

                    data['taxes'] = taxes
                    lines.append(data)
            
        return lines
