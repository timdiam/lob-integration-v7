# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014 TRESCloud S.A. David Romero 
#    Copyright (C) 2004 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields
from tools.translate import _

class sale_order(osv.osv):
    
    _inherit = 'sale.order'
    
    def action_recalculate(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        order_line_obj = self.pool.get('sale.order.line')
        sale_data = self.browse(cr, uid, ids[0], context)
        pricelist_id  = sale_data.pricelist_id.id
        partner_id = sale_data.partner_id.id
        date_order = sale_data.date_order

        if sale_data['state'] == 'draft':
            self.write(cr, uid, ids, {'pricelist_id': pricelist_id})
            for line in sale_data.order_line:
                vals = order_line_obj.product_id_change(cr, uid, line.id, pricelist_id, line.product_id.id ,qty=line.product_uom_qty,uom=line.product_uom.id,uos=line.product_uos.id, partner_id=partner_id, date_order=date_order)
                if vals.get('value',False):
                    if 'price_unit' in vals['value'].keys():
                        order_line_obj.write(cr, uid, line.id, {'price_unit': vals['value']['price_unit']},context=context)
            # DR agrega la funcionalidad de autentificacion
            # DR agregar dependencia del modulo authorizacion_for_pricelist
            sale_obj = self.pool.get('sale.order')
            sale_obj.write(cr, uid, ids[0], {'pricelist_approver_id' : ''})
            return self.action_confirm(cr, uid, ids, context)
        
        else:
            raise osv.except_osv(_('Warning'),_('PriceList cannot be changed! Make sure the Sales Order is in "Quotation" state!'))
        return True
    
    # D.R. Permite eliminar las lineas en el tree solo cuando esta en estado draft 
    """def onchange_line(self, cr, uid, ids, lines, context=None):
        self.load_line(cr, uid, ids, lines[0][2], context=None)
        #self.load_attentions(cr, uid, ids, context)
        value = {}
        return True 
    # D.R. Permite cargar las lineas de las ateciones 
    def load_attentions(self, cr, uid, ids, context=None):
        self.load_line(cr, uid, ids, line_ids=None, context=None)
    #D.R. Bargar las lineas  de las atenciones en procesos
    def load_line(self, cr, uid, ids, line_ids,context=None):
    
        self.load_line(cr, uid, ids, lines[0][2], context=None)
        self.load_line(cr, uid, ids, line_ids=None, context=None)
    
        """
    
    
    
    
    def onchange_pricelist_id(self, cr, uid, ids, pricelist_id, order_lines, context=None):
        context = context or {}
        if not pricelist_id:
            return {}
        value = {
            'currency_id': self.pool.get('product.pricelist').browse(cr, uid, pricelist_id, context=context).currency_id.id
        }
        if not order_lines:
            return {'value': value}
        warning = {
            'title': _('Pricelist Warning!'),
            'message' : _('If you change the pricelist of this order (and eventually the currency), prices of existing order lines will not be updated. To recompute the prices click on button Recalculate!')
        }
        """ Crear una funcion la cual distinga de donde se esta llamando sea 
            onchange o desde un botton.
            modificar el action_confirm para que sea llamado desde un 
            onchange y desde un botton
        
            self.load_line(cr, uid, ids, lines[0][2], context=None)
            self.load_line(cr, uid, ids, line_ids=None, context=None)
    
            
            self.action_confirm(cr, uid, ids, context)
        """
        
 #       self.action_button_confirm(cr, uid, ids, context)
        
#        return True
        return {'warning': warning, 'value': value}
