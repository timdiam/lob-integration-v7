{ 'name'         : 'AltaTec Migrate Retention Transaction Type',
  'version'      : '1.0',
  'description'  : """
                   This module goes through all the retentions in the system, and sets the transaction_type and transaction_type_orig
                   for all of them based on whether or not they're still in the 'cancel' state and stuff like that.
                   """,
  'author'       : 'Dan Haggerty',
  'website'      : 'www.altatec.ec',
  "depends"      : [ 'ecua_tax_withhold',
                     'ecua_autorizaciones_sri',
                     'account',
                   ],
  "data"         : [
                   ],
  "installable"  : True,
  "auto_install" : False
}
