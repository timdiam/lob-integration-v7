from openerp import tools, pooler
from openerp.osv import fields,osv
import logging
_logger = logging.getLogger(__name__)

class retention_transaction_type_migrate(osv.osv):
    _name = "retention.transaction.type.migrate"
    _auto = False

    def init(self, cr):
        pool = pooler.get_pool(cr.dbname)
        uid = 1

        retention_ids = pool.get('account.withhold').search(cr, uid, [])
        for retention in pool.get('account.withhold').browse(cr, uid, retention_ids):

            _logger.debug("Migrating retention: " + str(retention.number))

            if retention.transaction_type == 'canceled':

                _logger.debug("    transaction_type: " + retention.transaction_type)

                # Sale retentions should never be in the 'canceled' state
                if retention.authorizations_id.autorization_partners:
                    _logger.debug("    Setting transaction_type      to: sale")
                    _logger.debug("    Setting transaction_type_orig to: sale")
                    retention.write({'transaction_type' : 'sale' })
                    retention.write({'transaction_type_orig' : 'sale'})
                else:
                    if retention.state != 'canceled':
                        _logger.debug("    Setting transaction_type  to: sale")
                        retention.write({'transaction_type' : 'purchase' })
                    _logger.debug("    Setting transaction_type_orig to: sale")
                    retention.write({'transaction_type_orig' : 'purchase'})

            else:
                _logger.debug("    transaction_type: " + retention.transaction_type)
                _logger.debug("    Setting transaction_type_orig to: " + retention.transaction_type)
                retention.write({'transaction_type_orig' : retention.transaction_type})