from openerp.osv import fields, orm, osv

class product_product(osv.osv):

    _inherit = 'product.product'

    _columns = { 'stock_min_simple' : fields.float( "Stock Min" ),
                 'stock_max_simple' : fields.float( "Stock Max" ),
               }