{
    'name': "Stock Min Max Simple",
    'version': '0.1',
    'description': """
Adds a "Stock Min" and "Stock Max" fields to product.product and adds a report showing a list of stock
above max and stock below min.
""",
    'author': 'AltaTec',
    'website': 'www.altatececuador.com',
    'license': 'AGPL-3',
    "depends": ['product'],
    "data": ['product.xml'],
    "demo": [],
    "active": False,
    "installable": True
}