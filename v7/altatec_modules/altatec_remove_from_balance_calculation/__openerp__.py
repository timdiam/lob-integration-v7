{
    'name'         : 'AltaTec Remove Accounts from Calculation of Balance (residual) on Invoices',
    'version'      : '1.0',
    'description'  : """
                    This module adds a boolean field on acccount.account which allows us to manual tell the system that
                    we do not want to take into consideration a specific account when calculating the reminder to be paid on an
                    invoice.
                     """,
    'author'       : 'Tim Diamond',
    'website'      : 'www.altatec.ec',
    "depends"      : [ 'account',
                       'l10n_ec_niif_minimal',
                     ],
    "data"         : [ 'views/account_account_view.xml',
                     ],

    "installable"  : True,
    "auto_install" : False
}
