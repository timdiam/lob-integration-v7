import locale
from openerp.osv import fields, osv, orm
import logging
import decimal_precision as dp

_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')

class account_account(osv.osv):
    _inherit = "account.account"
    _columns = {
                'remove_balance_invoice_calculation':fields.boolean('No tomar en cuenta en calculo de saldo en facturas', required=True),
    }

    _defaults = {
        'remove_balance_invoice_calculation': False,
    }