# -*- coding: utf-8 -
import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree
import openerp.addons.decimal_precision as dp
import openerp.exceptions

from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp import tools
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging
import xml.etree.cElementTree as ET


_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')


class stock_location(osv.osv):
    _inherit = "stock.location"

    _columns={
                "origen_ingreso":fields.boolean("Origen"),
                "destino_ingreso":fields.boolean("Destino"),
                "origen_egresos":fields.boolean("Origen"),
                "destino_egreso":fields.boolean("Destino"),
                "origen_interno":fields.boolean("Origen"),
                "destino_interno":fields.boolean("Destino"),


    }


class stock_move(osv.osv):
    _inherit ="stock.move"

    def _default_location_destination(self, cr, uid, context=None):
        value=super(stock_move, self)._default_location_destination(cr, uid, context)
        return None

    def _default_location_source(self, cr, uid, context=None):
        value = super(stock_move, self)._default_location_source(cr, uid, context=context)
        return None

    _defaults = {
        'location_id': _default_location_source,
        'location_dest_id': _default_location_destination,
    }

class stock_picking(osv.osv):
    _inherit = 'stock.picking'

    _columns = {
        "add_location"   :fields.boolean("Agregar ubicación", default=False),
        "location_origin" :fields.many2one("stock.location", string="Origen"),
        "location_dest"   :fields.many2one("stock.location", string="Destino"),
    }

class stock_picking_in(osv.osv):

    _inherit = "stock.picking.in"

    fix_field_list = [ 'add_location',
                       'location_origin',
                       'location_dest',
                    ]

    def __init__(self, pool, cr):
        super(stock_picking_in, self).__init__(pool, cr)
        for fix_field in self.fix_field_list:
            self._columns[fix_field] = self.pool['stock.picking']._columns[fix_field]

class stock_picking_out(osv.osv):

    _inherit = "stock.picking.out"

    fix_field_list = [ 'add_location',
                       'location_origin',
                       'location_dest',
                    ]

    def __init__(self, pool, cr):
        super(stock_picking_out, self).__init__(pool, cr)
        for fix_field in self.fix_field_list:
            self._columns[fix_field] = self.pool['stock.picking']._columns[fix_field]


