{
    'name': "Stock Picking Filters",
    'version': '1.0',
    'description': """
            visibilidad para los campos de ubicacion
""",
    'author': 'Harry Alvarez',
    'website': 'www.altatececuador.com',
    "depends": ['stock'],
    "data": ['stock_picking_filters_view.xml',
            ],
    'license': 'AGPL-3',
    "demo": [],
    "installable": True

}
