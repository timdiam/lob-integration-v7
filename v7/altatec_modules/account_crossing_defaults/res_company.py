# -*- coding: utf-8 -*-
#################################################################################
#
# This module adds default anticipo ingreso/egreso fields to res.company, and modifies
# account.voucher to use these accounts in the "Differences" section
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    Feb. 12th, 2015
#
#################################################################################
from openerp.osv import fields, osv, orm

class res_company(osv.osv):

    _inherit = "res.company"

    _columns = { 'default_account_in'  : fields.many2one( "account.account", "Anticipo ingreso por defecto", domain=[('type','!=','view')] ),
                 'default_account_out' : fields.many2one( "account.account", "Anticipo egreso por defecto", domain=[('type','!=','view')] ),
               }