# -*- coding: utf-8 -*-
#################################################################################
#
# This module adds default anticipo ingreso/egreso fields to res.company, and modifies
# account.voucher to use these accounts in the "Differences" section
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    Feb. 12th, 2015
#
#################################################################################
from openerp.osv import fields, osv, orm

class account_voucher(osv.osv):

    _inherit = "account.voucher"

    def _get_default_writeoff_acc_id(self, cr, uid, context=None):

        company_ids = self.pool.get('res.company').search( cr, uid, [], context=context )

        if( len(company_ids) ):

            company = self.pool.get('res.company').browse( cr, uid, company_ids[0], context=context )

            if 'type' in context:
                if( context['type'] == 'receipt' and company.default_account_in):
                    if not company.default_account_in:
                        raise osv.except_osv( ( 'Error' ), ( 'Por favor, defina las cuentas de cruce por default en la config de la compania' ) )
                    return company.default_account_in.id

                elif( context['type'] == 'payment' and company.default_account_out ):
                    if not company.default_account_out:
                        raise osv.except_osv( ( 'Error' ), ( 'Por favor, define las cuentas de cruce por default en la config de la compania' ) )
                    return company.default_account_out.id

        return None


    _defaults = { 'writeoff_acc_id': _get_default_writeoff_acc_id,
                }