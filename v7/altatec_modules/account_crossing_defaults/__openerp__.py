{
    'name': 'Account Crossing Defaults',
    'version': '1.0',
    'description': """
        This module adds default anticipo ingreso/egreso fields to res.company, and modifies account.voucher to
        use these accounts in the "Differences" section
    """,
    'author': 'Dan Haggerty',
    'website': 'www.altatececuador.com',
    "depends" : [ 'ecua_sri_refund',
                  'account',
                ],
    "data" : [ 'res_company.xml',
             ],
    "installable": True,
    "auto_install": False
}