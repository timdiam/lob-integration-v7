
from   openerp.osv             import fields, osv, orm

class account_invoice(osv.osv):

    _inherit = "account.invoice"

    def action_cancel_draft(self, cr, uid, ids, context={}):

        invoices = self.browse( cr, uid, ids, context )

        for invoice in invoices:
            if( context.get( 'type', False ) in ['in_invoice', 'in_refund'] ):
                invoice.write( { 'transfer_document' : False } )

        return super( account_invoice, self ).action_cancel_draft( cr, uid, ids, context )


    def cancelar_transfernecia(self, cr, uid, ids, context=None):
        reconcile_pool = self.pool.get('account.move.reconcile')
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        invoice=self.pool.get('account.invoice')

        # Anulo el asiento de la Transferencia

        for record in self.browse(cr,uid,ids):
            # record.transfer_document.line_id[7].reconcile_id
            for orden_line in record.transfer_document.line_id:
                if orden_line.reconcile_id:
                    reconcile_pool.unlink(cr,uid,[orden_line.reconcile_id.id])
                elif orden_line.reconcile_partial_id:
                    reconcile_pool.unlink(cr,uid,[orden_line.reconcile_partial_id.id])

            move_pool.button_cancel(cr,uid,[record.transfer_document.id],context=context)
            move_pool.write(cr,uid,[record.transfer_document.id],{'ref':'ANULADO'},context=context)

            for line in record.transfer_document.line_id:
                move_line_pool.unlink(cr,uid,[line.id],context=context)

            invoice.action_cancel(cr, uid, [record.id], context=context)

        # Anulo el asiento de la factura original

            # for orden_line in record.move_id.line_id:
            #     if orden_line.reconcile_id:
            #         reconcile_pool.unlink(cr,uid,[orden_line.reconcile_id.id])
            #     elif orden_line.reconcile_partial_id:
            #         reconcile_pool.unlink(cr,uid,[orden_line.reconcile_partial_id.id])

            # move_pool.button_cancel(cr,uid,[record.move_id.id],context=context)
            # move_pool.write(cr,uid,[record.move_id.id],{'ref':'ANULADO'},context=context)
            #
            # for line in record.move_id.line_id:
            #     move_line_pool.unlink(cr,uid,[line.id],context=context)

            self.write(cr,uid,[record.id], {'state':'cancel'})

        return True



    def onchange_transfer_oap(self,cr,uid,ids,transfer_oap,context=None):
        if transfer_oap ==False:
            return {'value': {'transfer_partner':None,'transfer_account': None, 'tarjeta_credito': None}, }
        else:
            return True

    def onchange_tipo_transferencia(self,cr,uid,ids,tipo_transferencia,context=None):
        if tipo_transferencia =='caja':
            return {'value': {'transfer_partner':None,'transfer_account': None, 'tarjeta_credito': None}, }
        else:
            return {'value': {'transfer_partner':None,'transfer_account': None}}

    def onchange_tipo_tarjeta(self,cr,uid,ids,tipo_transferencia,transfer_oap,tarjeta_credito,context=None):
        if (tipo_transferencia =='tarjeta' and transfer_oap==True):
            tc_obj= self.pool.get('ecua.credit.card').browse(cr,uid,tarjeta_credito)
            return {'value': {'transfer_partner': tc_obj.partner.id ,'transfer_account': tc_obj.partner.property_account_payable.id}, }
        if transfer_oap==False:
            return {'value': {'tipo_transferencia':None, }}
        else:
            return True


    def invoice_open_enhanced(self, cr, uid, ids, context=None):
        records= self.browse(cr, uid, ids)
        res = super(account_invoice, self).invoice_open_enhanced(cr, uid, ids, context=context)

        #sometimes invoice_open_enhanced returns a report, sometimes a window to add information that is missing.
        #this line makes sure we go well with both cases
        if res and ('type' in res) and (res['type'] == 'ir.actions.act_window'):
            return res

        for invoice in records:
            if invoice.transfer_oap==True:
                move_obj= self.pool.get('account.move')
                move_line_obj= self.pool.get('account.move.line')

                journal_to_use = 0
                if invoice.tarjeta_credito and invoice.tarjeta_credito.diario:
                    journal_to_use = invoice.tarjeta_credito.diario.id
                else:
                    journal_to_use = invoice.journal_id.id

                transfered_move = move_obj.create( cr,
                                       uid,
                                       { 'journal_id' : journal_to_use,
                                         'period_id'  : invoice.period_id.id,
                                         'date'       : invoice.date_invoice,
                                         'ref': invoice.internal_number,
                                         }
                                       )
                lines_new = []
                lines_old = []
                running_total = 0


                p_move_line = self.pool.get('account.move.line').search( cr,
                                                                     uid,
                                                                     [ ('move_id',    '=', invoice.move_id.id    ),
                                                                       ('account_id', '=', invoice.account_id.id ),
                                                                       ('partner_id', '=', invoice.partner_id.id ),
                                                                       ]
                                                                     )
                if( not p_move_line ):
                    raise osv.except_osv( ('Invalid Action!'),
                                         ('No se encontro el asiento de la factura: ' ))

                # if( len(p_move_line) > 1 ):
                #     raise osv.except_osv(_('Invalid Action!'),
                #                          _('Un invoice tiene varias lineas con la misma cuenta. Invoice: '
                #                            +invoice.name+". Cuenta: " + invoice.account_id.name ))


                for olds in p_move_line:
                    p_move_line_obj = self.pool.get('account.move.line').browse(cr,uid,olds)

                    if( p_move_line_obj.reconcile_id ):
                        raise osv.except_osv( ('Invalid Action!'),
                                      ('El asiento de la factura :' + invoice.number+ " ya esta conciliado" ) )

                    if( p_move_line_obj.reconcile_partial_id ):
                        raise osv.except_osv( ('Invalid Action!'),
                                      ('El asiento de la factura :' + invoice.number+ " ya esta conciliado" ) )


                    amount_on_invoice = p_move_line_obj.credit if invoice.type == "in_invoice" else p_move_line_obj.debit
                    running_total= running_total + amount_on_invoice



                    lines_old.append(olds)


                    lines_new.append( move_line_obj.create( cr,
                                                    uid,
                                                    { 'name'       : invoice.move_name,
                                                      'account_id' : p_move_line_obj.account_id.id,
                                                      'debit'      : amount_on_invoice if invoice.type == "in_invoice" else 0,
                                                      'credit'     : 0 if invoice.type == "in_invoice" else amount_on_invoice,
                                                      'move_id'    : transfered_move,
                                                      'partner_id' : invoice.partner_id.id,
                                                      }
                                                    )
                              )


                    name_new_line="Trasnferencia - "+invoice.internal_number

                    if invoice.tipo_transferencia and ((invoice.tipo_transferencia)=='tarjeta'):
                        name_new_line="Pago TC - "+invoice.internal_number
                    if invoice.tipo_transferencia and ((invoice.tipo_transferencia)=='caja'):
                        name_new_line="TR Caja - "+invoice.internal_number

                    new_line_in_name_of_person_to_transfer= move_line_obj.create( cr, uid,
                                                {
                                                    'master':True,
                                                    'name'       : name_new_line,
                                                    #'account_id' : invoice.journal_id.default_debit_account_id.id if invoice.type == "in_invoice" else invoice.journal_id.default_credit_account_id.id,
                                                    'account_id' : invoice.transfer_account.id,
                                                    'debit'      : 0 if invoice.type == "in_invoice" else amount_on_invoice,
                                                    'credit'     : amount_on_invoice if invoice.type == "in_invoice" else 0,
                                                    'move_id'    : transfered_move,
                                                    'partner_id' : invoice.transfer_partner.id,
                                                    }
                                                )

                for i in range(len(lines_new)):
                    self.pool.get( 'account.move.line' ).reconcile_partial( cr,
                                                                        uid,
                                                                        [ lines_new[ i ], lines_old[ i ] ]
                                                                        )


                self.pool.get('account.invoice').write(cr, uid, invoice.id,
                                                   {'transfer_document': transfered_move})

        return res


    _columns = { 'transfer_oap':fields.boolean('Transferencia OAP'),
                 'transfer_partner':fields.many2one('res.partner',string="Partner"),
                 'transfer_account':fields.many2one('account.account',string="Cuenta"),
                 'transfer_document':fields.many2one('account.move',string="Asiento Transferido"),
                 "tipo_transferencia":fields.selection([('tarjeta', "Tarjeta de Credito"), ('caja', 'Caja')]),
                 "tarjeta_credito":fields.many2one('ecua.credit.card',string="Tarjeta de Credito"),
                 }