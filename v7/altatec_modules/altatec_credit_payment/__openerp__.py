{   'name': 'Credit Payment',
    'version': '1.0',
    'description': """
    Credit Payment
    """,
    'author': 'Harry Alvarez',
    'website': 'www.altatececuador.com',
    "depends" : [ 'base',
                  'account',
                  'ecua_ats',
                  'ecua_credit_card',

                ],
    "data" : [
                'views/account_invoice_view.xml',
             ],
    "installable": True,
    "auto_install": False
}