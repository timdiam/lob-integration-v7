#####################################################################################
#
#####################################################################################
from openerp.osv import fields, osv


#####################################################################################
# This is the inherited asset class. Adds a function that gets called when
# a "Nueva Amortizacion" button is pressed.
#####################################################################################
class account_asset_asset(osv.osv):

    _inherit = 'account.asset.asset'


    #####################################################################################
    # This function contains the logic that is executed with the "Nueva Amortizacion"
    # button is pressed.
    #####################################################################################
    def nueva_amortizacion( self, cr, uid, ids, context = None ):

        asset = self.browse( cr, uid, ids[ 0 ], context )

        total_amortizacions = 0.00

        for line in asset.depreciation_line_ids:
            total_amortizacions += line.amount

        if( asset.value_residual - total_amortizacions < 0.01 ):
            raise osv.except_osv( 'Error!', "El valor residual ha sido totalmento amortizado!" )

        return { 'name':       "Nueva Amortizacion",
                 'view_mode': 'form',
                 'view_type': 'form',
                 'res_model': 'amortizacion.wizard',
                 'type':      'ir.actions.act_window',
                 'target':    'new',
                 'context':   context
               }


    #####################################################################################
    # This function overrides account.asset.asset's compute_depreciation_board and does
    # nothing if the asset type is set to "Amortizaction"
    #####################################################################################
    def compute_depreciation_board(self, cr, uid, ids, context=None):

        for asset_id in ids:

            asset = self.browse( cr, uid, asset_id, context )

            if( asset.ecua_asset_type == 'amortizacion' ):
                continue
            else:
                super(account_asset_asset, self).compute_depreciation_board(cr, uid, [asset_id], context=context)


    #####################################################################################
    # This function overrides account.asset.asset's compute_depreciation_board and does
    # nothing if the asset type is set to "Amortizaction"
    #####################################################################################
    def sanity_check( self, cr, uid, vals, asset_id = None, context = None ):

        purchase_value = 0.0
        salvage_value  = 0.0

        if( asset_id != None ):
            asset = self.browse( cr, uid, asset_id, context )
            purchase_value = asset.purchase_value
            salvage_value  = asset.salvage_value

        if 'purchase_value' in vals:
            purchase_value = vals[ 'purchase_value' ]

        if 'salvage_value' in vals:
            salvage_value = vals[ 'salvage_value' ]

        error_string = ""

        # First check the asset's fields for errors
        if( purchase_value < 0.00 ):
            error_string += "El valor bruto debe ser al menos 0\n"

        if( salvage_value < 0.00 ):
            error_string += "El valor de salvaguarda debe ser al menos 0\n"

        if( salvage_value > purchase_value ):
            error_string += "El valor de salvaguarda debe ser menor que el valor bruto\n"

        if( error_string != "" ):
            raise osv.except_osv( 'Error!', 'Se encontraron los siguientes problemas:\n' + error_string )


    #####################################################################################
    # This function overrides account.asset.asset's write function and does all sorts of
    # error checking
    #####################################################################################
    def write( self, cr, uid, ids, vals, context=None ):

        for asset_id in ids:
            self.sanity_check( cr, uid, vals, asset_id, context )

        return super( account_asset_asset, self ).write( cr, uid, ids, vals, context = None )


    #####################################################################################
    # This function overrides account.asset.asset's write function and does all sorts of
    # error checking
    #####################################################################################
    def create( self, cr, uid, vals, context=None ):

        self.sanity_check( cr, uid, vals, asset_id = None, context = context )

        return super( account_asset_asset, self ).create( cr, uid, vals, context = None )


    #####################################################################################
    # This function gets the default asset type based on the context
    #####################################################################################
    def _get_default_asset_type( self, cr, uid, context = None ):

        if 'ecua_asset_type' in context:
            if( context[ 'ecua_asset_type' ] != 'normal' and context[ 'ecua_asset_type' ] != 'amortizacion' ):
                return 'normal'
            else:
                return context[ 'ecua_asset_type' ]
        else:
            return 'normal'



    _columns = { 'ecua_asset_type' : fields.selection( ( ('normal', 'Normal'),('amortizacion','Amortizacion') ),
                                                       'Asset Type',
                                                       required = True
                                                     )
               }

    _defaults = { 'ecua_asset_type' : _get_default_asset_type
                }


#####################################################################################
# This class is the wizard that appears when the "Nueva Amortizacion" button is
# pressed. Creates one amortizacion line in the asset.
#####################################################################################
class amortizacion_wizard( osv.osv ):

    _name = 'amortizacion.wizard'


    #####################################################################################
    # This function gets the default value for "secuencia" by adding 1 to the highest
    # secuencia of all the asset's depreciation lines
    #####################################################################################
    def _get_default_secuencia( self, cr, uid, context = None ):

        asset_id = context[ 'active_ids' ][ 0 ]

        asset = self.pool.get( 'account.asset.asset' ).browse( cr, uid, asset_id, context )

        max_sequence = 0

        for line in asset.depreciation_line_ids:
            if line.sequence > max_sequence:
                max_sequence = line.sequence

        return max_sequence + 1


    #####################################################################################
    # This function calculates the default "importe_amortizado".
    #####################################################################################
    def _get_default_importe_amortizado( self, cr, uid, context = None ):

        asset_id = context[ 'active_ids' ][ 0 ]

        asset = self.pool.get( 'account.asset.asset' ).browse( cr, uid, asset_id, context )

        most_recent_depreciation_amount = 0.0
        most_recent_depreciated_value   = 0.0
        highest_sequence                = 0

        for line in asset.depreciation_line_ids:

            if( line.sequence > highest_sequence ):
                highest_sequence                = line.sequence
                most_recent_depreciation_amount = line.amount
                most_recent_depreciated_value   = line.depreciated_value

        return most_recent_depreciated_value + most_recent_depreciation_amount


    #####################################################################################
    # This function calculates the default "amortizacion_siguiente"
    #####################################################################################
    def _get_default_amortizacion_siguiente( self, cr, uid, context = None ):

        asset_id = context[ 'active_ids' ][ 0 ]

        asset = self.pool.get( 'account.asset.asset' ).browse( cr, uid, asset_id, context )

        if( len( asset.depreciation_line_ids ) == 0 ):
            return asset.value_residual

        most_recent_remaining_value = 0.00
        highest_sequence            = 0

        for line in asset.depreciation_line_ids:

            if( line.sequence > highest_sequence ):
                highest_sequence            = line.sequence
                most_recent_remaining_value = line.remaining_value

        return most_recent_remaining_value


    #####################################################################################
    # This function calculates the default "amortizacion_actual" by
    #####################################################################################
    def _get_default_amortizacion_actual( self, cr, uid, context = None ):

        asset_id = context[ 'active_ids' ][ 0 ]

        asset = self.pool.get( 'account.asset.asset' ).browse( cr, uid, asset_id, context )

        if( len( asset.depreciation_line_ids ) == 0 ):
            return 0.00

        most_recent_remaining_value     = 0.00
        most_recent_depreciation_amount = 0.00
        highest_sequence                = 0

        for line in asset.depreciation_line_ids:

            if( line.sequence > highest_sequence ):
                highest_sequence                = line.sequence
                most_recent_depreciation_amount = line.amount
                most_recent_remaining_value     = line.remaining_value

        if( most_recent_depreciation_amount > most_recent_remaining_value ):
            return most_recent_remaining_value
        else:
            return most_recent_depreciation_amount


    #####################################################################################
    # This function creates an amortizaction (account.asset.depreciation.line) with the
    # values provided by the user in the wizard.
    #####################################################################################
    def crear_amortizacion( self, cr, uid, ids, context = None ):

        asset_id = context[ 'active_ids' ][ 0 ]

        line_id_obj = self.pool.get( 'account.asset.depreciation.line' )

        wizard = self.browse( cr, uid, ids, context = context )[ 0 ]

        asset = self.pool.get( 'account.asset.asset' ).browse( cr, uid, asset_id, context )

        if( wizard.amortizacion_siguiente - wizard.amortizacion_actual < 0.00 ):
            raise osv.except_osv( 'Error!', "El valor de amortizacion no puede ser mayor que el valor restante del activo" )

        if( wizard.amortizacion_actual < 0.01 ):
            raise osv.except_osv( 'Error!', "El valor de amortizacion debe ser mayor que 0" )

        new_line_id = line_id_obj.create( cr,
                                          uid,
                                          { 'name'              : str( asset_id ) + '/' + str( wizard.secuencia ),
                                            'sequence'          : wizard.secuencia,
                                            'asset_id'          : asset_id,
                                            'depreciated_value' : wizard.importe_amortizado,
                                            'amount'            : wizard.amortizacion_actual,
                                            'remaining_value'   : wizard.amortizacion_siguiente - wizard.amortizacion_actual,
                                            'depreciation_date' : wizard.fecha_de_amortizacion,
                                          },
                                        )


    #####################################################################################
    # This function is called when the "Cancelar" button is pressed in the
    # "Nueva Amortizacion" wizard.
    #####################################################################################
    def cancelar_amortizacion( self, cr, uid, ids, context = None ):
        return


    #####################################################################################
    # This function calculates the "Amortizacion del siguiente periodo" in the wizard
    # based on the "Amortizacion actual"
    #####################################################################################
    def on_change_amortizacion_actual( self, cr, uid, ids, amortizacion_actual, context=None ):

        amortizacion_siguiente = self._get_default_amortizacion_siguiente( cr, uid, context ) - amortizacion_actual

        return { 'value' : { 'amortizacion_siguiente' : amortizacion_siguiente } }


    #####################################################################################
    # Field and default definitions
    #####################################################################################
    _columns =  { "fecha_de_amortizacion"  : fields.date( 'Fecha de amortizacion' ),
                  "amortizacion_actual"    : fields.float( "Amortizacion actual" ),
                  "secuencia"              : fields.integer( "Secuencia", readonly = True ),
                  "importe_amortizado"     : fields.float( "Importe amortizado", readonly = True ),
                  "amortizacion_siguiente" : fields.float( "Amortizacion del siguiente periodo", readonly = True ),
                }

    _defaults = { "fecha_de_amortizacion"  : fields.date.context_today,
                  "amortizacion_actual"    : _get_default_amortizacion_actual,
                  "secuencia"              : _get_default_secuencia,
                  "importe_amortizado"     : _get_default_importe_amortizado,
                  "amortizacion_siguiente" : _get_default_amortizacion_siguiente,
                }