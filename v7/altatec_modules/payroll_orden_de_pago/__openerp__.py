
{
    'name': 'Recursos_ec',
    'version': '1.0',
    'description': """
        Recursos Addons
    """,
    'author': 'Harry Alvarez',
    'website': 'www.google.com',
    "depends" : ['hr','ecua_hr', 'hr_advance_payment',"hr_extra_input_output", 'hr_payroll'],
    "data" : [ 
                'payroll_orden_de_pago_view.xml',
                 
                ],
    "installable": True,
    "auto_install": False
}