import time
from datetime import date
from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree
import openerp.addons.decimal_precision as dp
import openerp.exceptions

from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp import tools
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging
import xml.etree.cElementTree as ET

class orden_de_pago(orm.Model):
    _name = 'orden.de.pago'
    
    def unlink(self, cr, uid, ids, context=None):
        records = self.browse(cr,uid,ids,context)
        for r in records:
            if r.state != 'anulado':
                raise osv.except_osv(_('Invalid Action!'), _('No puedes borrar un orden de pago en el estado realizado'))
            
        return super(orden_de_pago,self).unlink(cr, uid, r.id, context=context)
            
    def cancelar_orden_de_pago(self,cr,uid,ids,context=None):
        reconcile_pool = self.pool.get('account.move.reconcile')
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        
        for record in self.browse(cr,uid,ids):
            
            for orden_line in record.lineas_de_pago:
                if orden_line.contrapartida.reconcile_id:
                    reconcile_pool.unlink(cr,uid,[orden_line.contrapartida.reconcile_id.id])
                elif orden_line.contrapartida.reconcile_partial_id:
                    reconcile_pool.unlink(cr,uid,[orden_line.contrapartida.reconcile_partial_id.id])
            
            move_pool.button_cancel(cr,uid,[record.move_id.id],context=context)
            move_pool.write(cr,uid,[record.move_id.id],{'ref':'ANULADO'},context=context)
            
            for line in record.move_id.line_id:
                move_line_pool.unlink(cr,uid,[line.id],context=context)
            
            self.write(cr,uid,[record.id], {'state':'anulado'})
            
        return True
    
    _columns = {
               'diario': fields.related('move_id','journal_id',type='many2one',relation='account.journal', string="Diario"),
               'periodo': fields.related('move_id', 'period_id',type='many2one',relation='account.period', string = "Periodo"),
               'fecha': fields.related('move_id','date', type='date', string='Fecha'),
               'name': fields.char('Nombre / Razon'),
               'cuenta_por_cruzar': fields.many2one('account.account',required=True,string='Cuenta por cruzar'),
               'partner_id': fields.many2one('res.partner',string="Bancaria / Institucion a Pagar"),
               'cuenta_partner_a_pagar': fields.many2one('account.account', required=True, string='Cuenta a pagar de Bancaria / Institucion'),
               'monto': fields.float('Monto'),
               'move_id':fields.many2one('account.move',string='Move ID', required=True),
               'linea_a_pagar':fields.many2one('account.move.line','Apunte a pagar'),
               'lineas_de_pago':fields.one2many('orden.de.pago.line','orden_de_pago',string='Lineas de Pago'),
               'state': fields.selection([('realizado','Realizado'), ('anulado','Anulado')], 'Estado', size=32),

               }


class orden_de_pago_line(osv.osv):
    _name = 'orden.de.pago.line'
    
    _columns = {
               'orden_de_pago':fields.many2one('orden.de.pago', string='Orden De Pago'),
               'partner_id':fields.many2one('res.partner', string='Persona'),
               'monto':fields.float('Monto Asignacion'),
               'contrapartida':fields.many2one('account.move.line',string='Contrapartida'),
               'rec_line':fields.many2one('account.move.line', string='Linea de Reconiliacion'),
               'name':fields.char("Descripcion"),
               'reconcile_id':fields.related('contrapartida','reconcile_id',type='many2one',relation='account.move.reconcile',string='ID de Reconciliacion'),
               'reconcile_partial_id':fields.related('contrapartida','reconcile_partial_id',type='many2one',relation='account.move.reconcile',string='ID de part. Reconciliacion'),
               }


class orden_pago_wizard (osv.osv):
    _name= 'orden.pago.wizard'
    
    def onchange_amount(self, cr, uid, ids, amount=False, roles=[], cuenta=None, context=None):
        res = {}
        total_debe=0
        total_haber=0
        
        if amount:
            total_haber = total_haber + amount

        move_line_obj = self.pool.get('account.move.line')
        
        account = self.pool.get('account.account').browse(cr,uid,cuenta,context=context)
        
        if (len(roles) >= 1) and (len(roles[0]) == 3):
            for role in roles[0][2]:
                payslip = self.pool.get('hr.payslip').browse(cr,uid,role,context)
                line_id = move_line_obj.search(cr,uid,[('move_id','=', payslip.move_id.id),
                                                    ('account_id','=', account.id),
                                                    ('partner_id','=', payslip.employee_id.address_home_id.id)])
                
                line = move_line_obj.browse(cr,uid,line_id,context=context)
                
                if len(line)>1:
                    raise osv.except_osv(_('Invalid Action!'), 
                                         _('El asiento del role tiene varias lineas con la misma cuenta cruzada: '
                                           +payslip.name+".  Cuenta: "+account.name))
                
                if not line:
                    raise osv.except_osv(_('Invalid Action!'), 
                                         _('La cuenta cruzada se encuentra en el rol de: '
                                           +payslip.name+".  Cuenta: "+account.name))                    
                
                total_debe = total_debe + line[0].credit
                total_haber = total_haber + line[0].debit

        res['value'] = {'total_debe':total_debe,'total_haber':total_haber}

        return res    
    
    def crear_orden_pago(self, cr, uid, ids,context=None):
        
        for wiz in self.browse(cr,uid,ids,context=context):
            move_obj = self.pool.get('account.move')
            move_line_obj = self.pool.get('account.move.line')
            
            #data = self.read(cr, uid, ids, context=context)[0]
            
            my_move = move_obj.create(cr,uid,{'journal_id': wiz.diario.id,
                                    'period_id': wiz.periodo.id,
                                    'date':wiz.date,
                                    })
            
            debit_lines_new = []
            debit_lines_old = []
            
            running_total = 0
                   
            orden_de_pago_obj = self.pool.get('orden.de.pago')
            orden_de_pago_line_obj = self.pool.get('orden.de.pago.line')
            
            orden_id = orden_de_pago_obj.create(cr,uid,{
                                             'name': wiz.nombre,
                                             'cuenta_por_cruzar': wiz.cuenta_cruzada.id,
                                             'partner_id': wiz.partner_id.id,
                                            'cuenta_partner_a_pagar': wiz.cuenta_bancaria.id,
                                            'monto': wiz.amount,
                                            'move_id':my_move,
                                            #'linea_a_pagar': credit_line,
                                             })

            
            for payslip in wiz.roles:

                p_move_line = self.pool.get('account.move.line').search(cr,uid,[('move_id','=',payslip.move_id.id),
                                                                                ('account_id','=',wiz.cuenta_cruzada.id),
                                                                                ('partner_id','=',payslip.employee_id.address_home_id.id)])
                
                if(not p_move_line):
                    raise osv.except_osv(_('Invalid Action!'), 
                                         _('El asiento del rol de :'+payslip.name+" no tiene la cuenta: "+wiz.cuenta_cruzada.name))                    
                
                if(len(p_move_line) > 1):
                    raise osv.except_osv(_('Invalid Action!'), 
                                         _('Un role tiene varias lineas con la misma cuenta: Role: '
                                           +payslip.name+". Cuenta: "+ wiz.cuenta_cruzada.name))

                p_move_line_obj = self.pool.get('account.move.line').browse(cr,uid,p_move_line[0])

                if( p_move_line_obj.reconcile_id ):
                    raise osv.except_osv( _('Invalid Action!'),
                                          _('El asiento del rol de :' + payslip.name+ " ya esta conciliado" ) )

                if( p_move_line_obj.reconcile_partial_id ):
                    raise osv.except_osv( _('Invalid Action!'),
                                          _('El asiento del rol de :' + payslip.name+ " ya esta conciliado" ) )
                
                if(abs(p_move_line_obj.debit) > .01):
                    raise osv.except_osv(_('Invalid Action!'), 
                                         _('Apunte en rol tiene un valor >0 en debe. (este proceso solo funciona para reconciliar un credito '
                                           +payslip.name+". Cuenta: "+ wiz.cuenta_cruzada.name))

                                        
                amount_on_payslip = p_move_line_obj.credit
                running_total = running_total+amount_on_payslip
                
                
                debit_lines_old.append(p_move_line[0])
                debit_lines_new.append( move_line_obj.create(cr,uid,{'name':payslip.name,
                                             'account_id':p_move_line_obj.account_id.id,
                                             'debit':amount_on_payslip,
                                             'credit':0,
                                             'move_id': my_move,
                                             'partner_id': payslip.employee_id.address_home_id.id,
                                             }))
                
                orden_de_pago_line_obj.create(cr,uid,{
                                                 'orden_de_pago': orden_id,
                                                 'partner_id': payslip.employee_id.address_home_id.id,
                                                 'monto': amount_on_payslip,
                                                'contrapartida': p_move_line[0],
                                                'rec_line': debit_lines_new[-1],
                                                'name': payslip.name,
                                                 })    
            
            if(not abs(running_total - wiz.amount)<.01):
                raise osv.except_osv(_('Invalid Action!'),
                                          _('Monto total ingresado no es equal al total de los documentos: Monto ingresado: '
                                            +str(wiz.amount)+" Monto total de documentos: "+ str(running_total) ))
                     
            credit_line = move_line_obj.create(cr,uid,{'name':wiz.nombre,
                                         'account_id':wiz.cuenta_bancaria.id,
                                         'debit':0,
                                         'credit':wiz.amount,
                                         'move_id': my_move,
                                         'partner_id':wiz.partner_id.id,
                                         })
            
            for i in range(len(debit_lines_new)):
                self.pool.get('account.move.line').reconcile_partial(cr, uid, [debit_lines_new[i],debit_lines_old[i]])
            
            orden_de_pago_obj.write(cr,uid,orden_id,{'linea_a_pagar': credit_line,'state':'realizado'})
            
            return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form,tree',
                    'res_model': 'orden.de.pago',
                    'res_id': orden_id,
                    'nodestroy': True,
                    }

        

    _columns={
              "nombre": fields.char("Nombre"),
              "cuenta_cruzada": fields.many2one("account.account", string="Cuenta Por Cruzar"),
              "cuenta_bancaria": fields.many2one("account.account", string="Cuenta Bancaria por Pagar"),
              "diario": fields.many2one("account.journal", string="Diario"),
              "periodo": fields.many2one("account.period", string="Periodo"),
              "partner_id": fields.many2one("res.partner", string="Banco / Institucion"),
              'amount':fields.float('Monto'),
              'roles':fields.many2many('hr.payslip','orden_roles_rel','orden_id','role_id',string='Roles'),
              'total_debe':fields.float('Total Debito'),
              'total_haber':fields.float('Total Credito'),
              'date':fields.date("Fecha"),
              }

class hr_payslip(orm.Model):
    _inherit = "hr.payslip"

    _columns = {
                'pay_with_transfer':fields.boolean('Pago con Transferencia'),
                'orden_de_pago':fields.many2one('orden.de.pago',string="Orden de Pago"),
                'roles':fields.many2many('orden.de.pago','orden_roles_rel','role_id','orden_id',string='Orden de Pago'),
        }