##############################################################################
#
#    TRESCLOUD CIA. LTDA.
#    Copyright (C) 2013 Trescloud (<http://trescloud.com>).
#    Author: Patricio Rangles
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import math
import random

def rounding(number, digits, naprox=0):
    """
    Funcion de redondeo especial
    number: numero flotante a redondear
    digits: precision decimal
    naprox: numero de digitos extras a usarse en la aproximacion 
    
    >>> rounding(51,4249999,2,2)
    >>>51,43
    >>> rounding(51,4249999,2)
    >>>51,42
    """
    num_div=[]
    separ=''
    lst=[]
    roundt=False
    try:
        num=str(number) 
    except:
        print "mal"
    if num.find('.')>=0:
        num_div= num.split('.')
        separ='.'
    elif num.find(',')>=0:
        num_div= num.split(',')
        separ=','   
    if num_div:
        d=len(num_div[1])
        entero= num_div[0]
        decimal= num_div[1]
        decimal=decimal[0:2+naprox+1]
        for l in decimal:
            lst.append(int(l))
        ##here
        i = len(decimal)
        j=1
        num=""
        while i > 0 and j<len(lst) and i>digits:
            x = lst[i-1]
            if x >= 5:
                v=lst[i-2]+1
                lst[i-2]=v
            i = i -1
            j=j+1
            
        #CASO ESPECIAL .99999999
        i=0
        e=0
        while  i< len(lst):
            if lst[0]==10:
                ent=int(entero)+1
                entero=str(ent)
            if lst[i]==10:
                if i==1:
                    if lst[i+1]==5 and lst[i-1]!=9:
                        v1=lst[i-1]+1
                        lst[i-1]=v1
                    elif lst[i+1]==5 and lst[i-1]==9 and lst[i]==10:
                            e=int(entero)+1
                            break
                    else:
                        roundt=True
                if lst[i-1]==9:
                    roundt=True
                lst[i]=0
            i+=1
        # END CASO ESPECIAL
        i=0
        while i <digits and i<len(lst):
            num=num+ str(lst[i]) 
            i+=1     
        numberstr= entero+"."+num
        if e:
            numberstr=str(e)
        if roundt==True:
            return round(number,digits)
        return float(numberstr)    
    return float(numberstr)

#print "loaded..."

if __name__ == "__main__":
    print "Testing..."
