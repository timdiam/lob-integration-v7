{
    'name': "Required Tax on Purchase Invoices",
    'version': '1.0',
    'description': """
        This module makes having a tax on a purchase invoice required.
    """,
    'author': 'Tim Diamond',
    'website': 'www.altatececuador.com',
    "depends" : ['account'],
    "data" : [ 'account_invoice_view.xml'],
    "installable": True,
    "auto_install": False
}