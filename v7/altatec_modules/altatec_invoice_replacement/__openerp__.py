{ 'name'         : 'AltaTec Invoice Replacement',
  'version'      : '1.0',
  'description'  : """
                   AltaTec Invoice Replacement
                   """,
  'author'       : 'Dan Haggerty',
  'website'      : 'www.altatec.ec',
  "depends"      : [ 'account',
                     'base',
                     'ecua_invoice',
                   ],
  "data"         : ['data/sequence.xml',
                    'views/account_invoice_view.xml',
                    'views/res_company.xml',
                   ],
  "installable"  : True,
  "auto_install" : False
}
