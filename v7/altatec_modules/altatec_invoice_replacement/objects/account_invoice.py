###################################################################################################
#
###################################################################################################
from openerp.osv import osv
from openerp.osv import orm
from openerp.osv import fields


###################################################################################################
# Inherited account_invoice class definition
###################################################################################################
class account_invoice( orm.Model ):

    _inherit = 'account.invoice'

    ###################################################################################################
    # Column and default definitions
    ###################################################################################################
    _columns = { 'is_replacement'      : fields.boolean( "Es un reemplazo" ),
                 'replaced_invoice'    : fields.many2one( "account.invoice", "Factura reemplazado" ),
                 'is_replaced'         : fields.boolean( "Esta reemplazado" ),
                 'replacement_invoice' : fields.many2one( "account.invoice", "Factura reemplazo" ),
                 'state'               : fields.selection([('draft','Draft'),
                                                           ('proforma' ,'Pro-forma'),
                                                           ('proforma2','Pro-forma'),
                                                           ('open'     ,'Open'),
                                                           ('paid'     ,'Paid'),
                                                           ('cancel'   ,'Cancelled'),
                                                           ('replaced' ,'Reemplazado'),
                                                            ],
                                                            'Status', select=True, readonly=True, track_visibility='onchange',
                                                            help=' * The \'Draft\' status is used when a user is encoding a new and unconfirmed Invoice. \
                                                            \n* The \'Pro-forma\' when invoice is in Pro-forma status,invoice does not have an invoice number. \
                                                            \n* The \'Open\' status is used when user create invoice,a invoice number is generated.Its in open status till user does not pay invoice. \
                                                            \n* The \'Paid\' status is set automatically when the invoice is paid. Its related journal entries may or may not be reconciled. \
                                                            \n* The \'Cancelled\' status is used when user cancel invoice.\
                                                            \n* The \'Replaced\' status is used when the document has been nulled and replaced by another valid invoice in the system')
                }

    _defaults = { 'is_replacement' : False,
                  'is_replaced'    : False,
                }

    ###################################################################################################
    # replace_invoice() definition
    ###################################################################################################
    def replace_invoice(self, cr, uid, ids, context=None):

        if context:
            context={}

        if len(ids) != 1:
            raise osv.except_osv( _( 'Error!' ), _( 'Usted solo puede reemplazar 1 factura a la vez' ) )

        invoice = self.browse(cr, uid, ids[0], context=context)

        if invoice.state != 'cancel':
            raise osv.except_osv( _( 'Error' ), _( 'Usted solo puede reemplazar una factura en el estado anulado' ) )

        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id

        new_invoice_id               = self.copy( cr, uid, invoice.id, context = context )
        next_replacement_sequence_id = company.replaced_invoice_sequence_id.id

        if not next_replacement_sequence_id:
            raise osv.except_osv( "Error!", "La compania: " + company.name + " debe tener una 'Secuencia de Facturas Reemplazadas'" )

        invoice_internal_number = invoice.internal_number
        invoice_number          = invoice.internal_number
        invoice_name            = invoice.name
        next_number             = self.pool.get('ir.sequence').next_by_id(cr, uid, next_replacement_sequence_id, context=context)

        # To the cancelled invoice, write True to 'is_replaced', and the id of the new invoice to 'replacement_invoice'
        # TODO: Figure out which name/number fields we should keep on the old invoice, and which should have the new replacement sequence #
        self.write(cr, uid, [invoice.id], { 'internal_number'     : next_number,
                                            # 'number'              : next_number,
                                            # 'name'                : next_number,
                                            'is_replaced'         : True,
                                            'state'               : 'replaced',
                                            'replacement_invoice' : new_invoice_id,
                                          }, context=context)

        # To the new invoice, write True to 'is_replacement', and the id of the old invoice to 'replaced_invoice'
        self.write(cr, uid, [new_invoice_id], { 'internal_number'  : invoice_internal_number,
                                                'number'           : invoice_number,
                                                'name'             : invoice_name,
                                                'type'             : invoice.type,
                                                'date_invoice'     : invoice.date_invoice,
                                                'is_replacement'   : True,
                                                'replaced_invoice' : invoice.id,
                                              }, context=context )

        return { 'name'      : 'Invoice',
                 'view_type' : 'form',
                 'view_mode' : 'form',
                 'res_model' : 'account.invoice',
                 'context'   : context,
                 'type'      : 'ir.actions.act_window',
                 'res_id'    : new_invoice_id,
                 'view_id'   : self.pool.get( 'ir.model.data' ).get_object_reference( cr, uid, 'account', 'invoice_form' )[1],
               }


