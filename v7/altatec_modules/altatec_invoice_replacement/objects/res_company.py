# -*- coding: utf-8 -*-
#################################################################################
#
# This module adds default anticipo ingreso/egreso fields to res.company, and modifies
# account.voucher to use these accounts in the "Differences" section
#
# Author:  Tim Diamond
# Company: AltaTec Ecuador
# Date:    March. 12th, 2015
#
#################################################################################
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

class res_company(osv.osv):

    _inherit = "res.company"

    _columns = { 'replaced_invoice_sequence_id': fields.many2one( 'ir.sequence', string= 'Secuencia de Facturas Reemplazadas'),
               }

    # def get_default_sequence(self,cr, uid, context=None):
    #     sequence = self.pool.get('ir.sequence').search(cr,uid,[('name','=','Facturas Anuladas')], context=context)
    #     if len(sequence) != 1:
    #         raise osv.except_osv( _( 'Error' ), _( 'Error de secuencias...' ) )
    #     return sequence[0]
    #
    # _defaults = {
    #             'replaced_invoice_sequence_id':get_default_sequence,
    #             }