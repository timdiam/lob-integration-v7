# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2014 TRESCLOUD Cia Ltda (trescloud.com)
#    @author Santiago Orozco
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv

class stock_picking(osv.osv):
    _inherit = 'stock.picking'

    # stock_return_picking: while returning incoming shipment with average type product the product price is not 
    # updated according to return quantity 
    # https://code.launchpad.net/~openerp-dev/openobject-addons/7.0-opw-600550_601711-ado/+merge/197818
 
    def do_partial(self, cr, uid, ids, partial_datas, context=None):
        product_obj = self.pool.get('product.product')
        currency_obj = self.pool.get('res.currency')
        uom_obj = self.pool.get('product.uom')
        for pick in self.browse(cr, uid, ids, context=context):
            for move in pick.move_lines:
                if (pick.type == 'out' and pick.purchase_id) and (move.product_id.cost_method == 'average'):
                    if move.state in ('done', 'cancel'):
                        continue
                    partial_data = partial_datas.get('move%s'%(move.id), False)
                    product = product_obj.browse(cr, uid, move.product_id.id)
                    context['currency_id'] = move.company_id.currency_id.id
                    product_qty = partial_data.get('product_qty',0.0)
                    product_uom = partial_data.get('product_uom',False)
                    product_currency = partial_data.get('product_currency',False)
                    product_price = partial_data.get('product_price',0.0)
                    move_currency_id = move.company_id.currency_id.id
                    prod_available = product.qty_available
                    qty = uom_obj._compute_qty(cr, uid, product_uom, product_qty, product.uom_id.id)
                    new_price = currency_obj.compute(cr, uid, product_currency,
                                                     move_currency_id, product_price, round=False)
                    new_price = uom_obj._compute_price(cr, uid, product_uom, new_price,
                                                       product.uom_id.id)
                    if qty > 0 and qty != prod_available:
                        new_price = move.price_unit
                        if prod_available <= 0:
                            new_std_price = new_price
                        else:
                            old_price = product.price_get('standard_price', context=context)[product.id]
                            new_std_price = ((old_price * prod_available) - (new_price * qty))/(prod_available - qty)
                            product_obj.write(cr, uid, [product.id],{'standard_price': new_std_price})
        return super(stock_picking, self).do_partial(cr, uid, ids, partial_datas, context=context)
stock_picking()
