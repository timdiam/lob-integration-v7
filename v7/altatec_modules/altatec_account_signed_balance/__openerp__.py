{
    'name'         : 'AltaTec Account Signed Balance',
    'version'      : '1.0',
    'description'  : """
                     A simple module that adds a function field to account.account, which is a signed balance.
                     It takes the normal balance field, and multiplies it by -1 if the account starts with 2, 3, or 4.
                     """,
    'author'       : 'Dan Haggerty',
    'website'      : 'www.altatec.ec',
    "depends"      : [ 'account',
                     ],
    "data"         : [ 'account_account.xml',
                     ],
    "installable"  : True,
    "auto_install" : False
}