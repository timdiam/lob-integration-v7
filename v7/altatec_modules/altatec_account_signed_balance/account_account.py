from openerp.osv import fields, osv, orm
import openerp.addons.decimal_precision as dp
import logging

_logger = logging.getLogger(__name__)

class account_account(osv.osv):

    _inherit = "account.account"

    def create(self, cr, uid, vals, context={}):
        _logger.debug(vals)
        return super(account_account,self).create(cr,uid,vals,context=context)

    def _get_signed_balance(self, cr, uid, ids, field_names, arg=None, context=None):
        res = dict.fromkeys( ids, False )
        compute_res = self.__compute(cr,uid,ids, ['balance'], arg=None, context=None )
        for rec in self.browse( cr, uid, ids, context ):
            if rec.is_sign_reversed:
                res[rec.id] = -1 * compute_res[rec.id]['balance']
            else:
                res[rec.id] = compute_res[rec.id]['balance']
        return res

    def _is_sign_reversed(self, cr, uid, ids, field_names, arg=None, context=None):
        res = dict.fromkeys( ids, False )
        for rec in self.browse( cr, uid, ids, context ):
            if rec.code and len(rec.code) > 0 and rec.code[0] in ['2','3','4']:
                res[rec.id] = True
            else:
                res[rec.id] = False
        return res

    _columns = { 'balance_signed' : fields.function( _get_signed_balance, digits_compute=dp.get_precision('Account'), string='Signed balance' ),
                 'is_sign_reversed' : fields.function( _is_sign_reversed, type="boolean", string="Is balance reversed?" ),
               }