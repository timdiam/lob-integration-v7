import locale
from openerp.osv import fields, osv, orm
import logging
import decimal_precision as dp

_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')

class purchase_order(osv.osv):
    _inherit = "purchase.order"
    def _amount_all2(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        cur_obj=self.pool.get('res.currency')
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_tax_iva': 0.0,
                'amount_total_plus_iva_less_retention': 0.0,
                'amount_total_plus_iva': 0.0,
                'amount_retention': 0.0,
            }
            val = val1 = 0.0
            total_iva=0.0
            total_retenciones=0.0
            cur = order.pricelist_id.currency_id
            for line in order.order_line:
                val1 += line.price_subtotal
                tax_info = self.pool.get('account.tax').compute_all(cr, uid, line.taxes_id, line.price_unit, line.product_qty, line.product_id, order.partner_id)['taxes']
                for tax in tax_info:
                    percent = self.pool.get('account.tax').browse(cr,uid,tax['id'],context=context).amount
                    if percent > 0:
                        total_iva += tax['amount']
                    elif percent < 0:
                        total_retenciones += tax['amount']


            res[order.id]['amount_retention']=cur_obj.round(cr, uid, cur, total_retenciones)
            res[order.id]['amount_tax_iva']=cur_obj.round(cr, uid, cur, total_iva)
            x=cur_obj.round(cr, uid, cur, val1)
            res[order.id]['amount_total_plus_iva']=x +  res[order.id]['amount_tax_iva']
            res[order.id]['amount_total_plus_iva_less_retention']=x + res[order.id]['amount_tax_iva']+res[order.id]['amount_retention']
        return res

    def _get_order2(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('purchase.order.line').browse(cr, uid, ids, context=context):
            result[line.order_id.id] = True
        return result.keys()


    _columns={
        'amount_total_plus_iva_less_retention': fields.function(_amount_all2, digits_compute= dp.get_precision('Account'), string='Total + IVA - Retenciones',
            store={
                'purchase.order.line': (_get_order2, None, 11),
            }, multi="sums", help="The tax amount"),
        'amount_total_plus_iva': fields.function(_amount_all2, digits_compute= dp.get_precision('Account'), string='Total + IVA',
            store={
                'purchase.order.line': (_get_order2, None, 11),
            }, multi="sums", help="The tax amount"),
        'amount_retention': fields.function(_amount_all2, digits_compute= dp.get_precision('Account'), string='Total de retenciones',
            store={
                'purchase.order.line': (_get_order2, None, 11),
            }, multi="sums", help="The tax amount"),
        'amount_tax_iva': fields.function(_amount_all2, digits_compute= dp.get_precision('Account'), string='Total de Iva',
            store={
                'purchase.order.line': (_get_order2, None, 11),
            }, multi="sums", help="The tax amount"),

        }
