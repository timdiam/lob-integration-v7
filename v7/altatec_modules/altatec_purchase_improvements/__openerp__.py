{
    'name': 'Altatec Puchase Improvements',
    'version': '1.0',
    'description': """
       Cambia el default de name en purchase_requisition para que la secuencia se use al momento del create
    """,
    'author': 'Henry Lomas A',
    'website': 'www.altatececuador.com',
    "depends" : ['purchase','purchase_requisition'],
    "data" : [ 'purchase_requisition_view.xml'
               ,'purchase_view.xml',],
    "installable": True,
    "auto_install": False
}
