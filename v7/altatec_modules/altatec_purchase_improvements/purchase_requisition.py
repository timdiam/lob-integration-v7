# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time
from openerp import netsvc

from openerp.osv import fields,osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

class purchase_requisition(osv.osv):
    _inherit = "purchase.requisition"


    def on_change_responsable(self, cr, uid, ids, user_id, context=None):
        user=self.pool.get('res.users').browse(cr, uid, user_id, context)
        responsable_id = self.pool.get('hr.employee').search(cr, uid, [('name','=',user.name)], context)
        if responsable_id:
            responsable= self.pool.get('hr.employee').browse(cr, uid, responsable_id, context)[0]
            if responsable.department_id:
                return {'value':{'department':responsable.department_id.id}}

        return {'value':{'department':False}}

    _columns = {
        'name': fields.char('Requisition Reference', size=32),
        'create_date' : fields.datetime('Date Created', readonly=True),
        'create_uid'  : fields.many2one('res.users', 'Owner', readonly=True),
        'write_date'  : fields.datetime('Date Last Modified', readonly=True),
        'write_uid'   : fields.many2one('res.users', 'Last Modification User', readonly=True),
    }

    _defaults = {
        'name': ''
    }
    def create(self, cr, uid, vals, context={}):
        secuencia=self.pool.get('ir.sequence').get(cr, uid, 'purchase.order.requisition')
        vals.update({'name':secuencia})
        res=super(purchase_requisition, self).create(cr, uid, vals, context)


        return res