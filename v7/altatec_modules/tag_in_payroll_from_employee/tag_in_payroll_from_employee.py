import time

from datetime import datetime
from dateutil.relativedelta import relativedelta
import locale
from lxml import etree
import openerp.addons.decimal_precision as dp
import openerp.exceptions

from openerp.tools import float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import netsvc
from openerp import pooler
from openerp import tools
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
from operator import itemgetter

import logging
import xml.etree.cElementTree as ET



class hr_payslip(osv.osv):

    _inherit= 'hr.payslip'


    def onchange_employee_id(self, cr, uid, ids, date_from, date_to, employee_id=False, contract_id=False, context=None):
        res = {'value':{
                      'line_ids':[],
                      'input_line_ids': [],
                      'worked_days_line_ids': [],
                      #'details_by_salary_head':[], TODO put me back
                      'contract_id': False,
                      'struct_id': False,
                      }
            }
        if (not employee_id) or (not date_from) or (not date_to):
            return res
        res = super(hr_payslip, self).onchange_employee_id(cr, uid, ids, date_from, date_to, employee_id, contract_id, context=context)
        employees=self.pool.get('hr.employee').browse(cr,uid,employee_id)
        prueba = employees.category_ids
        list_category=[]
        for p in prueba:
            list_category.append(p.id)

        res['value'].update({'category_ids':list_category})
        return  res

    _columns={

                'category_ids': fields.many2many('hr.employee.category', 'employee_category_rel2', 'emp_id2', 'category_id2', 'Tags'),
    }




