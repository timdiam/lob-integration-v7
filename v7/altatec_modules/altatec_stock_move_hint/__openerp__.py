{
    'name'         : 'AltaTec Stock move hints',
    'version'      : '1.0',
    'description'  : """
                     Agrega la funcionalidad de ver disponibilidad de productos en transferencia de bodegas
                     """,
    'author'       : 'Henry Lomas A',
    'website'      : 'www.altatec.ec',
    "depends"      : [
                       'stock',
                       'altatec_stock_analytic'
                     ],
    "data"         : [ 'views/stock_picking.xml'
                      ],
    "installable"  : True,
    "auto_install" : False
}
