############################################################################################
#
############################################################################################
from openerp.osv import fields, osv, orm

class stock_move(orm.Model):

    _inherit = "stock.move"

    ###################################################################################
    #
    ###################################################################################
    def onchange_product_id(self, cr, uid, ids, prod_id=False, loc_id=False,loc_dest_id=False, partner_id=False):
        res  = super(stock_move, self).onchange_product_id(cr, uid, ids, prod_id, loc_id,loc_dest_id, partner_id)
        context = {}
        context.update({'states': ('done',), 'what': ('in', 'out')})
        res['value'].update({  'availability': [] } )
        if prod_id:

            location_obj   = self.pool.get('stock.location')


            # Get the product's uom_id
            product_obj = self.pool.get('product.product')
            product = product_obj.browse(cr, uid, prod_id, context=context )

            # Get the stock available of this product in all locations
            location_ids = location_obj.search( cr, uid, [("active","=",True),("usage","in",["internal"])], context=context )
            for location_id in location_ids:

                # If this location has any child locations, ignore it
                parent_ids = location_obj.search( cr, uid, [("location_id", "=", location_id)], context=context )
                if len(parent_ids) > 0:
                    continue


                context.update({'location': location_id})
                qty = product_obj.get_product_available(cr, uid, [prod_id], context=context)

                if( qty[prod_id] > 0 ):
                    res['value']['availability'].append( ( 0, 0, { 'location_id'   : location_id,
                                                                   'qty_available' : qty[prod_id],
                                                                   'uom_id'        : product.uom_id.id,
                                                                   }
                                                           ) )

        return res


    _columns={
        'availability'   : fields.one2many("stock.move.sources.availability.line", "source_line", "Disponibilidad",readonly=True),
        }

class stock_move_sources_availability_line(osv.osv_memory):

    _name = "stock.move.sources.availability.line"

    _columns = { 'source_line' : fields.many2one("stock.move", "Sources Line"),
                 'location_id'    : fields.many2one("stock.location", "Ubicacion ID"),
                 'qty_available'  : fields.float("Cantidad disponible"),
                 'uom_id'         : fields.many2one("product.uom","Unidad(es)"),
               }