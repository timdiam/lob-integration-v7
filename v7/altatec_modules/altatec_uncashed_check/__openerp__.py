# -*- coding: utf-8 -*-
{
    "name" : "Altatec Uncashed Check",
    "version" : "0.1",
    "description" : """
    This module is for debit or credit check added manually to reconcile.
    Este módulo es para agregar manualmente los cheques o depósitos no cobrados para su reconciliación.

    Authors:
    Denisse Ochoa
        """,
    "author" : "Denisse Ochoa",
    "website" : "www.altatec.com",
    "depends" : [],
    "category" : "Custom Modules",
    "init_xml" : [],
    "demo_xml" : [],
    "auto_install": False,
    "installable": True,
}

