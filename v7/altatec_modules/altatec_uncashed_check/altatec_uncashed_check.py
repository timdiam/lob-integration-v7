from openerp.osv import osv, fields, orm



class altatec_uncashed_check(osv.osv):
    _name = 'altatec.uncashed.check'

    _columns = {
        'name': fields.char('Descripcion'),
        'sign': fields.selection([('+', 'plus'), ('-', 'minus')], 'Signo'),
        'amount': fields.float('Monto'),
    }
