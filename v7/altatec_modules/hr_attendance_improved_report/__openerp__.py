# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 TRESCLOUD Cia. Ltda. (<http://www.trescloud.com>)
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

{
    'name' : 'HR Attendance Improved Report',
    'version' : '1.0',
    'author' : 'Trescloud Cia. Ltda.',
    'category' : 'Human Resource',
    'description' : """ 
    HR Attendance Improved Report
    
    Add a report and configuration to check the attendance between several times 
    configured in calendar of attendance in every contract for a employee
    
    Autor: Patricio Rangles
    
    """,
    'website': 'http://www.trescloud.com',
    'depends' : [
                 'hr_timesheet_sheet',
                 'hr_holidays',
                 'report_aeroo_ooo'
                 ],
    'data': [
        'report/improved_attendance_report.xml',
        'wizard/improved_attendance_wiz_view.xml',
        'hr_attendance_view.xml'
    ],
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: