# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 TRESCLOUD Cia. Ltda. (<http://www.trescloud.com>)
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp.osv import osv, fields
from tools.translate import _

class improved_attendance(osv.TransientModel):
    _name = 'improved.attendance'

    _columns = {
        'from_date' :fields.date('From Date',required=True),
        'to_date': fields.date('To Date',required=True)
    }

#    def _print_improved_attendance_excel_report(self, cr, uid, ids, data, context=None):
#        data = self.pre_print_report(cr, uid, ids, data, context=context)
#        return {'type': 'ir.actions.report.xml', 'report_name': 'account_trial_balance_excel', 'datas': data}

    def improved_attendance_excel_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = {}
        data = self.read(cr, uid, ids, [], context=context)[0]
        datas = {
       'ids': context.get('active_ids',[]),
       'model': 'hr.employee',
       'form': data
       }
        return {'type': 'ir.actions.report.xml', 'report_name': 'improved_attendance_aeroo_report_xls', 'datas': datas}

    def improved_attendance_pdf_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = {}
        data = self.read(cr, uid, ids, [], context=context)[0]
        datas = {
       'ids': context.get('active_ids',[]),
       'model': 'hr.employee',
       'form': data
       }
        return {'type': 'ir.actions.report.xml', 'report_name': 'improved_attendance_aeroo_report_pdf', 'datas': datas}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: