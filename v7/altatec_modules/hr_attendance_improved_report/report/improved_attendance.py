# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 TRESCLOUD Cia. Ltda. (<http://www.trescloud.com>)
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp.osv import osv, fields
from report import report_sxw
from datetime import datetime,timedelta
from tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT,DEFAULT_SERVER_DATETIME_FORMAT

def _offset_format_timestamp(src_tstamp_str, src_format, dst_format, server_to_client=True, ignore_unparsable_time=True, context=None):
    """
    Convert a source timestamp string into a destination timestamp string, attempting to apply the
    correct offset if both the server and local timezone are recognized, or no
    offset at all if they aren"t or if tz_offset is false (i.e. assuming they are both in the same TZ).

    @param src_tstamp_str: the str value containing the timestamp.
    @param src_format: the format to use when parsing the local timestamp.
    @param dst_format: the format to use when formatting the resulting timestamp.
    @param server_to_client: specify timezone offset direction (server=src and client=dest if True, or client=src and server=dest if False)
    @param ignore_unparsable_time: if True, return False if src_tstamp_str cannot be parsed
                                   using src_format or formatted using dst_format.

    @return: destination formatted timestamp, expressed in the destination timezone if possible
            and if tz_offset is true, or src_tstamp_str if timezone offset could not be determined.
    """
    if not src_tstamp_str:
        return False

    res = src_tstamp_str
    if src_format and dst_format:
        try:
            # dt_value needs to be a datetime.datetime object (so no time.struct_time or mx.DateTime.DateTime here!)
            dt_value = datetime.strptime(src_tstamp_str,src_format)
            if context.get("tz",False):
                try:
                    import pytz
                    if server_to_client:
                        src_tz = pytz.timezone("UTC")
                        dst_tz = pytz.timezone(context["tz"])
                    else:
                        src_tz = pytz.timezone(context["tz"])
                        dst_tz = pytz.timezone("UTC")
                    src_dt = src_tz.localize(dt_value, is_dst=True)
                    dt_value = src_dt.astimezone(dst_tz)
                except Exception,e:
                    pass
            res = dt_value.strftime(dst_format)
        except Exception,e:
            # Normal ways to end up here are if strptime or strftime failed
            if not ignore_unparsable_time:
                return False
            pass
    return res


class Parser(report_sxw.rml_parse):
   
    _name = 'report.improved.attendance'
    
    def __init__(self, cr, uid, name, context=None):
        super(Parser, self).__init__(cr, uid, name, context=context)
#        self.count = 0
        self.localcontext.update({
                'get_data': self.get_data,
                'in_out_traslate': self.in_out_traslate,
        })
        self.context = context

    def get_data(self, emp, data):
        context = {}
        context.update({'tz': self.pool.get('res.users').browse(self.cr, self.uid, self.uid).tz or False})
        hr_attendance_obj = self.pool.get('hr.attendance')
        contract_obj = self.pool.get("hr.contract")
        res_cal_atten_obj = self.pool.get("resource.calendar.attendance")
        res_calendar_obj = self.pool.get("resource.calendar")
        payslip_obj = self.pool.get('hr.payslip')
        # dates related to TimeZone of user, add time to search correctly
        datetime_from = data['from_date'] + " 00:00:00"
        datetime_to = data['to_date'] + " 23:59:59"
        if 'tz' in context and context['tz'] != False:
            # The user have a TimeZOne configured, make the traslation
            datetime_from = _offset_format_timestamp(datetime_from, "%Y-%m-%d %H:%M:%S", DEFAULT_SERVER_DATETIME_FORMAT, server_to_client=False, context=context) 
            datetime_to = _offset_format_timestamp(datetime_to, "%Y-%m-%d %H:%M:%S", DEFAULT_SERVER_DATETIME_FORMAT, server_to_client=False, context=context)
        else:
            # generated a warning showing this error 
            raise osv.except_osv(_("No Time Zone"), _("No Time Zone configured in the User"))
            
        IDS = hr_attendance_obj.search(self.cr, self.uid, [('employee_id','=',emp.id),
                                                           ('name','>=',datetime_from),
                                                           ('name','<=',datetime_to)], order='name')
        #hr_list = []
        # group by date
        date_list = []
        last_date = ''
        #temp_date = {}
        lista_id = -1
        for attendance_rec in hr_attendance_obj.browse(self.cr, self.uid, IDS):
            attendance_rec_time =  _offset_format_timestamp(attendance_rec.name, "%Y-%m-%d %H:%M:%S", DEFAULT_SERVER_DATETIME_FORMAT, context=context)
            attendance_create_date = _offset_format_timestamp(attendance_rec.create_date, "%Y-%m-%d %H:%M:%S", DEFAULT_SERVER_DATETIME_FORMAT, context=context)
            # group by date
            date_now = attendance_rec_time[:10]
            if last_date != date_now:
                last_date = date_now
                date_list.append({
                                'date':date_now,
                                'res': [],
                                })
                lista_id = lista_id + 1
                
            res={'date_registry':attendance_rec_time,
                 'date_create':attendance_create_date,
                 'action':attendance_rec.action == 'sign_in' and 'IN' or 'Out',
                 'comment':attendance_rec.comments or '',
                 'status':'',
            }
#            contact_id = contract_obj.search(self.cr, self.uid, [("employee_id", "=", attendance_rec.employee_id.id)])
            contact_id = payslip_obj.get_contract(self.cr, self.uid, emp, data['from_date'], data['to_date'])
            if not contact_id:
                res.update({'status':_('UNDEFINED')})
            if contact_id:
                contract_data = contract_obj.browse(self.cr, self.uid, contact_id[0])
                if not contract_data.working_hours:
                    raise osv.except_osv(_("No Working Hours"), _("No Work hour schedule define for this contract"))
                if contract_data.working_hours:
                    delta = datetime.strptime(attendance_rec.name, '%Y-%m-%d %H:%M:%S')
                    calendar_attendance = res_cal_atten_obj.search(self.cr, self.uid, [("dayofweek","=", str(delta.weekday()))])
                    hours_per_day = False
                    for hour_diff in res_cal_atten_obj.browse(self.cr, self.uid, calendar_attendance):
                        start_hour = hour_diff.hour_from
                        end_hour = hour_diff.hour_to
                        hours_per_day += hour_diff.hour_to - hour_diff.hour_from
                        reg_time = str(delta.time())
#                        working_hore = res_calendar_obj.working_hours_on_day(self.cr, self.uid, contract_data.working_hours, delta)
                        attendance_data = res_calendar_obj.interval_get(self.cr, self.uid, contract_data.working_hours.id, delta, hours_per_day)
                        if attendance_rec.action == 'sign_in':
                            if reg_time <= attendance_data[0][0].strftime("%H:%M:%S"):
                                res.update({'status':_('EARLIER')})
                            if reg_time >= attendance_data[0][0].strftime("%H:%M:%S"):
                                res.update({'status':_('LATE')})
                            elif reg_time == attendance_data[0][0].strftime("%H:%M:%S"):
                                res.update({'status':_('OK')})
                        elif attendance_rec.action == 'sign_out':
                            if len(attendance_data) > 1:
                                if reg_time <= attendance_data[1][1].strftime("%H:%M:%S"):
                                    res.update({'status':_('EARLIER')})
                                if reg_time >= attendance_data[1][1].strftime("%H:%M:%S"):
                                    res.update({'status':_('LATE')})
                                elif reg_time == attendance_data[1][1].strftime("%H:%M:%S"):
                                    res.update({'status':_('OK')})
                        reg_time = delta.strftime("%M")
                        create_time = datetime.strptime(attendance_rec.create_date, '%Y-%m-%d %H:%M:%S')
                        cr_time = create_time.strftime("%M")
                        if int(cr_time) - int(reg_time) == 1:
                            res.update({'status':_('ALARMED')})
                        check_date = datetime.strptime(attendance_rec.name, '%Y-%m-%d %H:%M:%S')
                        if attendance_data[0][0] and attendance_data[0][0].strftime("%Y-%m-%d") !=  check_date.strftime("%Y-%m-%d"):
                            res.update({'status':_('UNDEFINED')})
                            
            # add this record to the corresponding group
            date_list[lista_id]['res'].append(res)
            #hr_list.append(res)
            
        #return hr_list
        return date_list

    def in_out_traslate(self, text):
        
        if text == 'IN':
            return "Entrada"
        
        if text == 'Out':
            return "Salida"
        
        return text


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
