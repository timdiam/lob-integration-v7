# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 TRESCLOUD Cia. Ltda. (<http://www.trescloud.com>)
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from osv import osv, fields

class hr_attendance(osv.Model):
    
    _inherit = "hr.attendance"
    
    _columns = {
        'comments':fields.char('Comments',size=252),
        'create_date':fields.datetime('Date',readonly=True),
        'calendar_id' : fields.many2one("resource.calendar", "Resource's Calendar"),
    }
    
#    def create(self, cr, uid, vals, context=None):
#        res = super(hr_attendance,self).create(cr, uid, vals, context=context)
#        rec = self.browse(cr,uid,res,context=context)
#        self.write(cr,uid,res,{'calendar_id':rec.employee_id.contract_id.working_hours.id or ''},context=context)
#        return res

class hr_config_settings(osv.osv_memory):
    _inherit = 'hr.config.settings'

    _columns = {
        'punctuality_tolerance': fields.integer('Punctuality Tolerance',
            help ="""Minutes of tolerance in attendance timing compared to the contract working schedule."""),
    }
    _defaults={
        'punctuality_tolerance':5
    }