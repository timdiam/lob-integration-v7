# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010-2013 Elico Corp. All Rights Reserved.
#    Author: Andy Lu <andy.lu@elico-corp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, osv, orm

##############################################################################
# Inherited stock.warehouse class definition. Adds a many2many of res.user's
# that are "Managers" of the warehouse. Also add's a group_location that is used
# to group the locations of a warehouse
##############################################################################
class stock_warehouse( orm.Model ):

    _inherit = 'stock.warehouse'

    _columns = { 'manager_ids'    : fields.many2many( 'res.users', 'rel_warehouse_users', 'warehouse_id', 'user_id', string='Responsables'),
                 'group_location' : fields.many2one('stock.location',string='Ubicacion de Agrupacion',domain=[('usage','in',['internal','view'])]),
               }

##############################################################################
# Inherited stock.location class definition
##############################################################################
class stock_location(osv.osv):

    _inherit = 'stock.location'

    ##############################################################################
    #
    ##############################################################################
    def get_need_test_origin(self,cr,uid,move_id, move_state, context={}):
        move = self.pool.get('stock.move').browse(cr,uid,move_id,context=context)
        location_origin = self.browse(cr,uid,move.location_id.id,context=context)
        location_destination = self.browse(cr,uid,move.location_dest_id.id,context=context)
        if location_origin.usage in ['inventory','procurement','supplier','customer','production']:
            return False
        else:
            if location_destination.usage in ['inventory','procurement','supplier','customer','production']:
                if move_state == 'assigned':
                    return False
                else:
                    return True
            return True

    ##############################################################################
    #
    ##############################################################################
    def get_need_test_destination(self,cr,uid,move_id, move_state, context={}):
        move = self.pool.get('stock.move').browse(cr,uid,move_id,context=context)
        location_origin = self.browse(cr,uid,move.location_id.id,context=context)
        if location_origin.usage in ['inventory','procurement','supplier','customer','production']:
            if move_state == 'assigned':
                return False
            else:
                return True
        else:
            return False

    ##############################################################################
    #
    ##############################################################################
    def get_warehouse_from_location(self,cr,uid,location_id, context={}):

        warehouse_ids = self.pool.get('stock.warehouse').search(cr,uid,[],context=context)
        location = self.browse(cr,uid,location_id,context=context)
        for warehouse in self.pool.get('stock.warehouse').browse(cr,uid,warehouse_ids,context=context):
            c1 = warehouse.group_location.parent_left < location.parent_left
            c2 = warehouse.group_location.parent_left < location.parent_right
            c3 = location.parent_left < warehouse.group_location.parent_right
            c4 = location.parent_right < warehouse.group_location.parent_right

            if (c1 and c2 and c3 and c4) or (warehouse.group_location.id == location.id):
                return warehouse.id
        return False

##############################################################################
#
##############################################################################
class stock_move( orm.Model ):

    _inherit = 'stock.move'

    ##############################################################################
    #
    ##############################################################################
    def add_approval(self,cr,uid,ids,context={}):
        self.write(cr,uid,ids,{'manager_approved':True},context=context)
        return True

    ##############################################################################
    #
    ##############################################################################
    def write( self, cr, uid, ids, values, context = None):

        if context is None:
            context = { }
        if not hasattr( ids, '__iter__' ):
            ids = [ ids ]

        if 'state' in values and values['state'] in ['assigned','done']:
            moves = self.browse(cr,uid,ids,context=context)
            for m in moves:
                if not self.pool.get('res.users').can_do_stock_move(cr,uid,m.id,values['state'],context=context):
                    if not m.manager_approved:
                        raise osv.except_osv( ( 'ERROR' ), ( 'Usuario no es el responsable de esa bodega' ) )

        res = super(stock_move, self).write(cr,uid,ids,values, context=context)

        # Now that we've saved the record, let's call write on the picking to recalculate its warehouse_code field
        for move in self.browse(cr, uid, ids, context=context):
            if move.picking_id:
                move.picking_id.write({})

        return res

    ##############################################################################
    #
    ##############################################################################
    _columns = { 'manager_approved' : fields.boolean('Aprobado por Responsable?'),
               }

##############################################################################
# Inherited stock.picking class definition
##############################################################################
class stock_picking( orm.Model ):

    _inherit = 'stock.picking'

    # #############################################################################################
    # # Calculates a "warehouse_code" for the picking. For example, If a picking has moves that
    # # are sourced from locations in warehouse 8 and 9, then the code will be "-8-9-"
    # #############################################################################################
    def get_warehouse_code(self, cr, uid, ids, field_name, field_value, arg, context=None):

        res = {}

        location_obj = self.pool.get('stock.location')

        for picking in self.browse(cr, uid, ids, context=context):
            warehouse_code = ""
            warehouse_id_list = []

            for move in picking.move_lines:
                warehouse_from_id = location_obj.get_warehouse_from_location(cr, uid, move.location_id.id, context=context)
                warehouse_to_id   = location_obj.get_warehouse_from_location(cr, uid, move.location_dest_id.id, context=context)
                if warehouse_from_id:
                    warehouse_id_list.append(warehouse_from_id)
                if warehouse_to_id:
                    warehouse_id_list.append(warehouse_to_id)

            if len(warehouse_id_list):
                warehouse_code += "-"
            for warehouse_id in set(warehouse_id_list):
                warehouse_code += str(warehouse_id) + "-"

            res[picking.id] = warehouse_code

        return res

    ###########################################################################################
    # Calculates a "warehouse_code" for the picking. For example, If a picking has moves that
    # are sourced from locations in warehouse 8 and 9, then the code will be "-8-9-"
    ###########################################################################################
    _columns = { "warehouse_code" : fields.function(get_warehouse_code, type="char", store=True, string="Warehouse Code"),
               }
