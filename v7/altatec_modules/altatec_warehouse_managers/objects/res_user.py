# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010-2013 Elico Corp. All Rights Reserved.
#    Author: Andy Lu <andy.lu@elico-corp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv, orm

##############################################################################
#
##############################################################################
class res_users( orm.Model ):

    _inherit = 'res.users'

    ##############################################################################
    #
    ##############################################################################
    def can_do_stock_move(self, cr, uid, move_id, move_state, context={}):
        location_obj = self.pool.get('stock.location')
        move = self.pool.get('stock.move').browse(cr,uid,move_id,context=context)

        warehouse_list = []
        for w in self.browse(cr,uid,uid,context=context).warehouse_ids:
            warehouse_list.append(w.id)

        if location_obj.get_need_test_origin(cr,uid,move_id, move_state, context=context):
            warehouse_id = location_obj.get_warehouse_from_location(cr,uid,move.location_id.id,context=context)
            if not warehouse_id:
                raise osv.except_osv( ( 'Error' ), ( 'La ubicacion no se encuentra adentro ningun bodega.  Consulte con Altatec.' ) )
            if not warehouse_id in warehouse_list:
                if move.location_id.id != 47 and move.location_id.id != 48 and move.location_id.id != 49:
                    return False

        if location_obj.get_need_test_destination(cr,uid,move_id, move_state, context=context):
            warehouse_id = location_obj.get_warehouse_from_location(cr,uid,move.location_dest_id.id,context=context)
            if not warehouse_id:
                raise osv.except_osv( ( 'Error' ), ( 'La ubicacion no se encuentra adentro ningun bodega.  Consulte con Altatec.' ) )
            if not warehouse_id in warehouse_list:
                if move.location_id.id != 47 and move.location_id.id != 48 and move.location_id.id != 49:
                    return False


        return True

    ##############################################################################
    #
    ##############################################################################
    _columns = { 'warehouse_ids': fields.many2many( 'stock.warehouse', 'rel_warehouse_users', 'user_id', 'warehouse_id', string='Responsables'),
               }
