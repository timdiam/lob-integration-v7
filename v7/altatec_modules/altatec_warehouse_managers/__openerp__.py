{
    'name': 'Altatec Warehouse Managers',
    'version': '1.0',
    'description': """
        This module adds a person responsible (or many) to a warehouse.
        The module then restricts the ability to process a stock.picking if the current user is not
        listed as a manager for the warhouse.  If the picking is coming from multiple warehouses,
        we add a field on stock.move to allow a manager to manually flag the move as OK to be processed.
    """,
    'author': 'Tim Diamond',
    'website': 'www.altatececuador.com',
    "depends" : [
                    'stock',

                ],
    "data" : [  'views/stock_view.xml'

             ],
    "installable": True,
    "auto_install": False
}
