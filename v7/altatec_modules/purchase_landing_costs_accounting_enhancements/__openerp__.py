# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 Serpent Consulting Services (<http://www.serpentcs.com>)
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

{
    'name' : 'Purchase Landing Cost Enhancements',
    'version' : '1.4',
    'author' : 'Trescloud',
    'category' : 'Purchases',
    'description' : """
    This module enhances functionality of purchase landing cost 
    Funcion: 
        Calcula el costo d eimportacion a los productos
        
    TRESCloud S.A.
    Romero David
    """,
    'website': 'http://www.trescloud.com',
    'depends' : ['purchase_landing_costs'],
    'data': [
             'views/product_template_view.xml',
             'views/account_move_line_view.xml',
             'views/purchase_order_view.xml',
             'views/stock_view.xml',
    ],
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: