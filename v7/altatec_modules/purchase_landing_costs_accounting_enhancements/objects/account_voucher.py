import time
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

class account_voucher(osv.osv):
    _inherit = 'account.voucher'
    _name = 'account.voucher'

    def get_value_voucher_line_stock(self, cr, user, l_cr_ids,l_dr_ids, currency_id, amount, context=None):
        '''
        Retorna una linea unica de pago asociada a la factura provista
        #ADVERTENCIA: Funciona solo para pagos desde facturas de venta
        #TODO: Extenderla para incluir facturas de compra, y otros documentos
        '''
        """"""
        move_obj = self.pool.get('account.move.line')
        
        #obtenemos las lineas de movimientos contable de la factura por pagar ordenados por fecha de vencimiento
        #TODO: Mejorar la eficiencia... buscar todos los miles de asientos por invice_id no es eficiente
        if not l_cr_ids:
            raise osv.except_osv(_('Error de Pago!'),_("No se ha encontrado el asiento contable a pagar") )
        if not l_dr_ids:
            raise osv.except_osv(_('Error de Pago!'),_("No se ha encontrado el asiento contable a pagar") )        
        move_lines_dr_to_pay = move_obj.browse(cr, user, l_dr_ids, context=context)
        move_lines_cr_to_pay = move_obj.browse(cr, user, l_cr_ids, context=context)
        
        #contruimos la linea a pagar
        line_cr_ids = []
        line_dr_ids = []
        writeoff_amount = 0.0 #valor residual, en escenario ideal es 0.00        
        for move_line in move_lines_cr_to_pay:
            #calculamos el monto a pagar en cada linea
            #print move_line.id.amount_residual
            #amount_to_pay = min(writeoff_amount, move_line.id.amount_residual) or 0.0
            #TODO: Agregar redondeo de openerp
            #writeoff_amount = writeoff_amount - amount_to_pay
            line_cr_ids.append({
                            'currency_id': currency_id, 
                            'amount': move_line.debit, 
                            'date_due': move_line.date, 
                            'name': move_line.name, 
                            'date_original': move_line.date_created, 
                            'move_line_id': move_line.id, 
                            'amount_unreconciled': move_line.amount_residual, 
                            'type': 'cr', 
                            'amount_original': move_line.debit, 
                            'account_id': move_line.account_id.id,
                            })
        for move_line in move_lines_dr_to_pay:
            #calculamos el monto a pagar en cada linea
            #amount_to_pay = min(writeoff_amount, move_line.amount_residual) or 0.0
            #TODO: Agregar redondeo de openerp
            #writeoff_amount = writeoff_amount - amount_to_pay
            line_dr_ids.append({
                            'currency_id': currency_id, 
                            'amount': move_line.amount,
                            'date_due': move_line.date, 
                            'name': move_line.name, 
                            'date_original': move_line.date_created, 
                            'move_line_id': move_line.id, 
                            'amount_unreconciled': move_line.amount_residual, 
                            'type': 'dr', 
                            'amount_original': move_line.credit, 
                            'account_id': move_line.account_id.id,
                            })
        return {'line_cr_ids': line_cr_ids, 'line_dr_ids': line_dr_ids, 'writeoff_amount': writeoff_amount}
    
               
account_voucher()