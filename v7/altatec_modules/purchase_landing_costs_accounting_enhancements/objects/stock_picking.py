# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2014 TRESCLOUD Cia Ltda (trescloud.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import netsvc
from tools.translate import _
import openerp.addons.decimal_precision as dp
import time

    
class stock_move(osv.osv):

    _inherit = "stock.move"

    def _landing_costs_price_unit_with_costs(self, cr, uid, ids, name, args, context):
        if not ids:
            return {}
        result = {}
        """ 
                Funcion creada para calcular el costo del produto con los landing cost especificos para cada producto 
                Variables por la que sepueden realizar los calculos del landing cost actualmente 
                        cantidad     qty_total = qty_total + line.product_qty,  
                        precio       preci_out_total = preci_out_total + line.price_unit_without_costs, 
                        Peso         peso_total = peso_total + line.qmt,  
                Agregar por ejemplo bajo las lineas existentes y agregar el if para la devolucion del resultado    
                para agregar buscar "#agregar aqui" en el codigo 
                        Valumen      valumen_total = valumen_total + line.valumen,  
            """
        landing_cost_obj = self.pool.get('purchase.landing.cost.position')
        picking_in_obj = self.pool.get('stock.picking') 
        product_obj = self.pool.get('product.product')
        product_qty_total=0
        price_unit_without_costs_total=0
        distribution_type=""
        """lista de lineas en move"""        
        for move_line in self.browse(cr, uid, ids):
            """lista de lineas en landing"""
            total_qty=0
            total_price=0
            total_weight=0
            #agregar aqui
            if move_line.product_id.landing_cost_calculate:             
                landing_list = landing_cost_obj.search(cr, uid, [('picking_id','=',move_line.picking_id.id)])
                #landing = landing_cost_obj.browse(cr, uid, landing_list)
                for landing_lines in landing_cost_obj.browse(cr, uid, landing_list):
                    distribution_type = landing_lines.distribution_type
                    # si se requiere que se divida para aalgun o algunos productos en especial el LC
                    if landing_lines.product_ids:
                        """ si existe productos en landing para asignar el impuesto"""
                        for landing_line_obj in landing_lines.product_ids:
                            if landing_line_obj == move_line.product_id:
                                #landing_l= self.search(cr, uid, [('product_id','=',move_line.product_id.id)])
                                product_qty_total = 0
                                price_unit_without_costs_total =0
                                product_weight_total=0
                                #agregar aqui
                                for m_line in self.browse(cr, uid, ids ):
                                    if m_line.product_id == landing_line_obj:
                                        product_qty_total += m_line.product_qty
                                        price_unit_without_costs_total += m_line.price_unit_without_costs * m_line.product_qty
                                        product_weight_total += m_line.product_id.weight * m_line.product_qty
                                        #agregar aqui
                                #agregar aqui
                                    
                                if product_weight_total !=0 :
                                    percentage_weight = (move_line.product_id.weight * move_line.product_qty)/product_weight_total
                                    total_weight += (percentage_weight * landing_lines.amount)/move_line.product_qty
                                    
                                if price_unit_without_costs_total !=0 :
                                    percentage_price_unit_without_costs = (move_line.product_qty * move_line.price_unit_without_costs) / price_unit_without_costs_total
                                    total_price += (percentage_price_unit_without_costs * landing_lines.amount)/move_line.product_qty
                                    
                                if product_qty_total !=0:
                                    percentage_qty = move_line.product_qty / product_qty_total
                                    total_qty += (percentage_qty * landing_lines.amount)/move_line.product_qty      
                                #agregar aqui
                                """ tener calculado del porcentage de este impuesto q se va aplicar
                                    en esta linea de stock.move  percentage =  """
                                """ sumar el amount sobre la lista de productos de landing"""
                            else:
                                   # raise osv.except_osv(_('Warning'),_('Existe un producto en la linea de la  pestania de Landing Costs que no se encuentra en los Productos: Producto'))
                                    """ sms de error de los productos a los que se quiere aplicar el error"""
                    else:                    
                        product_qty_total = 0
                        price_unit_without_costs_total = 0
                        product_weight_total = 0
                        for m_line in self.browse(cr, uid, ids ):
                            product_qty_total += m_line.product_qty
                            price_unit_without_costs_total += m_line.price_unit_without_costs * m_line.product_qty
                            product_weight_total += m_line.product_id.weight * m_line.product_qty
                            #agregar aqui
                        
                        #agregar aqui
                        if product_weight_total !=0 :
                            percentage_weight = (move_line.product_id.weight * move_line.product_qty)/product_weight_total
                            total_weight += (percentage_weight * landing_lines.amount)/move_line.product_qty
                            
                        if price_unit_without_costs_total !=0 :
                            percentage_price_unit_without_costs = (move_line.product_qty * move_line.price_unit_without_costs) / price_unit_without_costs_total
                            total_price += (percentage_price_unit_without_costs * landing_lines.amount)/move_line.product_qty
                            
                        if product_qty_total !=0:
                            percentage_qty = move_line.product_qty / product_qty_total
                            total_qty += (percentage_qty * landing_lines.amount)/move_line.product_qty
                                                                #agregar aqui
                        """sumar el amount sobre todos los stock.move"""
                """ agregar un if para presentar la respuesta correcta segun el tipo de calculo q se queire presentar """
                if distribution_type == 'per_value':
                    total_price += move_line.price_unit_without_costs
                elif distribution_type == 'per_weight':
                    total_price = total_weight + move_line.price_unit_without_costs
                elif distribution_type == 'per_unit':
                    total_price = total_qty + move_line.price_unit_without_costs
                #agregar aqui
                #else:
                    #unknown()
            if move_line.price_unit != total_price:
                self.write(cr, uid, [move_line.id], {'price_unit': total_price})                 
            result[move_line.id] = total_price  
        return result                    

    # TODO: Aniandir help faltante
    _columns = {
          'price_unit_with_costs': fields.function(_landing_costs_price_unit_with_costs, digits_compute=dp.get_precision('Product Price'), 
                                                   string='Unit Price', help=""),
        
        #=======================================================================
        # 'purchase_landing_cost': fields.function(_landing_cost, string='Purchase Landing Costs'),
        #=======================================================================
        }    
stock_move()

class stock_picking_in(osv.osv):

    _inherit = "stock.picking.in"

    _columns = {
        'account_id':fields.many2one('account.account','Filter for Landing Costs', help=""),
        'allow_landing_costs': fields.boolean('Allows landing Cost',help='Allow the inclusion of landing cost into the product, such as freight, insurance, etc /'),
        }    
stock_picking_in()

class stock_picking(osv.osv):
    _inherit = "stock.picking"


    """ Funcion para realizar los ascientos contables desde los movimientos de stock
    con la funcion "get_value_voucher_line"  se debe crear los DR y CR para los cuales :
    tomaremos los ids del journal creado por santiago con el valor calculado por el valor de ajuste - el valor original CR
    y los ids a.m.l  del landing cost con el amount  para armar el DR
    y 
    retornamos para que en la funcion "get_value_vouchar" pueda armar el asiento a realizar segun los montos y el journal de importaciones
    luego
    En la Funcion approve_sale_order_for_retail  agrega el movimiento del voucher q se esta realizando a cada una de las lineas CR y DR y 
    y
    crear en la base de datos el asiento realizado
    """

    def get_value_vouchar(self, cr, user, l_cr_ids,l_dr_ids, journal_id, amount, context=None):
        if context is None:
            context = {}
        
        vou_obj = self.pool.get('account.voucher')
        inv_obj = self.pool.get('account.invoice')
        journal_obj = self.pool.get('account.journal')
        list_cr_ids=[]
        list_dr_ids=[]
        for stock_move_obj in l_cr_ids:
            list_cr_ids.append(stock_move_obj.id)
        for stock_move_obj in l_dr_ids:
            list_dr_ids.append(stock_move_obj.id)
        inv_cr = inv_obj.browse(cr, user, list_cr_ids, context=context)
        inv_dr = inv_obj.browse(cr, user, list_dr_ids, context=context)
        #inv_dr = inv_obj.browse(cr, user, inv_dr_id, context=context)
        journal_id, currency_id, tax_id = self._get_journal_with_currency_with_tax(cr, user, inv_cr, journal_id, context)
        journal_id = 1
        currency_id = 1
        tax_id = 1
        
        #journal_id, currency_id, tax_id = self._get_journal_with_currency_with_tax(cr, user, inv_dr, journal_id, context)
        date = time.strftime('%Y-%m-%d')
        #partner_cr_id = self.pool.get('res.partner')._find_accounting_partner(inv_cr.partner_id).id
        #partner_dr_id = self.pool.get('res.partner')._find_accounting_partner(inv_dr.partner_id).id
#===============================================================================
# 
#         context.update({
#             'lang': 'en_US',
#             'default_amount': amount,
#             'close_after_process': True,
#             'tz': 'Europe/Brussels',
#             'user': user,
#             'payment_expected_currency': currency_id,
#             'active_model': 'account.invoice',
#             'invoice_id': inv[0].id.id,
#             'journal_type': 'sale',
#             'default_type': 'payment',
#             'invoice_type': inv[0].id.type,
#             'search_disable_custom_filters': True,
#             'default_reference': False,
#             'default_partner_id':  1, #partner_id or
#             'active_ids': [inv[0].id.id],
#             'type': inv[0].id.type in ('out_invoice', 'out_refund') and 'receipt' or 'payment',
#             'active_id': inv[0].id.id,
#             'retail_type': 'quick',
#         })
#===============================================================================
        value_voucher_line = vou_obj.get_value_voucher_line_stock(cr, user, l_cr_ids,l_dr_ids, currency_id, amount, context)
        line_dr_ids = value_voucher_line['line_dr_ids']
        line_cr_ids = value_voucher_line['line_cr_ids']
        
        account_id = journal_obj.browse(cr, user, journal_id).default_debit_account_id.id
        return {
            'active': True,
            'period_id': self._get_period(cr, user),
            'partner_id': partner_id,
            'journal_id': journal_id,
            'reference': inv.name,
            'amount': amount,
            'default_type': 'payment',
            'invoice_id': inv.id,
            'currency_id': currency_id,
            'close_after_process': True,
            'type': inv.type in ('out_invoice', 'out_refund') and 'receipt' or 'payment',
            'state': 'draft',
            'pay_now': 'pay_now',
            'pre_line': False, #probar con 1
            'c_line_dr_ids': line_dr_ids,
            'c_line_cr_ids': line_cr_ids,
            'date': date,
            'pos_session': pos_session_id,
            'tax_id': tax_id,
            'payment_option': 'without_writeoff',
            'comment': _('Write-Off'),
            #'payment_rate': values['value'].get('payment_rate', 1.0),
            'payment_rate_currency_id': currency_id,
            'account_id': account_id or False,
            'name': inv.name,
            'retail_type': 'quick',
        }


    def create_account_move_line(self, cr, uid, ids, context=None):
        """In state to done, create_account_move_line  then picking
        
        @return: True\
        @return: True\
        
        """
        """
        invoice_list lista de dr y cr
        """
        for res in self.browse(cr, uid, ids):
            l_cr_ids = res.move_lines
            #l_cr_ids = res.journal_ids  
            l_dr_ids = res.landing_costs_line_ids
            print l_cr_ids
            print l_dr_ids
            vouchar_vals = self.get_value_vouchar(cr, uid, l_cr_ids,l_dr_ids, res.id, 0.0, context)
        return True

    def action_done(self, cr, uid, ids, context=None):
        """Changes picking state to done.
        @return: True
        """
        if context is None:
            context = {}
        #self.create_account_move_line(cr, uid, ids, context)

        #=======================================================================
        # sale_obj = self.pool.get('sale.order')
        # sale_ids = context.get('active_ids', [])
        # invoice_list = []# 
#         invoice_res = sale_obj.manual_invoice(cr, user, sale_ids, context)
#         invoice_list = [invoice_res.get('res_id')]
#         self.pool.get('account.invoice').write(cr, user, invoice_list, {'retail_type':'quick'})
#         avl_obj = self.pool.get('account.voucher.line')
#         if invoice_list:
#             for pay in self.browse(cr, user, ids[0]).sale_payment_ids:
#                 #si es cero no hace falta crear movimiento contable   
#                 #DR verificar el journal en esta linea para crear la retencion o el movimeinto contable
#                 if pay.amount != 0: 
#                     vouchar_vals = self.get_value_vouchar(cr, user, invoice_list[0],
#                                                           pay.journal_id.id, pay.amount, context)
#                     voucher_id = self.pool.get('account.voucher').create(cr,
#                                                         user, vouchar_vals, context=context)
#                     for dr_lines in vouchar_vals.get('c_line_dr_ids'):
#                         dr_lines.update({'voucher_id': voucher_id})
#                         avl_obj.create(cr, user, dr_lines)
#                     for cr_lines in vouchar_vals.get('c_line_cr_ids'):
#                         cr_lines.update({'voucher_id': voucher_id})
#                         avl_obj.create(cr, user, cr_lines)
#                     wf_service.trg_validate(user, 'account.voucher', voucher_id,
#                                             'proforma_voucher', cr)
#===============================================================================
        return super(stock_picking, self).action_done(cr, uid, ids, context=context)
    
       
    _columns = {
        'account_id':fields.many2one('account.account','Filter for Landing Costs', help=""),
        'allow_landing_costs': fields.boolean('Allows landing Cost',help='Allow the inclusion of landing cost into the product, such as freight, insurance, etc /'),
        }
    
stock_picking()

class stock_picking_out(osv.osv):

    _inherit = "stock.picking.out"
    
    _columns = {
        'allow_landing_costs': fields.boolean('Allows landing Cost',help='Allow the inclusion of landing cost into the product, such as freight, insurance, etc /'),
        }

    
stock_picking_out()
