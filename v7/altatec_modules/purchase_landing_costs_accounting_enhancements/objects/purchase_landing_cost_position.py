# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 Serpent Consulting Services (<http://www.serpentcs.com>)
#    Copyright (C) 2004-2010 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from osv import fields, osv
import decimal_precision as dp

class product_product(osv.Model):
    _inherit = "product.product"
    
    _columns = {
                'landing_ids': fields.many2many('purchase.landing.cost.position','rel_landing_product','product_id','landing_id','Products', help="Products that increases the cost price of the product."),
                }
product_product()


class purchase_landing_cost_position(osv.Model):
    _inherit = "purchase.landing.cost.position"
    # TODO: Aniadir los helps de los campo, solo los voy a dejar declarando ya que no se que hacen cada uno
    _columns = {
        'distribution_type': fields.selection( [('per_value','Per Value'),('per_weight','Per Weight'),('per_unit','Per Quantity')], 'Amount Type', required=True, help="Defines if the amount is to be calculated for each quantity, weight or an absolute value"),
        'account_move_line_id' : fields.many2one('account.move.line', 'Lines', required=False, help="Account move lines related to the landing cost"),
        'date' : fields.date('Date', help=""),
        'name' : fields.char('Description', size=64, help=""),
        'partner_id' : fields.many2one('res.partner', 'Partner'),
        'original_amount' : fields.float('Original Amount', digits_compute=dp.get_precision('Product Price'), help=""),
        'dummy_product_id' : fields.many2one('product.product', 'Product', help=""),
        'dummy_partner_id' : fields.many2one('res.partner', 'Partner', help=""),
        'dummy_original_amount' : fields.float('Original Amount', digits_compute=dp.get_precision('Product Price'), help=""),
        'dummy_date' : fields.date('Date', help=""),
        'open_amount' : fields.float('Open Amount', help=""),
        'dummy_open_amount' : fields.float('Dummy Open Amount', help=""),
        'product_ids': fields.many2many('product.product','rel_landing_product','landing_id','product_id','Products',  help='Al Dejar este campo vacío se distribuira este costo para todos los productos. En el caso de escoger en esta campo almenos un producto, el monto se distribuira para os prodctos seleccionados.'),
               }
    
    def _check_amount(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid, ids, context=context):
            if rec.amount > rec.open_amount:
                return False
        return True
    

    _constraints = [
        (_check_amount, 'Error!\nAmount must be less than open amount', ['amount'])]
    
    def create(self, cr, uid, vals, context=None):
        if vals.get('dummy_product_id'):
            vals['product_id'] = vals.get('dummy_product_id', False)
        if vals.get('dummy_partner_id'):
            vals['partner_id'] = vals.get('dummy_partner_id', False)
        if vals.get('dummy_original_amount'):
            vals['original_amount'] = vals.get('dummy_original_amount', False)
        if vals.get('dummy_date'):
            vals['date'] = vals.get('dummy_date', False)
        if vals.get('dummy_open_amount'):
            vals['open_amount'] = vals.get('dummy_open_amount', False)
        return super(purchase_landing_cost_position, self).create(cr, uid, vals, context=context)
    
    def write(self, cr, uid, ids, vals, context=None):
        for rec in self.browse(cr, uid, ids, context=context):
            if vals.get('dummy_product_id'):
                vals['product_id'] = vals.get('dummy_product_id', False)
            if vals.get('dummy_partner_id'):
                vals['partner_id'] = vals.get('dummy_partner_id', False)
            if vals.get('dummy_original_amount'):
                vals['original_amount'] = vals.get('dummy_original_amount', False)
            if vals.get('dummy_date'):
                vals['date'] = vals.get('dummy_date', False)
            if vals.get('dummy_open_amount'):
                vals['open_amount'] = vals.get('dummy_open_amount', False)
        return super(purchase_landing_cost_position, self).write(cr, uid, ids, vals, context=context)
    
    def on_change_line(self, cr, uid, ids, line_id, context=None):
        """
        This function changes values of purchase landing cost lines 
        if the value of move_line changed
         @param self: The object pointer.
         @param cr: A database cursor
         @param uid: ID of the purchase landing cost position
         @param line_id: ID of move line id
         @param context: A standard dictionary
         @return: dictionary of fields 
        """
        value = {}
        if line_id:
            line_obj = self.pool.get('account.move.line')
            line_rec = line_obj.browse(cr, uid, line_id, context=context)
            value = {
                'name' : line_rec.name,
                'date' : line_rec.date,
                'partner_id' : line_rec.partner_id and line_rec.partner_id.id or False,
                'product_id' : line_rec.product_id and line_rec.product_id.id or False,
                'original_amount' : line_rec.credit or line_rec.debit,
                'dummy_date' : line_rec.date,
                'dummy_partner_id' : line_rec.partner_id and line_rec.partner_id.id or False,
                'dummy_product_id' : line_rec.product_id and line_rec.product_id.id or False,
                'dummy_original_amount' : line_rec.credit or line_rec.debit,
                'amount' : line_rec.amount_residual or line_rec.credit or line_rec.debit or 0.0,
                'open_amount' : line_rec.amount_residual_to_reconcile or 0.0,
                'dummy_open_amount' : line_rec.amount_residual_to_reconcile or 0.0
                }
        return {'value' : value}
        
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
