{
    'name': 'Altatec Notification RRHH',
    'version': '1.0',
    'description': """
        Mr-Jones Addons
    """,
    'author': 'Harry Alvarez',
    'website': 'www.google.com',
    "depends" : ['hr',
                 'hr_contract',
                 ],
    "data" : [
        'views/hr_contract_view.xml',

        ],
    "installable": True,
    "auto_install": False
}