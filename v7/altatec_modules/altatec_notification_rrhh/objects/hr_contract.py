from datetime import timedelta
from datetime import datetime
from dateutil.relativedelta import relativedelta

from openerp.osv import fields, osv, orm

class hr_contract(osv.osv):
    _inherit = "hr.contract"

    durations = {
                    'fin_prueba':+60,
                    'fin_contrato':+90,
                    }

    #TODO ALTATEC these dates should be configured on the company level
    def on_change_dates(self, cr, uid, ids, date, context=None):
        fecha_inicial=datetime.strptime(date,"%Y-%m-%d")
        start_prueba=fecha_inicial

        fin_prueba=fecha_inicial + relativedelta( days=self.durations['fin_prueba'])
        fin_contrato=fecha_inicial + relativedelta( days=self.durations['fin_contrato'])

        return {'value' : {'trial_date_end':fin_prueba.strftime('%Y-%m-%d') , 'trial_date_start':start_prueba.strftime('%Y-%m-%d'),'date_start':fecha_inicial.strftime('%Y-%m-%d'),'date_end':fin_contrato.strftime('%Y-%m-%d'), },}


    #TODO THIS NEEDS TO BE CONFIGURED ON THE COMPANY LEVEL
    def get_alert(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}
        today = datetime.today()
        for r in records:
            if not r.trial_date_start:
                result[r.id] = 'black'
            else:
                fecha_start = datetime.strptime(r.trial_date_start,"%Y-%m-%d")
                fecha_mark_1 = fecha_start + timedelta(days=60)
                fecha_mark_2 = fecha_start + timedelta(days=90)

                if (today >= fecha_start) and (today < fecha_mark_1):
                    result[r.id] = 'blue'
                elif (today >= fecha_mark_1) and (today < fecha_mark_2):
                    result[r.id] = 'red'
                elif (today >= fecha_mark_2):
                    result[r.id] = 'black'
        return result

    _columns={
        "alert":fields.function(get_alert, string="alert", method=True, type='char'),
        }
