{
    'name': 'AltaTec Point of Sale Sync',
    'version': '1.0',
    'description': """
    A module that uses the Cross-Company Module to connect to Odoo servers running as point of sale terminals.
    It keeps track of sale orders made at the POS terminals, and creates invoices and stock moves manually. It
    also syncs products and stock. It uses the client RUC to
    """,
    'author': 'Dan Haggerty',
    'website': 'www.altatececuador.com',
    "depends" : [ 'cross_company',
                  'account',
                  'stock',
                ],
    "data" : [ 'views/altatec_pos_terminal.xml',
             ],
    "installable": True,
    "auto_install": False
}