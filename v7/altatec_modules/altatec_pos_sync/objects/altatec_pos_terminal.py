####################################################################################################
#
####################################################################################################
from openerp.osv import fields, osv, orm
import xmlrpclib
import one2many_sorted

####################################################################################################
#
####################################################################################################
class altatec_pos_terminal(orm.Model):

    _name = "altatec.pos.terminal"

    ####################################################################################################
    #
    ####################################################################################################
    def sync_products(self, cr, uid, ids, context={}):

        for terminal in self.browse(cr, uid, ids, context=context):

            url      = terminal.cross_company.url
            port     = str(terminal.cross_company.port)
            dbname   = terminal.cross_company.dbname
            username = terminal.cross_company.username
            password = terminal.cross_company.password

            sock_common = xmlrpclib.ServerProxy( url + ':' + port + '/xmlrpc/common' )
            uid         = sock_common.login( dbname, username, password )
            sock        = xmlrpclib.ServerProxy( url + ':' + port + '/xmlrpc/object' )

            res = sock.execute( dbname, uid, password, 'altatec.pos.sync', 'sync_products' )

    ####################################################################################################
    #
    ####################################################################################################
    def sync_clients(self, cr, uid, ids, context={}):
        for terminal in self.browse(cr, uid, ids, context=context):

            url      = terminal.cross_company.url
            port     = str(terminal.cross_company.port)
            dbname   = terminal.cross_company.dbname
            username = terminal.cross_company.username
            password = terminal.cross_company.password

            sock_common = xmlrpclib.ServerProxy( url + ':' + port + '/xmlrpc/common' )
            uid         = sock_common.login( dbname, username, password )
            sock        = xmlrpclib.ServerProxy( url + ':' + port + '/xmlrpc/object' )

            res = sock.execute( dbname, uid, password, 'altatec.pos.sync', 'sync_clients' )

    ####################################################################################################
    #
    ####################################################################################################
    def sync_sale_orders(self, cr, uid, ids, context={}):
        for terminal in self.browse(cr, uid, ids, context=context):

            url      = terminal.cross_company.url
            port     = str(terminal.cross_company.port)
            dbname   = terminal.cross_company.dbname
            username = terminal.cross_company.username
            password = terminal.cross_company.password

            sock_common = xmlrpclib.ServerProxy( url + ':' + port + '/xmlrpc/common' )
            uid         = sock_common.login( dbname, username, password )
            sock        = xmlrpclib.ServerProxy( url + ':' + port + '/xmlrpc/object' )

            res = sock.execute( dbname, uid, password, 'altatec.pos.sync', 'sync_orders' )

    ####################################################################################################
    # Column definitions
    ####################################################################################################
    _columns = { "name"          : fields.char("Nombre de Tienda", required=True),
                 "cross_company" : fields.many2one("cross.company", "Servidor", required=True),
                 "location_id"   : fields.many2one("stock.location", "Ubicacion Interno", required=True),
                 "product_syncs" : one2many_sorted.one2many_sorted("altatec.pos.product.sync", "terminal_id", "Sincronizaciones de productos", order='date DESC'),
                 "client_syncs"  : one2many_sorted.one2many_sorted("altatec.pos.client.sync", "terminal_id", "Sincronizaciones de clientes", order='date DESC'),
                 "sale_order_syncs" : one2many_sorted.one2many_sorted("altatec.pos.sale.order.sync", "terminal_id", "Sincronizaciones de pedidos de venta", order='date DESC'),
                 'state'         : fields.related("cross_company", "state", type="selection",
                                                  selection=[('not_connected', 'No Conectado'), ('connected', 'Conectado')],
                                                  readonly="1", string="Estado",
                                                 ),
               }