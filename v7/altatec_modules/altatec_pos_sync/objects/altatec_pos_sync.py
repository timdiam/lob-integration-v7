####################################################################################################
#
####################################################################################################
from openerp.osv import fields, osv, orm

####################################################################################################
#
####################################################################################################
class altatec_pos_product_sync(orm.Model):

    _name = "altatec.pos.product.sync"

    _columns = { "terminal_id"        : fields.many2one("altatec.pos.terminal", "Tienda/Terminal", required=True, ondelete='cascade'),
                 "date"               : fields.datetime("Fecha de sincronizacion", required=True),
                 "num_products_added" : fields.integer("Num. productos creados", required=True),
                 "num_products_updated" : fields.integer("Num. productos actualizados", required=True),
                 "num_products_deleted" : fields.integer("Num. productos eliminados", required=True),
                 "products_added"   : fields.many2many( 'product.product', 'rel_product_sync_added', 'product_sync_id', 'product_id', 'Productos creados'),
                 "products_updated" : fields.many2many( 'product.product', 'rel_product_sync_updated', 'product_sync_id', 'product_id', 'Productos actualizados'),
                 "products_deleted" : fields.one2many("altatec.pos.product.sync.deletion", "sync_id", "Productos eliminados"),
                 "state" : fields.selection([("success", "Realizado"),("failed", "Error"),], "Estado", required=True),
                 "errors"  : fields.text("Errores"),
               }

    _defaults = { "date"               : fields.date.context_today,
                  "num_products_added" : 0,
                  "num_products_updated" : 0,
                  "num_products_deleted" : 0,
                  "state"              : "fail",
                }

class altatec_pos_product_sync_deletion(orm.Model):

    _name = "altatec.pos.product.sync.deletion"

    _columns = { "sync_id"      : fields.many2one("altatec.pos.product.sync", "Sync ID"),
                 "name"         : fields.char("Nombre"),
                 "default_code" : fields.char("Referencia Interna") ,
               }

####################################################################################################
#
####################################################################################################
class altatec_pos_client_sync(orm.Model):

    _name = "altatec.pos.client.sync"

    _columns = { "terminal_id"       : fields.many2one("altatec.pos.terminal", "Tienda/Terminal", required=True),
                 "date"              : fields.datetime("Fecha de sincronizacion", required=True),
                 "num_clients_added" : fields.integer("Num. clientes creados", required=True),
                 "num_clients_updated" : fields.integer("Num. clientes actualizados", required=True),
                 "num_clients_sync" : fields.integer("Num. clientes sincronizados", required=True),
                 "clients_added"   : fields.many2many( 'res.partner', 'rel_client_sync_added', 'client_sync_id', 'partner_id', 'Clientes creados'),
                 "clients_updated" : fields.many2many( 'res.partner', 'rel_client_sync_updated', 'client_sync_id', 'partner_id', 'Clientes actualizados'),
                 "clients_sync" : fields.many2many( 'res.partner', 'rel_client_sync_sync', 'client_sync_id', 'partner_id', 'Clientes sincronizados'),
                 "state"             : fields.selection([("success", "Realizado"),("failed", "Error"),], "Estado", required=True),
                 "errors"               : fields.text("Errores"),
               }

    _defaults = { "date"                : fields.date.context_today,
                  "num_clients_added"   : 0,
                  "num_clients_updated" : 0,
                  "num_clients_sync"    : 0,
                  "state"               : "fail",
                }

    #------------------------------------------------------------------------------------------------------------------#
    def create_client_lote(self, cr, uid, local_clients, context={}):
        client_obj = self.pool.get('res.partner')
        clients_sync = []

        for client in local_clients:
            try:
                print "Creating client in Central Server:", "[" + client['name'] + "]"
                new_client_id = client_obj.create(cr, uid, client, context=context)
                clients_sync.append(new_client_id)
            except Exception as e:
                raise osv.except_osv( "Error", "Con el cliente [" + client['vat'] + "] " + str(e) + "\n" )

        return clients_sync

####################################################################################################
#
####################################################################################################
class altatec_pos_invoice_order_sync(orm.Model):

    _name = "altatec.pos.invoice.order.sync"

    _columns = { "sale_order" : fields.char("Orden de venta", required=True),
                 "invoice"    : fields.many2one('account.invoice', 'Factura', required=True ),
                 'type_order' : fields.char("Tipo de Orden de venta", required=True),
                 "total"      : fields.float("Total", required=True),
               }

class altatec_pos_sale_order_sync(orm.Model):

    _name = "altatec.pos.sale.order.sync"

    _columns = { "terminal_id"        : fields.many2one("altatec.pos.terminal", "Tienda/Terminal", required=True),
                 "date"               : fields.datetime("Fecha de sincronizacion", required=True),
                 'num_sale_orders_added' : fields.integer("Num. pedidos sincronizados", required=True),
                 "invoice_sale_orders_added" : fields.many2many( 'altatec.pos.invoice.order.sync', 'rel_invoice_sale_orders_added', 'sale_order_sync_id', 'invoice_order_sync_id', 'Ordenes de venta sincronizados'),
                 "state"              : fields.selection([("success", "Realizado"),("failed", "Error"),], "Estado", required=True),
                 "errors"               : fields.text("Errores"),
               }

    _defaults = { "date"               : fields.date.context_today,
                  'num_sale_orders_added' : 0,
                  "state"              : "fail",
                }

    #------------------------------------------------------------------------------------------------------------------#
    # Permite hacer la sincronizacion de ordenes de compra en facturas
    #------------------------------------------------------------------------------------------------------------------#
    def sale_order_sync(self, cr, uid, local_orders, context={}):
        factura_obj = self.pool.get('account.invoice')
        orders_added = []

        # validamos el tipo de docuemnto
        documento_id = self.getDocumentInvoiceId(cr, uid, '18', context)
        if documento_id == False:
            raise osv.except_osv("Error", "El Tipo de documento: [Factura de Venta (Documentos autorizados utilizados en ventas excepto N/C N/D)] No existe en el servidor central")

        # validamos autorizacion del SRI
        autorizacion_sri_id = self.getAutorizacionSriId(cr, uid, '1245367891', context)
        if autorizacion_sri_id == False:
            raise osv.except_osv("Error", "La Aurotizacion del sRI codigo: [1245367891] No existe en el servidor central")

        # validamos la cuenta para los productos
        cuenta_id = self.getCuentaId(cr, uid, '101010101', 'Caja General', context)
        if cuenta_id == False:
            raise osv.except_osv("Error", "La Cuenta codigo: [101010101 - Caja General] No existe en el servidor central")

        # validamos el metodo de pago
        metodo_pago_id = self.getMetodoPagoId(cr, uid, 'Efectivo', context)
        if metodo_pago_id == False:
            raise osv.except_osv("Error", "El metodo de pago [Efectivo] No existe en el servidor central")

        # validamos el metodo de pago
        stock_location_id = self.getStockLocationId(cr, uid, 'Stock', context)
        if stock_location_id == False:
            raise osv.except_osv("Error", "El ubicaion del [Stock] No existe en el servidor central")


        # por cada orden de la tienda remota crearemos una factura en el servidor central
        for order in local_orders:
            invoice_order_id = 0

            try:
                #creamos una factura en el servidor central
                # validamos que exista el cliente
                cliente_id = self.getClienteIdByRuc(cr, uid, order['partner_vat'], context)
                if cliente_id == False:
                    raise osv.except_osv("Error", "El Cliente " + order['partner_vat'] + " No existe en el servidor central")

                #---------------------- Factura --------------------------
                # creamos una nueva factura para esta orden
                print "Creating invoice in Central Server: Order", "[" + order['name'] + "]"
                factura_id = self.nuevaFactura(cr, uid, order, cliente_id, cuenta_id, documento_id, autorizacion_sri_id, factura_obj, context)

                #---------------------- Log  invoice.order.sync --------------------------
                # si la creacion de la factura fue correcta
                invoice_order_id = self.nuevoLogInvoiceOrder(cr, uid, order, factura_id, context)

                #---------------------- Orden Pago --------------------------
                # Validamos el tipo de orden apra continuar con el proceso
                if order['order_type'] == "Venta":
                    # creamos una nueva orden de pago si la orden es tipo VENTA
                    print "Creating paid order in Central Server, Factura:", "[" + self.generateNumeroFactura(order) + "]"
                    self.nuevaOrdenPago(cr, uid, order, factura_id, cliente_id, metodo_pago_id, context)

                    # creamos un movimiento de stock si la orden es tipo VENTA
                    print "Creating stock move in Central Server, Factura:", "[" + self.generateNumeroFactura(order) + "]"
                    self.nuevoMovimientoStock(cr, uid, order, cliente_id, stock_location_id, context)

                elif order['order_type'] == "Devolucion":
                    # codigo para la devolucion
                    pass

                else:
                    # si es tarjeta de credito no se crea la orden de pago y la factura queda en estado abierta
                    pass

            except Exception as e:
                raise osv.except_osv("Error", str(e))

            #agregamos el id del objeto invoice_order
            orders_added.append(invoice_order_id)

        return orders_added

    #------------------------------------------------------------------------------------------------------------------#
    # Retorna el codigo de la factura, esta funcion es temporal
    #------------------------------------------------------------------------------------------------------------------#
    def generateNumeroFactura(self, order):
        return order['pos_reference']

    #------------------------------------------------------------------------------------------------------------------#
    # Crear Factura
    #------------------------------------------------------------------------------------------------------------------#
    def nuevaFactura(self, cr, uid, order, cliente_id, cuenta_id, documento_id, autorizacion_sri_id, factura_obj, context):
        factura_dict = {
            'invoice_address'          : order['partner_street'],
            'invoice_phone'            : order['partner_phone'],
            'date_invoice'             : order['date_order'],
            'origin'                   : order['name'],
            'name'                     : order['name'],
            'type'                     : 'out_invoice',
            'state'                    : 'draft',
            'partner_id'               : cliente_id,
            'currency_id'              : 3,
            'internal_number'          : self.generateNumeroFactura(order),
            'document_invoice_type_id' : documento_id,
            'authorizations_id'        : autorizacion_sri_id,
            'print_id'                 : self.getPrintId(cr, uid, order['pos_reference'], context),
        }
        # actualizamos para obtener el campo account_id
        factura_dict.update( factura_obj.onchange_partner_id(cr, uid, [], type=factura_dict['type'], partner_id=cliente_id)['value'] )
        # creamos
        factura_id = factura_obj.create(cr, uid, factura_dict, context=context)

        # agregamos los productos comprados en la roden a la nueva factura
        for line in order['lines']:
            factura_line_obj = self.pool.get('account.invoice.line')

            # validamos que exista el producto
            producto_id = self.getProductoIdByCode(cr, uid, line['product_code_default'], context)
            if producto_id == False:
                raise osv.except_osv("Error", "El producto " + line['product_code_default'] + " No existe en el servidor central")

            # creamos una nueva linea en la factura
            print "Creating order line in Central Server: [" + str(producto_id) + "] " + order['pos_reference']
            factura_line_dict = {
                'product_id' : producto_id,
                'quantity'   : line['quantity'],
                'discount'   : line['discount'],
                'account_id' : None,
                'invoice_id' : None
            }
            # actualizamos los campos
            factura_line_dict.update(factura_line_obj.product_id_change(cr, uid, [],
                    product=producto_id,
                    uom_id=False,
                    qty=line['quantity'],
                    partner_id=cliente_id,
                    fposition_id=factura_dict['fiscal_position'],
                    context=context)['value']
            )

            # actualizamos los campos
            factura_line_dict['invoice_line_tax_id'] = [(6, 0, tuple(factura_line_dict['invoice_line_tax_id']))]
            factura_line_dict['account_id'] = cuenta_id
            factura_line_dict['invoice_id'] = factura_id
            # creamos
            factura_line_obj.create(cr, uid,  factura_line_dict, context=context)

        # validamos la factura creada
        try:
            factura_obj.browse(cr, uid, [factura_id], context=context)[0].invoice_open_enhanced()
        except Exception as e:
            raise osv.except_osv("Error", "La factura[" + self.generateNumeroFactura(order['id']) + "] - " + str(e))

        return factura_id

    #------------------------------------------------------------------------------------------------------------------#
    # Crear Log de registro de la sync
    #------------------------------------------------------------------------------------------------------------------#
    def nuevoLogInvoiceOrder(self, cr, uid, order, factura_id, context):
         # creamos un nuevo registro en el log para esta factura
        invoice_order_dict = {
            "invoice"    : factura_id,
            "type_order" : order['order_type'],
            "total"      : order['amount_total'],
            "sale_order" : order['name'] + " - " + order['pos_reference'],
        }
        invoice_order_id = self.pool.get('altatec.pos.invoice.order.sync').create(cr, uid,  invoice_order_dict, context=context)

        return invoice_order_id

    #------------------------------------------------------------------------------------------------------------------#
    # Crear Orden de PAgo
    #------------------------------------------------------------------------------------------------------------------#
    def nuevaOrdenPago(self, cr, uid, order, factura_id, cliente_id, metodo_pago_id, context):
        pago_dict = {
            'currency_id' : 3,
            'type'        : 'receipt',
            'partner_id'  : cliente_id,
            'journal_id'  : metodo_pago_id,
            'amount'      : order['amount_total'],
            'name'        : "Pago de la Factura #" + self.generateNumeroFactura(order),
            'reference'   : "Pago de la Factura #" + self.generateNumeroFactura(order),
        }
        # actualizamos
        pago_dict.update( self.pool.get('account.voucher').onchange_partner_id(cr, uid, [],
                    date=order['date_order'],
                    ttype='receipt',
                    currency_id=3,
                    partner_id=cliente_id,
                    journal_id=metodo_pago_id,
                    amount=order['amount_total'],
                    context=context)['value']
        )
        # actualizamos los campos
        pago_dict['line_cr_ids'] = [(0, 0, line_dict) for line_dict in pago_dict['line_cr_ids']]
        pago_dict['line_dr_ids'] = [(0, 0, line_dict) for line_dict in pago_dict['line_dr_ids']]
        # creamos
        pago_id = self.pool.get('account.voucher').create(cr, uid, pago_dict, context=context)

        # validamos la orden de pago creada
        try:
            self.pool.get('account.voucher').browse(cr, uid, [pago_id], context=context)[0].proforma_voucher()
        except Exception as e:
            raise osv.except_osv("Error", "La orden de pago de la Factura #" + self.generateNumeroFactura(order) + " - " + str(e))

        return pago_id

    #------------------------------------------------------------------------------------------------------------------#
    # Crear Movimiento de Stock
    #------------------------------------------------------------------------------------------------------------------#
    def nuevoMovimientoStock(self, cr, uid, order, cliente_id, stock_location_id, context):
        for line in order['lines']:

            # validamos que exista el producto
            producto_id = self.getProductoIdByCode(cr, uid, line['product_code_default'], context)
            if producto_id == False:
                raise osv.except_osv("Error", "El producto " + line['product_code_default'] + " No existe en el servidor central")

            stock_dict = {
                'product_id'  : producto_id,
                'picking_id'  : None,
                'type'        : 'out',
                'location_id'  : stock_location_id,
                'location_dest_id'  : stock_location_id,
                'partner_id'  : cliente_id,
                'date_expected'  : order['date_order'],
            }
            # actualizamos
            stock_dict.update( self.pool.get('stock.move').onchange_product_id(cr, uid, [],
                        prod_id=producto_id,
                        loc_id=stock_location_id,
                        loc_dest_id=stock_location_id,
                        partner_id=cliente_id)['value']
            )
            # actualizamos los campos
            stock_dict['product_qty'] = line['quantity']
            # creamos
            stock_id = self.pool.get('stock.move').create(cr, uid, stock_dict, context=context)


    #------------------------------------------------------------------------------------------------------------------#
    # Retorna el ID de un cliente y recive el RUC
    #------------------------------------------------------------------------------------------------------------------#
    def getClienteIdByRuc(self, cr, uid, ruc, context):
        client_id_list = self.pool.get('res.partner').search(cr, uid, [('vat', '=', ruc)], context=context)

        if len( client_id_list ) > 0:
            return client_id_list[0]

        return False

    #------------------------------------------------------------------------------------------------------------------#
    # Retorna el ID d eun producto y recive el Codigo
    #------------------------------------------------------------------------------------------------------------------#
    def getProductoIdByCode( self, cr, uid, default_code, context):
        product_id_list = self.pool.get('product.product').search(cr, uid, [('default_code', '=', default_code)], context=context)

        if len( product_id_list ) > 0:
            return product_id_list[0]

        return False

    #------------------------------------------------------------------------------------------------------------------#
    # Retorna el ID de un invoice document y recibe el doc_code
    #------------------------------------------------------------------------------------------------------------------#
    def getDocumentInvoiceId(self, cr, uid, doc_code, context):
        doc_id_list = self.pool.get('account.invoice.document.type').search(cr, uid, [('code', '=', doc_code)], context=context)

        if len( doc_id_list ) > 0:
            return doc_id_list[0]

        return False

    #------------------------------------------------------------------------------------------------------------------#
    # Retorna el ID de una autorizacion del SRI
    #------------------------------------------------------------------------------------------------------------------#
    def getAutorizacionSriId(self, cr, uid, autorizacion_code, context):
        obj_id_list = self.pool.get('sri.authorizations').search(cr, uid, [('name', '=', autorizacion_code)], context=context)

        if len( obj_id_list ) > 0:
            return obj_id_list[0]

        return False

    #------------------------------------------------------------------------------------------------------------------#
    # Retorna el ID de una cuenta
    #------------------------------------------------------------------------------------------------------------------#
    def getCuentaId(self, cr, uid, code, name, context):
        obj_id_list = self.pool.get('account.account').search(cr, uid, [('code', '=', code), ('name', '=', name)], context=context)

        if len( obj_id_list ) > 0:
            return obj_id_list[0]

        return False

    #------------------------------------------------------------------------------------------------------------------#
    # Retorna el ID de un metodo de pago
    #------------------------------------------------------------------------------------------------------------------#
    def getMetodoPagoId(self, cr, uid, name, context):
        obj_id_list = self.pool.get('account.journal').search(cr, uid, [('name', '=', name)], context=context)

        if len( obj_id_list ) > 0:
            return obj_id_list[0]

        return False

    #------------------------------------------------------------------------------------------------------------------#
    # Retorna el ID de un stock location
    #------------------------------------------------------------------------------------------------------------------#
    def getStockLocationId(self, cr, uid, name, context):
        obj_id_list = self.pool.get('stock.location').search(cr, uid, [('name', '=', name)], context=context)

        if len( obj_id_list ) > 0:
            return obj_id_list[0]

        return False

    #------------------------------------------------------------------------------------------------------------------#
    # Retorna el ID del punto de impresion
    #------------------------------------------------------------------------------------------------------------------#
    def getPrintId(self, cr, uid, pos_reference, context):
        import re

        try:
            print_secuence = re.search('([0-9]{3}-[0-9]{3})', pos_reference).group(1)
        except AttributeError:
            print_secuence = '001-001'

        obj_id_list = self.pool.get('account.journal').search(cr, uid, [('name', '=', print_secuence)], context=context)

        if len( obj_id_list ) > 0:
            return obj_id_list[0]

        return False