#######################################################################################
#
#######################################################################################
from openerp.osv import osv, fields
from openerp.tools.translate import _

#######################################################################################
# pos_session_opening class inherit
#######################################################################################
class pos_session_opening(osv.osv_memory):

    _inherit = 'pos.session.opening'

    #######################################################################################
    # Funcion que se invoca cuando se presiona el boton de New Session y Start Selling del menu
    # Daily Operations en Point of Sale
    #######################################################################################
    def open_ui(self, cr, uid, ids, context=None):
        
        context = context or {}
        if 'retail_type' in context:
            if context['retail_type'] == 'quick':
                model_data = self.pool.get('ir.model.data')
                # Busqueda de las vistas tree y form de ordenes de venta y quick sale orders
                tree_view = model_data.get_object_reference(cr, uid, 'sale', 'view_order_tree')
                form_view = model_data.get_object_reference(cr, uid, 'sale_order_for_retail', 'quick_sale_order_form_view')
                value = { 'name'      : _('Create Quick Sale Orders'),
                          'view_type' : 'form',
                          'view_mode' : 'tree,form',
                          'res_model' : 'sale.order',
                          'views'     : [(tree_view and tree_view[1] or False, 'tree') , (form_view and form_view[1] or False, 'form') , (False, 'calendar')],
                          'type'      : 'ir.actions.act_window',
                        }
                return value
            else:
                return super(pos_session_opening,self).open_ui(cr, uid, ids, context)
        else:
            return super(pos_session_opening,self).open_ui(cr, uid, ids, context)