# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd.
#    (<http://www.acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import netsvc

##############################################################################
# wizard_sale_order_for_retail definition
##############################################################################
class wizard_sale_order_for_retail(osv.osv_memory):

    _name = 'wizard.sale.order.for.retail'

    _columns = { 'sale_payment_ids' : fields.one2many('sale.payment', 'sale_order_for_retail_id', 'Sale Payment'),
                 'invoice_number'   : fields.char('Invoice Number'),
                 'is_done'          : fields.boolean('Is Done'),
               }

    ##############################################################################
    # default_get()
    ##############################################################################
    def default_get(self, cr, user, fields, context=None):

        if context is None:
            context = {}

        data = super(wizard_sale_order_for_retail, self).default_get(cr, user, fields, context)
        sale_order_obj  = self.pool.get('sale.order')
        pos_session_obj = self.pool.get('pos.session')
        pos_session_id  = pos_session_obj.search(cr, user, [('user_id', '=', user)])

        if not pos_session_id:
            raise osv.except_osv(_('Error!'), _('Current user does not have any POS Session'))

        pos_session_id = pos_session_id[0]
        pos_brw = pos_session_obj.browse(cr, user, pos_session_id).config_id

        if context.get('active_id'):
            sale_order_brw = sale_order_obj.browse(cr, user, context.get('active_id'))
            lines = []

            # Create the sale.payment.lines
            if sale_order_brw.is_done == True:
                for record in sale_order_brw.sale_payments_ids:
                    lines.append((0, 0, { 'journal_id' : record.journal_id.id,
                                          'amount'     : record.amount,
                                          'is_done'    : True
                                        }
                                ))

                data.update({ 'sale_payment_ids' : lines,
                              'invoice_number'   : sale_order_brw.invoice,
                              'is_done'          : True
                            })
            else:
                sale_order_total = sale_order_brw.amount_total
                for payment_type in pos_brw.journal_ids:
                    lines.append((0, 0, { 'journal_id' : payment_type.id,
                                          'amount'     : sale_order_total
                                        }
                                ))
                    sale_order_total = 0.0
                data.update({ 'sale_payment_ids': lines })

        return data

    ##############################################################################
    # save_sale_order_for_retail()
    ##############################################################################
    def save_sale_order_for_retail(self, cr, user, ids, context=None):
        if context is None:
            context = {}
        cash = bank = 0
        sale_payment_so = self.pool.get('sale.payment.so')
        sale_order_obj = self.pool.get('sale.order')
        self_brw = self.browse(cr, user, ids[0])
        if context.get('active_id'):
            for wiz_data in self_brw.sale_payment_ids:
                if wiz_data.journal_id.type == 'cash':
                    cash += wiz_data.amount
                elif wiz_data.journal_id.type == 'bank':
                    bank += wiz_data.amount
                sale_payment_so.create(cr, user, {
                                    'so_id': context.get('active_id'),
                                    'journal_id': wiz_data.journal_id and \
                                                wiz_data.journal_id.id or False,
                                    'amount': wiz_data.amount
                                })
            sale_order_obj.write(cr, user, context.get('active_id'), {
                                'cash': cash,
                                'bank': bank,
                                'invoice': self_brw.invoice_number,
                                'is_done': True
                            }, context)
        sale_order_brw = sale_order_obj.browse(cr, user, context.get('active_id'))
        if sale_order_brw.amount_total < (cash + bank):
            raise osv.except_osv(_('Error!'),
                                 _('Total amount should not be greater than sales amount'))
        return True

    ##############################################################################
    # _get_journal_with_currency_with_tax()
    ##############################################################################
    def _get_journal_with_currency_with_tax(self, cr, user, inv,
                                            journal_id, context=None):
        if context is None:
            context = {}
        journal_proxy = self.pool.get('account.journal')
        journal = journal_proxy.browse(cr, user, journal_id)
        tax_id = self._get_tax(cr, user, journal, context)
        return journal_id, inv.currency_id.id, tax_id

    ##############################################################################
    # _get_tax()
    ##############################################################################
    def _get_tax(self, cr, user, journal, context=None):
        if context is None:
            context = {}
        account_id = journal.default_credit_account_id or \
                        journal.default_debit_account_id
        if account_id and account_id.tax_ids:
            tax_id = account_id.tax_ids[0].id
            return tax_id
        return False

    ##############################################################################
    # _get_period()
    ##############################################################################
    def _get_period(self, cr, user, context=None):
        if context is None:
            context = {}
        if context.get('period_id', False):
            return context.get('period_id')
        ctx = dict(context, account_period_prefer_normal=True)
        periods = self.pool.get('account.period').find(cr, user, context=ctx)
        return periods and periods[0] or False

    ##############################################################################
    # get_value_vouchar()
    ##############################################################################
    def get_value_vouchar(self, cr, user, inv_id, journal_id,
                          amount, context=None):
        if context is None:
            context = {}
        pos_session_id = self.pool.get('pos.session').search(cr, user,
                                                    [('user_id', '=', user)])
        if pos_session_id:
            pos_session_id = pos_session_id[0]
        inv_obj = self.pool.get('account.invoice')
        vou_obj = self.pool.get('account.voucher')
        journal_obj = self.pool.get('account.journal')
        inv = inv_obj.browse(cr, user, inv_id, context=context)
        journal_id, currency_id, tax_id = self._get_journal_with_currency_with_tax(cr, user, inv, journal_id, context)

        now_date = fields.date.context_today(self, cr, user, context=context)
        date = now_date

        partner_id = self.pool.get('res.partner')._find_accounting_partner(inv.partner_id).id
        context.update({ 'lang': 'en_US',
                         'default_amount': amount,
                         'close_after_process': True,
                         'tz': 'Europe/Brussels',
                         'user': user,
                         'payment_expected_currency': currency_id,
                         'active_model': 'account.invoice',
                         'invoice_id': inv.id,
                         'journal_type': 'sale',
                         'default_type': 'payment',
                         'invoice_type': inv.type,
                         'search_disable_custom_filters': True,
                         'default_reference': False,
                         'default_partner_id': partner_id,
                         'active_ids': [inv.id],
                         'type': inv.type in ('out_invoice', 'out_refund') and 'receipt' or 'payment',
                         'active_id': inv.id,
                         'retail_type': 'quick',
                       })
        
        line_cr_ids = vou_obj.get_value_voucher_line(cr, user, inv_id, currency_id, amount, context)['line_cr_ids']
        
        account_id = journal_obj.browse(cr, user, journal_id).default_debit_account_id.id
        return { 'active'         : True,
                 'period_id'      : self._get_period(cr, user),
                 'partner_id'     : partner_id,
                 'journal_id'     : journal_id,
                 'reference'      : inv.name,
                 'amount'         : amount,
                 'default_type'   : 'payment',
                 'invoice_id'     : inv.id,
                 'currency_id'    : currency_id,
                 'close_after_process' : True,
                 'type'           : inv.type in ('out_invoice', 'out_refund') and 'receipt' or 'payment',
                 'state'          : 'draft',
                 'pay_now'        : 'pay_now',
                 'pre_line'       : False, #probar con 1
                 'c_line_dr_ids'  : [],
                 'c_line_cr_ids'  : line_cr_ids,
                 'date'           : date,
                 'pos_session'    : pos_session_id,
                 'tax_id'         : tax_id,
                 'payment_option' : 'without_writeoff',
                 'comment'        : _('Write-Off'),
                 'payment_rate_currency_id' : currency_id,
                 'account_id'     : account_id or False,
                 'name'           : inv.name,
                 'retail_type'    : 'quick',
               }

    ##############################################################################
    # approve_sale_order_for_retail()
    ##############################################################################
    def approve_sale_order_for_retail(self, cr, user, ids, context=None):

        if context is None:
            context = {}

        sale_obj     = self.pool.get('sale.order')
        sale_ids     = context.get('active_ids', [])
        so_payment   = self.pool.get('sale.payment.so')
        invoice_obj  = self.pool.get('account.invoice')
        invoice_list = []

        # Right now we don't allow a mix of credit cards and cash payments, so if the user
        # has attempted it, throw an error.
        credit_card_used = False
        credit_card_journal = False
        num_payment_methods = 0
        for line in self.browse( cr, user, ids[0], context=context ).sale_payment_ids:
            if( line.amount ):
                if( line.journal_id.is_credit_card ):
                    credit_card_used = True
                    credit_card_journal = line.journal_id
                num_payment_methods += 1

        if( credit_card_used and num_payment_methods > 1 ):
            raise osv.except_osv( "Error", "No se puede pagar con una tarjeta de credito y otro metodo de pago" )

        # Unlink the sale.payment.so's associated with the sale order
        for sp_id in sale_obj.browse(cr, user, sale_ids[0]).sale_payments_ids:
            so_payment.unlink(cr, user, sp_id.id)

        self.save_sale_order_for_retail(cr, user, ids, context)

        # Confirm sale order
        wf_service = netsvc.LocalService('workflow')
        wf_service.trg_validate(user, 'sale.order', context.get('active_id'), 'order_confirm', cr)

        # Create the sales invoice, set it's retail_type as "quick"
        invoice_res  = sale_obj.manual_invoice(cr, user, sale_ids, context)
        invoice_list = [invoice_res.get('res_id')]
        self.pool.get('account.invoice').write(cr, user, invoice_list, {'retail_type':'quick'})

        # If a credit card was used, set the credit_card field on the invoice
        if( credit_card_used ):
            credit_card_ids = self.pool.get("ecua.credit.card").search(cr, user, [("diario","=",credit_card_journal.id)], context=context )
            if( len( credit_card_ids ) < 1 ):
                raise osv.except_osv( "Error", "No se encuentra una tarjeta de credito con el diario: "
                                      + credit_card_journal.name + ". Por favor agregue una tarjeta de credito con este diario." )
            self.pool.get('account.invoice').write(cr, user, invoice_list, {'credit_card': credit_card_ids[0] })

        avl_obj = self.pool.get('account.voucher.line')

        if invoice_list:

            wf_service.trg_validate(user, 'account.invoice', invoice_list[0], 'invoice_open', cr)

            for pay in self.browse(cr, user, ids[0]).sale_payment_ids:

                # Don't create payment if it's a credit card payment
                if pay.amount != 0 and not credit_card_used:

                    vouchar_vals = self.get_value_vouchar(cr, user, invoice_list[0], pay.journal_id.id, pay.amount, context)
                    voucher_id   = self.pool.get('account.voucher').create(cr, user, vouchar_vals, context=context)

                    for dr_lines in vouchar_vals.get('c_line_dr_ids'):
                        dr_lines.update({'voucher_id': voucher_id})
                        avl_obj.create(cr, user, dr_lines)

                    for cr_lines in vouchar_vals.get('c_line_cr_ids'):
                        cr_lines.update({'voucher_id': voucher_id})
                        avl_obj.create(cr, user, cr_lines)

                    wf_service.trg_validate(user, 'account.voucher', voucher_id, 'proforma_voucher', cr)
        
        return sale_obj.print_related_invoice(cr, user, sale_ids, context)