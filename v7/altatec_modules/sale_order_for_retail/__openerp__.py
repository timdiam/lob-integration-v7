# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Sale Order for Retail',
    'version': '1.0',
    'category': 'sale',
    'author': 'TRESCLOUD Cia Ltda',
    'description': 
    """
    Designed for retail industry, allows a sale order to be "quickly processed" by
    creating the sale order, payment, delivery, etc in one single click
    Also adds point of sale features to the sale order, like a POS Session, 
    closing cash box, etc.
    
    Disenado para la industria de venta al minorio, permite que una orden de venta
    sea "procesada rapidamente", es decir, que su factura, pagos, enregas, etc sean
    creadas en un solo click.
    Tambien aniade la funcionalidad de terminal de punto de venta a la orden
    de venta, incluyendo sesion de TPV, control de efectivo, etc.
    
    Authors: 
    David Romero
    Andres Calle
    TRESCLOUD Cia Ltda
    """,
    'website': 'http://www.trescloud.com',
    'depends': [ 'sale',
                 'account',
                 'account_voucher',
                 'point_of_sale',
                 'sale_pricelist_recalculation',
                 'authorization_for_pricelist',
                 'ecua_credit_card'
               ],
    'data': [ 'security/security.xml',
              'views/account_view.xml',
              'security/ir.model.access.csv',
              'views/sale_payment_view.xml',
              'wizard/wizard_sale_order_for_retail_view.xml',
              'wizard/pos_session_opening.xml',
              'views/sale_view.xml',
              'views/quick_sale_order.xml',
              'views/point_of_sale_view.xml',
            ],
    'installable': True,
    'auto_install': False,
}
