# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2014 TRESCLOUD Cia Ltda (trescloud.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp import netsvc

class pos_session(osv.osv):
    _inherit = 'pos.session'
    
    def open_cb(self, cr, uid, ids, context=None):
        '''
        Redefinimos la fucncion, abre la sesion pero ya no llama a la interafaz del TPV
        TODO: Hacer que la modificacion afecte solo si el context indica que es de quick invoice
        '''
        if context is None:
            context = dict()

        if isinstance(ids, (int, long)):
            ids = [ids]

        this_record = self.browse(cr, uid, ids[0], context=context)
        this_record._workflow_signal('open')

        context.update(active_id=this_record.id)
        return True

#        return {
#            'type' : 'ir.actions.client',
#            'name' : _('Start Point Of Sale'),
#            'tag' : 'pos.ui',
#            'context' : context,
#        }
        
    def open_frontend_cb(self, cr, uid, ids, context=None):
        '''
        Redefinimos la fucncion, abre la sesion pero ya no llama a la interafaz del TPV
        TODO: Hacer que la modificacion afecte solo si el context indica que es de quick invoice
        '''
        if not context:
            context = {}
        if not ids:
            return {}
        for session in self.browse(cr, uid, ids, context=context):
            if session.user_id.id != uid:
                raise osv.except_osv(
                        _('Error!'),
                        _("You cannot use the session of another users. This session is owned by %s. Please first close this one to use this point of sale." % session.user_id.name))
        context.update({'active_id': ids[0]})
        #Mandamos en el contexto, retail_type que la venta continue como quick
        context.update({'retail_type': 'quick'})
        #Devolvemos el metodo que ya nos abre las vistas de en otro boton para no reescribir el mismo codigo
        #por esto solo aumentamos la variable al contexto
        pos_session_opening_obj=self.pool.get('pos.session.opening');        
        return pos_session_opening_obj.open_ui(cr, uid, ids, context)

    def create(self, cr, uid, values, context=None):
        context = context or {}
        config_id = values.get('config_id', False) or context.get('default_config_id', False)
        if not config_id:
            raise osv.except_osv( _('Error!'),
                _("You should assign a Point of Sale to your session."))

        # journal_id is not required on the pos_config because it does not
        # exists at the installation. If nothing is configured at the
        # installation we do the minimal configuration. Impossible to do in
        # the .xml files as the CoA is not yet installed.
        jobj = self.pool.get('pos.config')
        pos_config = jobj.browse(cr, uid, config_id, context=context)
        context.update({'company_id': pos_config.shop_id.company_id.id})
        if not pos_config.journal_id:
            jid = jobj.default_get(cr, uid, ['journal_id'], context=context)['journal_id']
            if jid:
                jobj.write(cr, uid, [pos_config.id], {'journal_id': jid}, context=context)
            else:
                raise osv.except_osv( _('error!'),
                    _("Unable to open the session. You have to assign a sale journal to your point of sale."))

        # define some cash journal if no payment method exists
        if not pos_config.journal_ids:
            journal_proxy = self.pool.get('account.journal')
            cashids = journal_proxy.search(cr, uid, [('journal_user', '=', True), ('type','=','cash')], context=context)
            if not cashids:
                cashids = journal_proxy.search(cr, uid, [('type', '=', 'cash')], context=context)
                if not cashids:
                    cashids = journal_proxy.search(cr, uid, [('journal_user','=',True)], context=context)

            jobj.write(cr, uid, [pos_config.id], {'journal_ids': [(6,0, cashids)]})


        pos_config = jobj.browse(cr, uid, config_id, context=context)
        bank_statement_ids = []
        voucher_ids = []
        for journal in pos_config.journal_ids:
            bank_values = {
                'journal_id' : journal.id,
                'user_id' : uid,
                'company_id' : pos_config.shop_id.company_id.id
            }
            statement_id = self.pool.get('account.bank.statement').create(cr, uid, bank_values, context=context)
            voucher_id = self.pool.get('voucher.control').create(cr, uid, bank_values, context=context)
            bank_statement_ids.append(statement_id)
            voucher_ids.append(voucher_id)

        values.update({
            'name' : pos_config.sequence_id._next(),
            'statement_ids' : [(6, 0, bank_statement_ids)],
            'voucher_ids' : [(6, 0, voucher_ids)],
            'config_id': config_id
        })
        return super(pos_session, self).create(cr, uid, values, context=context)

    _columns = {    
        'voucher_ids' : fields.one2many('voucher.control', 'voucher_session_id', 'Control Voucher', readonly=True),
    }

pos_session()

class pos_config(osv.osv):
    _inherit = 'pos.config'

    _columns = {
        'invoice_sequence_id' : fields.many2one('ir.sequence', 'Invoice IDs Sequence', readonly=True,
            help="This sequence is automatically created by Odoo but you can change it "\
                "to customize the generated invoice numbers of your orders."),
    }
    
    def copy(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        d = {
            'invoice_sequence_id' : False,
        }
        d.update(default)
        return super(pos_config, self).copy(cr, uid, id, d, context=context)
    
    def create(self, cr, uid, values, context=None):
        proxy = self.pool.get('ir.sequence')
        sequence_values = dict(
            name='Invoice Sequences for %s' % values['name'],
            padding=9,
            prefix="%s"  % values['name'],
        )
        invoice_sequence_id = proxy.create(cr, uid, sequence_values, context=context)
        values['invoice_sequence_id'] = invoice_sequence_id
        return super(pos_config, self).create(cr, uid, values, context=context)
    
    def unlink(self, cr, uid, ids, context=None):
        for obj in self.browse(cr, uid, ids, context=context):
            if obj.invoice_sequence_id:
                obj.invoice_sequence_id.unlink()
        return super(pos_config, self).unlink(cr, uid, ids, context=context)
    
pos_config()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
