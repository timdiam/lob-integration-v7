import time
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

class account_voucher(osv.osv):
    _inherit = 'account.voucher'
    _name = 'account.voucher'

    def get_value_voucher_line(self, cr, user, inv_id, currency_id, amount, context=None):
        '''
        Retorna una linea unica de pago asociada a la factura provista
        #ADVERTENCIA: Funciona solo para pagos desde facturas de venta
        #TODO: Extenderla para incluir facturas de compra, y otros documentos
        '''
        
        inv_obj = self.pool.get('account.invoice')
        move_obj = self.pool.get('account.move.line')
        inv = inv_obj.browse(cr, user, inv_id, context=context)
        
        #obtenemos las lineas de movimientos contable de la factura por pagar ordenados por fecha de vencimiento
        #TODO: Mejorar la eficiencia... buscar todos los miles de asientos por invice_id no es eficiente
        move_lines_to_pay_ids = move_obj.search(cr, user, 
                                                [('invoice', '=', inv.id),('debit', '>', 0)], 
                                                order = 'date asc')
        if not move_lines_to_pay_ids:
            raise osv.except_osv(_('Error de Pago!'),_("No se ha encontrado el asiento contable a pagar") )
        move_lines_to_pay = move_obj.browse(cr, user, move_lines_to_pay_ids, context=context)
        
        #contruimos la linea a pagar
        line_cr_ids = []
        writeoff_amount = amount #valor residual, en escenario ideal es 0.00        
        for move_line in move_lines_to_pay:
            #calculamos el monto a pagar en cada linea
            amount_to_pay = min(writeoff_amount, move_line.amount_residual) or 0.0
            #TODO: Agregar redondeo de openerp
            writeoff_amount = writeoff_amount - amount_to_pay
            line_cr_ids.append({
                            'currency_id': currency_id, 
                            'amount': amount_to_pay, 
                            'date_due': move_line.date, 
                            'name': inv.name, 
                            'date_original': move_line.date_created, 
                            'move_line_id': move_line.id, 
                            'amount_unreconciled': move_line.amount_residual, 
                            'type': 'cr', 
                            'amount_original': move_line.debit, 
                            'account_id': move_line.account_id.id,
                            })
        return {'line_cr_ids': line_cr_ids, 'line_dr_ids': [], 'writeoff_amount': writeoff_amount} 
    
    def recompute_voucher_lines(self, cr, uid, ids, partner_id, journal_id, price, currency_id, ttype, date, context=None):
        """
        Returns a dict that contains new values and context

        @param partner_id: latest value from user input for field partner_id
        @param args: other arguments
        @param context: context arguments, like lang, time zone

        @return: Returns a dict which contains new values, and context        
        """
        
        #Para ventas al retail se construye la linea del asiento en funcion del asiento contable asociado a la factura
        #TODO: Hacer la funcion escalable para otros tipos de factura 
        if context.get("invoice_id",False) and (context.get("invoice_type",False) == 'out_invoice'):
            voucher_lines = self.get_value_voucher_line(cr, uid, 
                                                      context["invoice_id"], 
                                                      currency_id, 
                                                      price, 
                                                      context)
            res = {'value': {'pre_line': 1,
                             'writeoff_amount': voucher_lines["writeoff_amount"],
                             'line_cr_ids': voucher_lines["line_cr_ids"],
                             'line_dr_ids': voucher_lines["line_dr_ids"]}}
            return res
        
        res = super(account_voucher, self).recompute_voucher_lines(cr, uid, ids, partner_id, journal_id, price, currency_id, ttype, date, context)
        return res


    def button_print_pay_voucher(self, cr, uid, ids, context=None):
        #self.print_document_type(cr, uid, ids, context=context)
        self.button_proforma_voucher(cr, uid, ids, context=context)

        return True;

    def _default_session(self, cr, uid, context=None):
        '''
        Busca una sesion abierta de TPV y la asigna
        Reutilizamos los metodos de pos order
        Solo si el tipo de documento es retail
        '''
        if context is None:
            context = {}

        session = False

        if 'retail_type' in context:
            if context['retail_type'] == 'quick':
                pos_order_obj = self.pool.get('pos.order')
                session = pos_order_obj._default_session(cr, uid, context)
        return session
    
    def _get_retail_type(self, cr, uid, context=None):
        '''Tipo por defecto, puede ser quick o regular, 
        si el context no tiene el valor entonces es regular.
        '''
        if context is None:
            context = {}
        return context.get('retail_type', 'regular')
    
    _columns = {
               'session_id' : fields.many2one('pos.session', 'Session', 
                                        #required=True,
                                        select=1,
                                        domain="[('state', '=', 'opened')]",
                                        #states={'draft' : [('readonly', False)]},
                                        readonly=True),
                'retail_type': fields.selection([
                    ('quick','Retail Payment'),
                    ('regular','Regular Payment'),
                    ],'Type', readonly=True, select=True, change_default=True, track_visibility='always'),
               'voucher_line_id': fields.many2one('voucher.control', 'Payments', states={'draft': [('readonly', False)]}, readonly=True),
               
                }

    _defaults = {
        'retail_type': _get_retail_type,
        }
    
    def proforma_voucher(self, cr, uid, ids, context=None):
        '''
        1. Asigna una sesion TPV actual
        2. Valida que la sesion de la factura este activa y corresponda al usuario
        3. Crear entrada en  voucher.control.line
        '''
        if context is None:
            context = {}
            
        for voucher in self.browse(cr, uid, ids, context):
            if voucher.retail_type == 'quick':
                context.update({'retail_type':'quick'}) #necesario para las funciones default
                current_session_id = self._default_session(cr, uid, context)
                voucher.write({'session_id': current_session_id})
                voucher = self.browse(cr, uid, voucher.id, context) #actualizmos el objeto para tener el session_id actualizado
                if not voucher.session_id.id:
                    raise osv.except_osv(_('Error!'), _('A quick payment requires a POS Session!, please start a POS Session then create a new sale order'))
                if voucher.session_id.state != 'opened':
                    raise osv.except_osv(_('Error!'), _('The POS Session state should be In Progress, please start a POS Session then create a new sale order'))
                if voucher.session_id.user_id.id != uid:
                    raise osv.except_osv(_('Error!'), _('You are not the user of this POS Session, please start a POS Session then create a new sale order'))
                
            res = super(account_voucher, self).proforma_voucher(cr, uid, ids, context)
            #self.check(cr, uid, ids, context=context)
        return res

    def check(self, cr, uid, ids, context=None):
        """Check the order:
        if the order is not paid: continue payment,
        if the order is paid print ticket.
        """
        context = context or {}
        voucher_obj = self.pool.get('account.voucher')
        obj_partner = self.pool.get('res.partner')
        active_id = context and context.get('active_id', False)

        voucher = voucher_obj.browse(cr, uid, ids, context=context)
        print voucher[0]
        amount = voucher[0].amount
        data = self.read(cr, uid, ids, context=context)[0]
        # this is probably a problem of osv_memory as it's not compatible with normal OSV's
        data['journal'] = data['journal_id'][0]

        if amount != 0.0:
            voucher_obj.add_payment(cr, uid, ids, data, context=context)

        #if order_obj.test_paid(cr, uid, [active_id]):
            #wf_service = netsvc.LocalService("workflow")
            #wf_service.trg_validate(uid, 'pos.order', active_id, 'paid', cr)
         #   return {'type' : 'ir.actions.act_window_close' }
         ##self.print_report(cr, uid, ids, context=context)        #return self.launch_payment(cr, uid, ids, context=context)


    def add_payment(self, cr, uid, voucher_id, data, context=None):
        """Create a new payment for the voucher"""
        if not context:
            context = {}
        voucher_line_obj = self.pool.get('voucher.control.line')
        property_obj = self.pool.get('ir.property')
        voucher = self.browse(cr, uid, voucher_id, context=context)[0]
        args = {
            'amount': data['amount'],
            'date': data.get('payment_date', time.strftime('%Y-%m-%d')),
            'name': voucher.name + ': ' + (data.get('payment_name', '') or ''),
        }
        ## mone_line_ids
        #DR codigo para agregar el tipo e cuenta transitoria        #args['account_id'] = (voucher.journal_id.internal_account_id.id) or False
        #args['account_id'] = (voucher.journal_id.internal_account_id.id) or False
        account_def = property_obj.get(cr, uid, 'property_account_receivable', 'res.partner', context=context)
        args['account_id'] = (voucher.partner_id and voucher.partner_id.property_account_receivable \
                             and voucher.partner_id.property_account_receivable.id) or (account_def and account_def.id) or False
        args['partner_id'] = voucher.partner_id and voucher.partner_id.id or None

        if not args['account_id']:
            if not args['partner_id']:
                msg = _('There is no receivable account defined to make payment.')
            else:
                msg = _('There is no receivable account defined to make payment for the partner: "%s" (id:%d).') % (voucher.partner_id.name, voucher.partner_id.id,)
            raise osv.except_osv(_('Configuration Error!'), msg)

        context.pop('session_id', False)

        journal_id = data.get('journal', False)
        voucher_id = data.get('voucher_id', False)
        assert journal_id or voucher_id, "No voucher_id or journal_id passed to the method!"

        for voucher in voucher.session_id.voucher_ids:
            if voucher.id == voucher_id:
                journal_id = voucher.journal_id.id
                break
            elif voucher.journal_id.id == journal_id:
                voucher_id = voucher.id
                break

        if not voucher_id:
            raise osv.except_osv(_('Error!'), _('You have to open at least one cashbox.'))

        args.update({
            'voucher_id' : voucher_id,
            'voucher_voucher_id' : voucher_id[0],
            'journal_id' : journal_id,
            'type' : 'customer',
            'ref' : voucher.session_id.name,
        })

        voucher_line_obj.create(cr, uid, args, context=context)

        return voucher_id


    def copy(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        
        #La sesion no debe copiarse, ej. si duplico una venta de hace un mes.       
        default.update({
            'session_id': False
        })
        return super(account_voucher, self).copy(cr, uid, order_id, default, context)
               
account_voucher()