# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, osv


class sale_payment(osv.osv_memory):
    _name = "sale.payment"
    _columns = {
        'sale_order_for_retail_id': fields.many2one('wizard.sale.order.for.retail', 'Quick Invoice'),
        'journal_id': fields.many2one('account.journal', 'Journal',
                                      domain=['|', ('type', '=', 'cash'),
                                                   ('type', '=', 'bank')], required=True),
        'amount': fields.float('Amount', required=True),
        'is_done': fields.boolean('Is Done'),
    }
sale_payment()


class sale_payment_so(osv.osv):
    _name = "sale.payment.so"
    _columns = {
        'so_id': fields.many2one('sale.order', 'Sale Order'),
        'journal_id': fields.many2one('account.journal', 'Journal',
                                      domain=['|', ('type', '=', 'cash'),
                                                   ('type', '=', 'bank')]),
        'amount': fields.float('Amount'),
    }
sale_payment_so()
