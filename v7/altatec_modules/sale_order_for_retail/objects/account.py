# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013-Present Acespritech Solutions Pvt. Ltd. (<http://acespritech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
from openerp.osv import fields, osv
from openerp import netsvc
from tools.translate import _
from datetime import datetime
import pytz

class account_invoice(osv.osv):
    _inherit = "account.invoice"
    
    def _default_session(self, cr, uid, context=None):
        '''
        Busca una sesion abierta de TPV y la asigna
        Reutilizamos los metodos de pos order
        Solo si el tipo de documento es retail
        '''
        if context is None:
            context = {}

        session = False
        if 'retail_type' in context:
            if context['retail_type'] == 'quick':
                pos_order_obj = self.pool.get('pos.order')
                session = pos_order_obj._default_session(cr, uid, context)
        return session
    
    def _get_retail_type(self, cr, uid, context=None):
        '''Tipo por defecto, puede ser quick o regular, 
        si el context no tiene el valor entonces es regular.
        '''
        if context is None:
            context = {}
        return context.get('retail_type', 'regular')
    
    _columns = {
               'session_id' : fields.many2one('pos.session', 'Session', 
                                        #required=True,
                                        select=1,
                                        domain="[('state', '=', 'opened')]",
                                        #states={'draft' : [('readonly', False)]},
                                        readonly=True),
                'retail_type': fields.selection([
                    ('quick','Retail Invoice'),
                    ('regular','Regular Invoice'),
                    ],'Type', readonly=True, select=True, change_default=True, track_visibility='always'),
                }
    
    _defaults = {
        'retail_type': _get_retail_type,
        }

    #def invoice_validate(self, cr, uid, ids, context=None): #TODO: Cambiar el metodo por la funcion invoice_validate, pero requiere cambiar el framework ecua de action_number a invoice_validate
    def action_number(self, cr, uid, ids, context=None):
        '''
        1. Asigna una sesion TPV actual
        2. Valida que la sesion de la factura este activa y corresponda al usuario
        3. Asigna un numero de factura al momento de aprobarla
        '''
        if context is None:
            context = {}
        for invoice in self.browse(cr, uid, ids, context):
            if invoice.retail_type == 'quick':
                # 1. Asigna una sesion TPV actual
                context.update({'retail_type':'quick'}) #necesario para las funciones default
                current_session_id = self._default_session(cr, uid, context)
                invoice.write({'session_id': current_session_id})
                invoice = self.browse(cr, uid, invoice.id, context) #actualizmos el objeto para tener el session_id actualizado
                
                # 2. Valida que la sesion de la factura este activa y corresponda al usuario
                if not invoice.session_id.id:
                    raise osv.except_osv(_('Error!'), _('A quick invoice requires a POS Session!, please start a POS Session then create a new invoice'))
                if invoice.session_id.state != 'opened':
                    raise osv.except_osv(_('Error!'), _('The POS Session state should be In Progress, please start a POS Session then create a new invoice'))
                if invoice.session_id.user_id.id != uid:
                    raise osv.except_osv(_('Error!'), _('You are not the user of this POS Session, please start a POS Session then create a new invoice'))

                #CHECKS ADDED BY ALTATEC.  We wanted to make sure printer point has an invoice sequence defined, and that it is the same
                #as the one defined inside the cofig section of the point of sale.
                session_printer_config = invoice.session_id.config_id.sri_printer_point_id
                user_printer = self.pool.get('res.users').browse(cr,uid,uid,context=context).printer_id
                
                if session_printer_config.id != user_printer.id:
                    raise osv.except_osv(_('Error!'), _('El punto de impresion de usuario no es lo mismo que la session de terminal punto de venta'))

                if session_printer_config.invoice_sequence_id:
                    context.update({'force_printer_id':session_printer_config.id})
                else:
                    raise osv.except_osv(_('Error!'), _('No hay ningun secuencia de factura definda en el punto de impresion'))
                    
                # 3. Asigna un numero de factura al momento de aprobarla
#                 if invoice.type in ['out_invoice']:
#                     invoice_sequence_id = invoice.session_id.config_id.invoice_sequence_id.id
#                     number = self.pool.get('ir.sequence').next_by_id(cr, uid, invoice_sequence_id, context)
#                     invoice.write({'internal_number': number})
#                     invoice.write({'number':number})               
                    #TODO: Implementar secuenciales para los otros tipos de documentos
                    #TODO: Migrar la secuencia del POS CONFIG al PRINTER POINT
        #TODO TEST WHY WE ARE NOT THROWING ERROR HERE WHEN PRINTER POINT IS LATER SWITCHED AFTER INVOICE GENREATED
        return super(account_invoice, self).action_number(cr, uid, ids, context)
    
    def _suggested_internal_number(self, cr, uid, printer_id, type, company_id=None, context=None):
        '''
        Cambia el numero de factura por defecto por un texto explicativo
        '''
        if context is None:
            context = {}
        if 'retail_type' in context:
            if context['retail_type'] == 'quick':
                return 'Numeracion Automatica'
        return super(account_invoice, self)._suggested_internal_number(cr, uid, printer_id, type, company_id, context)
        
    
    def copy(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        
        default.update({
            'session_id': False, #La sesion no debe copiarse, ej. si duplico una venta de hace un mes.
            #'internal_number': 'Numeracion Automatica', el numero de factura debio ser manejado en otros modulos
        })
        return super(account_invoice, self).copy(cr, uid, id, default, context)
    
account_invoice()


class account_voucher(osv.osv):
    _inherit = "account.voucher"
    
    def _default_session(self, cr, uid, context=None):
        '''
        Busca una sesion abierta de TPV y la asigna
        Reutilizamos los metodos de pos order
        Solo si el tipo de documento es retail
        '''
        if context is None:
            context = {}

        session = False

        if 'retail_type' in context:
            if context['retail_type'] == 'quick':
                pos_order_obj = self.pool.get('pos.order')
                session = pos_order_obj._default_session(cr, uid, context)
        return session
    
    def _get_retail_type(self, cr, uid, context=None):
        '''Tipo por defecto, puede ser quick o regular, 
        si el context no tiene el valor entonces es regular.
        '''
        if context is None:
            context = {}
        return context.get('retail_type', 'regular')
    
    _columns = {
               'session_id' : fields.many2one('pos.session', 'Session', 
                                        #required=True,
                                        select=1,
                                        domain="[('state', '=', 'opened')]",
                                        #states={'draft' : [('readonly', False)]},
                                        readonly=True),
                'retail_type': fields.selection([
                    ('quick','Retail Payment'),
                    ('regular','Regular Payment'),
                    ],'Type', readonly=True, select=True, change_default=True, track_visibility='always'),
               'voucher_line_id': fields.many2one('voucher.control', 'Payments', states={'draft': [('readonly', False)]}, readonly=True),
               
                }

    _defaults = {
        'retail_type': _get_retail_type,
        }
    
    def proforma_voucher(self, cr, uid, ids, context=None):
        '''
        1. Asigna una sesion TPV actual
        2. Valida que la sesion de la factura este activa y corresponda al usuario
        3. Crear entrada en  voucher.control.line
        '''
        if context is None:
            context = {}
            
        for voucher in self.browse(cr, uid, ids, context):
            if voucher.retail_type == 'quick':
                context.update({'retail_type':'quick'}) #necesario para las funciones default
                current_session_id = self._default_session(cr, uid, context)
                voucher.write({'session_id': current_session_id})
                voucher = self.browse(cr, uid, voucher.id, context) #actualizmos el objeto para tener el session_id actualizado
                if not voucher.session_id.id:
                    raise osv.except_osv(_('Error!'), _('A quick payment requires a POS Session!, please start a POS Session then create a new sale order'))
                if voucher.session_id.state != 'opened':
                    raise osv.except_osv(_('Error!'), _('The POS Session state should be In Progress, please start a POS Session then create a new sale order'))
                if voucher.session_id.user_id.id != uid:
                    raise osv.except_osv(_('Error!'), _('You are not the user of this POS Session, please start a POS Session then create a new sale order'))
                
            res = super(account_voucher, self).proforma_voucher(cr, uid, ids, context)
            #self.check(cr, uid, ids, context=context)
        return res

    def check(self, cr, uid, ids, context=None):
        """Check the order:
        if the order is not paid: continue payment,
        if the order is paid print ticket.
        """
        context = context or {}
        voucher_obj = self.pool.get('account.voucher')
        obj_partner = self.pool.get('res.partner')
        active_id = context and context.get('active_id', False)

        voucher = voucher_obj.browse(cr, uid, ids, context=context)
        print voucher[0]
        amount = voucher[0].amount
        data = self.read(cr, uid, ids, context=context)[0]
        # this is probably a problem of osv_memory as it's not compatible with normal OSV's
        data['journal'] = data['journal_id'][0]

        if amount != 0.0:
            voucher_obj.add_payment(cr, uid, ids, data, context=context)

        #if order_obj.test_paid(cr, uid, [active_id]):
            #wf_service = netsvc.LocalService("workflow")
            #wf_service.trg_validate(uid, 'pos.order', active_id, 'paid', cr)
         #   return {'type' : 'ir.actions.act_window_close' }
         ##self.print_report(cr, uid, ids, context=context)        #return self.launch_payment(cr, uid, ids, context=context)


    def add_payment(self, cr, uid, voucher_id, data, context=None):
        """Create a new payment for the voucher"""
        if not context:
            context = {}
        voucher_line_obj = self.pool.get('voucher.control.line')
        property_obj = self.pool.get('ir.property')
        voucher = self.browse(cr, uid, voucher_id, context=context)[0]

        #now = datetime.now(pytz.timezone("America/Guayaquil"))
        now_date = fields.date.context_today(self, cr,uid,context=context)
        args = {
            'amount': data['amount'],
            'date': data.get('payment_date', now_date),
            'name': voucher.name + ': ' + (data.get('payment_name', '') or ''),
        }
        ## mone_line_ids
        #DR codigo para agregar el tipo e cuenta transitoria        #args['account_id'] = (voucher.journal_id.internal_account_id.id) or False
        #args['account_id'] = (voucher.journal_id.internal_account_id.id) or False
        account_def = property_obj.get(cr, uid, 'property_account_receivable', 'res.partner', context=context)
        args['account_id'] = (voucher.partner_id and voucher.partner_id.property_account_receivable \
                             and voucher.partner_id.property_account_receivable.id) or (account_def and account_def.id) or False
        args['partner_id'] = voucher.partner_id and voucher.partner_id.id or None

        if not args['account_id']:
            if not args['partner_id']:
                msg = _('There is no receivable account defined to make payment.')
            else:
                msg = _('There is no receivable account defined to make payment for the partner: "%s" (id:%d).') % (voucher.partner_id.name, voucher.partner_id.id,)
            raise osv.except_osv(_('Configuration Error!'), msg)

        context.pop('session_id', False)

        journal_id = data.get('journal', False)
        voucher_id = data.get('voucher_id', False)
        assert journal_id or voucher_id, "No voucher_id or journal_id passed to the method!"

        for voucher in voucher.session_id.voucher_ids:
            if voucher.id == voucher_id:
                journal_id = voucher.journal_id.id
                break
            elif voucher.journal_id.id == journal_id:
                voucher_id = voucher.id
                break

        if not voucher_id:
            raise osv.except_osv(_('Error!'), _('You have to open at least one cashbox.'))

        args.update({
            'voucher_id' : voucher_id,
            'voucher_voucher_id' : voucher_id[0],
            'journal_id' : journal_id,
            'type' : 'customer',
            'ref' : voucher.session_id.name,
        })

        voucher_line_obj.create(cr, uid, args, context=context)

        return voucher_id


    def copy(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        
        #La sesion no debe copiarse, ej. si duplico una venta de hace un mes.       
        default.update({
            'session_id': False
        })
        return super(account_voucher, self).copy(cr, uid, order_id, default, context)

account_voucher()
