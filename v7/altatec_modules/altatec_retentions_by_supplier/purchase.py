from openerp.osv import fields, osv, orm

class purchase_order_line(orm.Model):

    _inherit = 'purchase.order.line'

    def onchange_product_id( self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
                             partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
                             name=False, price_unit=False, context=None
                           ):

        res_prod = super(purchase_order_line, self).product_id_change( cr, uid, ids, pricelist_id, product_id, qty, uom_id,
                                                                       partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
                                                                       name=False, price_unit=False, context=None
                                                                     )

        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            if len(partner.retention_ids):
                ret_list = []
                for ret in partner.retention_ids:
                    ret_list.append(ret.id)
                res_prod['value']['ecua_shadow_retentions'] = ret_list

        return res_prod