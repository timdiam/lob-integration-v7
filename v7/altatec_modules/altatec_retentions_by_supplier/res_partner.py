from openerp.osv import fields, osv, orm

class res_partner(orm.Model):

    _inherit = "res.partner"

    _columns = { 'retention_ids' : fields.many2many( 'account.tax', 'res_partner_retention_rel', 'partner_id', 'tax_id', 'Retenciones'),
               }