from openerp.osv import fields, osv, orm

class account_invoice_line(orm.Model):

    _inherit = 'account.invoice.line'

    def product_id_change(self, cr, uid, ids, product, uom_id, qty=0, name='', type='out_invoice', partner_id=False, fposition_id=False, price_unit=False, currency_id=False, context={}, company_id=None):

        res_prod = super(account_invoice_line, self).product_id_change(cr, uid, ids, product, uom_id, qty, name, type, partner_id, fposition_id, price_unit, currency_id=currency_id, context=context, company_id=company_id)

        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)

            if type and type == 'in_invoice' and len(partner.retention_ids):
                ret_list = []
                for ret in partner.retention_ids:
                    ret_list.append(ret.id)
                res_prod['value']['ecua_shadow_retentions'] = ret_list

        return res_prod