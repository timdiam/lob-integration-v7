{
    'name': 'Altatec Retentions by Supplier',
    'version': '1.0',
    'description': """
    This module defines retentions on res.partner, and automatically chooses the retention on a sale order
    product based on the supplier if there is one defined on the supplier. Otherwise, it gets the retention
    from the product as usual
    """,
    'author': 'Dan Haggerty',
    'website': 'www.altatec.ec',
    "depends" : [ 'purchase',
                  'product',
                  'ecua_split_tax_and_retentions',
                ],
    "data" : [ 'res_partner.xml',
             ],
    "installable": True,
    "auto_install": False
}
