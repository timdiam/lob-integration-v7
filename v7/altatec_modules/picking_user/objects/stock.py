# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2014 TRESCLOUD Cia Ltda (trescloud.com) David Romero
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, osv
from openerp import netsvc
from tools.translate import _

#------------------------------------------------------------------------------ 
# Se tuvo que definir las columnas en la clase base y en las clases heredadas, es una clase de herencia 
# que se tiene que realizar de esta manera, para que funcione las optimizaciones, este comporatmiento fue
# copiado de una funcionalidad del codigo base.
# Se redefinio metodos genericos para mantener el control de las tranferencias, historicos de 
# Usuario que aprobo
# Usuario que transfirio
# Usuario que cancelo
# Para guias de salida fue pensado este modulo
#------------------------------------------------------------------------------ 
class stock_picking_in(osv.osv):
    _inherit = "stock.picking.in"
    _columns = {
                'user_id': fields.many2one('res.users', 'User who approved'),
                'transfer_user_id': fields.many2one('res.users', 'User who transfer'),
                'cancel_user_id': fields.many2one('res.users', 'User who cancel'),
                }
    _defaults = {  
        'user_id': lambda s, c, u, ctx: u,
        'transfer_user_id': lambda s, c, u, ctx: u,
        'cancel_user_id': lambda s, c, u, ctx: u,  
        }
    
    def draft_force_assign(self, cr, uid, ids, *args):
        """ Confirms picking directly from draft state.
        @return: True
        """
        res = super(stock_picking_in,self).draft_force_assign(cr, uid, ids, *args)
        for pick in self.browse(cr, uid, ids):
            pick.write({'user_id': uid})
        return res
    
    def copy(self, cr, uid, id, default=None, context=None):
        default = default or {}
        default.update({'user_id' : False})
        default.update({'transfer_user_id' : False})
        default.update({'cancel_user_id' : False})
        return super(stock_picking_in, self).copy(cr, uid, id, default, context)
    
stock_picking_in()

class stock_picking_out(osv.osv):
    _inherit = "stock.picking.out"
    _columns = {
                'user_id': fields.many2one('res.users', 'User who approved'),
                'transfer_user_id': fields.many2one('res.users', 'User who transfer'),
                'cancel_user_id': fields.many2one('res.users', 'User who cancel'),
                }
    _defaults = {  
        'user_id': lambda s, c, u, ctx: u,
        'transfer_user_id': lambda s, c, u, ctx: u,
        'cancel_user_id': lambda s, c, u, ctx: u,  
        }
    
    def draft_force_assign(self, cr, uid, ids, *args):
        """ Confirms picking directly from draft state.
        @return: True
        """
        res = super(stock_picking_out,self).draft_force_assign(cr, uid, ids, *args)
        for pick in self.browse(cr, uid, ids):
            pick.write({'user_id': uid})
        return res
    
    def copy(self, cr, uid, id, default=None, context=None):
        default = default or {}
        default.update({'user_id' : False})
        default.update({'transfer_user_id' : False})
        default.update({'cancel_user_id' : False})
        return super(stock_picking_out, self).copy(cr, uid, id, default, context)
    
stock_picking_out()

class stock_picking(osv.osv):
    _inherit = "stock.picking"
    _columns = {
                'user_id': fields.many2one('res.users', 'User who approved'),
                'transfer_user_id': fields.many2one('res.users', 'User who transfer'),
                'cancel_user_id': fields.many2one('res.users', 'User who cancel'),
                }
    _defaults = {  
        'user_id': lambda s, c, u, ctx: u,
        'transfer_user_id': lambda s, c, u, ctx: u,
        'cancel_user_id': lambda s, c, u, ctx: u,    
        }
    
    def draft_force_assign(self, cr, uid, ids, *args):
        """ Confirms picking directly from draft state.
        @return: True
        """
        res = super(stock_picking,self).draft_force_assign(cr, uid, ids, *args)
        for pick in self.browse(cr, uid, ids):
            pick.write({'user_id': uid})
        return res
    
    def do_partial(self, cr, uid, ids, partial_datas, context=None):
        res = super(stock_picking,self).do_partial(cr, uid, ids, partial_datas, context=None)
        for pick in self.browse(cr, uid, ids, context=context):
            pick.write({'transfer_user_id': uid})            
        return res
    
    def action_cancel(self, cr, uid, ids, context=None):
        """ Cancels the moves and if all moves are cancelled it cancels the picking.
        @return: True
        """
        res = super(stock_picking,self).action_cancel(cr, uid, ids, context=None)
        for pick in self.browse(cr, uid, ids, context=context):
            pick.write({'cancel_user_id': uid})            
        return res
    
    def copy(self, cr, uid, id, default=None, context=None):
        default = default or {}
        default.update({'user_id' : False})
        default.update({'transfer_user_id' : False})
        default.update({'cancel_user_id' : False})
        #---------------------------------------- seq_obj_name = 'stock.picking'
        #--------- numbe=self.pool.get('ir.sequence').get(cr, uid, seq_obj_name)
        #default.update({'name': numbe})
        return super(stock_picking, self).copy(cr, uid, id, default, context)

stock_picking()
