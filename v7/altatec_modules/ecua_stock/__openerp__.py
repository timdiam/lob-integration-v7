# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2014 TRESCLOUD Cia Ltda (trescloud.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Ecuador Stock Control',
    'version': '1.0',
    'category': 'sale',
    'depends': ['base','stock',"web_m2x_options",
                'stock_delivery_date', 'picking_user',
                'purchase',
                #'stock_inventory',
                'stock_product_moves',
                'ecua_invoice',
                'trescloud_fleet',
                'stock_analytic',
                'product_purchase_history',
                'stock_picking_invoice_link',],
    'author': 'TRESCLOUD Cia Ltda',
    'description': 
    """
    Resumen del Modulo:
        - Agrega campo conductor para guias de remosion
        - Agrega nuevo modulo para el conductor
        - Muestra los movimientos contables asociados a los movimientos de inventario en stock.picking.in y out
        - Muestra los campos de direcciones origen/destino con la que la transaccion fue realizada desde ventas. 
    Funcionalidades:
        - Se integra sales con stock.picking
        - Filtro de devoluciones
    Authors: 
    Santiago Orozco,
    Romero David,
    Andres Calle,
    TRESCLOUD Cia Ltda
    """,
    'website': 'http://www.trescloud.com',
    'data': [
             'views/stock_picking_view.xml',
             'views/res_partner_view.xml',
             'views/stock_view.xml',
             'views/product_view.xml',
             'security/security.xml',
             'security/ir.model.access.csv',
    ],
    'installable': True,
    'auto_install': False,
}
