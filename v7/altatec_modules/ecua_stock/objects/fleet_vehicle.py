# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2014 TRESCLOUD Cia Ltda (trescloud.com), Romero David
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv
from openerp.osv import fields
from openerp.tools.translate import _
from openerp.tools.misc import ustr
import openerp.addons.decimal_precision as dp


"""
Modulo: heredado fleet.vehicle
Detalle: agrega una relacion con el nuevo modulo fleet.vehicle.drive.stock
vehicle_ids o2m
"""

class fleet_vehicle_drive_stock(osv.osv):
    _name = "fleet.vehicle.drive.stock"
    _columns = {    
        'driver_id':fields.many2one('res.partner', 'Driver', required=False,help='Este campo relaciona el transportista asignado a la guia de remision.'),
        'vehicle_id':fields.many2one('fleet.vehicle', 'Vehicle', required=False, help='Este campo relaciona el vehiculo asignado a la guia de remision.'),
        'stock_picking_id':fields.many2one('stock.picking', 'Stock Picking', required=False ,help='Este campo relacional que asigna la guia de remision.'), 
    }
    _defaults = {
    }

fleet_vehicle_drive_stock()

class fleet_vehicle(osv.osv):
    _inherit = "fleet.vehicle"
    _name = "fleet.vehicle"
    _columns = {
        'vehicle_ids':fields.one2many('fleet.vehicle.drive.stock', 'vehicle_id', 'Vehicle', required=False, help='Este campo relaciona el vehiculo asignado a la guia de remision.'),
#        'stock_picking_ids': fields.many2many('stock.picking', 'rel_picking_vehicle','stock_picking_id','vehicle_conductor_id','Transporte'),
    #    'stock_picking_out_ids': fields.many2many('stock.picking', 'rel_picking_out_vehicle','stock_picking_out_id','vehicle_conductor_out_id','Transporte'),
    }
    _defaults = {
    }

fleet_vehicle()

"""
Modulo: heredado res.partner
Detalle: agrega una relacion con el nuevo modulo fleet.vehicle.drive.stock
driver_ids o2m
"""
class trescloud_partner(osv.osv):
    _inherit = 'res.partner'
    
    _columns={
        'driver_ids':fields.one2many('fleet.vehicle.drive.stock', 'driver_id', 'Conductor', required=False, help='Este campo relaciona el conductor asignado a la guia de remision.'),
            }
    _defaults = {
            }
trescloud_partner()
"""
Modulo: Nuevo
Detalle: relaciona 3 distintos objetos para poder crear la guia de remosion
driver_id -> res_partner
vehicle_id -> fleet_vehicle 
stock_pinking_id -> sotock_piking
"""

