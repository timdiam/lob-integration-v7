# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2014 TRESCLOUD Cia Ltda (trescloud.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import netsvc
from tools.translate import _
import openerp.addons.decimal_precision as dp

class stock_move(osv.osv):
    _name = "stock.move"
    _inherit = "stock.move"    

    def _create_account_move_line(self, cr, uid, move, src_account_id, dest_account_id, reference_amount, reference_currency_id, context=None):
        '''
        Agrega la relacion entre asiento contable de valoracion de inventario y el movimiento de inventario
        Campo stock_move_id en el objeto account.move.line
        '''
        _move_lines = super(stock_move, self)._create_account_move_line(cr, uid, move, src_account_id, dest_account_id, reference_amount, reference_currency_id, context=context)
        _debit_lines = _move_lines[0][2]
        _credit_lines = _move_lines[1][2]
        _debit_lines['stock_move_id'] = move.id
        _credit_lines['stock_move_id'] = move.id
        _move_lines2 = [(0, 0, _debit_lines), (0, 0, _credit_lines)]
        return _move_lines2
    
    def _default_location_source(self, cr, uid, context=None):
        if context is None:
            context = {}
        if context.get('location_id', []):
            try:
                return context['location_id']
            except:
                pass
        else:
            return super(stock_move,self)._default_location_source(cr, uid, context)
        
    def _default_location_destination(self, cr, uid, context=None):
        if context is None:
            context = {}
        if context.get('location_dest_id', []):
            try:
                return context['location_dest_id']
            except:
                pass
        else:
            return super(stock_move,self)._default_location_destination(cr, uid, context)
        
    _defaults={
               'location_id': _default_location_source,
               'location_dest_id': _default_location_destination,
               }       
stock_move()