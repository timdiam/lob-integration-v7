# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2014 TRESCLOUD Cia Ltda (trescloud.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import netsvc
from tools.translate import _
import openerp.addons.decimal_precision as dp
from datetime import datetime


class tariff_heading(osv.osv):
    _name = "tariff.heading"
    _columns = {
                'name': fields.char('Name', size=32),
                'physical_unit': fields.char('Physical Unit', size=32),
                'ad_valorem': fields.char('Ad Valorem', size=32),
                'description': fields.text('Tariff Description'),
    }
    
tariff_heading()


class product_product(osv.osv):
    _inherit = "product.product"
    
    def _calculate_last_purchase(self, cr, uid, ids, name, args, context=None):
        res = {}
        for product in self.browse(cr, uid, ids):
            res[product.id]={}
            if product.invoice_line_ids:
                last_purchase = product.invoice_line_ids[0]
                if last_purchase:
                    res[product.id].update({
                                            'last_date_purchase': last_purchase.date_invoice,
                                            'last_price_purchase': last_purchase.price_unit
                                            })
            elif product.inicial_date_purchase and product.inicial_price_purchase:
                res[product.id].update({
                                        'last_date_purchase': product.inicial_date_purchase,
                                        'last_price_purchase': product.inicial_price_purchase
                                        })
                
        return res

    def _check_default_code(self, cr, uid, ids, context=None):
        for obj in self.browse(cr, uid, ids, context=context):
            if obj.default_code and self.search(cr, uid, [('id', '!=', obj.id), ('default_code', '=', obj.default_code)], context=context):
                return False
        return True

    def copy(self, cr, uid, id, default=None, context=None):
        default = default or {}
        default.update(default_code=self.browse(cr, uid, id, context=context).default_code + ' ' + datetime.now().strftime('(%Y%m%d%H%M%S)'))
        return super(product_product, self).copy(cr, uid, id, default, context=context)
    
    def _location(self, cr, uid, ids, name, args, context=None):
        def f(obj):
            lista = self.show_stock_product(cr, uid, obj.id, context=None)
            loc = self.parser_list(lista)
            return '\n '.join(["%s: %s" % l for l in loc])
        return {obj.id: f(obj) for obj in self.browse(cr, uid, ids, context=context)}          

    def parser_name(self, s):
        return s.split('/')[-1] if '/' in s else s
    
    def parser_list(self, lista):
        return [(self.parser_name(p[0]), p[1]) for p in lista]
    
    #Devuelve el stock de un producto por almacenes
    def show_stock_product(self, cr, uid, product_id, context=None):
        cr.execute('''
                select *
                from (
                    select complete_name as bodega, sum(product_qty) as cantidad
                    from report_stock_inventory rsi join 
                         stock_location sl 
                        on rsi.location_id = sl.id join
                         product_product pp 
                        on rsi.product_id = pp.id    
                    where product_id = %s and location_type = 'internal' and state = 'done'
                    group by complete_name
                    Union
                    select 'Total' as bodega, sum(product_qty) as cantidad
                    from report_stock_inventory rsi join 
                         stock_location sl 
                        on rsi.location_id = sl.id join
                         product_product pp 
                        on rsi.product_id = pp.id    
                    where product_id = %s and location_type = 'internal' and state = 'done'
                ) as result
                where result.cantidad > 0
            ''', (product_id, product_id,)
        )
        res = cr.fetchall()
        return res
    
    _columns = {
                'tariff_heading': fields.many2many('tariff.heading','name', 
                                                 string = 'Tariff Heading',
                                                 help = 'Tariff heading for the product'),
                'last_date_purchase': fields.function(_calculate_last_purchase, multi='date_price', method=True, type='datetime', string='Last Purchase Date'),
                'last_price_purchase': fields.function(_calculate_last_purchase, multi='date_price', method=True, type='float', string='Last Purchase Price'),
                'inicial_date_purchase':  fields.datetime('Inicial Purchase Date'),
                'inicial_price_purchase': fields.float('Inicial Purchase Price', digits_compute= dp.get_precision('Account')),
                'location': fields.function(_location, type='text', string='Location', help='Displays the warehouses where the product has stock.')                
    }
    
    _constraints = [
        (_check_default_code, "La referencia interna debe ser única entre todos los productos", ['default_code'])
    ]
    
product_product()