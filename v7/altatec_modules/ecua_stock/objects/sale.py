 # -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2014 TRESCLOUD Cia Ltda (trescloud.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import netsvc
from tools.translate import _
import openerp.addons.decimal_precision as dp

class sale_order(osv.osv):
    _inherit = 'sale.order' 
    #Do not touch _name it must be same as _inherit
    #_name = 'sale.order'
    
    # Este metodo encargado de preparar los pickings, se obtienen las configuraciones por default
    # de los stock.pickings y se obtiene las direcciones y telefonos, aparte se entrega los parametros
    # para los nuevos campos creados de los stock pickings
    
    def _prepare_order_picking(self, cr, uid, order, context=None):
        result = super(sale_order,self)._prepare_order_picking(cr, uid, order, context=None)
        picking_obj = self.pool.get('stock.picking.out')
        location_obj = self.pool.get('stock.location')
        location_id = False
        if order.stock_warehouse_id:
            location_id = order.stock_warehouse_id.lot_stock_id.id
        else:
            location_id = order.shop_id.warehouse_id.lot_stock_id.id
        #output_id = order.shop_id.warehouse_id.lot_output_id.id
        location_source_id = location_obj.browse(cr, uid,location_id)
        #location_dest_id = location_obj.browse(cr, uid, output_id)
        var={'loc_address':'','loc_phone':''}
        if location_source_id.partner_id:
            var.update({
                        'loc_address':self.get_partner_address(cr, uid, location_source_id.partner_id.id, context),
                        'loc_phone':self.get_partner_phone(cr, uid, location_source_id.partner_id.id, context),
                        })
        result.update({
                       'move_reason': 'sales',
                       'location_id': location_source_id.id,
                       #'location_dest_id': location_dest_id.id,
                       'waybill_loc_dest_address': self.get_partner_address(cr, uid, order.partner_shipping_id.id, context),
                       'waybill_loc_dest_phone': self.get_partner_phone(cr, uid, order.partner_shipping_id.id, context),
                       'waybill_loc_address': var['loc_address'],
                       'waybill_loc_phone': var['loc_phone'],
                       })
        return result
    
    def get_partner_address(self, cr, uid, contact_id, context=None):
        '''
        Retorna la direccion para entrega, puede diferir de la direccion de contactoW 
        TODO: Integrarlo con modulos comunidad de multiples direcciones
        '''
        waybill_address = None
        contact_obj=self.pool.get('res.partner')
        contact = contact_obj.browse(cr,uid,[contact_id])[0]
        waybill_address = contact.street
        return waybill_address
    
    def get_partner_phone(self, cr, uid, contact_id, context=None):
        '''
        Retorna el telefono para entrega, puede diferir del telefono de contacto 
        TODO: Integrarlo con modulos comunidad de multiples direcciones
        TODO: Implementar printer_id (segun el punto de impresion puede cambiar la direccion)
        '''
        waybill_phone = None
        contact_obj=self.pool.get('res.partner')
        contact = contact_obj.browse(cr,uid,[contact_id])[0]
        waybill_phone = contact.phone or contact.mobile
        return waybill_phone
    # seteamos la factura en los pickings cuando es factura manual
    def manual_invoice(self, cr, uid, ids, context=None):
        inv_obj = self.pool.get('account.invoice')
        sale_obj = self.pool.get('sale.order')
        picking_obj = self.pool.get('stock.picking')
        # add the invoice to the sales order's invoices       
        res = super(sale_order,self).manual_invoice(cr, uid, ids, context=context)
        for sale_id in self.browse(cr, uid, ids, context=context):
            for picking_id in sale_id.picking_ids:
                picking_obj.write(cr, uid, [picking_id.id], {
                                                           'invoice_id': res['res_id'],}, 
                                  context=context)
        return res
    
sale_order()

from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

class sale_advance_payment_inv(osv.osv_memory):
    _inherit = "sale.advance.payment.inv"
    # seteamos la factura en los pickings cuando es factura manual por lineas
    # TODO: Faltan contemplar el escenario cuando la factura es asociada a las lineas porque no cubre
    # por ejemplo una fatcura por lineas el stock picking a cual factura parcial deberia estar asociado.
    def _create_invoices(self, cr, uid, inv_values, sale_id, context=None):
        res = super(sale_advance_payment_inv,self)._create_invoices(cr, uid, inv_values, sale_id, context=context)
        inv_obj = self.pool.get('account.invoice')
        sale_obj = self.pool.get('sale.order')
        picking_obj = self.pool.get('stock.picking')
        # add the invoice to the sales order's invoices
        sale_id = sale_obj.browse(cr, uid,sale_id,context=context)
        for picking_id in sale_id.picking_ids:
            picking_obj.write(cr, uid, [picking_id.id], {'invoice_id': inv_id}, context=context)
        return inv_id
    
sale_advance_payment_inv()
