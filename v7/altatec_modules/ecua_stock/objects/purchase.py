# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2014 TRESCLOUD Cia Ltda (trescloud.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import netsvc
from tools.translate import _
import openerp.addons.decimal_precision as dp

class purchase_order(osv.osv):
    _inherit = "purchase.order" 
    #Do not touch _name it must be same as _inherit
    #_name = 'openerpmodel'
    
    # TODO: Bug en el codigo base y con este metodo se parcha parcialmente
    def _create_pickings(self, cr, uid, order, order_lines, picking_id=False, context=None):
        if not picking_id:
            picking_id = self.pool.get('stock.picking.in').create(cr, uid, self._prepare_order_picking(cr, uid, order, context=context))
        return super(purchase_order,self)._create_pickings(cr, uid, order, order_lines, picking_id, context)
    
    # Se envia los campos extra de ecua_stock desde purchase
    
    def _prepare_order_picking(self, cr, uid, order, context=None):
        result = super(purchase_order,self)._prepare_order_picking(cr, uid, order, context)
        picking_obj = self.pool.get('stock.picking.in')
        location_obj = self.pool.get('stock.location')
        sale_obj = self.pool.get('sale.order') 
        location_destination = picking_obj._default_location_destination(cr, uid, context),
        location_destination_id = location_obj.browse(cr, uid, location_destination[0])
        location_source = picking_obj._default_location_source(cr, uid, context),
        location_source_id = location_obj.browse(cr, uid, location_source[0])
        var={'loc_address':'','loc_phone':''}
        if location_source_id.partner_id:
            var.update({
                        'loc_address':self.get_partner_address(cr, uid, location_source_id.parner_id.id, context),
                        'loc_phone':self.get_partner_phone(cr, uid, location_source_id.partner_id.id, context),
                        })
        result.update({
                       'move_reason': 'purchase',
                       'location_id': location_source,
                       'location_dest_id': location_destination,
                       'waybill_loc_address': sale_obj.get_partner_address(cr, uid, order.partner_id.id, context),
                       'waybill_loc_phone': sale_obj.get_partner_phone(cr, uid, order.partner_id.id, context),
                       'waybill_loc_dest_address': var['loc_address'],
                       'waybill_loc_dest_phone': var['loc_phone'],
                       })
        return result
    
purchase_order()