# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2014 TRESCLOUD Cia Ltda (trescloud.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import netsvc
from tools.translate import _
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp


class stock_picking_events(object):

    def onchange_location(self, cr, uid, ids, location_id, context=None):
        if context is None:
            context = {}
        values = {
            'waybill_loc_address': '',
            'waybill_loc_phone': ''
        }
        stock_move_obj = self.pool.get('stock.move')
        if ids:
            pick = self.browse(cr, uid, ids[0], context=context)
            for move in pick.move_lines:
                if move.location_id is False or move.location_id != location_id:
                    stock_move_obj.write(cr, uid, [move.id], {'location_id': location_id}, context=context)

        location_id = location_id and self.pool.get('stock.location').browse(cr, uid, location_id, context)
        if location_id and location_id.partner_id:
            values.update({
                'waybill_loc_address': location_id.partner_id.street or '',
                'waybill_loc_phone': location_id.partner_id.phone or ''
            })

        return {
            'value': values
        }

    def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
        values = {
            'waybill_loc_dest_address': '',
            'waybill_loc_dest_phone': ''
        }
        partner_id = partner_id and self.pool.get('res.partner').browse(cr, uid, partner_id, context)
        if partner_id:
            values.update({
                'waybill_loc_dest_address': partner_id.street or partner_id.street2 or '',
                'waybill_loc_dest_phone': partner_id.phone or ''
            })

        return {
            'value': values
        }


class stock_return_picking(osv.osv_memory):
    _inherit = 'stock.return.picking'
    #Do not touch _name it must be same as _inherit
    _name = 'stock.return.picking'

    def create_returns(self, cr, uid, ids, context=None):
        """ 
         Creates return picking, se aniade si es devolucion o no
         @param self: The object pointer.
         @param cr: A database cursor
         @param uid: ID of the user currently logged in
         @param ids: List of ids selected
         @param context: A standard dictionary
         @return: A dictionary which of fields with values.
        """
        context.update({'is_return':True})
        return super(stock_return_picking,self).create_returns(cr, uid, ids, context)
    
stock_return_picking()


class stock_picking_in(osv.osv):
    _inherit = "stock.picking.in"

    def _default_type(self, cr, uid, context=None):
        '''
        Retorna el tipo de movimiento de bodega, es redefinido en cada clase heredada
        (ejemplo stock.picking.out) para que muestre el tipo correcto en cada escenario.
        '''
        return 'in'
    
    def _compute_lines(self, cr, uid, ids, name, args, context=None):
        result = self.pool.get('stock.picking')._compute_lines(cr, uid, context=None)
        return result

    def onchange_location(self, cr, uid, ids, location_id, context=None):
        if context is None:
            context = {}
        stock_move_obj = self.pool.get('stock.move')
        if ids:
            pick = self.browse(cr, uid, ids[0], context=context)
            for move in pick.move_lines:
                if move.location_id is False or move.location_id != location_id:
                    stock_move_obj.write(cr, uid, [move.id], {'location_id': location_id}, context=context)
        return True

    def onchange_location_dest(self, cr, uid, ids, location_dest_id, context=None):
        if context is None:
            context = {}
        stock_move_obj = self.pool.get('stock.move')
        if ids:
            pick = self.browse(cr, uid, ids[0], context=context)
            for move in pick.move_lines:
                if move.location_dest_id is False or move.location_dest_id != location_dest_id:
                    stock_move_obj.write(cr, uid, [move.id], {'location_dest_id': location_dest_id}, context=context)
        return True
    
    def move_reason_selection(self, cr, uid, context=None):
        context['default_type']=self._default_type(cr, uid, context=None)
        return self.pool.get('stock.picking').move_reason_selection(cr, uid, context)
    
    _columns = {
                'driver_ids':fields.one2many('fleet.vehicle.drive.stock', 'stock_picking_id', 'Stock picking', required=False, help='This field associate a driver with a car.'),
                'move_reason': fields.selection(move_reason_selection,
                                         'Move Reason',
                                         help='Reason for moving stock'),
                'journal_ids': fields.function(_compute_lines, relation='account.move.line', type="many2many", string='Journal Items', 
                                               help="Account move lines associated to the stock picking"),
                'waybill_loc_address':fields.char("Origin Waybill address", help="Origin Waybill address as in VAT document, saved in picking orders only not in partner"),
                'waybill_loc_phone':fields.char("Origin Waybill phone", help="Origin Waybill phone as in VAT document, saved in picking orders only not in partner"),
                'waybill_loc_dest_address':fields.char("Destination Waybill address", help="Destination Waybill address as in VAT document, saved in picking orders only not in partner"),
                'waybill_loc_dest_phone':fields.char("Destination Waybill phone", help="Destination Waybill phone as in VAT document, saved in picking orders only not in partner"),
    }
    
    def _default_location_destination(self, cr, uid, context=None):
        stock_move_obj = self.pool.get('stock.move')
        if context is None:
            context={}
        context['picking_type']=self._default_type(cr, uid, context=None)
        return stock_move_obj._default_location_destination(cr, uid, context)
    
    def _default_location_source(self, cr, uid, context=None):
        stock_move_obj = self.pool.get('stock.move')
        if context is None:
            context={}
        context['picking_type']=self._default_type(cr, uid, context=None)
        return stock_move_obj._default_location_source(cr, uid, context)

    _defaults = {
                 'move_reason': 'purchase',
                 'location_id': _default_location_source,
                 'location_dest_id': _default_location_destination,
        }

stock_picking_in()


class stock_picking(osv.osv, stock_picking_events):
    _inherit = "stock.picking"
    
    def onchange_location_dest(self, cr, uid, ids, location_dest_id, context=None):
        if context is None:
            context = {}
        stock_move_obj = self.pool.get('stock.move')
        if ids:
            pick = self.browse(cr, uid, ids[0], context=context)
            for move in pick.move_lines:
                if move.location_dest_id is False or move.location_dest_id != location_dest_id:
                    stock_move_obj.write(cr, uid, [move.id], {'location_dest_id': location_dest_id}, context=context)
        return True
    
    def onchange_iswaybill(self, cr, uid, ids, waybill_number, is_waybill, context=None):
        """
        Metodo de cambio que pone valores vacios cuando el se desmarca el check Is Waybill
        caso contrario obtiene los valores por defecto de dichos campos
        """
        res = {'value': {},'warning':{},'domain':{}}
        value = {}
        if context == False:
            value['printer_id']= ''
            value['waybill_number']= ''
        else:
            value['printer_id']= self._default_printer_point(cr, uid, None)
            value['waybill_number']= self.default_waybill_number(cr, uid, None)
        
        res['value'] = value        
        return res
    
    def _default_type(self, cr, uid, context=None):
        '''
        Retorna el tipo de movimiento de bodega, es redefinido en cada clase heredada
        (ejemplo stock.picking.out) para que muestre el tipo correcto en cada escenario.
        '''
        return 'internal'
    
    def move_reason_selection(self, cr, uid, context=None):
        '''
        Nos indica las opciones validas para la razon del movimiento para in, internal, out
        (utilizamos el tipo por defeto para discernir la opcion correcta) 
        '''
        if context is None:
            context = {}
        if 'default_type' in context:
            type = context['default_type']
        elif 'picking_type' in context:
            type = context['picking_type']
        else: 
            type = self._default_type(cr, uid, context)
        #res = ()
        if type in ['internal']:
            res = (
                   ('inttransfer', 'Internal Transfer'),
                   ('exhibition', 'Exhibition'),
                   ('others', 'Others')
                    )
        elif type in ['out']:
            res = (
                    ('sales', 'Sale Order'),
                    ('return', 'Return Incoming Shipment'),
                    ('itinsale', 'Itinerant Sale'),
                    ('warranty', 'Replenishment Warranty'),
                    ('repair', 'Repairing'),
                    ('others', 'Others')
                    )
        if type in ['in']:
            res = (
                   ('purchase', 'Purchase Order'),
                   ('return', 'Return Delivery Order'),
                   ('others', 'Others')
                   )
        return res
    
    def _compute_lines(self, cr, uid, ids, name, args, context=None):
        result = {}
        res = []
        for picking_id in self.browse(cr, uid, ids, context=context):
            src = []
            lines = []
            account_move_obj=self.pool.get('account.move.line')
            if picking_id.move_lines:
                for m in picking_id.move_lines:
                    account_move_ids = account_move_obj.search(cr, uid, 
                                            [('stock_move_id', '=', m.id)]
                                            , context=context)
                    if account_move_ids != []:
                        for account_move_line in account_move_obj.browse(cr, uid,account_move_ids,context=context):
                            # mover un tab las lineas de abajo
                            temp_lines = {}
                            # TODO: filtrado mejora credit mayor que 0
                            if account_move_line.credit >= 0.0:
                                # almacenar en res una lista de los movimientos de stock con sus contables
                                res.append((account_move_line.id))
        result[picking_id.id]=res           
        return result
    
    """ agrego campo para relacionar driver_ids con el picking.out """

    _columns = {
        'driver_ids':fields.one2many('fleet.vehicle.drive.stock', 'stock_picking_id', 'Stock picking', required=False, help='This field associate a driver with a car.'),
        'journal_ids': fields.function(_compute_lines, relation='account.move.line', type="many2many", string='Journal Items',
                                       help="Account move lines associated to the stock picking"),
        'move_reason' : fields.selection(move_reason_selection,
                                         'Move Reason',
                                         help='Reason for moving stock'),
        'is_waybill': fields.boolean('Is waybill',help='Indicates if the picking is a legally issued waybill'),
        'printer_id':fields.many2one('sri.printer.point', 'Printer Point', required=False),
        'waybill_number': fields.char('Waybill Number', size=17, readonly=False, help="Unique number of the invoice, computed automatically when the invoice is created."),
        'shop_id': fields.related('printer_id', 
                                  'shop_id',
                                  type='many2one',
                                  relation='sale.shop',
                                  string='Shop', 
                                  readonly=True),
        'location_id': fields.many2one('stock.location', 'Source Location', required=True, select=True,states={'done': [('readonly', True)]}, help="Sets a location if you produce at a fixed location. This can be a partner location if you subcontract the manufacturing operations."),
        'location_dest_id': fields.many2one('stock.location', 'Destination Location', required=True,states={'done': [('readonly', True)]}, select=True, help="Location where the system will stock the finished products."),
        'waybill_loc_address':fields.char("Origin Waybill address", help="Origin Waybill address as in VAT document, saved in picking orders only not in partner"),
        'waybill_loc_phone':fields.char("Origin Waybill phone", help="Origin Waybill phone as in VAT document, saved in picking orders only not in partner"),
        'waybill_loc_dest_address':fields.char("Destination Waybill address", help="Destination Waybill address as in VAT document, saved in picking orders only not in partner"),
        'waybill_loc_dest_phone':fields.char("Destination Waybill phone", help="Destination Waybill phone as in VAT document, saved in picking orders only not in partner"),
        }
    # TODO: Constraint para cuando las bodegas sean de tipo view salte un error
    # def _check_location(self, cr, uid, ids, context=None):
    #    stock_move_obj = self.pool.get('stock.move')
    #    return stock_move_obj._check_location(cr, uid, ids, context)
    
    # _constraints = [
    #    (_check_location, 'You cannot move products from or to a location of the type view.',
    #        ['location_id','location_dest_id'])]
    
    """
    Funcion copy sobreescrita para que esta analice si el proceso es de devoluciones o de tipo copia normal
    """
    def copy(self, cr, uid, id, default=None, context=None):
        default = default or {}
        default.update({'move_reason' : False})
        stock_picking_type = 'stock.picking'
        if default.get('type',False) == 'in':
            stock_picking_type = 'stock.picking.in'
        elif default.get('type',False) == 'out':
            stock_picking_type = 'stock.picking.out'
        stock_picking_id = self.pool.get(stock_picking_type).browse(cr, uid, id, context=context)
        if 'name' in default.keys():
            if default['name'].find('return') > -1 or default['name'].find('devolver') > -1:
                default.update({'location_id' : stock_picking_id.location_dest_id.id})
                default.update({'move_reason': 'return'})
                if 'type' in default and default['type'] == 'in':
                    stock_location_scrapped_id = self.pool.get('stock.location').search(cr, uid, 
                                            [('scrap_location', '=', True)]
                                            , context=context)
                    default.update({'location_dest_id' : stock_location_scrapped_id[0]})
                if 'type' in default and default['type'] == 'out':
                    default.update({'location_dest_id' : stock_picking_id.location_id.id})
                if not 'type' in default.keys():
                    super(stock_picking, self).copy(cr, uid, id, default, context)
        else:
            default.update({'name':False})
        return super(stock_picking, self).copy(cr, uid, id, default, context)
    
    """
    Funcion create se manda el tipo de transaccion que es in, out, internal en el create para que 
    se cree la lista move_reason segun sea el parametro 
    """
    
    def create(self, cr, user, vals, context=None):
        if context is None:
            context = {}
        if 'type' in vals:
            context.update({'default_type': vals['type']})
        new_id = super(stock_picking, self).create(cr, user, vals, context)
        return new_id

    """
    Funciones de retorno de valores por default, sacadas de otras clases para no reescribir codigo
    """
    def _default_printer_point(self, cr, uid, context=None):
        invoice_obj=self.pool.get('account.invoice')
        return invoice_obj._default_printer_point(cr, uid, context)
    
    def onchange_waybill_number(self, cr, uid, ids, waybill_number, context=None):
        invoice_obj=self.pool.get('account.invoice')
        value = invoice_obj.onchange_internal_number(cr, uid, ids, waybill_number, context)
        if value['value'] != {} :
            waybill_num= value['value']['internal_number']
            value.update({
                      'waybill_number' : waybill_num,
                      })
        else:
            return value
              
        return {'value': value}

    def _suggested_wallbill_number(self, cr, uid, printer_id=None, type=None, company_id=None, context=None):
        if context is None:
            context = {}
            
        if not printer_id:
            printer_id = _default_printer_point(cr, uid, context)
        
        number = '001-001-'
        type = self._default_type(cr,uid, context)
        if type in ['internal','out']:
            printer = self.pool.get('sri.printer.point').browse(cr, uid, printer_id, context=context)
            number = printer.shop_id.number + "-" + printer.name + "-"
        return number
    
    def default_waybill_number(self, cr, uid, context=None):
        '''Numero de guia de remision sugerida, depende del punto de impresion
           Puede ser redefinida posteriormente por ejemplo para numeracion automatica
        '''
        number = ''
        printer_id = None
        type = None

        if context is None:
            context = {}
            
        if context.has_key('default_type'):
            type = context['default_type']
        else:
            type = self._default_type(cr,uid,context)

        if context.has_key('printer_id'):
            printer_id = context['printer_id']
        else:
            printer_id = self._default_printer_point(cr, uid, context)
        
        if printer_id and ((type in ['internal']) or (type in ['out'])) :
            number = self._suggested_wallbill_number(cr, uid, printer_id, type, None, context)
        
        return number
    
    def default_company(self, cr, uid, printer_id=None, type=None, company_id=None, context=None):
        sale_obj=self.pool.get('sale.order')
        return sale_obj._get_default_shop(cr,uid, context=None)
    
    def _default_location_destination(self, cr, uid, context=None):
        stock_move_obj = self.pool.get('stock.move')
        context['picking_type']=self._default_type(cr, uid, context=None)
        return stock_move_obj._default_location_destination(cr, uid, context)
    
    def _default_location_source(self, cr, uid, context=None):
        stock_move_obj = self.pool.get('stock.move')
        context['picking_type']=self._default_type(cr, uid, context=None)
        return stock_move_obj._default_location_source(cr, uid, context)

        return None
    
    _defaults = {
        'move_reason': 'inttransfer',
        'is_waybill': False,
        'shop_id': default_company,
        'location_id': _default_location_source,
        'location_dest_id': _default_location_destination,
        'printer_id': _default_printer_point,
        }
    
    #Definicion de los metodos para verificar si es guia de remision o no
    def waybill_generate(self,cr,uid,ids,context=None):
        self.write(cr, uid, ids, {'is_waybill' : True}, context=context)
        return True
         
    def waybill_cancel(self,cr,uid,ids,context=None):
        self.write(cr, uid, ids, {'is_waybill' : False}, context=context)
        return False

    def do_partial(self, cr, uid, ids, partial_datas, context=None):
        for pick in self.browse(cr, uid, ids, context=context):
            if pick.is_waybill:
                if pick.driver_ids:
                    res = super(stock_picking,self).do_partial(cr, uid, ids, partial_datas, context=None)   
                else:
                    raise osv.except_osv(_('Invalid Action!'),
                                         _('Can not transfer products until you insert a vehicle and a carrier.'))
            else:
                res = super(stock_picking, self).do_partial(cr, uid, ids, partial_datas, context=None)         
        return res
stock_picking()


class stock_picking_out(osv.osv, stock_picking_events):
    _inherit = "stock.picking.out"
    
    """
    Se tuvo que definir las funciones nuevamente para que obtuviese el comportamiento de la clase padre
    con la diferencia que se tuvo que pasar el tipo de transaccion en la funcion obtenida con defaul_type
    he instanciar la clase. La herencia aqui en stock.picking esta actuando de manera especial es por eso
    que se redefine los valores y se instancia la clase padre.
    """
    # TODO: Constraint para cuando las bodegas sean de tipo view salte un error
    # def _check_location(self, cr, uid, ids, context=None):
    #    stock_move_obj = self.pool.get('stock.move')
    #    return stock_move_obj._check_location(cr, uid, ids, context)
    
    def onchange_location_dest(self, cr, uid, ids, location_dest_id, context=None):
        if context is None:
            context = {}
        stock_move_obj = self.pool.get('stock.move')
        if ids:
            pick = self.browse(cr, uid, ids[0], context=context)
            for move in pick.move_lines:
                if move.location_dest_id is False or move.location_dest_id != location_dest_id:
                    stock_move_obj.write(cr, uid, [move.id], {'location_dest_id': location_dest_id}, context=context)
        return True
    
    def onchange_iswaybill(self, cr, uid, ids, waybill_number, is_waybill, context=None):
        """
        Metodo de cambio que pone valores vacios cuando el se desmarca el check Is Waybill
        caso contrario obtiene los valores por defecto de dichos campos
        """
        value = {}
        if context == False:
            value['printer_id']= ''
            value['waybill_number']= ''
        else:
            value['printer_id']= self._default_printer_point(cr, uid, None)
            value['waybill_number']= self.default_waybill_number(cr, uid, None)
        return {'value': value}

    def move_reason_selection(self, cr, uid, context=None):
        context['default_type']=self._default_type(cr, uid, context=None)
        return self.pool.get('stock.picking').move_reason_selection(cr, uid, context)

    def _default_printer_point(self, cr, uid, context=None):
        if context is not None:
            context['default_type']=self._default_type(cr, uid, context=None)
        return self.pool.get('stock.picking')._default_printer_point(cr, uid, context=None)

    def default_waybill_number(self, cr, uid, context=None):
        if context is not None:
            context['default_type']=self._default_type(cr, uid, context=None)
        return self.pool.get('stock.picking').default_waybill_number(cr, uid, context=None)

    def default_company(self, cr, uid, printer_id=None, type=None, company_id=None, context=None):
        return self.pool.get('stock.picking').default_company(cr, uid, context=None)

    def onchange_waybill_number(self, cr, uid, ids, waybill_number, context=None):
        return self.pool.get('stock.picking').onchange_waybill_number(cr, uid, ids, waybill_number, context)

    #
    # Los defaults para las funciones no se estan ejecutando pero los campos si aparecen
    # Si alguien alguna sugerencia me la reporte o me avise
    #
    def _default_location_destination(self, cr, uid, context=None):
        stock_move_obj = self.pool.get('stock.move')
        if context is None:
            context={}
        context['picking_type']=self._default_type(cr, uid, context=None)
        context['default_type']=context['picking_type']
        return stock_move_obj._default_location_destination(cr, uid, context)
    
    def _default_location_source(self, cr, uid, context=None):
        stock_move_obj = self.pool.get('stock.move')
        if context is None:
            context={}
        context['picking_type']=self._default_type(cr, uid, context=None)
        context['default_type']=context['picking_type']
        return stock_move_obj._default_location_source(cr, uid, context)

    def action_number(self, cr, uid, ids, context=None):
        """
        Assigns a number, based on the printer point sequence, if any.
        """
        printer_point_obj = self.pool.get('sri.printer.point')
        return {pick.id: printer_point_obj.get_next_sequence_number(cr, uid, pick.printer_id,
                                                                    'waybill', pick.waybill_number, context)
                for pick in self.browse(cr, uid, ids, context=context) if pick.is_waybill}

    def action_number_validate(self, cr, uid, numbers, context=None):
        """
        Validates whether the numbers are repeated or not.
        """
        ids = numbers.keys()
        nums = numbers.values()

        if len(nums) != len(set(nums)) or self.search(cr, uid, [('id', 'not in', ids),
                                                                ('waybill_number', 'in', nums),
                                                                ('is_waybill', '=', True)], context=context):
            raise osv.except_osv(_('Error!'), _('The same number is being used in more than one waybill.'
                                                'Either choose a different number or fix the current printer point\'s'
                                                'waybill sequence number.'))

    def draft_force_assign(self, cr, uid, ids, context=None):
        """
        It calls the number assignation, and the parent implementation.
        """
        numbers = self.action_number(cr, uid, ids, context)
        self.action_number_validate(cr, uid, numbers, context)
        for id_, number in numbers.iteritems():
            self.write(cr, uid, [id_], {'waybill_number': number, 'name': number})
        return super(stock_picking_out, self).draft_force_assign(cr, uid, ids, context)

    def _compute_lines(self, cr, uid, ids, name, args, context=None):
        result = self.pool.get('stock.picking')._compute_lines(cr, uid, context=None)
        return result
    
    _columns = {
		'driver_ids':fields.one2many('fleet.vehicle.drive.stock', 'stock_picking_id', 'Stock picking', required=False, help='This field associate a driver with a car.'),
        'journal_ids': fields.function(_compute_lines, relation='account.move.line', type="many2many", string='Journal Items',
                                       help="Account move lines associated to the stock picking"),
        'move_reason' : fields.selection(move_reason_selection,
                                         'Move Reason',
                                         help='Reason for moving stock'),
        'is_waybill': fields.boolean('Is waybill',help='Indicates if the picking is a legally issued waybill'),
        'printer_id':fields.many2one('sri.printer.point', 'Printer Point', required=False),
        'waybill_number': fields.char('Waybill Number', size=17, readonly=False, help="Unique number of the invoice, computed automatically when the invoice is created."),
        'shop_id': fields.related('printer_id', 
                                  'shop_id',
                                  type='many2one',
                                  relation='sale.shop',
                                  string='Shop', 
                                  readonly=True),
        'location_id': fields.many2one('stock.location', 'Location', states={'done':[('readonly', True)], 'cancel':[('readonly',True)]}, help="Keep empty if you produce at the location where the finished products are needed." \
                "Set a location if you produce at a fixed location. This can be a partner location " \
                "if you subcontract the manufacturing operations.", select=True),
        'location_dest_id': fields.many2one('stock.location', 'Dest. Location', states={'done':[('readonly', True)], 'cancel':[('readonly',True)]}, help="Location where the system will stock the finished products.", select=True),
        'waybill_loc_address':fields.char("Origin Waybill address", help="Origin Waybill address as in VAT document, saved in picking orders only not in partner"),
        'waybill_loc_phone':fields.char("Origin Waybill phone", help="Origin Waybill phone as in VAT document, saved in picking orders only not in partner"),
        'waybill_loc_dest_address':fields.char("Destination Waybill address", help="Destination Waybill address as in VAT document, saved in picking orders only not in partner"),
        'waybill_loc_dest_phone':fields.char("Destination Waybill phone", help="Destination Waybill phone as in VAT document, saved in picking orders only not in partner"),
        }
    # _constraints = [
    #    (_check_location, 'You cannot move products from or to a location of the type view.',
    #        ['location_id','location_dest_id'])]
    _defaults = {
        'move_reason': 'sales',
        'is_waybill': False,
        'printer_id': _default_printer_point,
        'waybill_number': default_waybill_number,
        'shop_id': default_company,
        'location_id': _default_location_source,
        'location_dest_id': _default_location_destination,
        }
    
    def _default_type(self, cr, uid, context=None):
        '''
        Retorna el tipo de movimiento de bodega, es redefinido en cada clase heredada
        (ejemplo stock.picking.out) para que muestre el tipo correcto en cada escenario.
        '''
        return 'out'
    
    def waybill_generate(self,cr,uid,ids,context=None):
        self.write(cr, uid, ids, {'is_waybill' : True}, context=context)
        return True
         
    def waybill_cancel(self,cr,uid,ids,context=None):
        self.write(cr, uid, ids, {'is_waybill' : False}, context=context)
        return False
stock_picking_out()
