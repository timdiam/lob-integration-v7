# -*- encoding: utf-8 -*-
#######################################################################################################
#
#######################################################################################################
from openerp.osv import fields, osv


#######################################################################################################
#
#######################################################################################################
class altatec_deposit(osv.osv):

    _name = "altatec.deposit"

    #######################################################################################################
    #
    #######################################################################################################
    def unlink(self, cr, uid, ids, context=None):
        for deposit in self.browse(cr, uid, ids, context=context):
            for voucher in deposit.voucher_ids:
                if voucher.deposit_state == 'deposited':
                    raise osv.except_osv("Error!", "No se puede eliminar este deposito hasta que el deposito esta anulado")
            for move in deposit.move_ids:
                if move.state == 'posted':
                    raise osv.except_osv("Error!", "No se puede eliminar este deposito hasta que todos los asientos contables estan anulados")

    #######################################################################################################
    #
    #######################################################################################################
    def confirm_deposit(self, cr, uid, ids, context=None):
        account_move_obj = self.pool.get('account.move')
        for deposit in self.browse(cr, uid, ids, context=context):
            for account_move in deposit.move_ids:
                account_move_obj.button_validate(cr, uid, [account_move.id], context=context)
            for voucher in deposit.voucher_ids:
                voucher.write({"deposit_state":"deposited"})
        self.write(cr, uid, ids, {'state':'done'}, context=context)

    #######################################################################################################
    #
    #######################################################################################################
    def cancel_deposit(self, cr, uid, ids, context=None):
        account_move_obj = self.pool.get('account.move')
        for deposit in self.browse(cr, uid, ids, context=context):
            for account_move in deposit.move_ids:
                account_move_obj.button_cancel(cr, uid, [account_move.id], context=context)
            for voucher in deposit.voucher_ids:
                voucher.write({"deposit_state":"not_deposited"})
        self.write(cr, uid, ids, {'state':'new'}, context=context)

    #######################################################################################################
    #
    #######################################################################################################
    def get_default_name_from_sequence(self, cr, uid, context=None):

        company_obj = self.pool.get("res.company")
        company_ids = company_obj.search(cr, uid, [], context=context)
        if not len(company_ids):
            raise osv.except_osv("Error!", "No se encuentra un res.company. Contacte a AltaTec.")
        company = company_obj.browse(cr, uid, company_ids[0], context=context)

        if not company.deposit_sequence_id:
            raise osv.except_osv("Error!", "No se ha asignado una secuencia para depositos en la compania. Contacte a AltaTec")

        return self.pool.get('ir.sequence')._next(cr, uid, [company.deposit_sequence_id.id], context=context)

    #######################################################################################################
    #
    #######################################################################################################
    _columns = { "name"        : fields.char("Nombre"),
                 "account_id"  : fields.many2one("account.account", "Cuenta de deposito"),
                 "date"        : fields.date("Fecha de deposito"),
                 "reference"   : fields.char("Referencia"),
                 "state"       : fields.selection([("new","Nuevo"),("done","Depositado")], "Estado"),
                 "voucher_ids" : fields.one2many("account.voucher", "deposit_id", "Pagos depositados"),
                 "move_ids"    : fields.one2many("account.move", "deposit_id", "Asiento contable"),
               }

    _defaults = { "name"  : get_default_name_from_sequence,
                  "date"  : fields.date.context_today,
                  "state" : "new",
                }