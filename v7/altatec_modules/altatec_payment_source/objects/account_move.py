# -*- encoding: utf-8 -*-
##############################################################################
#
##############################################################################
from openerp.osv import fields, osv


##############################################################################
#
##############################################################################
class account_move(osv.osv):

    _inherit = "account.move"

    _columns = {
                 'create_date'      : fields.datetime('Fecha de Creacion', readonly=True),
                 'create_uid'       : fields.many2one('res.users', 'Usuario de Creacion', readonly=True),
                 'write_date'       : fields.datetime('Fecha Ultima Modificacion', readonly=True),
                 'write_uid'        : fields.many2one('res.users', 'Usuario Ultima Modificacion', readonly=True),
                'payment_source_type_id' : fields.many2one( "altatec.payment.source.type", "Tipo Transaccion" ),
                 'check_number'           : fields.char( "Numero de Cheque" ),
                 'deposit_id'             : fields.many2one("altatec.deposit", "Deposit ID"),
               }