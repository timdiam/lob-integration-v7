# -*- encoding: utf-8 -*-
##############################################################################
#
##############################################################################
from openerp.osv import fields, osv
from lxml import etree

##############################################################################
#
##############################################################################
class account_voucher(osv.osv):

    _inherit = "account.voucher"

    ##############################################################################
    #
    ##############################################################################
    def proforma_voucher(self, cr, uid, ids, context=None):

        for voucher in self.browse( cr, uid, ids, context ):

            if( voucher.need_check_info ):

                line_obj = self.pool.get('altatec.payment.source.line')

                # Get the preview check number that has been presented to the user
                preview_check_number = voucher.check_number_preview

                # Get the actual check number from the payment_source_line that matches this voucher's
                # payment_source and payment_source_type
                line_ids = line_obj.search( cr, uid, [('type_id','=',voucher.payment_source_type_id.id),
                                                 ('source_id', '=', voucher.payment_source_id.id ),
                                                ], context=context
                                     )
                if( len(line_ids) < 1 ):
                    raise osv.except_osv( "Error!", "No se puede encontrar el Metodo de Pago." )

                line = line_obj.browse( cr, uid, line_ids[ 0 ], context=context )

                if( not line.check_sequence_id ):
                    raise osv.except_osv( "Error!", "No se puede encontrar la secuencia del cheque para este metodo de pago." )

                actual_check_number = self.pool.get('ir.sequence')._next( cr, uid, [line.check_sequence_id.id], context=context )

                if( actual_check_number != preview_check_number ):
                    voucher.write( { 'check_num_warning': True, 'check_num_warning_msg': "Advertencia: El numero de cheque previsto: " + preview_check_number + " ha cambiado"} )

                voucher.write( { 'check_number' : actual_check_number } )

        # Call super
        retval = super( account_voucher, self ).proforma_voucher( cr, uid, ids, context=context )

        # Overwrite the new fields on the account move
        for voucher in self.browse( cr, uid, ids, context=context ):
            if( voucher.move_id ):
                voucher.move_id.write( { 'check_number': voucher.check_number, 'payment_source_type_id': voucher.payment_source_type_id.id } )

        return retval

    ##############################################################################
    #
    ##############################################################################
    def dismiss_warning(self, cr, uid, ids, context=None):
        for voucher in self.browse( cr, uid, ids, context=context ):
            voucher.write( { 'check_num_warning': False, 'check_num_warning_msg' : "" } )

    ##############################################################################
    #
    ##############################################################################
    def onchange_payment_source(self, cr, uid, ids, payment_source_id, context=None):

        retval = { 'value': { 'journal_id' : False,
                              'number'     : False,
                              'check_number_preview'   : False,
                              'payment_source_type_id' : False
                            }
                 }

        source_line_obj = self.pool.get('altatec.payment.source.line')

        line_ids = source_line_obj.search( cr, uid, [('source_id', '=', payment_source_id)], context=context )

        if( len(line_ids) == 0 ):
            retval[ 'warning' ] = { 'title': "Warning!", 'message' : "Este Concepto de Pago no tiene lineas de Metodos de Pago. Por favor configure el Concepto de Pago" }

        type_ids = []

        # Determine the payment.source.types that are allowed in the domain, based on what type of payment
        # this is, and the information of the payment.source.type
        for line in source_line_obj.browse( cr, uid, line_ids, context=context ):

            # If this is an "Ingreso" payment type and this isn't a client payment, leave this type out
            if( line.type_id.in_out == 'in' ):
                if( 'type' not in context or context['type'] != 'receipt' ):
                    continue
            else: # If this is an "Egreso" payment type and this isn't a payment to supplier, leave this type out
                if( 'type' not in context or context['type'] != 'payment' ):
                    continue

            # If this is an "Out Check" payment type and this isn't an "Escribir Cheques" payment, leave this out
            if( line.type_id.is_out_check ):
                if( 'write_check' not in context or context['write_check'] == False ):
                    continue
            else:  # If this isn't an "Out Check" payment type and this is an "Escribir Cheques" payment, leave this out
                if( 'write_check' in context and context['write_check'] == True ):
                    continue

            type_ids.append( line.type_id.id )

        if( len(type_ids) < 1 and payment_source_id ):
            retval[ 'warning' ] = { 'title': "Warning!", 'message': "Este Concepto de Pago no tiene ningun 'Metodo de Pago' para este tipo de pago" }

        retval[ 'domain' ] = { 'payment_source_type_id' : [ ('id', 'in', type_ids) ] }

        return retval

    ##############################################################################
    #
    ##############################################################################
    def onchange_payment_source_type(self, cr, uid, ids, payment_source_type_id, payment_source_id, context=None):

        retval = { 'value': { 'journal_id' : False,
                              'number'     : False,
                              'check_number_preview' : False,
                            }
                 }

        source_line_obj = self.pool.get('altatec.payment.source.line')

        line_ids = source_line_obj.search( cr, uid, [('source_id', '=', payment_source_id), ('type_id', '=', payment_source_type_id)], context=context )

        # The selected payment source has a line with the selected payment source type
        if( len(line_ids) > 0 ):

            source_line = source_line_obj.browse( cr, uid, line_ids[0], context=context )

            # Set the journal_id, or throw an error
            if( not source_line.journal_id ):
                retval[ 'warning' ] = { 'title': "Error!", 'message': "Este Metodo de pago no tiene un Diario. Por favor configure el Concepto de Pago" }
                retval[ 'value'   ][ 'payment_source_type_id' ] = False
            else:
                retval[ 'value' ][ 'journal_id' ] = source_line.journal_id.id

                # If we're writing a check, set the check_number_preview and number based on this line's check_sequence_id
                if( source_line.is_out_check ):
                    if( source_line.check_sequence_id ):
                        retval[ 'value' ][ 'check_number_preview' ] = self.get_next_from_sequence(cr, uid, source_line.check_sequence_id.id, context=context)[0]
                    else:
                        retval[ 'warning' ] = { 'title': "Error!", 'message': "Este Metodo de pago no tiene una secuencia para el numero de cheque. Por favor configure el Concepto de Pago" }
                        retval[ 'value' ][ 'payment_source_type_id' ] = False

                if( not source_line.journal_id.sequence_id ):
                    retval[ 'warning' ] = { 'title': "Error!", 'message': "El diario de este Metodo de Pago no tiene una secuencia. Por favor configure el Concepto de Pago" }
                    retval[ 'value' ][ 'payment_source_type_id' ] = False

        # The selected payment source has NO line with the selected payment source type
        else:
            retval[ 'value' ][ 'payment_source_type_id' ] = False

        return retval

    ##############################################################################
    #
    ##############################################################################
    def onchange_journal(self, cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount, ttype, company_id, context=None):

        if not context:
            context = {}

        res = super( account_voucher, self ).onchange_journal( cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount, ttype, company_id, context )

        # We want to calculate check_numberand number using the onchange_payment_source_type, not onchange_journal
        if( res and 'value' in res ):
            if( 'check_number' in res['value'] ):
                del res['value']['check_number']
            if( 'number' in res['value'] ):
                del res['value']['number']
            if( 'need_check_info' in res['value'] ):
                del res['value']['need_check_info']

        return res

    ##############################################################################
    # Column definitions
    ##############################################################################
    _columns = { "payment_source_id"      : fields.many2one( "altatec.payment.source", "Concepto de pago", required=True ),
                 "payment_source_type_id" : fields.many2one( "altatec.payment.source.type", "Metodo de concepto", required=True ),
                 "date_to_deposit"        : fields.date( "Fecha a Depositar" ),
                 "check_number_preview"   : fields.char( "Numero de Cheque Previsto" ),
                 "check_num_warning"      : fields.boolean( "Mostrar Advertencia" ),
                 "check_num_warning_msg"  : fields.char( "Check Number Warning" ),
                 "deposit_id"             : fields.many2one("altatec.deposit", "Deposit ID"),
                 "deposit_state"          : fields.selection([("deposited","Depositado"),("not_deposited","No depositado")], "Estado de deposito", required=True),
               }

    def _get_default_source(self, cr, uid, context={}):
        if 'default_concept' in context:
            obj_data = self.pool.get('ir.model.data')
            res = obj_data.get_object_reference(cr, uid, 'altatec_payment_source', 'altatec_payment_source_retenciones_source')
            return res[1]
        else:
            return None

    def _get_default_source_type(self, cr, uid, context={}):
        if 'default_concept' in context:
            if context['default_concept'] == 'ir':
                obj_data = self.pool.get('ir.model.data')
                res = obj_data.get_object_reference(cr, uid, 'altatec_payment_source', 'altatec_payment_source_type_retenciones_ir')
                return res[1]

            elif context['default_concept'] == 'iva':
                obj_data = self.pool.get('ir.model.data')
                res = obj_data.get_object_reference(cr, uid, 'altatec_payment_source', 'altatec_payment_source_type_retenciones_iva')
                return res[1]

        else:
            return None

    _defaults = { 'journal_id'            : False,
                  "date_to_deposit"       : fields.date.context_today,
                  'check_num_warning'     : False,
                  'check_num_warning_msg' : "",
                  'deposit_state'         : "not_deposited",
                  'payment_source_id'     : _get_default_source,
                  'payment_source_type_id': _get_default_source_type,
                }