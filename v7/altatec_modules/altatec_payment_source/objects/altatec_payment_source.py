# -*- encoding: utf-8 -*-
#######################################################################################################
#
#######################################################################################################
from openerp.osv import fields, osv


#######################################################################################################
# altatec_payment_source class definition
#######################################################################################################
class altatec_payment_source(osv.osv):

    _name = 'altatec.payment.source'

    #######################################################################################################
    # Get the line of the provided source_id with the given type_id
    #######################################################################################################
    def _get_line_from_type_id(self, cr, uid, source_id, type_id, context=None):
        source = self.browse(cr, uid, source_id, context=context)
        for line in source.line_ids:
            if line.type_id.id == type_id:
                return line
        return False

    #######################################################################################################
    # create() definition. Ensure that there's not already a Concepto de Pago with
    # this code, and that no two lines in this object have the same "Tipo".
    #######################################################################################################
    def create(self, cr, uid, vals, context=None ):

        # Make sure no Concepto de Pago already has this code
        if( 'code' in vals ):
            ids = self.search( cr, uid, [('code', '=', vals['code'])], context=context )
            if( len(ids) > 0 ):
                raise osv.except_osv( "Error!", "Un concepto de pago existente ya tiene el codigo: " + vals['code'] )

        # Make sure no two lines have the same type
        if( 'line_ids' in vals ):
            type_ids = []
            for line in vals['line_ids']:
                if line[2]['type_id'] in type_ids:
                    raise osv.except_osv( "Error!", "Un Concepto de Pago no puede tener dos lineas con el mismo tipo" )
                type_ids.append( line[2]['type_id'] )

        return super( altatec_payment_source, self ).create( cr, uid, vals, context=context )

    #######################################################################################################
    #
    #######################################################################################################
    def write(self, cr, uid, ids, vals, context=None ):

        for source in self.browse( cr, uid, ids, context=context ):

            # Make sure no Concepto de Pago already has this code
            if( 'code' in vals ):
                source_ids = self.search( cr, uid, [('code', '=', vals['code'])], context=context )
                if( len(source_ids) > 0 ):
                    if( len(source_ids) == 1 and source_ids[0] != source.id ):
                        raise osv.except_osv( "Error!", "Un concepto de pago existente ya tiene el codigo: " + vals['code'] )
                    if( len(source_ids) > 1 ):
                        raise osv.except_osv( "Error!", "Un concepto de pago existente ya tiene el codigo: " + vals['code'] )

            # Make sure no two lines have the same type
            if( 'line_ids' in vals ):

                new_line_type_ids = []

                for line in vals['line_ids']:
                    if( line[0] == 0 ):
                        if( line[2]['type_id'] in new_line_type_ids ):
                            raise osv.except_osv( "Error!", "Un Concepto de Pago no puede tener dos lineas con el mismo tipo" )
                        new_line_type_ids.append( line[2]['type_id'] )

                for line in source.line_ids:
                    if( line.type_id.id in new_line_type_ids ):
                        raise osv.except_osv( "Error!", "Un Concepto de Pago no puede tener dos lineas con el mismo tipo" )

        return super( altatec_payment_source, self ).write( cr, uid, ids, vals, context=context )

    #######################################################################################################
    # Iterate over all line_ids, assign a diary to each one based on the
    # the name and code of the line's type.
    #######################################################################################################
    def create_journals(self, cr, uid, ids, context=None ):

        for source in self.browse( cr, uid, ids, context=context ):

            for line in source.line_ids:

                journal_obj = self.pool.get('account.journal')

                # Calculate the journal's name and code
                journal_name = source.name + '-' + line.type_id.name
                journal_code = source.code + line.type_id.code


                journal_ids = journal_obj.search( cr, uid, ['|',('code','=',journal_code),('name','=',journal_name)], context=context )

                # A journal with this code already exists, verify it
                if( len( journal_ids ) > 0 ):
                    raise osv.except_osv( "Error!", "No se puede crear un diario para tipo: " + line.type_id.name
                                          + ".\n Ya existe un diario con el codigo: " + journal_code + " o nombre: " + journal_name )

                # Create a sequence
                sequence_id = self.pool.get('ir.sequence').create( cr, uid,
                                                                   { 'name'    : journal_name,
                                                                     'prefix'  : '%(year)s-' + journal_code + '-',
                                                                     'padding' : 6,
                                                                   }
                                                                 )

                # Create the journal
                journal_field_vals = { 'name' : journal_name,
                                       'code' : journal_code,
                                       'type' : line.type_id.type,
                                       'default_debit_account_id'  : line.journal_account_id.id,
                                       'default_credit_account_id' : line.journal_account_id.id,
                                       'update_posted' : True,
                                       'allow_date'    : True,
                                       'entry_posted'  : True,
                                       'sequence_id'   : sequence_id,
                                     }

                if( line.type_id.is_out_check ):
                    journal_field_vals[ 'allow_check_writing' ] = True
                    if( line.check_report_id ):
                        journal_field_vals[ 'report_check_id' ] = line.check_report_id.id
                    else:
                        raise osv.except_osv( "Error!", "No se puede crear el diario para tipo " + line.type_id.name
                                              + " es un cheque propio pero esta linea no tiene un 'Reporte del Cheque'" )

                journal_id = journal_obj.create( cr, uid, journal_field_vals )

                line.write( { 'journal_id' : journal_id } )

    #######################################################################################################
    # Column definitions
    #######################################################################################################
    _columns = { 'name'        : fields.char( "Nombre", size=30, required=True ),
                 'code'        : fields.char( "Codigo", size=3, required=True ),
                 'line_ids'    : fields.one2many( "altatec.payment.source.line", "source_id", "Lines",  ),
               }


#######################################################################################################
# altatec_payment_source_line class definition
#######################################################################################################
class altatec_payment_source_line( osv.osv ):

    _name = 'altatec.payment.source.line'

    #######################################################################################################
    #
    #######################################################################################################
    def onchange_type_id( self, cr, uid, ids, type_id, context=None):

        type = self.pool.get('altatec.payment.source.type').browse( cr, uid, type_id, context=context )

        return { 'value' : { 'is_out_check' : type.is_out_check,
                             'in_out'       : type.in_out,
                           }
               }

    #######################################################################################################
    #
    #######################################################################################################
    def onchange_journal_account_id(self, cr, uid, ids, journal_account_id, journal_id, context=None ):

        retval = { 'value' : {} }

        if( journal_id and journal_account_id ):
            journal = self.pool.get( 'account.journal' ).browse( cr, uid, journal_id, context=context )
            if( journal.default_debit_account_id.id != journal_account_id ):
                retval[ 'value' ][ 'journal_id' ] = False

        return retval

    #######################################################################################################
    #
    #######################################################################################################
    def onchange_journal_id(self, cr, uid, ids, journal_id, journal_account_id, type_id, context=None ):

        retval = { 'value' : {} }

        # Validate the journal
        if( journal_id and type_id ):

            retval['value']['journal_account_id'] = False

            journal = self.pool.get( 'account.journal' ).browse( cr, uid, journal_id, context=context )
            type    = self.pool.get( 'altatec.payment.source.type' ).browse( cr, uid, type_id, context=context )

            if( journal.type != type.type ):
                retval[ 'warning' ] = { 'title': "Error!", 'message': "El 'Tipo' del diario no es igual al 'Tipo' del 'Tipo'" }
                retval[ 'value'   ][ 'journal_id' ] = False
            elif( not journal.update_posted ):
                retval[ 'warning' ] = { 'title': "Error!", 'message': "El camp 'Permitir cancelacion de asientos' en el diario debe estar seleccionado" }
                retval[ 'value'   ][ 'journal_id' ] = False
            elif( not journal.allow_date ):
                retval[ 'warning' ] = { 'title': "Error!", 'message': "El campo 'Validar fecha en periodo' en el diario debe estar seleccionado" }
                retval[ 'value'   ][ 'journal_id' ] = False
            elif( not journal.entry_posted ):
                retval[ 'warning' ] = { 'title': "Error!", 'message': "El campo 'Omitir estado 'Borrador' para asientos manuales' en el diario debe estar seleccionado" }
                retval[ 'value'   ][ 'journal_id' ] = False
            elif( not journal.sequence_id ):
                retval[ 'warning' ] = { 'title': "Error!", 'message': "Diario debe tener una 'Secuencia del Asiento'" }
                retval[ 'value'   ][ 'journal_id' ] = False
            else:
                retval[ 'value' ]['journal_account_id' ] = journal.default_debit_account_id.id

        return retval

    #######################################################################################################
    #
    #######################################################################################################
    def create_journal(self, cr, uid, ids, context=None ):

        for line in self.browse( cr, uid, ids, context=context):

            journal_obj = self.pool.get('account.journal')

            # Calculate the journal's name and code
            journal_name = line.source_id.name + '-' + line.type_id.name
            journal_code = line.source_id.code + line.type_id.code


            journal_ids = journal_obj.search( cr, uid, ['|',('code','=',journal_code),('name','=',journal_name)], context=context )

            # A journal with this code already exists, verify it
            if( len( journal_ids ) > 0 ):
                raise osv.except_osv( "Error!", "No se puede crear el diario. Ya existe un diario con el codigo: "
                                      + journal_code + " o nombre: " + journal_name )

            # Create a sequence
            sequence_id = self.pool.get('ir.sequence').create( cr, uid,
                                                               { 'name'    : journal_name,
                                                                 'prefix'  : '%(year)s-' + journal_code + '-',
                                                                 'padding' : 6,
                                                                 'implementation' : 'no_gap',
                                                               }
                                                             )

            # Calculate the new journal's field values
            journal_field_vals = { 'name' : journal_name,
                                   'code' : journal_code,
                                   'type' : line.type_id.type,
                                   'default_debit_account_id'  : line.journal_account_id.id,
                                   'default_credit_account_id' : line.journal_account_id.id,
                                   'update_posted' : True,
                                   'allow_date'    : True,
                                   'entry_posted'  : True,
                                   'sequence_id'   : sequence_id,
                                 }

            # If it's an outgoing check, set these fields on the journal as well
            if( line.type_id.is_out_check ):
                journal_field_vals[ 'allow_check_writing' ] = True
                if( line.check_report_id ):
                    journal_field_vals[ 'report_check_id' ] = line.check_report_id.id
                else:
                    raise osv.except_osv( "Error!", "No se puede crear el diario. Tipo " + line.type_id.name
                                          + " es un cheque propio pero esta linea no tiene un 'Reporte del Cheque'" )

            # Create the journal
            journal_id = journal_obj.create( cr, uid, journal_field_vals )

            line.write( { 'journal_id' : journal_id } )


    #######################################################################################################
    # Column definition
    #######################################################################################################
    _columns = { "source_id"          : fields.many2one( "altatec.payment.source", "Source", ondelete='cascade' ),
                 "type_id"            : fields.many2one( "altatec.payment.source.type", "Tipo", required=True ),
                 "in_out"             : fields.related( 'type_id', 'in_out', type='selection', selection=[("in","Ingreso"),("out","Egreso")], string="Ingreso/Egreso" ),
                 "is_out_check"       : fields.related( 'type_id', 'is_out_check', type='boolean', string="Es cheque propio?" ),
                 "check_report_id"    : fields.many2one( "ir.actions.report.xml", "Reporte del Cheque" ),
                 "check_sequence_id"  : fields.many2one( "ir.sequence", "Secuencia de cheque" ),
                 "journal_account_id" : fields.many2one( "account.account", "Cuenta de diario", ),
                 "deposit_account_id" : fields.many2one( "account.account", "Cuenta de deposito" ),
                 "deposit_journal_id" : fields.many2one( "account.journal", "Diario de deposito" ),
                 "is_protected"       : fields.boolean( "Protected" ),
                 "journal_id"         : fields.many2one( "account.journal", "Diario" ),
               }


#######################################################################################################
# altatec_payment_source_type class definition
#######################################################################################################
class altatec_payment_source_type( osv.osv ):

    _name = 'altatec.payment.source.type'

    _columns = { "name"     : fields.char( "Nombre", size=30, required=True ),
                 "code"     : fields.char( "Codigo", size=2, required=True ),
                 "type"     : fields.selection( [ ('sale'           , 'Venta'                             ), #TODO: figure out which of these to remove
                                                  ('sale_refund'    , 'Abono ventas'                      ),
                                                  ('purchase'       , 'Compra'                            ),
                                                  ('purchase_refund', 'Factura rectificativa de proveedor'),
                                                  ('cash'           , 'Efectivo'                          ),
                                                  ('bank'           , 'Banco y cheques'                   ),
                                                  ('general'        , 'General'                           ),
                                                  ('Situation'      , 'Situacion apertura/cierre'         ),
                                                ], "Tipo", required=True,
                                              ),
                 "in_out"       : fields.selection( [("in","Ingreso"),("out","Egreso")], "Ingreso/Egreso", required=True ),
                 "is_out_check" : fields.boolean( "Es cheque propio?" ),
                 "report"       : fields.many2one( 'ir.actions.report.xml', 'Reporte Comprobante', required=True ),
               }