# -*- encoding: utf-8 -*-
#######################################################################################################
#
#######################################################################################################
from openerp.osv import fields, osv


#######################################################################################################
#
#######################################################################################################
class altatec_deposit_wizard(osv.osv_memory):

    _name = 'altatec.deposit.wizard'

    #######################################################################################################
    #
    #######################################################################################################
    def confirm_deposit(self, cr, uid, ids, context=None):

        deposit_obj   = self.pool.get("altatec.deposit")
        move_obj      = self.pool.get("account.move")
        move_line_obj = self.pool.get("account.move.line")
        voucher_obj   = self.pool.get("account.voucher")

        for wizard in self.browse( cr, uid, ids, context=context ):

            # Create the deposit document
            deposit_dict = {}
            if wizard.date:
                deposit_dict['date'] = wizard.date
            if wizard.reference:
                deposit_dict['reference'] = wizard.reference
            deposit_id = deposit_obj.create( cr, uid, deposit_dict, context=context)
            deposit    = deposit_obj.browse(cr, uid, deposit_id, context=context)

            for payment in wizard.payment_lines:

                if not payment.deposit_account:
                    raise osv.except_osv("Error", "Debe eligir la cuenta de deposito para pago:" + str(payment.number))

                payment.voucher_id.write({ 'deposit_id' : deposit_id })

                # Make the account move
                account_move_dict = { 'deposit_id' : deposit_id,
                                      'name'       : deposit.name,
                                      'journal_id' : payment.deposit_journal.id,
                                      'period_id'  : voucher_obj._get_period(cr, uid, context=context),
                                      'payment_source_type_id' : payment.voucher_id.payment_source_type_id.id,
                                    }
                deposit_move_id = move_obj.create(cr, uid, account_move_dict, context=context)

                # Get the voucher's account move line with a debit
                debit_line = False
                for move_line in payment.voucher_id.move_ids:
                    if move_line.debit > 0.001: #TODO: We should use more criteria for finding this line
                        debit_line = move_line
                        break

                # Make the move lines
                move_line_dict = { 'move_id'    : deposit_move_id,
                                   'name'       : '/',
                                   'account_id' : debit_line.account_id.id,
                                   'debit'      : 0.00,
                                   'credit'     : debit_line.debit,
                                   'partner_id' : debit_line.partner_id.id,
                                 }
                move_line_obj.create(cr, uid, move_line_dict, context=context)

                move_line_dict = { 'move_id'    : deposit_move_id,
                                   'name'       : account_move_dict['name'],
                                   'account_id' : payment.deposit_account.id,
                                   'debit'      : debit_line.debit,
                                   'credit'     : 0.00,
                                   # 'partner_id' : , #TODO: Figure this out
                                 }
                move_line_obj.create(cr, uid, move_line_dict, context=context)

            # Confirm the deposit
            deposit_obj.confirm_deposit(cr, uid, [deposit_id], context=context)

            return { 'type'      : 'ir.actions.act_window',
                     'name'      : 'Deposito',
                     'view_type' : 'form',
                     'view_mode' : 'form,tree',
                     'res_model' : 'altatec.deposit',
                     'res_id'    : deposit_id
                   }

    #######################################################################################################
    #
    #######################################################################################################
    def view_init(self, cr, uid, fields, context=None):

        if not context or 'active_ids' not in context:
            raise osv.except_osv("Programming Error", "altatec.deposit.wizard view_init() Context not found or not correct")

        for voucher in self.pool.get('account.voucher').browse(cr, uid, context['active_ids'], context=context):

            if voucher.type != 'receipt':
                raise osv.except_osv("Error!", "Solo se puede hacer depositos con pagos de cliente")

            if voucher.state != 'posted':
                raise osv.except_osv("Error!", "Solo se puede hacer depositos con pagos de cliente que estan el es estado 'Contabilizado'.")

            if voucher.deposit_state == 'deposited':
                raise osv.except_osv("Error!", "Pago: " + str(voucher.number) + " ya esta depositado.")

    #######################################################################################################
    #
    #######################################################################################################
    def _get_payment_lines(self, cr, uid, context=None):

        source_obj = self.pool.get('altatec.payment.source')

        if not context or 'active_ids' not in context:
            raise osv.except_osv("Programming Error", "altatec.deposit.wizard _get_payment_lines() Context not found or not correct")

        res = []
        for voucher in self.pool.get('account.voucher').browse(cr, uid, context['active_ids'], context=context):
            payment_dict = { 'voucher_id'   : voucher.id,
                             "date"         : voucher.date,
                             "number"       : voucher.number,
                             "check_number" : voucher.check_number,
                             "payee_name"   : voucher.payee_name,
                             "reference"    : voucher.reference,
                             "partner_id"   : voucher.partner_id.id,
                             "journal_id"   : voucher.journal_id.id,
                             "amount"       : voucher.amount,
                           }
            if voucher.payment_source_id:
                source_id   = voucher.payment_source_id.id
                type_id     = voucher.payment_source_type_id.id
                source_line = source_obj._get_line_from_type_id(cr, uid, source_id, type_id, context)
                payment_dict["deposit_account"] = source_line.deposit_account_id.id
                payment_dict["deposit_journal"] = source_line.deposit_journal_id.id
                payment_dict["payment_source_id"] = voucher.payment_source_id.id
                payment_dict["payment_source_type_id"] = voucher.payment_source_type_id.id

            res.append( (0, 0, payment_dict) )

        return res

    #######################################################################################################
    # Column and default definitions
    #######################################################################################################
    _columns = { "payment_lines" : fields.one2many("altatec.deposit.wizard.line", "wizard_id", "Pagos a depositar"),
                 "date"          : fields.date("Fecha de deposito"),
                 "reference"     : fields.char("Referencia"),
               }

    _defaults = { "payment_lines" : _get_payment_lines,
                }

#######################################################################################################
#
#######################################################################################################
class altatec_deposit_wizard_line(osv.osv_memory):

    _name = 'altatec.deposit.wizard.line'

    _columns = { "wizard_id"       : fields.many2one("altatec.deposit.wizard", "Wizard ID"),
                 "voucher_id"      : fields.many2one("account.voucher", "Pago" ),
                 "date"            : fields.date("Fecha"),
                 "number"          : fields.char("Numero"),
                 "check_number"    : fields.char("Numero de Cheque"),
                 "payee_name"      : fields.char("Nombre del Beneficiario"),
                 "reference"       : fields.char("Ref. #"),
                 "partner_id"      : fields.many2one("res.partner", "Empresa"),
                 "journal_id"      : fields.many2one("account.journal", "Diario"),
                 "amount"          : fields.float("Total"),
                 "deposit_account" : fields.many2one("account.account", "Cuenta de deposito"),
                 "deposit_journal" : fields.many2one("account.journal", "Diario de deposito"),
                 "payment_source_id"      : fields.many2one("altatec.payment.source", "Concepto de pago"),
                 "payment_source_type_id" : fields.many2one("altatec.payment.source.type", "Metodo de concepto"),
               }