{ 'name'         : 'AltaTec Migrate Production Products',
  'version'      : '1.0',
  'description'  : """
                   This module copies the product_id, qty, and unit to the product_ids field on mrp.production.
                   """,
  'author'       : 'Dan Haggerty',
  'website'      : 'www.altatec.ec',
  "depends"      : [ 'altatec_production_improvements',
                     'mrp',
                   ],
  "data"         : [
                   ],
  "installable"  : True,
  "auto_install" : False
}
