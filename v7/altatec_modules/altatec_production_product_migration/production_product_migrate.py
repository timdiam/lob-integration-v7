from openerp import tools, pooler
from openerp.osv import fields,osv

class production_product_migrate(osv.osv):
    _name = "production.product.migrate"
    _auto = False

    def init(self, cr):
        pool = pooler.get_pool(cr.dbname)
        uid = 1

        production_ids = pool.get('mrp.production').search(cr, uid, [])
        for production in pool.get('mrp.production').browse(cr, uid, production_ids):
            if not production.product_ids or not len(production.product_ids):
                production.write({ 'product_ids' : [(0, 0, {'product_id'  : production.product_id.id,
                                                            'product_qty' : production.product_qty,
                                                            'uom_id'      : production.product_uom.id,
                                                           })]
                                 })