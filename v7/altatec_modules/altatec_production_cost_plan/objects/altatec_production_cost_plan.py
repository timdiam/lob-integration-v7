# -*- encoding: utf-8 -*-
#########################################################################
# Author:  Jonathan Bravo @jbravot
# Company: AltaTec Ecuador
# Date:    04/06/2015
########################################################################

from openerp.osv import fields, osv, orm

########################################################################
# Class Altatec Production Cost Type
########################################################################
class altatec_production_cost_type(osv.osv):

    _name = 'altatec.production.cost.type'
    _columns = {
        'name': fields.char('Nombre', size=64, required=True),
    }

########################################################################
# Class Altatec Production Cost Plan Line
########################################################################
class altatec_production_cost_plan_line(osv.osv):

    #---------------------------------------------------------------------
    def _getDiference( self, cr, uid, ids, field_name, arg, context={} ):
        res = {}
        for object in self.browse( cr, uid, ids, context=context ):
            diference = object.real - object.estimate
            res[object.id] = diference

        return res

    #---------------------------------------------------------------------
    _name = 'altatec.production.cost.plan.line'
    _columns = {
        'production_cost_type': fields.many2one('altatec.production.cost.type', u'Tipo de Costo de Producción', required=True),
        'estimate': fields.float('Costo estimado', required=True),
        'real': fields.float('Costo real', required=True),
        'diference': fields.function(
            fnct=_getDiference,
            type="float",
            method=True,
            string="Diferencia de costo",
            help="Diferencia entre el costo estimado y el costo real"
        ),
        'altatec_production_cost_plan': fields.many2one('altatec.production.cost.plan', 'Plan de costo', ondelete='cascade'),
    }

########################################################################
# Class Altatec Production Cost Plan
########################################################################
class altatec_production_cost_plan(osv.osv):

    #---------------------------------------------------------------------
    _name = 'altatec.production.cost.plan'
    _columns = {
        'period': fields.many2one('account.period', u'Periodo', required=True),
        'state': fields.selection( [('draft','Borrador'), ('confirm','Confirmado'), ('close','Cerrado')], string="Estado", required=True),
        'description': fields.char(u'Descripción'),
        'production_cost_plan_line': fields.one2many('altatec.production.cost.plan.line','altatec_production_cost_plan', u'Linea de Costo'),
        'production_distribution_instance': fields.one2many('altatec.production.distribution.instance','altatec_production_cost_plan', u'Disitribución'),
        'production_cost_workcenter': fields.one2many('altatec.production.cost.workcenter','altatec_production_cost_plan', u'Tiempos por Centro'),
    }
    _sql_constraints = [
        ('period_uniq', 'unique (period)', 'Ya existe un Plan de Costo con este periodo, por favor corregir')
    ]

    #---------------------------------------------------------------------
    # Metodo para cargar el period actual
    #---------------------------------------------------------------------
    def _get_default_period(self, cr, uid, context=None):
        account_obj = self.pool.get('account.period')
        return account_obj.find(cr, uid, context=context)

    #---------------------------------------------------------------------
    # Metodo para cargar los altatec.production.cost.plan.line
    #---------------------------------------------------------------------
    def _get_default_cost_plan_lines(self, cr, uid, context=None):
        cost_plan_lines = []
        cost_type_obj = self.pool.get('altatec.production.cost.type')
        cost_type_ids = cost_type_obj.search(cr, uid, [], context=context)

        if( len(cost_type_ids) ):
            cost_types = cost_type_obj.browse(cr, uid, cost_type_ids, context=context)
            for cost_type in cost_types:
                cost_plan_line = {
                    'production_cost_type':cost_type.id,
                    'estimate':0.00,
                    'real':0.00,
                }
                cost_plan_lines.append(cost_plan_line)

        return cost_plan_lines

    #---------------------------------------------------------------------
    # Metodo para cargar los altatec.production.distribution.instance
    #---------------------------------------------------------------------
    def _get_default_distribution_instances(self, cr, uid, context=None):
        distribution_instances = []
        cost_type_obj = self.pool.get('altatec.production.cost.type')
        cost_type_ids = cost_type_obj.search(cr, uid, [], context=context)

        if( len(cost_type_ids) ):
            cost_types = cost_type_obj.browse(cr, uid, cost_type_ids, context=context)
            for cost_type in cost_types:
                distribution_instance = {
                    'production_cost_type':cost_type.id,
                    'equal_distribution':True,
                }
                distribution_instances.append(distribution_instance)

        return distribution_instances

    #---------------------------------------------------------------------
    # Metodo para cargar los altatec.production.cost.workcenter
    #---------------------------------------------------------------------
    def _get_default_cost_workcenter(self, cr, uid, context=None):
        cost_workcenters = []
        workcenter_obj = self.pool.get('mrp.workcenter')
        workcenter_ids = workcenter_obj.search(cr, uid, [], context=context)

        if( len(workcenter_ids) ):
            workcenters = workcenter_obj.browse(cr, uid, workcenter_ids, context=context)
            for workcenter in workcenters:
                cost_workcenter = {
                    'workcenter': workcenter.id,
                    'cost': 0.00,
                    'hours': 0.00
                }
                cost_workcenters.append(cost_workcenter)

        return cost_workcenters

    #---------------------------------------------------------------------
    def cost_confirm(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'confirm'})

    #---------------------------------------------------------------------
    def cost_close(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'close'})

    #---------------------------------------------------------------------
    # Metodo para calcular los costos por workcenter
    #---------------------------------------------------------------------
    def calc_cost_workcenter(self, cr, uid, ids, context=None):
        cost_plan = self.browse(cr, uid, ids)[0]

        production_cost_plan_lines = cost_plan.production_cost_plan_line
        distribution_instances = cost_plan.production_distribution_instance
        cost_workcenters = cost_plan.production_cost_workcenter

        num_cost_workcenters = float(len(cost_workcenters))
        cost_workcenter_obj = self.pool.get('altatec.production.cost.workcenter')

        #por cada centro de trabajo que existe
        for cost_workcenter in cost_workcenters:
            total_cost = 0.00

            #Primero borramos las relaciones production_workcenter_cost_type_line si existen
            cost_workcenter_obj.deleteLine(cr, uid, [cost_workcenter.id], cost_workcenter_obj, cost_workcenter, context)

            #recorremos los production_cost_plan_line y tambien las distribution_instances
            for production_cost_plan_line in production_cost_plan_lines:
                for distribution_instance in distribution_instances:
                    #preguntamos si coiciden los production_cost_type
                    if production_cost_plan_line.production_cost_type == distribution_instance.production_cost_type:
                        cost = 0.00
                        if distribution_instance.equal_distribution:
                            #calculamos el costo para distribuciones iguales
                            cost = (production_cost_plan_line.estimate/num_cost_workcenters)
                        else:
                            #si la distribucion es diferente, recorremos cada distribucion
                            for distribution_line in distribution_instance.production_distribution.production_distribution_line:
                                #preguntamos si la distribucion es de mi cost_workcenter actual
                                if distribution_line.workcenter.id == cost_workcenter.id:
                                    #calculamos el costo segun la distribucion
                                    cost = (production_cost_plan_line.estimate * (distribution_line.percentage / 100.00))

                                    #salimos del for
                                    break
                        #guaramos un nuevo production_workcenter_cost_type_line en la base
                        self._saveWorkcenterCostTypeLine(cr, uid, cost_workcenter.id, production_cost_plan_line.production_cost_type.id, cost)

                        #actualizamos el costo total
                        total_cost += cost

                        #salimos del for
                        break

            cost_workcenter_obj.write(cr, uid, [cost_workcenter.id], {'cost': total_cost}, context=context)

    #---------------------------------------------------------------------
    # Metodo para guardar ltatec_production_workcenter_cost_type_line
    #---------------------------------------------------------------------
    def _saveWorkcenterCostTypeLine(self, cr, uid, cost_workcenter_id, cost_type_id, cost):
        self.pool.get('altatec.production.workcenter.cost.type.line').create(cr, uid,{
                                'cost_type': cost_type_id,
                                'cost': cost,
                                'production_cost_workcenter': cost_workcenter_id})

    #---------------------------------------------------------------------
    # Metodo para obtener los altatec.production.cost.plan por Periodo
    #---------------------------------------------------------------------
    def getCostPlanByPeriod(self, cr, uid, period_id, context):
        cost_plan_ids = self.search(cr, uid, [('period','=', period_id)], context=context)
        for cost_plan_obj in self.browse(cr, uid, cost_plan_ids, context=context):
           return cost_plan_obj

        return None

    #---------------------------------------------------------------------
    # Metodo para obtener los altatec.production.cost.plan.line por Periodo
    #---------------------------------------------------------------------
    def getCostPlanLinesByPeriod(self, cr, uid, period_id, context):
        production_cost_plan_lines  =[]
        cost_plan_ids = self.search(cr, uid, [('period','=', period_id)], context=context)
        for cost_plan_obj in self.browse(cr, uid, cost_plan_ids, context=context):
            production_cost_plan_lines = cost_plan_obj.production_cost_plan_line

        return production_cost_plan_lines

    #----------------------------------------------------------------------
    _defaults = {
        'state': 'draft',
        'period': _get_default_period,
        'production_cost_plan_line': _get_default_cost_plan_lines,
        'production_distribution_instance': _get_default_distribution_instances,
        'production_cost_workcenter': _get_default_cost_workcenter,
    }