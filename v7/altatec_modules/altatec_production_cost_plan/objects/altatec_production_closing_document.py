# -*- encoding: utf-8 -*-
#########################################################################
# Author:  Jonathan Bravo @jbravot
# Company: AltaTec Ecuador
# Date:    09/06/2015
########################################################################

from openerp.osv import fields, osv, orm

########################################################################
# Class Altatec Production Cost Type Instance Line
########################################################################
class altatec_production_cost_type_instance_line(osv.osv):

    _name = 'altatec.production.cost.type.instance.line'
    _columns = {
        'cost_type': fields.many2one('altatec.production.cost.type', 'Tipo costo'),
        'cost_hours': fields.float('Costo por Hora'),
        'cost': fields.float('Costo'),
        'production_cost_instance_line': fields.many2one('altatec.production.cost.instance.line', 'Cost intance Line', ondelete='cascade'),
    }

########################################################################
# Class Altatec Production Cost Instance Line
########################################################################
class altatec_production_cost_instance_line(osv.osv):

    _name = 'altatec.production.cost.instance.line'
    _columns = {
        'workcenter': fields.many2one('mrp.workcenter', 'Centro de Trabajo'),
        'hours': fields.float('Horas'),
        'cost_hours': fields.float('Costo por Hora'),
        'cost': fields.float('Costo Total'),
        'production_cost_type_instance_line': fields.one2many('altatec.production.cost.type.instance.line', 'production_cost_instance_line', 'Tipo costo'),
        'production_closing_document_line': fields.many2one('altatec.production.closing.document.line', 'Closing Document Line', ondelete='cascade'),
    }

########################################################################
# Class Altatec Production Closing Document Line
########################################################################
class altatec_production_closing_document_line(osv.osv):

    _name = 'altatec.production.closing.document.line'

    _columns = {
        'production': fields.many2one('mrp.production', u'Producción',),
        'state': fields.selection( [('draft','Nuevo'), ('cancel','Cancelada'), ('picking_except',u'Excepción de acopio'), ('confirmed','Esperando materia prima'),
                                    ('ready','Listo para fabricar'), ('in_production','En proceso'), ('done','Finalizada')], string="Estado" ),
        'production_cost_instance_line': fields.one2many('altatec.production.cost.instance.line', 'production_closing_document_line', u'Orden de Producción'),
        'production_closing_document': fields.many2one('altatec.production.closing.document', 'Documento de cierre', ondelete='cascade'),

    }

########################################################################
# Class Altatec Production Closing Document
########################################################################
class altatec_production_closing_document(osv.osv):

    _name = 'altatec.production.closing.document'
    _columns = {
        'period': fields.many2one('account.period', u'Periodo', required=True ),
        'state': fields.selection( [('draft','Borrador'), ('confirm','Confirmado')], string="Estado" ),
        'production_closing_document_line': fields.one2many('altatec.production.closing.document.line', 'production_closing_document', u'Orden de Producción'),
    }

    _sql_constraints = [
        ('period_uniq', 'unique (period)', 'Ya existe un Docuemnto de cierre con este periodo, por favor corregir!')
    ]

    #---------------------------------------------------------------------
    # Metodo para cargar el period actual
    #---------------------------------------------------------------------
    def _get_default_period(self, cr, uid, context=None):
        account_obj = self.pool.get('account.period')
        return account_obj.find(cr, uid, context=context)

    #---------------------------------------------------------------------
    # Metodo para cambiar el estado a 'Confirmado'
    #---------------------------------------------------------------------
    def document_confirm(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'confirm'})

    #---------------------------------------------------------------------
    # metodo que cargar las ordenes de trabajo de un periodo especifico
    #---------------------------------------------------------------------
    def document_op(self, cr, uid, ids, context=None):
        document = self.browse(cr, uid, ids)[0]
        periodo_id = document.period.id

        cost_plan_period = self.pool.get('altatec.production.cost.plan').getCostPlanByPeriod(cr, uid, periodo_id, context)

        #si NO existe un plan de costo
        if cost_plan_period == None:
            raise osv.except_osv(('Advertencia!'), ('No exite un Plan de Costo para este Periodo'))

        #si existe
        else:
            mrp_production_obj = self.pool.get('mrp.production')
            mrp_production_ids = mrp_production_obj.search(cr, uid, [('workcenter_lines.horas_mes.periodo_id','=', periodo_id)], context=context)
            mrp_productions_dict = mrp_production_obj.get_time_per_cost_center(cr, uid, mrp_production_ids, periodo_id)

            #Si ya existen datos los borrramos
            if len(document.production_closing_document_line) > 0:
               self._deleteClosingDocumentLine(cr, uid, ids, document, context)

            #empesamos a recorrer el diccionario
            for production in mrp_productions_dict:
                self._saveClosingDocumentLine(cr, uid, production, document.id, mrp_production_obj, cost_plan_period.id, context)

        return True

    #---------------------------------------------------------------------
    # Metodo para borrar las altatec.production.closing.document.line si existen
    #---------------------------------------------------------------------
    def _deleteClosingDocumentLine(self, cr, uid, ids, document, context):
        closing_document_lines = []
        for closing_document_line in document.production_closing_document_line:
            closing_document_lines.append((3, closing_document_line.id))

            self.write(cr, uid, ids, {'production_closing_document_line': closing_document_lines}, context=context)

    #---------------------------------------------------------------------
    # Metodo para guardar altatec.production.closing.document.line
    #---------------------------------------------------------------------
    def _saveClosingDocumentLine(self, cr, uid, production, document_id, mrp_production_obj, cost_plan_id, context):
        for production_id, workcenters in production.items():
            op = mrp_production_obj.browse(cr, uid, production_id)

            closing_document_line_id = self.pool.get('altatec.production.closing.document.line').create(cr, uid,{
                                    'production': production_id,
                                    'state': op.state,
                                    'production_closing_document': document_id})

            self._saveCostInstanceLine(cr, uid, workcenters, closing_document_line_id, cost_plan_id, context)

    #---------------------------------------------------------------------
    # Metodo para guardar altatec.production.cost.instance.line
    #---------------------------------------------------------------------
    def _saveCostInstanceLine(self, cr, uid, workcenters, closing_document_line_id, cost_plan_id, context):
        for workcenter_id, hours_workcenter in workcenters.items():
            cost_workcenter = self.pool.get('altatec.production.cost.workcenter').getByCostPlanWorkercenter(cr, uid, cost_plan_id, workcenter_id, context)
            cost_hours = round(cost_workcenter.cost_hours,2)

            cost_instance_line_id = self.pool.get('altatec.production.cost.instance.line').create(cr, uid,{
                                    'workcenter': workcenter_id,
                                    'hours': hours_workcenter,
                                    'cost_hours': cost_hours,
                                    'cost': cost_hours * hours_workcenter,
                                    'production_closing_document_line': closing_document_line_id})

            self._saveCostTypeInstanceLine(cr, uid, cost_instance_line_id, cost_workcenter, hours_workcenter)

    #---------------------------------------------------------------------
    # Metodo para guardar altatec.production.cost.type.instance.line
    #---------------------------------------------------------------------
    def _saveCostTypeInstanceLine(self, cr, uid, cost_instance_line_id, cost_workcenter, hours_workcenter):
        for workcenter_cost_type_line in cost_workcenter.production_workcenter_cost_type_line:
            cost_hours_line = workcenter_cost_type_line.cost/cost_workcenter.hours

            cost_hours_line = round(cost_hours_line,2)

            self.pool.get('altatec.production.cost.type.instance.line').create(cr, uid,{
                                'cost_type': workcenter_cost_type_line.cost_type.id,
                                'cost_hours': cost_hours_line,
                                'cost': cost_hours_line * hours_workcenter,
                                'production_cost_instance_line': cost_instance_line_id})

    #------------------------------------------------------------------------
    _defaults = {
        'period': _get_default_period,
        'state': 'draft',
    }