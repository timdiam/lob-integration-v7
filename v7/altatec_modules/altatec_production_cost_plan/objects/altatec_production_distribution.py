# -*- encoding: utf-8 -*-
#########################################################################
# Author:  Jonathan Bravo @jbravot
# Company: AltaTec Ecuador
# Date:    11/06/2015
########################################################################

from openerp.osv import fields, osv, orm

########################################################################
# Class Altatec Production Distribution Line
########################################################################
class altatec_production_distribution_line(osv.osv):

    _name = 'altatec.production.distribution.line'
    _columns = {
        'workcenter': fields.many2one('mrp.workcenter', u'Centro de Trabajo', required=True),
        'percentage': fields.float('Porcentaje', required=True),
        'altatec_production_distribution': fields.many2one('altatec.production.distribution', u'Distribución'),
    }

    #---------------------------------------------------------------------
    # Metodo para validar que el valor ingresado este entre 0 y 100
    #---------------------------------------------------------------------
    def _check_percentage(self, cr, uid, ids, context=None):

        for distribution_line in self.browse(cr, uid, ids, context=context):
            if distribution_line.percentage > 100.00 or distribution_line.percentage < 0.00:
                    return False
        return True

    _constraints = [
        (_check_percentage, 'Advertencia!\nEl campo porcentaje solo acepta valores entre 0 y 100.', ['percentage']),
    ]



########################################################################
# Class Altatec Production Distribution
########################################################################
class altatec_production_distribution(osv.osv):

    #---------------------------------------------------------------------
    def _distributionName( self, cr, uid, ids, name, arg, context={} ):

        res = {}
        name_new = ""
        for production_distribution in self.browse( cr, uid, ids, context=context ):

            for distribution_line in production_distribution.production_distribution_line:
                if name_new == "":
                     name_new = "%s (%s)" % (str(distribution_line.workcenter.name), str(distribution_line.percentage))
                else:
                    name_new = "%s / %s (%s)" % (name_new, str(distribution_line.workcenter.name), str(distribution_line.percentage))

            res[production_distribution.id] = name_new

        return res

    #---------------------------------------------------------------------
    _name = 'altatec.production.distribution'
    _columns = {
        'name': fields.function(_distributionName, type='char', size=500, string="Nombre", store=True),
        'production_distribution_line': fields.one2many('altatec.production.distribution.line', 'altatec_production_distribution',  u'Linea de Distribución'),
    }

    #---------------------------------------------------------------------
    # Metodo para cargar los altatec.production.distribution.line
    #---------------------------------------------------------------------
    def _get_default_distribution_lines(self, cr, uid, context=None):
        distribution_lines = []
        workcenter_obj = self.pool.get('mrp.workcenter')
        workcenter_ids = workcenter_obj.search(cr, uid, [], context=context)

        if( len(workcenter_ids) ):
            workcenters = workcenter_obj.browse(cr, uid, workcenter_ids, context=context)
            for workcenter in workcenters:
                distribution_line = {'workcenter': workcenter.id,'percentage':0.00}
                distribution_lines.append(distribution_line)

        return distribution_lines

    _defaults = {
        'production_distribution_line': _get_default_distribution_lines
    }


########################################################################
# Class Altatec Production Distribution Instance
########################################################################
class altatec_production_distribution_instance(osv.osv):

    _name = 'altatec.production.distribution.instance'
    _columns = {
        'production_cost_type': fields.many2one('altatec.production.cost.type', u'Tipo de Costo de Producción', required=True),
        'equal_distribution': fields.boolean(u'Distribución Equitativa'),
        'production_distribution': fields.many2one('altatec.production.distribution', u'Distribución', ondelete='cascade'),
        'altatec_production_cost_plan': fields.many2one('altatec.production.cost.plan', 'Plan de costo', ondelete='cascade'),
    }

    _defaults = {
        'equal_distribution': True
    }