# -*- encoding: utf-8 -*-
#########################################################################
# Author:  Jonathan Bravo @jbravot
# Company: AltaTec Ecuador
# Date:    04/06/2015
########################################################################

from openerp.osv import fields, osv, orm

########################################################################
# Class Altatec Production Cost Type Instance Line
########################################################################
class altatec_production_workcenter_cost_type_line(osv.osv):

    _name = 'altatec.production.workcenter.cost.type.line'
    _columns = {
        'cost_type': fields.many2one('altatec.production.cost.type', 'Tipo costo'),
        'cost': fields.float('Costo total'),
        'cost_hours': fields.float('Costo por Hora'),
        'production_cost_workcenter': fields.many2one('altatec.production.cost.workcenter', 'Cost intance Line', ondelete='cascade'),
    }

########################################################################
# Class Altatec Production Distribution Instance
########################################################################
class altatec_production_cost_workcenter(osv.osv):

    #---------------------------------------------------------------------
    def _getCostHours( self, cr, uid, ids, field_value, arg, context={} ):
        res = {}
        for object in self.browse( cr, uid, ids, context=context ):
            cost_hours = 0.00

            if object.hours > 0.00:
                cost_hours = object.cost / object.hours
                res[object.id] = cost_hours

            res[object.id] = cost_hours
            #actualizamos el costo hora de cada linea
            for workcenter_cost_type_line in object.production_workcenter_cost_type_line:
                cost_hours = workcenter_cost_type_line.cost / object.hours
                self.pool.get('altatec.production.workcenter.cost.type.line').write(cr, uid, [workcenter_cost_type_line.id], {'cost_hours': cost_hours}, context=context)

        return res

    _name = 'altatec.production.cost.workcenter'
    _columns = {
        'workcenter': fields.many2one('mrp.workcenter', u'Centro de Trabajo', required=True),
        'cost': fields.float('Total'),
        'hours': fields.float('Horas Estimadas'),
        'cost_hours': fields.function(
            fnct=_getCostHours,
            type="float",
            method=True,
            string="Costo por Hora",
        ),
        'production_workcenter_cost_type_line': fields.one2many('altatec.production.workcenter.cost.type.line', 'production_cost_workcenter', 'Tipo costo'),
        'altatec_production_cost_plan': fields.many2one('altatec.production.cost.plan', 'Plan de costo', ondelete='cascade'),
    }

    #---------------------------------------------------------------------
    def openForm(self, cr, uid, ids, context=None):

        return { 'type'      : 'ir.actions.act_window',
                 'view_type' : 'form',
                 'view_mode' : 'form,tree',
                 'res_model' : 'altatec.production.cost.workcenter',
                 'res_id'    : ids[0],
                 'target': 'new',
                 'name': 'Costos detallados',
               }

    #---------------------------------------------------------------------
    # Metodo para borrar las lineas si existen
    #---------------------------------------------------------------------
    def deleteLine(self, cr, uid, ids, cost_workcenter_obj, object, context):
        delete_lines = []

        for obj_line in object.production_workcenter_cost_type_line:
            delete_lines.append((3, obj_line.id))
            cost_workcenter_obj.write(cr, uid, ids, {'production_workcenter_cost_type_line': delete_lines}, context=context)

    #---------------------------------------------------------------------
    # Metodo para obtener el costo por hora
    #---------------------------------------------------------------------
    def getByCostPlanWorkercenter(self, cr, uid, cost_plan_id, workcenter_id, context):

        cost_workcenter_ids = self.search(cr, uid, [('altatec_production_cost_plan','=', cost_plan_id),
                                         ('workcenter','=', workcenter_id)], context=context)
        cost_workcenters = self.browse(cr, uid, cost_workcenter_ids, context=context)
        for cost_workcenter in cost_workcenters:
            return cost_workcenter

        return None