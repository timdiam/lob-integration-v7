# -*- encoding: utf-8 -*-
#########################################################################
# Author:  Jonathan Bravo @jbravot
# Company: AltaTec Ecuador
# Date:    04/06/2015
########################################################################
{
    'name': 'Altatec Production Cost Plan',
    'version': '1.0',
    'description': """
        Altatec Production Cost Plan
    """,
    'author': 'Jonathan Bravo',
    'website': 'www.altatec.ec',
    "depends": [
        'mrp',
        'account',
        'altatec_production_improvements'
    ],
    "data": [
        'views/altatec_production_cost_plan_view.xml',
        'views/altatec_production_closing_document_view.xml',
        'views/altatec_production_distribution_view.xml',
        'views/altatec_production_cost_workcenter_view.xml',
        'views/menu_view.xml',
    ],
    "installable": True,
    "auto_install": False
}