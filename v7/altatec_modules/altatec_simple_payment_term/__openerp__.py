{ 'name'         : 'AltaTec Simple Payment Terms',
  'version'      : '1.0',
  'description'  : """
                   This module adds a "Simple Payment Terms" option to payment terms. This adds the payment terms:
                   Monthly, Quincenal, and Weekly.
                   """,
  'author'       : 'Dan Haggerty',
  'website'      : 'www.altatec.ec',
  "depends"      : [ 'account',
                   ],
  "data"         : [ 'account_payment_term.xml',
                   ],
  "installable"  : True,
  "auto_install" : False
}
