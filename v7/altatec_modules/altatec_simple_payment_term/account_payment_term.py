# -*- coding: utf-8 -*-
#################################################################################
#
# This module adds a "Simple Payment Terms" option to payment terms. This adds the
# payment terms: Monthly, Quincenal, and Weekly.
#
# Author:  Dan Haggerty
# Company: AltaTec
# Date:    March 5th, 2014
#
#################################################################################
from openerp.osv import fields, osv, orm

from openerp.tools.float_utils import float_round as round
from datetime import datetime
from dateutil.relativedelta import relativedelta
import openerp.addons.decimal_precision as dp

#################################################################################
# account.payment.term class definition
#################################################################################
class account_payment_term(orm.Model):

    _inherit = 'account.payment.term'

    #################################################################################
    # write() override
    #################################################################################
    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}
        if not ids:
            return True
        if isinstance(ids, (int, long)):
            ids = [ids]

        for id in ids:
            payment_term = self.browse( cr, uid, id, context )

            if( 'type' in vals ):
                type = vals[ 'type' ]
            else:
                type = payment_term.type

            if( type != 'advanced' ):
                if( 'num_payments' in vals ):
                    if( vals['num_payments'] < 1 ):
                        raise osv.except_osv( 'Error', "El campo 'Numero de Pagos' debe ser > 0" )
                if( 'num_periods_post_date' in vals ):
                    if( vals['num_periods_post_date'] < 0 ):
                        raise osv.except_osv( 'Error', "El campo 'Numero de periodos postfechados' debe ser >= 0" )

        return super( account_payment_term, self ).write( cr, uid, ids, vals, context )


    #################################################################################
    # create() override
    #################################################################################
    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}

        if( vals['type'] != 'advanced' ):
            if( vals['num_payments'] < 1 ):
                raise osv.except_osv( 'Error', "El campo 'Numero de Pagos' debe ser > 0" )
            if( vals['num_periods_post_date'] < 0 ):
                raise osv.except_osv( 'Error', "El campo 'Numero de periodos postfechados' debe ser >= 0" )

        return super( account_payment_term, self ).create( cr, uid, vals, context )


    #################################################################################
    # compute() override
    #################################################################################
    def compute(self, cr, uid, id, value, date_ref=False, context=None):

        # Get start date, precision, this payment_term
        if not date_ref:
            date_ref = datetime.now().strftime('%Y-%m-%d')
        payment_term = self.browse(cr, uid, id, context=context)
        ref_date     = datetime.strptime(date_ref, '%Y-%m-%d')
        precision    = self.pool.get('decimal.precision').precision_get(cr, uid, 'Account')

        if payment_term.type == 'advanced':
            return super( account_payment_term, self ).compute( cr, uid, id, value, date_ref, context )
        else:

            weekdaydict = { 0 : 'monday',
                            1 : 'tuesday',
                            2 : 'wednesday',
                            3 : 'thursday',
                            4 : 'friday',
                            5 : 'saturday',
                            6 : 'sunday',
                          }

            # Error checking on the account.payment.term
            if( payment_term.num_payments < 1 ):
                raise osv.except_osv( 'Error', "El campo 'Numero de Pagos' en los Terminos de Pago debe ser > 0" )
            if( payment_term.num_periods_post_date < 0 ):
                raise osv.except_osv( 'Error', "El campo 'Numero de periodos postfechados' en los Terminos de Pago debe ser >= 0" )

            # First we generate a list of possible payment dates
            dates = []
            last_date = ref_date
            for i in range( 0, payment_term.num_payments + payment_term.num_periods_post_date ):

                if( payment_term.type == 'monthly' ):

                    if( payment_term.monthly_option == 'first_of_month' ):
                        if( i == 0 and ref_date.day == 1 ):
                            dates.append( ref_date )
                        else:
                            next_date = last_date + relativedelta( months=1 )
                            next_date = next_date.replace( day = 1 )
                            dates.append( next_date )
                            last_date = next_date
                    else:
                         if( i == 0 and ref_date.day == 28 ):
                            dates.append( ref_date )
                         else:
                            if( last_date.day < 28 ):
                                next_date = last_date.replace( day = 28 )
                                dates.append( next_date )
                                last_date = next_date
                            else:
                                next_date = last_date + relativedelta( months=1 )
                                next_date = next_date.replace( day = 28 )
                                dates.append( next_date )
                                last_date = next_date

                elif( payment_term.type == 'biweekly' ):

                    # If the initial date (ref_date) falls on the 15th or 28th, start with this date
                    if( i == 0 and ( ref_date.day == 15 or ref_date.day == 28 ) ):
                        dates.append( ref_date )
                    else:
                        if( last_date.day < 15 ):
                            next_date = last_date.replace( day = 15 )
                            dates.append( next_date )
                            last_date = next_date

                        elif( last_date.day >= 15 and last_date.day < 28 ):
                            next_date = last_date.replace( day = 28 )
                            dates.append( next_date )
                            last_date = next_date

                        elif( last_date.day >= 28 ):
                            next_date = last_date + relativedelta(months=1)
                            next_date = next_date.replace( day = 15 )
                            dates.append( next_date )
                            last_date = next_date

                elif( payment_term.type == 'weekly' ):

                    # If the initial date (ref_date) falls on the weekday specified by the user, start with this date
                    if( i == 0 and weekdaydict[ ref_date.weekday() ] == payment_term.weekday ):
                        dates.append( ref_date )
                    else:
                        if( weekdaydict[ last_date.weekday() ] == payment_term.weekday ):
                            next_date = last_date + relativedelta(days=7)
                            dates.append( next_date )
                            last_date = next_date
                        else:
                            next_date = last_date
                            while( weekdaydict[ next_date.weekday() ] != payment_term.weekday ):
                                next_date += relativedelta( days=1 )
                            dates.append( next_date )
                            last_date = next_date


            # Create 'payment_term.num_payments' payments
            result = []
            amount = value
            for i in range( 0, payment_term.num_payments ):

                # Calculate the amount
                if( i + 1 >= payment_term.num_payments ): # This is the last payment
                    amt = round( amount, precision )
                else:
                    amt = round( value / payment_term.num_payments, precision)

                # Finally append this payment to the result
                result.append( (dates[ i + payment_term.num_periods_post_date ].strftime('%Y-%m-%d'), amt) )
                amount -= amt

            return result


    #################################################################################
    # Columns and Defaults
    #################################################################################
    _columns = { 'type'                  : fields.selection( [('monthly' ,'Mensual'  ),
                                                              ('biweekly','Quincenal'),
                                                              ('weekly'  ,'Semanal'  ),
                                                              ('advanced','Avanzado' )], "Tipo", required=True ),
                 'num_payments'          : fields.integer( 'Numero de pagos', required=True ),
                 'num_periods_post_date' : fields.integer( 'Numero de periodos postfechado', required=True ),
                 'monthly_option'        : fields.selection( [('first_of_month','Primer dia del mes'),('last_of_month','Ultimo dia del mes')], "Dia", required=True ),
                 'weekday'               : fields.selection( [('monday'   , 'Lunes'    ),
                                                              ('tuesday'  , 'Martes'   ),
                                                              ('wednesday', 'Miercoles'),
                                                              ('thursday' , 'Jueves'   ),
                                                              ('friday'   , 'Viernes'  ),
                                                              ('saturday' , 'Sabado'   ),
                                                              ('sunday'   , 'Domingo'  ),], "Dia", required=True ),
               }

    _defaults = { 'type'           : 'advanced',
                  'num_payments'   : 1,
                  'monthly_option' : 'first_of_month',
                  'weekday'        : 'monday',
                }