{
    'name'         : 'AltaTec Tools',
    'version'      : '1.0',
    'description'  : """
            Algunas herramientas para usar en programmer. Tiene funcionalidad para cambiar fechas, y numeros en palabras.                    """,
    'author'       : 'AltaTec',
    'website'      : 'www.altatec.ec',
    "depends"      : [ 'base',
                     ],
    "data"         : [
                     ],
    "installable"  : True,
    "auto_install" : False
}
