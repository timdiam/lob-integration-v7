################################################################################################
#
# This file contains the model for the General Payment Order (Orden de Pago) and its wizard.
# This module allows a user to reconcile multiple invoices with one payment.
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    Jan. 8th, 2014
#
################################################################################################
from   openerp.osv             import fields, osv, orm
from   openerp.tools.translate import _

################################################################################################
# Wizard class definition
################################################################################################
class orden_pago_general_wizard(osv.osv_memory):

    _name= 'orden.pago.general.wizard'

    ################################################################################################
    # Update the debit and credit fields on amount change
    ################################################################################################
    def onchange_type(self, cr, uid, ids, type=False, context=None):

        res = { 'value' : {} }

        if( type == "tarjeta_compras" ):
            res[ 'domain' ] = { "invoices" : [('type', '=', 'in_invoice'),('state','not in',['draft','paid'])] }

        elif( type == "caja" ):
            res[ 'domain' ] = { "invoices" : [('type', '=', 'in_invoice'),('state','not in',['draft','paid'])] }

        elif( type == "tarjeta_ventas" ):
            res[ 'domain' ] = { "invoices" : [('type', '=', 'out_invoice'),('state','not in',['draft','paid'])] }

        else:
            res[ 'value'   ][ 'type' ] = 'tarjeta_compras'
            res[ 'warning' ] = { 'title' : "Error", 'message' : "Debe elegir un Tipo para el orden de pago" }

        return res

    ################################################################################################
    # Update Banco/Institucion field when the credit card changes
    ################################################################################################
    def onchange_tarjeta( self, cr, uid, ids, tarjeta_de_credito, context=None ):

        res = { 'value' : {} }

        if( tarjeta_de_credito  ):

            tarjeta = self.pool.get( 'ecua.credit.card' ).browse( cr, uid, tarjeta_de_credito, context=context )

            if( tarjeta.partner ):
                res[ 'value' ][ 'partner_id' ] = tarjeta.partner.id

            if( tarjeta.diario ):
                res[ 'value' ][ 'diario' ] = tarjeta.diario.id

        return res


    ################################################################################################
    # Update the Date field when the Period changes
    ################################################################################################
    def onchange_periodo( self, cr, uid, ids, periodo, context=None ):

        res = { 'value' : {} }

        if( periodo ):
            date           = self.pool.get( 'account.period' ).browse( cr, uid, periodo, context=context ).date_stop
            res[ 'value' ] = { 'date': date }

        return res


    ################################################################################################
    # Update the debit and credit fields on amount change
    ################################################################################################
    def onchange_amount(self, cr, uid, ids, amount=False, invoices=[], type=False, context=None):

        if( not type ):
            raise osv.except_osv( "Error!", "Debe elegir un Tipo para el orden de pago" )

        res         = {}
        total_debe  = 0
        total_haber = 0
        
        if amount:
            if( type == "tarjeta_ventas" ):
                total_debe = total_debe + amount
            else:
                total_haber = total_haber + amount

        move_line_obj = self.pool.get('account.move.line')

        if (len(invoices) >= 1) and (len(invoices[0]) == 3):

            for invoice in invoices[0][2]:

                invoice = self.pool.get('account.invoice').browse(cr,uid,invoice,context)

                line_id = move_line_obj.search( cr,
                                                uid,
                                                [ ('move_id',   '=', invoice.move_id.id    ),
                                                  ('account_id','=', invoice.account_id.id ),
                                                  ('partner_id','=', invoice.partner_id.id ),
                                                ]
                                              )
                
                line = move_line_obj.browse(cr,uid,line_id,context=context)
                
                if len(line_id)>1:
                    raise osv.except_osv(_('Invalid Action!'), 
                                         _('El asiento del invoice tiene varias lineas con la misma cuenta cruzada: '
                                           + invoice.internal_number + ".  Cuenta: "+invoice.account_id.name))
                
                if not line:
                    raise osv.except_osv(_('Invalid Action!'), 
                                         _('La cuenta cruzada se encuentra en la factura de: '
                                           + invoice.internal_number + ".  Cuenta: "+invoice.account_id.name))
                
                total_debe  = total_debe  + line[0].credit
                total_haber = total_haber + line[0].debit

        res['value'] = { 'total_debe' : total_debe, 'total_haber' : total_haber}

        return res    


    ################################################################################################
    # Grab the relevant invoices given the period and tarjeta de credito
    ################################################################################################
    def sacar_facturas(self, cr, uid, ids, context=None ):

        wizard = self.browse( cr, uid, ids[ 0 ], context=context )

        search_params = [ ( 'period_id', '=', wizard.periodo.id ),('state','not in',['draft','paid']) ]

        if( wizard.type == "tarjeta_compras" ):
            search_params.append( ( 'credit_card', '=', wizard.tarjeta_de_credito.id ) )
            search_params.append( ( 'type', '=', 'in_invoice' ) )

        elif( wizard.type == "tarjeta_ventas" ):
            search_params.append( ( 'date_invoice', '=', wizard.date ) )
            search_params.append( ( 'credit_card', '=', wizard.tarjeta_de_credito.id ) )
            search_params.append( ( 'type', '=', 'out_invoice' ) )

        elif( wizard.type == "caja" ):
            search_params.append( ( 'credit_card', '=', False ) )
            search_params.append( ( 'type', '=', 'in_invoice' ) )

        else:
            raise osv.except_osv( "Error!", "Por favor elige un Tipo del orden de pago" )

        invoice_ids = []

        if( len( search_params ) ):
            invoice_ids = self.pool.get( 'account.invoice' ).search( cr, uid, search_params )

        if( len( invoice_ids ) < 1 ):
            raise osv.except_osv( "Error!", "No se puede encontrar facturas con los parametros especificados" )

        invoice_tuple_list = []

        for invoice in wizard.invoices:
            invoice_tuple_list.append( ( 3, invoice.id ) )

        for invoice_id in invoice_ids:
            invoice_tuple_list.append( ( 4, invoice_id ) )

        dict = self.onchange_amount( cr, uid, ids, amount=False, invoices=[[6, False, invoice_ids]], type=wizard.type, context=context)

        self.write( cr,
                    uid,
                    ids[ 0 ],
                    { 'invoices'    : invoice_tuple_list,
                      'total_debe'  : dict[ 'value' ][ 'total_debe'  ],
                      'total_haber' : dict[ 'value' ][ 'total_haber' ],
                    },
                    context=context
                  )

        return { 'type'      : 'ir.actions.act_window',
                 'name'      : 'Nueva Orden de Pago',
                 'view_type' : 'form',
                 'view_mode' : 'form',
                 'res_model' : 'orden.pago.general.wizard',
                 'res_id'    : ids[0],
                 'target'    : 'new',
               }


    ################################################################################################
    # Crear button in wizard. Create the General Payment Order.
    ################################################################################################
    def crear_orden_pago(self, cr, uid, ids,context=None):

        for wiz in self.browse(cr,uid,ids,context=context):

            # Error checking
            if( wiz.amount < 0.01 ): raise osv.except_osv( 'Error!', 'Debe introducir un monto.' )
            if( not wiz.invoices  ): raise osv.except_osv( 'Error!', 'Debe elegir algunas facturas.' )
            if( not wiz.type      ): raise osv.except_osv( 'Error!', 'Debe elegir un Tipo para el orden de pago' )

            move_obj      = self.pool.get( 'account.move'      )
            move_line_obj = self.pool.get( 'account.move.line' )

            context.update({'period_id':wiz.periodo.id, 'date':wiz.date})

            # Create the account move
            my_move = move_obj.create( cr,
                                       uid,
                                       { 'journal_id' : wiz.diario.id,
                                         'period_id'  : wiz.periodo.id,
                                         'date'       : wiz.date,
                                       },
                                       context=context,
                                     )
                   
            orden_de_pago_general_obj      = self.pool.get( 'orden.de.pago.general'      )
            orden_de_pago_general_line_obj = self.pool.get( 'orden.de.pago.general.line' )

            # Create the Orden de Pago
            orden_id = orden_de_pago_general_obj.create( cr,
                                                         uid,
                                                         { 'name'       : wiz.nombre,
                                                           'partner_id' : wiz.partner_id.id,
                                                           'monto'      : wiz.amount,
                                                           'move_id'    : my_move,
                                                           'type'       : wiz.type,
                                                         },
                                                         context=context,
                                                       )

            lines_new = []
            lines_old = []
            running_total = 0

            # Iterate over all invoices selected by the user
            for invoice in wiz.invoices:

                p_move_line = self.pool.get('account.move.line').search( cr,
                                                                         uid,
                                                                         [ ('move_id',    '=', invoice.move_id.id    ),
                                                                           ('account_id', '=', invoice.account_id.id ),
                                                                           ('partner_id', '=', invoice.partner_id.id ),
                                                                         ]
                                                                       )
                
                if( not p_move_line ):
                    raise osv.except_osv(_('Error!'),
                                         _('No se encontro el asiento de la factura: ' + invoice.name ))
                
                if( len(p_move_line) > 1 ):
                    raise osv.except_osv(_('Invalid Action!'), 
                                         _('Un invoice tiene varias lineas con la misma cuenta. Invoice: '
                                           +invoice.name+". Cuenta: " + invoice.account_id.name ))
                
                p_move_line_obj = self.pool.get('account.move.line').browse(cr,uid,p_move_line[0])

                # Make sure this line isn't reconciled
                if( p_move_line_obj.reconcile_id ):
                    raise osv.except_osv( _('Invalid Action!'),
                                          _('El asiento de la factura :' + invoice.number+ " ya esta conciliado" ) )

                if( p_move_line_obj.reconcile_partial_id ):
                    raise osv.except_osv( _('Invalid Action!'),
                                          _('El asiento de la factura :' + invoice.number+ " ya esta conciliado" ) )

                # Make sure the amount in credit/debit of this account.move.line makes sense for the type of orden de pago
                if( wiz.type == "tarjeta_ventas" ):
                    if( abs( p_move_line_obj.credit) > .01 ):
                        raise osv.except_osv( 'Error!', 'Apunte en factura tiene un valor >0 en haber. ' + invoice.internal_number + ". Cuenta: "+ invoice.account_id.name )

                elif( abs( p_move_line_obj.debit) > .01 ):
                    raise osv.except_osv( 'Error!', 'Apunte en factura tiene un valor >0 en debe. ' + invoice.internal_number + ". Cuenta: "+ invoice.account_id.name )

                                        
                amount_on_invoice = p_move_line_obj.debit if wiz.type == "tarjeta_ventas" else p_move_line_obj.credit
                running_total     = running_total + amount_on_invoice
                
                lines_old.append( p_move_line[0] )
                lines_new.append( move_line_obj.create( cr,
                                                              uid,
                                                              { 'name'       : invoice.move_name,
                                                                'account_id' : p_move_line_obj.account_id.id,
                                                                'debit'      : 0 if wiz.type == "tarjeta_ventas" else amount_on_invoice,
                                                                'credit'     : amount_on_invoice if wiz.type == "tarjeta_ventas" else 0,
                                                                'move_id'    : my_move,
                                                                'partner_id' : invoice.partner_id.id,
                                                              },
                                                              context=context,

                                                            )
                                      )
                
                orden_de_pago_general_line_obj.create( cr,
                                                       uid,
                                                       { 'orden_de_pago_general' : orden_id,
                                                         'partner_id'            : invoice.partner_id.id,
                                                         'monto'                 : amount_on_invoice,
                                                         'contrapartida'         : p_move_line[0],
                                                         'rec_line'              : lines_new[-1],
                                                         'name'                  : invoice.name,
                                                         'invoice_id'            : invoice.id,
                                                       },
                                                       context=context
                                                     )
            
            if(not abs( running_total - wiz.amount ) < .01 ):
                raise osv.except_osv(_('Invalid Action!'),
                                          _('Monto total ingresado no es equal al total de los documentos: Monto ingresado: '
                                            +str(wiz.amount)+" Monto total de documentos: "+ str(running_total) ))

            # Now add a line to our account move
            credit_line = move_line_obj.create( cr,
                                                uid,
                                                {
                                                  'name'       : wiz.nombre,
                                                  'account_id' : wiz.diario.default_debit_account_id.id if wiz.type == "tarjeta_ventas" else wiz.diario.default_credit_account_id.id,
                                                  'debit'      : wiz.amount if wiz.type == "tarjeta_ventas" else 0,
                                                  'credit'     : 0 if wiz.type == "tarjeta_ventas" else wiz.amount,
                                                  'move_id'    : my_move,
                                                  'partner_id' : wiz.partner_id.id,
                                                },
                                                context=context,
                                              )
            
            for i in range(len(lines_new)):
                self.pool.get( 'account.move.line' ).reconcile_partial( cr,
                                                                        uid,
                                                                        [ lines_new[ i ], lines_old[ i ] ]
                                                                      )
            
            orden_de_pago_general_obj.write(cr,uid,orden_id,{'linea_a_pagar': credit_line,'state':'realizado'})
            
            return { 'type'      : 'ir.actions.act_window',
                     'view_type' : 'form',
                     'view_mode' : 'form,tree',
                     'res_model' : 'orden.de.pago.general',
                     'res_id'    : orden_id,
                     'nodestroy' : True,
                   }


    ################################################################################################
    # Cancel button on the wizard
    ################################################################################################
    def cancelar(self, cr, uid, ids, context=None):
        return


    ################################################################################################
    # Columns an defaults definition
    ################################################################################################
    _columns = { "nombre"             : fields.char( "Nombre", required=True ),
                 "type"               : fields.selection( [ ( "tarjeta_compras", "Tarjeta de Credito - Compras" ),
                                                            ( "tarjeta_ventas" , "Tarjeta de Credito - Ventas"  ),
                                                            ( "caja"           , "Cancelacion de Caja"          ),
                                                          ], "Tipo", required=True
                                                        ),
                 "tarjeta_de_credito" : fields.many2one( "ecua.credit.card", string="Tarjeta de Credito",),
                 "diario"             : fields.many2one("account.journal", string="Diario", required=True),
                 "periodo"            : fields.many2one("account.period", string="Periodo", required=True),
                 "partner_id"         : fields.many2one("res.partner", string="Banco / Institucion", required=True),
                 'amount'             : fields.float('Monto', ),
                 'invoices'           : fields.many2many('account.invoice','orden_invoices_rel','orden_id','invoice_id',string='Facturas'),
                 'total_debe'         : fields.float('Total Debito'),
                 'total_haber'        : fields.float('Total Credito'),
                 'date'               : fields.date("Fecha", required=True),

               }

    _defaults = { 'type' : 'tarjeta_compras',
                  'date' : fields.date.context_today,
                }