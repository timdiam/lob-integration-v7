################################################################################################
#
# This file contains the model for the General Payment Order (Orden de Pago) and its wizard.
# This module allows a user to reconcile multiple invoices with one payment.
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    Jan. 8th, 2014
#
################################################################################################
from   openerp.osv             import fields, osv, orm

################################################################################################
# Inherited account_invoice class definition. Adds many2many relationship with orden.de.pago.general
################################################################################################
class account_invoice(orm.Model):

    _inherit = "account.invoice"

    ################################################################################################
    # Columns definition
    ################################################################################################
    _columns = { 'pay_with_transfer'     : fields.boolean('Pago con Transferencia'),
                 'orden_de_pago_general' : fields.many2one('orden.de.pago.general',string="Orden de Pago"),
                 'invoices'              : fields.many2many('orden.de.pago.general','orden_invoices_rel','invoice_id','orden_id',string='Orden de Pago'),
               }