################################################################################################
#
# This file contains the model for the General Payment Order (Orden de Pago) and its wizard.
# This module allows a user to reconcile multiple invoices with one payment.
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    Jan. 8th, 2014
#
################################################################################################
from   openerp.osv             import fields, osv, orm
from   openerp.tools.translate import _

################################################################################################
# Inherited account.withhold class definition
################################################################################################
class account_withhold(osv.osv):

    _inherit = "account.withhold"

    ################################################################################################
    # button_aprove() override. Verify that the sum of the withhold lines isn't greater than
    # the amount of the Orden de Pago
    ################################################################################################
    def button_aprove(self, cr, uid, ids, context = None):

        if not context:
            context={}

        for withhold in self.browse( cr, uid, ids, context=context ):

            if( withhold.transaction_type == 'sale_massive' ):

                withhold_sum = 0.0

                for line in withhold.withhold_line_ids:
                    withhold_sum += line.tax_amount

                if( not withhold.payment_order ):
                    raise osv.except_osv("Error!", "El tipo de esta retencion es 'sale_massive' pero no tiene un Orden de Pago. Consulta con AltaTec" )

                orden = self.pool.get('orden.de.pago.general').browse( cr, uid, withhold.payment_order.id )

                orden_sum = 0.0

                for line in orden.lineas_de_pago:
                    orden_sum += line.monto

                if( withhold_sum > orden_sum ):
                    raise osv.except_osv( "Error!", "El monto total de la retencion debe ser menor que el monto total del orden de pago: " + str(orden_sum) )

        return super( account_withhold, self ).button_aprove( cr, uid, ids, context=context )

    ################################################################################################
    # action_aprove() override
    ################################################################################################
    def action_aprove(self, cr, uid, ids, context=None):

        withhold_obj = self.pool.get('account.withhold')
        num_withholds = len(ids)
        is_massive_flag = False

        #Check to make sure we aren't mixing calls here with massive retentions
        for withhold in withhold_obj.browse(cr, uid, ids, context):
            if withhold.transaction_type == 'sale_massive':
                is_massive_flag = True
                if num_withholds > 1:
                    raise osv.except_osv( ( 'Error' ), ( 'No se puede ejecutar este funcion con varias retenciones.  Consulta con AltaTec' ) )

        #If not a massive retention, run normally...
        if not is_massive_flag:
            return super(account_withhold,self).action_aprove(cr,uid,ids,context=context)
        else:
            self.action_approve_sale_massive(cr,uid,ids,context=context)

            #asign the id to the order
            self.pool.get('orden.de.pago.general').write(cr, uid, [withhold.payment_order.id], {'withhold_id': withhold.id})
            return True

    ################################################################################################
    # action_aprove_sale-massive()
    ################################################################################################
    def action_approve_sale_massive(self, cr, uid, ids, context=None):

        acc_vou_obj = self.pool.get('account.voucher')
        acc_vou_line_obj = self.pool.get('account.voucher.line')
        acc_move_line_obj = self.pool.get('account.move.line')
        ret_line_obj = self.pool.get('account.withhold.line')
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        journal_iva = company.journal_iva_id
        journal_ir = company.journal_ir_id

        if not journal_iva.default_debit_account_id:
            raise osv.except_osv('Error!', _("Iva Retention Journal doesn't have debit account assigned!, can't complete operation"))

        if not journal_ir.default_debit_account_id:
            raise osv.except_osv('Error!', _("IR Retention Journal doesn't have debit account assigned!, can't complete operation"))

        for ret in self.browse(cr, uid, ids, context=None):
            #lineas contables no conciliadas que pertenecen a la factura
            # Se requiere la suma de los haberes a cancelar
            total = 0
            sub_iva = 0
            sub_renta = 0

            for withhold in ret.withhold_line_ids:
                total = total + withhold.tax_amount

                if withhold.description == 'iva':
                    sub_iva = sub_iva + withhold.tax_amount

                if withhold.description == 'renta':
                    sub_renta = sub_renta + withhold.tax_amount


            if total == 0.0:
                raise osv.except_osv('Error!', _("Amount of withhold can't be zero, please verify"))

            move_line = acc_move_line_obj.browse(cr, uid, ret.payment_order.linea_a_pagar.id, context)

            #se comprueba que la factura se encuentre abierta
            if not ret.payment_order.state == 'realizado':
                raise osv.except_osv('Error!', "La orden no esta realizada.  No se puede agregar retencion")

            period=ret.payment_order.periodo.id
            #creacion de vauchers de pago con retencion
            line_ids = ret_line_obj.search(cr, uid, [('withhold_id', '=', ret['id']),])
            lines = ret_line_obj.browse(cr, uid, line_ids, context)

            #variable que guarda los ids de los voucher que se crean para su posterior uso desde retencion
            vouchers = []
            #verifico que existan lineas de retencion
            if lines:
                #creo la cabecera del voucher para las retenciones de iva
                vals_vou_iva = {
                            'type':'receipt',
                            #periodo de la factura
                            'period_id': ret.payment_order.periodo.id,
                            #fecha de la retencion
                            'date': ret.creation_date,
                            #el diario de iva de la compania
                            'journal_id': journal_iva.id,
                            #numero de retencion como referencia
                            'reference':_('RET CLI: %s') % ret.payment_order.name,
                            #la cuenta de debido que va a registrar el impuesto
                            'account_id': journal_iva.default_debit_account_id.id,
                            'company_id' : company.id,
                            'amount': sub_iva,
                            'currency_id': company.currency_id.id,
                            #'withhold_id': ret.id,
                            'partner_id': ret.payment_order.partner_id.id,
                            'withhold_id': ret.id,
                }
                #creo la cabecera del voucher para las lineas de iva
                voucher_iva = acc_vou_obj.create(cr, uid, vals_vou_iva, context)
                vouchers.append(voucher_iva)
                #creo la cabecera del voucher para las retenciones de renta
                vals_vou_ir = {'type':'receipt',
                            #periodo de la factura
                            'period_id': ret.payment_order.periodo.id,
                            #fecha de la retencion
                            'date': ret.creation_date,
                            #el diario de iva de la compania
                            'journal_id': journal_ir.id,
                            #numero de retencion como referencia
                            'reference':_('RET CLI: %s') % ret.payment_order.name,
                            #la cuenta de debido que va a registrar el impuesto
                            'account_id': journal_ir.default_debit_account_id.id,
                            'company_id' : company.id,
                            'amount': sub_renta,
                            'currency_id': company.currency_id.id,
                            #'withhold_id': ret.id,
                            'partner_id': ret.payment_order.partner_id.id,
                            'withhold_id': ret.id,
                }

                #creo la cabecera del voucher para las lineas de renta
                voucher_ir = acc_vou_obj.create(cr, uid, vals_vou_ir, context)
                vouchers.append(voucher_ir)
                #recorro cada linea de retencion
                #variables de control para verificar que existen lineas de cada tipo
                renta = False
                iva = False
                for line in lines:
                    #verifico las lineas por tipo para seleccionar el diario correspondiente
                    if line.description == 'iva':
                        vals_vou__iva_line = {
                                    'voucher_id': voucher_iva,
                                    'move_line_id':move_line.id,
                                    'account_id':move_line.account_id.id,
                                    'amount':line.tax_amount,
                                         }
                        acc_vou_line_obj.create(cr, uid, vals_vou__iva_line, context)
                        #se cambia el valor de la variable ya que se encontro al menos una linea de retencion
                        iva = True

                    if line.description == 'renta':
                        vals_vou_ir_line = {
                                    'voucher_id': voucher_ir,
                                    'move_line_id':move_line.id,
                                    'account_id':move_line.account_id.id,
                                    'amount':line.tax_amount,
                                         }
                        acc_vou_line_obj.create(cr, uid, vals_vou_ir_line, context)
                        #se cambia el valor de la variable ya que se encontro al menos una linea de retencion
                        renta = True
                #se aprueba los voucher de rentencion, y se verifica que existan lineas
                #acc_vou_obj.proforma_voucher(cr, uid, [voucher_iva, voucher_ir,], context)
                if iva:
                    #por medio de la variable contexto se especifica que tipo de impuesto es del voucher
                    self.action_move_line_create(cr, uid, [voucher_iva,], context={'tax':'iva', 'withhold_id':ret.id})
                #en caso de no existir lineas en el voucher se elimina el que se creo anteriormente
                else:
                    acc_vou_obj.unlink(cr, uid,[voucher_iva,])
                    vouchers.remove(voucher_iva)

                if renta:
                    #por medio de la variable contexto se especifica que tipo de impuesto es del voucher
                    self.action_move_line_create(cr, uid, [voucher_ir,], context={'tax':'renta', 'withhold_id':ret.id})
                #en caso de no existir lineas en el voucher se elimina el que se creo anteriormente
                else:
                    acc_vou_obj.unlink(cr, uid,[voucher_ir,])
                    vouchers.remove(voucher_ir)
                #print vouchers
                #se cambia el estado de la retencion
                if vouchers:
                    acc_vou_obj.write(cr, uid, vouchers, {'withhold_id':ret.id},context)
                date_ret = None
                if not ret.creation_date:
                    #now_date = datetime.now(pytz.timezone("America/Guayaquil")).strftime("%Y-%m-%d")
                    now_date = fields.date.context_today(self, cr,uid,context=context)
                    date_ret = now_date
                else:
                    date_ret = ret.creation_date
                self.write(cr, uid, [ret.id,], { 'state': 'approved','creation_date': date_ret,'number':ret.number, 'period_id': period}, context)
            else:
                raise osv.except_osv('Error!', _("You can't aprove a withhold without withhold lines"))

        return True

    ################################################################################################
    # action_cancel override()
    ################################################################################################
    def action_cancel(self,cr,uid,ids,context=None):

        withhold_obj = self.pool.get('account.withhold')
        num_withholds = len(ids)
        is_massive_flag = False

        #Check to make sure we aren't mixing calls here with massive retentions
        for withhold in withhold_obj.browse(cr, uid, ids, context):
            if withhold.transaction_type == 'sale_massive':
                is_massive_flag = True
                if num_withholds > 1:
                    raise osv.except_osv( ( 'Error' ), ( 'No se puede ejecutar este funcion con varias retenciones.  Consulta con AltaTec' ) )

        #If not a massive retention, run normally...
        if not is_massive_flag:
            return super(account_withhold,self).action_cancel(cr,uid,ids,context=context)
        else:
            for withhold in self.pool.get('account.withhold').browse(cr, uid, ids, context):
                if withhold.transaction_type == "sale_massive":
                    moves = []
                    vouchers = []

                    for line in withhold.account_voucher_ids:
                        if not line.move_id.id in moves:
                            moves.append(line.move_id.id)

                    for move in moves:
                        vou = self.pool.get('account.voucher').search(cr, uid, [('move_id','=',move)])
                        vouchers.append(vou[0])

                    self.pool.get('account.voucher').cancel_voucher(cr, uid, vouchers, context)
                    self.pool.get('account.voucher').unlink(cr, uid, vouchers, context)

                    self.pool.get('account.withhold').write(cr, uid, [withhold.id, ], {'state':'canceled'}, context)

            return True

    ################################################################################################
    # action_aprove_sale-massive()
    ################################################################################################
    def default_get(self, cr, uid, field_list, context=None):

        if 'active_model' in context and context['active_model'] == 'orden.de.pago.general':

            order_obj = self.pool.get('orden.de.pago.general').browse(cr,uid,context['active_id'],context=context)

            return { 'allow_electronic_document' : False,
                     'period_id'                 : order_obj.periodo.id ,
                     'state'                     : 'draft',
                     'transaction_type'          : 'sale_massive',
                     'partner_id'                : order_obj.partner_id.id,
                     'payment_order'             : order_obj.id,
                     'creation_date'             : fields.date.context_today(self, cr, uid, context),
                     'autorization_partners'    : True,
                   }
        else:
            return super(account_withhold,self).default_get(cr,uid,field_list,context=context)

    ################################################################################################
    # Add a transaction_type field and a payment_order field
    ################################################################################################
    _columns = { 'transaction_type' : fields.selection([ ('sale_massive',"Venta Masiva"),
                                                         ('purchase','Purchases'),
                                                         ('sale','Sales'),
                                                         ('canceled','Canceled'),
                                                       ],  'Transaction type', required=True, readonly=True),
                 'payment_order'    : fields.many2one('orden.de.pago.general', string="Orden de Pago"),
               }

################################################################################################
# Inherited account_withhold_line class definition
################################################################################################
class account_withhold_line(osv.osv):

    _inherit = "account.withhold.line"

    ################################################################################################
    # default_get() override
    ################################################################################################
    def default_get(self, cr, uid, fields, context=None):

        if context is None:
            context = {}

        values = {}

        if (context.get('transaction_type') and context.get('transaction_type') == 'sale_massive'):
            transaction_type = context.get('transaction_type')
            values = { 'transaction_type_line': transaction_type,
                       'description': 'renta', #un valor por defecto, pudo haber sido iva sin problema
                     }
            return values
        else:
            return super(account_withhold_line, self).default_get(cr,uid,fields,context=context)

    ################################################################################################
    # Add a transaction_type_line field
    ################################################################################################
    _columns = { 'transaction_type_line': fields.selection([('purchase','Purchases'),
                                                            ('sale','Sales'),
                                                            ('sale_massive', "Masivo de Ventas"),
                                                           ],'Transaction type', required=True, readonly=True, track_visibility='onchange'),
               }