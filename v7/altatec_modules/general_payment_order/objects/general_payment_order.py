################################################################################################
#
# This file contains the model for the General Payment Order (Orden de Pago) and its wizard.
# This module allows a user to reconcile multiple invoices with one payment.
#
# Author:  Dan Haggerty
# Company: AltaTec Ecuador
# Date:    Jan. 8th, 2014
#
################################################################################################
from   openerp.osv             import fields, osv, orm
from   openerp.tools.translate import _

################################################################################################
# General Payment Order class definition
################################################################################################
class orden_de_pago_general(orm.Model):

    _name = 'orden.de.pago.general'

    ###########################################################################################
    # Add retention button
    ################################################################################################
    def add_retention(self, cr, uid, ids, context=None):

        for orden in self.browse( cr, uid, ids, context=context ):
            if orden.withhold_id:
                raise osv.except_osv( "Error!", "Este Orden de Pago ya tiene una retencion!" )

        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ecua_tax_withhold', 'withhold_wizard_form_sale') #in model name replace . with _ Example sale.order become sale_order

        return { 'name'      : ("Andadir Retecion Masiva"),#Name You want to display on wizard
                 'view_mode' : 'form',
                 'view_id'   : view_id,
                 'view_type' : 'form',
                 'res_model' : 'account.withhold',# With . Example sale.order
                 'type'      : 'ir.actions.act_window',
                 'target'    : 'new',
                 'context'   : {'transaction_type' : 'sale_massive', },
        }


    ################################################################################################
    # Unlink method override
    ################################################################################################
    def unlink(self, cr, uid, ids, context=None):
        records = self.browse(cr,uid,ids,context)
        for r in records:
            if r.state != 'anulado':
                raise osv.except_osv(_('Invalid Action!'), _('No puedes borrar un orden de pago en el estado realizado'))
        return super(orden_de_pago_general,self).unlink(cr, uid, r.id, context=context)


    ################################################################################################
    # Cancelar Orden de Pago button. Unreconcile every line
    ################################################################################################
    def cancelar_orden_de_pago(self,cr,uid,ids,context=None):

        reconcile_pool = self.pool.get('account.move.reconcile')
        move_pool      = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        
        for record in self.browse(cr,uid,ids):

            if( record.withhold_id ):
                raise osv.except_osv( "Error!", "No se puede anular un orden de pago que ya tiene una retencion." )
            
            for orden_line in record.lineas_de_pago:
                if orden_line.contrapartida.reconcile_id:
                    reconcile_pool.unlink(cr,uid,[orden_line.contrapartida.reconcile_id.id])
                elif orden_line.contrapartida.reconcile_partial_id:
                    reconcile_pool.unlink(cr,uid,[orden_line.contrapartida.reconcile_partial_id.id])
            
            move_pool.button_cancel(cr,uid,[record.move_id.id],context=context)
            move_pool.write(cr,uid,[record.move_id.id],{'ref':'ANULADO'},context=context)
            
            for line in record.move_id.line_id:
                move_line_pool.unlink(cr,uid,[line.id],context=context)
            
            self.write(cr,uid,[record.id], {'state':'anulado'})
            
        return True


    ################################################################################################
    # Columns definitions
    ################################################################################################
    _columns = { 'name'           : fields.char('Nombre / Razon' ,required=True),
                 "type"           : fields.selection( [ ( "tarjeta_compras", "Tarjeta de Credito - Compras" ),
                                                        ( "tarjeta_ventas" , "Tarjeta de Credito - Ventas"  ),
                                                        ( "caja"           , "Cancelacion de Caja"          ),
                                                      ], "Tipo", required=True
                                                    ),
                 'diario'         : fields.related('move_id','journal_id',type='many2one',relation='account.journal', string="Diario", required=True ),
                 'periodo'        : fields.related('move_id', 'period_id',type='many2one',relation='account.period', string = "Periodo", required=True ),
                 'date'           : fields.related('move_id','date', type='date', string='Fecha'),
                 'partner_id'     : fields.many2one('res.partner',string="Bancaria / Institucion a Pagar"),
                 'monto'          : fields.float('Monto'),
                 'move_id'        : fields.many2one('account.move',string='Asiento contable',),
                 'linea_a_pagar'  : fields.many2one('account.move.line','Apunte a pagar', ),
                 'lineas_de_pago' : fields.one2many('orden.de.pago.general.line','orden_de_pago_general',string='Lineas de Pago', ),
                 'state'          : fields.selection([('realizado','Realizado'), ('anulado','Anulado')], 'Estado', size=32 ),
                 'withhold_id'    : fields.many2one('account.withhold', string="Retencion"),
               }


################################################################################################
# General Payment Order line class definition
################################################################################################
class orden_de_pago_general_line(osv.osv):

    _name = 'orden.de.pago.general.line'


    ################################################################################################
    # Column definitions
    ################################################################################################
    _columns = { 'master'                : fields.boolean('Master Line?'),
                 'orden_de_pago_general' : fields.many2one('orden.de.pago.general', string='Orden De Pago'),
                 'partner_id'            : fields.many2one('res.partner', string='Empresa'),
                 'monto'                 : fields.float('Monto Asignacion'),
                 'contrapartida'         : fields.many2one('account.move.line',string='Contrapartida'),
                 'rec_line'              : fields.many2one('account.move.line', string='Linea de Reconciliacion'),
                 'name'                  : fields.char("Descripcion"),
                 'reconcile_id'          : fields.related('contrapartida','reconcile_id',type='many2one',relation='account.move.reconcile',string='ID de Reconciliacion'),
                 'reconcile_partial_id'  : fields.related('contrapartida','reconcile_partial_id',type='many2one',relation='account.move.reconcile',string='ID de part. Reconciliacion'),
                 'invoice_id'            : fields.many2one('account.invoice', string="Factura"),
                 'ret_iva'               : fields.float("Valor Ret IVA"),
                 'ret'                   : fields.float("Valor Ret"),
               }