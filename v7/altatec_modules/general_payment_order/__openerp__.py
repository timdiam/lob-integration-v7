{   'name': 'General Payment Order',
    'version': '1.0',
    'description': """
    General Payment Order
    """,
    'author': 'Dan Haggerty',
    'website': 'www.altatececuador.com',
    "depends" : [ 'account',
                  'ecua_credit_card',
                  'ecua_tax_withhold',
                  'ecua_autorizaciones_sri',
                  'web_hide_duplicate'
                ],
    "data" : [ 'views/general_payment_order.xml',
               'views/account_withhold.xml',
               'security/ir.model.access.csv',
             ],
    "installable": True,
    "auto_install": False
}