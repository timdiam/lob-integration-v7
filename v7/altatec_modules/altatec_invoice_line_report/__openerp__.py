{
    'name': 'Altatec Invoice Line Report',
    'version': '1.0',
    'description': """
        Wizard for invoices
    """,
    'author': 'Denisse Ochoa',
    'website': 'www.altatececuador.com',
    "depends" : [
                'base',
                'product',
                'account_analytic_analysis'

                ],
    "data" : [
                'views/invoice_line_report.xml',
                'report/data/report.xml',


             ],
    "installable": True,
    "auto_install": False
}
