#################################################################################
#
# This is the parser for the ecua_payable_receivable module.
#
# Author:  Denisse Ochoa
# Company: AltaTec Ecuador
# Date:    21/04/2015
#
#################################################################################
from report      import report_sxw
import datetime

#################################################################################
# Parser class definition
#################################################################################
class Parser(report_sxw.rml_parse):

    #################################################################################
    # __init__() definition
    #################################################################################
    def __init__(self, cr, uid, name, context):

        super(Parser, self).__init__(cr, uid, name, context)

        self.localcontext.update( { 'cr'              : cr,
                                    'uid'             : uid,
                                    'invoice_total'   : 0.0,
                                    'get_lines'       : self.get_lines,
                                    'print_column'    : self.print_column,
                                    'get_partner_name': self.get_partner_name,
                                    'get_total'       : self.get_total,
                                    'convert_type_to_name' : self.convert_type_to_name,
                                    'get_sum_total'   : self.get_sum_total,
                                    'get_product_name' : self.get_product_name,
                                    'get_project_name' : self.get_project_name,

                                  }
                                )
    #################################################################################
    # get_lines() returns data from invoices
    #################################################################################
    def get_lines(self, invoice_line):
        lines = []
        columns = []
        partner_name = self.get_partner_name(invoice_line.invoice_id.partner_id.id)
        inv_type = self.convert_type_to_name(invoice_line.invoice_id.type)
        inv_state = self.convert_state_to_name(invoice_line.invoice_id.state)
        #product_name = self.get_product_name(invoice.invoice_line.product_id)
        if (inv_state != 'ANULADA'):
            columns.append(invoice_line.invoice_id.internal_number) #Numero de Factura
            columns.append(invoice_line.invoice_id.date_invoice) #Fecha
            columns.append(partner_name) #Nombre de cliente
            columns.append(invoice_line.invoice_id.user_id.name) #Nombre del vendedor
            columns.append(invoice_line.invoice_id.reference) #referencia
            columns.append(inv_type) #tipo de factura
            columns.append(inv_state) #estado de factura
            if invoice_line.product_id: #producto
                product_name = self.get_product_name(invoice_line.product_id)
                columns.append(product_name)
            else:
                columns.append(invoice_line.name) #Si no hay productos seleccionados coloca la descripcion
            if hasattr(invoice_line, 'project_id'): #proyecto
                if invoice_line.project_id:
                    project_name = self.get_project_name(invoice_line.project_id)
                    columns.append(project_name)
                else:
                    columns.append('')
            elif invoice_line.account_analytic_id:
                project_name = self.get_project_name(invoice_line.account_analytic_id)
                columns.append(project_name)
            else:
                columns.append('')
            columns.append(invoice_line.price_subtotal) #Debe ser precio de los productos.
            lines.append(columns)
            self.get_sum_total(invoice_line.price_subtotal)#subtotal
        else:
            columns.append(invoice_line.invoice_id.internal_number) #Numero de factura
            columns.append(invoice_line.invoice_id.date_invoice) # Fecha
            columns.append(inv_state) #En lugar del nombre de cliente mostrar ANULADA
            columns.append(None) #vacio para nombre de vendedor
            columns.append(None) #vacio para referencia
            columns.append(None) #vacio para tipo de factura
            columns.append(inv_state) #estado de factura
            columns.append(None) #vacio para producto
            columns.append(None) #vacio para proyecto
            columns.append(0.0) #0.0 para precio de producto
            lines.append(columns)
        return lines

    #################################################################################
    # print_columns() returns column to fill spreadsheet
    #################################################################################
    def print_column(self, column):
        return column

    def get_partner_name( self, partner ):
        return self.pool.get( 'res.partner' ).browse( self.localcontext['cr'],
                                                      self.localcontext['uid'],
                                                      partner,
                                                      self.localcontext
                                                    ).name

    #################################################################################
    # Converts the invoice.type (out_invoice, in_invoice, etc) to a nicer name
    #################################################################################
    def convert_type_to_name(self, type ):
        if( type == 'out_invoice' ):
            return 'FACTURA DE CLIENTE'
        elif( type == 'in_invoice' ):
            return 'FACTURA DE PROVEEDOR'
        elif( type == 'out_refund' ):
            return 'NOTA DE CREDITO DE CLIENTE'
        elif( type == 'in_refund' ):
            return 'NOTA DE CREDITO DE PROVEEDOR'
        else:
            return 'FACTURA'

    def convert_state_to_name(self, state ):
        if( state == 'open' ):
            return 'ABIERTO'
        elif( state == 'paid' ):
            return 'PAGADA'
        elif( state == 'cancel' ):
            return 'ANULADA'
        else:
            return

    #################################################################################
    # get_sum_total() sum all price unit subtotal
    #################################################################################
    def get_sum_total(self, amount):
        last_total = self.localcontext['invoice_total']
        new_total = last_total + amount
        self.localcontext.update({'invoice_total': new_total})

        return

    #################################################################################
    # get_total() sum all price unit subtotal
    #################################################################################
    def get_total(self):
        return self.localcontext['invoice_total']

    #################################################################################
    # get_product_name() return product name
    #################################################################################
    def get_product_name(self,product_id):
        return self.pool.get( 'product.product' ).browse( self.localcontext['cr'],
                                                      self.localcontext['uid'],
                                                      product_id.id,
                                                      self.localcontext
                                                    ).name

    #################################################################################
    # get_project_name() return project name
    #################################################################################
    def get_project_name(self,project_id):
        return self.pool.get( 'account.analytic.account' ).browse( self.localcontext['cr'],
                                                      self.localcontext['uid'],
                                                      project_id.id,
                                                      self.localcontext
                                                    ).name
