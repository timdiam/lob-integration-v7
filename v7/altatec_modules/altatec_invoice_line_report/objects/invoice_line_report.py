from openerp.osv import fields, osv, orm
import locale
import logging

_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')


class altatec_invoice_line_report_wizard(osv.osv_memory):
    _name='altatec.invoice.line.report.wizard'

    def get_invoices(self,cr, uid, ids, context=None):
        records = self.browse(cr,uid,ids)[0]
        invoices = []
        search_params = []

        if records.analysis_type == 'sale':
            search_params.append(('type','in',['out_invoice','out_refund']))

        if records.analysis_type == 'purchase':
            search_params.append(('type','in',['in_invoice','in_refund']))

        if records.date_start:
            search_params.append(('date_invoice', '>=', records.date_start))

        if records.date_end:
            search_params.append(('date_invoice', '<=', records.date_end))

        if records.inv_cancelled:
            search_params.append(('state','in',['open','paid','cancel']))

        else:
            search_params.append(('state','in',['open','paid']))

        #Obtiene las facturas segun los paramentros de busqueda
        invoices = self.pool.get('account.invoice').search(cr, uid, search_params)

        #Obtiene las lineas de las facturas
        invoice_lines = self.pool.get('account.invoice.line').search(cr, uid, [('invoice_id', 'in', invoices)], context=None)

        if records.project_id and records.product_id:
            inv_obj = self.pool.get('account.invoice.line')
            inv_line_ids = inv_obj.search(cr, uid, [('invoice_id', 'in', invoices)])
            inv_line = inv_obj.browse(cr, uid, inv_line_ids)

            if hasattr(inv_line[0], 'project_id'):
                invoice_lines = inv_obj.search(cr, uid, [('invoice_id', 'in', invoices),
                                                        ('project_id', '=', records.project_id.id),
                                                        ('product_id', '=', records.product_id.id)])

            else:
                invoice_lines = inv_obj.search(cr, uid, [('invoice_id', 'in', invoices),
                                                        ('account_analytic_id', '=', records.project_id.id),
                                                        ('product_id', '=', records.product_id.id)])

        elif records.project_id:
            inv_obj = self.pool.get('account.invoice.line')
            inv_line_ids = inv_obj.search(cr, uid, [('invoice_id', 'in', invoices)])
            inv_line = inv_obj.browse(cr, uid, inv_line_ids)

            if hasattr(inv_line[0], 'project_id'):
                invoice_lines = inv_obj.search(cr, uid, [('invoice_id', 'in', invoices),
                                                        ('project_id', '=', records.project_id.id)])
            else:
                invoice_lines = inv_obj.search(cr, uid, [('invoice_id', 'in', invoices),
                                                        ('account_analytic_id', '=', records.project_id.id)])

        elif records.product_id:
            inv_obj = self.pool.get('account.invoice.line')
            invoice_lines = inv_obj.search(cr, uid, [('invoice_id', 'in', invoices), ('product_id', '=', records.product_id.id)])


        context = {
                    'date_start' : records.date_start,
                    'date_end'   : records.date_end,
                    'project_id' : records.project_id.id,
                    'product_id' : records.product_id.id,
                }

        #No muestra mensaje si no se encuentra por proyecto o por producto
        if invoices:
            return { 'type'        : 'ir.actions.report.xml',
                     'context'     : context,
                     'report_name' : 'invoice_line_report',
                     'datas'       : { 'ids' : invoice_lines }
                   }
        else:
            raise orm.except_orm('Aviso!','No hay datos que mostrar, realice otra consulta')

    _columns = {
    'date_start' : fields.date('Fecha Inicio'),
    'date_end'   : fields.date('Fecha Final'),
    'product_id' : fields.many2one('product.product', string='Producto'),
    'project_id' : fields.many2one('account.analytic.account', string='Proyecto'),
    'inv_cancelled': fields.boolean('Incluir Facturas Anuladas'),
    'analysis_type' : fields.selection([('sale', 'Vendido'),
                                        ('purchase', 'Comprado'),
                                        ('all', 'Todas')],
                                        string='Tipo de Analisis',
                                        required=True),
    }





