from openerp.osv import fields, osv
import re
from openerp.tools.translate import _
import time
from string import split

class sri_authorizations(osv.osv):
    _inherit = 'sri.authorizations'
    
    _columns={
              'first_sequence': fields.integer('Initial Sequence',required=False,track_visibility='onchange',help="The number from which the authorized start sequence can be copied or printed invoice retention"),
              'last_sequence': fields.integer('Last Sequence ',required=False,track_visibility='onchange',help="The number to which the authorized ending sequence, can be copied or printed invoice retention."),
              }
     
sri_authorizations()