from openerp.osv import fields, osv

class hr_config_settings(osv.osv_memory):
    _inherit = 'hr.config.settings'

    _columns = {
        'unified_wage': fields.float('Unified Wage',help ="""This show the unified wage."""),
                }
    _defaults={
               'unified_wage':318.00
               }
    
    def get_default_wage(self, cr, uid, fields, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        return {
            'unified_wage': user.company_id.wage_unified,
        }
    
    def set_default_wage(self, cr, uid, ids, context=None):
        """ set default wage for contracts"""
        ir_values = self.pool.get('ir.values')
        config = self.browse(cr, uid, ids[0], context)
        ir_values.set_default(cr, uid, 'hr.contract', 'wage',
            config.unified_wage or False)
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        user.company_id.write({'wage_unified': config.unified_wage})
        
        
        
hr_config_settings()
      