# -*- encoding: utf-8 -*-
########################################################################
#                                                                       
# @authors: Andrea García                                                                           
# Copyright (C) 2013  TRESCLOUD Cia Ltda.                                 
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import osv, fields 
from datetime import datetime
import calendar
from dateutil.relativedelta import relativedelta
from tools.translate import _

class hr_holidays_status(osv.osv):
    '''
    Holidays
    '''
    _inherit = 'hr.holidays.status'
    _columns={
              'tabulated':fields.boolean('Tabulated',help='If you want your holidays to be counted on the payroll'),
              'code':fields.char('Code',size=18),
              }
hr_holidays_status()