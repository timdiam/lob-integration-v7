# -*- coding: utf-8 -*-
########################################################################
#                                                                       
#@authors:TRESCLOUD Cia.Ltda                                                                          
# Copyright (C) 2013                     
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
#ice
########################################################################

import ecua_hr
import hr_payslip
import hr_holidays
import hr_salary_rule
import res_config
import hr_employee
import res_company
import hr_payroll_structure
import hr_payslip_employees