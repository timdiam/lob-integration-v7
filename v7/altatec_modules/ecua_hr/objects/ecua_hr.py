# -*- encoding: utf-8 -*-
########################################################################
#                                                                       
# @authors:  Andrea García                                                                           
# Copyright (C) 2013  TRESCLOUD Cia Ltda.                                 
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import osv, fields 
from datetime import datetime
import calendar
from dateutil.relativedelta import relativedelta
from tools.translate import _

class hr_contract(osv.osv):
    
    _name = 'hr.contract' 
    _inherit = ['hr.contract','mail.thread']
    def _compute_day_pay(self, cr, uid, ids, field, arg, context=None):
        """Funcion que me ayuda para calcular los fondos de reserva del empleado"""
        res = {}
        DATETIME_FORMAT = "%Y-%m-%d"
        #TODO SHOULD WE DELETE THE VARIABLE TODAY?  It looks like it is not used....
        today = datetime.now()
        last_day=30
        for contract in self.browse(cr, uid, ids, context=context):
            date_start = datetime.strptime(contract.date_start, DATETIME_FORMAT)
            if date_start.day<last_day and date_start.day>1:
                res[contract.id]=last_day-date_start.day
            else:
                res[contract.id] = last_day
        return res
#    def _compute_wage_unified(self, cr, uid, ids, field, arg, context=None):
#        config=self.pool.get('hr.config.settings')
#        search
#        res[payslip.id]=
#        return True
    
    _columns = {
       'day_pay':fields.function(_compute_day_pay,string="Day Pay",type='float',store=False,method=True,track_visibility='onchange'),
       'bonus':fields.float('Performance bonus',help='Compliance Indicators Bonuses',track_visibility='onchange'),
       'date_start': fields.date('Initial Date of the Contract ', required=True,track_visibility='onchange'),
       'transport':fields.float('Transport',help='Transport [dollars]',track_visibility='onchange'),
       'feeding':fields.float('Feeding',help='Feeding [dollars]',track_visibility='onchange'),
       'income_tax_withhold':fields.float('Income Tax Withholding',help='Value to retain the employee for anticipated income tax [dollars]',track_visibility='onchange'),
       'commission':fields.float('Commission',help='Commission per Sales',track_visibility='onchange'),
       'legal_hours':fields.float('Legal Hours',help='Used for wage calculations such as overtime',track_visibility='onchange'),
       'validate_f':fields.boolean('Validate Reserve Funds',track_visibility='onchange',help='Validates special case, Example: when an employee entrance on the fifteenth of the month, this validates its reserve fund starting in the fifteenth to the end of the month'),
        'name': fields.char('Contract Reference', size=64, required=True,track_visibility='onchange'),
        'employee_id': fields.many2one('hr.employee', "Employee", required=True,track_visibility='onchange'),
        'department_id': fields.related('employee_id','department_id',track_visibility='onchange', type='many2one', relation='hr.department', string="Department", readonly=True),
        'type_id': fields.many2one('hr.contract.type', "Contract Type",track_visibility='onchange',required=True),
        'job_id': fields.many2one('hr.job', 'Job Title',track_visibility='onchange'),
        'date_end': fields.date('End Date',track_visibility='onchange'),
        'trial_date_start': fields.date('Trial Start Date',track_visibility='onchange'),
        'trial_date_end': fields.date('Trial End Date',track_visibility='onchange'),
        'working_hours': fields.many2one('resource.calendar','Working Schedule',track_visibility='onchange'),
        'wage': fields.float('Wage', digits=(16,2), required=True, help="Basic Salary of the employee",track_visibility='onchange'),
        'advantages': fields.text('Advantages',track_visibility='onchange'),
        'notes': fields.text('Notes',track_visibility='onchange'),
        'permit_no': fields.char('Work Permit No', size=256, required=False, readonly=False,track_visibility='onchange'),
        'visa_no': fields.char('Visa No', size=64, required=False, readonly=False,track_visibility='onchange'),
        'visa_expire': fields.date('Visa Expire Date',track_visibility='onchange'),
        'struct_id': fields.many2one('hr.payroll.structure', 'Salary Structure',track_visibility='onchange'),
        'analytic_account_id':fields.many2one('account.analytic.account', 'Ref. Project',track_visibility='onchange', help='For reference only, the project this employee is assigned to, doesnt have any accounting effect'),
        'analytics_id': fields.many2one('account.analytic.plan.instance', 'Analytic Distribution', 
                                        help='Distributes the expense made on this employee into several cost centers by percentage, the value can be preset in the employee contract',
                                        track_visibility='onchange',
                                        ),
        'journal_id': fields.many2one('account.journal', 'Salary Journal',track_visibility='onchange'),
        'hours_week':fields.related('working_hours','hours_work_per_week',type='float',relation='resource.calendar',string='Hours per Week',store=True),
        'pagar_decimo_13':fields.boolean('Pago Decimo 13 Mensual'),
        'pagar_decimo_14':fields.boolean('Pago Decimo 14 Mensual'),
        }


    def _get_type_ecua(self, cr, uid, context=None):
        #res=super(hr_contract, self)._get_type( cr, uid, context=context)
        "ADMINISTRATIVO"
        admi = 0
        obj_model_data=self.pool.get("ir.model.data")
        model_adm=obj_model_data.search(cr,uid,[('name','=','hr_contract_type_wrkr')])
        if model_adm:
            admi=obj_model_data.browse(cr,uid,model_adm)[0].res_id
        type_ids = self.pool.get('hr.contract.type').search(cr, uid, [('id', '=',admi)])
        return type_ids and type_ids[0] or False
    
    def _get_work_hours(self,cr,uid,context=None):
        ir_model=self.pool.get('ir.model.data')
        res=ir_model.search(cr, uid, [('name', '=', 'timesheet_group_ecua')])
        res_id=ir_model.browse(cr,uid,res)[0].res_id
        work_ids = self.pool.get('resource.calendar').search(cr, uid, [('id', '=', res_id)])
        return work_ids and work_ids[0] or False
    
    _defaults={
       'validate_f':True,
       'legal_hours':240,
       'type_id': _get_type_ecua,
       'working_hours':_get_work_hours,
        }

hr_contract()

class resource_calendar(osv.osv):
    _inherit = 'resource.calendar'
    _name = 'resource.calendar' 
      
    def _compute_hours(self, cr, uid, ids, field, arg, context=None):
        ''' Función que calcula el número de horas trabajadas a la semana. por Carlos Yumbillo'''
        
        res = {}
        hours_per_day = 0 
        hours_per_week = 0

        calendar_obj = self.pool.get('resource.calendar')
        calendar = calendar_obj.browse(cr, uid, ids, context=context)[0]

        for hours in calendar.attendance_ids:
            hours_per_day = hours.hour_to - hours.hour_from 
            hours_per_week = hours_per_week + hours_per_day      
            
        res[calendar.id] = hours_per_week
        
        return res
    
    _columns = {
       'hours_work_per_week': fields.function(_compute_hours, string='Hours per week', type='float', store=False, method=True, help='Number of hours of work per week.'),
        }

resource_calendar()

class hr_contract_type(osv.osv):
    _inherit = 'hr.contract.type'
    
    _columns = {
        'active': fields.boolean('Active', help='Indicates whether the contract type is active.',),
    }

hr_contract_type()
