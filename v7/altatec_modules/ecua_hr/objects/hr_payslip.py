# -*- encoding: utf-8 -*-
########################################################################
#                                                                       
# @authors:  Carlos Yumbillo, Andrea García                                                                           
# Copyright (C) 2013  TRESCLOUD Cia Ltda.                                 
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

import time
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta
#from dateutil.relativedelta import relativedelta
from osv import osv, fields 
import calendar
from tools.translate import _



class hr_payslip(osv.osv):
    '''
    Pay Slip
    '''
    _name='hr.payslip'
    _inherit = ['hr.payslip','mail.thread']


    #We are forced to override everything here because the parent (hr_payroll) deletes
    #the account move!  This is not nice funcionality to keep sequences friendly.
    def cancel_sheet(self, cr, uid, ids, context=None):
        if not context:
            context={}
        #########################################################
        #code from hr_payroll_account
        #########################################################
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        move_ids = []
        move_to_cancel = []
        context.update({'force_cancel':1})

        for slip in self.browse(cr, uid, ids, context=context):
            if slip.move_id:
                if slip.move_id.state == 'posted':
                    move_pool.button_cancel(cr, uid, [slip.move_id.id], context)

                for ml in slip.move_id.line_id:
                    move_line_pool.unlink(cr, uid, [ml.id])

                #We dont want the move to be deleted!
                #move_pool.unlink(cr, uid, move_ids, context=context)
                #Instead, we mark it ANULADO... keeping the name (number) and the sequence
                move_pool.write(cr,uid,[slip.move_id.id],{'ref':'ANULADO'})

        #############################################################
        #############################################################

        ###############################################################
        #code from hr_extra_input_output
        ###############################################################
        reminder_obj = self.pool.get('hr.salary.reminder')
        for payslip in self.browse(cr, uid, ids, context=context):
            salery_rule_ids = []
            for line in payslip.line_ids:
                salery_rule_ids.append(line.salary_rule_id.id)
            reminder_ids = reminder_obj.search(cr, uid, [('rule_id', 'in', salery_rule_ids)], context=context)
            if reminder_ids:
                reminder_obj.write(cr, uid, reminder_ids, {'processed' : False}, context=context)
        ##################################################################

        ##################################################################
        #code from hr_payrolll
        ##################################################################
        return self.write(cr, uid, ids, {'state': 'cancel'}, context=context)
        ###################################################################



    def get_vacations(self, cr, uid, holiday_ids, date_from_payslip, date_to_payslip,contract_id, context=None):
        '''CODIGO SOLO EN EL CASO QUE SE QUIERA SUMAR TODAS LAS VACACIONES CON EL MISMO CODIGO por ANDREA GARCIA'''
        holidays_obj=self.pool.get('hr.holidays')
        total_holidays=0.0
        lista=holidays_obj.browse(cr,uid,holiday_ids)
        i=0
        code=''
        res=[]
        enter=False
        for holiday in holidays_obj.browse(cr,uid,holiday_ids):
            DATETIME_FORMAT1= '%Y-%m-%d %H:%M:%S'
            date_from = datetime.strptime(holiday.date_from, DATETIME_FORMAT1)
            date_to= datetime.strptime(holiday.date_to, DATETIME_FORMAT1)
            while i < len(lista):
                if holiday.holiday_status_id.code==lista[i].holiday_status_id.code:
                    if lista[i].holiday_status_id.tabulated:
                        if date_from>=date_from_payslip and date_to<=date_to_payslip:
                            total_holidays+=lista[i].number_of_days_temp
                            enter=True
                i+=1
            i=0
            if enter:   
                if holiday.holiday_status_id.code!=code:
                    input7 = {
                         'name': 'Dias de Vacacion Tomados en el periodo [dias]'+holiday.holiday_status_id.name,
                         'code': holiday.holiday_status_id.code or 'VACATION',
                         'amount': total_holidays,
                         'contract_id': contract_id,
                    }
                    res+=[input7]
                    code=holiday.holiday_status_id.code
                total_holidays=0
        return res
    
    def get_inputs(self, cr, uid, contract_ids, date_from, date_to, context=None):
        res = super(hr_payslip, self).get_inputs(cr, uid, contract_ids, date_from, date_to, context=context)
        contract_obj = self.pool.get('hr.contract')
        holidays_obj=self.pool.get('hr.holidays')
        DATETIME_FORMAT = '%Y-%m-%d'
        total_holidays=0.0
        date_from_payslip = datetime.strptime(date_from, DATETIME_FORMAT)
        date_to_payslip= datetime.strptime(date_to, DATETIME_FORMAT)
        for contract in contract_obj.browse(cr, uid, contract_ids, context=context):
            contract_id=contract.id
            work=30.00
            
            input2 = {
                 'name': 'Numero de Horas Extras Ordinarias (150%) [horas]',
                 'code': 'HORA_EXTRA_REGULAR',
                 'amount': 0.0,
                 'contract_id': contract_id,
            }            
            input3 = {
                 'name': 'Numero de Horas Extras Extraordinarias (200%) [horas]',
                 'code': 'HORA_EXTRA_EXTRAORDINARIA',
                 'amount': 0.0,
                 'contract_id': contract_id,
            }
            input4 = {
                 'name': 'Bonificaciones por Indicadores de Cumplimiento [dólares]',
                 'code': 'BONO',
                 'amount': contract.bonus or 0.0,
                 'contract_id': contract_id,
            }
            input5 = {
                 'name': 'Alimentacion [dólares]',
                 'code': 'ALIMENTACION',
                 'amount': contract.feeding or 0.0,
                 'contract_id': contract_id,
            }
            input6 = {
                 'name': 'Transporte [dólares]',
                 'code': 'TRANSPORTE',
                 'amount': contract.transport or 0.0,
                 'contract_id': contract_id,
            }
            input9 = {
                 'name': 'Comision por Ventas [dólares]',
                 'code': 'COMISION',
                 'amount': contract.commission or 0.0,
                 'contract_id': contract_id,
            }
            
            holiday_ids=holidays_obj.search(cr,uid,[('employee_id','=',contract.employee_id.id),('state','=','validate'),('type','=','remove')])
            res1=self.get_vacations(cr, uid, holiday_ids, date_from_payslip, date_to_payslip,contract_id, context)
            if res1:
                if res1[0]['code']=='IMPA':
                    work=work-res1[0]['amount']
            input1 = {
                 'name': 'Numero de días calendario trabajados (1-30) [días]',
                 'code': 'DIAS_TRABAJADOS',
                 'amount': work or 0.0,
                 'contract_id': contract_id,
            }     
            res += [input1,input2,input3,input4,input9,input5,input6]   
            for i in range(0,len(res1)):
                res.append(res1[i])
            input8={
                                       'name': 'Valor a retener al empleado por Impuesto a la Renta anticipado [dólares]',
                                       'code': 'RETENCION_IMPUESTO_RENTA',
                                       'amount': contract.income_tax_withhold or 0.0,
                                       'contract_id': contract_id,
                                       }
            res+=[input8]
        return res
    
    def _compute_year(self, cr, uid, ids, field, arg, context=None):
        ''' Función que calcula el número de años de servicio de un empleado trabajando para la empresa.'''
        
        res = {}
        DATETIME_FORMAT = "%Y-%m-%d"

        for payslip in self.browse(cr, uid, ids, context=context):
            if payslip.date_to and payslip.contract_id.date_start:               
                date_start = datetime.strptime(payslip.contract_id.date_start, DATETIME_FORMAT)
                today=datetime.strptime(payslip.date_to, DATETIME_FORMAT)
                diffyears = today.year - date_start.year
                difference = today - date_start.replace(today.year)
                days_in_year = calendar.isleap(today.year) and 366 or 365
                difference_in_years = diffyears + (difference.days + difference.seconds / 86400.0) / days_in_year
                total_years = relativedelta.relativedelta(today, date_start).years
                total_months = relativedelta.relativedelta(today, date_start).months
                months_in_years = total_months*0.083333333
                year_month = float(total_months) / 100 + total_years
                number_of_year = total_years + months_in_years  
                res[payslip.id] = number_of_year
            else:
                res[payslip.id] = 0.0
        return res
#    def _compute_prov_vacations(self, cr, uid, ids, field, arg, context=None):
#        res={}
#        DATETIME_FORMAT = "%Y-%m-%d"
#        amove_line_obj=self.pool.get('account.move.line')
#        for payslip in self.browse(cr, uid, ids, context=context):
#            if payslip.date_to: 
#                amove_line_obj.search(cr,uid,[('employee_id','=',payslip.employee_id.id)])
#        return res

    def button_proforma_voucher(self, cr, uid, ids, context=None):
        context = context or {}
        res_id=0
        inv=self.browse(cr,uid,ids)[0]
        mod_obj = self.pool.get('ir.model.data')
        res = mod_obj.get_object_reference(cr, uid, 'account_voucher', 'view_vendor_payment_form')
        res_ids = res and res[1] or False,
        res_id=res_ids[0]
        return {
            'name': _('Voucher Payment'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res_id],
            'res_model': 'account.voucher',
            'context': {
                    'invoice_type':'in_invoice',
                    'default_partner_id':inv.employee_id.address_home_id.id,
                    'invoice_type':'in_invoice',
                    'type':'payment',
                    'default_type':'payment',
                },
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': False,
        }
    def _change_state(self, cr, uid, ids, field, arg, context=None):
        res={}
        if context is None:
            context = {}
        toremove = []
        obj_move_line = self.pool.get('account.move.line')
        payslip=self.browse(cr,uid,ids)[0]
        done = {}
        if not payslip.state in ['draft']:
            account_voucher_obj=self.pool.get('account.voucher')
            account_move_obj=self.pool.get('account.move')
            if payslip.move_id:                
                move=payslip.move_id
                line_ids = map(lambda x: x.id, move.line_id)
                context['journal_id'] = move.journal_id.id
                context['period_id'] = move.period_id.id 
                for line in obj_move_line.browse(cr, uid, line_ids, context=context):
                   # err_msg = _('Move name (id): %s (%s)') % (line.move_id.name, str(line.move_id.id))
                    if line.move_id.state == 'posted' and line.journal_id.entry_posted and line.reconcile_id:
                        self.write(cr,uid,ids[0],{'state':'paid'})
                        res[payslip.id]=True
        return res
    _columns={
              'time_in': fields.function(_compute_year, string='No. of years of service', type='float', store=False, method=True, help='Total years of work experience in the company'),
              'if_pay':fields.function(_change_state,string='If Pay',type='boolean',store=False,method=True),
              'state': fields.selection([
                        ('draft', 'Draft'),
                        ('verify', 'Waiting'),
                        ('done', 'Done'),
                        ('paid','Paid'),
                        ('cancel', 'Rejected'),
                    ], 'Status', select=True, readonly=True,
                        help='* When the payslip is created the status is \'Draft\'.\
                        \n* If the payslip is under verification, the status is \'Waiting\'. \
                        \n* If the payslip is confirmed then status is set to \'Done\'.\
                        \n* When user cancel payslip the status is \'Rejected\'.',track_visibility='onchange'),
              #'prov_vacations':fields.function(_compute_prov_vacations,string='Provision for Accrued Vacation', type='float', store=False, method=True, help='Provision for accrued vacation'),
              'analytics_id': fields.many2one('account.analytic.plan.instance', 'Analytic Distribution', 
                                              readonly=True, 
                                              states={'draft':[('readonly',False)]},
                                              help='Distributes the expense made on this employee into several cost centers by percentage, the value can be preset in the employee contract',
                                              track_visibility='onchange',
                                              ),
              }
    
    def _default_analytics_id(self, cr, uid, context=None):
        '''
        Valor por defecto para distribución de centros de costos
        Se define el metodo para poder ser heredado y expandido en modulos futuros
        '''
        return False
    
    def onchange_contract_id(self, cr, uid, ids, date_from, date_to, employee_id=False, contract_id=False, context=None):
        '''
        Crea una copia de la distribucion analitica del contrato relacionado
        y le asigna a la nomina. La copia es necesaria porque a futuro se podria modificar la 
        distribucion analitica.
        '''
        res = super(hr_payslip, self).onchange_contract_id(cr, uid, ids, date_from, date_to, employee_id, contract_id, context)
        
        if contract_id:
            contract_analytics_id = self.pool.get("hr.contract").browse(cr,uid,contract_id).analytics_id.id
            if contract_analytics_id:
                contract_analytics_obj = self.pool.get("account.analytic.plan.instance")
                analytics_id = contract_analytics_obj.copy(cr, uid, contract_analytics_id, context)
                res['value'].update({'analytics_id': analytics_id})
        return res

    def copy(self, cr, uid, id, default=None, context=None): 
        '''
        - Setea las fechas al dia de hoy (caso contrario no se puede duplicar por un costraint)
        - Crea una copia de la distribucion analitica del contrato relacionado
        y le asigna a la nomina. La copia es necesaria porque a futuro se podria modificar la 
        distribucion analitica.
        '''
        if not context:
            context={}
        if not default: default={}
        
        if 'default_employee_id' in context:
            default_date_from = time.strftime('%Y-%m-01')
            default_date_to = str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1))[:10]
            employee = self.pool.get("hr.employee").browse(cr,uid,context['default_employee_id'])
            contract = self.get_contract(cr, uid, employee, default_date_from, default_date_to, context)
            default.update({
                            'default_date_from': default_date_from,
                            'default_date_to': default_date_to,
                            })  
            if contract and contract[0]:
                contract_analytics_id = self.pool.get("hr.contract").browse(cr,uid,contract[0]).analytics_id.id
                if contract_analytics_id:
                    contract_analytics_obj = self.pool.get("account.analytic.plan.instance")
                    analytics_id = contract_analytics_obj.copy(cr, uid, contract_analytics_id, context)
                    default.update({'analytics_id': analytics_id})                
        
        return super(hr_payslip, self).copy(cr, uid, id, default, context)

    _defaults = {
        'analytics_id' : _default_analytics_id,
        #'code':_create,        
        }
hr_payslip()