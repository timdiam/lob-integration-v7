from osv import osv, fields 
from datetime import datetime
import calendar
from dateutil.relativedelta import relativedelta
from tools.translate import _

class hr_employee(osv.osv):
#    _inherit = "hr.employee"
    _name = "hr.employee"
    _inherit = ['hr.employee','mail.thread']

    def _get_correct_contract(self,cr,uid, employee_id,advance_date):
        res =False
        obj_contract = self.pool.get('hr.contract')
        for emp in self.browse(cr, uid, [employee_id]):
            contract_ids = obj_contract.search(cr, uid, [('employee_id','=',emp.id),], order='date_start')
            if contract_ids:
                for contract in obj_contract.browse(cr,uid,contract_ids):
                    if advance_date<=contract.date_end or contract.date_end==False:
                        if advance_date>=contract.date_start:
                            res=contract.id       
            else:
                res = False
        return res
    def _get_contract_vigente(self,cr,uid, employee_id,date_end,date_start):
        res =False
        obj_contract = self.pool.get('hr.contract')
        for emp in self.browse(cr, uid, [employee_id]):
            contract_ids = obj_contract.search(cr, uid, [('employee_id','=',emp.id),], order='date_start')
            if contract_ids:
                for contract in obj_contract.browse(cr,uid,contract_ids):
                    if date_start>=contract.date_start:
                        res=True
                    if contract.date_end<date_end and contract.date_end:
                        res=False
            else:
                res = False
        return res
    def _get_user(self, cr, uid, ids, field, arg, context=None):
        res = {}
        user_obj=self.pool.get('res.users')
        for employee in self.browse(cr, uid, ids, context=context):
            user=user_obj.search(cr,uid,[('partner_id','=',employee.address_home_id.id)])
            if user:
                user_id=user_obj.browse(cr,uid,user[0])
                res[employee.id]=user_id and user_id.id or False
        return res
    
    def _get_ids_from_partner(self, cr, uid, ids, context=None):
        
        res = []
        name=self.pool.get('res.partner').browse(cr, uid, ids, context=context)[0].name
        employee_obj=self.pool.get('hr.employee')
        employee_id=employee_obj.search(cr,uid,[('address_home_id','in',ids)])
        for employee in employee_obj.browse(cr,uid,employee_id):
            res.append(employee.id)
            self.pool.get('resource.resource').write(cr,uid,employee.resource_id.id,{'name':name})
        return res
    
    def _get_name_res(self, cr, uid, ids, field_name, arg, context=None):
        
        res = dict.fromkeys(ids, False)
        for employee in self.browse(cr, uid, ids, context=context):
            if employee.address_home_id:
                #res[employee.id] = employee.address_home_id.name
                res[employee.id]=employee.resource_id.id
                self.pool.get('resource.resource').write(cr,uid,employee.resource_id.id,{'name':employee.address_home_id.name})
            elif employee.name_related:
                res[employee.id] = employee.name_related
        return res
    
    def _get_name_addres_res(self, cr, uid, ids, context=None):
        res = dict.fromkeys(ids, False)
        for employee in self.browse(cr, uid, ids, context=context):
            if employee.address_home_id:
                res[employee.id] = employee.address_home_id.name
                #res[employee.resource_id.id]=employee.address_home_id.name
            elif employee.name_related:
                res[employee.id] = employee.name_related
        return res
    
    def create(self, cr, uid, vals, context=None):
        address_home_id=vals.get('address_home_id', False)
        name_related=vals.get('name_related', False)
        resource_id=vals.get('resource_id',False)

        emp_with_same_address_home_id = self.search(cr,uid,[('address_home_id','=',address_home_id)],context=context)

        if len(emp_with_same_address_home_id) != 0:
            emp_obj = self.browse(cr,uid,emp_with_same_address_home_id,context=context)
            raise osv.except_osv( ( 'Error' ), ( 'Ya hay un empleado ligado a ese proveedor.  Revisa el empleado: ' +
                                                emp_obj[0].name) )

        if address_home_id:
            name=self.pool.get('res.partner').browse(cr,uid,address_home_id).name
            vals['name']=name.encode('ascii','ignore')
        return super(hr_employee, self).create(cr, uid, vals, context=context)

    _columns={
                'marital': fields.selection([('single', 'Single'), ('married', 'Married'), ('widower', 'Widower'), ('divorced', 'Divorced'),('cohabitation','Cohabitation')], 'Marital Status'),
                'user_id':fields.function(_get_user,string="Related User",type='many2one', relation="res.users",store=True,method=True,track_visibility='onchange'),             
                'department_id':fields.many2one('hr.department', 'Department',track_visibility='onchange'),
                'address_id': fields.many2one('res.partner', 'Working Address',track_visibility='onchange'),
                'address_home_id': fields.many2one('res.partner', 'Name',track_visibility='onchange',), 
                'name_related': fields.related('resource_id', 'name', type='char', string='Name', readonly=True, 
                                               store={
                                                    'res.partner': (_get_ids_from_partner, ['name'], 10),
                                                    'hr.employee':(_get_name_addres_res, ['address_home_id'], 10),
                                                    }),
#                'name':fields.function(_get_name_res, type='char',required=False,method=True,
#                                        string="Name",readonly=False,
#                                        store={'res.partner': (_get_ids_from_partner, ['name'], 10),
#                                               'hr.employee':(_get_name_addres_res, ['address_home_id'], 10)}
#                                        ),
                'parent_id': fields.many2one('hr.employee', 'Manager',track_visibility='onchange'),
                'coach_id': fields.many2one('hr.employee', 'Coach',track_visibility='onchange'),
                'job_id': fields.many2one('hr.job', 'Job',track_visibility='onchange'),
                'work_phone': fields.char('Work Phone', size=32, readonly=False,track_visibility='onchange'),
                'mobile_phone': fields.char('Work Mobile', size=32, readonly=False,track_visibility='onchange'),
                'bank_account_id':fields.many2one('res.partner.bank', 'Bank Account Number',track_visibility='onchange', domain="[('partner_id','=',address_home_id)]", help="Employee bank salary account"),
                'passport_id':fields.char('Passport No', size=64,track_visibility='onchange'),
                'identification_id': fields.char('Identification No', size=32,track_visibility='onchange'),
                'resource_id': fields.function(_get_name_res, type='many2one',obj='resource.resource',ondelete='cascade',required=True,method=True,
                                        string="Resource",readonly=False,
                                        store={'res.partner': (_get_ids_from_partner, ['name'], 10),
                                               'hr.employee':(_get_name_addres_res, ['address_home_id'], 10)}
                                        ),
                #'remaining_leaves': fields.function(_get_remaining_days, string='Remaining Legal Leaves',track_visibility='onchange', fnct_inv=_set_remaining_days, type="float", help='Total number of legal leaves allocated to this employee, change this value to create allocation/leave request. Total based on all the leave types without overriding limit.'),

              }
    _order = 'name_related'
hr_employee()