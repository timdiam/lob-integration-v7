# -*- encoding: utf-8 -*-
########################################################################
#                                                                       
# @authors:  Andrea García                                                                           
# Copyright (C) 2013  TRESCLOUD Cia Ltda.                                 
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

import time
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta
from osv import osv, fields 
import calendar
from tools.translate import _



class hr_payslip_employees(osv.osv):

    _inherit = 'hr.payslip.employees'
    def compute_sheet(self, cr, uid, ids, context=None):
        emp_pool = self.pool.get('hr.employee')
        run_pool = self.pool.get('hr.payslip.run')
        data = self.read(cr, uid, ids, context=context)[0]
        employee_ids=[]
        run_data = {}
        if context and context.get('active_id', False):
            run_data = run_pool.read(cr, uid, context['active_id'], ['date_start', 'date_end', 'credit_note'])
        from_date =  run_data.get('date_start', False)
        to_date = run_data.get('date_end', False)
        for emp in emp_pool.browse(cr, uid, data['employee_ids'], context=context):
            status=emp_pool._get_contract_vigente(cr,uid, emp.id,to_date,from_date)
            if status:
                employee_ids.append(emp.id)
        self.write(cr,uid,ids,{'employee_ids':[(6,0,employee_ids)]},context)
        res=super(hr_payslip_employees, self).compute_sheet(cr, uid, ids, context=context)
        return res
hr_payslip_employees()