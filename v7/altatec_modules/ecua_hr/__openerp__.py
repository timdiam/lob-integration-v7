# -*- coding: utf-8 -*-
########################################################################
#                                                                       
# @authors:TRESCLOUD Cia.Ltda Andrea García                                                                          
# Copyright (C) 2013                                  
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
#ice
########################################################################
{
   "name" : "Módulo para personalizar la vista de planilla de pagos de empleados",
   "author" : "TRESCLOUD Cia. Ltda.",
   "maintainer": 'TRESCLOUD Cia. Ltda.',
   "website": 'http://www.trescloud.com',
   'complexity': "easy",
   "description": """
      
   Este sistema permite agregar automaticamente lineas con valor cero a Dias trabajados (HORA_EXTRA_REGULAR, HORA_EXTRA_EXTRAORDINARIA, DIAS_DEL_MES Y DIAS_TRABAJADOS) y 
   Otros Ingresos (BONIFICACIÓN, COMISIÓN, TRANSPORTE Y ALIMENTACIÓN).
   Se agrega el campo number_of_year tipo función en el objeto hr.contract.
       
   Desarrollador:
   
   Andrea García
   Carlos Yumbillo
   
   
   """,
   "category": "Human Resources",
   "version" : "1.0",
   'depends': ['base','hr_contract','hr_payroll','account',
               'hr_payroll_account',
               'report_aeroo',
               'report_aeroo_ooo',
               'hr_attendance',
               #'account_analytic_plans_simplified',
               ],
   'init_xml': [],
   'data':     [
                    'data/decimos_pagados_data.xml',
                ],
   'update_xml': [
                  'data/hr_contract_work_days_data.xml',
                  'data/hr_contract_type_data.xml',
                  'report/report_data.xml',
                  'security/ir.model.access.csv',
                  'views/hr_payroll_structure_view.xml',
                  'views/hr_payroll_workflow.xml',
                  'views/ecua_hr_view.xml',
                  'views/hr_payslip_view.xml',
                  'views/hr_holidays_view.xml',
                  'views/hr_salary_rule_view.xml',
                  'views/res_config_view.xml',
                  'views/hr_employee_view.xml',
                  'views/res_company_view.xml',
                  'views/hr_payslip_run_view.xml',
                  ],
   'installable': True,
}
