import locale
from openerp.osv import fields, osv, orm
import logging
from amount_to_words import amount_to_words_es
from date_to_words import date_to_words_es

_logger = logging.getLogger(__name__)

import time
from datetime import datetime
from datetime import date
from dateutil.relativedelta import relativedelta
from openerp.osv import fields
from openerp.osv import osv
from openerp.tools.translate import _
from openerp import netsvc

locale.setlocale(locale.LC_ALL,'en_US.utf8')

class account_invoice(osv.osv):

    _inherit="account.invoice"

    def create(self, cr, uid, vals, context=None):
        res = super(account_invoice, self).create(cr, uid, vals, context=context)
        return res


class opportunity_product_line(osv.osv):
    _name = "opportunity.product.line"

    _columns={
        "product_id":fields.many2one("product.product", string="Producto",domain=[('sale_ok','=',True)]),
        "quantity":fields.float("Cantidad"),
        "opportunity":fields.many2one("crm.lead", string="Oportunidad"),
        }

    _defaults= {
                'quantity':None,
    }

    def create(self, cr, uid, vals, context=None):

        if 'quantity' in vals and vals['quantity'] <= .0000001:
            raise osv.except_osv( ( 'ERROR' ),
                                  ( 'La linea de solicitud debe tener un valor mayor a cero' ) )

        return super(osv.osv,self).create(cr, uid, vals, context=context)


    def write(self, cr, uid, ids, vals, context={}):
        if 'quantity' in vals and vals['quantity'] <= .0000001:
            raise osv.except_osv( ( 'ERROR' ),
                                  ( 'La linea de solicitud debe tener un valor mayor a cero' ) )

        return super(osv.osv, self).write(cr,uid,ids,vals,context=context)





class opportunity_product_line(osv.osv):
    _inherit = "opportunity.product.line"

    _columns={
        "observacion":fields.text("Observacion"),
        "descripcion":fields.text("Descripcion"),

        }



class ajuste_cotizacion_wizard(osv.osv):
    _name = "ajuste.cotizacion.wizard"

    _columns={
        "prueba":fields.char("Nombre"),

        }

class sale_order(osv.osv):
    _inherit = "sale.order"

    def _prepare_order_picking(self, cr, uid, order, context=None):
        res = super(sale_order, self)._prepare_order_picking(cr, uid, order, context=context)
        res.update( { 'date'     : fields.date.context_today(self, cr, uid, context=context),
                      'min_date' : order.date_order,
                    })
        return res

    ###################################################################################################
    # Take the description and observation from the sale order and put them on the procurement
    ###################################################################################################
    def _prepare_order_line_procurement(self, cr, uid, order, line, move_id, date_planned, context=None):
        res=super(sale_order, self)._prepare_order_line_procurement(cr, uid, order,line,move_id,date_planned, context=context)
        res.update({ 'descripcion_mrp' : line.name,
                     'observation'     : line.observacion,
                     'internal_note'   : line.order_id.notas_internas,
                     'from_sale_order'    : True,
                   })
        return res

    def ajuste_cotizacion(self, cr, uid, ids, context=None):
        r = self.browse(cr, uid, ids, context=context)[0]
        return {
            'type':'ir.actions.act_window',
            'name':'AJuste de Cotizacion',
            'view_mode':'form',
            'view_type':'form',
            'res_model': 'ajuste.cotizacion.wizard',
            'nodestroy':'true',
            'target':'new',
            'context':{},
            }


    def action_confirm_operaciones(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'listo_confirmar'}, context=context)
        return True

    def listo_pendiente_confirmar(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'draft'}, context=context)
        return True

    def cmpt_discount(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}
        prov_totals = 0
        ss_tots=0
        for r in records:
            for entry in r.order_line:
                ss_tots = entry.price_subtotal + ss_tots
                prov_totals = prov_totals + (entry.price_unit * entry.product_uom_qty)
            result[r.id] = prov_totals-ss_tots

        return result

    def cmpt_total_before_discount(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)
        result = {}
        prov_totals = 0
        for r in records:
            for entry in r.order_line:
                prov_totals = prov_totals+(entry.price_unit * entry.product_uom_qty)
            result[r.id] = prov_totals
        return result

    #Dont update the payment term on onchange partner
    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        res = super(sale_order, self).onchange_partner_id( cr, uid, ids, part, context=context)
        if 'payment_term' in res['value']:
            del res['value']['payment_term']

        return res

    _columns={
        "opportunity":fields.many2one("crm.lead", string="Oportunidad"),
        'state': fields.selection([
            ('draft', 'Borrador'),
            ('operaciones_comerciales', 'Esperando Operaciones Comerciales'),
            ('pendiente_operaciones', 'Pendiente Operaciones Comerciales'),
            ('listo_confirmar', 'Cotizado'),
            ('perdida', 'Perdida'),
            ('sent', 'Quotation Sent'),
            ('orden_dp_stand', 'Orden de Producion Stand by'),
            ('orden_dp_terminado', 'Orden de Producion Terminado'),
            ('instalado', 'Instalado'),
            ('cerrado', 'Cerrado'),
            ('cancel', 'Cancelled'),
            ('waiting_date', 'Waiting Schedule'),
            ('progress', 'Sales Order'),
            ('manual', 'Sale to Invoice'),
            ('invoice_except', 'Invoice Exception'),
            ('done', 'Done'),
            ('shipping_except','Excepcion de Envio'),
            ], 'Status', #readonly=True,
            track_visibility='onchange',),
        "alternativa":fields.char("Alternativa", size=2),
        "version":fields.char("Version"),
        'notas_internas':fields.text('Notas Internas'),
        'client_contact':fields.many2one('res.partner',string="Contacto"),
        "validez_oferta":fields.char("Validez de Oferta"),
        'order_line': fields.one2many('sale.order.line', 'order_id', 'Order Lines', readonly=True, states={'operaciones_comerciales': [('readonly', False)],'draft': [('readonly', False)], 'sent': [('readonly', False)]}),
        'discount':fields.function(cmpt_discount, type='float', method=True, string='Descuento ($)'),
        'total_before_discount':fields.function(cmpt_total_before_discount, type='float', method=True, string='Total Antes Desc.'),
        'create_date'       : fields.datetime('Fecha de Creacion', readonly=True),
        }

    _defaults = {

        "state":"draft",
        "validez_oferta":'10 dias',
        }

class crm_lead(osv.osv):
    _inherit='crm.lead'

    def onchange_partner_id(self, cr, uid, ids, partner_id, email_from, context=None):
        res=super(crm_lead, self).onchange_partner_id(cr, uid, ids, partner_id, email_from)

        if partner_id:
            partner_obj=self.pool.get('res.partner').browse(cr, uid, partner_id, context)

            is_company=partner_obj.is_company
            res['value'].update({'is_company': is_company,'client_contact':None})

            return res

        else:
            return res



    _columns={ "opportunity_product_line" : fields.one2many("opportunity.product.line","opportunity", string="Linea de Producto"),
               "fecha_entrega" : fields.date("Fecha de Entrega"),
               "dias_credito"  : fields.integer("Dias a Credito"),
               "presupuesto"   : fields.one2many("sale.order","opportunity", string="Linea de Presupuesto"),
               'partner_id'    : fields.many2one('res.partner', 'Partner', ondelete='set null', track_visibility='onchange',
                                                 select=True, help="Linked partner (optional). Usually created when converting the lead.", required=True ),
               'email_from'    : fields.char('Email', size=128, help="Email address of the contact", select=1, required=True),
               'phone'         : fields.char("Phone", size=64, required=True),
               'client_contact': fields.many2one('res.partner', string="Contacto"),
               "is_company":fields.related('partner_id','is_company', type="boolean", relation='res.partner', string="Es Empresa"),
               'create_date'       : fields.datetime('Fecha de Creacion', readonly=True),
               }


class sale_order_line(osv.osv):
    _inherit='sale.order.line'

    _columns={
        "boceto_externo":fields.boolean("Boceto Externo"),
        "boceto_interno":fields.boolean("Boceto Interno"),
        "fotomontaje":fields.boolean("Fotomontaje"),
        "observacion":fields.text("Observacion"),
        "sequence":fields.integer("Secuencia"),




        }



class hr_contract(osv.osv):
    _inherit = "hr.contract"


    def get_wage_in_word(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)[0]
        result = {}
        wage_in_words = amount_to_words_es(records.wage)
        result[records.id] = wage_in_words
        return result

    def get_date_in_word(self, cr, uid, ids, field_name, field_value, arg, context=None):
        records = self.browse(cr, uid, ids)[0]
        result = {}
        date_in_words = date_to_words_es(records.date_start)
        result[records.id] = date_in_words
        return result


    _columns={
        "wage_in_word":fields.function(get_wage_in_word, string='Salario en Letras', method=True, type='char'),
        "date_in_word":fields.function(get_date_in_word, string="Fecha en Letras", type='char'),
        }


class procurement_order(osv.osv):

    _inherit = 'procurement.order'

    def make_mo(self, cr, uid, ids, context=None):

        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context).company_id
        production_obj = self.pool.get('mrp.production')
        move_obj = self.pool.get('stock.move')
        wf_service = netsvc.LocalService("workflow")
        procurement_obj = self.pool.get('procurement.order')
        for procurement in procurement_obj.browse(cr, uid, ids, context=context):
            res_id = procurement.move_id.id
            newdate = datetime.strptime(procurement.date_planned, '%Y-%m-%d %H:%M:%S') - relativedelta(days=procurement.product_id.produce_delay or 0.0)
            newdate = newdate - relativedelta(days=company.manufacturing_lead)
            produce_id = production_obj.create(cr, uid, {
                'origin': procurement.origin,
                'product_id': procurement.product_id.id,
                'product_qty': procurement.product_qty,
                'product_uom': procurement.product_uom.id,
                'product_uos_qty': procurement.product_uos and procurement.product_uos_qty or False,
                'product_uos': procurement.product_uos and procurement.product_uos.id or False,
                'bom_id': procurement.bom_id and procurement.bom_id.id or False,
                'date_planned': newdate.strftime('%Y-%m-%d %H:%M:%S'),
                'move_prod_id': res_id,
                'company_id': procurement.company_id.id,
            })

            res[procurement.id] = produce_id
            self.write(cr, uid, [procurement.id], {'state': 'running', 'production_id': produce_id})
            bom_result = production_obj.action_compute(cr, uid,
                    [produce_id], properties=[x.id for x in procurement.property_ids])
            #wf_service.trg_validate(uid, 'mrp.production', produce_id, 'button_confirm', cr) # We DON'T confirm the production order
        self.production_order_create_note(cr, uid, ids, context=context)

        for orden in res.values():
            order=self.pool.get('mrp.production').browse(cr,uid,orden,context=context)
            procurement=self.browse(cr, uid, ids, context=context)
            for procure in procurement:
                fecha_entrega=datetime.strptime(procure.date_planned[:-8]+" "+"05:00:00",'%Y-%m-%d %H:%M:%S')
            prueba=order.date_planned
            self.pool.get('mrp.production').write(cr, uid,[order.id], {'date_planned': fecha_entrega.strftime('%Y-%m-%d %H:%M:%S')}, context=context)
        return res

    _columns = { 'descripcion_mrp' : fields.text('Descripcion'),
                 'observation'     : fields.text('Observacion'),
                 'internal_note'   : fields.text('Notas internas'),
                 'from_sale_order'  : fields.boolean("Cantidad esta readonly?"),
               }

    _defaults = { 'from_sale_order' : False,
                }

class mrp_production(osv.osv):

    _inherit = "mrp.production"

    ############################################################################################
    # create() override for mrp.production
    ############################################################################################
    def create(self, cr, uid, vals, context={}):
        res = super(mrp_production, self).create(cr, uid, vals, context)

        if 'move_prod_id' not in vals:
            return res
        else:
            procurement_id = self.pool.get('procurement.order').search(cr,uid,[('move_id.id', '=',vals['move_prod_id'] )])
            procurement=self.pool.get('procurement.order').browse(cr,uid,procurement_id)
            validacion=len(procurement)
            if (validacion>1):
                raise osv.except_osv(
                    _('Hay mas de una orden!'),
                    _('Contactar a Altatec!'))

            if (validacion==0):
                raise osv.except_osv(
                    _('No hay orden!'),
                    _('Contactar a Altatec!'))

            if(procurement[0].descripcion_mrp):
                self.pool.get('mrp.production').write(cr, uid, res, { 'description' : procurement[0].descripcion_mrp }, context=context)

            if(procurement[0].observation):
                self.pool.get('mrp.production').write(cr, uid, res, { 'observation' : procurement[0].observation }, context=context)

            if(procurement[0].internal_note):
                self.pool.get('mrp.production').write(cr, uid, res, { 'internal_note' : procurement[0].internal_note }, context=context)

            if(procurement[0].from_sale_order):
                self.pool.get('mrp.production').write(cr, uid, res, { 'from_sale_order' : procurement[0].from_sale_order }, context=context)

            return res

    _columns = { 'description'     : fields.text('Descripcion'),
                 'observation'     : fields.text('Observacion'),
                 'internal_note'   : fields.text('Notas internas'),
                 'from_sale_order' : fields.boolean("Cantidad esta readonly?"),
               }

    _defaults = { 'from_sale_order' : False,
                }


class crm_make_sale(osv.osv):
    _inherit='crm.make.sale'

    def makeOrder(self, cr, uid, ids, context=None):
        records=self.browse(cr,uid,ids)[0]
        value = super(crm_make_sale, self).makeOrder(cr, uid, ids, context=context)
        id_sale_order=int(value['res_id'])
        opportunity_id= self.pool.get('crm.lead').search(cr,uid,[('ref', '=', 'sale.order,%s' % id_sale_order)])
        opportunity= self.pool.get("crm.lead").browse(cr, uid, opportunity_id)[0]
        notas_internas=opportunity.description
        fechaDeOrden=opportunity.fecha_entrega
        sale_order=self.pool.get('sale.order').browse(cr,uid,id_sale_order)
        sale_order.write({'opportunity':opportunity.id,
                          'state':'draft',
                          'client_contact':opportunity.client_contact.id,
                          'client_order_ref':opportunity.name,
                          'date_order':fechaDeOrden,
                          'notas_internas':notas_internas,
                          'user_id':opportunity.user_id.id,
                          'payment_term':9,
                          'validez_oferta':'10 dias',
                          })
        for line in opportunity.opportunity_product_line:
            producto= line.product_id.id
            cantidad=line.quantity
            nombre=line.product_id.name
            precio=line.product_id.list_price
            unidad=line.product_id.uom_id.id
            descripcion = line.descripcion
            observacion =line.observacion
            if (len(line.product_id.customer_tax_shadow)==0):
                # raise Exception("Debe elegir un impuesto para el producto ingresado")
                raise orm.except_orm( 'Error', "Debe elegir un impuesto para el producto ingresado" )

            tax_list = []
            if line.product_id.customer_tax_shadow:
                for tax in line.product_id.customer_tax_shadow:
                    tax_list.append( tax.id )

            sale_order_line_id= self.pool.get('sale.order.line').create(cr,uid,{
                'ecua_shadow_taxes': [(6,0,tax_list)],
                'order_id':sale_order.id,
                'product_id':producto,
                'product_uom_qty':cantidad,
                'name':nombre,
                'price_unit':precio,
                'product_uom':unidad,
                'type':'make_to_order',
                'state':'draft',
                'name':descripcion if descripcion else "n/a",
                'observacion':observacion,

                })

        return value