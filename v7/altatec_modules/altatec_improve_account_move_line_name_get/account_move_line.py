from openerp.osv import fields, osv

class account_move_line(osv.osv):
    _inherit = 'account.move.line'

    def name_get(self, cr, uid, ids, context=None):
        if not ids:
            return []
        result = []
        for line in self.browse(cr, uid, ids, context=context):
            if line.ref:
                result.append((line.id, (line.move_id.name or '')+' ('+line.ref+')' + " " + line.invoice.internal_number if line.invoice else "") )
            else:
                result.append((line.id, line.move_id.name + " " + (line.invoice.internal_number if line.invoice else "") + (line.name if line.name else "")  ))
        return result