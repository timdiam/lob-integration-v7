{ 'name'         : 'AltaTec Improve account move line nameget',
  'version'      : '1.0',
  'description'  : """
                   This module improves the nameget feature of account.move.line.
                   """,
  'author'       : 'Tim Diamond',
  'website'      : 'www.altatec.ec',
  "depends"      : [ 'base','account',
                   ],
  "data"         : [  ],
  "installable"  : True,
  "auto_install" : False
}
